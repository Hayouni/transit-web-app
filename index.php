<?php
/**
 * This file is part of the GTeCh+ Group.
 
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
// If user is already logged in, header that weenis away
if($user_ok == true){
	header("location: public/users/user?u=".$_SESSION["username"]);
    exit();
}
?><?php
// AJAX CALLS THIS LOGIN CODE TO EXECUTE
if(isset($_POST["e"])){
	// CONNECT TO THE DATABASE
	include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
	// GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
	$e = mysqli_real_escape_string($db_conx, $_POST['e']);
	$p = $_POST['p'];//md5($_POST['p']);
	// GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
	// FORM DATA ERROR HANDLING
	if($e == "" || $p == ""){
		echo "login_failed";
        exit();
	} else {
	// END FORM DATA ERROR HANDLING
		$sql = "SELECT id, username, password, name, subname, avatar, userlevel,
        mail_autoload,email,smtp_pass,imap_host,imap_port,smtp_host,smtp_port 
        FROM users WHERE username='$e' AND activated='1' LIMIT 1";
        $query = mysqli_query($db_conx, $sql);
        $row = mysqli_fetch_row($query);
		$db_id = $row[0];
		$db_username = $row[1];
        $db_pass_str = $row[2];
        $full_name = $row[3].' '.$row[4];
        $avatar = $row[5];
        $userlevel = $row[6];
        $mail_autoload = $row[7];
        $email = ($row[8]) ? $row[8] : 'off' ;
        $smtp_pass = ($row[9]) ? 'on' : 'off' ;
        $imap_host = ($row[10]) ? 'on' : 'off' ;
        $imap_port = ($row[11]) ? 'on' : 'off' ;
        $smtp_host = ($row[12]) ? 'on' : 'off' ;
        $smtp_port = ($row[13]) ? 'on' : 'off' ;
        if($avatar != ""){
            $pic = '/GTRANS/public/users/user_data/'.$db_username.'/'.$avatar.'';
        } else {
            $pic = '/GTRANS/assets/images/default-img.png';
        }
		if($p != $db_pass_str){
			echo "login_failed";
            exit();
		} else {
			// CREATE THEIR SESSIONS AND COOKIES
			$_SESSION['userid'] = $db_id;
            $_SESSION['full_name'] = $full_name;
			$_SESSION['username'] = $db_username;
			$_SESSION['password'] = $db_pass_str;
            $_SESSION['user_pic'] = $pic;
            $_SESSION['email'] = $email;
            $_SESSION['smtp_pass'] = $smtp_pass;
            $_SESSION['mail_autoload'] = $mail_autoload;
            $_SESSION['imap_host'] = $imap_host;
            $_SESSION['imap_port'] = $imap_port;
            $_SESSION['smtp_host'] = $smtp_host;
            $_SESSION['smtp_port'] = $smtp_port;
            $_SESSION['LEVEL'] = $userlevel;
            $_SESSION['lock'] = '0';
			setcookie("id", $db_id, strtotime( '+30 days' ), "/", "", "", TRUE);
			setcookie("user", $db_username, strtotime( '+30 days' ), "/", "", "", TRUE);
    		setcookie("pic", $pic, strtotime( '+30 days' ), "/", "", "", TRUE);
            setcookie("fname", $full_name, strtotime( '+30 days' ), "/", "", "", TRUE); 
            setcookie("lock", '0', strtotime( '+30 days' ), "/", "", "", TRUE); 
			// UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
			$sql = "UPDATE users SET ip='$ip', lastlogin=now(), lock_account='0' WHERE username='$db_username' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
			echo $db_username;
		    exit();
		}
	}
	exit();
}
?>
    <!DOCTYPE html>
    <html lang="fr">

    <head>

        <head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>GTARNS|S'identifier</title>

            <!-- Global stylesheets -->
            <link href="assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
            <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
            <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
            <link href="assets/css/core.css" rel="stylesheet" type="text/css">
            <link href="assets/css/components.css" rel="stylesheet" type="text/css">
            <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
            <!-- /global stylesheets -->

            <!-- Core JS files -->
            <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
            <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
            <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
            <script type="text/javascript" src="assets/js/plugins/extensions/session_timeout.min.js"></script>
            <!-- /core JS files -->

            <!-- Theme JS files -->
            <script type="text/javascript" src="assets/js/core/app.js"></script>
            <!-- /theme JS files -->

            <script>
                function emptyElement(x) {
                    _(x).innerHTML = "";
                }

                function _(x) {
                    return document.getElementById(x);
                }

                function ajaxObj(meth, url) {
                    var x = new XMLHttpRequest();
                    x.open(meth, url, true);
                    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    return x;
                }

                function ajaxReturn(x) {
                    if (x.readyState == 4 && x.status == 200) {
                        return true;
                    }
                }

                function login() {
                    var e = _("username").value;
                    var p = _("password").value;
                    if (e == "" || p == "") {
                        _("status").innerHTML = '<span class="label border-left-info label-striped" style="font-size: 13px;" id="status">Remplissez toutes les données du formulaire</span>';
                    } else {
                        _("loginbtn").disabled = true;
                        _("status").innerHTML = '<span class="label border-left-info label-striped" style="font-size: 13px;" id="status">Patientez s\'il-vous-plait ...</span>';
                        var ajax = ajaxObj("POST", "index.php");
                        ajax.onreadystatechange = function() {
                            if (ajaxReturn(ajax) == true) {
                                if (ajax.responseText == "login_failed") {
                                    _("status").innerHTML = '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Authentification échouée !<br/> - peuvent être données erronées <br/>ou compte pas encore activé </span>';;
                                    _("loginbtn").disabled = false;
                                } else {
                                    window.location = "public/users/user?u=" + ajax.responseText ;//+".php";
                                }
                            }
                        }
                        ajax.send("e=" + e + "&p=" + p);
                    }
                }
            </script>
        </head>

        <body class="login-container">
            <!-- Main navbar -->
            <div class="navbar navbar-inverse">
                <div class="navbar-header">
                    <a class="navbar-brand position-right"><img src="assets/images/medways_light.png" alt=""></a>

                    <ul class="nav navbar-nav pull-right visible-xs-block">
                        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    </ul>
                </div>
            </div>
            <!-- /main navbar -->


            <!-- Page container -->
            <div class="page-container">

                <!-- Page content -->
                <div class="page-content">

                    <!-- Main content -->
                    <div class="content-wrapper">

                        <!-- Content area -->
                        <div class="content">

                            <!-- Simple login form -->
                            <form id="loginform" onsubmit="return false;">
                                <div class="panel panel-body login-form">
                                    <div class="text-center">
                                        <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                                        <h5 class="content-group">Connectez-vous à votre compte <small class="display-block">Entrez vos informations d'identification ci-dessous</small></h5>
                                    </div>

                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" onfocus="emptyElement('status')" maxlength="88" class="form-control" placeholder="Username" name="username" id="username">
                                        <div class="form-control-feedback">
                                            <i class="icon-user text-muted"></i>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="password" onfocus="emptyElement('status')" maxlength="100" class="form-control" placeholder="Password" name="password" id="password">
                                        <div class="form-control-feedback">
                                            <i class="icon-lock2 text-muted"></i>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" name="loginbtn" id="loginbtn" onclick="login()" class="btn btn-primary btn-block">Se connecter <i class="icon-circle-right2 position-right"></i></button>
                                        <!--a  name="signupbtn" id="signupbtn" href="public/users/signup" class="btn bg-blue-800 btn-block">S'inscrire <i class="icon-circle-right2 position-right"></i></a-->
                                    </div>

                                    <div class="text-center">
                                        <a href="">Mot de passe oublié ?</a>
                                    </div>
                                    <div class="text-center">
                                        <span id="status"></span>
                                    </div>
                                    <div class="content-divider text-muted form-group"><span>MEDWAYS international</span></div>
                                    <ul class="list-inline form-group list-inline-condensed text-center">
                                        <img src="assets/images/medways_log.png" alt="MEDWAYS" class="img-responsive">
                                    </ul>
                                </div>
                            </form>
                            <!-- /simple login form -->


                            <!-- Footer -->
                            <div class="footer text-muted text-center">
                                &copy; 2017. <a href="#">Gestions Transit Web App </a> by <a href="https://github.com/orgs/GTeCHSOFT" target="_blank">GTeCH+</a>
                            </div>
                            <!-- /footer -->

                        </div>
                        <!-- /content area -->

                    </div>
                    <!-- /main content -->

                </div>
                <!-- /page content -->

            </div>
            <!-- /page container -->
        </body>

    </html>