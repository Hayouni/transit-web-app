<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur","Commercial","Exploitation"])) {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Recherche Rapport</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css"> 
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/plugin/print/summernote-ext-print.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/plugin/specialchars/summernote-ext-specialchars.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/lang/summernote-fr-FR.js"></script>


    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/rprosp.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->
    <style>
        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }
    </style>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Commercial</span> - Recherche Rapport
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Recherche</h6>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-4 text-center">
                                            <div class="form-group">
                                                <label class="display-block">Commercial : </label>
                                                <select class="border-black border-lg text-black form-control livesearch" id="commercial_sl"></select>
                                            </div>  
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <div class="form-group">
                                                <label class="display-block">Client : </label>
                                                <select class="border-black border-lg text-black form-control livesearch" id="clients_sl"></select>
                                            </div> 
                                        </div>
                                        
                                        <div class="col-sm-3 text-center">
                                            <div class="form-group">
                                                <label class="display-block">Sélectionnez la plage de dates : </label>
                                                <button type="button" class="btn btn-danger daterange-ranges" id="daterange">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-sm-1 text-center">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-danger btn-icon btn-rounded" id="get_tab_btn"><i class="icon-search4"></i></button>
                                        </div>
                                    </div>
                                    <table class="table datatable-button-print-rows" id="table">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Évaluation</th>
                                                <th>Client</th>
                                                <th>Responsable</th>
                                                <th>Date</th>
                                                <th>dernière mise à jour</th>
                                                <th>Propriétaire</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- view modal -->
					<div id="modal_view" class="modal fade">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<div class="modal-body">
                                    <div class="summernote" id="summernote_view"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /view modal -->

                    <!-- edit modal -->
					<div id="modal_edit" class="modal fade">
						<div class="modal-dialog modal-full">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<div class="modal-body">
                                    <div class="summernote" id="summernote_edit"></div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
									<button type="button" id="save_edit" class="btn btn-primary">Sauvegarder les modifications</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /edit modal -->


                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>