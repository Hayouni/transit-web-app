/*
 * @Author: GAYTH BACCARI 
 * @Date: 2018-04-14 12:33:22 
 * @Last Modified by: GAYTH BACCARI
 * @Last Modified time: 2018-04-17 21:45:40
 */
$(document).ready(function () {
    moment.locale('fr');

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }

    $.urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        return results[1] || 0;
    }

    console.log($.urlParam('id'))

    if ($.urlParam('id') === null || $.urlParam('id') === undefined) {
        alert('wrong access');
        window.close();
    }

    var table = $('#table').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    table = $('#table').DataTable({
        select: {
            style: 'single'
        },
        destroy: true,
        scrollY: false, //"400px",
        scrollX: false,
        scrollCollapse: true,
        paging: true,
        ajax: {
            url: "/GTRANS/public/Commercial/api/prosp.php",
            type: "POST",
            data: function (d) {
                return JSON.stringify({
                    "0": "GET_PROSP_HISTORY",
                    "1": $.urlParam('id'),
                });
            },
        },
        aoColumnDefs: [{
            "sClass": "hide_me",
            "aTargets": [0]
        }],
        columns: [{
                "data": "CPH_CLE"
            },
            {
                "data": "CPH_LIBE_PROS",
                "width": "10%",
                "render": function (data, type, row) {
                    switch (row.CPH_LIBE_PROS) {
                        case 'PROSPECTE':
                            return '<span style="color:#1565C0"><i class="fa fa-circle"></i> PROSPECTE</span>';
                        case 'POTENTIEL':
                            return '<span style="color:#C62828"><i class="fa fa-circle"></i> POTENTIEL</span>';
                        case 'FIDEL':
                            return '<span style="color:#2E7D32"><i class="fa fa-circle"></i> FIDEL</span>';
                        case 'VEILLEUSE':
                            return '<span style="color:#D84315"><i class="fa fa-circle"></i> VEILLEUSE</span>';
                        case 'PASSAGE':
                            return '<span style="color:#00838F"><i class="fa fa-circle"></i> PASSAGE</span>';

                        default:
                            return '<span style="color:#37474F"><i class="fa fa-circle"></i> ' + row.CPH_LIBE_PROS + '</span>';
                    }
                }
            },
            {
                "data": "CL_LIBELLE",
                "render": function (data, type, row) {
                    return '<b>' + row.CL_LIBELLE + '</b>';
                }
            },
            {
                "data": "CL_RSPONSABLE"
            },
            {
                "data": "CPH_DATE",
                "render": function (data, type, row) {
                    return '<span class="text-black">' + row.CPH_DATE + '</span>';
                }
            },
            {
                "data": "CPH_UPDATE",
                "render": function (data, type, row) {
                    return '<span class="text-black">' + row.CPH_UPDATE + '</span>';
                }
            },
            {
                "data": "CPH_DATE_OPER",
                "render": function (data, type, row) {
                    return '<span class="text-danger">' + row.CPH_DATE_OPER + '</span>';
                }
            },
            {
                "data": "avatar",
                "width": "15%",
                "render": function (data, type, row) {
                    switch (row.avatar) {
                        case null:
                            return '<img class="img-circle img-md" src="/GTRANS/assets/images/default-img.png" alt="Pic">';

                        default:
                            return '<img class="img-circle img-md" src="/GTRANS/public/users/user_data/' + row.CPH_COMER_ID + '/' + row.avatar + '" alt="Pic"> ' + row.CPH_COMER_NOM;
                    }
                }
            },
            {
                "data": null,
                "width": "10%",
                "defaultContent": '<a id="view" class="btn btn-link text-success"><i class="icon-file-eye2"></i></a>'
            }
        ]
    }).columns.adjust().draw();


    $('#modal_view').on('hidden.bs.modal', function () {
        $('#summernote_view').empty();
    });

    $('#table tbody').on('click', 'a#view', function () {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['VIEW_LINE_HISTORY', data.CPH_CLE])).done(function (data) {
            //$('#summernote_view').summernote('destroy');
            $('#summernote_view').html(data[0].CPH_CONTENU1);
            $('#modal_view').modal('show');
        });
    });




});