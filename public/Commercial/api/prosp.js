/*
 * @Author: GAYTH BACCARI 
 * @Date: 2018-04-18 22:03:31 
 * @Last Modified by:   GAYTH BACCARI 
 * @Last Modified time: 2018-04-18 22:03:31 
 */
$(document).ready(function () {

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }

    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function (data) {
        console.log('fail', data);
    }).done(function (data) {
        data.forEach(function (element) {
            $('#clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    }).always(function () {
        $(".livesearch").chosen({
            width: '100%',
            enable_split_word_search: false
        });
    });

    $('#summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'hr']],
            ['view', ['fullscreen']],
            ['misc', ['print']]
        ]
    });

    $('.daterange-single').daterangepicker({
        applyClass: 'bg-slate-600',
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY-MM-DD H:mm:ss'
        },
        cancelClass: 'btn-default'
    });

    function get_client_info(params) {
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['GET_CLIENT_INFO', params])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            $('#CL_CODE').val(data[0].CL_CODE);
            $('#CL_ADRESSE').val(data[0].CL_ADRESSE);
            $('#CL_RSPONSABLE').val(data[0].CL_RSPONSABLE);
            $('#CL_TEL_RESPONSABLE').val(data[0].CL_TEL_RESPONSABLE);
            $('#CL_TEL_ETABLISS').val(data[0].CL_TEL_ETABLISS);
            $('#CL_FAX').val(data[0].CL_FAX);
            $('#CL_EMAIL').val(data[0].CL_EMAIL);
            $('#CL_AUTRE_INFO').val(data[0].CL_AUTRE_INFO);
        });
    }

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        //dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "Chargement...",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    var table = $('#table').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        },
        select: {
            style: 'single'
        },
        destroy: true,
        scrollY: false, //"400px",
        scrollX: false,
        scrollCollapse: true,
        paging: true,
        ajax: {
            url: "/GTRANS/public/Commercial/api/prosp.php",
            type: "POST",
            data: function (d) {
                return JSON.stringify({
                    "0": "GET_PROSP_USER"
                });
            },
        },
        //aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
        columns: [{
                "data": "CP_CLE",
                "width": "10%"
            },
            {
                "data": "CP_LIBE_PROS",
                "width": "10%",
                "render": function (data, type, row) {
                    switch (row.CP_LIBE_PROS) {
                        case 'PROSPECTE':
                            return '<span style="color:#1565C0"><i class="fa fa-circle"></i> PROSPECTE</span>';
                        case 'POTENTIEL':
                            return '<span style="color:#C62828"><i class="fa fa-circle"></i> POTENTIEL</span>';
                        case 'FIDEL':
                            return '<span style="color:#2E7D32"><i class="fa fa-circle"></i> FIDEL</span>';
                        case 'VEILLEUSE':
                            return '<span style="color:#D84315"><i class="fa fa-circle"></i> VEILLEUSE</span>';
                        case 'PASSAGE':
                            return '<span style="color:#00838F"><i class="fa fa-circle"></i> PASSAGE</span>';

                        default:
                            return '<span style="color:#37474F"><i class="fa fa-circle"></i> ' + row.CP_LIBE_PROS + '</span>';
                    }
                }
            },
            {
                "data": "CL_LIBELLE",
                "render": function (data, type, row) {
                    return '<b>' + row.CL_LIBELLE + '</b>';
                }
            },
            {
                "data": "CL_RSPONSABLE"
            },
            {
                "data": "CP_DATE",
                "render": function (data, type, row) {
                    return '<span class="text-black">' + row.CP_DATE + '</span>';
                }
            },
            {
                "data": "CP_UPDATE",
                "render": function (data, type, row) {
                    return '<span class="text-black">' + row.CP_UPDATE + '</span>';
                }
            },
            {
                "data": "avatar",
                "width": "15%",
                "render": function (data, type, row) {
                    switch (row.avatar) {
                        case null:
                            return '<img class="img-circle img-md" src="/GTRANS/assets/images/default-img.png" alt="Pic">';

                        default:
                            return '<img class="img-circle img-md" src="/GTRANS/public/users/user_data/' + row.CP_COMER_ID + '/' + row.avatar + '" alt="Pic"> ' + row.CP_COMER_NOM;
                    }
                }
            },
            {
                "data": null,
                "width": "10%",
                "render": function (data, type, row) {
                    if (Number(row.MCOUNT) > 0) {
                        return '<a id="view" class="btn btn-link text-success"><i class="icon-file-eye2"></i></a>' +
                            '<a id="edit" class="btn btn-link text-primary"><i class="icon-file-plus2"></i></a>' +
                            '<a id="del" class="btn btn-link text-danger"><i class="icon-file-minus2"></i></a>' +
                            '<a id="hist" class="btn btn-link text-warning-400"><i class="icon-profile"></i><span class="badge bg-warning-400">' + row.MCOUNT + '</span></a>';
                    } else {
                        return '<a id="view" class="btn btn-link text-success"><i class="icon-file-eye2"></i></a>' +
                            '<a id="edit" class="btn btn-link text-primary"><i class="icon-file-plus2"></i></a>' +
                            '<a id="del" class="btn btn-link text-danger"><i class="icon-file-minus2"></i></a>' +
                            '<a id="hist" class="btn btn-link text-teal-400"><i class="icon-profile"></i><span class="badge bg-teal-400">' + row.MCOUNT + '</span></a>';
                    }
                }
            }
        ]
    }).columns.adjust().draw();

    $('#clients').on('change', function (e) {
        var selected = e.target.value;
        get_client_info(selected);
        $('#clname').html(selected);
    });

    $('#save_pros').on('click', function () {
        var html = $('#summernote').summernote('code');
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify([
            'ADD_NEW', {
                'CP_CL_CODE': $('#clients').val(),
                'CP_DATE': $('.daterange-single').val(),
                'CP_CONTENU1': html,
                'CP_CODE_PROS': $('#prosp_sel').val(),
                'CP_LIBE_PROS': $('#prosp_sel option:selected').text()
            }
        ])).done(function (data) {
            PNotify_Alert('Prospection', 'Ajouté avec succès', 'success');
            table.ajax.reload();
        }).fail(function (error) {
            console.log('fail', error);
        });
    });

    $('#modal_view').on('hidden.bs.modal', function () {
        $('#summernote_view').empty();
    });

    $('#table tbody').on('click', 'a#hist', function () {
        var data = table.row($(this).parents('tr')).data();
        var win = window.open('/GTRANS/public/Commercial/history?id=' + data.CP_CLE, '_blank');
        win.focus();
    });

    $('#table tbody').on('click', 'a#view', function () {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['VIEW_LINE', data.CP_CLE])).done(function (data) {
            //$('#summernote_view').summernote('destroy');
            $('#summernote_view').html(data[0].CP_CONTENU1);
            $('#modal_view').modal('show');
        });
    });

    $('#table tbody').on('click', 'a#edit', function () {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['VIEW_LINE', data.CP_CLE])).done(function (data) {
            $('#summernote_edit').summernote('destroy');
            $('#summernote_edit').html(data[0].CP_CONTENU1);
            $('#summernote_edit').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'hr']],
                    ['view', ['fullscreen']],
                    ['misc', ['print']]
                ]
            });
            $('#modal_edit').modal('show');
        });
    });

    $('#save_edit').on('click', function () {
        var dataArr;
        $.each($("#table tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        var html = $('#summernote_edit').summernote('code');
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['UPDATE_LINE', {
            'CP_CLE': dataArr
        }, {
            'CP_CONTENU1': html
        }, dataArr])).done(function (data) {
            PNotify_Alert('Prospection', 'Modifié avec succès', 'success');
        });
    });

    $('#table tbody').on('click', 'a#del', function () {
        var data = table.row($(this).parents('tr')).data();
        var notice = new PNotify({
            title: 'Confirmation de suppression',
            text: '<p>êtes-vous sûr de supprimer cette prospection?</p>',
            addclass: 'alert bg-danger alert-styled-left',
            hide: false,
            confirm: {
                confirm: true,
                buttons: [{
                        text: 'Oui',
                        addClass: 'btn btn-sm btn-primary'
                    },
                    {
                        text: 'Non',
                        addClass: 'btn btn-sm btn-link'
                    }
                ]
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        })

        // On confirm
        notice.get().on('pnotify.confirm', function () {
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DEL_LINE', {
                'CP_CLE': data.CP_CLE
            }, data.CP_CLE])).done(function (data) {
                PNotify_Alert('Prospection', 'Supprimé avec succès', 'success');
                $('#table').DataTable().ajax.reload();
            });
        })

    });

    $('#apply_cl_mod').on('click', function () {
        if ($('#CL_CODE').val()) {
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify([
                'MOD_CLIENT_INFO', {
                    'CL_ADRESSE': $('#CL_ADRESSE').val(),
                    'CL_RSPONSABLE': $('#CL_RSPONSABLE').val(),
                    'CL_TEL_RESPONSABLE': $('#CL_TEL_RESPONSABLE').val(),
                    'CL_TEL_ETABLISS': $('#CL_TEL_ETABLISS').val(),
                    'CL_FAX': $('#CL_FAX').val(),
                    'CL_EMAIL': $('#CL_EMAIL').val(),
                    'CL_AUTRE_INFO': $('#CL_AUTRE_INFO').val()
                }, {
                    'CL_CODE': $('#clients').val()
                }
            ])).done(function (data) {
                PNotify_Alert('Client', 'Modifié avec succès', 'success');
            }).fail(function (error) {
                PNotify_Alert('Client', 'modification échouée', 'warning');
                console.log('fail', error);
            });
        }
    });
});