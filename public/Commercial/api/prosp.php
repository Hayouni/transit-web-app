<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json");
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false) {
    $jqxdata = explode('&', $str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=', $value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_CLIENT_INFO':
        $db = new MySQL();
        $GET_CLIENT_INFO = $db->get_results("SELECT * from trans.client WHERE CL_CODE = '$response[1]'");
        echo json_encode($GET_CLIENT_INFO);
        break;

    case 'ADD_NEW':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(CP_CLE) + 1 as k FROM client_prospection")[0]['k'];
        $response[1]['CP_CLE'] = $key;
        $response[1]['CP_COMER_ID'] = $_SESSION['username'];
        $response[1]['CP_COMER_NOM'] = $_SESSION['full_name'];
        $date = new DateTime();
        $response[1]['CP_UPDATE'] = $date->format('Y-m-d H:i:s');
        echo $db->insert('client_prospection', $response[1]);
        break;

    case 'GET_PROSP':
        $db = new MySQL();
        $GET_PROSP["data"] = $db->get_results("SELECT
                                                    CP_CLE,
                                                    CL_LIBELLE,
                                                    CL_RSPONSABLE,
                                                    CP_LIBE_PROS,
                                                    CP_COMER_ID,
                                                    avatar,
                                                    CP_COMER_NOM,
                                                    CP_DATE,
                                                    CP_UPDATE
                                                FROM
                                                    client_prospection
                                                    INNER JOIN client ON CP_CL_CODE = CL_CODE 
                                                    INNER JOIN users ON CP_COMER_ID = username 
                                                WHERE
                                                    (CP_DATE BETWEEN '{$response[1]}' AND '{$response[2]}') {$response[3]}
                                                ORDER BY
	                                                CP_DATE DESC");
        echo json_encode($GET_PROSP);
        break;

    case 'GET_PROSP_USER':
        $db = new MySQL();
        $GET_PROSP_USER["data"] = $db->get_results("SELECT
                                                    CP_CLE,
                                                    CL_LIBELLE,
                                                    CL_RSPONSABLE,
                                                    CP_LIBE_PROS,
                                                    CP_COMER_ID,
                                                    avatar,
                                                    CP_COMER_NOM,
                                                    CP_DATE,
                                                    CP_UPDATE,
                                                    (SELECT COUNT(*) FROM client_pros_history WHERE CPH_CLE_NODE = CP_CLE) AS MCOUNT
                                                FROM
                                                    client_prospection
                                                    INNER JOIN client ON CP_CL_CODE = CL_CODE 
                                                    INNER JOIN users ON CP_COMER_ID = username 
                                                WHERE
                                                    CP_COMER_ID = '" . $_SESSION["username"] . "'
                                                ORDER BY
	                                                CP_DATE DESC");
        echo json_encode($GET_PROSP_USER);
        break;

    case 'VIEW_LINE':
        $db = new MySQL();
        $VIEW_LINE = $db->get_results("SELECT CP_CONTENU1 FROM client_prospection WHERE CP_CLE = '$response[1]'");
        echo json_encode($VIEW_LINE);
        break;

    case 'UPDATE_LINE':
        $db = new MySQL();
        $date = new DateTime();
        $old = $db->get_results("SELECT * FROM client_prospection WHERE CP_CLE = '{$response[3]}'");
        $hist = array(
            'CPH_CLE' => '', 'CPH_CLE_NODE' => $response[3], 'CPH_CL_CODE' => $old[0]['CP_CL_CODE'], 'CPH_CODE_PROS' => $old[0]['CP_CODE_PROS'],
            'CPH_LIBE_PROS' => $old[0]['CP_LIBE_PROS'], 'CPH_CONTENU1' => $old[0]['CP_CONTENU1'], 'CPH_COMER_ID' => $old[0]['CP_COMER_ID'], 'CPH_COMER_NOM' => $old[0]['CP_COMER_NOM'],
            'CPH_DATE' => $old[0]['CP_DATE'], 'CPH_UPDATE' => $old[0]['CP_UPDATE'], 'CPH_DATE_OPER' => $date->format('Y-m-d H:i:s'), 'CPH_TYPE' => 'M'
        );
        $rsin = $db->insert('client_pros_history', $hist);
        if ($rsin) {
            $response[2]['CP_UPDATE'] = $date->format('Y-m-d H:i:s');
            echo $db->update('client_prospection', $response[2], $response[1], 1);
        }
        break;

    case 'DEL_LINE':
        $db = new MySQL();
        $date = new DateTime();
        $old = $db->get_results("SELECT * FROM client_prospection WHERE CP_CLE = '{$response[2]}'");
        $hist = array(
            'CPH_CLE' => '', 'CPH_CLE_NODE' => $response[2], 'CPH_CL_CODE' => $old[0]['CP_CL_CODE'], 'CPH_CODE_PROS' => $old[0]['CP_CODE_PROS'],
            'CPH_LIBE_PROS' => $old[0]['CP_LIBE_PROS'], 'CPH_CONTENU1' => $old[0]['CP_CONTENU1'], 'CPH_COMER_ID' => $old[0]['CP_COMER_ID'], 'CPH_COMER_NOM' => $old[0]['CP_COMER_NOM'],
            'CPH_DATE' => $old[0]['CP_DATE'], 'CPH_UPDATE' => $old[0]['CP_UPDATE'], 'CPH_DATE_OPER' => $date->format('Y-m-d H:i:s'), 'CPH_TYPE' => 'S'
        );
        $rsin = $db->insert('client_pros_history', $hist);
        if ($rsin) {
            echo $db->delete('client_prospection', $response[1], 1);
        }
        break;

    case 'GET_DATE_TIME':
        $date = date('d/m/Y');
        $time = date('H:i');
        $res = array('date' => $date, 'time' => $time);
        echo json_encode($res);
        break;

    case 'MOD_CLIENT_INFO':
        $db = new MySQL();
        echo $db->update('client', $response[1], $response[2]);
        break;

    case 'GET_USERS':
        $db = new MySQL();
        $GET_USERS = $db->get_results("SELECT username, CONCAT(name,' ',subname) AS Nom FROM users WHERE username NOT IN ('admin')"); //userlevel = 'Commercial'
        echo json_encode($GET_USERS);
        break;

    case 'GET_PROSP_HISTORY':
        $db = new MySQL();
        $GET_PROSP_HISTORY["data"] = $db->get_results("SELECT
                                                    CPH_CLE,
                                                    CL_LIBELLE,
                                                    CL_RSPONSABLE,
                                                    CPH_LIBE_PROS,
                                                    CPH_COMER_ID,
                                                    avatar,
                                                    CPH_COMER_NOM,
                                                    CPH_DATE,
                                                    CPH_UPDATE,
                                                    CPH_DATE_OPER
                                                FROM
                                                    client_pros_history
                                                    INNER JOIN client ON CPH_CL_CODE = CL_CODE 
                                                    INNER JOIN users ON CPH_COMER_ID = username 
                                                WHERE
                                                    CPH_CLE_NODE = '{$response[1]}'
                                                ORDER BY
	                                                CPH_DATE DESC");
        echo json_encode($GET_PROSP_HISTORY);
        break;

    case 'VIEW_LINE_HISTORY':
        $db = new MySQL();
        $VIEW_LINE = $db->get_results("SELECT CPH_CONTENU1 FROM client_pros_history WHERE CPH_CLE = '$response[1]'");
        echo json_encode($VIEW_LINE);
        break;

    //Draft

    case 'GET_DRAFT_CONNECTED':
        $db = new MySQL();
        $level = $db->get_results("SELECT userlevel FROM users WHERE username = '" . $_SESSION['username'] . "'");
        $inc = "";
        $SQL = "SELECT
                    DP_ID,
                    CONCAT(name,' ', subname) AS USRFULLNAME,
                    CL_LIBELLE,
                    CL_RSPONSABLE,
                    CL_TEL_RESPONSABLE,
                    CL_EMAIL,
                    CL_ADRESSE,
                    DP_COMMENT,
                    DATE_FORMAT(DP_DATE, '%d/%m/%Y %H:%i:%s') AS DP_DATE,
                    DP_DONE
                FROM draft_prospection
                    INNER JOIN users
                    ON DP_USER = username
                    INNER JOIN client
                    ON DP_CLIENT = CL_CODE
                    %INC%
                ORDER BY DP_DATE ASC";
        if ($level[0]['userlevel'] === 'Administrateur') {
            switch ($response[1]) {
                case 'ALL':
                    $SQL = str_replace("%INC%", "WHERE (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;

                case 'DONE':
                    $SQL = str_replace("%INC%", "WHERE DP_DONE = TRUE AND (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;

                default:
                    $SQL = str_replace("%INC%", "WHERE DP_DONE = FALSE AND (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;
            }
        } else {
            switch ($response[1]) {
                case 'ALL':
                    $SQL = str_replace("%INC%", "WHERE DP_USER = {$_SESSION['username']} AND (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;

                case 'DONE':
                    $SQL = str_replace("%INC%", "WHERE DP_USER = {$_SESSION['username']} AND DP_DONE = TRUE AND (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;

                default:
                    $SQL = str_replace("%INC%", "WHERE DP_USER = {$_SESSION['username']} AND DP_DONE = FALSE AND (DATE_FORMAT(DP_DATE, '%Y-%m-%d') BETWEEN '{$response[2]}' AND '{$response[3]}' )", $SQL);
                    $GET_DRAFT_CONNECTED["data"] = $db->get_results($SQL);
                    echo json_encode($GET_DRAFT_CONNECTED);
                    break;
            }
        }
        break;

    case 'DRAFT':
        $db = new MySQL();
        switch ($response[1]) {
            case 'DONE':
                $rs = $db->update('draft_prospection', ['DP_DONE' => 1], ['DP_ID' => $response[2]]);
                echo json_encode(['result' => $rs]);
                break;

            case 'ACTIVE':
                $rs = $db->update('draft_prospection', ['DP_DONE' => 0], ['DP_ID' => $response[2]]);
                echo json_encode(['result' => $rs]);
                break;

            case 'DEL':
                $rs = $db->delete('draft_prospection', ['DP_ID' => $response[2]]);
                echo json_encode(['result' => $rs]);
                break;

            case 'ADD':
                $response[2]['DP_USER'] = $_SESSION['username'];
                $rs = $db->insert('draft_prospection', $response[2]);
                echo json_encode(['result' => $rs]);
                break;

            case 'NUMBER_TODAY':
                try {
                    $rs = $db->get_results("SELECT COUNT(*) AS Notfi FROM draft_prospection WHERE DP_USER = '{$_SESSION['username']}' AND DATE_FORMAT(DP_DATE, '%Y-%m-%d') = CURDATE() AND DP_DONE = FALSE");
                    echo json_encode(['DP_NOTIF' => $rs[0]['Notfi']]);
                } catch (Exception $e) {
                    echo json_encode(['DP_NOTIF' => 'Er']);
                }
                break;


            default:
                echo json_encode(['result' => 0]);
                break;
        }
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>