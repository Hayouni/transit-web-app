/*
 * @Author: GAYTH BACCARI 
 * @Date: 2018-04-18 22:11:18 
 * @Last Modified by: GAYTH BACCARI
 * @Last Modified time: 2018-04-22 22:35:40
 */

$(document).ready(function () {

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function (start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD');
    endDate = moment().format('YYYY-MM-DD');

    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function (data) {
        console.log('fail', data);
    }).done(function (data) {
        data.forEach(function (element) {
            $('#clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    }).always(function () {
        $(".livesearch").chosen({
            width: '100%',
            enable_split_word_search: false
        });
    });

    $('.daterange-single').daterangepicker({
        applyClass: 'bg-slate-600',
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        locale: {
            format: 'YYYY-MM-DD H:mm:ss'
        },
        cancelClass: 'btn-default'
    });

    function get_client_info(params) {
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['GET_CLIENT_INFO', params])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            $('#CL_CODE').val(data[0].CL_CODE);
            $('#CL_ADRESSE').val(data[0].CL_ADRESSE);
            $('#CL_RSPONSABLE').val(data[0].CL_RSPONSABLE);
            $('#CL_TEL_RESPONSABLE').val(data[0].CL_TEL_RESPONSABLE);
            $('#CL_TEL_ETABLISS').val(data[0].CL_TEL_ETABLISS);
            $('#CL_FAX').val(data[0].CL_FAX);
            $('#CL_EMAIL').val(data[0].CL_EMAIL);
            $('#CL_AUTRE_INFO').val(data[0].CL_AUTRE_INFO);
        });
    }

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        //dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "Chargement...",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        }
    });

    $('#clients').on('change', function (e) {
        var selected = e.target.value;
        get_client_info(selected);
        $('#clname').html(selected);
    });

    $('#apply_cl_mod').on('click', function () {
        if ($('#CL_CODE').val()) {
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify([
                'MOD_CLIENT_INFO', {
                    'CL_ADRESSE': $('#CL_ADRESSE').val(),
                    'CL_RSPONSABLE': $('#CL_RSPONSABLE').val(),
                    'CL_TEL_RESPONSABLE': $('#CL_TEL_RESPONSABLE').val(),
                    'CL_TEL_ETABLISS': $('#CL_TEL_ETABLISS').val(),
                    'CL_FAX': $('#CL_FAX').val(),
                    'CL_EMAIL': $('#CL_EMAIL').val(),
                    'CL_AUTRE_INFO': $('#CL_AUTRE_INFO').val()
                }, {
                    'CL_CODE': $('#clients').val()
                }
            ])).done(function (data) {
                PNotify_Alert('Client', 'Modifié avec succès', 'success');
            }).fail(function (error) {
                PNotify_Alert('Client', 'modification échouée', 'warning');
                console.log('fail', error);
            });
        }
    });

    var table = $('#table').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#actif_btn').on('click', function () {
        create_table('NOT_YET');
    });

    $('#actif_btn').click();

    $('#done_btn').on('click', function () {
        create_table('DONE');
    });

    $('#all_btn').on('click', function () {
        create_table('ALL');
    });

    function create_table(params) {
        table = $('#table').DataTable({
            select: {
                style: 'single'
            },
            destroy: true,
            scrollY: false, //"400px",
            scrollX: 'auto',
            scrollCollapse: true,
            paging: false,
            ajax: {
                url: "/GTRANS/public/Commercial/api/prosp.php",
                type: "POST",
                data: function (d) {
                    return JSON.stringify({
                        "0": "GET_DRAFT_CONNECTED",
                        "1": params,
                        "2": startDate,
                        "3": endDate
                    });
                },
            },
            aoColumnDefs: [
                /*{
                                "sClass": "hide_me",
                                "aTargets": [0]
                            },*/
                {
                    "sClass": "text-center",
                    "aTargets": [9]
                }, {
                    "type": "date",
                    "aTargets": [8]
                    //"targets": 8
                }
            ],
            columns: [{
                "data": "DP_ID",
                "render": function (data, type, row) {
                    switch (row.DP_DONE) {
                        case '0':
                            return '<span class="label label-success">Actif</span>';
                        case '1':
                            return '<span class="label label-danger">Terminé</span>';
                    }
                }
            }, {
                "data": "USRFULLNAME"
            }, {
                "data": "CL_LIBELLE",
                "render": function (data, type, row) {
                    return '<b>' + row.CL_LIBELLE + '</b>';
                }
            }, {
                "data": "CL_RSPONSABLE"
            }, {
                "data": "CL_TEL_RESPONSABLE"
            }, {
                "data": "CL_EMAIL",
                "render": function (data, type, row) {
                    return '<a>' + ((row.CL_EMAIL !== null) ? row.CL_EMAIL : '_') + '</a>';
                }
            }, {
                "data": "CL_ADRESSE"
            }, {
                "data": "DP_COMMENT",
                "render": function (data, type, row) {
                    return '<b>' + row.DP_COMMENT + '</b>';
                }
            }, {
                "data": "DP_DATE"
            }, {
                "data": null,
                "width": "10%",
                "defaultContent": '<ul class="icons-list">\
                            <li class="dropdown">\
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">\
                                    <i class="icon-menu9"></i>\
                                </a>\
                                <ul class="dropdown-menu dropdown-menu-right">\
                                    <li><a id="donebtn"><i class="icon-checkmark2"></i> Définir terminé</a></li>\
                                    <li><a id="activebtn"><i class="icon-circle-small"></i> Définir pas fait</a></li>\
                                    <li><a id="delbtn"><i class="icon-cross3"></i> Effacer</a></li>\
                                </ul>\
                            </li>\
                        </ul>'
            }]
        }).columns.adjust().draw();

        $('#table tbody').on('click', '#donebtn', function () {
            var data = table.row($(this).parents('tr')).data();
            var id = data.DP_ID;
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DRAFT', 'DONE', id])).done(function (data) {
                PNotify_Alert('Brouillon', 'Operation complétée avec succès', 'success');
                table.ajax.reload();
            }).fail(function (error) {
                console.log('fail', error);
            });
        });

        $('#table tbody').on('click', '#activebtn', function () {
            var data = table.row($(this).parents('tr')).data();
            var id = data.DP_ID;
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DRAFT', 'ACTIVE', id])).done(function (data) {
                PNotify_Alert('Brouillon', 'Operation complétée avec succès', 'success');
                table.ajax.reload();
            }).fail(function (error) {
                console.log('fail', error);
            });
        });

        $('#table tbody').on('click', '#delbtn', function () {
            var data = table.row($(this).parents('tr')).data();
            var id = data.DP_ID;
            $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DRAFT', 'DEL', id])).done(function (data) {
                PNotify_Alert('Brouillon', 'Operation complétée avec succès', 'success');
                table.ajax.reload();
            }).fail(function (error) {
                console.log('fail', error);
            });
        });
    }

    $('#add_darft_btn').on('click', function () {
        if ($('#clients').val()) {
            if ($('#comment').val()) {
                let data = {
                    'DP_CLIENT': $('#clients').val(),
                    'DP_COMMENT': $('#comment').val(),
                    'DP_DATE': $('#datedraft').val(),
                    'DP_DONE': 0
                }
                $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DRAFT', 'ADD', data])).done(function (data) {
                    PNotify_Alert('Brouillon', 'Operation complétée avec succès', 'success');
                    try {
                        table.ajax.reload();
                    } catch (error) {
                        console.log('error', error);
                    }
                }).fail(function (error) {
                    console.log('fail', error);
                });
            }
        }
    });

});