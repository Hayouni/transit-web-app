$(document).ready(function () {
    moment.locale('fr');

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function (start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['GET_USERS'])).fail(function (data) {
        console.log('fail', data);
    }).done(function (data) {
        $('#commercial_sl').append('<option value="ALL">Tous</option>');
        data.forEach(function (element) {
            $('#commercial_sl').append('<option value="' + element.username + '">' + element.Nom + '</option>');
        }, this);
    }).always(function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            $('#clients_sl').append('<option value="ALL">Tous</option>');
            data.forEach(function (element) {
                $('#clients_sl').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
            }, this);
        }).always(function () {
            $(".livesearch").chosen({
                width: '100%',
                enable_split_word_search: false
            });
        });
    });

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        //dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "Chargement...",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });
    var table = $('#table').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#get_tab_btn').on('click', function () {
        var rsp3 = '';
        if ($('#commercial_sl').val() !== 'ALL') {
            rsp3 = ' AND CP_COMER_ID = ' + $('#commercial_sl').val();
        }
        if ($('#clients_sl').val() !== 'ALL') {
            rsp3 = ' AND CP_CL_CODE = ' + $('#clients_sl').val();
        }
        table = $('#table').DataTable({
            select: {
                style: 'single'
            },
            destroy: true,
            scrollY: false, //"400px",
            scrollX: false,
            scrollCollapse: true,
            paging: true,
            ajax: {
                url: "/GTRANS/public/Commercial/api/prosp.php",
                type: "POST",
                data: function (d) {
                    return JSON.stringify({
                        "0": "GET_PROSP",
                        "1": startDate,
                        "2": endDate,
                        "3": rsp3
                    });
                },
            },
            //aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
            columns: [{
                    "data": "CP_CLE",
                    "width": "10%"
                },
                {
                    "data": "CP_LIBE_PROS",
                    "width": "10%",
                    "render": function (data, type, row) {
                        switch (row.CP_LIBE_PROS) {
                            case 'PROSPECTE':
                                return '<span style="color:#1565C0"><i class="fa fa-circle"></i> PROSPECTE</span>';
                            case 'POTENTIEL':
                                return '<span style="color:#C62828"><i class="fa fa-circle"></i> POTENTIEL</span>';
                            case 'FIDEL':
                                return '<span style="color:#2E7D32"><i class="fa fa-circle"></i> FIDEL</span>';
                            case 'VEILLEUSE':
                                return '<span style="color:#D84315"><i class="fa fa-circle"></i> VEILLEUSE</span>';
                            case 'PASSAGE':
                                return '<span style="color:#00838F"><i class="fa fa-circle"></i> PASSAGE</span>';

                            default:
                                return '<span style="color:#37474F"><i class="fa fa-circle"></i> ' + row.CP_LIBE_PROS + '</span>';
                        }
                    }
                },
                {
                    "data": "CL_LIBELLE",
                    "render": function (data, type, row) {
                        return '<b>' + row.CL_LIBELLE + '</b>';
                    }
                },
                {
                    "data": "CL_RSPONSABLE"
                },
                {
                    "data": "CP_DATE",
                    "render": function (data, type, row) {
                        return '<span class="text-black">' + row.CP_DATE + '</span>';
                    }
                },
                {
                    "data": "CP_UPDATE",
                    "render": function (data, type, row) {
                        return '<span class="text-black">' + row.CP_UPDATE + '</span>';
                    }
                },
                {
                    "data": "avatar",
                    "width": "15%",
                    "render": function (data, type, row) {
                        switch (row.avatar) {
                            case null:
                                return '<img class="img-circle img-md" src="/GTRANS/assets/images/default-img.png" alt="Pic">';

                            default:
                                return '<img class="img-circle img-md" src="/GTRANS/public/users/user_data/' + row.CP_COMER_ID + '/' + row.avatar + '" alt="Pic"> ' + row.CP_COMER_NOM;
                        }
                    }
                },
                {
                    "data": null,
                    "width": "10%",
                    "defaultContent": '<a id="view" class="btn btn-link text-success"><i class="icon-file-eye2"></i></a>'
                }
            ]
        }).columns.adjust().draw();
    });

    $('#daterange').on('apply.daterangepicker', function (ev, picker) {
        var startDate = picker.startDate.format('YYYY-MM-DD');
        var endDate = picker.endDate.format('YYYY-MM-DD');

    });

    $('#modal_view').on('hidden.bs.modal', function () {
        $('#summernote_view').empty();
    });

    $('#table tbody').on('click', 'a#view', function () {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['VIEW_LINE', data.CP_CLE])).done(function (data) {
            //$('#summernote_view').summernote('destroy');
            $('#summernote_view').html(data[0].CP_CONTENU1);
            $('#modal_view').modal('show');
        });
    });




});