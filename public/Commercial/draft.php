<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur", "Commercial", "Exploitation"])) {
    header("location: /GTRANS");
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Brouillon</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/draft.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->
    <style>
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
                
        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }  
    </style>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Commercial</span> - Brouillon
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Ajout</h6>
                                </div>
                                <div class="panel-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                <label class="control-label font_text_Bold">Clients</label>
                                                    <select class="border-black border-lg text-black form-control livesearch" id="clients">
                                                        <option value="">-</option>
                                                    </select>
                                                </div>  
                                            </div>
                                            <div class="col-md-1">

                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" class="btn bg-indigo-400 btn-labeled" id="apply_cl_mod"><b><i class="icon-arrow-right16"></i></b> appliquer les modifications</button>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Code</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" readonly disabled id="CL_CODE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Responsable</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_RSPONSABLE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Tel Responsable</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_TEL_RESPONSABLE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Tel Etablissement</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_TEL_ETABLISS" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Fax</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_FAX" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Email</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_EMAIL" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Adresse</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_ADRESSE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Autre information</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_AUTRE_INFO" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label>Date : </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                                        <input type="text" id="datedraft" class="form-control border-black border-lg text-black daterange-single" value="<?= date("Y-m-d H:i:s"); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group text-center">
                                                    <button type="button" id="add_darft_btn" class="btn bg-green-600 btn-rounded"><i class="icon-file-check2 position-left"></i> Ajouter</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 text-center">
                                            <div class="form-group">
                                                <label class="display-block">Commentaire </label>
                                                <textarea rows="5" cols="5" id="comment" class="form-control border-black border-lg text-black" placeholder="..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="btn-group btn-group-justified">
                                        <div class="btn-group">
                                                <button type="button" class="btn bg-indigo-700" id="actif_btn"><i class="icon-hour-glass2 position-left"></i> Afficher uniquement les éléments actifs</button>
                                            </div>

                                            <div class="btn-group">
                                                <button type="button" class="btn bg-teal-700" id="done_btn"><i class="icon-checkmark4 position-left"></i> Afficher les éléments terminés uniquement</button>
                                            </div>

                                            <div class="btn-group">
                                                <button type="button" class="btn bg-brown-700" id="all_btn"><i class="icon-file-text2 position-left"></i> Afficher tous les éléments</button>
                                            </div>

                                            <div class="btn-group">
                                                <button type="button" class="btn btn-danger daterange-ranges" id="daterange">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table datatable-button-print-rows table-bordered" id="table" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Etat</th>
                                                <th>Commercial</th>
                                                <th>Client</th>
                                                <th>Responsable</th>
                                                <th>Téléphone</th>
                                                <th>Email</th>
                                                <th>Adresse</th>
                                                <th>Commentaire</th>
                                                <th>Date</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>