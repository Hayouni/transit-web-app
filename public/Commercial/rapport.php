<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur", "Commercial", "Exploitation"])) {
    header("location: /GTRANS");
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Rapport</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <!-- Theme JS files -->

    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/plugin/print/summernote-ext-print.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/plugin/specialchars/summernote-ext-specialchars.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/lang/summernote-fr-FR.js"></script>
    
    <script type="text/javascript" src="../../assets/js/dateFormat/jquery-dateFormat.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/prosp.js"></script>
    <!-- /theme JS files -->

    <style>
        <style>
        .font_text_Bold_24 {
            font-weight: bold;
            font-size: 24px;
            /*font: normal normal bold 24px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .font_text_Bold {
            font-size: 16px;
            font-weight: bold;
            /* font: normal normal bold 16px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .border_2px {
            border: 2px solid rgba(0, 0, 0, 0.91);
        }
        
        .background_blue {
            font-size: 13px;
            /*color: rgba(255, 255, 255, 1);
            background: #2196f3;*/
            color: rgb(0, 0, 0);
            background: #a9b8c3;
            font-weight: bold;
        }
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
                
        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }    
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Commercial</span> - Rapport
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Informations Client</h6>
                                </div>

                                <div class="panel-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-4">

                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                <label class="control-label font_text_Bold">Clients</label>
                                                    <select class="border-black border-lg text-black form-control livesearch" id="clients">
                                                        <option value="">-</option>
                                                    </select>
                                                </div>  
                                            </div>
                                            <div class="col-md-1">

                                            </div>
                                            <div class="col-md-3">
                                                <button type="button" class="btn bg-indigo-400 btn-labeled" id="apply_cl_mod"><b><i class="icon-arrow-right16"></i></b> appliquer les modifications</button>
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Code</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" readonly disabled id="CL_CODE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Responsable</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_RSPONSABLE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Tel Responsable</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_TEL_RESPONSABLE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Tel Etablissement</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_TEL_ETABLISS" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Fax</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_FAX" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Email</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_EMAIL" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Adresse</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_ADRESSE" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="control-label font_text_Bold">Autre information</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="CL_AUTRE_INFO" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"> Text</h6>
                                </div>
                                <div class="panel-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-flat border-top-primary">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title">Notation du client</h6>
                                                        <div class="heading-elements">
                                                            <form class="heading-form">
                                                                <div class="form-group">
                                                                    <select class="border-black border-lg text-black form-control" id="prosp_sel">
                                                                        <option value="1" style="color:#1565C0">PROSPECTE</option>
                                                                        <option value="2" style="color:#C62828">POTENTIEL</option>
                                                                        <option value="3" style="color:#2E7D32">FIDEL</option>
                                                                        <option value="4" style="color:#D84315">VEILLEUSE</option>
                                                                        <option value="5" style="color:#00838F">PASSAGE</option>
                                                                    </select>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-body">
                                                        <div class="form-group">
                                                            <label>Date : </label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                                                <input type="text" class="form-control daterange-single" value="<?= date("Y-m-d H:i:s"); ?>">
                                                            </div>
                                                        </div>
                                                        <div class="summernote" id="summernote">
                                                            <p style="color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">
                                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKcAAABiCAYAAAA1M8PrAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTZEaa/1AABnEUlEQVR4Xu299Xdc197m2X/IrDXda6bfS7kGMbNlptiJHTtx4jDHSeyYmZlBlmUxMzODLcnMzGwnufe+/XavmfnhmefZp7ZUKpccJxeS7tU/PD5Vx1Kpzt6f86UN5z8B+N/63/pNyuvJn9KXRT14K70Tb+adwsiEVsQndiA+6ShGpvRgTFoPxmX0YFLmMUzO6sXEnB6jV3N6MSX3GF7LPW40LfeE0ayis0bvFJ0zml18Hu8XX8AHJRfxYcl5fFZ8Dl+WnDX6qvQs5pSdwdcVp6mT+KbyJObVnqCO4bv6Xsxv6MWCph4sbO42WtTUjeWNx7Cy4QRWN5zEqsaTWNN4CmuaT2Nd8xmsbTmDLZ2XsLnrErZ2XcbWo1ew7QjVfZWX6f3af23tbr+MHe2XsJ3a0n4Rm1rPY2PbeaxvO4e11Oq2s1jVegbLW09jGbWo+aTRQl73ArbB/PpT+K6O7VZzBnOrT2NO+Smjr0pPm/ZVO39RfAafF53Gp9T7xSfwXskxvFt8nOrF7KJjeKewG28X9FBHMSu/G2/ldeHN3COYkd2ON7I6qDZznJHZhrkN57Gu5za/uvfreZG8nhxMc8p6MCWpDmHbSxG2txFxmWcQnnUeodmXEJJzFaH51xFRcB2RRdcQU3QdscXXEVdyEyNKbxnFl93GqLI7GF1+F2Mr7mF8+T1MqX6MqTVP8Hr1E0yreYrptc8wo+57zKz/AW/y+F79Y7xf/wgfNDzGh42P8FHzfXzccg+ftN2hbuGT9uv4tOMaPiNcnx+5jC+6L+HLnotG3/L1ws5rWML/X9ZxHcu6bmBF102sOHoLq47exsru21h//AHWn3yIDSeoU494md6v/beqtb33sLr3LlYeu4vlvXewtPc2lvTcwsKem1jA4zfUV3z9Ja/5iyM38VnnTbbXDdN2H7fepO4afdR8Fx823cMHjffxfsM9vFd3H+/W32U/3MYb9TeoW5hedx2v10hX8Vr1NUytutx3fLXiMiaVnccEGpTxhHscDc644pN8fRSjc+owJa8e37ae4Ff2fh3e5PWkp1Z03cLUpEaMPlCL8WndGJtzFiNyryC28CZiyx4isuwRIsofI7T8EcJKHyC09C5Ci+9QtxBeft8ogv8XWfkQ0ZWPEEsQ46qeYGTVU4yufoYxNd9jfM0PmFD7IybV/gWT6/6KV+v/htfqfmDjfI8365/irYYnmNX4FG83P8Y7LY/wXutDvN9GSI8+wqfdD/Fpz118Tn15/C6+OnGPuoNvj9/Hop77WNb9CMt7HmNF7xN24lOsOv4Eq098Tz01WnvyGdad+h7rT//Ay/XeBr81bTj5vfnOa/jdV596hhV8v4yvF598ikUnnmDB8ceYf+Ix5p58gq95bs7xZ/iS1/9ZzxPTZh938UbvfIz32h/j3bZHmN3+BO+0PsWs5id4q+mx0RtNjzC1/h4mNdzBZII6sf4Oxtfewrjq2xhbdatPY6pvYVTlLYysuE7ddB2v89w1Hs8igpY1OLkGUcmVeLusg1/f+zV5yutJd72R3I6JaScwIe003fU5jM68hJHZBDPnBqJyCV/OLYRKebcRln+LlvMmIgtvIJqKouWMLOP7iluIrriLmMo7iK+6h5HV9zG65gHG1TwklA+MJtbcJ5QP2RiP8HrjE0wniDNoLd9pfIzZPL7Lc+81PcEH1IcE9GMC+nHLA3zSdB+fNt/DZ9QX1Je0Al8R2jnt9/Ft210san+AJXy/lO+XdTzAio6HWNlFdT7CqiOPsKb7CdYdfYIN7LSNvU95yd7b4beijbyp1p/gzXScYFK62aTl/O5Lux9jEW/EhUcfYsGRB5jP65x/5An1DHO7nuKbjseYQ33R9hift/KmbnuCj9iO7wvO1sd4m+febiGcLY/xZvMjzOD/vdFwF9Ma72I6IX2t7h6m1t7Hq7UPMLn6IfvsESbwOK7qAcZUPcToynsYQW8YV36bRuuW8ZpxRTdoxK4jvoQQl97AiMyTmMiQcFnbZV6O92u08nrSamEFzXNSD+JSziIm5QJiUi8iMu0qIjOuISzzBkKzKBecYXk3EZ5PIPNvEky69QK59muILuO5clpYAhpXeZvWkq69+g7G1tC1UxNq7hhNrrmNKXV3TQO8Xn+fjXIfMxse4O2Gh3ivgZaSep+gGtfOu/pTNt5nvLOlz3nuq6aH+Jqvv2l5iHls1O/Y2N+xsRfTMixtf0gwHxkt73SB2fXYaH33UyPB6Xn9v0XJYgpOgSmtMoDSchLO5byGpbyWxUcIKS3joq4nWNz5DAs7nmFB+1PMo3X8tu0pvmLbfEEIP299Ym7yD9hO7xFIwTmLN34fmAyhZhBOufQ32C/T1Dc0IlOqHxFOit5vAr3fOB7HVD7G6IpHhPMB4sruI7b0DmJK7mIUPWpMwQNEFvBc0QNEZF1FdMoxvEeXv6XrCi/J+3VKXk9K7+8rxoxDDRh5uBuRqYwtU88jjApJv4LgjCsIyryOwKzrCMq7hUAqiBYzmNYylK4+rPgaIghmZAkly0nFEFDBGU/rOar6Xh+c4+kiHDjv8o507k4HzgeE86GB8536h7Sej2g9H9NyOnCqUQXo52zML/j+S77/mg38DRt4Li2DOmIewVzADlJHqcOW0ELKuiyj1JHSanastIbuXR3v2Q6/Jen7KQTRdxWY5ruf+MEAuoKvHev51FznwqOua/eA8xsCaeH8jO01GJxvND80cL5BVy44p9FwvF5L48G+exk4Y5hbxJTcNudjCu/Sq95FROE9RJKVKHrhCWldeD+rCXvaj/PSvF+v15PSq5tzMPUwM/H0E4jMPI9wuvOwnMtMfK4jJPeGgTKAblzyK7gFf0IZUHwDwcU3EVJ6DaEEM4zHCJpyC6esp+AcWXWXcYpjPcdVO4BO4nvBOZUNYOGU3mp0AUrrKL1LCylAP2TjfUTp7ldDf8kGn0M39TWBnMtOkOZ1Uuyo7wjjAnbWQrrwRT20LMcIKV2htIodLq0+9QPWnGYcx7hz/Zkf2QTe2+XXlDucAnMV40hPOJf1POuDc+GRpwTzKeYT0O94/JY3reDUjayb+hO2mdrxfXqbdwnk23z9FttWcM5kO89kyDTdA84pNffwKl34JGpC1WP2XT+cI5lPeMIZX8ZwrvA2wgsIZ8kDRJc8RCy96ujME5ia2ozPMip5ad6v1+vJj5Jq8WZqGyakdCE26xQici8inAlQqET3LSsZVHgH/kx6fAtvYTjlQyh9S24ggHFGYNk1BAlQHsPp1i2g0RW3Xa6dcDL2NIBWMcjme8Ep66mY5rW6B27W8wHedEE6iw03m42mhlSDqmE/IZCKnRRHfcWjYqpvOmghJFqObwnkXAIpzSOU8wnkAnbqouNMHKilhHIpoVxBKFcSylVnCOpZgnruR6w7+9uxpEp+DJgui2nduTkaOH8gnN9jcff3WHSU1pJgynLO73xqwHRc+mPjXb4gnNZqeoNTYM5ofEDJld/GtPqbBPMOXqOHexk4Y5kAWzhNiMdjZOl9kyyHlTIpLrmP+MIrGJ/ZjWlJ1Vhc1MxLfP6anzuxtvIYRqzJxNuFZxG6r43x5RmEZJ0z5aJgKjD3OgLyrhtracEcVtQPp1+pA6cUXC7reb0PzqhyJ+4cQSAtoILTAErrKTh18RZOyWkkAko41XCz2JBqxHeZDL3X/AAfssE/pgX4lEG9AP2ynfEnAf2aVnMO9W33M3xN9/0Nrcq8XloQAikJ0AXM2hez0y2gy8784IKUHU6tOf2UGfFjNsvANvpXy2bmFk5ZTIFpYk2XDJi8Ab2CScmTzGEbyaV/zvYTmEqG1IZqS930fZaTbe0NzimE81V6uklMgMZLhFFwjiWcowiq4Iyr6IczmixEEMJYJkhRFfcRUHQHPkX3EExII8hMVNpJTEnvwpTdRbzM56/7uROfZzQjfnsZJmWfg//eToSnE9IMKvMiIb2IoOxrCMi5Bj+6dl8mPxZQv6KbtKSO5ZTVtJZTcIaXMounW3eydsJZedfA2efeXdZzAuMZufYpdfeZtTuATmuka2lyANUd/SYbU4C+wwZVoyrTVMz0YRutKMH8jMnP50x8BKj0zVFmqZSxoHR38xiTfdf7BAsIq7SYycUydr4jpyRjteqk4GQs+isCasA8+SPW0n3bBMhaTQulXLm1mILyOyZ6JqSRCKa8iMKdr9g+jtX0DucstrPAlLdSe5tMnXC+XncDU+t+HpxR5Q6c4QUXTTgXUXEPw+hxhxbdR1Dl9wgovI/hh05hchEtaEIj5hYc4eUOvPYBb6RZSQ2YktmL6NTTCE49h8j084jKOE8Lypgzi8q+jmDGnTYRsjFnIGPOoCIn5lS86cSc1xFGWCUBauGMJYimpMTjaIIqQBV7OnA+6IPzNcaa09hQFlAF6YLzLVnP1gcGUKfeKUBlRQXogz5AJbl2aS4TIif+ZAzGzrTx57LjP1AE1KWVhNVK9dC1J58YOH8tQDcQTAunarMDwXRiTHeL6YDJpNCEN4/xLa2n8SJ8La+iEtInbDNTQmI7WjhlNd3hVLVEcL5ee4t9cQNTam+zb5zQa0K1A+e4qkfsN9WqHTjj+V5wRrvglLcMK7xoqjUh9JqC06f8CcIa/zv8y57h3xJPY1zFQ0zJO483D7fwcgde+4A3G2tP4vWkRoTva0KQ4GRWHpF2jqCeRXTaRYJKmTIS4ctiUpR9g8kRM3VCqlg0rOAGwgmpRoiMih2rGVbGO6eMcYfuJle9U9ZTydHY6vtGqnVOYLypeuckAyi/tIGUSRIb6TW5GDakMkjHet43gM72AuenHQ/xGTtInaEOUtb+Ha2H3NzCTpVZnmBJF7NaduRyun1pJTt4Fd3+WlqidYzf1hMGU0+k5fw1XbuxmCcFppP4CEwLpUl+en4wkuVcyOtYcPT7vrqmJGupdviCbfM528ixmg/64DSAsg01sGHhnKF4v+6eSYbc4VS5zxNOZeiymPGErM9qlt0zcMpbRshzltGY8bUfzw8rfYQhpY8xpPgxfAofkZP7iKUBnJzchXkFPbzk/msf0BDLy7sxIaEWIQc7EMjs3C/9CqJSL5g6Z2zqRa91TluEj8gllHmuOmf+VVPnFKAOnPyiA+C8xwtxAB3lgnOcC1BPOFX7tHC+zuzRG5xq3MHgnEtw5zMWXUAtZNy1iG5uMS3JUoIqrWCHrqLVWc3OXcvX63oIJ7X+mKTykgPnr2U5TQLkspjSgKycN5OgtGB6wvktLamxlvQggvPTtgeO1WRbuYPpaTnd4Zxa58D5Kt274FTZb3wV+4twqvAuME0iVPkQMdZqEkL1t+AMK6enZe7hwMmYk/HmUGbsgnN40SPmL/cRQsM36tBRvJvezkvuv/YBDfFtXjNG7ClHWPJRBBOyIamXDJwjks9iRApjB8IZnXoVUekEL51gZlBZdNmCk1Y0kslSFAGNIZwqFyhTE5whhDK8/K6RAI1yA1Rw9gFqrOdDTKx7ZEaLJrORLJyvN9By0r0rSLdwSoPCSdcuq6Fi/HxmpgtbCCaz+cXttJgd7GBCKq3sYqezI1ezQ9cdIZDs4A2EUzKA0nrKtTsW9B87grTpuPex/A0n/oKNx/8CHdcw7DAWsw/M/hhzIJg/GDDn8Rrmdj0z7vwbXpfAtO2htvmYbaS2GgxOE2+64FS8KTineIFzbOX9PjhlNd3hjCx34AxnIhRsrOZ1JshOsuxTQkBL7mJY8QMDp0/uXRpCJk2Hj2PigXpefn87DGiUT9OrELW9iElQL0L4IX9KuUC37lhOwSnLKThlOQWnrKfgDMsWoDdoPR04jeWkBKfizWB+UVlPTzglDWVaOPvd+0NjPSexgQycDMZfq3cANXDSgr7VfI8Net9JikxD3zcW4eO2+8ZCqDO+ZEeoIC84F7QyCaKs9RSgS+jqV3QyfqMsoGvYwdaCriUMpuB94rEB1MSftKRWarON/H/JtuEvlWJLaf1xxZfSXwnmX7Dq2I/PWUwV2hVjmjjzOYvpAlNJED2E2kFwCswP2++7wLxv2kxtZ2Uydd78Jt6U1ax1kqEptTcJ5nVMpnufUHPLVFXk5cYQUMHpHcy7Bk6Fc0E8SoIzgC5eSbOS5+GFt6m78Ct8YIa+4zLOYcTuWqyoOc3mcNpkQAN9nFpGOAsRkXGM6f5d/D7lHMIJp2JO49IZc0akXzUxp8AMpls3yiGAKs7n8XweY4wChgMFVxl/8mdLePfws2Q9QwmlA+g9494lJzG6Zy52rGIZN+tp3DvvVJUxVATW+K6G0wTnrKZ7eIdSw5qSEhvcjLW33jexlSQ4VZhX4Vk1PpVTVFpR7KmRE2kxXZ+0lB27jFm93PxKgrmqlxaLblNlGyVGDqTOJBFJWbSksW73NhxMG4/fI+x3GM/ex8aT/RbTWGbGkw6QPxogVxso/2rAVP3S1jCtxbTJjykZEcrnLCbBnEMP8RWvVWB+1MEYk7Jwvsc2Ggim2lO1ZMJJqznDBafiTQvnRDc4x7K/NI4+uvJBX6zpCafAVJ/7l94zClQVh2FeUPEVBBZdJpTXTCIdVEK3zmNczmVE7qrDVwVH+9pmQAN+lV2DuN2lCEs7akZ7/pB+ASEZsp5nCeVFhGVcRGjGVQRn84/QjQfQWvq7pNpnYP41hgOEtPAKk6MrCC26SgusL3DHWE/r3lVW6IeTF+gBZx+gGsclnK/RnQjQPjhpQd9uvOvASVA/YMNqupem05lJIATVAqrCvC3Oy5Ko8yTFZN9RmhixgFCqo9XpynwFgWCQpeqbxeSShVRQSYLTyr0trTacvM+D6/WpB1h7/D7WHLuHdccfmzDBxpRKeOS6+4H8i5HzPRwwrcW0YNpJHc9ZTCaAgvNL3oyymH1gtt1zgenc1LPZXgLTwql2FZxvyGoao3ALr9bcoNW8RjBvGjidGUkOnCMJp8C0GboFM7z0joFTfe5Xdt9IcAYXkYkCMpV/HoF5F8nMVYQSTpUmY3MvI2xPHWZn9BfkBzTk/OImjE2sQnBKO3zyL+GP2RcRqAK8q84ZlKX3V82HCUjVOn3oxofn81hwnbpq7gh/3hm6Q4wYcxo4ZT0ZKFs4FTQLzhGMXQSoLlaAKtAeX/2wD86pbIzXKTWW7uaZdO8Wztl08e/zbpcEp2YoaXbS54TzCxecn7GjPpN7oz4njF8yU/+KHTuHHe3UP9mplOqf7jXQhXSjGuZccVyAPjaykFpAjSXlz6zjzyh5MgkULa2k9pS7F5C2fa3WnnpICKkTj4xlNqUrgqmyloBc2vsj9VdzFJTGYqpc1M2wpA9MB0rdYCb5cbOYxmryhhScBkzqA8KpKYYWTgulA+Y9YzXVrrKagnM623sa4Zxce4NezIFzfPVNgnkLY5jIqr/iK+57hVMhXCjjS/W5X+lD+JY8QGAxjRONVkQ+vXHeWYTknjOA6ud9aABj8ultE1oxM62pr70GNNqK8g5MPlyD4KRWBqrn8AppDsiS9TznFOAFZzbPEU4DaO41+OZeJaDXjHxpNR04GQTLhFN9ltPl2gWnde0qN8QSSGXtulgF2YJTpYqJNUyIXHBayynXrmDdwNnAhiWgspzWejoTkR3XrpKJRo0+pcX8mPqEcH7KTvyC1uZLQvkVQZQE6dfsfDvMqSFOO4K00IzBC1DB6UBqxrMFKGESnCt7+Lr3MUVQe2gNGQqsI+A223dvX3fJzW84dhcbeu9jLaXPWMXPMLOLaCmX9PwFixla6CbRSNYC/v/8Hk2HszOO+LrzoZGmxn3HxGcuXfgcgvhlBz0Hz3/aSW/ScRcfdNyheCO74FR92CRAbCNN9HAfspR3csbRHZdu4HS5dMdq3mFfqQxIueBU+WhQOIsfGQUU0ZLSaIUWCM7TCCNfIbnMYyoewTf9vJnBFkOPrVn0to0GNNjO+tOYmlCD+Iwe/OFwDwIYLwZmXkAo3btcunHrtKBhWXTbVCgVTkhDaUE1GUS1zuB83iEFhNElTToWoKGlToAsKYuLYKauorxqnhrSHMX3uiMn0KpO4lGaTPeh4UxNBpE0Y2m63I5rOt1MWk47nNk/AfkRrcRDZu6P8RGzdWXwOir2+rzrsYHTAHrksdHXRx4ZfcNOF6DzCJisp7WgS3hcQjDk4pcTVOvqneFDuXxnrFtZtamNMqHRXEvVS42Y2CiWdG9nT21tvYCNrZewtfceNhP4FfweioMX0L1/fZrf+QzDE4YHc088wKLeB1jW/QDLCd3y1ttY2X4XS9tuM8m7SUt6i9dwC1/23MTHR2/g3c4r+KjrGi3ndeoG3m+/7YJTs4+eMKl8ZubNTmt6iqkE9FXe5JN5w09svEXdwIQGunImRfJg49kPGihxXLoLTiqO/RVLY/OcWyc7YczKQ5jwBBY9MEPbvqWXmRCdQ1DBWXLDcDHrsqn0aHZbWAE5Su/BuJQ6NonTLs811LQDVRiZ2oUhh49hWI6mx10ycCoxUtwpabRI0kylSMafEQRU2brmdCrzCs4jkPn8YlLRbTMjPoRfTndTH5yVt4yeg5PvJ/L1q3T5kploYIY0CSjvajOkqXKHGftlnNT8yIwYGUhbNWmWWSgB1QxvyZaYTDml81E/oLQ80hy+dwd0bvejPkDn0yrKtTuAOjOZDKQGUMHpSIAKTkkFfElgKqla0kUrSHe7mn/bs63dtbPtLNY1nsEGQrShmy5fM6mO3MeCsw8w7zyPp+/yOxHAzqtY2HoZy1quYGXrNazuuIUlzVfwTd1ZfF53mqHNWVrMS/i89zq+OHGbXuMydR0ftV3H+623XC5dFvMJE8vvMb3he0xr/B5TG5/g1caHmNR0F+ObaCUbr1OEk0bBFN2ZBzhZ+kA4BaZkCu+mvjkQzvD8BwT0HnxKb2BY6SUe6c4LCSdDxqjMq4jIZjafed1MtwyiURyT2sDmcNrkuUaadbgOcYdaEJB+AkPTSTlhDE0XoI6sBTVT6LIIKN28ptKF0sW7W08jWk7BGVJ0c4D1tJZTcJqZSnw9gsfRPI7nhU7kHfg8nG7j7YRT0pCm4JQsnO/QYkoWUAunrKdcu+Ds16MBFtRaT2tBBaeZYueSit7SUrpsm6isJIDSaiYya5hlS3rtZPv6PyY2jGmX09WuaL6BTe032MwD29yb1rffxBICt6yZ0oK1lnNY3HQaC+pPYCE93FJa2xVHrhtQF7Zfo5u/haU997DsxGMsOc4brPs+5nTexmdmrdBtfNRyBx+20MWzzTTtUCsMtOxlZsNjvFH/hO35yAwXywBMrr+DSbKY9beNxbRgWqs5GJyRdOP9YMooMYz7CTgjsxw4g5izBKZ3Y3RKf63zuUb5IrcTcftr+Usn4ZN2CsGEUxV8d0Dl2u1EEGeO52UEM/YMYtxpxD9kptVRBkwXnA6g/dZTiqLVNJNBKFnPsQRSrl3W07h2uhTBaQG1k5EtoM68w4d0Uc6EEAtonwV1K9Ar9pT1dFe/BX04AFBrQc0YvNysB6Q2UVnR872RQBSUFkyVo8x5Ay2tKq3tKrrVFY2Xsb75Ept6YLv/M7S856/43JUoKh7/kGC+T+uoVQWawP02j2/VsQ2pN+oeMgHizc/2dqbG2XH0fjj7raaMCT0eJTA1A8kTTnnLF8EZYS1nJmPRDKfKIzjHJL8AzpXVpzFydzmi048iMO0EwdQMeAKYRutIMEPozo0ymZEzZgii6w+gAvOYKFGBzLoCCvi+8KqRzHVwIcEkoOYLG+upxMiB004G0bqTkUyQxlDWegpQwamYRwV5jRjZCSEWUFlPyZmx1G9B+1w8g38LqKynAJXk5k0cysRBEqDuFtS4+KMqM/VPVLZyt6bukForqtd2zH5R530sPfqQ57Ve6QHWdt3BOrrZDW2XsKXlPOTOPfvg79Xqo3/DqjZm/a1/wxf1z/Bxw1N8SGkFq1ayvquVBQTybWpWLRNM3vhv8vgGIZxOCF9nUvoak9OpVQ+d0SAXmHYmmTucBkz2mcCMYOLbD6b6nH3sglMx53DC6VfSD2d0Rj+cMmoBTIhG03Pb63juwqRJe8sQl9SMkKQjCEs5g9AUunAXoAJTcajG3p3M/QqzdukS/AsuGgUUWvFnvcDpnhxpxEiAas6fLliuXdZTgMqC6s4VnBMZ+zgjRv3uvc+CNjrzPt0taB+ktBgCVKMi1r0bF9/OrJ7SCMpnzHAtoNaCSt8S1nlHaUEpT0jdranqj+5WVFCqoK/zS5lhL2EMqSRmRc9DWtCHWNfNTL3rJja2X8ZWArq3/ZcDuoox6cLjdOG0yp833MYXtHhf0rrNKeM1lTIRrP0rPqj7K6H8C96rf4p363jTCkrG8LPout+svWM0k1ZyOhOeaQbMh9QjTKl4aCzmaLa/ltZYMJUjqL8kZ/YRjYwHnKa/NQPeC5xKiCyc4Rn0sOlXzQBOYOpLwPlRZhPGJNYi4mArIlJOE05m7JTcezAB1WylgMwr8GO2LjD98iSa7PwL8C2UzsGv6LwBVC5eIwAOoIw/XbGnykqaRmWydr7WhVrr6axrd+S4FQdQjRjJer5KIGU9X2PW7g6ouwW1SZJqeCo220xegPap/b4B1CRLboC6Z/EC1MwDdUlT7iQ7bChp1EYzg5a5aanO8ee1BERx4DJm/IuPPsDirrtYxlhwVectrO+8gd3MrLc1nmSzP98PL9Lm1uNY2nwSn5Z34ZOqUwTzuim+LzjyNyzo+A982/zv+KLub3i/7t8xu+E/6Mb/hrfrfyCQj+nKNRJ0GzPqb2BG3S2zDMMMVxJQWc2pBsynDKsem4GRkWx/KZ7waqJ4jCsUkwyYlMAML6bRKaLRodzhDC24Z4YtfUqehzMyvR/OgJQjPw3n0opuTE2sxoiERsQcPo6I5HO0oFQq4WQMGpBBi0k4fbOp3MsGThXtB8J5lnCeN3BqEkk/nKqBWTjl3lWUd1yDtZ6jCOUYWk1l786IkRucjI0EZ797H+jiLZzWxWvkQ3Aa62ngfDwAUE1QthbUHU6bxQtOC2jfZOUXAGqnry3R2Det63eddzH/KLNtWs75Xfcwv+MOFrTfwXK69/Xddxl/XsCm+mNs9uf7YTDtq+/AuqoOrKzrZaJ0AfObruCr+uv4hMB9zLb4SBtR1D3FO3U/MK78D7zV8N+pv+HNhh+cBIg39fQGAtlww+i1hptm7oIqIq/WOAvXJlU+hSYSa97DCBeYcVTsS8CpRY4mz6BRihgEzijmKxbO4DSGf8pZCOeYn4JT+iS7CVMONyFyXzv89nQR0DOIy2FskHIefslnzUiRX85VU4QfzlhzeN5F6jx8Cs4bOANVzyomzHTrqnuGFDplJcEpqUBrhzTl3jVRwAGUd6cLULl4uRXJG6T9MagDpzYBmNH82CxrNdZTLp5w2jqoEqQPlMH3yeXmaTWtBXVKTv2x6FdMmgSq1ddHXGuTjriGQI9+b2THuK0WHtGRbv/EM1rcB0ZLjj/FIlrQ7+iCF3XcxoZeWtLKwVcf/pRWlXZide1ZLKo+h+8Imf7u3GN/wwedzMJbH2Nm2/eY2fxXvNH4V7xe9wxTax6bxNLM8mq6i9eYwU9puoXJjbed+ibbcQLd/vjaJ2zzpxhT85Rg3kccf0dgxlTfQXQVoaSUyEqadWbkDmYBQzkmODJKYbn3EJqntWbXMJw8yGgF5p8xcMYSzqg0/nz6NbNbTIg27HhRQmS1vv4UXk+owviUowje24HgA0cRmXLWqXfmXjcloz8RWANnH6AOnP4FDpzB/DL6ooIzVHBKLjg1aiT1W1DegTxqSFOF3T4LqiFNN0AHwEnZGNRYTmaiFlBZUGNFeU6ACk5tHOAU6Z2jdfPuMai3RMmJRfvhdAd0Hq3kYIDKumrE5lsmREaynIKUgC49cs/UJ7ccvcXm9t4H6Q212Nf+/AxxT21uuYnvqi/i85pL+KjxBkOam5jZcQ9v8btNqLyF1wjcW0yQ3u3+bxQtaNf3mNrCG73+pqlrTmDoM55wjm2gt6InGsl2HUn3H1/7iGDeRyxj/Vi6fAtnFD9TYCpXMPN1tdLBC5xmAroLTo0cCk7fwjMIzjuD6MyLiNF8YcIZmuZMGBKc4w+/BJzS+yk1mJjUjsgDHYhKPIqo5JMITT6NSMKonT2G0NX7ZV+HD5Minxy69Ry6dAIaIDiZkQUXMKunaTfFeJf1DC7i6+J7BszAUlrQPjjp3svu9llPC6gdc/c6pc4jBp3m2kLFysKp4TlZT8H5btsTI3c4rSykFtA+K+oC1Lp8JyZ1INW4/Fy6cGnekef1Wes9189o+fJtzKWb13j9Mh7nlL9476AVuYVY3NCEBW2dWN9zhqe8/5zVwpZr+LT+AmY1nMebLRfxFmPaGbSMmmZoFqsRQE3Yniox5JEmMZEcz7YbzeMo3sQj+TqeN3ucSzGMRaPo7qOZaEVVE8gqJrGUatThFYSPYBq5gekOZ3g2+zv3toHThyGfvwvOqIwLiE1z4AxJvWrq5MFMiMYlvYRblzY1nUX89hLE7G/CuPQTiE8/Cf/9XfBLYhxK6xmUdwO+Wfyj2RQB9c1mTJHDmDRXseZ5murL5gvqzrFDmgI0sFi6beAMIpCa72cBtdbTmYzsTKdzAHXkbkHdkySTwTc+MtbTXWYUiUd36ykpQXJKTM4Sj75aaJuTJFkJTne9yJIaa2pmPjnSZN8v6GK/7v4Rc5nFf8Pf1xY5ijkF0oK6c2zm59t9W+NxbG86gbXNx7Hs+BV+xml8wdcL285ibdc1/sjzv+Ouee0XGXP2YkblEXxEQN9tvY6ZdZeY5JzHpLLLjCdvOh6n8Rkm1dGFM0YdS42qf+QCk7FlHfuhnskqM3kpooYuvFpQ3jQSmIPBKTBVGtKgjOAMzbkN34KrBk6/gtOML08PgDM45QrCshl3Jh/B2EO1vATnOp67ME/Jtcfvq8eopA6zxWHwgS4EJh43Y+r6436CM1Ni/MkEyT9btU9azNyLrnXutw2cGtIMLqDlJKAOnHfN2L0DJ+8uF5x2UogAlQW1IxHWggpQa0E9s/gphFCAOnJcvR2HN4DS3QvQfjk1UJPJtzhzHd0hNSJQVp8Qyr7ifedjI81ykrQMWdK0PKs5nc8wp/vf8Wn79wbSBT2EtP0evqq9gCVt3iGbW9aNb8uPYVnTZSwmWF8fvcvvdxXvNlzEp0x8vm64jPnUFiZYnr/rrm1HL2BhfTc+qDqCd+uP4/2Gc3iv4Qo/57YpvE/T7n6VT/Bq9feYWP0D2/IZRtfKlTPGZAYfW3sdMXXXEV5LCAlmGMEMrSKEVGjlDYSVOwplomPkYTWVfWvORVgWDVL2LfjnE04aLAunFk7GpF5GdCp/nnBqnkZgUifGJNbw6zvX8NxFeWpNB+OS5CaEbClFdEIrRqSdQJxGhvhhw5KY/GRYOK/Bl4Aqiw8mpBo5ChecWmPkgjMo37GeWr/swElIaT2Ne3eznmZKHeH0jD+9WVBbB1UNtL/E1F9m0kQRzWQSoNa9u0tZvCnUN99zzRB3IP2I7tiIMaOVAHVcvbMux6zNcUFqlyJr3qiVgPys629mIzItxV3Y+1csYCz4dc05bDz2PFzfNVzAN4Kv/S7mdtwnjHfMtMB3eKOpyvAl49ivdUM03cSc+ktMgi5gy0/sfbm46Qh/vhsf1fTiXYYRb5dexDtsy9k1P2B2/f/Aa+V/wWRqQuWPMEt82Z7x1TcZY16mS79q4AypccA0IpghFXTZZS55wGmtprJv7Q5j4TQVHRecQTmnEJ3WD2fI4cvOoE5iB8YkVPNrO9/9uYvxpk+qziFiWyniEpowOoMfnH6GWftZ+B5mDEE4fTNuwCf9ugE1kNIf0pi7JoTIpIfk0GK64JQCGHcKUO0Y4l/myFjPsv75nu7WM47/J0DdLajGeZ1ESeWme31Jkty7lYY6tfmUALXWU4BKcvO2zGRLTbZY7w6paqGSN0glC6kWknnq845n+LidiUjT9/ikRdn9M8xvu4PFLdfZrP3tu4auek7VKQJ3xdRSvyXEn3b9iPdbv8f7TX+jBf8f/Fv/nX/7L/i443vzHT6gFZxd3ouvqo9jfdfFAZ/nTcuPXsQntMizck7iTYZgs0uf4YPa/wfTi/+G10r/hlfLfsSEcmbobGNtXRjPBCum+hJdOUEjrFJQFeEjmEFasObam0ATekKKCKkLTpN1e8AZnHWzH868UwbOqNRziEm+hKgUZvVJroGdg+0YfaCKX9f5zs9dhDdt7LqD1w5WY0pyM0Ymd8J3XzN8knrNHTI86yqGUMNoNWU9BWco4YxSJsZzmhKlgFjLhzUZJEgqdKyn5vhZy+kOpzugBk5X/dPCKXnCKevpwKk9JQWncxywtY1bDdRA2uyUmeykW7tRg6aVWXkmTJIz/NkvM6HZiz5tf0qQBdp/MPP/0Wxo8HXzXbNfqG3bJR3X6ebP4QvGhF8TOlnbd0wowlCj478R7v+Xn/P/4d3Gv+H1SnqC2numVKQ9Nj9gtv9B9Ul8WdmNRYwvdzBOtZ87mJbwBvik4ALeIgxT069hZvFTTC39gXA+w7iKx2YUKLb6BhMg9l/NVSY/mqSjfmHMSRlXToup/bCMigkhFcyERzITOPKvmVnu2h0mNPP2c3CGZA+EM/DwJTPByD/xF8Bp9VZSFaL2lCAirY2AncKfGDv8oegyfl94BX/klxnKoFYr6SIzrmAUL3wU41GtylRsKplivKsg74jnXRNC7MhR//CmU/9Uoddunyj1bQTmcu/uWbyTKDmZ/OS6xwZW6+KNm3fFoXY8fnrTPQLrLDUWqFo0Z1d1CtS+TRuY1TqJky3iO/qo/QlBfWog9CZZuU9o7Yw6f8BntKSfd2iG+iOzMvQrDZ0SVi0tkczkDN4s77c8xrvNT6infP3vvGH+G/Xv/D5/wTutz8wemua7tdzBB0038HH9ZXxWdRafFh/DvLIT2Nb109tcL1b8WXQG79BNT2FiM47tOqr+ISKZmfvRcvrW3kAUv8OICsaixT9iROETxOTfM0vAwwlfdLEz5BxadIVwOqsetALCv4i5hzJzJkD+SoYJZ2imA+fw/HMD4IxOuYQIwhlASDVPI+BwJ0Yd+hkxp7uWlPCXd+chMqkaQblH8PuMo/h9wVn8ruAi/pB7iRaUXzJdccRVjJIIqFy7zLwJkGnyFZOYgNkVPNsxd09AFX8KTu2vJDgtoJ5wWkD7s3jBqaXFDpyv0oIqUZrSqITJAdRAShidTRqcrf4EqIGU1lKygGprQMEifdD6xOjDtqd9+qj92d8l+5n2b0jv0bJLAvSd5h9p1f/K7/Mjb54fqGf8ns7sdWP5G29jdtMtJj3XMbuSWToz8i8bb5g1SZ7956n13TfxVk4npuZ3Y0LZOUxgshTHmzWw5iaG0IUHMHyKzH+METlPKR4LHmJE0UPE0utpe/VgJb1aJ+YGpx8NlTJzrYrQUh5vcAZmn+yDMzzVgTMg6xJ8mXT/Yjilz3JbELe3BMEH6xCQ0Q2f7DMYmnMewxlj+tKdq9ofk3YNI/lH4/k6XCUCxR8uQC2cfYASTiMPC6oESdPr3LdPlDS1zgIqaQqXAHVcvCtJMnVQZ/Xm5AaCSmupZMlCOpVW00DKjpAF7duL0gNSAard1yws7gB5A9Wb7M8NJvfP7IOS4Yc0u+kx3mx6ihnN3xu90fQM0wnsNP6fbixZfg1Dzmy85UBafwOzqi9jVsVFzK64wBjzFLvMez+6a2nzeUzLbsOoTCYkZRcxil4mmu0VRji1ZaE2f40uYA5Q9ABRZQ8QUXbfuG/VtrX9kKStiMxUSfWxtivK5jGL2XqGC04myL555+Cbe7IPzqjkiwhPZgjwj4JTmnGoCuFbizCKcEakH2cCdNYkQVoyHJVxE3EZtzCSX2oEv5SFc1BALZyU2dvTBag7nNodWYCaDWhpSS2gkp38qhjUxqHWghorSlc1iTGnLda/2nDPAGpdvbuLt27euPqWe8bNK1OeTVlgLEAWVm/AucsdPm9y/zz7NwyY/Jva8vGNpicEUlA+w2sE9TVazSk8P4VwTuHNNJlwTqXFe4Nwzmy8w6TvFmbUXMPrhGx6wQm8l3fUTNHz7ENPbT1yFZ+UHMOU3B6MoTeML71u1napzqx4P1oejVm5+kg1apUBVW1RUqv8weQSzCtCtIdB1nWEkwWzt0EazzFhFpw+uUyiXXBqtNHC6X9Y8zUuGjhHJv7MbN1TW5sv4M2EGozf34DRh7rMbg3RyfxjqZeMS49JI0yUvlwo406VlvS0DW+AmvKDYk8vgCoT1PaJ2m8n0rV9t7WiAtQTUuPi3QA1kDIhcsSM3gNSSdZTsrOa3CE1mzfwvFlMRwlUA6vgcUlu/++R+2fZz5f097QnqYZiX2eMKb1GTeHvSJOYXE1i2DGRgE5s0MMEbvN6nBEgXYsWqU1jhj2j/DpmFJzDJ8WnsfnIXXaf9z61WtZ8HdMZrmlH69gMWriCM4gsPc/k9BKTIcaRJUxsiq/Dt/QuAiue8PVDuvOHBPOBqcqEZzEUY7/HiAPGk5Gp9ISEUwM0Fs6ArBN9cIYd5me64PQ51P73wyntaLyCN3ZWYOIuArqvA/EJBPTQaUQevoCw5CumsBqUquKqA6c3QM0ogiv29Aao7lSzhWKJdkh2APW0oFaaRe/NgmpzMEfsQAFKCVA9HUIyFrSRkBJU06mNdw2cdsjPLkWeVX/PHB1oHFBNKYpQKeMf7GgA1IjUIEf9nBm9cn2e/Xz9PbP7Bq2kXLks5lTqVVpYB8xHmMDPn8TPGM/fG8vvObbmNsYTSt2ECmG0Lui1embhbN8JaScwO68X61pebhb+/OqreJ2QRqQ0IiyvE5ElJxBecQ7B5Vfgx77QtjLD6fLNljKFjxGQ+4j9e58W844Z9YklmJLgDEm/Dj+6bQNn9gkEZZ4wE4kMK4Lz0AUDpx/hjD/4C7N1T60rPY03tldiyp4mjEs4gvhDvNN4R2jeZ0D6VbMRmDZgUPV/MEAHs6CSKe66tlMMLxm4CW0MkyVliyozeZaanJEkJ4sfV0tQqfF1hJUSoP2Q3u2zoALUE1JtAaitWUwRn3KWJT9wCvrsfGdY1NZN/76j/Tz7+TqaPYt4VJ1W5TDdSLL6iqE1Hi6N4++Ma3yMsYytxzIBtE8pmcCEcCLBHKU926ufYirj8+klF/BWdgfmFr3841Zmlx/FhNIOjMhvY5behfBi9m8pgWISO4SufDjBlPzyHiEg5wGCM+8ijJYzmjlHVAqNSgr70gXn8JwzBs7ADE3DPIOopAuIcMHpn34Bvolt/zg4pQ92lGDWvnq8ntiJCanHEa/947MvIpAAaiKySgSecBq54LQZvLWg7nC6A+oOp0mSXgBnX6G+Wh1FuQFqtll0wWlHlQSnFnYNhNNlRXlegFpI7WiTlQHrBXL/WW/SzxgoXbLn7d96o4ZhRrVrIjCtvxb86XvrOnQ9GtHR2PhEJksTG38glA6ME2poMet+xJiGv5gZRvIiU6pu4rXC05iW1YFPCrqwovblput92XICk7LrEalSYkYHogvPMUun9cxjLFn4wGxl6JdH9557DwFmNSX7kYYpPNWBMzSNYUAm3TaTZ3+69CDmKZEuOMOTriAw8QIC0y7A7yCTsoR/IJzSovxuTNpRjpGMQeNSe+Cf0oMh6SfMMg2fNH6JfCY1BbcQkMqsnl9KM1CiihhgC0oXpCbL87CikoHTJTM1y6WI0ltGGt40Q5zlzg4UkrZJ0T4+glSdp2UGph7KDragWlfvQHqnz83b/UDtznayWlb2IQozCJGVPfdL5f75nprB7/gWYZxFON/kdczk62m8FvssIFUkJuhZQNT46v792cdVPsN4amylA+0oXmcMY8/48mv8/1uYVHUDk4rOYVLWEcwu6GUXeu9XT80uaMeoxFqMyerF6CLCl+vahp0WNCDvLmG9iWGZVzCUIGqehcbWgw6zbxnmaeOEYZmn4Jd5vA/OGMIZmnARofx/7XIccKjDjK3vYHKmv+f1S/wSfZjWgJG7ihF1sAERWd380scxLOsYhqecMJOTNQKg/Tv1ZAVNtxOgZjmxC07JwmnkgtMMjbkB6gln3/Q6wukOqAPnAzOT2zxaRnC6u3l2tAC1FtQCatfHW0Dtak/BYkeaVMgXmGb+qOv9Lz1qeNUOs3oeNRo0i/Hz23TJOs6Ua+Y1vM7reY1ewT5yRQ+q0sMD9NgVQTmx4pkZipxQ/pDx9y2MrL6GEQTSTBTm70fxszQ0PKL0JkbknMasivNY23uP3ei9b931ZUkPpiY3Y1xKJ0ZnnUa0tiuit9RCR7OzddltM+v9z9kX8MfD2h2bfUgL6pdxAT6Zp+GbcQyBqb2IIIxRifxdF5w+fK+x9bEH/85S0mB6O7Xa1ECjUxoQldtFl96NsGxay8ST5o8LzFFssAjdbQyArYsfrMxkY1D3DF4Fey2i0uRWSTtNSNoSRVuj6GkO2vnMEa0oO8IC6g1Ss2lAnwV14tABkPaN0zuTSsyykMZHBMs1+4nv7flfctTn2s/3PApQWcxZBOkthiyCcxqvR3BOZdjyKq9D+7PryRYTKx4ZTSintSxzNKGM4Q3jzNEVFwjodTMnM5gg+9PC+tf8gFC6fQEVQmDGZXbiu8aXW2i3oeUqZqe1YQIN0fjULuMtwxlHBuecRWABrWbRJfy54DyGEFBl6iq0+8utE06f9F4E0LOGHWRStP+0gVMTP8SHxtb/aXBKb2fVIWZ/ATOxCsRkd2JUMaHLuMQvpDvsspm3JytqNmuQ5fwpQAmmu8wKTsrCaXeaiKEMoGX3DaCOnJ3QBKgt2veNKtHFm4d00YKOZ6dpdaezwlMbCtyh27xjANW8R6s+UAWVS/bcL5X753tK7nsGXfqMyjt4o+oOwbyD13gNSm4mEVCz+I+Qmp2Gtc8UNb6MyRI1wYg3XukljC0nnBVXEVV9B0FVzK5rvsfwmr9QPyKk9gdTEYnJPMaMvgmfF3t/7Io3zSvqxtT9FWbW2riso4jLoVXMpsfMOY4/5zMzL2Cf0mqGMTFSwjM84xTdez+ckQfOIOQAASacMmCCU7OSdnY6T3bz+kf/Xr2TWYmRe7IQfaACepxHDGMRbSobyJjT7/AZMwNFkKpwP1gWbxMl3dkBdBPugFoLKmlJqpU7pI6cXeyc/eedgvIASF0PSpAEqDdI+92+LUHZOqkje+6Xyv3zPaX1PtrgQNKGZq9SZmdhymyqVXPbXMMoyg5GaFaR1v6PI5jjym8xCbqFccyu44qvmjAovPoJggmnb8VTDCl+gMDKR4itfcjfu4mRmUeYkFTgHSY/nn06mJaWtOPLvDbMyGjG+Iw2xNFbhuaeYEh3Aq8kn0BwEvuY8k09h2HpJw2c1q1HH6TB2n8BIUnM5AlnQELbPx9O6aPMCsTvysPwHQ0IZ5AcV3THWM4gZmUCVUmSMjQLpzdAlSRZOE084waohVOr/qwEaDQlQK2chMnZBECA2qFP25naNc0BVDv2Op0vTay97ZIDqwOsA83LwvUycv9873K+h9m4tfYWRvP1KJdG8H1czU2jeH53xZXxlXp6702GTzcxuuymiUvHMQzSNuhKQqMZVkVVPkFg6T0My7uOodpOiDG8uVHLrmB0djfGp3fgzcJefFPx8qtCv8xpwujdRYg5UItYWuHwnDMIymAsevAighOZGKecxfC0E31wqiYek3geIfsowZnABJpwjj1QhZ0dzkNbvf6hf5S+Lu1B8J4WBBw+i7CMy+ahnJoIor3jzUx6wqoyk3upSXBaQAVnYCHjFSZFAtTdgjplJif+lCyggtPKsaKuOaGE0xNSSZ0iuUMqy2Qh7YfVgcUdLME5EKSfL8+/4y5jIRlejKYFl+LrCGA9QatnIljPBKfhBiLr2J5UdC3fV19FTNV1xFVSFdcQX0agyx5hdOEjjCxgwlhIFbMt2F56mERY/iUTJ4blX0BM8RXE08LGl9CQ8H0kXfDopNZBn67mTd8Wd5sVu/GHOxDHzHwkPV/YocsIoWQ5BadPWo9x65EHTzpu/deCU5qZcQQxCV0I3deOmNSTZha9trhRzBme72x/px2SrewW3mZbRQHKuCWwkHGq3e+ToLrLWcPiDqj27LnfJ5Mw2UnLBlBm9AZOu/yjf9jTGVly3LziOSsDq14LqBpXnbTWGRLVe3ve82gA/Imj+Vz7+R5HxcNj6NpHMjGLZ/w7golaHCHV+h4BGlV304HSSkmPVHkNsYRTo2ixBXqUH+EseoT4IsbkBTfNk05i2ZZxpdfMjRnDY2ies1GwvyvzVlg1Kvcspu4rwsLcwZ9P6akNGv482IhRu+sxOrEHsYfOISJZdcxT8E/rhW9KN0O7owhKOEadQsCB82ay8XC+D9zfinH7CWf7P9mtu2tp5SlM5x+dmNCAUUld/BJH4JN0Ftq7cXjufQzLfwjffKeQG5hzz0zr14MQzEx6Npq2aZa0ZXNoAePVQrqiAoYBhY40ydXJ6m+7Vnbehx5hZx5jxwTJrOqkFdXKTiezd2BVwjSKGb00mufHMNsfy0RKD24yYrxqoFV9VFm+SaCcor5mPjlF/cGPTi3VOTpj/YKfiYu5AfTZso7Oee9HJ5nTtuSSYmfJegGzfSSPGoyw0uCEUSkh5A0bXcxrZptEFbEN6NYjCxkKMUSSVEgPUdu5JEPQN1mYAEfknse0/JMYs70AU3flY0vzT68AtZrPbP7NffWI2FFjnrE+Kp+5RnInhia0wo/ZfUAm3XzKefyJHPxxP+NNxpyhu5oxbkf/jfDch/6zND+7EZO35mLcvlpMzjuHEWyAIckX8ce0G/hDxi38KfMehmdrf/B7CNUEggxNt9LqzYFwygX1AUo5gPbDGUTLGUg4A0sIOo/hhE5wSppZL0ht6UlSVq/yljSaGlPxyGhcxWOMreR7ZsJ9Gb6AElyE5udKoJnfpayl1tFA6jrv7WiqDZSt39p6rqTlK+7SM86d55wTUoFZehtR9CaqakRSOmqVpKT2Mm3mqicbKBlC6SgwpZC8S3TPZzCeFm8iXfys1CasbXi5aXjS5vJjGLW1AAFbihGyuxJxaR2IzujFsEPt+MOhbiZNFzCUfTzMtaNM1J42TNrhMUK0I6u878Q/Uysre/HaAX7JPdWIPtyNiIxzCGIc6kvX7kNLKflnydXzrs5kw2lOYO5ls3d4EOMjScuNgwsc9YFJmUZm41tArcIIo7OrXf/KTgupO6DSyLJ+UAWoHWXqy4Q9IH1Z2REq1Vn1+zYhs1UDe97b0QBpKg6q4zoJnrX8VuYJvS4JSAdKun3KAKmwR0t3GaNbKD3B7JPifNcyC8X+2od1NPtgDDPtuF01mLKvHCuqXz5R0naPMzPbELuDydJe9n1qO2PMDvz5YCf+kHgcQ9Po0gmntjyK3NmCSds8LOdHq3dic05/8fOfqXVN5zFmaw5CN+VjNIPjuOzTiM5jssSGUaypFZw+2igsmwlP4V02Eu9kNpRZk8JG0xaLwXQ/nnp+4ojmHzr7MvUv+2BMSgtjZVw9ZctPqpG610lHEmJtLKYZT1aeoL5IgksyG2ERRO03pCRMm2HpqJn99vxgR8Fo1/Db2Fnhid16UEftVSQgdRSQKhlFMmnUSJoBkuc8gXQH09aUbZXEJqXqj4j8u2YvIz1mcnTGaYw+0IxJu8owv/D5B6m+SF8VdiB2YzrCtxVgDPOQ6KxTeOXAEfx+/zG8sv84Qg4eR+iWeozfXMIfd37H/PP+2gN4c/lu7Cj/eX/w79H7mfUYubMAodtLEXGoBTGENIpWUWvefbIuwy/3plmlqSd2SFqPIgUQwsD8/ru9r1hPQLWvuBRO96RnIJmYih2jJ3poWE2ykJpFdC5IbZ3UvQTlwEo4zMSS5+ePWgmiF0kgavMrexSQ2ghLc1KjXUtP9H6wo4C0y6TNA8Yo86hGyv1oapiSSRCpYme4V5UN5xlATjlOsu7btp+d4zBgUCT7inmcT1DeIwxN5Wdk3GTGzxs04zyidjZg4u4afE6L6NmvL9I3uY2YuDMfI/dVIjb5KEIOn8Sfdh/FsL29CGdCFLqxGuM3FvJHnZ83/3y9txBTlu7D66sS+/7jX6Gv8uoQsTYREdvzEJ3YaOIRPWUhIPcSfHnXDieUmkwgCVb/vFtGelKxnlhsIM1jB5gGJpyEWw/oiuQxisdwLb4ioOZxMy55WlKb6WtHXsmC6sAqy8SMt+ymkSD1nEf6IvUBVuUctceQgIys0LS/m+ao9/b880fCx6PdJjKcN4p9MprZo8jtKCC1ckBAmv33DZj9JThJJTmBqXhSXsjMp3VBKWlgxCjLeSCFllkMS78D39zHZjqcn0Z7Ui8T0IsYy5hx1NYKfJz68qUmaTVDglEbM+G3JhfhBzoRsK8boYknEH/4BKI2V2LyxgL+mPOz5p9Pdhdg+sZsjF6WhNmk2v7nv0JLy5vxfnYNRieU8MtWIySlC3pGjTajVYFYUBowc27Dj/LPvWMADcy7bQAVnGpkC2cEg/io/IuIZgIVWcBkSh3CjjPWwwWotaJS35i9C1oLqSQQ9MxwK5sJu2fHPyUDmNn8imAROrP5FaHUQ0t1NPC5zns7SqH8Of2sjlqaG+JanqvnSrofzTpyKrCElo8KLLkOP16/lVZFas8ik/CYpbtOPdmCqSU1glKDJpIevOufdYc/+xB+WfeYwPJ3Uq8jNoc3aPpFxO3rxJTEVsw+WIPdLefZnd772FMrKo5hyt5qxOysQ9iuVkQndGPMoV5EbyzF5A3Z/BHn58w/U1YmYsbOErzBXwidn4g3PB6Q+a/Qm8klCNmUisA9lWyYHmbp5wmk81Au414YgyoODWTDBOQ6cGrNiuB0GvgKwbyMSMIpMGP5+1HK8tkZFk5vgFo43QHtc/0ClD8bRUvkCejLQKplzRZKq749hlzbudj3g0lAWglKSTB6ym5yIDCtAoqvGCi1rMIdTm0N4w6nBkQEppELTC3vVrVkRMkjWszLTFp4Pv8BwvPuw+fQBTMHc2zudcQzbhy7uxYfpTSyG7337WCadqAJ4RvKEL29EWP3MbRbk43Ja1L5X87/m38+2FOE0SszMHJ9IWLWFSJ+cwXeTB347Ot/hT4qaKC7UCbfiMisbgOZNg0LTj1nGi6KUKpwr/2XwvIJT8Fds8pPRWM9NEHzCwVnVN45ROecRUSuaqkq3juxlxlZ8oDRHdgB0NKNClAHzsFls2NvUjwoVxxKSAeTdnd+kbyB6C4LqwVTQEoW0D7L6QJTVtMdTlNwd7lxM4qX7lLqJfNQ3ig9WpIKSz1vdrUOSuPfSeXvpdBaE9jIpNOIP3gU8dsqMG1XCXa1vrwFld5J6UDk6jyM21KGN3aWYeZGDzg/2leCqMVJiN9UijG7mxC3rR4Rm6vxWeHL17T+Ufqu7jjGHapETEIlRqR1YFLhJYzIOMe4hNmcloFkXkZMASHNoctJv2Qsq0pOCuK1N5OBM/eCC87zJkHqy05dw57ugPYlDB5wSr8FOINK6TVc7trb0bpzuXFBqfXj9ii3rjhTGgxMJT6ecIanXTLS8t241OMYkXIM0SknEJ56loaCbU44/dMYu6Y4MWhownGEb29igluFjzNffgmI1Uc0RpPXZWHqulS8veEQTznnnf/cW4zwBQcxZnsNJh08gogtDXhlaQFitpRjaeVPb3Hyz9AHBS0Yw5tmFEOMccndmJRxCmPSTiGSgXNokvOUD01eVp3UHU5Zz4jciwZMPSVMsagpk7hKTWZPH1tuIlyeE0rcITUuXslS8eCyY/qDyYYIg8mGEt6l79if4Hg7KhEyP0MAzRBvAcEr5DUbj+GKL60IpQVTD5pwh1OTcozcHukTmXoGI5O1T3sn4tkHUcnHzQMsggioXypDBiZIAcnMC/afQuj+HsQd6MKIHdWYcqAW619ipae7lmQ1YcbqBLyxZDvfOufMP18cLEXUggSMIowj97QgcEM1fNbRcu1qwqSdxZiT3f9UrX+lFlcdxZjN2ZjIWPj1FAbfab0Ym9yLGEruJpINa3YncwPU2XbxEsIIZhhjVr03MamrZBKmdUpWrrqfe5nF3ZIa16/JJa5RFW/yBqyVrQR4B8+RuxX3lFM0541FmbKYl6NKRuZnBCPBDMqnxXSrBZuZXZSAdIfSgqn2MxNwdMxwe5RP2kVaSgtnO0YcPmraPTT5lNl6PYAWU4AOO3gBPgnsC54bmX4OkbubEUoP/Fb2z9vnXlrGvOPtJZv40nlv/llT2Ir4pYmIXpVjAtSA9VX8I22I39+BkNVZiF+fjm8Kfr65/kdIj0L5LLsdE7YW8OYpxqvM5qcVnMeojLPwO9BjGlJwmh0mchiHSewE8+CubDayq27XVy7JGwiprKqt+XlCKvVZ2UEkgP4eefvMASJ0ksITb0fVdHWUhxCYzogaQcvTTan3hNQldyi18NCCKWmOrZUm5ejBFGFpZwhkD8HsMseI5GNmZ2vB6Z9yyUjJUcBheinGpFG0qtoBO2ZfG0YntmNG8k9vGe6p7dmlPDiv+06+u7eMCVE+YSwgnBX8A520nhUIZIIUu6UQU/eWY15+Z98v/qu1oOgopvM7jt9diXGJbRjFWEiNIfejzcOU0Qfm0uoZMR5iBipgTTxlyyQ6ajKJG6Syqta6eIXUxquDyCtQP0PePtNKN4+ge5EEqY4OiM4gho6SblDdmLpuOy1RR3covYGpJTSaaxtEOCNS2M60mBEKp2g1Q5LPmlUNAcmEkpKLD069AP+EXvjsakPoAW0yfAxxCW2I2JCPz9J/udcd8Obz/BOI2lgGnxVFiN3biVd4DN9RhxF7qc2FGLc2HYsz+5+H/Wvog6wujNlTixEJnRiVzbtbWWQGXZmm3qncxIw+wNRFnSl42tShr3bngtSUTlyQyqKY5ECuzwyPukH6EvIGnLsU475IZnTrBRJ0JlQZRGGMIXW0nsJdpoKhm1I3KGVjS08gJT30TFCazQ0ynHXkgXTTwQQ0JOW0gVJ7showKX9aS3+eC0zWbCNNfeuG764WhOxtRewhWtr9LQjfXIxxW/MxP7+VXee9P1+k505Movsc9h2TowPNCNpSYx5WELyrwVjQsBXpmL69EJvKXn456T9Dcwp7MHl/vSnixh8+ZuJPZZoa0VAdVMV6n5z7PN410ApQ0zHqJHWWG6CyLH1ujwmWMzTKmDP/NgILKAKkUZX+IT/+nJvsvpT9Fo/JlJv64ttB5A1Id1n37AmllSeceia+pNehlAWzD1CCKXnCadd1CU6BaeCk9dTSGrlxKwunJpAHHD4N3wPHEHDwGMKTTiKSfRGReAShdOvhVOy+JozZUYbRa1OxvOznD417PflhUinClh9E+PYKDN3ejFd2tMF/D//g3kZEbizAmE2Z+C7/5xdd/9H6PK0BMxObMHp/E6ITjiCW0EUXObtP/C7tAf7v5DsYkn3LmZbFRtfCflN0pnWMYKdqa3AdZUG1f6hCAv+cm/Dj7/hm38Gw7NvwKyDshMgpxVwxa/ElrTJ0pNeUSURorfM1vOqs49ZRoHuDzqovpBhENpGxN5CnzPNHPeJHd9mbUhrwfwTSXYoxjeiirQRj8GFaTpeCk04bBWkZr0sBB08gKEE6hmAqhKBKoXwdxex9ijL9DTmYvC0PG5q9P6BhMHk9Kb13qAQRG3MRtKcTv9vUile2NJnZ7OE7qxC8Jh0Td+bim/xfJ4t316K8VnyY3oapagTeucGHzmLIoct4JfUefPOewSf/HoYRvGG0ks42jbQMVEAG3ZLcViatA12ak1TRktHyBuXS4ubeJ2z3CKYAJXRGhMXAqCl87uLvE04zlJqrYVXX7/PobJjrHUzJG5DuUnbtLpvU9GkQOK37FpTuWbkkS2mhNA/bZWZu1fcoyeTzRmFJZ4xCD502Cklk3OlS8MGTLjD7FXjgeJ9C9x9F3O4GxG0tQdzmfLzl9nS2l5HXk1bvJNbDZ3khgra2I2o3zfXWRoRsrEDUlhLEbshGzIoEfJHxr5lq91OafbgR8TsqMTqpG2NpCbXlt2+S1kpfg0/2dfgy/tQzkyR/WlglTMZaMha1cDoWhjEq/98sE6FM/KrZUK6pe8qEtWmqnhbiLllh7YGuLQBDaHVDsu4RCh71OYRsMHmzhu76e+GU7P+7u3BrKS2U1lpaKKXgw+f6oHQHU1BaeUIZsP8Y/Pf1GsmwhW6rwViFYFuKEboiGR+lv7zH9XrSXRM2lmPkxnrEb2xE3KZ6Zu61ZvZI1IYSxKyjZV2wB+8k9E9z+rX1YU6PaYy4vS2IS2ajJTM+IqBamxTCZElHJUpy4RoG1Tmzsb6AVJyWeR6hGWeNgtMp/rziWLu2ScAovhOMqqNqXyjzcAbCHaF9KbNumm0AtduajnYt1GDyhNFTenSOu/Sk5gHS05spC55Vn7um7P/buNLIZOMDXbjjxulNXApJojws5YuspcD029sD3z3d8Nl9FH67OhHNBGn8IXq1PXUIXpWJ0ZtyXnqysteTnvpoTxMiv0nGmFWldOetCF9bidANVZh4iGZ7exUimMVP21uAXUd+3rjqP0tr6s7g3cM1JhiPOXgU4QzcldUbC5HGTk67Al+X5MYNeCqzEMywzDOISD+J8PRehKWfoiWlq866zWRL4s/xvU2wwrIuEUJHmighRaXTaqffMNL+pBpm9Qal1XOW0EPPweiStZieMHrKHUinPOSSC0ZPKAWkQqM+EUZNBJYUU0pBB2gVPRS4v8dMf/PfSyj3HIHv7i747uwwuYAK83H7WzGKuUHUxjyM35SFXe0/HX96PelNX9Jlxnx1ANFLcjBxbxfG7Kfp3lCHEQlHzSKmkLWZGLMlA0sqflnZ4J+hpaVH8FpiM0YldCFqfwfC9neZuYMmrsqgFSRsfum0Thl0n0yYgjPPGTijMk+aHTCisxhvZTIOzWTs6AJUy0cE7IASlQtO8wQR17bjkl7LInuziFbegHSXNzAlC6cFry+h8SKTdVvxJjWiR3GXwAxKcoAMSjxjZKylC0h3KAWilft7C6cF1J+AhuzpwtD11QjZ0Wz2CI2mMfNdkIBP0/qf1DaYvJ4cTHN2l2L00gxM2t2Ckfwyv19eiWGb2xDMeDRsRwMCV2cjbl0Kvs779TN5dy2pvYR3M45gYkKLAXVEyknEapID3XAgraf2EdVjugOZHIVmaQbUaURmn0BklrYTZ9KScQdBmbf4/0yWCGuQzimBcsVyNsazEyciaZ2jKR0Fp1MM9y5b+hlMAVkXBkjfUVIiJ5mNC6jgdEeac2Cl90FphI4KTD3rKPlMn1QKClL27SULty7c01K6gyjZ957nzf/tpQjoUIaEvtvbEJvYa6xo8Jp8TNlV+pPlJfPPvA378fXqnXzp/YfctSCzDUO/2IM/zMvGKAbNgTuPY+iWTvhvpwnfrVGBYkSvTsenWb9usd6b5mR24I2EBozb38xYqMPc2ZF0fbKAAszpdFqRDHZOurbqYxLAxMqAqRoq5S+5kipH/ZbME9IIWi0nURpc3oB0l77TL4VTGgCmG5wCczA4AxNtwsM2eAGYfRC6wWmlnw+mdw1lYhTCONRvVxd8drQjjJZUw+Kjt5dj9Kokdov3vpLMPx+vT8DkOauwYG8m33r/QXd9criJmVce4hj4jki8AL9tDIK3HEXknl5Eb2tD0MoixKwtxjv7+5d5/pb0eWY7Ju9rxMgD7Yg9zEZMPIaAZMaZhEwTlgVoYPoZJkhOMVpT82RdtfBOz/g0JSm6ZB/GjK9kXDDvtSG/AdUFTGimkyyFZBGSF8jcDINImxx4yvNnQjJcYvImaURHCk3V7CECmHLKKFBHXqPkf5hJTJKj4CRCSAUdYnJDCUgrs/GBgazHKIggSoF0254KoBuXdMNLSoakwF1HHDB3HsHwHV0Yvr0d/lsbEbapFPHrM7GipItd4r2fzD8fbk/HyK/XY+baRMxLLOIp7z/srkUVlxGwKBdDFpcieucxxOw5hWGrmuC3WpbpFMYwWwuan4Zp6w9jV82vNyb/Ii2svYbRe+oRva/ZjGxE0N0HseOGH+Ldz46N1sgPO9xk5nmMS5kA/Zmd/if+31A9HbnkNv7MDH9o7iXzvHk90tuHwPhl0DoJJL73BqS7PGHz1MvAqe/oCacZcqQMlG5geoPTgukOp1NUfx5MTzgtlN7gDBCcO3nc6cA5bOdRDNt+BD7bWhG8pQYxGwrw9v7+1ZaeMv98eqAYsd/tRPySfYhZsAsfJQz+C+5aUnYBMSsLEb6iAlHrmxG/sxcxtKL+a5oRsLYFI7a0IGJhAmZuS8fKkp+3Uu9fqW9rLiNuVxUCNpcRUrqdbCVFdHPJtKiENiCJnZB6HOG5F6GlGtoycEj2OfxfScfw57xLGEYwBajODSGYQ7XVH+PVoFyGPTwGZw6uIP78L1GwSyG08FIobxgphCBaBRPEQN5wAckn+hSYdNwRvYUUdIggUvIeRgLSpaADDpgh+470KZjJsKQapqcCd3cYBexqNwrc2Y7gnQSRR/8dnbScRzF0ByGlew/Y0ojITWWIX52CTYOUlsw/X6bUImpxAmJXpSNg/n6ELDyAD9JeLmbc3nAdr64vRtA3KRhPkz1h/wkMW16DoSsbMSnlPN1nM0auy8SIlUn48ODLQf9r6bv6Kxh/qBEh28oRvrceI9N78GrReYzIlDXpwLD9LRiW2AnfDFqZQsaLpTdoMS8aOIflXsCfCeKfmZFK2osyIPun4QtkXPtLpHhYCk5z5A6ku+QJLJj+hx0wAw45YLor6CBhlAik1cuCaaF0B9N/ZxstZgtCtzMEpAJ2dDDm7DJwDiWovlubEbK5CrGrs/BFsvfwz+mUHAaqCw4hZn0RIhgrDl2YgrA1eZi8JY///fwvedNbm3IRMGcvwlcVYCIbIC7hJIasrkXENiYfm2nC15dg1Po8vLW7GBsq//Xrk36Ovi4/julJDRi7rwqj9tdgfHIHJmb3GliDEtsw7GAb/FKPIST/Inzomn0Zm+o4LOMshqXw5qR802i1PEDyJm1w9SIFuOR5PijVkSeUWgsebNw1byCCaeVpMQMIouQNysD9/S58MDDdgfQGphS0owXhWvKzpQ5B5MCPSbNcuwAdzuw9ZGutme02bav3XMf8s7rsFPy/PYjgVcWIorkdurwEfqvK4Dc/FeMX96/p+Cm9vTMHoUsOIWpbhZmV8qf1NRhCdx+6oxvxzNpG8EtGLEnHpNVpWJH789Y7/xqaX95rdu4dvasccTvKMDKhCRNyjmNsPuO71B78cW8rfFK1IepZxpmMNdN4pBv1lZUioJ5AeVMAf/9F8nfJ87zcteRpJQ2ULlct9UFJWYtpwZQCEwgjZaGUAvb1x5QWyp8LpoFzG133lipEUxr69qdnFZxDqOE7+XtbGxCzuWjQrL3vRcTCTPgvLUbwhha8sqwKgZta4L+sFP5fHMLUZTnYXvJy+4V/UcCkiC78j2uyEHqom67hPOMNZrBbjyNqaydGrK/GqOVZmLIiGe+te3nwf02t7biLGYeaELulGHG76zA2vRejc88ihm7bJ7EHfrJOaUpMzjsZsgsUI1o3b1BaCeIXyc8lz/PWVff9HUoA9kHpspDuYBq5oLQWU/MwJU8obYIjKEP2diB4D+NHKmg3oXNT4K7WPsmNW/nvoNve1kAwKwhgBcIIYuD2VpOxGzh5DNyuWW5FiF91GKu97APa92Li6jyEL81HwKoq/G5hKanml1zfhvgNjB2+SMeo+enY9pKAfpBTg9CNyfDZkI+oxFMYvq2XFrQVods6MIJ32QhmanGrsjGSVnbqqkNY/wsno/4a+iyvF3GbChCyoQiRu+oQuqcJYQmdZi6j9h+NTGHidLgX4cz4QxN7EZLMeJWwOpaOltLoDHWOkNHaJtPaUpq46y7Nl/Tn7yl80JaBenyOf8oxowAlapQSNuvCzaI/QqkRsL7hRkoZuWJNPyZvkj+/kyRA/Wkx/QycBFHa12Xkv5cZtlG7ATNsT9tzcAZSAQLSTf5uYEpB2xsQtbncKHRLLWFsZtzZYcBUzVOWNZpJ6PgNWViQ+fwEor4Xn27LweuEadi3h+nWK+C3tRu/W9kCn3VdCCVYwUtKELUoDfNSXm5dyMamLozdlIihyw4x9qCb398A3+1VBJVfcn8rgvbyszdWMLMvROiKbMxK+Ndv5PD3aH39WXyc0YIZibWYvL8Wo3fXMHSpR3xCK+ITNabMm3FPC0ISjpjlJPHZVxCddYXQncfQA2cw5MAFDE28iuFJ1zHs0HUMP3iNCRcTK8rnwCXGttcQlHKR4HbDN7WD1lMiOMm0OIePICiJsSA9k26AMEIYQUUmnED0/lNGMfvOICKBCdehU/AhnH1K6nWUeNTI9+AR+CQwWdnfDt+9bfDZS4AoP8qfCtGCtV087mqhdWw2CuA5311NLrUY+ezk71F+hNKXCZCfkiBazggBurOBSVEj/DbVm0QoeHcngre3wG9tmVnxO2VbET4/0L92yKrvxcrDJRjx9RaMWp2P4LXV+PO6NvzXNfzS247DfxvN/Lp6+C/JQfzKVMzNePl5eWM2HkTQmkQErE9D+J4yBtl1eGV7LV7Z3eJYlP1HELq7FSFMxGRN52X96zYT+0doW9M5s+PazP3lGLUxm16hAOMP1JiHm76ZfwajkrX1eCuGMObSZIjItKuIz3uAsIy7BPIqhlDDEm/Sqt1GQNIdBPB1QMI1+O+7Ap99pwlnL+HsMmA6cB5xwclkhXCGHKSFTjiGiAMSQ6d9JxHtUoRrU1ZfunVJUPoecsD0M+qC74EOigkeDYaA9KUnkPz3NiJwT6MBM5zQ6RhMGAWnfx+Y/bJw9oPJz9hab1x72A7+Ht26z6Y6+G6uJ5hNCNlSj4DVxYhlEj5yeQq+TX6eqQFvwj5cjle3lCKGH/KHZdX4w3pmV9u78com/vEN1QhcWwDfuXsQ9s0mfJv08tPkNMgfvvQAgpYnIXp7OSL28Yvzgobt5JekZQkiqCFbqxGxoRBxqzMwc0cxttSd4696/7zfqra0XcHcoi68lViBsZuzEcHEL2JbqVn3PSXvPMYxRg1J6MZQZqpDd3UTjvN0z5cRmHwNIWm3EZ7uKCLthnnUc0iyHlZKuBgyWMk12/hRMaWtTwpQK+3YZnZto8zolyvOlJwEiCEbjULgASY3+zroxRgzakk4+8Rod4NZmhOyuxEROx1p7kQILWAQXwe45Mf3/eJ7WklZSyPBZ0VQ/bY0YPjGWgOozgVurEHQ6kKMWFeAqPn7sKHk+QrOgDdvLN+HuEWHEU1z+8fFZRiysQX/dV0t/vMKvuc5zT7SlKfg73YhfsF2vL/55ROaxZmNGLloP8KY/Y/aUYXYPc0YtrGSF0lQeeEjDh3BpIxuRG8txZB5exG3Nh1z8367hfuf0trKbjPzZuJeWocdRQjfUmiWvcQltGAk48fowz3G6mk6n8ayfffRsu09RmvZA9/9BElzI7U+J5Ex6SErWlKXZBEDGGP6W/gSu5ngWPGGp4Ip3Qx2lGdA3ZKxpBNPdhjPFcpYMYR9Ifcbur3eWDsBqZ085Jp11P8F72g0gAby/9RvVn78nQCJ4AW5yZ9uXK7cZwuN0SbHevoLzHUVCF1VgJHrcg0Xnu0nDXizs4JB94frCFAGfJcUw2d9I/7LylL851XF+MO6Mga01YjZRW3IQ9jCfQj5fD1m0WUfrO7mrw/8YG/aUdKNaatTELMoyZjzuJ0y+3UI453rv7MOf95YhoAdlRh5qBWx+2n+16Rjxq5crPwZD276LWpx7Sm8lVyNMTvyELctH/G7yhC9pwoRjFO1JjxKi8LoXoMY94UwuQrT+4NdprwTwLjRP+G8m5hIHTwDXyaavkx+fBhzqmJgY0ifQ0w2DjEco/wPErz9RxGxlzfBnm5HDC2ksF38O7vaaRW19KaF8DUjjBbOES0ljwbMbc2E0zmvYrpcsrJsqz5L6bKWAjJYrntjHYIIY8DWNsLpyH8LQ4dNjfBfX4Vg8hRNTxy56CCmr+/fH8ldz50Y891uhM9NRPDSAvisKsfvCOYrm6sxlBn2n9aUYMhKxqTrShC9rhgxy9IRO28fppD8TTkvv4D+XXZQ7OLDiFqTi5gd1QjYXMFsvpR/g6/38Q5mo/ox7vHZVIL4rYV4dVse3j9UiQ0NL79h/m9VC8q6MONAMcYyAR3J64rZWoCIrVqCXcYkpg6jk9swJr0To1M7Eae48sAJQnrOTcz0BezBU9QJ+NJND6e1HEbLOYxQDz3U6VI7hid2mkJ6+J4ehO3udrTzSJ9CmTVH7Ozk36YF3a5qSgtCaeWkML6Wwnnevtb/q5iurNu4aoKqoxNfEtQttKqbKUIZvJEhwEb+39Z2+GymCGbgFmb4G2ld15QhlLnNiLU5iF2wH4syvK9Fe+7EBgb3I+YfQNiidAxflI1hq0oZxNYa/YFW9N+WFGLoqgoEbahH1IY6M7Ye8tV+TCJsW/KP8iMGft5gmpfWjMkMEeI3FcN/RQ4CN1WYjRLCDx3Ff+EN8X+uLDHx0AgG67HbqhC5vhBjtxbjk4z/eV29p1ZV9eK9pHJM30dYtzKZ2pRNy5qLETsYe28vQiRDnEi53n3MyClNP5M0FS1IU9goX1pGnwNHMYxx5NCDR5lgOXqFVlTvA/by5/c4CtpN176rh26Zrn8XXb+bAncy2dregcBtjoK2M6PeNlD2//y2tVO0hoydzXErPR+BDthCcJmfCMqgDbSsG5g8bWrF8A1MmiiBG7yeBmh5DsKXp2HsmjTMOTz4GjSvJyctSUDUdwfh9+1hfhAt6IoSWtFK+DNj92McOnRtE/60vB5/WlqNSP7x2PU1CJuXgogvd+GrPc+XBAaTkp5xazIRvyEf4bTEvmuKjWsI2+cUgV+hC/jdykq6kjbEswNiGHiHrSnAuG0lWFD662ww9s/Sxtrj+CSl0oQxr27LwqQtGWbv/FF7ahDLzFkxegyzaa3JidxHd8wbN3R/l7mBVZt0IKX1ZIw5jNAOTeg1R3/GsgF7GZvuOeZoF8+55LvraN/Rd0e3mZjhs71f/jwXtM2R6t6q2ki+BNURLSLdteQvq7iZ1nETrSsZCVpPUAlnwHqCuYbWkrlL2IZahLKPQxanImpxIiaufHHO4vWkNH1tBkYtSUfI3DQEzS9AyPIqhK5rxvCVTfjdkgb8eW0nQvacxbB1dB+r6hC4sgwhS7MRvfAg3ticgX0tF/gx3j/bU3NzuzFxcwnC+Pshq0toMZoQuYMuiXdv6L6TCN7HpIHZre8m/v3V5QhgaBG1Lg9jNuZgbePL/53/mbSyqBVfpFZh2oEijN2dj7gtmYjenEUvQqvKECCSyVWwCtzMssN2tzBWdyyZJlf40joG7DtlQoDAg0yg9p/AcFrOYbScPnsZCrBNddRe7Hrtl3AS/gdO8/1xvEIo/7i1C3/aptlD/Jwdx5j8MOna3mvm7UrD+X9G7J8/rm1g4uy476CttLib2gilA2cI+yuCljRgeRmC6XVH0DvGrqZnWHoYnyX99FxfryelzUU9mL6ajfHNYYxYmI/IBcXwm0cXvJzWaysvdkMv/o8Ftfjd6jb8fnUDhq5hAMwMLGhlBiJXHMbM/cVYWfbyIz8LszoxiTHoyBX5GLOh2qz4DNvQgOGbOvCnjV14ZSMD/G1diGT8FLenE7Fb6xj3FiJ6VRre3FuC1dXH+THeP/t/du3oOY85eTWYvjsd4zclYfzWLEzeU4xXE2oZWhWZIcK4nc1OuzDpiSKcwYTHd0uHmZ42bKcmXHRgOJOgYXTfQ3b1679ubMS/bW7G7wmX1R/ouv/Mz/Dbc8KAGbzdUSBhlfx3HDF9Ifc+MvUCYhJO8O/R3a+nZ11Ti0CyEMrXEevrGLbVIo65yojVBRixPANxCxPwNuNtz2v0Jq8nrTbmdSNi9kaMpfWcsroCY1fV0CRXYtiiagxZ3oRX1vBiVrXg31bW4ZV1dbx7KDbWsCWH+DN7EbV8J5YXVfCjvH++N324rRRBH29D+JxETKCLj2QjBGw5Ap8NrfChiwjYqDuyEaEba50d8RanIVgb365OwXuJVdjS9NtYAfrP1O6OM5ibXYdZewowcUMmRq/NRuzKHEQuz0cEQ7DoNTWIo8WKozWNpusPZ+YfwaQp7ADjyz3ttKwtdOGOTKmJUmXAfy+tIbP3oUyCVIvVUGOA1ogxoQkh6Bp+lgK3tpjYUsmNz7pKerMyM1EoeG2VqZFrCXkMz+v7jF9fjNCv9iDw4414izG157W8SF5Pumt3ySlMX5QC3xmrEfnlQcaI/KOMA32XlMOPJl3ADFlbh2EbauiG65jVVSBwfQ78GeyGrzmAwDmL8fGBnzfBQ1Z79iZmst8l8W8xKeCdHbu9HVFbmszoVRC/QwzBHbu3DZP2t2LC7jqM2lTI5CwNY9amG0jXNfyv6e4H0/qqc5iT2oZZOyoxeW0hxiyn+1yRx3Arh6FYFoavyGaf5DMTL0f07noTxyqG1UZtpsC+jaHZ5mr4byRsG9i/65ljrK+CP8O1oFXlps2D6RmloLWl8F9VBD+GVxEb2T8btckG3TaP8WuLEMO/Fb4gBWHf7EPs17vwwbY87Ko+za/p/bsPJq8nPXWw5gxmLjuIMd/uRgz/oM8nOzDsq4OIZHw4jBn9kMXM6hkv+q3KNSswA+lqg6modcmIX7kbY5dvwcf7vdeyXiTtaBe3IAlxbOgxa5nRbqrE+C01GEeNoZUeTUWuKTJ72I/ZVmlGG8KWJJtC/5j1WZi5uwBb61++gvC/knZXn8TKwi58ntGMmQfYbpsYMq1JNxO/R67LRtTKVAQtSETMKsayDN/0sIDY9aqeFJrFZ+N3VmHCrmqM3lSCsZtLMY5Qj6fhGb+9zCSkqpyM2VKA0CUHEb40AZGL9zP0243YhXsxafVhvMuwY25KLbaX/fL293pyMO2pPYaZa5NM8T3oq62IWHSIXyyF1i0D0WvzzMXF0sVo9WUMFbeG/7eYQC/ciqivlmP8t8uwLjmDH+X9871pe/UpfH2oAZOXJSP6672InX8Y8UszELEwHT5fH0QcoR1B9z56SxUbtAaTdlabBlQFYMSqZMTMW4fZW/dhS9lva7nyr6UdDeextrwX32U04dOEcszamovpmzIxhZ5uwsrDGLssEfFLDiBm/j5Ezt2N6O8Yns13FL1gHxPePYhZxD5dtBMxi7djzMp9mL41GZ8eLML8rBqsL2vnn/H+t3+e8J/+f5Rskvv0z2SgAAAAAElFTkSuQmCC" data-filename="medways_log.png" style="width: 58.1923px; height: 34px;">&nbsp;
                                                                <span style="font-weight: 700;"><span style="font-size: 14px;"><span style="font-size: 18px;">MEDWAYS&nbsp;</span></span></span>
                                                                <span style="font-size: 14px; font-weight: bold;">INTERNATIONAL</span></p>
                                                                <p style="color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                                                <p style="color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">
                                                                    <span style="font-weight: 700;">
                                                                        <span style="font-size: 14px;">Tunis le <?php echo date("d/m/Y"); ?></span>
                                                                    </span>
                                                                </p>
                                                                <p style="color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; text-align: center;">
                                                                    <span style="font-size: 24px;">
                                                                        <span style="font-weight: 700;"><u>Rapport de prospection</u></span>
                                                                    </span>
                                                                </p>
                                                                <ul>
                                                                    <li style="text-align: left; color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">
                                                                        <span style="font-size: 24px;" id="clname">Client :&nbsp;</span>
                                                                    </li>
                                                                    <li style="text-align: left; color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;">
                                                                        <span style="font-size: 24px;">Sujet&nbsp; :&nbsp;</span>
                                                                    </li>
                                                                </ul>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <button type="button" id="save_pros" class="btn btn-primary btn-labeled"><b><i class="icon-floppy-disk"></i></b> Enregistrer </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Recherche</h6>
                                </div>

                                <div class="panel-body">
                                    
                                    <table class="table datatable-button-print-rows" id="table">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Évaluation</th>
                                                <th>Client</th>
                                                <th>Responsable</th>
                                                <th>Date</th>
                                                <th>dernière mise à jour</th>
                                                <th>Propriétaire</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- view modal -->
					<div id="modal_view" class="modal fade">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<div class="modal-body">
                                    <div class="summernote" id="summernote_view"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /view modal -->

                    <!-- edit modal -->
					<div id="modal_edit" class="modal fade">
						<div class="modal-dialog modal-full">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>

								<div class="modal-body">
                                    <div class="summernote" id="summernote_edit"></div>
								</div>

								<div class="modal-footer">
									<button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
									<button type="button" id="save_edit" class="btn btn-primary">Sauvegarder les modifications</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /edit modal -->

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>