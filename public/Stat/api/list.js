$(document).ready(function() {
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "|*|-------------------->{ Aucune donnée disponible }<--------------------|*|",
        }
    });
    $.post('/GTRANS/public/Stat/api/list.php', JSON.stringify(['GET_COLUMN', 'dossier_maritime'])).done(function(data) {
        data.forEach(function(element) {
            $("#dosm_col").append('<label class="checkbox-inline"><input type="checkbox" id="' + element[0].ID + '" class="styled doscls">' + element[1].VAL + '</label>')
        }, this);
    }).fail(function(errors) {
        console.log('error : ', errors)
    }).always(function() {
        // Default initialization
        $.post('/GTRANS/public/Stat/api/list.php', JSON.stringify(['GET_COLUMN', 'avis_arrive_mig'])).done(function(data) {
            data.forEach(function(element) {
                $("#avis_col").append('<label class="checkbox-inline"><input type="checkbox" id="' + element[0].ID + '" class="styled avicls">' + element[1].VAL + '</label>')
            }, this);
        }).fail(function(errors) {
            console.log('error : ', errors)
        }).always(function() {
            // Default initialization
            $(".styled, .multiselect-container input").uniform({
                radioClass: 'choice'
            });
            $('input:checkbox').change(function() {
                var name = $(this).parents().eq(2).text();
                var id = $(this).attr('id');
                var ex = false;
                $("#SColumns_TAB li").each(function(n) {
                    if ($(this).attr("id") === id) {
                        $(this).remove();
                        ex = true;
                    }
                });
                if (!ex) {
                    $("#SColumns_TAB").append('<li id="' + id + '">' + name + '</li>');
                }
                $('#SColumns_TAB').sortable('refresh');
            });

        });
    });

    $('#appliq').on('click', () => {
        $('#appliq').addClass('disabled');
        var sel = $("ul.nav-tabs li.active a").attr('href');
        switch (sel) {
            case '#tab_dos':
                $.post('/GTRANS/public/Stat/api/list.php', JSON.stringify(['GET_FILTER', 'dossier_maritime'])).done((data) => {
                    $('#builder').queryBuilder({
                        plugins: [
                            'sortable',
                            'filter-description',
                            'unique-filter',
                            'bt-tooltip-errors',
                            'bt-selectpicker',
                            'bt-checkbox',
                            'invert',
                            'not-group'
                        ],
                        filters: data
                    });
                }).always(() => {
                    $('#btn-get,#btn-reset,#title_print,#message_print,#SColumns_TAB,#dtab').css('visibility', 'visible');
                    $('#SColumns_TAB').empty();
                    $('#SColumns_TAB').sortable('refresh');
                    $('#navy li').not('.active').addClass('disabled');
                    $('#navy li').not('.active').find('a').removeAttr("data-toggle");
                });
                break;

            default:
                $.post('/GTRANS/public/Stat/api/list.php', JSON.stringify(['GET_FILTER', 'avis_arrive_mig'])).done((data) => {
                    $('#builder').queryBuilder({
                        plugins: [
                            'sortable',
                            'filter-description',
                            'unique-filter',
                            'bt-tooltip-errors',
                            'bt-selectpicker',
                            'bt-checkbox',
                            'invert',
                            'not-group'
                        ],
                        filters: data
                    });
                }).always(() => {
                    $('#btn-get,#btn-reset,#title_print,#message_print,#SColumns_TAB,#dtab').css('visibility', 'visible');
                    $('#SColumns_TAB').empty();
                    $('#SColumns_TAB').sortable('refresh');
                    $('#navy li').not('.active').addClass('disabled');
                    $('#navy li').not('.active').find('a').removeAttr("data-toggle");
                });
                break;
        }
    });

    $('#del').on('click', () => {
        $('#appliq').removeClass('disabled');
        $('#navy li.active').next('li').removeClass('disabled');
        $('#navy li.active').next('li').find('a').attr("data-toggle", "tab");
        $('#navy li.active').prev('li').removeClass('disabled');
        $('#navy li.active').prev('li').find('a').attr("data-toggle", "tab");
        $('#btn-get,#btn-reset,#title_print,#message_print,#SColumns_TAB,#dtab').css('visibility', 'hidden');
        $('#builder').queryBuilder('destroy');
        $('#SColumns_TAB').empty();
        $('#SColumns_TAB').sortable('refresh');
        $('#table').DataTable().clear().destroy();
    });

    $('#btn-get').on('click', function() {
        var result = $('#builder').queryBuilder('getSQL');
        var sel = $("ul.nav-tabs li.active a").attr('href');
        switch (sel) {
            case '#tab_dos':
                var count = $('input.doscls:checkbox:checked').length;
                if (!count) {
                    break;
                }
                var selected = '';
                var itemsProcessed = 0;
                var object = $('#SColumns_TAB').sortable("toArray");
                object.forEach(function(item, index, array) {
                    selected += item + ',';
                    itemsProcessed++;
                    if (itemsProcessed === array.length) {
                        //console.log('SELECT ' + selected.slice(0, -1) + ' FROM dossier_maritime WHERE ' + result.sql + '<br>');
                        datatable_gen('SELECT ' + selected.slice(0, -1) + ' FROM dossier_maritime WHERE ' + result.sql);
                    }
                }, this);
                break;

            default:

                var count = $('input.avicls:checkbox:checked').length;
                if (!count) {
                    break;
                }
                var selected = '';
                var itemsProcessed = 0;
                var object = $('#SColumns_TAB').sortable("toArray");
                object.forEach(function(item, index, array) {
                    selected += item + ',';
                    itemsProcessed++;
                    if (itemsProcessed === array.length) {
                        //console.log('SELECT ' + selected.slice(0, -1) + ' FROM avis_arrive_mig WHERE ' + result.sql + '<br>');
                        datatable_gen('SELECT ' + selected.slice(0, -1) + ' FROM avis_arrive_mig WHERE ' + result.sql);
                    }
                }, this);
                break;
        }
    });

    $('#btn-reset').on('click', function() {
        $('#builder').queryBuilder('reset');
    });

    $("#SColumns_TAB").sortable({
        connectWith: ".selectable"
    }).disableSelection();

    function datatable_gen(sql) {
        $.post('/GTRANS/public/Stat/api/list.php', JSON.stringify(['GET_TAB_COLUMNS', sql])).done((data) => {
            $('#table').DataTable().clear().destroy();
            $('#table').empty();
            table = $('#table').DataTable({
                aaSorting: [],
                buttons: {
                    buttons: [{
                            extend: 'print',
                            className: 'btn bg-blue btn-icon',
                            text: '<i class="icon-printer position-left"></i>',
                            title: $('#title_print').val(),
                            message: $('#message_print').val(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(win) {
                                // Style the body..
                                $(win.document.body)
                                    .addClass('asset-print-body')
                                    .css({ 'text-align': 'center' });

                                /* Style for the table */
                                $(win.document.body)
                                    .find('table')
                                    .addClass('compact minimalistBlack')
                                    .css({
                                        margin: '5px 5px auto'
                                    });
                            },
                        },
                        {
                            extend: 'excelHtml5',
                            className: 'btn bg-success btn-icon',
                            text: '<i class="icon-file-excel position-left"></i>',
                            title: $('#title_print').val(),
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            className: 'btn bg-danger btn-icon',
                            title: $('#title_print').val(),
                            text: '<i class="icon-file-pdf position-left"></i>',
                            message: $('#message_print').val(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.styles.title = {
                                    fontSize: '20',
                                    alignment: 'center'
                                };
                                doc.styles.tableHeader = {
                                    bold: true,
                                    fontSize: 11,
                                    color: "white",
                                    fillColor: "#2d4154",
                                    border: "3px solid #000000",
                                    width: "100%",
                                    "text-align": "left",
                                    "border-collapse": "collapse",

                                };
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                            className: 'btn bg-default btn-icon'
                        }
                    ],
                },
                bDestroy: true,
                select: false,
                destroy: true,
                scrollY: true,
                scrollX: true,
                ajax: {
                    url: "/GTRANS/public/Stat/api/list.php",
                    type: "POST",
                    data: function(d) {
                        return JSON.stringify({
                            "0": "GET_TAB_DATA",
                            "1": sql
                        });
                    }
                },
                columns: data
            });
        }).fail((errors) => {
            console.log('error : ', errors);
        }).always((data) => {

        });
    }
});