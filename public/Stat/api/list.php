<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {
    case 'GET_COLUMN':
        $database = new MySQL();
        $fields = $database->list_fields( "SELECT * FROM {$response[1]}");
        $Columns = [];
        foreach ($fields as $key => $value) {
            $rs2 = Dictio($value->name);
            if (!empty($rs2)) {
                array_push($Columns, [['ID' => $value->name],['VAL' => $rs2]]);
            }
        }
        echo json_encode($Columns);
        break;

    case 'GET_FILTER':
        $database = new MySQL();
        $fields = $database->list_fields( "SELECT * FROM {$response[1]}");
        $Filter_out = [];
        foreach ($fields as $key => $value) {
            $rs1 = filter_generator($value->name);
            if (count($rs1)>1) {
                array_push($Filter_out, $rs1);
            }
        }
        echo json_encode($Filter_out);
        break;

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results($response[1]);
        echo json_encode($GET_TAB_DATA);
        break;

    case 'GET_TAB_COLUMNS':
        $database = new MySQL();
        $fields = $database->list_fields($response[1]);
        $Filter_out = [];
        $Columns = [];
        foreach ($fields as $key => $value) {
            $rs2 = Dictio($value->name);
            if (!empty($rs2)) {
                array_push($Columns, array('data' => $value->name,'title' => $rs2 ));
            }
        }
        echo json_encode($Columns);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}

function Dictio($var = null)
{
    switch ($var) {
        // Dossier
        case 'DM_NUM_DOSSIER' : 
        case 'AAM_NUM_DOSSIER':
            return 'Numero Dossier';
        case 'DM_NUM_BL':
            return 'BL';
        case 'DM_IMP_EXP' : 
        case 'AAM_IMP_EXP':
            return 'Import/Export';
        case 'DM_DATE_EMBARQ' : 
        case 'AAM_DATE_DEBARQ':
            return 'Date Embarquement';
        case 'DM_DATE_DECHARG':
            return 'Date Décharge';
        case 'DM_FOURNISSEUR' : 
        case 'AAM_CLE_FOURNISSSEUR':
            return 'Fournisseur';
        case 'DM_CLIENT' : 
        case 'AAM_CLE_CLIENT':
            return 'Client';
        case 'DM_NAVIRE' : 
        case 'AAM_NAVIRE':
            return 'Navire';
        case 'DM_POL' : 
        case 'AAM_PORT_EMBARQ':
            return 'Port de chargement';
        case 'DM_POD' : 
        case 'AAM_PORT_DECHAG':
            return 'Port de déchargement';
        case 'DM_CODE_COMP_GROUP' : 
        case 'AAM_G_C':
            return 'Complet/Groupage';
        case 'DM_MARCHANDISE' : 
        case 'AAM_MARCHANDISE':
            return 'Marchandise';
        case 'DM_POIDS' : 
        case 'AAM_POIDS':
            return 'Poids';
        case 'DM_NOMBRE':
            return 'Nombre';
        case 'DM_MARQUE' : 
        case 'AAM_MARQUE':
            return 'Marque';
        case 'DM_TERME':
            return 'Terme';
        case 'DM_LONGUEUR':
            return 'Longueur';
        case 'DM_LARGEUR':
            return 'Largeur';
        case 'DM_HAUTEUR':
            return 'Hauteur';
        case 'DM_ESCALE' : 
        case 'AAM_ESCALE':
            return 'Escale';
        case 'DM_RUBRIQUE' : 
        case 'AAM_RUBRIQUE':
            return 'Rubrique';
        case 'DM_NUM_LTA' : 
        case 'AAM_NUM_LTA':
            return 'LTA';
        case 'DM_MAGASIN':
            return 'Magasin';
        case 'DM_ATTRIBUER':
            return 'Attribuer';
        case 'DM_CLOTURE':
            return 'Cloturer';
        // Avis D'arrivée et Facture
        case 'AAM_NUMERO':
            return 'Numéro Avis d\'arrivée';
        case 'AAM_DATE':
            return 'Date Avis d\'arrivée';
        case 'AAM_ADR_CLIENT':
            return 'Adresse Client';
        case 'AAM_NBR_COLIS':
            return 'Nombre de Colis';
        case 'AAM_TOT_NON_TAXABLE':
            return 'Total non Taxable';
        case 'AAM_TO_TAXABLE':
            return 'Total Taxable';
        case 'AAM_TVA_PORCENTAGE':
            return 'Pourcentage TVA';
        case 'AAM_TIMBRE':
            return 'Timbre';
        case 'AAM_TVA_VAL':
            return 'Valeur TVA';
        case 'AAM_TOTAL_TTC':
            return 'Total TTC';
        case 'AAM_NUM_FACTURE':
            return 'Numéro Facture';
        case 'AAM_DATE_FACTURE':
            return 'Date Facture';
        case 'AAM_AEROPOR_EMBARQ':
            return 'Aéroport Embarquement';
        case 'AAM_AEROPOR_DECHAG':
            return 'Aéroport Décharge';
        case 'AAM_TYPE_FACT':
            return 'Type Facture';
        case 'AAM_BON_COMMAND':
            return 'Bon de Commande';
        case 'AAM_NUM_CHEQUE':
            return 'N° Cheque';
        case 'AAM_BANQUE':
            return 'Banque';
        case 'AAM_DATE_REG':
            return 'Date de Règlement';
        default:
            return '';
    }
}

function filter_generator($var = null)
{
    switch ($var) {
        // Dossier
        case 'DM_NUM_DOSSIER' : 
            $ar=[];
            $ar['id'] = 'DM_NUM_DOSSIER';
            $ar['label'] = 'Numero Dossier';
            $ar['type'] = 'integer';
            return $ar;
        case 'AAM_NUM_DOSSIER':
            $ar=[];
            $ar['id'] = 'AAM_NUM_DOSSIER';
            $ar['label'] = 'Numero Dossier';
            $ar['type'] = 'integer';
            return $ar;
        case 'DM_NUM_BL':
            $ar=[];
            $ar['id'] = 'DM_NUM_BL';
            $ar['label'] = 'BL';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_IMP_EXP' : 
            $ar=[];
            $ar['id'] = 'DM_IMP_EXP';
            $ar['label'] = 'Import/Export';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'is_null', 'is_not_null'];
            $ar['values'] = ['IMPORT' => 'IMPORT', 'EXPORT' => 'EXPORT'];
            return $ar;
        case 'AAM_IMP_EXP':
            $ar=[];
            $ar['id'] = 'DM_IMP_EXP';
            $ar['label'] = 'Import/Export';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'is_null', 'is_not_null'];
            $ar['values'] = ['IMPORT' => 'IMPORT', 'EXPORT' => 'EXPORT'];
            return $ar;
        case 'DM_DATE_EMBARQ' : 
            $ar=[];
            $ar['id'] = 'DM_DATE_EMBARQ';
            $ar['label'] = 'Date Embarquement';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        case 'AAM_DATE_DEBARQ':
            $ar=[];
            $ar['id'] = 'AAM_DATE_DEBARQ';
            $ar['label'] = 'Date Embarquement';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        case 'DM_DATE_DECHARG':
            $ar=[];
            $ar['id'] = 'DM_DATE_DECHARG';
            $ar['label'] = 'Date Décharge';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        case 'DM_FOURNISSEUR' : 
            $ar=[];
            $ar['id'] = 'DM_FOURNISSEUR';
            $ar['label'] = 'Fournisseur';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Fournisseur();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_CLE_FOURNISSSEUR':
            $ar=[];
            $ar['id'] = 'AAM_CLE_FOURNISSSEUR';
            $ar['label'] = 'Fournisseur';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Fournisseur();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_CLIENT' : 
            $ar=[];
            $ar['id'] = 'DM_CLIENT';
            $ar['label'] = 'Client';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_client();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_CLE_CLIENT':
            $ar=[];
            $ar['id'] = 'AAM_CLE_CLIENT';
            $ar['label'] = 'Client';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_client();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_NAVIRE' : 
            $ar=[];
            $ar['id'] = 'DM_NAVIRE';
            $ar['label'] = 'Navire';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Navire();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_NAVIRE':
            $ar=[];
            $ar['id'] = 'AAM_NAVIRE';
            $ar['label'] = 'Navire';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Navire();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_POL' : 
            $ar=[];
            $ar['id'] = 'DM_POL';
            $ar['label'] = 'Port de chargement';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Port();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_PORT_EMBARQ':
            $ar=[];
            $ar['id'] = 'AAM_PORT_EMBARQ';
            $ar['label'] = 'Port de chargement';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Port();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_POD' : 
            $ar=[];
            $ar['id'] = 'DM_POD';
            $ar['label'] = 'Port de déchargement';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Port();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_PORT_DECHAG':
            $ar=[];
            $ar['id'] = 'AAM_PORT_DECHAG';
            $ar['label'] = 'Port de déchargement';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_Port();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_CODE_COMP_GROUP' : 
            $ar=[];
            $ar['id'] = 'DM_CODE_COMP_GROUP';
            $ar['label'] = 'Complet/Groupage';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'is_null', 'is_not_null'];
            $ar['values'] = ['C' => 'COMPLET', 'G' => 'GROUPAGE'];
            return $ar;
        case 'AAM_G_C':
            $ar=[];
            $ar['id'] = 'DM_CODE_COMP_GROUP';
            $ar['label'] = 'Complet/Groupage';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'is_null', 'is_not_null'];
            $ar['values'] = ['C' => 'COMPLET', 'G' => 'GROUPAGE'];
            return $ar;
        case 'DM_MARCHANDISE' : 
            $ar=[];
            $ar['id'] = 'DM_MARCHANDISE';
            $ar['label'] = 'Marchandise';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_MARCHANDISE':
            $ar=[];
            $ar['id'] = 'AAM_MARCHANDISE';
            $ar['label'] = 'Marchandise';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_POIDS' : 
            $ar=[];
            $ar['id'] = 'DM_POIDS';
            $ar['label'] = 'Poids';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_POIDS':
            $ar=[];
            $ar['id'] = 'AAM_POIDS';
            $ar['label'] = 'Poids';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_NOMBRE':
            $ar=[];
            $ar['id'] = 'DM_NOMBRE';
            $ar['label'] = 'Nombre';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_MARQUE' : 
            $ar=[];
            $ar['id'] = 'DM_MARQUE';
            $ar['label'] = 'Marque';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_MARQUE':
            $ar=[];
            $ar['id'] = 'AAM_MARQUE';
            $ar['label'] = 'Marque';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_TERME':
            $ar=[];
            $ar['id'] = 'DM_TERME';
            $ar['label'] = 'Terme';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_LONGUEUR':
            $ar=[];
            $ar['id'] = 'DM_LONGUEUR';
            $ar['label'] = 'Longueur';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_LARGEUR':
            $ar=[];
            $ar['id'] = 'DM_LARGEUR';
            $ar['label'] = 'Largeur';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_HAUTEUR':
            $ar=[];
            $ar['id'] = 'DM_HAUTEUR';
            $ar['label'] = 'Hauteur';
            $ar['type'] = 'integer';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_ESCALE' : 
            $ar=[];
            $ar['id'] = 'DM_ESCALE';
            $ar['label'] = 'Escale';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_ESCALE':
            $ar=[];
            $ar['id'] = 'AAM_ESCALE';
            $ar['label'] = 'Escale';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_RUBRIQUE' : 
            $ar=[];
            $ar['id'] = 'DM_RUBRIQUE';
            $ar['label'] = 'Rubrique';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_RUBRIQUE':
            $ar=[];
            $ar['id'] = 'AAM_RUBRIQUE';
            $ar['label'] = 'Rubrique';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_NUM_LTA' : 
            $ar=[];
            $ar['id'] = 'DM_NUM_LTA';
            $ar['label'] = 'LTA';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_NUM_LTA':
            $ar=[];
            $ar['id'] = 'AAM_NUM_LTA';
            $ar['label'] = 'LTA';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_MAGASIN':
            $ar=[];
            $ar['id'] = 'DM_MAGASIN';
            $ar['label'] = 'Magasin';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_ATTRIBUER':
            $ar=[];
            $ar['id'] = 'DM_ATTRIBUER';
            $ar['label'] = 'Attribuer';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'DM_CLOTURE':
            $ar=[];
            $ar['id'] = 'DM_CLOTURE';
            $ar['label'] = 'Cloturer';
            $ar['type'] = 'integer';
            $ar['input'] = 'radio';
            $ar['operators'] = ['equal', 'is_null', 'is_not_null'];
            $ar['values'] = [0 => 'Non', 1 => 'Oui'];
            return $ar;
        // Avis D'arrivée et Facture
        case 'AAM_NUMERO':
            $ar=[];
            $ar['id'] = 'AAM_NUMERO';
            $ar['label'] = 'Numéro Avis d\'arrivée';
            $ar['type'] = 'string';
            $ar['operators'] = ['equal', 'not_equal', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_DATE':
            $ar=[];
            $ar['id'] = 'AAM_DATE';
            $ar['label'] = 'Date Avis d\'arrivée';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        case 'AAM_NBR_COLIS':
            $ar=[];
            $ar['id'] = 'AAM_NBR_COLIS';
            $ar['label'] = 'Nombre de Colis';
            $ar['type'] = 'integer';
            return $ar;
        case 'AAM_TOT_NON_TAXABLE':
            $ar=[];
            $ar['id'] = 'AAM_TOT_NON_TAXABLE';
            $ar['label'] = 'Total non Taxable';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_TO_TAXABLE':
            $ar=[];
            $ar['id'] = 'AAM_TO_TAXABLE';
            $ar['label'] = 'Total Taxable';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_TVA_PORCENTAGE':
            $ar=[];
            $ar['id'] = 'AAM_TVA_PORCENTAGE';
            $ar['label'] = 'Pourcentage TVA';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_TIMBRE':
            $ar=[];
            $ar['id'] = 'AAM_TIMBRE';
            $ar['label'] = 'Timbre';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_TVA_VAL':
            $ar=[];
            $ar['id'] = 'AAM_TVA_VAL';
            $ar['label'] = 'Valeur TVA';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_TOTAL_TTC':
            $ar=[];
            $ar['id'] = 'AAM_TOTAL_TTC';
            $ar['label'] = 'Total TTC';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_NUM_FACTURE':
            $ar=[];
            $ar['id'] = 'AAM_NUM_FACTURE';
            $ar['label'] = 'Numéro Facture';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_DATE_FACTURE':
            $ar=[];
            $ar['id'] = 'AAM_DATE_FACTURE';
            $ar['label'] = 'Date Facture';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        case 'AAM_AEROPOR_EMBARQ':
            $ar=[];
            $ar['id'] = 'AAM_AEROPOR_EMBARQ';
            $ar['label'] = 'Aéroport Embarquement';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_aeroort();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_AEROPOR_DECHAG':
            $ar=[];
            $ar['id'] = 'AAM_AEROPOR_DECHAG';
            $ar['label'] = 'Aéroport Décharge';
            $ar['type'] = 'integer';
            $ar['input'] = 'select';
            $ar['values'] = get_aeroort();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_TYPE_FACT':
            $ar=[];
            $ar['id'] = 'AAM_TYPE_FACT';
            $ar['label'] = 'Type Facture';
            $ar['type'] = 'string';
            $ar['input'] = 'select';
            $ar['values'] = get_type_fact();
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_BON_COMMAND':
            $ar=[];
            $ar['id'] = 'AAM_BON_COMMAND';
            $ar['label'] = 'Bon de Commande';
            $ar['type'] = 'string';
            $ar['input'] = 'radio';
            $ar['values'] = ['BC' => 'BC', 'CHEQUE' => 'CHEQUE', 'TRAITE' => 'TRAITE', 'ESPECE' => 'ESPECE', 'VIREMENT' => 'VIREMENT'];
            $ar['operators'] = ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null'];
            return $ar;
        case 'AAM_NUM_CHEQUE':
            $ar=[];
            $ar['id'] = 'AAM_NUM_CHEQUE';
            $ar['label'] = 'N° Cheque';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_BANQUE':
            $ar=[];
            $ar['id'] = 'AAM_BANQUE';
            $ar['label'] = 'Banque';
            $ar['type'] = 'string';
            return $ar;
        case 'AAM_DATE_REG':
            $ar=[];
            $ar['id'] = 'AAM_DATE_REG';
            $ar['label'] = 'Date de Règlement';
            $ar['type'] = 'date';
            $ar['validation'] = ['format' => 'YYYY/MM/DD'];
            $ar['plugin'] = 'datepicker';
            $ar['plugin_config'] = ['format' => 'yyyy/mm/dd', 'todayBtn' => 'linked', 'todayHighlight' => true, 'autoclose' => true];
            return $ar;
        default:
            return '';
    }
}

function get_client($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT CL_CODE,CL_LIBELLE FROM client LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['CL_CODE']]=$value['CL_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT CL_LIBELLE FROM client WHERE CL_CODE = {$var}");
    }
    
}

function get_Fournisseur($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT FR_CODE,FR_LIBELLE FROM fournisseur LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['FR_CODE']]=$value['FR_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT FR_LIBELLE FROM fournisseur WHERE FR_CODE = {$var}");
    }
    
}

function get_Navire($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT NA_CODE,NA_LIBELLE FROM navire LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['NA_CODE']]=$value['NA_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT NA_LIBELLE FROM navire WHERE NA_CODE = {$var}");
    }
    
}

function get_Port($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT PO_CODE,PO_LIBELLE FROM port LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['PO_CODE']]=$value['PO_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT PO_LIBELLE FROM port WHERE PO_CODE = {$var}");
    }
    
}

function get_aeroort($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT AE_CODE,AE_LIBELLE FROM aeroport LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['AE_CODE']]=$value['AE_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT AE_LIBELLE FROM aeroport WHERE AE_CODE = {$var}");
    }
    
}

function get_type_fact($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT FT_CODE,FT_LIBELLE FROM type_facture LIMIT 10");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['FT_CODE']]=$value['FT_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT FT_LIBELLE FROM type_facture WHERE FT_CODE = {$var}");
    }
    
}
?>