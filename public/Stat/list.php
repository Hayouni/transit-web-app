<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>


<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|Liste</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../../assets/query-builder/css/query-builder.default.css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <script src="../../assets/js/QueryBuilder/js/query-builder.standalone.min.js"></script>
    <script src="../../assets/js/QueryBuilder/i18n/query-builder.fr.js"></script>
    <script src="../../assets/js/QueryBuilder/js/sql-parser.js"></script>
    <script src="../../assets/js/QueryBuilder/js/interact.min.js"></script>
    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/switch.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/touch.min.js"></script>


    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/maps/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/maps/jvectormap/map_files/world.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="./api/list.js"></script>
    <!-- /theme JS files -->

    <!-- Chart code -->

    <style>
        th.hide_me,
        td.hide_me {
            display: none;
        }
        
        .query-builder .rules-group-container {
            border: 1px solid #dddddd !important;
            background: rgb(255, 255, 255) !important;
        }
        
        .query-builder .rules-list>*::before,
        .query-builder .rules-list>*::after {
            border-color: #000 !important;
        }
        
        .checkbox-inline,
        .radio-inline {
            position: relative;
            padding-left: 23px;
            margin-left: 15px;
            padding-bottom: 7px;
        }
        
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }
        
        .selectable-demo-list>.ui-sortable-handle {
            padding: 6px 12px;
            background-color: #fcfcfc;
            border: 1px solid #000;
            border-radius: 0px;
            cursor: pointer;
            color: #777;
            font-size: 13px;
            /* margin-left: 6px; */
            margin: 6px;
        }
    </style>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Liste</h4>
                        </div>
                    </div>
                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">


                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title"></h6>
                                    <div class="heading-elements">
                                        <a class="btn btn-primary btn-xs" id="appliq"><i class="icon-checkmark3"></i> Appliquer le choix</a>
                                        <a class="btn btn-warning btn-xs" id="del"><i class="icon-eraser2"></i> Efaccer</a>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="tabbable nav-tabs-vertical nav-tabs-left">
                                        <ul class="nav nav-tabs nav-tabs-highlight" id="navy">
                                            <li class="active"><a href="#tab_dos" data-toggle="tab"><i class="icon-menu7 position-left"></i> Dossier</a></li>
                                            <li><a href="#tab_avis" data-toggle="tab"><i class="icon-mention position-left"></i> Avis d'arriv� | Facture</a></li>
                                        </ul>

                                        <div class="tab-content">
                                            <div class="tab-pane active has-padding" id="tab_dos">
                                                <div class="form-group theClass" id="dosm_col">
                                                    <label class="display-block text-semibold">Les details a afficher</label>
                                                </div>
                                            </div>

                                            <div class="tab-pane has-padding" id="tab_avis">
                                                <div class="form-group" id="avis_col">
                                                    <label class="display-block text-semibold">Les details a afficher</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <ul class="list-inline selectable selectable-demo-list" id="SColumns_TAB" style="visibility: hidden; width: 100%; border-color: #2196f3; border-width: 2px; border-style: solid;">
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="builder"></div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <button class="btn btn-primary" id="btn-get" style="visibility: hidden;">Obtenir des donn�es</button>
                                                </div>
                                                <div class="col-md-2">
                                                    <button class="btn btn-warning" id="btn-reset" style="visibility: hidden;">R�initialiser le filtre</button>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="title_print" style="visibility: hidden;" value="Titre : liste des ...">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" id="message_print" style="visibility: hidden;" value="titre optionel ...">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="visibility: hidden;" id="dtab">
                                        <div class="col-md-12">
                                            <div class="container-fluid">
                                                <table id="table" class="table display datatable-button-print-rows table-striped table-bordered" cellspacing="0" style="width:100%;">
                                                    <thead></thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
                <!-- /content area -->

            </div>
        </div>
    </div>
    <!-- /Page container -->
    <script>
    </script>


</body>

</html>

</html>