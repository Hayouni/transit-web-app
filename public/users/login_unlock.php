<?php
session_start();
$dn = $_SESSION['username'];
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
$sql = "UPDATE users SET lock_account= '1',disconnect=now() WHERE username='$dn' LIMIT 1";
$query = mysqli_query($db_conx, $sql);
?><?php
// AJAX CALLS THIS LOGIN CODE TO EXECUTE
if(isset($_POST["p"])){
	// CONNECT TO THE DATABASE
	include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
	// GATHER THE POSTED DATA INTO LOCAL VARIABLES AND SANITIZE
    $e = $_SESSION['username'];
	$p = $_POST['p'];//md5($_POST['p']);
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
	// FORM DATA ERROR HANDLING
	if($e == "" || $p == ""){
		echo "login_failed";
        exit();
	} else {
	// END FORM DATA ERROR HANDLING
		$sql = "SELECT id, username, password, name, subname, avatar FROM users WHERE username='$e' AND activated='1' LIMIT 1";
        $query = mysqli_query($db_conx, $sql);
        $row = mysqli_fetch_row($query);
		$db_id = $row[0];
		$db_username = $row[1];
        $db_pass_str = $row[2];
        $full_name = $row[3].' '.$row[4];
        $avatar = $row[5];
        if($avatar != ""){
            $pic = '/GTRANS/public/users/user_data/'.$db_username.'/'.$avatar.'';
        } else {
            $pic = '/GTRANS/assets/images/default-img.png';
        }
		if($p != $db_pass_str){
			echo "login_failed";
            exit();
		} else {
			// CREATE THEIR SESSIONS AND COOKIES
			$_SESSION['userid'] = $db_id;
            $_SESSION['full_name'] = $full_name;
			$_SESSION['username'] = $db_username;
			$_SESSION['password'] = $db_pass_str;
            $_SESSION['user_pic'] = $pic;
            $_SESSION['lock'] = '0';
			setcookie("id", $db_id, strtotime( '+30 days' ), "/", "", "", TRUE);
			setcookie("user", $db_username, strtotime( '+30 days' ), "/", "", "", TRUE);
    		setcookie("pic", $pic, strtotime( '+30 days' ), "/", "", "", TRUE);
            setcookie("fname", $full_name, strtotime( '+30 days' ), "/", "", "", TRUE); 
            setcookie("lock", '0', strtotime( '+30 days' ), "/", "", "", TRUE); 
			// UPDATE THEIR "IP" AND "LASTLOGIN" FIELDS
			$sql = "UPDATE users SET ip='$ip', lastlogin=now(), lock_account='0' WHERE username='$db_username' LIMIT 1";
            $query = mysqli_query($db_conx, $sql);
			echo $db_username;
            exit();
		}
	}
	exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Compte verrouillé</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <!-- /theme JS files -->

    <script>
        $(function() {
            // Style checkboxes and radios
            $('.styled').uniform();
        });
    </script>

    <script>
        function emptyElement(x) {
            _(x).innerHTML = "";
        }

        function _(x) {
            return document.getElementById(x);
        }

        function ajaxObj(meth, url) {
            var x = new XMLHttpRequest();
            x.open(meth, url, true);
            x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            return x;
        }

        function ajaxReturn(x) {
            if (x.readyState == 4 && x.status == 200) {
                return true;
            }
        }

        function login() {
            var p = _("password").value;
            if (p == "") {
                _("status").innerHTML = '<span class="label border-left-info label-striped" style="font-size: 13px;" id="status">Remplissez toutes les données du formulaire</span>';
            } else {
                _("loginbtn").disabled = true;
                _("status").innerHTML = '<span class="label border-left-info label-striped" style="font-size: 13px;" id="status">Patientez s\'il-vous-plait ...</span>';
                var ajax = ajaxObj("POST", "login_unlock.php");
                ajax.onreadystatechange = function() {
                    if (ajaxReturn(ajax) == true) {
                        if (ajax.responseText == "login_failed") {
                            _("status").innerHTML = '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Authentification échouée !<br/> - peuvent être données erronées <br/>ou compte pas encore activé </span>';;
                            _("loginbtn").disabled = false;
                        } else {
                            window.location = "javascript:history.go(-1)";//"user?u=" + ajax.responseText;
                        }
                    }
                }
                ajax.send("p=" + p);
            }
        }

        $.post('../../sys/session.php').done(function(data) {
            data = JSON.parse(data);
            $('#un_name').html(data.user_name + ' <small class="display-block">Débloquez votre compte</small>');
            //$('#un_ident').html(data.user_connected);
            $('#un_pic').attr('src', data.user_pic);
        });
    </script>

</head>

<body class="login-container">

    <!-- Main navbar -->
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand position-right"><img src="../../assets/images/medways_light.png" alt=""></a>

            <ul class="nav navbar-nav pull-right visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            </ul>
        </div>

    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Content area -->
                <div class="content">

                    <!-- Unlock user -->
                    <form id="loginform" onsubmit="return false;" class="login-form">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="thumb thumb-rounded">
                                    <img src="/GTRANS/assets/images/default-img.png" id="un_pic" alt="">
                                    <div class="caption-overflow">
                                        <span>
											<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded btn-xs"><i class="icon-collaboration"></i></a>
											<a href="#" class="btn border-white text-white btn-flat btn-icon btn-rounded btn-xs ml-5"><i class="icon-question7"></i></a>
										</span>
                                    </div>
                                </div>

                                <h6 class="content-group text-center text-semibold no-margin-top" id="un_name">GAYTH </h6>

                                <div class="form-group has-feedback">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Votre mot de passe">
                                    <div class="form-control-feedback">
                                        <i class="icon-user-lock text-muted"></i>
                                    </div>
                                </div>

                                <div class="content-divider text-muted form-group"><span>MEDWAYS international</span></div>

                                <button name="loginbtn" id="loginbtn" onclick="login()" class="btn btn-primary btn-block">Déverrouiller <i class="icon-arrow-right14 position-right"></i></button>
                                <a href="logout" class="btn btn-primary btn-block">Se déconnecter <i class="icon-switch2 position-right"></i></a>
                                <div class="text-center">
                                    <span id="status"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- /unlock user -->


                    <!-- Footer -->
                    <div class="footer text-muted text-center">
                        &copy; 2017. <a href="#">Gestions Transit Web App </a> by <a href="https://github.com/orgs/GTeCHSOFT" target="_blank">GTeCH+</a>
                    </div>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

</body>

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/login_unlock.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 May 2017 10:31:48 GMT -->

</html>