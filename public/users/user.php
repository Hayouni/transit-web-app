<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
$sql = "SELECT id, title, start, end, color FROM events WHERE username = '".$_SESSION['username']."'";
$result = mysqli_query($db_conx, $sql);
$events = $result->fetch_all(MYSQLI_ASSOC);
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/Encryption.php');
$_SESSION['smtp_msg'] = ""; 
$_SESSION['param_msg'] = "";
if (isset($_POST['name']) && isset($_POST['name'])){
    $db = new MySQL();
    $update_where = array('username' => $_SESSION['username']);
    $update_data = array('name' => $_POST['name'], 'subname' => $_POST['subname'], 'email' => $_POST['email'],
    'phone' => $_POST['mobile'], 'domicile' => $_POST['mobile_domi'], 'professionel' => $_POST['mobile_pro'],
    'address1' => $_POST['address1'], 'vill' => $_POST['ville'], 'pays' => $_POST['pays'], 'codezip' => $_POST['codezip'], 
    'soc1' => $_POST['linkedin'], 'soc2' => $_POST['twitter'], 'soc3' => $_POST['facebook']);
    echo $db->update( 'users', $update_data, $update_where );
}elseif (isset($_POST['ch_old_pass']) && isset($_POST['ch_new_pass']) && isset($_POST['ch_rnew_pass'])) {
    if ($_POST['ch_new_pass'] == $_POST['ch_rnew_pass'] && $_POST['ch_rnew_pass'] != ''){
        $db = new MySQL();
        $get_user_pass = $db->get_results("SELECT password FROM users WHERE username = '".$_SESSION['username']."'")[0]['password'];
        if ($get_user_pass == $_POST['ch_old_pass']){
                $update_where = array('username' => $_SESSION['username']);
                $update_data = array('password' => $_POST['ch_new_pass']);
                $res = $db->update( 'users', $update_data, $update_where );
                if ($res == '1'){
                    $_SESSION['param_msg'] = "<div class='label label-success'>Mot de passe mis à jour avec succès, reconnectez-vous</div>";
                    header("location: logout");
                } else {
                    $_SESSION['param_msg'] = "<div class='label label-danger'>Problème mis à jour par le mot de passe, reconnectez-vous ou contactez l'admin";
                }
        }else{
            $_SESSION['param_msg'] = "<div class='label label-success'>Ancien mot de passe non confirmé</div>";
        }
    }else {
        $_SESSION['param_msg'] = "<div class='label label-success'>Nouveau mot de passe non confirmé</div>";
    }
} elseif (isset($_POST['smtp_user']) && isset($_POST['smtp_pass'])) {
    $db = new MySQL();
    $cryptor = new Encryption();
    $update_where = array('username' => $_SESSION['username']);
    $update_data = array(
        'smtp_host' => $_POST['smtp_host'], 
        'smtp_port' => $_POST['smtp_port'], 
        'smtp_pass' => $cryptor->safe_b64encode($_POST['smtp_pass']), 
        'smtp_crypto' => $_POST['smtp_crypto'],
        'imap_host' => $_POST['imap_host'],
        'imap_port' => $_POST['imap_port'],
        'mail_autoload' => $_POST['auto_mail']);
    $res = $db->update( 'users', $update_data, $update_where );
    if ($res == '1'){
        $_SESSION['smtp_msg'] = "<div class='label label-success'>Email mis à jour avec succès</div>";
        $_SESSION['mail_autoload'] = $_POST['auto_mail'];
    } else {
        $_SESSION['smtp_msg'] = "<div class='label label-danger'>Problème mis à jour, reconnectez-vous ou contactez l'admin";
    }
}

$d = new MySQL();
$res = $d->get_results("SELECT email,smtp_host,smtp_port,smtp_pass,smtp_crypto,imap_host,imap_port,mail_autoload FROM users WHERE username = '".$_SESSION['username']."'");
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Profile</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jasny_bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/fullcalendar3.5.1/fullcalendar.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/fullcalendar3.5.1/locale/fr.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/uploaders/fileinput/fileinput.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/noty.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/jgrowl.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/touch.min.js"></script>


    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../../assets/js/jquery.timeago.js"></script>
    <script type="text/javascript" src="api/profile.js"></script>
    <!-- /theme JS files -->

    <style>
    

    .fc th {
        background-color: #37474f;
        color : #fff;
    }

    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Pages utilisateur</span></h4>
                        </div>
                    </div>
                </div>
                <!-- /page header -->


                <!-- Content area -->
                <div class="content">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel-body bg-slate-700 border-radius-top text-center" style="background-image: url(../../assets/images/backgrounds/panel_bg.png); background-size: contain;">
                                <div class="content-group-sm">
                                    <h6 class="text-semibold no-margin-bottom" id="un_name"></h6>

                                    <span class="display-block" id="un_ident"></span>
                                </div>

                                <a href="#" class="display-inline-block content-group-sm">
                                    <img src="/GTRANS/assets/images/default-img.png" class="img-circle img-responsive" alt="" style="width: 110px; height: 110px;" id="un_pic">
                                </a>

                                <ul class="list-inline list-inline-condensed no-margin-bottom">
                                    <li><a id="u_li" target="_blank" href="#" class="btn bg-slate btn-rounded btn-icon"><i class="icon-linkedin"></i></a></li>
                                    <li><a id="u_tw" target="_blank" href="#" class="btn bg-slate btn-rounded btn-icon"><i class="icon-twitter"></i></a></li>
                                    <li><a id="u_fb" target="_blank" href="#" class="btn bg-slate btn-rounded btn-icon"><i class="icon-facebook"></i></a></li>
                                    <li><a href="login_unlock" class="btn bg-slate btn-rounded btn-icon"><i class="icon-user-lock"></i> Verrouiller</a></li>
                                    <li><a href="logout" class="btn bg-slate btn-rounded btn-icon"><i class="icon-switch2"></i> Se déconnecter</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">         
                            <!-- Profile info -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Informations sur le profil</h6>
                                </div>

                                <div class="panel-body">
                                    <form action="user" method="POST">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Nom</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="name" id="name">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Prénom</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="subname" id="subname">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Linkedin</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="linkedin" id="linkedin" value="https://www.linkedin.com/in/">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Adresse 1</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="address1" id="address1">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Twitter</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="twitter" id="twitter" value="https://twitter.com/">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>Ville</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="ville" id="ville">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>État / Province</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="pays" id="pays">
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Code postal</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="codezip" id="codezip">
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Facebook</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name= "facebook" id="facebook" value="https://web.facebook.com/">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Email</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="email" id="email">
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Mobile</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="mobile" id="mobile">
                                                    <span class="help-block">+216-99-999-999</span>
                                                </div>
                                                <div class="col-md-3">
                                                    <label>Pro.</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="mobile_pro" id="mobile_pro">
                                                    <span class="help-block">+216-99-999-999</span>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Domicile</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" name="mobile_domi" id="mobile_domi">
                                                    <span class="help-block">+216-99-999-999</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary">Sauvegarder <i class="icon-arrow-right14 position-right"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Profile img -->
                            <div class="panel panel-flat">
                                <div class="panel-body">
                                    <form id="avatar_form" enctype="multipart/form-data" method="post" action="photo_system.php">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label class="display-block">Télécharger l'image de profil</label>
                                                    <input type="file" class="file-input" name="avatar">
                                                    <span class="help-block">Formats acceptés: gif, png, jpg. Taille maximale du fichier 2Mb</span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Calender-->
                            <!-- Modal -->
                            <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal" method="POST" action="api/addEvent.php">
                                            <div class="modal-header bg-primary">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><i class="icon-calendar2"></i> &nbsp;Ajouter un évènement</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="text" name="title" class="form-control border-black border-lg text-black" id="title" placeholder="Titre">
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <textarea rows="7" cols="6" name="description" id="description" class="form-control border-black border-lg text-black" placeholder="Description..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="color" class="col-sm-3 control-label">Couleur</label>
                                                            <div class="col-sm-9">
                                                                <input class="form-control border-black border-lg text-black" type="color" name="color" id="color">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="start" class="col-sm-3 control-label">Début</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="start" class="form-control border-black border-lg text-black" id="start">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="end" class="col-sm-3 control-label">Fin</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="end" class="form-control border-black border-lg text-black" id="end">
                                                            </div>
                                                        </div>                                                                
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                <button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <form class="form-horizontal" method="POST" action="api/editEventTitle.php">
                                            <div class="modal-header bg-teal-700">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel"><i class="icon-calendar2"></i> &nbsp;Modifier l'événement</h4>
                                            </div>
                                            <div class="modal-body">

                                                <div class="form-group">
                                                    <label for="title" class="col-sm-2 control-label">Titre</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="title" class="form-control border-black border-lg text-black" id="title2" placeholder="Titre">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="color" class="col-sm-2 control-label">Couleur</label>
                                                    <div class="col-sm-10">
                                                        <input class="form-control border-black border-lg text-black" type="color" name="color" id="color2">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <div class="checkbox">
                                                            <label class="text-danger">
                                                                <input type="checkbox" class="styled">
                                                                Supprimer l'événement
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <input type="hidden" name="id" class="form-control" id="id">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                                <button type="submit" class="btn bg-teal-700">Sauvegarder les modifications</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /available hours -->

                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    
                                    <!-- Account settings -->
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"><i class="icon-mail-read"></i> Paramètres E-mail</h6>
                                            <div class="heading-elements">
                                                <div class="heading-btn">
                                                    <!--button type="button" id="btn_add" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_fourn"><i class="fa fa-plus-circle"></i> Ajouter</button-->
                                                    <?= $_SESSION['smtp_msg'] ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-body">
                                            <form action="user" method="POST">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>NOM D'UTILISATEUR</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control border-black border-lg text-black" name="smtp_user" id="smtp_user" value="<?php echo $res[0]['email'] ?>">
                                                                <span class="input-group-addon bg-default"><a id="get_mail_seti"><i class="icon-help"></i></a></span>
                                                            </div>
                                                            <div class="form-group has-feedback">

                                                                <div class="form-control-feedback">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>MOT DE PASSE</label>
                                                            <input type="password" class="form-control border-black border-lg text-black" name="smtp_pass" id="smtp_pass" value="<?php echo $res[0]['smtp_pass'] ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>NOM D'HÔTE SMTP</label>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="smtp_host" id="smtp_host" value="<?php echo $res[0]['smtp_host'] ?>">
                                                        </div>

                                                        <div class="col-md-4">
                                                            <label>PORT SMTP (25, 465, 587)</label>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="smtp_port" id="smtp_port" value="<?php echo $res[0]['smtp_port'] ?>">
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label>SÉCURITÉ SMTP</label>
                                                            <select name="smtp_crypto" id="smtp_crypto" class="form-control border-black border-lg text-black">
                                                                <option value="" <?php if($res[0]['smtp_crypto'] == '') echo('selected="selected"') ; ?>>None</option>
                                                                <option value="tls" <?php if($res[0]['smtp_crypto'] == 'tls') echo('selected="selected"') ; ?>>TLS</option>
                                                                <option value="ssl" <?php if($res[0]['smtp_crypto'] == 'ssl') echo('selected="selected"') ; ?>>SSL</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label>NOM D'HÔTE IMAP</label>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="imap_host" id="imap_host" value="<?php echo $res[0]['imap_host'] ?>">
                                                        </div>

                                                        <div class="col-md-3">
                                                            <label>PORT IMAP (993)</label>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="imap_port" id="imap_port" value="<?php echo $res[0]['imap_port'] ?>">
                                                        </div>

                                                        <div class="col-md-5">
                                                            <label>Chargement automatique d'e-mails</label>
                                                            <select name="auto_mail" id="auto_mail" class="form-control border-black border-lg text-black">
                                                                <option value="on" <?php if($res[0]['mail_autoload'] == 'on') echo('selected="selected"') ; ?>>Oui</option>
                                                                <option value="off" <?php if($res[0]['mail_autoload'] == 'off') echo('selected="selected"') ; ?>>Non</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary">Sauvegarder <i class="icon-arrow-right14 position-right"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /account settings -->
                                </div>
                                <div class="col-lg-4 col-md-12">

                                    <!-- Account settings -->
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title"><i class="icon-cog"></i> Paramètres du compte</h6>
                                            <div class="heading-elements">
                                                <div class="heading-btn">
                                                    <!--button type="button" id="btn_add" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_fourn"><i class="fa fa-plus-circle"></i> Ajouter</button-->
                                                    <?= $_SESSION['param_msg'] ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel-body">
                                            <form action="user" method="POST">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Nom d'utilisateur</label>
                                                            <input type="text" disabled="true" readonly="readonly" class="form-control border-black border-lg text-black" id="ch_username">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Mot de passe actuel</label>
                                                            <input type="password" class="form-control border-black border-lg text-black" name="ch_old_pass" id="ch_old_pass">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Nouveau mot de passe</label>
                                                            <input type="password" class="form-control border-black border-lg text-black" name="ch_new_pass" id="ch_new_pass">
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label>Répétez le nouveau mot de passe</label>
                                                            <input type="password" class="form-control border-black border-lg text-black" name="ch_rnew_pass" id="ch_rnew_pass">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-primary">Sauvegarder <i class="icon-arrow-right14 position-right"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- /account settings -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Calendar -->
                                    <div class="panel panel-flat">
                                        <div class="panel-heading">
                                            <h6 class="panel-title">Mon emploi du temp</h6>
                                        </div>

                                        <div class="panel-body">
                                            <div class="fullcalendar" id="calendar"></div>
                                        </div>
                                    </div>
                                    <!-- /calendar -->
                                </div>
                            </div>
                        </div>              
                    </div>
                    <!-- Basic modal -->
                    <div id="PROF_INFO" class="modal fade">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body bg-teal-400">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="thumbnail">
                                                    <div class="thumb thumb-slide">
                                                        <img id="uo_pici" src="/GTRANS/assets/images/default-img.png" alt="">
                                                        <div class="caption">
                                                            <span>
                                                        <a href="../../assets/images/demo/images/3.png" class="btn bg-success-400 btn-icon btn-xs" data-popup="lightbox"><i class="icon-plus2"></i></a>
                                                        <a href="user_pages_profile.html" class="btn bg-success-400 btn-icon btn-xs"><i class="icon-link"></i></a>
                                                    </span>
                                                        </div>
                                                    </div>

                                                    <div class="caption text-center">
                                                        <h6 class="text-semibold no-margin" id="uo_name">-</h6>
                                                        <ul class="icons-list mt-15">
                                                            <li><a id="uo_soc1" target="_blank" href="#" data-popup="tooltip" title="Linkedin" data-container="body"><i class="icon-linkedin"></i></a></li>
                                                            <li><a id="uo_soc2" target="_blank" href="#" data-popup="tooltip" title="Twitter" data-container="body"><i class="icon-twitter"></i></a></li>
                                                            <li><a id="uo_soc3" target="_blank" href="#" data-popup="tooltip" title="Facebook" data-container="body"><i class="icon-facebook"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <ul class="navigation">
                                                    <li><a><i class="icon-mailbox"></i> Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<span class="text-slate-800" id="uo_mail">-</span></a></li>
                                                    <li><a><i class="icon-mobile"></i> Mobile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<span class="text-slate-800" id="uo_phone">-</span></a></li>
                                                    <li><a><i class="icon-phone2"></i> Profes.&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<span class="text-slate-800" id="uo_prof">-</span></a></li>
                                                    <li><a><i class="icon-phone"></i> Domicile&nbsp;:&nbsp;<span class="text-slate-800" id="uo_domi">-</span></a></li>
                                                    <li><a><i class="icon-home5"></i> Adresse&nbsp;&nbsp;:&nbsp;<span class="text-slate-800" id="uo_addr">-</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                        <!-- /basic modal -->

                    <div id="fullCalModal" class="modal fade">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                                    <h4 id="modalTitle" style="word-wrap: break-word;" class="modal-title"></h4>
                                </div>
                                <div id="modalBody" class="modal-body" style="word-wrap: break-word;"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                                    <!--button class="btn btn-primary"><a id="eventUrl" target="_blank">Event Page</a></button-->
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->

                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->


    <script>
        $(document).ready(function() {

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                defaultDate: new Date(),
                defaultView: 'month',
                businessHours: true,
                locale: 'fr',
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                selectHelper: true,
                dayPopoverFormat: 'NULL',
                select: function(start, end) {

                    $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD HH:mm:ss'));
                    $('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD HH:mm:ss'));
                    $('#ModalAdd').modal('show');
                },
                eventRender: function(event, element) {
                    /*element.bind('dblclick', function() {
                        $('#ModalEdit #id').val(event.id);
                        $('#ModalEdit #title').val(event.title);
                        $('#ModalEdit #color').val(event.color);
                        $('#ModalEdit').modal('show');
                    });*/
                    // how many milliseconds is a long press?
                    var longpress = 100;
                    // holds the start time
                    var start;
                    element.bind('mousedown', function( e ) {
                        start = new Date().getTime();
                    });
                    element.bind('mouseleave', function( e ) {
                        start = 0;
                    });
                    element.bind( 'mouseup', function( e ) {
                        if ( new Date().getTime() >= ( start + longpress )  ) {
                            $('#ModalEdit #id').val(event.id);
                            $('#ModalEdit #title2').val(event.title);
                            $('#ModalEdit #color2').val(event.color);
                            $('#ModalEdit').modal('show');  
                        } else {
                            $('#modalTitle').html(event.title);
                            $('#modalBody').html(event.description);
                            $('#eventUrl').attr('href',event.url);
                            $('#fullCalModal').modal();  
                        }
                    });
                },
                eventDrop: function(event, delta, revertFunc) { // si changement de position

                    edit(event);

                },
                eventResize: function(event, dayDelta, minuteDelta, revertFunc) { // si changement de longueur

                    edit(event);

                },
                events: [
                    <?php foreach($events as $event): 
                        $start = explode(" ", $event['start']);
                        $end = explode(" ", $event['end']);
                        if($start[1] == '00:00:00'){
                            $start = $start[0];
                        }else{
                            $start = $event['start'];
                        }
                        if($end[1] == '00:00:00'){
                            $end = $end[0];
                        }else{
                            $end = $event['end'];
                        }
                    ?> {
                        id: '<?php echo $event['id']; ?>',
                        title: '<?php echo $event['title']; ?>',
                        start: '<?php echo $start; ?>',
                        end: '<?php echo $end; ?>',
                        color: '<?php echo $event['color']; ?>',
                        description : '55555588965',
                    },
                    <?php endforeach; ?>
                ]
            });

            function edit(event) {
                start = event.start.format('YYYY-MM-DD HH:mm:ss');
                if (event.end) {
                    end = event.end.format('YYYY-MM-DD HH:mm:ss');
                } else {
                    end = start;
                }

                id = event.id;

                Event = [];
                Event[0] = id;
                Event[1] = start;
                Event[2] = end;

                $.ajax({
                    url: 'api/editEventDate.php',
                    type: "POST",
                    data: {
                        Event: Event
                    },
                    success: function(rep) {
                        if (rep == 'OK') {
                            //alert('Saved');
                        } else {
                            alert('Impossible d\'être enregistré. réessayer.');
                        }
                    }
                });
            }

        });
    </script>

</body>

</html>