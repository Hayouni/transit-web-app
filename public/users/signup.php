<?php
//error_reporting(E_ERROR | E_PARSE);
session_start();
// If user is logged in, header them away
if(isset($_SESSION["username"])){
	header("location: message.php?msg=Un compte déjà connecté, déconnectez-vous en premier");
    exit();
}
?>
<?php
// Ajax calls this NAME CHECK code to execute
if(isset($_POST["usernamecheck"])){
	include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
	$username = preg_replace('#[^a-z0-9]#i', '', $_POST['usernamecheck']);
	$sql = "SELECT id FROM users WHERE username='$username' LIMIT 1";
    $query = mysqli_query($db_conx, $sql); 
    $uname_check = mysqli_num_rows($query);
    if (strlen($username) < 3 || strlen($username) > 16) {
        echo '<span class="label label-block label-info text-center" ><strong style="color:#FFF;font-size: 13px;">Le nom d\'utilisateur doit être compris entre 3 et 16 caractères s\'il vous plaît</strong></span>';
	    exit();
    }
	/*if (is_numeric($username[0])) {
        echo '<span class="label label-block label-info text-center" ><strong style="color:#FFF;font-size: 13px;">Les noms d\'utilisateur doivent commencer par une lettre</strong></span>';
	    exit();
    }*/
    if ($uname_check < 1) {
	    echo '<span class="label label-block label-success text-center" ><strong style="color:#FFF;font-size: 13px;">' . $username . ' est un nom d\'utilisateur disponible</strong></span>';
	    exit();
    } else {
        echo '<span class="label label-block label-danger text-center" ><strong style="color:#FFF;font-size: 13px;">' . $username . ' est un nom d\'utilisateur déjà pris !</strong></span>';
	    exit();
    }
}
?>
        <?php
// Ajax calls this REGISTRATION code to execute
if(isset($_POST["u"])){
	// CONNECT TO THE DATABASE
    include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
	// GATHER THE POSTED DATA INTO LOCAL VARIABLES
	$u = preg_replace('#[^a-z0-9]#i', '', $_POST['u']);
    $n1 = preg_replace('#[^a-z0-9]#i', '', $_POST['n1']);
    $n2 = preg_replace('#[^a-z0-9]#i', '', $_POST['n2']);
	$e = mysqli_real_escape_string($db_conx, $_POST['e']);
	$p = $_POST['p'];
	$g = preg_replace('#[^a-z]#', '', $_POST['g']);
	$c = preg_replace('#[^a-z ]#i', '', $_POST['c']);
	// GET USER IP ADDRESS
    $ip = preg_replace('#[^0-9.]#', '', getenv('REMOTE_ADDR'));
	// DUPLICATE DATA CHECKS FOR USERNAME AND EMAIL
	$sql = "SELECT id FROM users WHERE username='$u' LIMIT 1";
    $query = mysqli_query($db_conx, $sql); 
	$u_check = mysqli_num_rows($query);
	// -------------------------------------------
	$sql = "SELECT id FROM users WHERE email='$e' LIMIT 1";
    $query = mysqli_query($db_conx, $sql); 
	$e_check = mysqli_num_rows($query);
	// FORM DATA ERROR HANDLING
	if($u == "" || $e == "" || $p == "" || $g == "" || $c == "" || $n1 == "" || $n2 == ""){
        echo '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">La soumission de formulaire est manquant certaines valeurs</span>';
        exit();
	} else if ($u_check > 0){ 
        echo '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Le nom d\'utilisateur que vous avez entré est déjà pris</span>';
        exit();
	} else if ($e_check > 0){ 
        echo '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Cette adresse email est déjà utilisée</span>';
        exit();
	} else if (strlen($u) < 3 || strlen($u) > 16) {
        echo '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Le nom d\'utilisateur doit être compris entre 3 et 16 caractère</span>';
        exit(); 
    } /*else if (is_numeric($u[0])) {
        echo '<span class="label border-left-danger label-striped" style="font-size: 13px;" id="status">Le nom d\'utilisateur ne peut pas commencer par un nombre</span>';
        exit();
    } */else {
	// END FORM DATA ERROR HANDLING
	    // Begin Insertion of data into the database
		// Hash the password and apply your own mysterious unique salt
		//$cryptpass = crypt($p);
		//include_once ("randStrGen.php");
		$p_hash = $p;//md5($p); //randStrGen(20)."$cryptpass".randStrGen(20);
		// Add user info into the database table for the main site table
		$sql = "INSERT INTO users (username, name, subname, email, password, gender, country, ip, signup, lastlogin, notescheck)       
		        VALUES('$u', '$n1', '$n2', '$e','$p_hash','$g','$c','$ip',now(),now(),now())";
		$query = mysqli_query($db_conx, $sql); 
		$uid = mysqli_insert_id($db_conx);
		// Establish their row in the useroptions table
		$sql = "INSERT INTO useroptions (id, username, background) VALUES ('$uid','$u','original')";
		$query = mysqli_query($db_conx, $sql);
		// Create directory(folder) to hold each user's files(pics, MP3s, etc.)
		if (!file_exists("user_data/$u")) {
			mkdir("user_data/$u", 0755);
		}
		// Email the user their activation link
		/*$to = "$e";							 
		$from = "gaiththewolf@gmail.com";
		$subject = 'yoursitename Account Activation';
		$message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>yoursitename Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#333; font-size:24px; color:#CCC;"><a href="http://www.yoursitename.com"><img src="http://www.yoursitename.com/images/logo.png" width="36" height="30" alt="yoursitename" style="border:none; float:left;"></a>yoursitename Account Activation</div><div style="padding:24px; font-size:17px;">Hello '.$u.',<br /><br />Click the link below to activate your account when ready:<br /><br /><a href="http://www.yoursitename.com/activation.php?id='.$uid.'&u='.$u.'&e='.$e.'&p='.$p_hash.'">Click here to activate your account now</a><br /><br />Login after successful activation using your:<br />* E-mail Address: <b>'.$e.'</b></div></body></html>';
		$headers = "From: $from\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
		mail($to, $subject, $message, $headers);*/
        echo '<span class="label border-left-success label-striped" style="font-size: 13px;" id="status">L\'inscription est réussie, attendez l\'activation du compte par l\'administrateur</span>';
		exit();
	}
	exit();
}
?>
            <!DOCTYPE html>
            <html lang="fr">

            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>GTRANS | Registration</title>

                <!-- Global stylesheets -->
                <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
                <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
                <!-- /global stylesheets -->

                <!-- Core JS files -->
                <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
                <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
                <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
                <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
                <!-- /core JS files -->

                <!-- Theme JS files -->
                <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>

                <script type="text/javascript" src="../../assets/js/core/app.js"></script>
                <script type="text/javascript" src="api/registration.js"></script>
                <!-- /theme JS files -->

                <script>
                    $(function() {

                        // Style checkboxes and radios
                        $('.styled').uniform();

                    });

                    function _(x) {
                        return document.getElementById(x);
                    }

                    function restrict(elem) {
                        var tf = _(elem);
                        var rx = new RegExp();
                        if (elem == "email") {
                            rx = /[' "]/gi;
                        } else if (elem == "username") {
                            rx = /[^a-z0-9]/gi;
                        }
                        tf.value = tf.value.replace(rx, "");
                    }

                    function emptyElement(x) {
                        _(x).innerHTML = "";
                    }

                    function checkusername() {
                        var u = _("username").value;
                        if (u != "") {
                            _("unamestatus").innerHTML = 'Vérification ...';
                            var ajax = ajaxObj("POST", "signup.php");
                            ajax.onreadystatechange = function() {
                                if (ajaxReturn(ajax) == true) {
                                    _("unamestatus").innerHTML = ajax.responseText;
                                }
                            }
                            ajax.send("usernamecheck=" + u);
                        }
                    }

                    function signup() {
                        var u = _("username").value;
                        var n1 = _("name1").value;
                        var n2 = _("name2").value;
                        var e = _("email").value;
                        var e2 = _("email2").value;
                        var p1 = _("pass1").value;
                        var p2 = _("pass2").value;
                        var c = "tunisia"; //_("country").value;
                        var g = "m"; //_("gender").value;
                        var status = _("status");
                        if (u == "" || e == "" || e2 == "" || p1 == "" || p2 == "" || n1 == "" || n2 == "") {
                            status.innerHTML = '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">S\'il vous plaît remplir toutes les données de formulaire</span>';
                        } else if (p1 != p2) {
                            status.innerHTML = '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Vos champs de mot de passe ne correspondent pas</span>';
                        } else if (e != e2) {
                            status.innerHTML = '<span class="label border-left-warning label-striped" style="font-size: 13px;" id="status">Vos email ne correspondent pas</span>';
                        } else if (_("terms").style.display == "none") {
                            status.innerHTML = "Please view the terms of use";
                        } else {
                            _("signupbtn").disabled = true; //.style.display = "none";
                            status.innerHTML = '<span class="label border-left-info label-striped" style="font-size: 13px;" id="status">Patientez s\'il-vous-plait ...</span>';
                            var ajax = ajaxObj("POST", "signup.php");
                            ajax.onreadystatechange = function() {
                                if (ajaxReturn(ajax) == true) {
                                    if (ajax.responseText != "signup_success") {
                                        status.innerHTML = ajax.responseText;
                                        _("signupbtn").disabled = false; //.style.display = "block";
                                    } else {
                                        window.scrollTo(0, 0);
                                        _("signupform").innerHTML = "OK " + u + ", check your email inbox and junk mail box at <u>" + e + "</u> in a moment to complete the sign up process by activating your account. You will not be able to do anything on the site until you successfully activate your account.";
                                    }
                                }
                            }
                            ajax.send("u=" + u + "&e=" + e + "&p=" + p1 + "&c=" + c + "&g=" + g + "&n1=" + n1 + "&n2=" + n2);
                        }
                    }

                    function openTerms() {
                        _("terms").style.display = "block";
                        emptyElement("status");
                    }
                    /* function addEvents(){
                    	_("elemID").addEventListener("click", func, false);
                    }
                    window.onload = addEvents; */

                    function ajaxObj(meth, url) {
                        var x = new XMLHttpRequest();
                        x.open(meth, url, true);
                        x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        return x;
                    }

                    function ajaxReturn(x) {
                        if (x.readyState == 4 && x.status == 200) {
                            return true;
                        }
                    }
                </script>

            </head>

            <body class="login-container">

                <!-- Main navbar -->
                <div class="navbar navbar-inverse">
                    <div class="navbar-header">
                        <a class="navbar-brand" href=""><img src="../../assets/images/medways_light.png" alt=""></a>

                        <ul class="nav navbar-nav pull-right visible-xs-block">
                            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                        </ul>
                    </div>

                    <div class="navbar-collapse collapse" id="navbar-mobile">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#">
                                    <i class="icon-display4"></i> <span class="visible-xs-inline-block position-right"> Go to website</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="icon-user-tie"></i> <span class="visible-xs-inline-block position-right"> Contact admin</span>
                                </a>
                            </li>

                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-cog3"></i>
                                    <span class="visible-xs-inline-block position-right"> Options</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /main navbar -->


                <!-- Page container -->
                <div class="page-container">

                    <!-- Page content -->
                    <div class="page-content">

                        <!-- Main content -->
                        <div class="content-wrapper">

                            <!-- Content area -->
                            <div class="content">

                                <!-- Registration form -->
                                <form name="signupform" id="signupform" onsubmit="return false;">
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-offset-3">
                                            <div class="panel registration-form">
                                                <div class="panel-body">
                                                    <div class="text-center">
                                                        <div class="icon-object border-success text-success"><i class="icon-plus3"></i></div>
                                                        <h5 class="content-group-lg">Créer un compte <small class="display-block">Tous les champs sont requis.</small></h5>
                                                    </div>

                                                    <div class="form-group has-feedback">
                                                        <input id="username" type="text" onblur="checkusername()" onkeyup="restrict('username')" maxlength="16" class="form-control" placeholder="Choisissez un nom d'utilisateur">
                                                        <div class="form-control-feedback">
                                                            <i class="icon-user-plus text-muted"></i>
                                                        </div>
                                                        <span id="unamestatus"></span>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="name1" type="text" class="form-control" placeholder="Prénom">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-user-check text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="name2" type="text" class="form-control" placeholder="Nom de famille">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-user-check text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="pass1" type="password" onfocus="emptyElement('status')" maxlength="16" class="form-control" placeholder="Créer un mot de passe">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-user-lock text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="pass2" type="password" onfocus="emptyElement('status')" maxlength="16" class="form-control" placeholder="Répéter le mot de passe">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-user-lock text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="email" type="text" onfocus="emptyElement('status')" onkeyup="restrict('email')" maxlength="88" class="form-control" placeholder="Votre email">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-mention text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group has-feedback">
                                                                <input id="email2" type="text" onfocus="emptyElement('status')" onkeyup="restrict('email')" maxlength="88" class="form-control" placeholder="Répétez l'email">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-mention text-muted"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" class="styled" id="terms" checked>
                                                                Acceptez <a href="#"> termes de service</a>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="text-right">
                                                        <a href="/GTRANS" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> Retour à l'identification</a>
                                                        <button id="signupbtn" onclick="signup()" class="btn bg-teal-400 btn-labeled btn-labeled-right ml-10"><b><i class="icon-plus3"></i></b> Créer un compte</button>
                                                    </div>
                                                    <hr>
                                                    <div class="text-center">
                                                        <span id="status"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- /registration form -->
                                <!-- Footer -->
                                <div class="footer text-muted text-center ">
                                    &copy; 2017. <a href="# ">Gestions Transit Web App </a> by <a href="https://github.com/orgs/GTeCHSOFT " target="_blank ">GTeCH+</a>
                                </div>
                                <!-- /footer -->

                            </div>
                            <!-- /content area -->

                        </div>
                        <!-- /main content -->

                    </div>
                    <!-- /page content -->

                </div>
                <!-- /page container -->

            </body>

            <!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/login_registration_advanced.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 19 May 2017 10:31:48 GMT -->

            </html>