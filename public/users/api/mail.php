<?php
require_once "../mailing/checker.php";
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'get_mail_data':
        $db = new MySQL();
        $get_mail_data["data"] = $db->get_results("SELECT * FROM mail_data ORDER BY mail_date DESC");
        echo json_encode($get_mail_data);
        break;

    case 'SET_SEEN':
        $message = $server->getMessageByUid($response[1]);
        if ($message->setFlag('seen')) {
            echo $db->update('mail_data',['mail_seen' => 1],['mail_id' => $response[1]],1);
        } else {
            echo json_encode(array('seen' => false));
        }
        break;

    case 'SET_TRASH':
        $def = $server->getServerString();
        $all_mailbox = $server->listMailBoxes();
        $mailbox ="";
        foreach ($all_mailbox as $key => $value) {
            if (strpos($value, 'Trash') !== false) {
                $mailbox = substr($value, strlen($def), strlen($value)) ;
            }
        }
        if ($mailbox === "") {
            echo json_encode(array('error' => 'Mailbox Not Match'));
            return false;
            die();
        }
        $message = $server->getMessageByUid($response[1]);
        if ($message) {
            if ($message->moveToMailBox($mailbox)) {
                $rs = $db->get_results("SELECT * FROM mail_data WHERE mail_id = $response[1]");
                if ($db->insert("mail_trash", $rs[0])) {
                    echo $db->delete("mail_data", ['mail_id' => $response[1]]);
                    echo $db->update('mail_trash',['mail_deleted' => 1],['mail_id' => $response[1]],1);
                }
            } else {
                echo json_encode(array('trash' => false));
            }
        }else {
            $rs = $db->get_results("SELECT * FROM mail_data WHERE mail_id = $response[1]");
            if ($db->insert("mail_trash", $rs[0])) {
                echo $db->delete("mail_data", ['mail_id' => $response[1]]);
                echo $db->update('mail_trash',['mail_deleted' => 1],['mail_id' => $response[1]],1);
            }
        }
        break;
        
    case 'SET_SPAM':
    $def = $server->getServerString();
        $all_mailbox = $server->listMailBoxes();
        $mailbox ="";
        foreach ($all_mailbox as $key => $value) {
            if (strpos($value, 'Spam') !== false) {
                $mailbox = substr($value, strlen($def), strlen($value)) ;
            }
        }
        if ($mailbox === "") {
            echo json_encode(array('error' => 'Mailbox Not Match'));
            return false;
            die();
        }
        $message = $server->getMessageByUid($response[1]);
        if ($message) {
            if ($message->moveToMailBox($mailbox)) {
                $rs = $db->get_results("SELECT * FROM mail_data WHERE mail_id = $response[1]");
                if ($db->insert("mail_spam", $rs[0])) {
                    echo $db->delete("mail_data", ['mail_id' => $response[1]]);
                }
            } else {
                echo json_encode(array('spam' => false));
            }
        }else {
            $rs = $db->get_results("SELECT * FROM mail_data WHERE mail_id = $response[1]");
            if ($db->insert("mail_spam", $rs[0])) {
                echo $db->delete("mail_data", ['mail_id' => $response[1]]);
            }
        }
        break;
    
    case 'get_uo_info':
        $db = new MySQL();
        $get_uo_info = $db->get_results("SELECT username,avatar,name,subname,email,phone,professionel,domicile,address1,vill,pays,codezip,soc1,soc2,soc3 FROM users WHERE username = '$response[1]'");
        echo json_encode($get_uo_info);
        break;

    case 'get_counter_for_mail':
        $db = new MySQL();
        $unseen = $db->get_results("SELECT count( * ) as unseen FROM mail_data WHERE mail_seen = 0")[0]["unseen"];
        $spam_count = $db->get_results("SELECT count( * ) as spam_count FROM mail_spam WHERE mail_seen = 0")[0]["spam_count"];
        echo json_encode(['unseen' => $unseen, 'spam_count' => $spam_count]);
        break;

    case 'get_mail_draft':
        $db = new MySQL();
        $get_mail_data["data"] = $db->get_results("SELECT * FROM mail_draft ORDER BY mail_date DESC");
        echo json_encode($get_mail_data);
        break;

    case 'get_mail_sent':
        $db = new MySQL();
        $get_mail_data["data"] = $db->get_results("SELECT * FROM mail_sent ORDER BY mail_date DESC");
        echo json_encode($get_mail_data);
        break;

    case 'get_mail_spam':
        $db = new MySQL();
        $get_mail_data["data"] = $db->get_results("SELECT * FROM mail_spam ORDER BY mail_date DESC");
        echo json_encode($get_mail_data);
        break;

    case 'get_mail_trash':
        $db = new MySQL();
        $get_mail_data["data"] = $db->get_results("SELECT * FROM mail_trash ORDER BY mail_date DESC");
        echo json_encode($get_mail_data);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>