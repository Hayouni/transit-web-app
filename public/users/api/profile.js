$(document).ready(function() {
    // Modal template
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    // Buttons inside zoom modal
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    // File actions
    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };
    $('.file-input').fileinput({
        browseLabel: 'Parcourir',
        browseIcon: '<i class="icon-file-plus"></i>',
        uploadIcon: '<i class="icon-file-upload2"></i>',
        removeLabel: 'Retirer',
        cancelLabel: 'Annuler',
        cancelTitle: "Annuler l'envoi en cours",
        uploadLabel: 'Transférer',
        removeIcon: '<i class="icon-cross3"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialCaption: "Aucun fichier sélectionné",
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings
    });

    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    $("#readme").on('click', function() {
        $("#list_mail").hide();
        $("#read_mail").fadeIn();
    });

    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });

    // Danger
    $(".control-danger").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-danger-600 text-danger-800'
    });

    // Success
    $(".control-success").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-success-600 text-success-800'
    });

    // Warning
    $(".control-warning").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-warning-600 text-warning-800'
    });

    // Info
    $(".control-info").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-info-600 text-info-800'
    });

    // Custom color
    $(".control-custom").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-indigo-600 text-indigo-800'
    });


    $.post('../../sys/session.php').done(function(data) {
        data = JSON.parse(data);
        $('#un_name').html(data.user_name);
        $('#un_ident').html(data.user_connected);
        $('#un_pic').attr('src', data.user_pic);
        $('#nv_uname').html(data.user_name);
        $('#nv_upic').attr('src', data.user_pic);
        $('#ch_username').val(data.user_connected);
    });

    $.post('./api/profile.php', JSON.stringify(['get_user_data']), function(data) {
        //console.log(data);
        $('#name').val(data[0].name);
        $('#subname').val(data[0].subname);
        $('#address1').val(data[0].address1);
        $('#mobile').val(data[0].phone);
        $('#mobile_domi').val(data[0].domicile);
        $('#mobile_pro').val(data[0].professionel);
        $('#ville').val(data[0].vill);
        $('#pays').val(data[0].pays);
        $('#codezip').val(data[0].codezip);
        $('#email').val(data[0].email);
        $('#u_li').attr('href', data[0].soc1);
        $('#u_tw').attr('href', data[0].soc2);
        $('#u_fb').attr('href', data[0].soc3);
    });

    $.post('/GTRANS/public/users/api/profile.php', JSON.stringify(['get_list_users']), function(data) {
        data.forEach(function(element) {
            var ttim, clsa, picpro, ttip = '';
            element.disconnect < element.lastlogin ? ttim = element.lastlogin : ttim = element.disconnect;
            element.disconnect < element.lastlogin ? clsa = 'border-success' : clsa = 'border-slate-800';
            element.disconnect < element.lastlogin ? ttip = 'Connecté' : ttip = 'Déconnecté';
            switch (element.avatar) {
                case null:
                    picpro = '/GTRANS/assets/images/default-img.png';
                    break;

                default:
                    picpro = ['/GTRANS/public/users/user_data', element.username, element.avatar].join('/');
                    break;
            }

            var htt = '<li class="media" data-popup="tooltip" title="' + ttip + '" data-placement="right" data-original-title="Right tooltip">\
                            <a class="media-left"><img src="' + picpro + '" class="img-sm img-circle" alt=""></a>\
                            <div class="media-body">\
                                <a uo="' + element.username + '" data-toggle="modal" data-target="#PROF_INFO" class="media-heading text-semibold uoc">' + element.name + ' ' + element.subname + '</a>\
                                <span class="text-size-mini text-muted display-block contime" title="' + ttim + '"></span>\
                            </div>\
                            <div class="media-right media-middle">\
                                <span class="status-mark ' + clsa + '"></span>\
                            </div>\
                        </li>';
            $('#online_us').append(htt);
        }, this);
        $("span.contime").timeago();
        $('a.uoc').on('click', function() {
            $.post('/GTRANS/public/users/api/profile.php', JSON.stringify(['get_uo_info', $(this).attr('uo')]), function(data) {
                switch (data[0].avatar) {
                    case null:
                        $('#uo_pici').attr('src', '/GTRANS/assets/images/default-img.png');
                        break;

                    default:
                        $('#uo_pici').attr('src', ['/GTRANS/public/users/user_data', data[0].username, data[0].avatar].join('/'));
                        break;
                }
                $('#uo_name').html(data[0].name + ' ' + data[0].subname);
                $('#uo_mail').html(data[0].email);
                $('#uo_phone').html(data[0].phone);
                $('#uo_prof').html(data[0].professionel);
                $('#uo_domi').html(data[0].domicile);
                $('#uo_addr').html([data[0].address1, data[0].vill, data[0].pays, data[0].codezip].join(', '));
                $('#uo_soc1').attr('href', data[0].soc1);
                $('#uo_soc2').attr('href', data[0].soc2);
                $('#uo_soc3').attr('href', data[0].soc3);
            });
        });
    });

    $('#get_mail_seti').on('click', function() {
        $.getJSON('https://emailsettings.firetrust.com/settings?q=' + $('#smtp_user').val()).done(function(data) {
            var mail_data = data.settings;
            var imap, smtp = false;
            mail_data.forEach(function(seti) {
                if (seti.protocol == 'IMAP') {
                    $('#imap_host').val(seti.address);
                    $('#imap_port').val(seti.port);
                    imap = true;
                } else if (seti.protocol == 'SMTP') {
                    $('#smtp_host').val(seti.address);
                    $('#smtp_port').val(seti.port);
                    $('#smtp_crypto').val(seti.secure.toLowerCase());
                    smtp = true;
                }
            }, this);
            if (!imap) {
                noty({
                    text: '<div class="text-center">nous ne pouvons pas saisir vos informations de courrier IMAP, veuillez vérifier que votre courrier est public ou mettre les informations manuellement</div>',
                    type: 'warning',
                    dismissQueue: true,
                    timeout: 6000,
                    layout: 'topRight'
                });
            } else if (!smtp) {
                noty({
                    text: '<div class="text-center">nous ne pouvons pas saisir vos informations de courrier SMTP, veuillez vérifier que votre courrier est public ou mettre les informations manuellement</div>',
                    type: 'warning',
                    dismissQueue: true,
                    timeout: 6000,
                    layout: 'topRight'
                });
            }
        }).fail(function(error) {
            noty({
                text: '<div class="text-center">nous ne pouvons pas saisir vos informations de courrier, veuillez vérifier que votre courrier est public ou mettre les informations manuellement</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });
});