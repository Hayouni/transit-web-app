$(document).ready(function() {
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href");
        if ((target == '#tab_inbox')) {
            $("#read_mail").hide();
            $("#list_mail").fadeIn();
        } else if ((target == '#tab_draft')) {
            $("#read_mail_draft").hide();
            $("#list_mail_draft").fadeIn();
        } else if ((target == '#tab_sent')) {
            $("#read_mail_sent").hide();
            $("#list_mail_sent").fadeIn();
        } else if ((target == '#tab_spam')) {
            $("#read_mail_spam").hide();
            $("#list_mail_spam").fadeIn();
        } else if ((target == '#tab_trash')) {
            $("#read_mail_trash").hide();
            $("#list_mail_trash").fadeIn();
        }
    });

    $('.summernote').summernote({ lang: "fr-FR" });
    Dropzone.autoDiscover = false;
    $("#dropzone_remove").dropzone({
        paramName: "file", // The name that will be used to transfer the file
        dictDefaultMessage: 'Déposez les fichiers à joindre <span> ou cliquez </span>',
        maxFilesize: 1, // MB
        addRemoveLinks: true
    });

    function init_checkbox() {
        // Plugins
        // ------------------------------

        // Default initialization
        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });

        // Initialize Row link plugin
        $('tbody.rowlink').rowlink();

        $(".styled, .multiselect-container input").uniform({
            radioClass: 'choice'
        });
        // Primary
        $(".control-primary").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $(".control-danger").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $(".control-success").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Warning
        $(".control-warning").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-warning-600 text-warning-800'
        });

        // Info
        $(".control-info").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-info-600 text-info-800'
        });

        // Custom color
        $(".control-custom").uniform({
            radioClass: 'choice',
            wrapperClass: 'border-indigo-600 text-indigo-800'
        });
    }

    function get_counter() {
        $.post('/GTRANS/public/users/api/mail.php', JSON.stringify(['get_counter_for_mail'])).done((data) => {
            $('#INBXCOUNT').text(data.unseen);
            $('.nv_mail_data_co').text(data.unseen);
            $('#INBXCOUNT_headtab').text(data.unseen + ' Email non lu');
            $('#SPAMCOUNT').text(data.spam_count);
        }).fail((error) => {
            console.log('error : ', error);
        });
    }

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        //dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">p',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: '<i class="icon-database-remove"></i> aucun e-mail trouvé',
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "Chargement...",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
    });

    var table = $('#table_inbox').DataTable({
        select: {
            style: 'multi'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        info: false,
        paging: true,
        ajax: {
            url: "/GTRANS/public/users/api/mail.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_mail_data"
                });
            }
        },
        columns: [{
            data: "mail_id",
            className: "table-inbox-checkbox rowlink-skip td_w",
            render: function(data, type, row) {
                return '<input type="checkbox" mail-id="' + row.mail_id + '" class="styled">';
            }
        }, {
            data: null,
            className: "table-inbox-image td_w",
            defaultContent: '<span class="btn bg-warning-400 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>',
            render: function(data, type, row) {
                switch (data.mail_seen) {
                    case '0':
                        return '<span class="clixc btn bg-primary-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';

                    default:
                        return '<span class="clixc btn bg-slate-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';
                }
            }
        }, {
            data: "mail_from",
            className: "table-inbox-name",
            render: function(data, type, row) {
                return '<a class="readme" mail-id="' + row.mail_id + '">\
                    <div class="letter-icon-title text-default">' + row.mail_from_name + '</div><span>' + row.mail_from + '</span>\
                    <span class="table-inbox-preview">' + row.mail_subject + '</span></a>';
            }
        }, {
            data: "mail_date",
            className: "table-inbox-time",
            render: function(data, type, row) {
                return row.mail_date;
            }
        }],
        fnCreatedRow: function(nRow, aData, iDataIndex) {
            // Bold the grade for all 'A' grade browsers
            if (aData.mail_seen === '0')
                $(nRow).addClass("unread");
        },
        fnDrawCallback: function(oSettings) {
            InitTablestyles();
            init_checkbox();
        },
        initComplete: function() {
            InitTablestyles();
            init_checkbox();
            get_counter();
        },
    });

    function InitTablestyles() {
        $(".readme").on('click', function(e) {
            var me = $(this);
            e.preventDefault();
            if (me.data('requestRunning')) {
                return;
            }
            me.data('requestRunning', true);
            var data = table.row($(this).parents('tr')).data();
            if ($(this).parents('tr').hasClass('unread')) {
                $(this).parents('tr').removeClass('unread');
                $(this).parents('tr').find('span.clixc')
                    .removeClass('bg-success-400 bg-indigo-400')
                    .addClass('bg-indigo-400');
                $.post('/GTRANS/public/users/api/mail.php', JSON.stringify(['SET_SEEN', data.mail_id])).done(() => {
                    me.data('requestRunning', false);
                });
            } else {
                me.data('requestRunning', false);
            }
            $('#iframe_data_read').contents().find('body')
                .empty()
                .append(data.mail_body);
            $('#mail_data_subject').text(data.mail_subject);
            $('#time_data_read').text(data.mail_date);
            $('#mail_data_name').html(data.mail_from_name + '<a href="#">&lt;' + data.mail_from + '&gt;</a>');
            var $title = $('#mail_data_name'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $title.parent().parent().find('#lic_data_read');
            $icon.eq(0).text(letter);
            $("#list_mail").hide();
            $("#read_mail").fadeIn();
        });

        $('#back_data_reader').on('click', function() {
            $('#iframe_data_read').contents().find('body')
                .empty();
            $("#read_mail").hide();
            $("#list_mail").fadeIn();
        });

        // Highlight row when checkbox is checked
        $('#table_inbox').find('tr > td:first-child').find('input[type=checkbox]').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parents('tr').addClass('selected warning');
            } else {
                $(this).parents('tr').removeClass('selected warning');
            }
        });

        // Grab first letter and insert to the icon
        $("#table_inbox tr").each(function(i) {

            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase() == '' ? '?' : $title.eq(0).text().charAt(0).toUpperCase();

            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });
    }

    $('#reload_mail_data').on('click', function() {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_data'])).done(function(data) {
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            if (data.error) {
                noty({
                    text: '<div class="text-center">problème lors de la connexion du courrier vérifier votre configuration</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 5000,
                    layout: 'topRight'
                });
                return false;
            }
            switch (data.count) {
                case 0:
                    noty({
                        text: '<div class="text-center">pas de nouveau courriel</div>',
                        type: 'information',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    break;

                default:
                    noty({
                        text: '<div class="text-center">' + data.count + ' nouveaux courriels reçus</div>',
                        type: 'success',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    table.ajax.reload();
                    get_counter();
                    break;
            }
        }).fail(function(error) {
            console.log('error : ', error);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            noty({
                text: '<div class="text-center">erreur lors de la récupération des e-mails, veuillez contacter l\'administrateur</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });

    $('#del_mail_data').on('click', function(e) {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        var me = $(this);
        e.preventDefault();
        if (me.data('requestRunning')) {
            return;
        }
        me.data('requestRunning', true);
        var da = table.rows('.selected').data();
        for (var i = 0; i < da.length; i++) {
            $.post('/GTRANS/public/users/api/mail.php', JSON.stringify(['SET_TRASH', da[i].mail_id])).done(function(data) {
                table.ajax.reload();
                get_counter();
                me.data('requestRunning', false);
                ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            }).fail(() => {
                console.log('error : ', error);
                me.data('requestRunning', false);
                ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
                noty({
                    text: '<div class="text-center">erreur lors de la suppression du courrier, veuillez contacter l\'administrateur</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 4000,
                    layout: 'topRight'
                });
            });
        }
        if (da.length === 0) {
            me.data('requestRunning', false);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
        }
    });

    $('#spam_mail_data').on('click', function(e) {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        var me = $(this);
        e.preventDefault();
        if (me.data('requestRunning')) {
            return;
        }
        me.data('requestRunning', true);
        var da = table.rows('.selected').data();
        for (var i = 0; i < da.length; i++) {
            $.post('/GTRANS/public/users/api/mail.php', JSON.stringify(['SET_SPAM', da[i].mail_id])).done(function(data) {
                table.ajax.reload();
                get_counter();
                me.data('requestRunning', false);
                ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            }).fail(() => {
                console.log('error : ', error);
                me.data('requestRunning', false);
                ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
                noty({
                    text: '<div class="text-center">erreur lors de la suppression du courrier, veuillez contacter l\'administrateur</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 4000,
                    layout: 'topRight'
                });
            });
        }
        if (da.length === 0) {
            me.data('requestRunning', false);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
        }
    });

    /** Mail Draft Part */
    var table_draft = $('#table_draft').DataTable({
        select: {
            style: 'multi'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        info: false,
        paging: true,
        ajax: {
            url: "/GTRANS/public/users/api/mail.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_mail_draft"
                });
            }
        },
        columns: [{
            data: "mail_id",
            className: "table-inbox-checkbox rowlink-skip td_w",
            render: function(data, type, row) {
                return '<input type="checkbox" mail-id="' + row.mail_id + '" class="styled">';
            }
        }, {
            data: null,
            className: "table-inbox-image td_w",
            defaultContent: '<span class="clixc btn bg-info-600 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>',
        }, {
            data: "mail_from",
            className: "table-inbox-name",
            render: function(data, type, row) {
                return '<a class="readme-draft" mail-id="' + row.mail_id + '">\
                    <div class="letter-icon-title text-default">' + row.mail_from_name + '</div><span>' + row.mail_from + '</span>\
                    <span class="table-inbox-preview">' + row.mail_subject + '</span></a>';
            }
        }, {
            data: "mail_date",
            className: "table-inbox-time",
            render: function(data, type, row) {
                return row.mail_date;
            }
        }],
        fnCreatedRow: function(nRow, aData, iDataIndex) {
            // Bold the grade for all 'A' grade browsers
            if (aData.mail_seen === '0')
                $(nRow).addClass("unread");
        },
        fnDrawCallback: function(oSettings) {
            InitTablestyles_draft();
            init_checkbox()
        },
        initComplete: function() {
            InitTablestyles_draft();
            init_checkbox()
        },
    });

    function InitTablestyles_draft() {
        $(".readme-draft").on('click', function() {
            var data = table_draft.row($(this).parents('tr')).data();
            $('#iframe_draft_read').contents().find('body')
                .empty()
                .append(data.mail_body);
            $('#mail_draft_subject').text(data.mail_subject);
            $('#time_draft_read').text(data.mail_date);
            $('#mail_draft_name').html(data.mail_from_name + '<a href="#">&lt;' + data.mail_from + '&gt;</a>');
            var $title = $('#mail_draft_name'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $title.parent().parent().find('#lic_data_read');
            $icon.eq(0).text(letter);
            $("#list_mail_draft").hide();
            $("#read_mail_draft").fadeIn();
        });

        $('#back_draft_reader').on('click', function() {
            $('#iframe_draft_read').contents().find('body')
                .empty();
            $("#read_mail_draft").hide();
            $("#list_mail_draft").fadeIn();
        });

        // Highlight row when checkbox is checked
        $('#table_draft').find('tr > td:first-child').find('input[type=checkbox]').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parents('tr').addClass('selected warning');
            } else {
                $(this).parents('tr').removeClass('selected warning');
            }
        });

        // Grab first letter and insert to the icon
        $("#table_draft tr").each(function(i) {

            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase() == '' ? '?' : $title.eq(0).text().charAt(0).toUpperCase();

            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });
    }

    $('#reload_mail_draft').on('click', function() {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_draft'])).done(function(data) {
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            if (data.error) {
                noty({
                    text: '<div class="text-center">problème lors de la connexion du courrier vérifier votre configuration</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 5000,
                    layout: 'topRight'
                });
                return false;
            }
            switch (data.count) {
                case 0:
                    noty({
                        text: '<div class="text-center">aucun brouillon trouvé</div>',
                        type: 'information',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    break;

                default:
                    noty({
                        text: '<div class="text-center">' + data.count + ' brouillon trouvé</div>',
                        type: 'success',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    table_draft.ajax.reload();
                    break;
            }
        }).fail(function(error) {
            console.log('error : ', error);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            noty({
                text: '<div class="text-center">erreur lors de la récupération des e-mails, veuillez contacter l\'administrateur</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });

    /** Mail sent Part */
    var table_sent = $('#table_sent').DataTable({
        select: {
            style: 'multi'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        info: false,
        paging: true,
        ajax: {
            url: "/GTRANS/public/users/api/mail.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_mail_sent"
                });
            }
        },
        columns: [{
            data: "mail_id",
            className: "table-inbox-checkbox rowlink-skip td_w",
            render: function(data, type, row) {
                return '<input type="checkbox" mail-id="' + row.mail_id + '" class="styled">';
            }
        }, {
            data: null,
            className: "table-inbox-image td_w",
            defaultContent: '<span class="clixc btn bg-success-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>',
        }, {
            data: "mail_from",
            className: "table-inbox-name",
            render: function(data, type, row) {
                return '<a class="readme-sent" mail-id="' + row.mail_id + '">\
                        <div class="letter-icon-title text-default">' + row.mail_from_name + '</div><span>' + row.mail_from + '</span>\
                        <span class="table-inbox-preview">' + row.mail_subject + '</span></a>';
            }
        }, {
            data: "mail_date",
            className: "table-inbox-time",
            render: function(data, type, row) {
                return row.mail_date;
            }
        }],
        fnCreatedRow: function(nRow, aData, iDataIndex) {
            // Bold the grade for all 'A' grade browsers
            if (aData.mail_seen === '0')
                $(nRow).addClass("unread");
        },
        fnDrawCallback: function(oSettings) {
            InitTablestyles_sent();
            init_checkbox()
        },
        initComplete: function() {
            InitTablestyles_sent();
            init_checkbox()
        },
    });

    function InitTablestyles_sent() {
        $(".readme-sent").on('click', function() {
            var data = table_sent.row($(this).parents('tr')).data();
            $('#iframe_sent_read').contents().find('body')
                .empty()
                .append(data.mail_body);
            $('#mail_sent_subject').text(data.mail_subject);
            $('#time_sent_read').text(data.mail_date);
            $('#mail_sent_name').html(data.mail_from_name + '<a href="#">&lt;' + data.mail_from + '&gt;</a>');
            var $title = $('#mail_sent_name'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $title.parent().parent().find('#lic_data_read');
            $icon.eq(0).text(letter);
            $("#list_mail_sent").hide();
            $("#read_mail_sent").fadeIn();
        });

        $('#back_sent_reader').on('click', function() {
            $('#iframe_sent_read').contents().find('body')
                .empty();
            $("#read_mail_sent").hide();
            $("#list_mail_sent").fadeIn();
        });

        // Highlight row when checkbox is checked
        $('#table_sent').find('tr > td:first-child').find('input[type=checkbox]').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parents('tr').addClass('selected warning');
            } else {
                $(this).parents('tr').removeClass('selected warning');
            }
        });

        // Grab first letter and insert to the icon
        $("#table_sent tr").each(function(i) {

            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase() == '' ? '?' : $title.eq(0).text().charAt(0).toUpperCase();

            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });
    }

    $('#reload_mail_sent').on('click', function() {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_sent'])).done(function(data) {
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            if (data.error) {
                noty({
                    text: '<div class="text-center">problème lors de la connexion du courrier vérifier votre configuration</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 5000,
                    layout: 'topRight'
                });
                return false;
            }
            switch (data.count) {
                case 0:
                    noty({
                        text: '<div class="text-center">aucun brouillon trouvé</div>',
                        type: 'information',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    break;

                default:
                    noty({
                        text: '<div class="text-center">' + data.count + ' brouillon trouvé</div>',
                        type: 'success',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    table_sent.ajax.reload();
                    break;
            }
        }).fail(function(error) {
            console.log('error : ', error);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            noty({
                text: '<div class="text-center">erreur lors de la récupération des e-mails, veuillez contacter l\'administrateur</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });

    /** Mail spam Part */
    var table_spam = $('#table_spam').DataTable({
        select: {
            style: 'multi'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        info: false,
        paging: true,
        ajax: {
            url: "/GTRANS/public/users/api/mail.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_mail_spam"
                });
            }
        },
        columns: [{
            data: "mail_id",
            className: "table-inbox-checkbox rowlink-skip td_w",
            render: function(data, type, row) {
                return '<input type="checkbox" mail-id="' + row.mail_id + '" class="styled">';
            }
        }, {
            data: null,
            className: "table-inbox-image td_w",
            defaultContent: '<span class="clixc btn bg-success-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>',
            render: function(data, type, row) {
                switch (data.mail_seen) {
                    case '0':
                        return '<span class="clixc btn bg-danger-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';

                    default:
                        return '<span class="clixc btn bg-slate-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';
                }
            }
        }, {
            data: "mail_from",
            className: "table-inbox-name",
            render: function(data, type, row) {
                return '<a class="readme-spam" mail-id="' + row.mail_id + '">\
                            <div class="letter-icon-title text-default">' + row.mail_from_name + '</div><span>' + row.mail_from + '</span>\
                            <span class="table-inbox-preview">' + row.mail_subject + '</span></a>';
            }
        }, {
            data: "mail_date",
            className: "table-inbox-time",
            render: function(data, type, row) {
                return row.mail_date;
            }
        }],
        fnCreatedRow: function(nRow, aData, iDataIndex) {
            // Bold the grade for all 'A' grade browsers
            if (aData.mail_seen === '0')
                $(nRow).addClass("unread");
        },
        fnDrawCallback: function(oSettings) {
            InitTablestyles_spam();
            init_checkbox()
        },
        initComplete: function() {
            InitTablestyles_spam();
            init_checkbox()
        },
    });

    function InitTablestyles_spam() {
        $(".readme-spam").on('click', function() {
            var data = table_spam.row($(this).parents('tr')).data();
            $('#iframe_spam_read').contents().find('body')
                .empty()
                .append(data.mail_body);
            $('#mail_spam_subject').text(data.mail_subject);
            $('#time_spam_read').text(data.mail_date);
            $('#mail_spam_name').html(data.mail_from_name + '<a href="#">&lt;' + data.mail_from + '&gt;</a>');
            var $title = $('#mail_spam_name'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $title.parent().parent().find('#lic_data_read');
            $icon.eq(0).text(letter);
            $("#list_mail_spam").hide();
            $("#read_mail_spam").fadeIn();
        });

        $('#back_spam_reader').on('click', function() {
            $('#iframe_spam_read').contents().find('body')
                .empty();
            $("#read_mail_spam").hide();
            $("#list_mail_spam").fadeIn();
        });

        // Highlight row when checkbox is checked
        $('#table_spam').find('tr > td:first-child').find('input[type=checkbox]').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parents('tr').addClass('selected warning');
            } else {
                $(this).parents('tr').removeClass('selected warning');
            }
        });

        // Grab first letter and insert to the icon
        $("#table_spam tr").each(function(i) {

            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase() == '' ? '?' : $title.eq(0).text().charAt(0).toUpperCase();

            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });
    }

    $('#reload_mail_spam').on('click', function() {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_spam'])).done(function(data) {
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            if (data.error) {
                noty({
                    text: '<div class="text-center">problème lors de la connexion du courrier vérifier votre configuration</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 5000,
                    layout: 'topRight'
                });
                return false;
            }
            switch (data.count) {
                case 0:
                    noty({
                        text: '<div class="text-center">aucun brouillon trouvé</div>',
                        type: 'information',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    break;

                default:
                    noty({
                        text: '<div class="text-center">' + data.count + ' brouillon trouvé</div>',
                        type: 'success',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    table_spam.ajax.reload();
                    break;
            }
        }).fail(function(error) {
            console.log('error : ', error);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            noty({
                text: '<div class="text-center">erreur lors de la récupération des e-mails, veuillez contacter l\'administrateur</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });

    /** Mail trash Part */
    var table_trash = $('#table_trash').DataTable({
        select: {
            style: 'multi'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        info: false,
        paging: true,
        ajax: {
            url: "/GTRANS/public/users/api/mail.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_mail_trash"
                });
            }
        },
        columns: [{
            data: "mail_id",
            className: "table-inbox-checkbox rowlink-skip td_w",
            render: function(data, type, row) {
                return '<input type="checkbox" mail-id="' + row.mail_id + '" class="styled">';
            }
        }, {
            data: null,
            className: "table-inbox-image td_w",
            defaultContent: '<span class="clixc btn bg-success-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>',
            render: function(data, type, row) {
                switch (data.mail_seen) {
                    case '0':
                        return '<span class="clixc btn bg-warning-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';

                    default:
                        return '<span class="clixc btn bg-slate-700 btn-rounded btn-icon btn-xs"><span class="letter-icon"></span></span>';
                }
            }
        }, {
            data: "mail_from",
            className: "table-inbox-name",
            render: function(data, type, row) {
                return '<a class="readme-spam" mail-id="' + row.mail_id + '">\
                            <div class="letter-icon-title text-default">' + row.mail_from_name + '</div><span>' + row.mail_from + '</span>\
                            <span class="table-inbox-preview">' + row.mail_subject + '</span></a>';
            }
        }, {
            data: "mail_date",
            className: "table-inbox-time",
            render: function(data, type, row) {
                return row.mail_date;
            }
        }],
        fnCreatedRow: function(nRow, aData, iDataIndex) {
            // Bold the grade for all 'A' grade browsers
            if (aData.mail_seen === '0')
                $(nRow).addClass("unread");
        },
        fnDrawCallback: function(oSettings) {
            InitTablestyles_trash();
            init_checkbox()
        },
        initComplete: function() {
            InitTablestyles_trash();
            init_checkbox()
        },
    });

    function InitTablestyles_trash() {
        $(".readme-spam").on('click', function() {
            var data = table_trash.row($(this).parents('tr')).data();
            $('#iframe_trash_read').contents().find('body')
                .empty()
                .append(data.mail_body);
            $('#mail_trash_subject').text(data.mail_subject);
            $('#time_trash_read').text(data.mail_date);
            $('#mail_trash_name').html(data.mail_from_name + '<a href="#">&lt;' + data.mail_from + '&gt;</a>');
            var $title = $('#mail_trash_name'),
                letter = $title.eq(0).text().charAt(0).toUpperCase();
            // Icon
            var $icon = $title.parent().parent().find('#lic_data_read');
            $icon.eq(0).text(letter);
            $("#list_mail_trash").hide();
            $("#read_mail_trash").fadeIn();
        });

        $('#back_trash_reader').on('click', function() {
            $('#iframe_trash_read').contents().find('body')
                .empty();
            $("#read_mail_trash").hide();
            $("#list_mail_trash").fadeIn();
        });

        // Highlight row when checkbox is checked
        $('#table_trash').find('tr > td:first-child').find('input[type=checkbox]').on('change', function() {
            if ($(this).is(':checked')) {
                $(this).parents('tr').addClass('selected warning');
            } else {
                $(this).parents('tr').removeClass('selected warning');
            }
        });

        // Grab first letter and insert to the icon
        $("#table_trash tr").each(function(i) {

            // Title
            var $title = $(this).find('.letter-icon-title'),
                letter = $title.eq(0).text().charAt(0).toUpperCase() == '' ? '?' : $title.eq(0).text().charAt(0).toUpperCase();

            // Icon
            var $icon = $(this).find('.letter-icon');
            $icon.eq(0).text(letter);
        });
    }

    $('#reload_mail_trash').on('click', function() {
        var ico = $(this).find('i');
        var oldclass = ico.attr("class");
        ico.removeClass(oldclass).addClass('icon-spinner3 spinner');
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_trash'])).done(function(data) {
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            if (data.error) {
                noty({
                    text: '<div class="text-center">problème lors de la connexion du courrier vérifier votre configuration</div>',
                    type: 'error',
                    dismissQueue: true,
                    timeout: 5000,
                    layout: 'topRight'
                });
                return false;
            }
            switch (data.count) {
                case 0:
                    noty({
                        text: '<div class="text-center">aucun brouillon trouvé</div>',
                        type: 'information',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    break;

                default:
                    noty({
                        text: '<div class="text-center">' + data.count + ' brouillon trouvé</div>',
                        type: 'success',
                        dismissQueue: true,
                        timeout: 4000,
                        layout: 'topRight'
                    });
                    table_trash.ajax.reload();
                    break;
            }
        }).fail(function(error) {
            console.log('error : ', error);
            ico.removeClass('icon-spinner3 spinner').addClass(oldclass);
            noty({
                text: '<div class="text-center">erreur lors de la récupération des e-mails, veuillez contacter l\'administrateur</div>',
                type: 'error',
                dismissQueue: true,
                timeout: 4000,
                layout: 'topRight'
            });
        });
    });

    $("#upload_link").on('click', function(e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
    });
});