<?php
/*include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
	exit();
}*/
?><?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {
    case 'get_user_data':
        $db = new MySQL();
        $get_user_data = $db->get_results("SELECT * FROM users WHERE username = '".$_SESSION['username']."'");
        echo json_encode($get_user_data);
        break;

    case 'get_user_cal_events':
        $db = new MySQL();
        $get_user_cal_events = $db->get_results("SELECT id, title, start, end, color FROM events WHERE username = '".$_SESSION['username']."' AND DATE(start) = CURDATE() AND vu = 0");
        echo json_encode($get_user_cal_events);
        break;

    case 'make_events_seen':
        $db = new MySQL();
        $update_where = array( 'DATE(start)' => date("Y-m-d"), 'username' => $_SESSION['username']);
        $update_data = array('vu' => '1');
        echo $db->update( 'events', $update_data, $update_where );
        break;
        
    case 'get_list_users':
        $db = new MySQL();
        $get_list_users = $db->get_results("SELECT username,avatar,name,subname,lastlogin,disconnect FROM users WHERE activated = '1' AND username NOT LIKE '".$_SESSION['username']."'");
        echo json_encode($get_list_users);
        break;
    
    case 'get_uo_info':
        $db = new MySQL();
        $get_uo_info = $db->get_results("SELECT username,avatar,name,subname,email,phone,professionel,domicile,address1,vill,pays,codezip,soc1,soc2,soc3 FROM users WHERE username = '$response[1]'");
        echo json_encode($get_uo_info);
        break;

    case 'get_todo':
        $db = new MySQL();
        $get_todo = $db->get_results("SELECT * FROM `tab_todo` WHERE user_id = '09700095'");
        echo json_encode($get_todo);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>