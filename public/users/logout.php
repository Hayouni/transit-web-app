<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//update disconnect
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
$db = new MySQL();
$update_where = array('username' => $_SESSION['username']);
$update_data = array('disconnect' => date("Y-m-d H:i:s"),'lock_account' => '0');

if($db->update( 'users', $update_data, $update_where )){
	// Set Session data to an empty array
	$_SESSION = array();
	// Expire their cookie files
	if(isset($_COOKIE["id"]) && isset($_COOKIE["user"]) && isset($_COOKIE["pic"]) && isset($_COOKIE["fname"])) {
		setcookie("id", '', strtotime( '-5 days' ), '/');
		setcookie("user", '', strtotime( '-5 days' ), '/');
		setcookie("pic", '', strtotime( '-5 days' ), '/');
		setcookie("fname", '', strtotime( '-5 days' ), '/');
	}
	// Destroy the session variables
	session_destroy();
	// Double check to see if their sessions exists
	if(isset($_SESSION['username'])){
		header("location: message.php?msg=Error:_Logout_Failed");
	} else {
		header("location: /GTRANS");
		exit();
	} 
} else {
	header("location: message.php?msg=Error:_Logout_Failed");
} 

?>