<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Liste M.Brute</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/forms/inputs/autosize.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/inputs/formatter.min.js"></script>

    <!--script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script-- type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script-->

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.html5.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/visualization/canvasjs/canvasjs.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/lmbrut.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }

        .btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary {
        background-color: #6e7479 !important;
        border-color: #2196f3;
        }

        .nav-tabs.nav-justified.nav-tabs-highlight2>li>a, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:focus, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:hover {
            border-top-width: 2px;
        }

        .nav-tabs.nav-tabs-highlight2>li.active>a, .nav-tabs.nav-tabs-highlight2>li.active>a:focus, .nav-tabs.nav-tabs-highlight2>li.active>a:hover {
            border-top-color: #5d4037;
        }

        .dt-buttons>.btn {
            border-radius: 0;
            border-left-color: white;
        }
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Achat</span> - Liste Marge Brute des Dossier </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Recherche</h6>
                                </div>

                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="display-block">Plage de N° Dossier </label>
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <input name="SRC_PDOS_INP1" id="SRC_PDOS_INP1" type="text" data-mask="9999999999" class="dosinpfilter form-control border-indigo border-lg text-indigo text-center">
                                                        <span class="help-block">format 10 numéros 9999999999</span>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <input name="SRC_PDOS_INP2" id="SRC_PDOS_INP2" type="text" data-mask="9999999999" class="dosinpfilter form-control border-indigo border-lg text-indigo text-center">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-indigo btn-icon btn-rounded" id="get_dostab_btn"><i class="icon-search4"></i></button>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="display-block">Sélectionnez la plage de dates </label>
                                                <button type="button" class="btn bg-teal-400 daterange-ranges" id="daterange">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="get_tab_btn"><i class="icon-search4"></i></button>
                                        </div>
                                        <div class="col-lg-2">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table display datatable-button-print-rows table-striped table-bordered" id="table">
                                                <thead class="bg-teal-400">
                                                    <tr>
                                                        <th rowspan="2" class="text-center">TYPE</th>
                                                        <th rowspan="2" class="text-center">N°Dos</th>
                                                        <th colspan="4" class="text-center">VENTE</th>
                                                        <th colspan="3" class="text-center">ACHAT</th>
                                                        <th rowspan="2" class="text-center">Marge</th>
                                                    </tr>
                                                    <tr>
                                                        <th>F.LOC</th>
                                                        <th>F.ETR</th>
                                                        <th>F.AVOIR</th>
                                                        <th>TTC</th>
                                                        <th>F.LOC</th>
                                                        <th>F.ETR</th>
                                                        <th>TTC</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>TYPE</th>
                                                        <th>N°Dos</th>
                                                        <th>F.LOC</th>
                                                        <th>F.ETR</th>
                                                        <th>F.AVOIR</th>
                                                        <th>TTC</th>
                                                        <th>F.LOC</th>
                                                        <th>F.ETR</th>
                                                        <th>TTC</th>
                                                        <th>Marge</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>