<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
$get_doss = null;
if(isset($_GET['Srch']) && $_GET['Srch'] !== '' && is_numeric($_GET['Srch'])){
    $get_doss = $_GET['Srch'];
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Achat-Facture</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/fact.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>

    th.hide_me,
    td.hide_me {
        display: none;
    }

    .selected td {
        background-color: #545252 !important;
        color: #ff4242 !important;
        /* Add !important to make sure override datables base styles */
    }

    .background_blue {
        font-size: 13px;
        /*color: rgba(255, 255, 255, 1);
        background: #2196f3;*/
        color: rgb(0, 0, 0);
        background: #a9b8c3;
        font-weight: bold;
    }

    select.bootstrap-select1 {
        width: 100%;
        padding: 6px 11px;
        border: 1px solid #a9b8c3;
        height: 34px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background: #a9b8c3;
    }
    /* CAUTION: IE hackery ahead */
    
    select::-ms-expand {
        display: none;
        /* remove default arrow on ie10 and ie11 */
    }
    /* target Internet Explorer 9 to undo the custom arrow */
    
    @media screen and (min-width: 0\0) {
        select {
            background: none \9;
            padding: 5px \9;
        }
    }

    .btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary {
        background-color: #6e7479 !important;
        border-color: #2196f3;
    }

    .nav-tabs.nav-justified.nav-tabs-highlight2>li>a, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:focus, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:hover {
        border-top-width: 2px;
    }

    .nav-tabs.nav-tabs-highlight2>li.active>a, .nav-tabs.nav-tabs-highlight2>li.active>a:focus, .nav-tabs.nav-tabs-highlight2>li.active>a:hover {
        border-top-color: #5d4037;
    }

    .dt-buttons>.btn {
        border-radius: 0;
        border-left-color: white;
    }

    .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
            /*border-width: 2px !important;
            border-color: #000000 !important;
            border: 1px solid #ddd !important;
            border-radius: 3px !important;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            /* border-bottom-right-radius: 0; */
            /* border-bottom-left-radius: 0; */
            /* background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #eee), color-stop(80%, #fff)); */
            /* background-image: linear-gradient(#eee 20%, #fff 80%); */
            /* -webkit-box-shadow: 0 1px 0 #fff inset; */
            /* box-shadow: 0 1px 0 #fff inset; */
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }
        
    </style>

    <script>
        var NumD = "<?= $get_doss; ?>";
    </script>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Achat</span> - Facture
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Facture Locale et Etrnager</h6>
                                </div>

                                <div class="panel-body">

                                    <div class="row"> 
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4 text-center">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button class="btn bg-slate-700" id="open_dos"><i class="icon-folder-search"></i></button>
												</span>
												<input type="text" class="form-control bg-slate text-center" id="DOS_NUM" placeholder="N°..." value="<?= $get_doss; ?>">
                                                <span class="input-group-btn">
                                                    <button class="btn bg-slate-700" id="get_dos"><i class="icon-file-download2"></i></button>
												</span>
                                            </div>
                                            <br>
                                            <span class="label label-block" id="DOS_TYPE" style="background-color: #455a64; border-color: #455a64; color: #fff;">Entrez le numéro de dossier</span>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <br>
                                    <div class="row text-center"> 
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Navire :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="Navire" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>BL/LTA :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="BL_LTA" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>POD :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="POD" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>POL :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="POL" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Date Embarquement :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="DEMB" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Date Déchargement :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="DDECH" placeholder="..." disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center"> 
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>MARCHANDISE :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="MARCH" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>NOMBRE :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="NOMBRE" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>MARQUE :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="MARQUE" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>POIDS :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="POIDS" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>ESCALE :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="ESCALE" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>RUBRIQUE :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="RuBRIQUE" placeholder="..." disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row text-center"> 
                                        <div class="col-md-12" id="complet_info">
                                           
                                        </div>
                                    </div>
                                    <br><br><br>

                                    <!-- FACTURES ETRANGERES -->
                                    <div class="panel panel-default border-grey">
                                        <div class="panel-heading">
                                            <h6 class="panel-title text-indigo-800 text-bold"><i class="icon-coins"></i> &nbsp;FACTURES ETRANGERES</h6>
                                        </div>
                                                                           
                                        <div class="panel-body">
                                            <table class="table datatable-button-print-rows" id="TAB_ETRANGE">
                                                <thead class="bg-info-700">
                                                    <tr>
                                                        <th class="hide_me">key</th>
                                                        <th>Agent</th>
                                                        <th>N°.Fatcure</th>
                                                        <th>Date</th>
                                                        <th>Devise</th>
                                                        <th>Montant</th>
                                                        <th>DT</th>
                                                        <th>Règlement</th>
                                                        <th>N°.Virement</th>
                                                        <th>Date.Virement</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="bg-slate-700">

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="hide_me">key</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th colspan="3" class="text-right"></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /FACTURES ETRANGERES -->
                                    
                                    <!-- FACTURES LOCALES -->
                                    <div class="panel panel-default border-grey">
                                        <div class="panel-heading">
                                            <h6 class="panel-title text-teal-800 text-bold"><i class="icon-price-tags2"></i> &nbsp;FACTURES LOCALES</h6>
                                        </div>
                                                                           
                                        <div class="panel-body">
                                            <table class="table datatable-button-print-rows" id="TAB_LOCALE">
                                                <thead class="bg-info-700">
                                                    <tr>
                                                        <th class="hide_me">key</th>
                                                        <th>Fournisseur</th>
                                                        <th>N°.Fatcure</th>
                                                        <th>Date</th>
                                                        <th>H.T</th>
                                                        <th>T.V.A</th>
                                                        <th>Timbre</th>
                                                        <th>T.T.C</th>
                                                        <th>Règlement</th>
                                                        <th>Numéro</th>
                                                        <th>Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="bg-slate-700">

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="hide_me">key</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th colspan="3" class="text-right"></th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /FACTURES LOCALES -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- modal_FETR modal -->
					<div id="modal_FETR" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-blue-700">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"><i class="icon-coins"></i> &nbsp;AJOUTER FACTURE ETRANGERE</h5>
								</div>
								<div class="modal-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Agent Entranger :</label>
                                                    <select class="form-control border-black border-lg text-black livesearch" id="agent_etrange" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>N° Facture :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_FET" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Date Facture :</label>
                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_FET" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Type Devise :</label>
                                                    <input type="text" list="LIST_DEVIS" class="form-control border-black border-lg text-black" id="TYPE_DETR" placeholder="..." value="EUR">
                                                    <datalist id="LIST_DEVIS">
                                                        <option value="EUR"></option>
                                                        <option value="USD"></option>
                                                        <option value="JPY"></option>
                                                        <option value="TRY"></option>
                                                        <option value="INR"></option>
                                                    </datalist>
                                                    <!--div class="btn-group" data-toggle="buttons">
                                                        <label class="btn btn-primary active">
                                                            <input type="radio" name="EUR" id="TYPE_DETR_EUR"> EUR
                                                        </label>

                                                        <label class="btn btn-primary">
                                                            <input type="radio" name="USD" id="TYPE_DETR_USD"> USD
                                                        </label>
                                                    </div-->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Montant Devise :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Mont_DEV" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Montant Dinar Tunisien :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Mont_DT" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <legend class="text-semibold">Règlement de Facture</legend>
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified" id="NV_ADDFET">
                                                <li class="active"><a href="#FET_BC" data-toggle="tab">BC</a></li>
                                                <li><a href="#FETC_VIR" data-toggle="tab">Virement</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="FET_BC">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            Pas de règlement
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FETC_VIR">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Virement :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FETC_NVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date Virement :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FETC_DVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
								</div>
                                <div class="modal-footer text-center">
									<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
									<button class="btn btn-primary" id="add_fetc"><i class="icon-check"></i> Ajouter</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /modal_FETR modal -->
                    <!-- modal_MFETR modal -->
					<div id="modal_MFETR" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-teal-700">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"><i class="icon-coins"></i> &nbsp;MODIFIER FACTURE ETRANGERE</h5>
								</div>
								<div class="modal-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Agent Entranger :</label>
                                                    <select class="form-control border-black border-lg text-black livesearch" id="m_agent_etrange" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>N° Facture :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_MFET" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Date Facture :</label>
                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_MFET" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Type Devise :</label>
                                                    <input type="text" list="LIST_DEVIS2" class="form-control border-black border-lg text-black" id="TYPE_MDETR" placeholder="..." value="EUR">
                                                    <datalist id="LIST_DEVIS2">
                                                        <option value="EUR"></option>
                                                        <option value="USD"></option>
                                                        <option value="JPY"></option>
                                                        <option value="TRY"></option>
                                                        <option value="INR"></option>
                                                    </datalist>
                                                    <!--div class="btn-group" data-toggle="buttons">
                                                        <label class="btn btn-primary active">
                                                            <input type="radio" name="EUR" id="TYPE_MDETR_EUR"> EUR
                                                        </label>

                                                        <label class="btn btn-primary">
                                                            <input type="radio" name="USD" id="TYPE_MDETR_USD"> USD
                                                        </label>
                                                    </div-->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Montant Devise :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Mont_MDEV" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Montant Dinar Tunisien :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Mont_MDT" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <legend class="text-semibold">Règlement de Facture</legend>
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified" id="NV_MODFET">
                                                <li class="active"><a href="#FET_MBC" data-toggle="tab">BC</a></li>
                                                <li><a href="#FETC_MVIR" data-toggle="tab">Virement</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="FET_MBC">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            Pas de règlement
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FETC_MVIR">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Virement :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FETC_MNVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date Virement :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FETC_MDVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
								</div>
                                <div class="modal-footer text-center">
									<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
									<button class="btn btn-primary" id="mod_fetc"><i class="icon-check"></i> Ajouter</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /modal_MFETR modal -->
                    <!-- modal_FLOC modal -->
					<div id="modal_FLOC" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-brown-700">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"><i class="icon-price-tags2"></i> &nbsp;AJOUTER FACTURES LOCALES</h5>
								</div>

								<div class="modal-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Client Locale :</label>
                                                    <select class="form-control border-black border-lg text-black livesearch" id="agent_local" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>N° Facture :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Date Facture :</label>
                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Hors Taxe [HT] :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="HT_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Timbre :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TIM_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>TVA :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TVA_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>TTC :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TTC_FLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="SURIST" style="display:none">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-switchery switchery-sm">
                                                        <label>
                                                            <input type="checkbox" class="switchery" id="IS_SURIST">
                                                            Facture Suristarie
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>CONTENEUR :</label>
                                                    <select class="form-control border-black border-lg text-black" id="SUR_CONTLIST" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <legend class="text-semibold">Règlement de Facture</legend>
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-highlight2 nav-justified" id="NVLOC">
                                                <li class="active"><a href="#FLOC_BC" data-toggle="tab">BC</a></li>
                                                <li><a href="#FLOC_ESP" data-toggle="tab">Espèce</a></li>
                                                <li><a href="#FLOC_CHQ" data-toggle="tab">Chèque</a></li>
                                                <li><a href="#FLOC_TRAIT" data-toggle="tab">Traite</a></li>
                                                <li><a href="#FLOC_VIR" data-toggle="tab">Virement</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="FLOC_BC">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            Pas de règlement
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_ESP">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_DESP" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_CHQ">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Chèque :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_NCHQ" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_DCHQ" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_TRAIT">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Traite :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_NTRAIT" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_DTRAIT" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_VIR">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Virement :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_NVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date Virement :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_DVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
								</div>

								<div class="modal-footer text-center">
									<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
									<button class="btn bg-brown-700" id="add_floc"><i class="icon-check"></i> Ajouter</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /modal_FLOC modal -->

                    <!-- modal_MFLOC modal -->
					<div id="modal_MFLOC" class="modal fade">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header bg-teal-700">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h5 class="modal-title"><i class="icon-price-tags2"></i> &nbsp;MODIFIER FACTURES LOCALES</h5>
								</div>

								<div class="modal-body">
                                    <form class="form-vertical">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Client Locale :</label>
                                                    <select class="form-control border-black border-lg text-black livesearch" id="m_agent_local" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>N° Facture :</label>
                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Date Facture :</label>
                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Hors Taxe [HT] :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="HT_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Timbre :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TIM_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>TVA :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TVA_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>TTC :</label>
                                                    <input type="number" class="form-control border-black border-lg text-black" id="TTC_MFLOC" placeholder="...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="MSURIST" style="display:none">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-switchery switchery-sm">
                                                        <label>
                                                            <input type="checkbox" class="switchery" id="IS_MSURIST">
                                                            Facture Suristarie
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>CONTENEUR : <span id="mcontsp"></span></label>
                                                    <select class="form-control border-black border-lg text-black" id="SUR_MCONTLIST" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                        </div>
                                        <legend class="text-semibold">Règlement de Facture</legend>
                                        <div class="tabbable">
                                            <ul class="nav nav-tabs nav-tabs-highlight2 nav-justified" id="NVMFLOC">
                                                <li class="active"><a href="#FLOC_MBC" data-toggle="tab">BC</a></li>
                                                <li><a href="#FLOC_MESP" data-toggle="tab">Espèce</a></li>
                                                <li><a href="#FLOC_MCHQ" data-toggle="tab">Chèque</a></li>
                                                <li><a href="#FLOC_MTRAIT" data-toggle="tab">Traite</a></li>
                                                <li><a href="#FLOC_MVIR" data-toggle="tab">Virement</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div class="tab-pane active" id="FLOC_MBC">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            Pas de règlement
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_MESP">
                                                    <div class="row">
                                                        <div class="col-md-12 text-center">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_MDESP" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_MCHQ">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Chèque :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_MNCHQ" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_MDCHQ" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_MTRAIT">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Traite :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_MNTRAIT" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_MDTRAIT" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="FLOC_MVIR">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>N° Virement :</label>
                                                                <input type="text" class="form-control border-black border-lg text-black" id="FLOC_MNVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Date Virement :</label>
                                                                <input type="date" class="form-control border-black border-lg text-black" id="FLOC_MDVIR" placeholder="...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
								</div>

								<div class="modal-footer text-center">
									<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
									<button class="btn bg-brown-700" id="mod_floc"><i class="icon-check"></i> Ajouter</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /modal_FLOC modal -->

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>