<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Marge Brute</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/mbrut.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>

    th.hide_me,
    td.hide_me {
        display: none;
    }

    .selected td {
        background-color: #545252 !important;
        color: #ff4242 !important;
        /* Add !important to make sure override datables base styles */
    }

    .background_blue {
        font-size: 13px;
        /*color: rgba(255, 255, 255, 1);
        background: #2196f3;*/
        color: rgb(0, 0, 0);
        background: #a9b8c3;
        font-weight: bold;
    }

    select.bootstrap-select1 {
        width: 100%;
        padding: 6px 11px;
        border: 1px solid #a9b8c3;
        height: 34px;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background: #a9b8c3;
    }
    /* CAUTION: IE hackery ahead */
    
    select::-ms-expand {
        display: none;
        /* remove default arrow on ie10 and ie11 */
    }
    /* target Internet Explorer 9 to undo the custom arrow */
    
    @media screen and (min-width: 0\0) {
        select {
            background: none \9;
            padding: 5px \9;
        }
    }

    .btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary {
        background-color: #6e7479 !important;
        border-color: #2196f3;
    }

    .nav-tabs.nav-justified.nav-tabs-highlight2>li>a, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:focus, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:hover {
        border-top-width: 2px;
    }

    .nav-tabs.nav-tabs-highlight2>li.active>a, .nav-tabs.nav-tabs-highlight2>li.active>a:focus, .nav-tabs.nav-tabs-highlight2>li.active>a:hover {
        border-top-color: #5d4037;
    }

    .dt-buttons>.btn {
        border-radius: 0;
        border-left-color: white;
    }
        
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Achat</span> - Marge Brute
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Marge Brute par Dossier</h6>
                                    <div class="heading-elements">
                                        <button type="button" id="MBLISTBTN" class="btn btn-info btn-labeled btn-xs heading-btn"><b><i class="icon-redo2"></i></b> Liste des Marges Brutes Par Plage de Date</button>
									</div>
                                </div>

                                <div class="panel-body">

                                    <div class="row"> 
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4 text-center">
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <button class="btn bg-primary-700" id="open_dos"><i class="icon-folder-search"></i></button>
												</span>
												<input type="text" class="form-control bg-primary text-center" id="DOS_NUM" placeholder="N°...">
                                                <span class="input-group-btn">
                                                    <button class="btn bg-primary-700" id="get_dos"><i class="icon-file-download2"></i></button>
												</span>
                                            </div>
                                            <br>
                                            <span class="label label-block" id="DOS_TYPE" style="background-color: #455a64; border-color: #455a64; color: #fff;">Entrez le numéro de dossier</span>
                                        </div>
                                        <div class="col-md-4"></div>
                                    </div>
                                    <br>
                                    <div class="row text-center"> 
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Navire :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="Navire" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>BL/LTA :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="BL_LTA" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>POD :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="POD" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>POL :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="POL" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Date Embarquement :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="DEMB" placeholder="..." disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Date Déchargement :</label>
                                                <input type="text" class="form-control text-center border-black border-lg text-black" id="DDECH" placeholder="..." disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <br><br><br>

                                    <!-- Detail -->
                                    <div class="panel panel-default border-grey">                              
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <legend class="text-semibold text-teal"><i class="icon-price-tags position-left"></i> Vente</legend>
                                                            <div class="panel panel-default border-teal">   
                                                                <div class="panel-heading">
                                                                    <h6 class="panel-title text-muted text-bold">Factures Locales</h6>
                                                                </div>                           
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Non.Tax</label>
                                                                                <input type="text" id="VFL_NTAX" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Tax</label>
                                                                                <input type="text" id="VFL_TAX" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>TVA</label>
                                                                                <input type="text" id="VFL_TVA" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Timbre</label>
                                                                                <input type="text" id="VFL_TIMBRE" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>TTC</label>
                                                                                <input type="text" id="VFL_TTC" class="form-control text-center border-danger border-lg text-danger" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="panel panel-default border-teal">   
                                                                <div class="panel-heading">
                                                                    <h6 class="panel-title text-muted text-bold">Factures Avoirs</h6>
                                                                </div>                           
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Non.Tax</label>
                                                                                <input type="text" id="VFA_NTAX" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Tax</label>
                                                                                <input type="text" id="VFA_TAX" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>TVA</label>
                                                                                <input type="text" id="VFA_TVA" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Timbre</label>
                                                                                <input type="text" id="VFA_TIMBRE" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>TTC</label>
                                                                                <input type="text" id="VFA_TTC" class="form-control text-center border-danger border-lg text-danger" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="panel panel-default border-teal">   
                                                                <div class="panel-heading">
                                                                    <h6 class="panel-title text-muted text-bold">Factures Etrangeres</h6>
                                                                </div>                           
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                    
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>Devise</label>
                                                                                <input type="text" id="VFE_DEV" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>Total Devise</label>
                                                                                <input type="text" id="VFE_TDEV" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>TTC</label>
                                                                                <input type="text" id="VFE_TTC" class="form-control text-center border-danger border-lg text-danger" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </fieldset>
                                                </div>

                                                <div class="col-md-6">
                                                    <fieldset>
                                                        <legend class="text-semibold text-blue"><i class="icon-cart-add position-left"></i> Achat</legend>

                                                            <div class="panel panel-default border-blue">   
                                                                <div class="panel-heading">
                                                                    <h6 class="panel-title text-muted text-bold">Factures Locales</h6>
                                                                </div>                           
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group">
                                                                                <label>HT</label>
                                                                                <input type="text" id="AFL_HT" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-3">
                                                                            <div class="form-group">
                                                                                <label>TVA</label>
                                                                                <input type="text" id="AFL_TVA" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <div class="form-group">
                                                                                <label>Timbre</label>
                                                                                <input type="text" id="AFL_TMBRE" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>TTC</label>
                                                                                <input type="text" id="AFL_TTC" class="form-control text-center border-danger border-lg text-danger" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="panel panel-default border-blue">   
                                                                <div class="panel-heading">
                                                                    <h6 class="panel-title text-muted text-bold">Factures Etrangeres</h6>
                                                                </div>                           
                                                                <div class="panel-body">
                                                                    <div class="row">
                                                                    
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>Devise</label>
                                                                                <input type="text" id="AFE_DEV" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>Total Devise</label>
                                                                                <input type="text" id="AFE_TDEV" class="form-control text-center border-black border-lg text-black" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <label>TTC</label>
                                                                                <input type="text" id="AFE_TTC" class="form-control text-center border-danger border-lg text-danger" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <legend class="text-semibold"><i class="icon-calculator3 position-left"></i> Résultat final</legend>
                                                            <div class="alert bg-success text-center" id="RESFNBRT">
                                                                <h6 class="text-semibold">[( <span id="SVLTTC" class="m-5">0</span> + <span id="SVETTC" class="m-5">0</span> ) - <span id="SVATTC" class="m-5">0</span>] - [ <span id="SALTTC" class="m-5">0</span> + <span id="SAETTC" class="m-5">0</span> ] = <span id="SMBRUR" class="m-5">0 DT</span></h6>
                                                            </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Detail -->
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>