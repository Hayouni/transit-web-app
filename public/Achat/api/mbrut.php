<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {

    case 'GET_V_FLOC':
        $db = new MySQL();
        $GET_V_FLOC = $db->get_results("SELECT SUM(AAM_TOT_NON_TAXABLE) AS NTAX, SUM(AAM_TO_TAXABLE) AS TAX, 
                        SUM(AAM_TVA_PORCENTAGE) AS TVA, SUM(AAM_TIMBRE) AS TIMBRE, SUM(AAM_TOTAL_TTC) AS TTC 
                    FROM trans.avis_arrive_mig WHERE AAM_TYPE_FACT <> 'V' AND AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_V_FLOC);
        break;
    case 'GET_V_FAVOIR':
        $db = new MySQL();
        $GET_V_FAVOIR = $db->get_results("SELECT SUM(AAM_TOT_NON_TAXABLE) AS NTAX, SUM(AAM_TO_TAXABLE) AS TAX, 
                        SUM(AAM_TVA_PORCENTAGE) AS TVA, SUM(AAM_TIMBRE) AS TIMBRE, SUM(AAM_TOTAL_TTC) AS TTC 
                    FROM trans.avis_arrive_mig WHERE AAM_TYPE_FACT = 'V' AND AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_V_FAVOIR);
        break;
    case 'GET_V_FETR':
        $db = new MySQL();
        $GET_V_FETR = $db->get_results("SELECT
                                            AAM_DEVISE AS DEV,
                                            SUM( AAM_TOTAL_TTC ) AS DTOT,
                                            IFNULL( SUM( AAM_TOTAL_TTC_DINARS ), SUM( AAM_TOTAL_TTC ) ) AS DTTC_old ,
	                                        TVD_VALEUR AS DTTC 
                                        FROM
                                            trans.invoice 
                                            LEFT JOIN trans.tab_valeur_dinar ON AAM_CODE = TVD_CLE_INVOICE
                                        WHERE
                                            AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_V_FETR);
        break;
    case 'GET_A_FLOC':
        $db = new MySQL();
        $GET_A_FLOC = $db->get_results("SELECT SUM(IE_HT) AS HT,SUM(IE_TVA) AS TVA,SUM(IE_TIMBRE) AS TIMBRE,
                                SUM(IE_TTC) AS TTC 
                                FROM trans.invoice_entre_local WHERE IE_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_A_FLOC);
        break;
    case 'GET_A_FETR':
        $db = new MySQL();
        $GET_A_FETR = $db->get_results("SELECT IE_DEVISE AS DEV,SUM(IE_MONT_TOT) AS DTOT, 
                                SUM(IE_MONT_DINAR) AS DTTC 
                                FROM trans.invoice_entre WHERE IE_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_A_FETR);
        break;


    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>