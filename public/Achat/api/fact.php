<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {

    case 'SEARCH_DOS':
        $db = new MySQL();
        $SEARCH_DOS = $db->get_results("SELECT DM_CODE_COMP_GROUP,DM_IMP_EXP FROM trans.dossier_maritime WHERE DM_NUM_DOSSIER LIKE '$response[1]'");
        echo json_encode($SEARCH_DOS);
        break;

    case 'GET_AER_INFO':
        $db = new MySQL();
        $GET_AER_INFO = $db->get_results("SELECT
                                            DM_CLE,
                                            DM_NUM_DOSSIER,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP,
                                            DM_LIB_COMP_GROUP,
                                            DM_NUM_LTA,
                                            p.AE_LIBELLE AS POD,
                                            o.AE_LIBELLE AS POL,
                                            DATE_FORMAT( DM_DATE_EMBARQ, '%d/%m/%Y' ) AS DM_DATE_EMBARQ,
	                                        DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                            DM_POIDS,
                                            DM_MARQUE,
                                            DM_MARCHANDISE,
                                            DM_NOMBRE,
                                            DM_ESCALE,
                                            DM_RUBRIQUE 
                                        FROM
                                            trans.dossier_maritime
                                            INNER JOIN aeroport p ON DM_POD = p.AE_CODE 
                                            INNER JOIN aeroport o ON DM_POL = o.AE_CODE 
                                        WHERE
                                            DM_NUM_DOSSIER LIKE '$response[1]'");
        echo json_encode($GET_AER_INFO);
        break;

    case 'GET_MAR_INFO':
        $db = new MySQL();
        $GET_MAR_INFO = $db->get_results("SELECT
                                            DM_CLE,
                                            DM_NUM_DOSSIER,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP,
                                            DM_LIB_COMP_GROUP,
                                            NA_LIBELLE,
                                            DM_NUM_BL,
                                            p.PO_LIBELLE AS POD,
                                            o.PO_LIBELLE AS POL,
                                            DATE_FORMAT( DM_DATE_EMBARQ, '%d/%m/%Y' ) AS DM_DATE_EMBARQ,
                                            DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                            DM_POIDS,
                                            DM_MARQUE,
                                            DM_MARCHANDISE,
                                            DM_NOMBRE,
                                            DM_ESCALE,
                                            DM_RUBRIQUE
                                        FROM
                                            trans.dossier_maritime
                                            INNER JOIN navire ON DM_NAVIRE = NA_CODE
                                            INNER JOIN PORT p ON DM_POD = p.PO_CODE 
                                            INNER JOIN PORT o ON DM_POL = o.PO_CODE 
                                        WHERE
                                            DM_NUM_DOSSIER LIKE '$response[1]'");
        echo json_encode($GET_MAR_INFO);
        break;

    case 'GET_FACT_ETR':
        $db = new MySQL();
        $GET_FACT_ETR["data"] = $db->get_results("SELECT
                                                        IE_CLE,
                                                        CLE_LIBELLE,
                                                        IE_FACT_NUM,
                                                        DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,
                                                        IE_DEVISE,
                                                        IE_MONT_TOT,
                                                        IE_MONT_DINAR,
                                                        IE_REGLEMENT,
                                                        IE_NUM_VIREMENT,
                                                        DATE_FORMAT( IE_DATE_VIREMENT, '%d/%m/%Y' ) AS IE_DATE_VIREMENT 
                                                    FROM
                                                        invoice_entre
                                                        INNER JOIN client_etranger ON IE_CODE_AGENT = CLE_CODE 
                                                    WHERE
                                                        IE_NUM_DOSSIER = {$response[1]} 
                                                    ORDER BY
                                                        IE_FACT_DATE DESC");
        echo json_encode($GET_FACT_ETR);
        break;

    case 'GET_FACT_LOC':
        $db = new MySQL();
        $GET_FACT_LOC["data"] = $db->get_results("SELECT
                                                    IE_CLE,
                                                    FR_LIBELLE,
                                                    IE_FACT_NUM,
                                                    DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,
                                                    IE_HT,
                                                    IE_TVA,
                                                    IE_TIMBRE,
                                                    IE_TTC,
                                                    IE_REGLEMENT,
                                                    IE_NUM_CHEQUE,
                                                    DATE_FORMAT( IE_DATE, '%d/%m/%Y' ) AS IE_DATE,
                                                    IE_TYPE
                                                FROM
                                                    invoice_entre_local
                                                    INNER JOIN fournisseur_local ON IE_CODE_AGENT = FR_CODE 
                                                WHERE
                                                    IE_NUM_DOSSIER = {$response[1]} 
                                                ORDER BY
                                                    IE_FACT_DATE DESC");
        echo json_encode($GET_FACT_LOC);
        break;

    case 'GET_ONE_FACT_ETR':
        $db = new MySQL();
        $GET_ONE_FACT_ETR = $db->get_results("SELECT IE_CLE,
                                                    IE_CODE_AGENT,
                                                    IE_FACT_NUM,
                                                    DATE_FORMAT( IE_FACT_DATE, '%Y-%m-%d' ) AS IE_FACT_DATE,
                                                    IE_DEVISE,
                                                    IE_MONT_TOT,
                                                    IE_MONT_DINAR,
                                                    IE_REGLEMENT,
                                                    IE_NUM_VIREMENT,
                                                    DATE_FORMAT( IE_DATE_VIREMENT, '%Y-%m-%d' ) AS IE_DATE_VIREMENT
                                                FROM trans.invoice_entre 
                                                WHERE IE_CLE = {$response[1]}");
        echo json_encode($GET_ONE_FACT_ETR);
        break;

    case 'GET_ONE_FACT_LOC':
        $db = new MySQL();
        $GET_ONE_FACT_LOC = $db->get_results("SELECT
                                                    IE_CLE,
                                                    IE_CODE_AGENT,
                                                    IE_FACT_NUM,
                                                    DATE_FORMAT( IE_FACT_DATE, '%Y-%m-%d' ) AS IE_FACT_DATE,
                                                    IE_HT,
                                                    IE_TVA,
                                                    IE_TIMBRE,
                                                    IE_TTC,
                                                    IE_REGLEMENT,
                                                    IE_NUM_CHEQUE,
                                                    DATE_FORMAT( IE_DATE, '%Y-%m-%d' ) AS IE_DATE,
                                                    IE_TYPE ,
                                                    DC_NUM_CONTAINER
                                                FROM
                                                    trans.invoice_entre_local
                                                    LEFT JOIN dossier_container ON IE_TYPE = DC_CODE 
                                                WHERE IE_CLE = {$response[1]}");
        echo json_encode($GET_ONE_FACT_LOC);
        break;

    case 'GET_AGT_ETR':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT CLE_CODE,CLE_LIBELLE FROM trans.client_etranger");
        echo json_encode($fournisseurs);
        break;

    case 'GET_AGT_LOC':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT FR_CODE,FR_LIBELLE FROM trans.fournisseur_local");
        echo json_encode($fournisseurs);
        break;

    case 'DEL_FACT_ETR':
        $db = new MySQL();
        $Del_where = array('IE_CLE' => $response[1]);
        echo $db->delete( 'invoice_entre', $Del_where, 1 );
        break;

    case 'DEL_FACT_LOC':
        $db = new MySQL();
        $Del_where = array('IE_CLE' => $response[1]);
        echo $db->delete( 'invoice_entre_local', $Del_where, 1 );
        break;

    case 'MOD_ETR':
        $db = new MySQL();
        $update_where= array( 'IE_CLE' => $response[1]);
        echo $db->update( 'invoice_entre', $response[2], $update_where, 1 );
        break;

    case 'MOD_LOC':
        $db = new MySQL();
        $update_where= array( 'IE_CLE' => $response[1]);
        echo $db->update( 'invoice_entre_local', $response[2], $update_where, 1 );
        break;

    case 'ADD_ETR':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(IE_CLE) + 1 as CPT FROM invoice_entre")[0]['CPT'];
        $response[1]['IE_CLE'] = $key;
        echo $db->insert( 'invoice_entre', $response[1]);
        break;

    case 'ADD_LOC':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(IE_CLE) + 1 as CPT FROM invoice_entre_local")[0]['CPT'];
        $response[1]['IE_CLE'] = $key;
        echo $db->insert( 'invoice_entre_local', $response[1]);
        break;

    case 'GET_COMPLET_INFO':
        $db = new MySQL();
        $GET_COMPLET_INFO = $db->get_results("SELECT
                                            DC_CODE,
                                            DM_MARCHANDISE,
                                            DC_NUM_CONTAINER,
                                            DC_CONTAINER,
                                            DC_POID,
                                            DC_RUBRIQUE
                                        FROM
                                            dossier_container
                                            INNER JOIN dossier_marchandise ON DC_CODE_MARCHANDISE = DM_CODE 
                                        WHERE
                                        DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[1]' )");
        echo json_encode($GET_COMPLET_INFO);
        break;

    case 'UPDATE_DATE_SUR':
        $db = new MySQL();
        echo $db->update( 'dossier_container', ['DC_ACH_DATE_R' => $response[2]], ['DC_CODE' => $response[1]] );
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>