<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results($response[1]);
        echo json_encode($GET_TAB_DATA);
        break;

    case 'GET_TAB_COLUMNS':
        $database = new MySQL();
        $fields = $database->list_fields($response[1]);
        $Filter_out = [];
        $Columns = [];
        foreach ($fields as $key => $value) {
            $rs2 = Dictio($value->name);
            if (!empty($rs2)) {
                array_push($Columns, array('data' => $value->name,'title' => $rs2 ));
            }
        }
        echo json_encode($Columns);
        break;

    case 'GET_AGT_ETR':
        $db = new MySQL();
        $GET_AGT_ETR = $db->get_results("SELECT CLE_CODE,CLE_LIBELLE FROM trans.client_etranger");
        echo json_encode($GET_AGT_ETR);
        break;

    case 'GET_AGT_LOC':
        $db = new MySQL();
        $GET_AGT_LOC = $db->get_results("SELECT FR_CODE,FR_LIBELLE FROM trans.fournisseur_local");
        echo json_encode($GET_AGT_LOC);
        break;

    case 'GET_FACTETR_DETAIL':
        $db = new MySQL();
        $GET_FACTETR_DETAIL = $db->get_results("SELECT
                                                    IE_CLE,
                                                    IE_NUM_DOSSIER,
                                                    IE_FACT_NUM,
                                                    DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,
                                                    IE_MONT_TOT,
                                                    IE_DEVISE,
                                                    IE_REGLEMENT,
                                                    IE_NUM_VIREMENT,
                                                    DATE_FORMAT( IE_DATE_VIREMENT, '%Y-%m-%d' ) AS IE_DATE_VIREMENT 
                                                FROM
                                                    invoice_entre
                                                WHERE
                                                    IE_CLE = {$response[1]}");
        echo json_encode($GET_FACTETR_DETAIL);
        break;

    case 'SET_ETR_REG':
        $db = new MySQL();
        $update_where_AA= array( 'IE_CLE' => $response[2]);
        echo $db->update( 'invoice_entre', $response[1], $update_where_AA );
        break;

    case 'GET_FACTLOC_DETAIL':
        $db = new MySQL();
        $GET_FACTLOC_DETAIL = $db->get_results("SELECT
                                                    IE_CLE,
                                                    IE_NUM_DOSSIER,
                                                    IE_FACT_NUM,
                                                    DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,
                                                    IE_TTC,
                                                    IE_REGLEMENT,
                                                    IE_NUM_CHEQUE,
                                                    DATE_FORMAT( IE_DATE, '%Y-%m-%d' ) AS IE_DATE 
                                                FROM
                                                    invoice_entre_local 
                                                WHERE
                                                    IE_CLE = {$response[1]}");
        echo json_encode($GET_FACTLOC_DETAIL);
        break;

    case 'SET_LOC_REG':
        $db = new MySQL();
        $update_where_AA= array( 'IE_CLE' => $response[2]);
        echo $db->update( 'invoice_entre_local', $response[1], $update_where_AA );
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}

function Dictio($var = null)
{
    switch ($var) {
        // Facture
        case 'IE_CODE_AGENT' : 
        case 'FR_LIBELLE':
        case 'CLE_LIBELLE':
            return 'Agent';
        case 'IE_FACT_NUM':
            return 'N° Facture';
        case 'IE_FACT_DATE':
            return 'Date Facture';
        case 'IE_DEVISE':
            return 'Devise';
        case 'IE_MONT_TOT':
            return 'Montant';
        case 'IE_MONT_DINAR':
            return 'Montant DT';
        case 'IE_NUM_VIREMENT':
            return 'N° Virement';
        case 'IE_DATE_VIREMENT':
            return 'Date Virement';
        case 'IE_REGLEMENT':
            return 'Règlement';
        case 'IE_NUM_DOSSIER':
            return 'N° Dossier';
        case 'IE_HT' :
            return 'H.T';
        case 'IE_TVA':
            return 'T.V.A';
        case 'IE_TIMBRE':
            return 'Timbre';
        case 'IE_TTC':
            return 'T.T.C';
        case 'IE_NUM_CHEQUE':
            return 'Numéro';
        case 'IE_DATE':
            return 'Date';
        default:
            return '-';
    }
}

function get_agent_etranger($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT CLE_CODE,CLE_LIBELLE FROM client_etranger");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['CLE_CODE']]=$value['CLE_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT CLE_LIBELLE FROM client_etranger WHERE CLE_CODE = {$var}");
    }
    
}

function get_agent_locale($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT FR_CODE,FR_LIBELLE FROM fournisseur_local");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['FR_CODE']]=$value['FR_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT FR_LIBELLE FROM fournisseur_local WHERE FR_CODE = {$var}");
    }
    
}

?>