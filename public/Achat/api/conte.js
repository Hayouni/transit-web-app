$(document).ready(function() {
    moment.locale('fr');
    var startDate = '';
    var endDate = '';

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        order: [],
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        bStateSave: true,
        bDestroy: true,
        destroy: true,
        paging: false
    });

    $('#get_tab_btn').on('click', function() {
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    title: 'title',
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({ 'text-align': 'center' });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                /*{
                    extend: 'excelHtml5',
                    className: 'btn bg-success btn-icon',
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: 'title2',
                    exportOptions: {
                        columns: ':visible'
                    }
                },*/
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    title: 'title3',
                    text: '<i class="icon-file-pdf position-left"></i>',
                    message: 'msg',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/conte.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_DATA",
                        "1": startDate,
                        "2": endDate
                    });
                },
            },
            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }, { "type": "date", "targets": 4 }],
            columns: [
                { "data": "DC_CODE", "width": "1%", },
                { "data": "IE_NUM_DOSSIER" },
                //{ "data": "DM_MARCHANDISE" },
                { "data": "DC_NUM_CONTAINER" },
                //{ "data": "CS_PLACE" },
                {
                    "data": "DC_DATE_A",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                {
                    "data": "DC_DATE_R",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                { "data": "IE_REGLEMENT" },
                { "data": "IE_NUM_CHEQUE" },
                {"data": "IE_DATE"},
                {
                    "data": "DC_VERIF",
                    "render": function(data, type, row) {
                        if (row.IE_REGLEMENT !== 'BC' && row.IE_REGLEMENT !== null ) {
                            return '<i class="icon-circles text-success"></i>';
                        } else {
                            return '<i class="icon-circles text-danger"></i>';
                        }
                    }
                },
                {
                    "data": null,
                    "width": "10%",
                    "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>'
                }
            ],
        });
    });

    $('#add_tab_btn').on('click', function() {
        $('#modal_add_cont').modal('show');
    });



    $('#get_march_btn').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['GET_MARCH', $('#NumDos').val()]))
            .done(function(data) {
                data.forEach(function(element) {
                    $('#DosMarch').append('<option value="' + element.DM_CODE + '">' + element.DM_MARCHANDISE + '</option>');
                }, this);
            }).always(function(data) {
                $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['GET_CONTE', data[0].DM_CODE]))
                    .done(function(data) {
                        data.forEach(function(element) {
                            $('#DosConte').append('<option value="' + element.DC_CODE + '">' + element.DC_NUM_CONTAINER + '</option>');
                        }, this);
                    }).fail(function(xhr, status, error) {
                        new PNotify({
                            title: 'une erreur est survenue, contactez l\'administrateur',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
            }).fail(function(xhr, status, error) {
                new PNotify({
                    title: 'une erreur est survenue, contactez l\'administrateur',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
    });

    $('#DosMarch').on('change', function() {
        $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['GET_CONTE', this.value]))
            .done(function(data) {
                data.forEach(function(element) {
                    $('#DosConte').append('<option value="' + element.DC_CODE + '">' + element.DC_NUM_CONTAINER + '</option>');
                }, this);
            }).fail(function(xhr, status, error) {
                new PNotify({
                    title: 'une erreur est survenue, contactez l\'administrateur',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
    });


    $('#add_ec').on('click', function() {
        if ($('#NumDos').val() && $('#DosMarch').val() && $('#DosConte').val()) {
            var sent = {
                'CS_DC_CODE': $('#DosConte').val(),
                'CS_MARCHANDISE': $('#DosMarch option:selected').text(),
                'CS_CONTAINER': $('#DosConte option:selected').text(),
                'CS_NUM_DOSSIER': $('#NumDos').val(),
                'CS_DATE': $('#ConteDate').val(),
                'CS_PLACE': $('#ContePlace').val(),
                'CS_DESC': $('#ConteDesc').val(),
            };
            $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['ADD_CONTE', sent]))
                .done(function(data) {
                    table.ajax.reload();
                }).fail(function(xhr, status, error) {
                    new PNotify({
                        title: 'une erreur est survenue, contactez l\'administrateur',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
        } else {
            new PNotify({
                title: 'validation!',
                text: 'il ya des champs require !! [' + '<span class="text-danger mr-5">*</span>' + ']',
                icon: 'icon-warning22'
            });
        }
    });

    $('#table tbody').on('click', 'a#remove', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['DEL_CONTE', data.CS_CODE])).done(function(data) {
            table.ajax.reload();
        }).fail(function(xhr, status, error) {
            table.DataTable().ajax.reload();
            new PNotify({
                title: 'une erreur est survenue, contactez l\'administrateur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table tbody').on('click', 'a#modif', function() {
        var data_tab = table.row($(this).parents('tr')).data();
        $('#MNumDos').val(data_tab.DM_NUM_DOSSIER),
            $('#MNumDos').attr('key-d', data_tab.DC_CODE),
            $('#MDosMarch').val(data_tab.DM_MARCHANDISE),
            $('#MDosConte').val(data_tab.DC_NUM_CONTAINER),
            $('#MConteDateR').val(data_tab.DC_DATE_R),
            $('#MConteDateT').val(data_tab.DC_DATE_T),
            $('#MConteDateA').val(data_tab.DC_DATE_A),
            $('#MConteDesc').val(data_tab.DC_DISCR),
            $('#modal_mod_cont').modal('show');
    });

    $('#AddRestDate').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#MNumDos').val()]))
            .done(function(data) {
                var dmcle = data[0].DM_CLE;
                var donum = $('#MNumDos').val();
                var dcnum = $('#MDosConte').val();
                var dmnum = $('#MDosMarch').val();
                var drest = $('#MConteDateR').val()
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_HAS_FACT_S', dmcle, 'S_CONT', dcnum, dmnum, donum, drest]))
                    .fail(function(error) {
                        console.log('fail', error);
                        PNotify_Alert('N° DOS : ' + $('#MNumDos').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                    })
                    .done(function(data) {
                        if (Object.keys(data).length) {
                            /*var win = window.open('/GTRANS/public/Dossier/fact/SU?DM_CLE=' + dmcle + '&CN=' + dcnum + '&CC=' + dcc, '_blank');
                            win.focus();*/
                            $('#modal_mod_cont').modal('hide');
                            table.ajax.reload();
                            PNotify_Alert('N° DOS : ' + $('#MNumDos').val(), 'la date de restitution pas encore ajouté avec succée', 'success');
                        } else {
                            PNotify_Alert('N° DOS : ' + $('#MNumDos').val(), 'la date de restitution pas encore ajouté, il faut l\'ajouter a partir [Conteneur Client]', 'warning');
                        }
                    });
            });
    });

    $('#mod_conte').on('click', function() {
        var key_r = $('#MNumDos').attr('key-d');
        if (key_r) {
            var sent = {
                'DC_DISCR': $('#MConteDesc').val()
            };
            $.post('/GTRANS/public/Dossier/api/conte.php', JSON.stringify(['MOD_CONTE', key_r, sent])).done(function(data) {
                table.ajax.reload();
                $('#modal_mod_cont').modal('hide');
            }).fail(function(xhr, status, error) {
                table.DataTable().ajax.reload();
                new PNotify({
                    title: 'une erreur est survenue, contactez l\'administrateur',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        }
    });



    $('#SRC_DOS_BTN').on('click', function() {
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    title: 'title',
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({ 'text-align': 'center' });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                /*{
                    extend: 'excelHtml5',
                    className: 'btn bg-success btn-icon',
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: 'title2',
                    exportOptions: {
                        columns: ':visible'
                    }
                },*/
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    title: 'title3',
                    text: '<i class="icon-file-pdf position-left"></i>',
                    message: 'msg',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/conte.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_BY_DOS",
                        "1": $('#SRC_DOS_INP').val(),
                        "2": $('#SRC_DOS_INP').val()
                    });
                },
            },
            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }, { "type": "date", "targets": 4 }],
            columns: [
                { "data": "DC_CODE", "width": "1%", },
                { "data": "IE_NUM_DOSSIER" },
                //{ "data": "DM_MARCHANDISE" },
                { "data": "DC_NUM_CONTAINER" },
                //{ "data": "CS_PLACE" },
                {
                    "data": "DC_DATE_A",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                {
                    "data": "DC_DATE_R",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                { "data": "IE_REGLEMENT" },
                { "data": "IE_NUM_CHEQUE" },
                {"data": "IE_DATE"},
                {
                    "data": "DC_VERIF",
                    "render": function(data, type, row) {
                        if (row.IE_REGLEMENT !== 'BC' && row.IE_REGLEMENT !== null ) {
                            return '<i class="icon-circles text-success"></i>';
                        } else {
                            return '<i class="icon-circles text-danger"></i>';
                        }
                    }
                },
                {
                    "data": null,
                    "width": "10%",
                    "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>'
                }
            ],
        });
    });

    $('#SRC_PDOS_BTN').on('click', function() {
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    title: 'title',
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({ 'text-align': 'center' });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                /*{
                    extend: 'excelHtml5',
                    className: 'btn bg-success btn-icon',
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: 'title2',
                    exportOptions: {
                        columns: ':visible'
                    }
                },*/
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    title: 'title3',
                    text: '<i class="icon-file-pdf position-left"></i>',
                    message: 'msg',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/conte.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_BY_DOS",
                        "1": $('#SRC_PDOS_INP1').val(),
                        "2": $('#SRC_PDOS_INP2').val()
                    });
                },
            },
            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }, { "type": "date", "targets": 4 }],
            columns: [
                { "data": "DC_CODE", "width": "1%", },
                { "data": "IE_NUM_DOSSIER" },
                //{ "data": "DM_MARCHANDISE" },
                { "data": "DC_NUM_CONTAINER" },
                //{ "data": "CS_PLACE" },
                {
                    "data": "DC_DATE_A",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                {
                    "data": "DC_DATE_R",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                { "data": "IE_REGLEMENT" },
                { "data": "IE_NUM_CHEQUE" },
                {"data": "IE_DATE"},
                {
                    "data": "DC_VERIF",
                    "render": function(data, type, row) {
                        if (row.IE_REGLEMENT !== 'BC' && row.IE_REGLEMENT !== null ) {
                            return '<i class="icon-circles text-success"></i>';
                        } else {
                            return '<i class="icon-circles text-danger"></i>';
                        }
                    }
                },
                {
                    "data": null,
                    "width": "10%",
                    "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>'
                }
            ],
        });
    });

});