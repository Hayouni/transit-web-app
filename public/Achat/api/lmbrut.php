<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json");
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false) {
    $jqxdata = explode('&', $str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=', $value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results("Call ListMB('$response[1]','$response[2]')");
        echo json_encode($GET_TAB_DATA);
        break;

    case 'GET_TAB_DATA_BY_DOS':
        $db = new MySQL();
        $GET_TAB_DATA_BY_DOS["data"] = $db->get_results("Call ListMBDOS('$response[1]','$response[2]')");
        echo json_encode($GET_TAB_DATA_BY_DOS);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>