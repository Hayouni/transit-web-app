$(document).ready(function() {
    moment.locale('fr');
    var startDate = ''
    var endDate = '';

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['GET_TYPE_DEP'])).fail(function(data) {
        console.log('fail', data);
    }).done(function(data) {
        data.forEach(function(element) {
            $('#type_depense').append('<option value="' + element.TD_CODE + '">' + element.TD_LIBELLE + '</option>');
            $('#type_depense_m').append('<option value="' + element.TD_CODE + '">' + element.TD_LIBELLE + '</option>');
        }, this);
    });

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    create_chart();

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        order: [],
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        bStateSave: true,
        bDestroy: true,
        destroy: true,
        paging: false
    });

    $('#get_tab_btn').on('click', function() {
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    title: 'title',
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({ 'text-align': 'center' });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn bg-success btn-icon',
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: 'title2',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    title: 'title3',
                    text: '<i class="icon-file-pdf position-left"></i>',
                    message: 'msg',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/dep.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_DATA",
                        "1": startDate,
                        "2": endDate
                    });
                },
            },
            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }, { "type": "date", "targets": 1 }],
            columns: [
                { "data": "AD_CLE", "width": "1%", },
                {
                    "data": "AD_DATE_OPER",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                { "data": "TD_LIBELLE" },
                { "data": "AD_NUM_FACT" },
                {
                    "data": "AD_DATE_FACT",
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy"
                },
                { "data": "AD_HT" },
                { "data": "AD_TVA" },
                { "data": "AD_TTC" },
                { "data": "AD_TYPE_REGLEMENT" },
                { "data": "AD_NUMERO_CHEQUE" },
                {
                    "data": null,
                    "width": "10%",
                    "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
                }
            ],
        });
    });

    $('#add_tab_btn').on('click', function() {
        $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['GET_DATE_TIME']))
            .done(function(data) {
                $('#Date_Fact').val(data.date);
                $('#Date_oper').val(data.date);
                $('#get_tab_btn').click();
            })
            .fail(function(data) {
                console.log(data);
            }).always(function() {
                $('#modal_AFact').modal('show');
            });
    });

    $('#add_fact').on('click', function() {
        var sel = $("#NVLOC.nav-tabs li.active a").attr('href');
        var Dat_vir, Num_vir, Type_vir = '';
        switch (sel) {
            case '#Fact_ESP':
                Type_vir = 'ESPECE';
                Num_vir = '';
                Dat_vir = $('#Fact_DESP').val();
                break;
            case '#Fact_CHQ':
                Type_vir = 'CHEQUE';
                Num_vir = $('#Fact_NCHQ').val();
                Dat_vir = $('#Fact_DCHQ').val();
                break;
            case '#Fact_VIR':
                Type_vir = 'VIREMENT';
                Num_vir = $('#Fact_NVIR').val();
                Dat_vir = $('#Fact_DVIR').val();
                break;

            default:
                Type_vir = 'BC';
                break;
        }
        var data_add = {
            'AD_DATE_OPER': $('#Date_oper').val(),
            'AD_CODE_TYPE': $('#type_depense').val(),
            'AD_NUM_FACT': $('#Num_Fact').val(),
            'AD_DATE_FACT': $('#Date_Fact').val(),
            'AD_HT': $('#HT_Fact').val(),
            'AD_TVA': $('#TVA_Fact').val(),
            'AD_TTC': $('#TTC_Fact').val(),
            'AD_TYPE_REGLEMENT': Type_vir,
            'AD_NUMERO_CHEQUE': Num_vir,
        }

        $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['ADD_FACT', data_add]))
            .done(function(data) {
                PNotify_Alert('Facture', 'Ajouter avec succès', 'success');
                table.ajax.reload();
                create_chart();
            });
    });

    $('#table tbody').on('click', 'a#remove', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['DEL_FACT', data.AD_CLE])).done(function(data) {
            table.ajax.reload();
            create_chart();
        }).fail(function(xhr, status, error) {
            table.DataTable().ajax.reload();
            new PNotify({
                title: 'une erreur est survenue, contactez l\'administrateur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table tbody').on('click', 'a#modif', function() {
        var data_tab = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['GET_ONE_FACT', data_tab.AD_CLE])).done(function(data) {
            $('#Date_Moper').val(data[0].AD_DATE_OPER),
                $('#Date_Moper').attr('key-t', data[0].AD_CLE),
                $('#type_depense_m').val(data[0].AD_CODE_TYPE),
                $('#Num_MFact').val(data[0].AD_NUM_FACT),
                $('#Date_MFact').val(data[0].AD_DATE_FACT),
                $('#HT_MFact').val(data[0].AD_HT),
                $('#TVA_MFact').val(data[0].AD_TVA),
                $('#TTC_MFact').val(data[0].AD_TTC);
            if (data[0].AD_TYPE_REGLEMENT == 'VIREMENT') {
                $('#NVMFLOC.nav-tabs a[href="#Fact_MVIR"]').tab('show');
                $('#Fact_MNVIR').val(data[0].AD_NUMERO_VIREMENT);
                //$('#FLOC_MDVIR').val(data[0].IE_DATE);
            } else if (data[0].AD_TYPE_REGLEMENT == 'CHEQUE') {
                $('#NVMFLOC.nav-tabs a[href="#Fact_MCHQ"]').tab('show');
                $('#Fact_MNCHQ').val(data[0].AD_NUMERO_CHEQUE);
                //$('#FLOC_MDCHQ').val(data[0].IE_DATE);
            } else if (data[0].AD_TYPE_REGLEMENT == 'ESPECE') {
                $('#NVMFLOC.nav-tabs a[href="#Fact_MESP"]').tab('show');
                //$('#FLOC_MDESP').val(data[0].IE_DATE);
            } else {
                $('#NVMFLOC.nav-tabs a[href="#Fact_MBC"]').tab('show');
            }
            $('#modal_MFact').modal('show');
        }).fail(function(xhr, status, error) {
            new PNotify({
                title: 'une erreur est survenue, contactez l\'administrateur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#mod_fact').on('click', function() {
        if (!isNaN($('#Date_Moper').attr('key-t'))) {
            var sel = $("#NVMFLOC.nav-tabs li.active a").attr('href');
            var Dat_vir, Num_vir, Type_vir = '';
            switch (sel) {
                case '#Fact_MESP':
                    Type_vir = 'ESPECE';
                    Num_vir = '';
                    Dat_vir = $('#Fact_MDESP').val();
                    break;
                case '#Fact_MCHQ':
                    Type_vir = 'CHEQUE';
                    Num_vir = $('#Fact_MNCHQ').val();
                    Dat_vir = $('#Fact_MDCHQ').val();
                    break;
                case '#Fact_MVIR':
                    Type_vir = 'VIREMENT';
                    Num_vir = $('#Fact_MNVIR').val();
                    Dat_vir = $('#Fact_MDVIR').val();
                    break;

                default:
                    Type_vir = 'BC';
                    break;
            }
            var data_mod = {
                'AD_DATE_OPER': $('#Date_Moper').val(),
                'AD_CODE_TYPE': $('#type_depense_m').val(),
                'AD_NUM_FACT': $('#Num_MFact').val(),
                'AD_DATE_FACT': $('#Date_MFact').val(),
                'AD_HT': $('#HT_MFact').val(),
                'AD_TVA': $('#TVA_MFact').val(),
                'AD_TTC': $('#TTC_MFact').val(),
                'AD_TYPE_REGLEMENT': Type_vir,
                'AD_NUMERO_CHEQUE': Num_vir,
            }
            $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['MOD_FACT', $('#Date_Moper').attr('key-t'), data_mod]))
                .done(function(data) {
                    PNotify_Alert('Facture', 'Ajouter avec succès', 'success');
                    table.ajax.reload();
                    create_chart();
                    $('#modal_MFact').modal('hide');
                });
        } else {
            new PNotify({
                title: 'une erreur est survenue, fermer et réessayer',
                text: error,
                icon: 'icon-warning22'
            });
        }
    });

    $('#TTC_Fact').on('click', function() {
        $(this).val(Number(Number($('#HT_Fact').val()) + Number($('#TVA_Fact').val())).toFixed(3));
    });

    $('#TTC_MFact').on('click', function() {
        $(this).val(Number(Number($('#HT_MFact').val()) + Number($('#TVA_MFact').val())).toFixed(3));
    });

    function create_chart() {
        var currentYear = (new Date).getFullYear();
        $.post('/GTRANS/public/Achat/api/dep.php', JSON.stringify(['CHART_DEP_DATA', currentYear]))
            .done(function(result) {
                var dp1 = [];
                for (var i = 0; i < result.length; i++) {
                    var data = result[i].x;
                    var xd = String(data.replace('-', ', '));

                    dp1.push({ x: new Date(xd), y: result[i].y })
                }
                var chart = new CanvasJS.Chart("chartContainer", {
                    animationEnabled: true,
                    title: {
                        text: "Dépense Graphe " + currentYear
                    },
                    axisX: {
                        valueFormatString: "MM-YYYY"
                    },
                    axisY: {
                        title: "Dépense",
                        includeZero: false,
                        /*scaleBreaks: {
                            autoCalculate: true
                        },*/
                        suffix: " DT"
                    },
                    data: [{
                        type: "line",
                        xValueFormatString: "MM-YYYY",
                        color: "#F08080",
                        dataPoints: dp1
                    }]
                });
                chart.render();
            });
    }
});