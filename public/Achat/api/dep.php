<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results("SELECT
                                                        AD_CLE,
                                                        DATE_FORMAT( AD_DATE_OPER, '%d/%m/%Y' ) AS AD_DATE_OPER,
                                                        TD_LIBELLE,
                                                        AD_NUM_FACT,
                                                        DATE_FORMAT( AD_DATE_FACT, '%d/%m/%Y' ) AS AD_DATE_FACT,
                                                        AD_HT,
                                                        AD_TVA,
                                                        AD_TTC,
                                                        AD_TYPE_REGLEMENT,
                                                        AD_NUMERO_CHEQUE,
                                                        AD_NUMERO_VIREMENT
                                                    FROM
                                                        trans.autre_depense
                                                        INNER JOIN trans.type_depense ON AD_CODE_TYPE = TD_CODE 
                                                    WHERE
                                                        (AD_DATE_OPER BETWEEN '$response[1]' AND '$response[2]')
                                                    ORDER BY AD_DATE_OPER ASC");
        echo json_encode($GET_TAB_DATA);
        break;
    
    case 'GET_ONE_FACT':
        $db = new MySQL();
        $GET_ONE_FACT = $db->get_results("SELECT
                                            AD_CLE,
                                            DATE_FORMAT( AD_DATE_OPER, '%Y-%m-%d' ) AS AD_DATE_OPER,
                                            AD_CODE_TYPE,
                                            AD_NUM_FACT,
                                            DATE_FORMAT( AD_DATE_FACT, '%Y-%m-%d' ) AS AD_DATE_FACT,
                                            AD_HT,
                                            AD_TVA,
                                            AD_TTC,
                                            AD_TYPE_REGLEMENT,
                                            AD_NUMERO_CHEQUE,
                                            AD_NUMERO_VIREMENT
                                        FROM
                                            trans.autre_depense
                                        WHERE
                                            AD_CLE = '$response[1]'");
        echo json_encode($GET_ONE_FACT);
        break;

    case 'GET_TYPE_DEP':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT TD_CODE,TD_LIBELLE FROM trans.type_depense");
        echo json_encode($fournisseurs);
        break;

    case 'ADD_FACT':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(AD_CLE) + 1 as CPT FROM trans.autre_depense")[0]['CPT'];
        $response[1]['AD_CLE'] = $key;
        echo $db->insert( 'autre_depense', $response[1]);
        break;

    case 'DEL_FACT':
        $db = new MySQL();
        $where= array('AD_CLE'=>$response[1]);
        echo $db->delete( 'autre_depense',$where );
        break;

    case 'MOD_FACT':
        $db = new MySQL();
        $update_where= array( 'AD_CLE' => $response[1]);
        echo $db->update( 'autre_depense', $response[2], $update_where, 1 );
        break;

    case 'CHART_DEP_DATA':
        $db = new MySQL();
        $CHART_DEP_DATA = $db->get_results("SELECT
                                                DATE_FORMAT( AD_DATE_OPER, '%Y-%m' ) AS x,
                                                SUM( AD_TTC ) AS y
                                            FROM
                                                trans.autre_depense 
                                            WHERE
                                                YEAR ( AD_DATE_OPER ) = '$response[1]' 
                                            GROUP BY
                                                DATE_FORMAT( AD_DATE_OPER, '%Y-%m' )");

        echo json_encode($CHART_DEP_DATA, JSON_NUMERIC_CHECK);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>