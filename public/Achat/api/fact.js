$(document).ready(function() {

    $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_AGT_ETR'])).fail(function(data) {
        console.log('fail', data);
    }).done(function(data) {
        $('#agent_etrange').empty().append('<option value=""></option>');
        $('#m_agent_etrange').empty().append('<option value=""></option>');
        data.forEach(function(element) {
            $('#agent_etrange').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
            $('#m_agent_etrange').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
        }, this);
    }).always(function() {
        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_AGT_LOC'])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            $('#agent_local').empty().append('<option value=""></option>');
            $('#m_agent_local').empty().append('<option value=""></option>');
            data.forEach(function(element) {
                $('#agent_local').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                $('#m_agent_local').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
            }, this);
        }).always(function() {
            $(".livesearch").chosen({
                width: '100%',
                enable_split_word_search: false
            });
            if (NumD) {
                $('#get_dos').click();
            }
        });
    });

    // Switchery
    // ------------------------------

    // Initialize multiple switches
    if (Array.prototype.forEach) {
        var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
        elems.forEach(function(html) {
            var switchery = new Switchery(html);
        });
    } else {
        var elems = document.querySelectorAll('.switchery');
        for (var i = 0; i < elems.length; i++) {
            var switchery = new Switchery(elems[i]);
        }
    }

    // Colored switches
    /* var primary = document.querySelector('.switchery-primary');
     var switchery = new Switchery(primary, { color: '#2196F3' });

     var danger = document.querySelector('.switchery-danger');
     var switchery = new Switchery(danger, { color: '#EF5350' });

     var warning = document.querySelector('.switchery-warning');
     var switchery = new Switchery(warning, { color: '#FF7043' });

     var info = document.querySelector('.switchery-info');
     var switchery = new Switchery(info, { color: '#00BCD4' });*/



    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    $('#open_dos').on('click', function() {
        //'/GTRANS/public/Dossier/?NumD='+$('#DOS_NUM').val()
        if ($('#DOS_NUM').val()) {
            var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('#DOS_NUM').val(), '_blank');
            win.focus();
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    function changeSwitcheryState(el, value) {
        console.log($(el).is(':checked'))
        if ($(el).is(':checked') != value) {
            $(el).trigger("click")
        }
    }

    function setSwitchery(switchElement, checkedBool) {
        if (checkedBool && !switchElement.checked) { // switch on if not on
            $(switchElement).trigger('click').attr("checked", "checked");
        } else if (!checkedBool && switchElement.checked) { // switch off if not off
            $(switchElement).trigger('click').removeAttr("checked");
        }
    }

    function setSelectByValue(eID, val) { //Loop through sequentially//
        var ele = document.getElementById(eID);
        console.log(ele)
        for (var ii = 0; ii < ele.length; ii++)
            if (ele.options[ii].value == val) { //Found!
                console.log('found')
                ele.options[ii].selected = true;
                return true;
            }
        return false;
    }


    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        buttons: [{
            text: '<i class="icon-plus-circle2"></i> Ajouter',
            className: 'btn bg-info-700 btn-xs',
            action: function(e, dt, node, config) {
                switch ($(dt).context[0].sTableId) {
                    case 'TAB_ETRANGE':
                        $('#modal_FETR').modal('show');
                        break;

                    default:
                        $('#modal_FLOC').modal('show');
                        break;
                }
                //console.log(config)
            }
        }, {
            text: '<i class="icon-pen-plus"></i> Modifier',
            className: 'btn bg-info-700 btn-xs',
            action: function(e, dt, node, config) {
                switch ($(dt).context[0].sTableId) {
                    case 'TAB_ETRANGE':
                        if (dt.rows('.selected').any()) {
                            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_ONE_FACT_ETR', dt.rows('.selected').data()[0].IE_CLE]))
                                .fail(function(error) {
                                    console.log('fail', error);
                                }).done(function(data) {
                                    $('#m_agent_etrange').val(data[0].IE_CODE_AGENT).trigger("chosen:updated");
                                    $('#Num_MFET').val(data[0].IE_FACT_NUM);
                                    $('#Date_MFET').val(data[0].IE_FACT_DATE);
                                    $('#TYPE_MDETR').val(data[0].IE_DEVISE);
                                    /*if (data[0].IE_DEVISE == 'USD') {
                                        $('#TYPE_DETR_EUR').parent('label').removeClass('active');
                                        $('#TYPE_MDETR_USD').parent('label').removeClass('active').addClass('active');
                                    }*/
                                    $('#Mont_MDEV').val(data[0].IE_MONT_TOT);
                                    $('#Mont_MDT').val(data[0].IE_MONT_DINAR);
                                    if (data[0].IE_REGLEMENT == 'VIREMENT') {
                                        $('.nav-tabs a[href="#FETC_MVIR"]').tab('show')
                                    }
                                    $('#FETC_MNVIR').val(data[0].IE_NUM_VIREMENT);
                                    $('#FETC_MDVIR').val(data[0].IE_DATE_VIREMENT);
                                    $('#modal_MFETR').modal('show');
                                });
                        } else {
                            PNotify_Alert('Avertissement', 'Aucune donnée sélectionnée dans le tableau', 'danger');
                        }
                        break;

                    default:
                        if (dt.rows('.selected').any()) {
                            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_ONE_FACT_LOC', dt.rows('.selected').data()[0].IE_CLE]))
                                .fail(function(error) {
                                    console.log('fail', error);
                                }).done(function(data) {
                                    console.log('mod_loc activ')
                                    $('#m_agent_local').val(data[0].IE_CODE_AGENT).trigger("chosen:updated");
                                    $('#Num_MFLOC').val(data[0].IE_FACT_NUM);
                                    $('#Date_MFLOC').val(data[0].IE_FACT_DATE);
                                    $('#TVA_MFLOC').val(data[0].IE_TVA);
                                    $('#TIM_MFLOC').val(data[0].IE_TIMBRE);
                                    $('#HT_MFLOC').val(data[0].IE_HT);
                                    $('#TTC_MFLOC').val(data[0].IE_TTC);
                                    if (data[0].IE_REGLEMENT == 'VIREMENT') {
                                        $('#NVMFLOC.nav-tabs a[href="#FLOC_MVIR"]').tab('show');
                                        $('#FLOC_MNVIR').val(data[0].IE_NUM_CHEQUE);
                                        $('#FLOC_MDVIR').val(data[0].IE_DATE);
                                    } else if (data[0].IE_REGLEMENT == 'CHEQUE') {
                                        $('#NVMFLOC.nav-tabs a[href="#FLOC_MCHQ"]').tab('show');
                                        $('#FLOC_MNCHQ').val(data[0].IE_NUM_CHEQUE);
                                        $('#FLOC_MDCHQ').val(data[0].IE_DATE);
                                    } else if (data[0].IE_REGLEMENT == 'TRAITE') {
                                        $('#NVMFLOC.nav-tabs a[href="#FLOC_MTRAIT"]').tab('show');
                                        $('#FLOC_MNTRAIT').val(data[0].IE_NUM_CHEQUE);
                                        $('#FLOC_MDTRAIT').val(data[0].IE_DATE);
                                    } else if (data[0].IE_REGLEMENT == 'ESPECE') {
                                        $('#NVMFLOC.nav-tabs a[href="#FLOC_MESP"]').tab('show');
                                        $('#FLOC_MDESP').val(data[0].IE_DATE);
                                    } else {
                                        $('#NVMFLOC.nav-tabs a[href="#FLOC_MBC"]').tab('show');
                                    }
                                    if (data[0].IE_TYPE !== '' && data[0].IE_TYPE !== null && data[0].IE_TYPE !== undefined) {
                                        //$('#IS_MSURIST').trigger('click');
                                        //var chs = $('#IS_MSURIST');
                                        //var chs = document.querySelector('#IS_MSURIST');
                                        //changeSwitcheryState(chs, true);
                                        //chs.trigger('click');
                                        $('#mcontsp').text(data[0].DC_NUM_CONTAINER);
                                        //$("#SUR_MCONTLIST option:text=" + data[0].DC_NUM_CONTAINER + "").prop("selected", "selected");
                                        //$('#SUR_MCONTLIST').val(data[0].IE_TYPE).change();
                                        $("#SUR_MCONTLIST option[value=" + data[0].IE_TYPE + "]").attr('selected', 'selected');
                                        //setSelectByValue('SUR_MCONTLIST', data[0].IE_TYPE);
                                    }
                                    $('#modal_MFLOC').modal('show');
                                });
                        } else {
                            PNotify_Alert('Avertissement', 'Aucune donnée sélectionnée dans le tableau', 'danger');
                        }
                        break;
                }
                //console.log(config)
            }
        }, {
            text: '<i class="icon-cancel-circle2"> Supprimer',
            className: 'btn bg-info-700 btn-xs',
            action: function(e, dt, node, config) {
                switch ($(dt).context[0].sTableId) {
                    case 'TAB_ETRANGE':
                        if (dt.rows('.selected').any()) {
                            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['DEL_FACT_ETR', dt.rows('.selected').data()[0].IE_CLE]))
                                .fail(function(error) {
                                    console.log('fail', error);
                                }).done(function(data) {
                                    PNotify_Alert('INFO', 'supprimée avec succès', 'info');
                                    dt.ajax.reload();
                                });
                        } else {
                            PNotify_Alert('Avertissement', 'Aucune donnée sélectionnée dans le tableau', 'danger');
                        }
                        break;

                    default:
                        if (dt.rows('.selected').any()) {
                            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['DEL_FACT_LOC', dt.rows('.selected').data()[0].IE_CLE]))
                                .fail(function(error) {
                                    console.log('fail', error);
                                }).done(function(data) {
                                    PNotify_Alert('INFO', 'supprimée avec succès', 'info');
                                    dt.ajax.reload();
                                });
                        } else {
                            PNotify_Alert('Avertissement', 'Aucune donnée sélectionnée dans le tableau', 'danger');
                        }
                        break;
                }

            }
        }, {
            text: '<i class="icon-printer"></i>',
            extend: 'print',
            footer: true,
            className: 'btn bg-info-700 btn-xs'
        }, {
            extend: 'collection',
            text: '<i class="icon-three-bars"></i>',
            buttons: [{
                extend: 'columnsToggle',
                columns: ':not([data-visible="false"])'
            }],
            className: 'btn bg-info-700 btn-xs'
        }],
        fade: true,
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var IS_SURISTARIE = false;
    var DC_CODE = '';


    var changeCheckbox = document.querySelector('#IS_SURIST');

    changeCheckbox.onchange = function() {
        if (changeCheckbox.checked) {
            IS_SURISTARIE = true;
        } else {
            IS_SURISTARIE = false;
            DC_CODE = '';
        }
    };

    $('#SUR_CONTLIST').on('change', function() {
        DC_CODE = $('#SUR_CONTLIST').val();
    });

    $('#SUR_MCONTLIST').on('change', function() {
        DC_CODE = $('#SUR_MCONTLIST').val();
    });

    var changeCheckbox = document.querySelector('#IS_MSURIST');

    changeCheckbox.onchange = function() {
        if (changeCheckbox.checked) {
            IS_SURISTARIE = true;
            DC_CODE = $('#SUR_MCONTLIST').val();
        } else {
            IS_SURISTARIE = false;
            DC_CODE = '';
        }
    };

    var TAB_ETRANGE = $('#TAB_ETRANGE').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        //paging: false,
        language: {
            sEmptyTable: '<i class="icon-file-empty"></i> Aucune donnée disponible',
        }
    });

    var TAB_LOCALE = $('#TAB_LOCALE').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        //paging: false,
        language: {
            sEmptyTable: '<i class="icon-file-empty"></i> Aucune donnée disponible',
        }
    });

    var DOSSIER_INFO = '';

    function get_all_fact_dos() {
        $('#complet_info').empty();
        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM').val()]), function(data) {
            if (Object.keys(data).length) {
                switch (data[0].DM_CODE_COMP_GROUP) {
                    case null: //Case Aerian
                    case '':
                        $('#DOS_TYPE').empty().text('Aèrien ' + data[0].DM_IMP_EXP);
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_AER_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val('');
                            $('#BL_LTA').val(data[0].DM_NUM_LTA);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                            $('#MARCH').val(data[0].DM_MARCHANDISE);
                            $('#NOMBRE').val(data[0].DM_NOMBRE);
                            $('#MARQUE').val(data[0].DM_MARQUE);
                            $('#POIDS').val(data[0].DM_POIDS);
                            $('#ESCALE').val(data[0].DM_ESCALE);
                            $('#RuBRIQUE').val(data[0].DM_RUBRIQUE);
                        });
                        break;

                    case 'G':
                        $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' GROUPAGE');
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_MAR_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val(data[0].NA_LIBELLE);
                            $('#BL_LTA').val(data[0].DM_NUM_BL);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                            $('#MARCH').val(data[0].DM_MARCHANDISE);
                            $('#NOMBRE').val(data[0].DM_NOMBRE);
                            $('#MARQUE').val(data[0].DM_MARQUE);
                            $('#POIDS').val(data[0].DM_POIDS);
                            $('#ESCALE').val(data[0].DM_ESCALE);
                            $('#RuBRIQUE').val(data[0].DM_RUBRIQUE);
                        });
                        break;

                    case 'C':
                        $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' COMPLET');
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_MAR_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val(data[0].NA_LIBELLE);
                            $('#BL_LTA').val(data[0].DM_NUM_BL);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                            $('#MARCH').val(data[0].DM_MARCHANDISE);
                            $('#NOMBRE').val(data[0].DM_NOMBRE);
                            $('#MARQUE').val(data[0].DM_MARQUE);
                            $('#POIDS').val(data[0].DM_POIDS);
                            $('#ESCALE').val(data[0].DM_ESCALE);
                            $('#RuBRIQUE').val(data[0].DM_RUBRIQUE);
                            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_COMPLET_INFO', $('#DOS_NUM').val()]))
                                .done(function(data) {
                                    $('#SURIST').css('display', 'block')
                                    $('#MSURIST').css('display', 'block')
                                    var tbhtm = '<div class="table-responsive">\
                                                    <table class="table table-xxs">\
                                                        <thead>\
                                                            <tr>\
                                                                <th>MARCHANDISE</th>\
                                                                <th>N° Conteneur</th>\
                                                                <th>Voume</th>\
                                                                <th>Poids</th>\
                                                                <th>Rubrique</th>\
                                                            </tr>\
                                                        </thead><tbody>'

                                    var tf = '</tbody>\
                                            </table>\
                                        </div>'

                                    var tb = ""
                                    $('#SUR_CONTLIST').empty()
                                    $('#SUR_CONTLIST').append('<option value="">-</option>');
                                    $('#SUR_MCONTLIST').empty()
                                    $('#SUR_MCONTLIST').append('<option value="">-</option>');
                                    data.forEach(element => {
                                        tb += '<tr>\
                                                    <td>' + element.DM_MARCHANDISE + '</td>\
                                                    <td>' + element.DC_NUM_CONTAINER + '</td>\
                                                    <td>' + element.DC_CONTAINER + '</td>\
                                                    <td>' + element.DC_POID + '</td>\
                                                    <td>' + element.DC_RUBRIQUE + '</td>\
                                                </tr>';
                                        $('#SUR_CONTLIST').append('<option value="' + element.DC_CODE + '">' + element.DC_NUM_CONTAINER + '</option>');
                                        $('#SUR_MCONTLIST').append('<option value="' + element.DC_CODE + '">' + element.DC_NUM_CONTAINER + '</option>');
                                    });
                                    $('#complet_info').html(tbhtm + tb + tf);
                                });
                        });
                        break;
                    default:
                        PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                        break;
                }
            } else {
                PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
            }
        }).always(function(data) {
            TAB_ETRANGE = $('#TAB_ETRANGE').DataTable({
                select: {
                    style: 'single'
                },
                destroy: true,
                scrollX: false,
                ajax: {
                    url: "/GTRANS/public/Achat/api/fact.php",
                    type: "POST",
                    data: function(d) {
                        return JSON.stringify({
                            "0": "GET_FACT_ETR",
                            "1": $('#DOS_NUM').val()
                        });
                    },
                },
                aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                columns: [
                    { "data": "IE_CLE" },
                    { "data": "CLE_LIBELLE" },
                    { "data": "IE_FACT_NUM" },
                    { "data": "IE_FACT_DATE" },
                    { "data": "IE_DEVISE" },
                    { "data": "IE_MONT_TOT" },
                    { "data": "IE_MONT_DINAR" },
                    { "data": "IE_REGLEMENT" },
                    { "data": "IE_NUM_VIREMENT" },
                    { "data": "IE_DATE_VIREMENT" }
                ],
                footerCallback: function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    // Total over all pages
                    total = api
                        .column(6)
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    pageTotal = api
                        .column(6, { page: 'current' })
                        .data()
                        .reduce(function(a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(6).footer()).html(
                        total + ' DT'
                    );
                },
                initComplete: function() {
                    TAB_LOCALE = $('#TAB_LOCALE').DataTable({
                        select: {
                            style: 'single'
                        },
                        //buttons: [],
                        destroy: true,
                        scrollX: false,
                        ajax: {
                            url: "/GTRANS/public/Achat/api/fact.php",
                            type: "POST",
                            data: function(d) {
                                return JSON.stringify({
                                    "0": "GET_FACT_LOC",
                                    "1": $('#DOS_NUM').val()
                                });
                            },
                        },
                        aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                        columns: [
                            { "data": "IE_CLE" },
                            { "data": "FR_LIBELLE" },
                            { "data": "IE_FACT_NUM" },
                            { "data": "IE_FACT_DATE" },
                            { "data": "IE_HT" },
                            { "data": "IE_TVA" },
                            { "data": "IE_TIMBRE" },
                            { "data": "IE_TTC" },
                            { "data": "IE_REGLEMENT" },
                            { "data": "IE_NUM_CHEQUE" },
                            { "data": "IE_DATE" }
                        ],
                        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                            if ((aData.IE_TYPE !== "") && (aData.IE_TYPE !== null)) {
                                $('td', nRow).css('background-color', '#f44336');
                            }
                        },
                        footerCallback: function(row, data, start, end, display) {
                            var api = this.api(),
                                data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function(i) {
                                return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                            };

                            // Total over all pages
                            total = api
                                .column(7)
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            // Total over this page
                            pageTotal = api
                                .column(7, { page: 'current' })
                                .data()
                                .reduce(function(a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                            // Update footer
                            $(api.column(7).footer()).html(
                                'T.T.C: ' + total + ' DT]'
                            );
                        },
                        initComplete: function() {
                            /*if (DOSSIER_INFO[0].DM_CODE_COMP_GROUP == 'C') {
                                TAB_LOCALE.button().add(0, {
                                    text: '<i class="icon-plus-circle2"></i> Suristarie',
                                    className: 'btn bg-pink-700 btn-xs',
                                    action: function(e, dt, node, config) {
                                        switch ($(dt).context[0].sTableId) {
                                            case 'TAB_ETRANGE':
                                                //$('#modal_FETR').modal('show');
                                                break;

                                            default:
                                                $('#modal_FSUR').modal('show');
                                                break;
                                        }
                                        //console.log(config)
                                    }
                                });
                                TAB_LOCALE.button().add(1, {
                                    text: '<i class="icon-pen-plus"></i> Modifier Suristarie',
                                    className: 'btn bg-pink-700 btn-xs',
                                    action: function(e, dt, node, config) {
                                        switch ($(dt).context[0].sTableId) {
                                            case 'TAB_ETRANGE':
                                                //$('#modal_FETR').modal('show');
                                                break;

                                            default:
                                                $('#modal_MFSUR').modal('show');
                                                break;
                                        }
                                        //console.log(config)
                                    }
                                });
                            }*/
                        }
                    }).columns.adjust().draw();
                }
            }).columns.adjust().draw();
        });
    }

    $('#get_dos').on('click', function() {
        if ($('#DOS_NUM').val()) {
            get_all_fact_dos($('#DOS_NUM').val());
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#DOS_NUM').on('keypress', function(e) {
        if (e.which === 13) {
            $('#get_dos').click();
        }
    });

    $('#add_fetc').on('click', function() {
        if (Object.keys(DOSSIER_INFO).length) {
            var sel = $("#NV_ADDFET.nav-tabs li.active a").attr('href');
            /*var type_devise = 'USD';
            if ($('#TYPE_DETR_EUR').parent('label').hasClass('active')) {
                type_devise = 'EUR';
            }*/
            var data_add = '';
            switch (sel) {
                case '#FETC_VIR':
                    data_add = {
                        'IE_CODE_AGENT': $('#agent_etrange').val(),
                        'IE_FACT_NUM': $('#Num_FET').val(),
                        'IE_FACT_DATE': $('#Date_FET').val(),
                        'IE_DEVISE': $('#TYPE_DETR').val(),
                        'IE_MONT_TOT': $('#Mont_DEV').val(),
                        'IE_MONT_DINAR': $('#Mont_DT').val(),
                        'IE_NUM_VIREMENT': $('#FETC_NVIR').val(),
                        'IE_DATE_VIREMENT': $('#FETC_DVIR').val(),
                        'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                        'IE_NAVIRE': $('#Navire').val(),
                        'IE_ESALE_LTA': $('#BL_LTA').val(),
                        'IE_POD': $('#POD').val(),
                        'IE_POL': $('#POL').val(),
                        'IE_DATE_EMBARQ': $('#DEMB').val(),
                        'IE_DATE_DECGARG': $('#DDECH').val(),
                        'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                        'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                        'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                        'IE_REGLEMENT': 'VIREMENT'
                    };
                    break;

                default:
                    data_add = {
                        'IE_CODE_AGENT': $('#agent_etrange').val(),
                        'IE_FACT_NUM': $('#Num_FET').val(),
                        'IE_FACT_DATE': $('#Date_FET').val(),
                        'IE_DEVISE': $('#TYPE_DETR').val(),
                        'IE_MONT_TOT': $('#Mont_DEV').val(),
                        'IE_MONT_DINAR': $('#Mont_DT').val(),
                        'IE_NUM_VIREMENT': $('#FETC_NVIR').val(),
                        'IE_DATE_VIREMENT': $('#FETC_DVIR').val(),
                        'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                        'IE_NAVIRE': $('#Navire').val(),
                        'IE_ESALE_LTA': $('#BL_LTA').val(),
                        'IE_POD': $('#POD').val(),
                        'IE_POL': $('#POL').val(),
                        'IE_DATE_EMBARQ': $('#DEMB').val(),
                        'IE_DATE_DECGARG': $('#DDECH').val(),
                        'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                        'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                        'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                        'IE_REGLEMENT': 'BC'
                    };
                    break;
            }
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['ADD_ETR', data_add])).done(function(data) {
                PNotify_Alert('Facture Etranger', 'Ajouter avec succès', 'success');
                TAB_ETRANGE.ajax.reload();
            });
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#mod_fetc').on('click', function() {
        if (Object.keys(DOSSIER_INFO).length && TAB_ETRANGE.rows('.selected').any()) {
            var sel = $("#NV_MODFET.nav-tabs li.active a").attr('href');
            /*var type_devise = 'USD';
            if ($('#TYPE_MDETR_EUR').parent('label').hasClass('active')) {
                type_devise = 'EUR';
            }*/
            var data_add = '';
            switch (sel) {
                case '#FETC_MVIR':
                    data_add = {
                        'IE_CODE_AGENT': $('#m_agent_etrange').val(),
                        'IE_FACT_NUM': $('#Num_MFET').val(),
                        'IE_FACT_DATE': $('#Date_MFET').val(),
                        'IE_DEVISE': $('#TYPE_MDETR').val(),
                        'IE_MONT_TOT': $('#Mont_MDEV').val(),
                        'IE_MONT_DINAR': $('#Mont_MDT').val(),
                        'IE_NUM_VIREMENT': $('#FETC_MNVIR').val(),
                        'IE_DATE_VIREMENT': $('#FETC_MDVIR').val(),
                        'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                        'IE_NAVIRE': $('#Navire').val(),
                        'IE_ESALE_LTA': $('#BL_LTA').val(),
                        'IE_POD': $('#POD').val(),
                        'IE_POL': $('#POL').val(),
                        'IE_DATE_EMBARQ': $('#DEMB').val(),
                        'IE_DATE_DECGARG': $('#DDECH').val(),
                        'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                        'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                        'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                        'IE_REGLEMENT': 'VIREMENT'
                    };
                    break;

                default:
                    data_add = {
                        'IE_CODE_AGENT': $('#m_agent_etrange').val(),
                        'IE_FACT_NUM': $('#Num_MFET').val(),
                        'IE_FACT_DATE': $('#Date_MFET').val(),
                        'IE_DEVISE': $('#TYPE_MDETR').val(),
                        'IE_MONT_TOT': $('#Mont_MDEV').val(),
                        'IE_MONT_DINAR': $('#Mont_MDT').val(),
                        'IE_NUM_VIREMENT': $('#FETC_MNVIR').val(),
                        'IE_DATE_VIREMENT': $('#FETC_MDVIR').val(),
                        'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                        'IE_NAVIRE': $('#Navire').val(),
                        'IE_ESALE_LTA': $('#BL_LTA').val(),
                        'IE_POD': $('#POD').val(),
                        'IE_POL': $('#POL').val(),
                        'IE_DATE_EMBARQ': $('#DEMB').val(),
                        'IE_DATE_DECGARG': $('#DDECH').val(),
                        'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                        'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                        'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                        'IE_REGLEMENT': 'BC'
                    };
                    break;
            }
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['MOD_ETR', TAB_ETRANGE.rows('.selected').data()[0].IE_CLE, data_add])).done(function(data) {
                PNotify_Alert('Facture Etranger', 'Modifier avec succès', 'success');
                $('#modal_MFETR').modal('hide');
                TAB_ETRANGE.ajax.reload();
            });

        } else {
            PNotify_Alert('Avertissement', 'Aucune ligne sélectionnée ou dossier non chargé', 'warning');
        }
    });

    $('#add_floc').on('click', function() {
        if (Object.keys(DOSSIER_INFO).length) {
            var sel = $("#NVLOC.nav-tabs li.active a").attr('href');
            var Dat_vir, Num_vir, Type_vir = '';
            switch (sel) {
                case '#FLOC_ESP':
                    Type_vir = 'ESPECE';
                    Num_vir = '';
                    Dat_vir = $('#FLOC_DESP').val();
                    break;
                case '#FLOC_CHQ':
                    Type_vir = 'CHEQUE';
                    Num_vir = $('#FLOC_NCHQ').val();
                    Dat_vir = $('#FLOC_DCHQ').val();
                    break;
                case '#FLOC_TRAIT':
                    Type_vir = 'TRAITE';
                    Num_vir = $('#FLOC_NTRAIT').val();
                    Dat_vir = $('#FLOC_DTRAIT').val();
                    break;
                case '#FLOC_VIR':
                    Type_vir = 'VIREMENT';
                    Num_vir = $('#FLOC_NVIR').val();
                    Dat_vir = $('#FLOC_DVIR').val();
                    break;

                default:
                    Type_vir = 'BC';
                    break;
            }
            var data_add = {
                'IE_CODE_AGENT': $('#agent_local').val(),
                'IE_FACT_NUM': $('#Num_FLOC').val(),
                'IE_FACT_DATE': $('#Date_FLOC').val(),
                'IE_HT': $('#HT_FLOC').val(),
                'IE_TVA': $('#TVA_FLOC').val(),
                'IE_TIMBRE': $('#TIM_FLOC').val(),
                'IE_TTC': $('#TTC_FLOC').val(),
                'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                'IE_NAVIRE': $('#Navire').val(),
                'IE_ESALE_LTA': $('#BL_LTA').val(),
                'IE_POD': $('#POD').val(),
                'IE_POL': $('#POL').val(),
                'IE_DATE_EMBARQ': $('#DEMB').val(),
                'IE_DATE_DECGARG': $('#DDECH').val(),
                'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                'IE_REGLEMENT': Type_vir,
                'IE_NUM_CHEQUE': Num_vir,
                'IE_DATE': Dat_vir,
                'IE_TYPE': DC_CODE
            };
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['ADD_LOC', data_add])).done(function(data) {
                PNotify_Alert('Facture Locale', 'Ajouter avec succès', 'success');
                TAB_LOCALE.ajax.reload();
            }).always(function() {
                console.log('IS_SURISTARIE', IS_SURISTARIE)
                if (IS_SURISTARIE) {
                    $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['UPDATE_DATE_SUR', DC_CODE, $('#Date_FLOC').val()]))
                        .fail(function(error) {
                            console.log('fail', error);
                        });
                }
            });
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#mod_floc').on('click', function() {
        if (Object.keys(DOSSIER_INFO).length && TAB_LOCALE.rows('.selected').any()) {
            var sel = $("#NVMFLOC.nav-tabs li.active a").attr('href');
            var Dat_vir, Num_vir, Type_vir = '';
            switch (sel) {
                case '#FLOC_MESP':
                    Type_vir = 'ESPECE';
                    Num_vir = '';
                    Dat_vir = $('#FLOC_MDESP').val();
                    break;
                case '#FLOC_MCHQ':
                    Type_vir = 'CHEQUE';
                    Num_vir = $('#FLOC_MNCHQ').val();
                    Dat_vir = $('#FLOC_MDCHQ').val();
                    break;
                case '#FLOC_MTRAIT':
                    Type_vir = 'TRAITE';
                    Num_vir = $('#FLOC_MNTRAIT').val();
                    Dat_vir = $('#FLOC_MDTRAIT').val();
                    break;
                case '#FLOC_MVIR':
                    Type_vir = 'VIREMENT';
                    Num_vir = $('#FLOC_MNVIR').val();
                    Dat_vir = $('#FLOC_MDVIR').val();
                    break;

                default:
                    Type_vir = 'BC';
                    break;
            }
            var data_add = {
                'IE_CODE_AGENT': $('#m_agent_local').val(),
                'IE_FACT_NUM': $('#Num_MFLOC').val(),
                'IE_FACT_DATE': $('#Date_MFLOC').val(),
                'IE_HT': $('#HT_MFLOC').val(),
                'IE_TVA': $('#TVA_MFLOC').val(),
                'IE_TIMBRE': $('#TIM_MFLOC').val(),
                'IE_TTC': $('#TTC_MFLOC').val(),
                'IE_NUM_DOSSIER': $('#DOS_NUM').val(),
                'IE_NAVIRE': $('#Navire').val(),
                'IE_ESALE_LTA': $('#BL_LTA').val(),
                'IE_POD': $('#POD').val(),
                'IE_POL': $('#POL').val(),
                'IE_DATE_EMBARQ': $('#DEMB').val(),
                'IE_DATE_DECGARG': $('#DDECH').val(),
                'IE_MAR_AER': ((DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== null) && DOSSIER_INFO[0].DM_CODE_COMP_GROUP !== '') ? 'MARITIME' : 'AERIEN',
                'IE_EXP_IMP': DOSSIER_INFO[0].DM_IMP_EXP,
                'IE_G_C': DOSSIER_INFO[0].DM_CODE_COMP_GROUP,
                'IE_REGLEMENT': Type_vir,
                'IE_NUM_CHEQUE': Num_vir,
                'IE_DATE': Dat_vir,
                'IE_TYPE': DC_CODE
            };
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['MOD_LOC', TAB_LOCALE.rows('.selected').data()[0].IE_CLE, data_add])).done(function(data) {
                PNotify_Alert('Facture Locale', 'Modifer avec succès', 'success');
                TAB_LOCALE.ajax.reload();
            }).always(function() {
                if (IS_SURISTARIE) {
                    $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['UPDATE_DATE_SUR', DC_CODE, $('#Date_MFLOC').val()]))
                        .fail(function(error) {
                            console.log('fail', error);
                        }).done(function() {
                            $('#modal_MFLOC').modal('hide');
                        });
                } else {
                    $('#modal_MFLOC').modal('hide');
                }
            });
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#TTC_FLOC').on('click', function() {
        $(this).val(Number(Number($('#HT_FLOC').val()) + Number($('#TVA_FLOC').val()) + Number($('#TIM_FLOC').val())).toFixed(3));
    });

    $('#TTC_MFLOC').on('click', function() {
        $(this).val(Number(Number($('#HT_MFLOC').val()) + Number($('#TVA_MFLOC').val()) + Number($('#TIM_MFLOC').val())).toFixed(3));
    });

    function Set_SW_true(params) {
        var special = document.querySelector(params);
        special.checked = true;
        if (typeof Event === 'function' || !document.fireEvent) {
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, true);
            special.dispatchEvent(event);
        } else {
            special.fireEvent('onchange');
        }
    }

    function Set_SW_false(params) {
        var special = document.querySelector(params);
        special.checked = false;
        if (typeof Event === 'function' || !document.fireEvent) {
            var event = document.createEvent('HTMLEvents');
            event.initEvent('change', true, true);
            special.dispatchEvent(event);
        } else {
            special.fireEvent('onchange');
        }
    }

    $('#modal_FLOC').on('show.bs.modal', function() {
        IS_SURISTARIE = false;
        DC_CODE = '';
        $('#SUR_CONTLIST').val("");
        Set_SW_false('#IS_SURIST');
    });

    $('#modal_FLOC').on('hide.bs.modal', function() {
        IS_SURISTARIE = false;
        DC_CODE = '';
        $('#SUR_CONTLIST').val("");
        Set_SW_false('#IS_SURIST');
    });

    $('#modal_MFLOC').on('show.bs.modal', function() {
        IS_SURISTARIE = false;
        DC_CODE = '';
        $('#SUR_MCONTLIST').val("");
        Set_SW_false('#IS_MSURIST');
    });

    $('#modal_MFLOC').on('hide.bs.modal', function() {
        IS_SURISTARIE = false;
        DC_CODE = '';
        $('#SUR_MCONTLIST').val("");
        Set_SW_false('#IS_MSURIST');
    });

});