<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA['data'] = $db->get_results("SELECT
                                                        IE_CLE,
                                                        DC_CODE,
                                                        IE_NUM_DOSSIER,
                                                        DC_NUM_CONTAINER,
                                                        DATE_FORMAT( DC_DATE_A, '%d/%m/%Y' ) AS DC_DATE_A,
                                                        DATE_FORMAT( DC_DATE_R, '%d/%m/%Y' ) AS DC_DATE_R,
                                                        IE_REGLEMENT,
                                                        IE_NUM_CHEQUE,
                                                        DATE_FORMAT( IE_DATE, '%d/%m/%Y' ) AS IE_DATE 
                                                    FROM
                                                        invoice_entre_local
                                                        INNER JOIN dossier_container ON IE_TYPE = DC_CODE 
                                                    WHERE
                                                        (DC_DATE_A BETWEEN '$response[1]' AND '$response[2]')");
        echo json_encode($GET_TAB_DATA);
        break;

    case 'GET_TAB_BY_DOS':
        $db = new MySQL();
        $GET_TAB_BY_DOS['data'] = $db->get_results("SELECT
                                                        IE_CLE,
                                                        DC_CODE,
                                                        IE_NUM_DOSSIER,
                                                        DC_NUM_CONTAINER,
                                                        DATE_FORMAT( DC_DATE_A, '%d/%m/%Y' ) AS DC_DATE_A,
                                                        DATE_FORMAT( DC_DATE_R, '%d/%m/%Y' ) AS DC_DATE_R,
                                                        IE_REGLEMENT,
                                                        IE_NUM_CHEQUE,
                                                        DATE_FORMAT( IE_DATE, '%d/%m/%Y' ) AS IE_DATE 
                                                    FROM
                                                        invoice_entre_local
                                                        INNER JOIN dossier_container ON IE_TYPE = DC_CODE 
                                                    WHERE
                                                        (IE_NUM_DOSSIER BETWEEN '$response[1]' AND '$response[2]' )");
        echo json_encode($GET_TAB_BY_DOS);
        break;

    case 'GET_MARCH':
        $db = new MySQL();
        $GET_MARCH = $db->get_results("SELECT DM_CODE,DM_MARCHANDISE FROM trans.dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_MARCH);
        break;

    case 'GET_CONTE':
        $db = new MySQL();
        $GET_MARCH = $db->get_results("SELECT DC_CODE,DC_NUM_CONTAINER FROM trans.dossier_container WHERE DC_CODE_MARCHANDISE = '$response[1]'");
        echo json_encode($GET_MARCH);
        break;

    case 'ADD_CONTE':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(CS_CODE) + 1 as CPT FROM trans.container_state")[0]['CPT'];
        $response[1]['CS_CODE'] = $key;
        echo $db->insert( 'container_state', $response[1]);
        break;

    case 'DEL_CONTE':
        $db = new MySQL();
        $where= array('CS_CODE'=>$response[1]);
        echo $db->delete( 'container_state',$where );
        break;

    case 'MOD_CONTE':
        $db = new MySQL();
        $update_where= array( 'DC_CODE' => $response[1]);
        echo $db->update( 'dossier_container', $response[2], $update_where, 1 );
        break;

    case 'GET_ONE_DATA':
        $db = new MySQL();
        $GET_ONE_DATA = $db->get_results("SELECT
                                                CS_CODE,
                                                CS_DC_CODE,
                                                CS_MARCHANDISE,
                                                CS_CONTAINER,
                                                CS_NUM_DOSSIER,
                                                DATE_FORMAT( CS_DATE, '%Y-%m-%d' ) AS CS_DATE,
                                                CS_PLACE,
                                                CS_DESC 
                                            FROM
                                                trans.container_state
                                            WHERE
                                                CS_CODE = '$response[1]'");
        echo json_encode($GET_ONE_DATA);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>