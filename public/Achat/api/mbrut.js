$(document).ready(function() {
    moment.locale('fr');

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    function get_MB_dos() {
        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM').val()]), function(data) {
            if (Object.keys(data).length) {
                switch (data[0].DM_CODE_COMP_GROUP) {
                    case null: //Case Aerian
                    case '':
                        $('#DOS_TYPE').empty().text('Aèrien ' + data[0].DM_IMP_EXP);
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_AER_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val('');
                            $('#BL_LTA').val(data[0].DM_NUM_LTA);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                        });
                        break;

                    case 'G':
                        $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' GROUPAGE');
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_MAR_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val(data[0].NA_LIBELLE);
                            $('#BL_LTA').val(data[0].DM_NUM_BL);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                        });
                        break;

                    case 'C':
                        $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' COMPLET');
                        $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['GET_MAR_INFO', $('#DOS_NUM').val()]), function(data) {
                            DOSSIER_INFO = data;
                            $('#Navire').val(data[0].NA_LIBELLE);
                            $('#BL_LTA').val(data[0].DM_NUM_BL);
                            $('#POD').val(data[0].POD);
                            $('#POL').val(data[0].POL);
                            $('#DEMB').val(data[0].DM_DATE_EMBARQ);
                            $('#DDECH').val(data[0].DM_DATE_DECHARG);
                        });
                        break;
                    default:
                        PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                        break;
                }
            } else {
                PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
            }
        }).always(function(data) {
            var SVLTTC, SVETTC, SVATTC, SALTTC, SAETTC, SMBRUR = 0;
            $.post('/GTRANS/public/Achat/api/mbrut.php', JSON.stringify(['GET_V_FLOC', $('#DOS_NUM').val()]))
                .done(function(data) {
                    $('#VFL_NTAX').val(data[0].NTAX),
                        $('#VFL_TAX').val(data[0].TAX),
                        $('#VFL_TVA').val(data[0].TAV),
                        $('#VFL_TIMBRE').val(data[0].TIMBRE),
                        $('#VFL_TTC').val(data[0].TTC);
                    SVLTTC = Number(data[0].TTC),
                        $('#SVLTTC').text(SVLTTC);

                })
                .always(function() {
                    $.post('/GTRANS/public/Achat/api/mbrut.php', JSON.stringify(['GET_V_FETR', $('#DOS_NUM').val()]))
                        .done(function(data) {
                            $('#VFE_DEV').val(data[0].DEV),
                                $('#VFE_TDEV').val(data[0].DTOT),
                                $('#VFE_TTC').val(data[0].DTTC);
                            SVETTC = Number(data[0].DTTC),
                                $('#SVETTC').text(SVETTC);
                        })
                        .always(function() {
                            $.post('/GTRANS/public/Achat/api/mbrut.php', JSON.stringify(['GET_V_FAVOIR', $('#DOS_NUM').val()]))
                                .done(function(data) {
                                    $('#VFA_NTAX').val(data[0].NTAX),
                                        $('#VFA_TAX').val(data[0].TAX),
                                        $('#VFA_TVA').val(data[0].TAV),
                                        $('#VFA_TIMBRE').val(data[0].TIMBRE),
                                        $('#VFA_TTC').val(data[0].TTC);
                                    SVATTC = Number(data[0].TTC),
                                        $('#SVATTC').text(SVATTC);

                                })
                                .always(function() {
                                    $.post('/GTRANS/public/Achat/api/mbrut.php', JSON.stringify(['GET_A_FLOC', $('#DOS_NUM').val()]))
                                        .done(function(data) {
                                            $('#AFL_HT').val(data[0].HT),
                                                $('#AFL_TVA').val(data[0].TAV),
                                                $('#AFL_TIMBRE').val(data[0].TIMBRE),
                                                $('#AFL_TTC').val(data[0].TTC);
                                            SALTTC = Number(data[0].TTC),
                                                $('#SALTTC').text(SALTTC);
                                        })
                                        .always(function() {
                                            $.post('/GTRANS/public/Achat/api/mbrut.php', JSON.stringify(['GET_A_FETR', $('#DOS_NUM').val()]))
                                                .done(function(data) {
                                                    $('#AFE_DEV').val(data[0].DEV),
                                                        $('#AFE_TDEV').val(data[0].DTOT),
                                                        $('#AFE_TTC').val(data[0].DTTC);
                                                    SAETTC = Number(data[0].DTTC),
                                                        $('#SAETTC').text(SAETTC);
                                                })
                                                .always(function() {
                                                    SMBRUR = ([(SVLTTC + SVETTC) - SVATTC] - [SALTTC + SAETTC]).toFixed(3),
                                                        $('#SMBRUR').text(SMBRUR + ' DT');
                                                    if (SMBRUR > 0) {
                                                        $('#RESFNBRT').removeClass('bg-success bg-danger').addClass('bg-success');
                                                    } else {
                                                        $('#RESFNBRT').removeClass('bg-success bg-danger').addClass('bg-danger');
                                                    }
                                                });
                                        });
                                });
                        });
                });
        });
    }

    $('#open_dos').on('click', function() {
        //'/GTRANS/public/Dossier/?NumD='+$('#DOS_NUM').val()
        if ($('#DOS_NUM').val()) {
            var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('#DOS_NUM').val(), '_blank');
            win.focus();
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#get_dos').on('click', function() {
        if ($('#DOS_NUM').val()) {
            get_MB_dos($('#DOS_NUM').val());
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#DOS_NUM').on('keypress', function(e) {
        if (e.which === 13) {
            $('#get_dos').click();
        }
    });

    $('#MBLISTBTN').on('click', function() {
        var win = window.open('/GTRANS/public/Achat/lmbrut', '_blank');
        win.focus();
    });


});