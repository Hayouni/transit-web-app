<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results("SELECT
        DC_CODE,
        DM_NUM_DOSSIER,
        DM_MARCHANDISE,
        DC_NUM_CONTAINER,
        DC_CONTAINER,
        DATE_FORMAT( DC_DATE_R, '%d/%m/%Y' ) AS DC_DATE_R,
        DATE_FORMAT( DC_DATE_T, '%d/%m/%Y' ) AS DC_DATE_T,
        DATE_FORMAT( DC_DATE_A, '%d/%m/%Y' ) AS DC_DATE_A,
        AAM_TOTAL_TTC,
        IE_TTC
    FROM
        dossier_container
        INNER JOIN dossier_marchandise ON DC_CODE_MARCHANDISE = DM_CODE
        INNER JOIN avis_arrive_mig ON ( DC_NUM_CONTAINER = AAM_NUM_CONTENEUR AND DC_CONTAINER = AAM_VOLUME_CONTENEUR AND DM_NUM_DOSSIER = AAM_NUM_DOSSIER )
        LEFT JOIN invoice_entre_local ON ( DC_CODE = IE_TYPE AND DM_NUM_DOSSIER = IE_NUM_DOSSIER ) 
    WHERE
        DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER BETWEEN '{$response[1]}' AND '{$response[2]}' )");
        echo json_encode($GET_TAB_DATA);
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>