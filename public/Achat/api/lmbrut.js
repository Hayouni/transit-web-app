$(document).ready(function() {
    moment.locale('fr');
    var startDate = ''
    var endDate = '';

    $('.dosinpfilter').formatter({
        pattern: '{{9999999999}}'
    });

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        order: [],
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        bStateSave: true,
        bDestroy: true,
        destroy: true,
        paging: false
    });

    $('#get_tab_btn').on('click', function() {
        let mbtitle = 'Marge Brute';
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    footer: true,
                    text: '<i class="icon-printer position-left"></i>',
                    title: mbtitle,
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({
                                'text-align': 'center'
                            });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                {
                    extend: 'excel',
                    className: 'btn bg-success btn-icon',
                    footer: true,
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: mbtitle,
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        $('row c[r^="C"]', sheet).attr('s', '2');
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    footer: true,
                    title: mbtitle,
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/lmbrut.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_DATA",
                        "1": startDate,
                        "2": endDate
                    });
                },
            },
            columns: [{
                    "data": "CGA",
                    "render": function(data, type, row) {
                        switch (row.CGA) {
                            case null: //Case Aerian
                            case '':
                                return 'AERIEN';

                            case 'G':
                                return 'MARITIME GROUPAGE';

                            case 'C':
                                return 'MARITIME COMPLET';
                            default:
                                return row.CGA;
                        }
                    }
                },
                {
                    "data": "NDOS",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.NDOS + '">' + row.NDOS + '</a>';
                    }
                },
                {
                    "data": "VTFL"
                },
                {
                    "data": "VTFE"
                },
                {
                    "data": "VTFA"
                },
                {
                    "data": "VTTC",
                    "render": function(data, type, row) {
                        return '<span class="text-black">' + row.VTTC + '</span>';
                    }
                },
                {
                    "data": "ATFL"
                },
                {
                    "data": "ATFE"
                },
                {
                    "data": "ATTC",
                    "render": function(data, type, row) {
                        return '<span class="text-black">' + row.ATTC + '</span>';
                    }
                },
                {
                    "data": "MGB",
                    "render": function(data, type, row) {
                        if (row.MGB >= 0) {
                            return '<span class="text-success-600"><i class="icon-stats-growth2 position-left"></i> ' + row.MGB + ' DT</span>';
                        } else {
                            return '<span class="text-danger"><i class="icon-stats-decline2 position-left"></i> ' + row.MGB + ' DT</span>';
                        }
                    }
                }
            ],
            initComplete: function() {
                this.api().columns().every(function() {
                    var column = this;
                    if (column[0] == 0) {
                        var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            switch (d) {
                                case null: //Case Aerian
                                case '':
                                    select.append('<option value="AERIEN">AERIEN</option>')
                                    break;

                                case 'G':
                                    select.append('<option value="MARITIME GROUPAGE">MARITIME GROUPAGE</option>')
                                    break;

                                case 'C':
                                    select.append('<option value="MARITIME COMPLET">MARITIME COMPLET</option>')
                                    break;
                                default:
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                    break;
                            }
                        });
                    }
                });
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                total_VTTC = api
                    .column(5)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                total_ATTC = api
                    .column(8)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over all pages
                total = api
                    .column(9)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(9, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(9).footer()).html(
                    '[ ' + Number(total).toFixed(3) + ' DT ]'
                );
                $(api.column(5).footer()).html(
                    '[ ' + Number(total_VTTC).toFixed(3) + ' DT ]'
                );
                $(api.column(8).footer()).html(
                    '[ ' + Number(total_ATTC).toFixed(3) + ' DT ]'
                );
            },
        });
    });

    $('#get_dostab_btn').on('click', function() {
        var notice = new PNotify({
            text: "S'il vous plaît, attendez",
            addclass: 'bg-primary',
            type: 'info',
            icon: 'icon-spinner4 spinner',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            },
            opacity: .9,
            width: "170px"
        });


        let mbtitle = 'Marge Brute';
        if (!$('#SRC_PDOS_INP1').val() || $('#SRC_PDOS_INP1').val().length !== 10 || !$('#SRC_PDOS_INP2').val() || $('#SRC_PDOS_INP2').val().length !== 10) {
            PNotify_Alert('errur', 'plage de dossier erroné', 'danger')
            return false;
        }
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    footer: true,
                    text: '<i class="icon-printer position-left"></i>',
                    title: mbtitle,
                    /*message: $('.daterange-ranges span').text(),*/
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({
                                'text-align': 'center'
                            });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                {
                    extend: 'excel',
                    className: 'btn bg-success btn-icon',
                    footer: true,
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: mbtitle,
                    /*message: $('.daterange-ranges span').text(),*/
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(xlsx) {
                        var sheet = xlsx.xl.worksheets['sheet1.xml'];

                        $('row c[r^="C"]', sheet).attr('s', '2');
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    footer: true,
                    title: mbtitle,
                    /*message: $('.daterange-ranges span').text(),*/
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            processing: true,
            deferRender: true,
            'language': {
                "loadingRecords": "&nbsp;",
                "processing": "Loading..."
            },
            ajax: {
                url: "/GTRANS/public/Achat/api/lmbrut.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_DATA_BY_DOS",
                        "1": $('#SRC_PDOS_INP1').val(),
                        "2": $('#SRC_PDOS_INP2').val()
                    });
                }
            },
            columns: [{
                    "data": "CGA",
                    "render": function(data, type, row) {
                        switch (row.CGA) {
                            case null: //Case Aerian
                            case '':
                                return 'AERIEN';

                            case 'G':
                                return 'MARITIME GROUPAGE';

                            case 'C':
                                return 'MARITIME COMPLET';
                            default:
                                return row.CGA;
                        }
                    }
                },
                {
                    "data": "NDOS",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.NDOS + '">' + row.NDOS + '</a>';
                    }
                },
                {
                    "data": "VTFL"
                },
                {
                    "data": "VTFE"
                },
                {
                    "data": "VTFA"
                },
                {
                    "data": "VTTC",
                    "render": function(data, type, row) {
                        return '<span class="text-black">' + row.VTTC + '</span>';
                    }
                },
                {
                    "data": "ATFL"
                },
                {
                    "data": "ATFE"
                },
                {
                    "data": "ATTC",
                    "render": function(data, type, row) {
                        return '<span class="text-black">' + row.ATTC + '</span>';
                    }
                },
                {
                    "data": "MGB",
                    "render": function(data, type, row) {
                        if (row.MGB >= 0) {
                            return '<span class="text-success-600"><i class="icon-stats-growth2 position-left"></i> ' + row.MGB + ' DT</span>';
                        } else {
                            return '<span class="text-danger"><i class="icon-stats-decline2 position-left"></i> ' + row.MGB + ' DT</span>';
                        }
                    }
                }
            ],
            initComplete: function() {
                PNotify.removeAll();
                this.api().columns().every(function() {
                    var column = this;
                    if (column[0] == 0) {
                        var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            switch (d) {
                                case null: //Case Aerian
                                case '':
                                    select.append('<option value="AERIEN">AERIEN</option>')
                                    break;

                                case 'G':
                                    select.append('<option value="MARITIME GROUPAGE">MARITIME GROUPAGE</option>')
                                    break;

                                case 'C':
                                    select.append('<option value="MARITIME COMPLET">MARITIME COMPLET</option>')
                                    break;
                                default:
                                    select.append('<option value="' + d + '">' + d + '</option>')
                                    break;
                            }
                        });
                    }
                });
            },
            footerCallback: function(row, data, start, end, display) {
                var api = this.api(),
                    data;

                // Remove the formatting to get integer data for summation
                var intVal = function(i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : 0;
                };

                total_VTTC = api
                    .column(5)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                total_ATTC = api
                    .column(8)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over all pages
                total = api
                    .column(9)
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Total over this page
                pageTotal = api
                    .column(9, {
                        page: 'current'
                    })
                    .data()
                    .reduce(function(a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(9).footer()).html(
                    '[ ' + Number(total).toFixed(3) + ' DT ]'
                );
                $(api.column(5).footer()).html(
                    '[ ' + Number(total_VTTC).toFixed(3) + ' DT ]'
                );
                $(api.column(8).footer()).html(
                    '[ ' + Number(total_ATTC).toFixed(3) + ' DT ]'
                );
            },
        });
    });

});