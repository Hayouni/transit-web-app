$(document).ready(function() {
    moment.locale('fr');
    var startDate, endDate = '';

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        //dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        dom: '<"toolbar">fBrtip',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    var sql_etr = "SELECT\
                        IE_CLE,\
                        IE_NUM_DOSSIER,\
                        CLE_LIBELLE,\
                        IE_FACT_NUM,\
                        DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,\
                        IE_DEVISE,\
                        IE_MONT_TOT,\
                        IE_MONT_DINAR,\
                        IE_REGLEMENT,\
                        IE_NUM_VIREMENT,\
                        DATE_FORMAT( IE_DATE_VIREMENT, '%d/%m/%Y' ) AS IE_DATE_VIREMENT \
                    FROM\
                        invoice_entre\
                        INNER JOIN client_etranger ON IE_CODE_AGENT = CLE_CODE \
                    WHERE\
                        (IE_FACT_DATE  BETWEEN '{{dd}}' AND '{{df}}') ";

    var sql_loc = "SELECT\
                        IE_CLE,\
                        IE_NUM_DOSSIER,\
                        FR_LIBELLE,\
                        IE_FACT_NUM,\
                        DATE_FORMAT( IE_FACT_DATE, '%d/%m/%Y' ) AS IE_FACT_DATE,\
                        IE_HT,\
                        IE_TVA,\
                        IE_TIMBRE,\
                        IE_TTC,\
                        IE_REGLEMENT,\
                        IE_NUM_CHEQUE,\
                        DATE_FORMAT( IE_DATE, '%d/%m/%Y' ) AS IE_DATE \
                    FROM\
                        invoice_entre_local\
                        INNER JOIN fournisseur_local ON IE_CODE_AGENT = FR_CODE \
                    WHERE\
                        (IE_FACT_DATE BETWEEN '{{dd}}' AND '{{df}}') ";

    var sql_end = "ORDER BY date(IE_FACT_DATE) ASC";


    $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_AGT_ETR'])).fail(function(data) {
        console.log('fail', data);
    }).done(function(data) {
        $('#Clients').empty().append('<option value="ALL">Client Etranger</option><option disabled>──────────</option>');
        data.forEach(function(element) {
            $('#Clients').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
        }, this);
    }).always(function() {
        $(".livesearch").chosen({
            width: '100%',
            enable_split_word_search: false
        });
    });

    $('#Type_Fact').on('change', function() {
        switch (this.value) {
            case '1':
                $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_AGT_ETR'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    $('#Clients').empty().append('<option value="ALL">Client Etranger</option><option disabled>──────────</option>');
                    data.forEach(function(element) {
                        $('#Clients').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
                    }, this);
                }).always(function() {
                    $(".livesearch").trigger("chosen:updated");
                });
                break;

            default:
                $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_AGT_LOC'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    $('#Clients').empty().append('<option value="ALL">Client Locale</option><option disabled>──────────</option>');
                    data.forEach(function(element) {
                        $('#Clients').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                    }, this);
                }).always(function() {
                    $(".livesearch").trigger("chosen:updated");
                });
                break;
        }
    });


    $('#get_tab_btn').on('click', function() {
        var sql = '';
        var lna = 'Liste des Factures ';
        switch ($('#Type_Fact').val()) {
            case '1':
                lna += 'Etranger';
                if ($('#Clients').val() == 'ALL') {
                    sql = sql_etr.replace('{{dd}}', startDate).replace('{{df}}', endDate) + sql_end;
                } else {
                    sql = sql_etr.replace('{{dd}}', startDate).replace('{{df}}', endDate) + " AND IE_CODE_AGENT = " + $('#Clients').val() + " " + sql_end;
                }
                break;
            default:
                lna += 'Locale';
                if ($('#Clients').val() == 'ALL') {
                    sql = sql_loc.replace('{{dd}}', startDate).replace('{{df}}', endDate) + sql_end;
                } else {
                    sql = sql_loc.replace('{{dd}}', startDate).replace('{{df}}', endDate) + " AND IE_CODE_AGENT = " + $('#Clients').val() + " " + sql_end;
                }
                break;
        }
        $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_TAB_COLUMNS', sql])).done((data) => {
            var tfffo = '';
            var cont = 0;
            data.forEach(element => {
                //' + element.title + '
                (cont == 0) ? tfffo += '<th class="hide_me"></th>': tfffo += '<th></th>';
            });
            $('#table').DataTable().clear().destroy();
            $('#table').empty();
            $("#table").append(
                $('<tfoot/>').append("<tr>" + tfffo + "</tr>")
                //$('<tfoot/>').append($("#table thead tr").clone())
            );
            // Setup - add a text input to each footer cell
            table = $('#table').DataTable({
                aaSorting: [],
                bSort: false,
                bPaginate: false,
                orderCellsTop: true,
                buttons: {
                    buttons: [{
                            extend: 'print',
                            footer: true,
                            className: 'btn bg-blue btn-icon',
                            text: '<i class="icon-printer position-left"></i>',
                            title: lna,
                            message: $('.daterange-ranges span').text(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(win) {
                                // Style the body..
                                $(win.document.body)
                                    .addClass('asset-print-body')
                                    .css({ 'text-align': 'center' });

                                /* Style for the table */
                                $(win.document.body)
                                    .find('table')
                                    .addClass('compact minimalistBlack')
                                    .css({
                                        margin: '5px 5px auto'
                                    });
                            },
                        },
                        {
                            extend: 'pdfHtml5',
                            className: 'btn bg-danger btn-icon',
                            title: 'title3',
                            text: '<i class="icon-file-pdf position-left"></i>',
                            message: 'msg',
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.styles.title = {
                                    fontSize: '20',
                                    alignment: 'center'
                                };
                                doc.styles.tableHeader = {
                                    bold: true,
                                    fontSize: 11,
                                    color: "white",
                                    fillColor: "#2d4154",
                                    border: "3px solid #000000",
                                    width: "100%",
                                    "text-align": "left",
                                    "border-collapse": "collapse",

                                };
                            }
                        },
                        {
                            extend: 'excel',
                            className: 'btn bg-success btn-icon',
                            footer: true,
                            text: '<i class="icon-file-excel position-left"></i>',
                            title: lna,
                            message: $('.daterange-ranges span').text(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];

                                $('row c[r^="C"]', sheet).attr('s', '2');
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                            className: 'btn bg-default btn-icon'
                        }
                    ],
                },
                bStateSave: true,
                bDestroy: true,
                select: false,
                destroy: true,
                scrollY: true,
                scrollX: true,
                ajax: {
                    url: "/GTRANS/public/Achat/api/ind.php",
                    type: "POST",
                    data: function(d) {
                        return JSON.stringify({
                            "0": "GET_TAB_DATA",
                            "1": sql
                        });
                    }
                },
                aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }, {
                    "type": "number",
                    "targets": 1,
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Achat/fact?Srch=' + row.IE_NUM_DOSSIER + '">' + row.IE_NUM_DOSSIER + '</a>';
                    }
                }, {
                    "targets": 3,
                    "render": function(data, type, row) {
                        return '<a id="SHOWFACT" class="btn btn-link"><i class="icon-credit-card position-left"></i> ' + row.IE_FACT_NUM + '</a>';
                    }
                }, {
                    "targets": 4,
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy",
                }],
                columns: data,
                footerCallback: function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    if ($('#Type_Fact').val() == 1) {
                        // Total over this page
                        pageTotal = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        var usdd = (api.column(5, { page: 'current' }).data()[0] !== undefined) ? api.column(5, { page: 'current' }).data()[0] : 'N';
                        // Update footer
                        $(api.column(4).footer()).html('Total');
                        $(api.column(5).footer()).html(usdd + ' =');
                        $(api.column(6).footer()).html(
                            Number(pageTotal).toFixed(3)
                        );
                        pageTotal_D = api
                            .column(7, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(7).footer()).html(
                            Number(pageTotal_D).toFixed(3) + ' DT'
                        );
                    } else {
                        // Total over this page
                        pageTotal_HT = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(5).footer()).html(
                            Number(pageTotal_HT).toFixed(3)
                        );
                        pageTotal_TVA = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(6).footer()).html(
                            Number(pageTotal_TVA).toFixed(3)
                        );
                        pageTotal_TIM = api
                            .column(7, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(7).footer()).html(
                            Number(pageTotal_TIM).toFixed(3)
                        );
                        pageTotal = api
                            .column(8, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(8).footer()).html(
                            Number(pageTotal).toFixed(3) + ' DT'
                        );
                    }
                },
                initComplete: function() {
                    $("div.toolbar").html('<div class="row mb-20"><div class="col-sm-6"><div class="form-group"><label class="text-black">Filtrer Client dans le tableau</label><div id="flspinpcl"></div></div></div><div class="col-sm-6"><div class="form-group"><label class="text-black">Filtrer Type de Règlement dans le tableau</label><div id="flspinpreg"></div></div></div>');
                    if ($('#Type_Fact').val() == 1) {
                        this.api().columns().every(function() {
                            var column = this;
                            if (column[0] == 2) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpcl").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            } else if (column[0] == 8) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpreg").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            }
                        });
                    } else {
                        this.api().columns().every(function() {
                            var column = this;
                            if (column[0] == 2) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpcl").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            } else if (column[0] == 9) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpreg").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            }
                        });
                    }
                }

            });
        }).fail((errors) => {
            console.log('error : ', errors);
        }).always((data) => {
            $('#table tbody').on('click', 'a#SHOWFACT', function() {
                data_table = table.row($(this).parents('tr')).data();
                //console.log(data_table)
                if ($('#Type_Fact').val() == 1) {
                    $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_FACTETR_DETAIL', data_table.IE_CLE]))
                        .done(function(data) {
                            $('#ndosetr').text(data[0].IE_NUM_DOSSIER);
                            $('#nfactetr').text(data[0].IE_FACT_NUM);
                            $('#dateetr').text(data[0].IE_FACT_DATE);
                            $('#totetr').text(data[0].IE_MONT_TOT + ' ' + data[0].IE_DEVISE);
                            $('#INP_FETR_VIREMENT_Num').val(data[0].IE_NUM_VIREMENT);
                            $('#INP_FETR_VIREMENT_DATE').val(data[0].IE_DATE_VIREMENT);
                        }).fail(function(data) {
                            new PNotify({
                                title: 'Rechercher une facture',
                                text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                            });
                        }).always(function(data) {
                            $('#modal_Reglement_etr').modal('show');
                        });
                } else {
                    $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_FACTLOC_DETAIL', data_table.IE_CLE]))
                        .done(function(data) {
                            $('#ndosloc').text(data[0].IE_NUM_DOSSIER);
                            $('#nfactloc').text(data[0].IE_FACT_NUM);
                            $('#dateloc').text(data[0].IE_FACT_DATE);
                            $('#totloc').text(data[0].IE_TTC);
                            if (data[0].IE_REGLEMENT == 'VIREMENT') {
                                $('#NVMFLOC.nav-tabs a[href="#VIREMENT"]').tab('show');
                                //$('#INP_F_VIREMENT_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_VIREMENT_Num').val(data[0].IE_NUM_CHEQUE);
                                $('#INP_F_VIREMENT_DATE').val(data[0].IE_DATE);
                            } else if (data[0].IE_REGLEMENT == 'CHEQUE') {
                                $('#NVMFLOC.nav-tabs a[href="#CHEQUE"]').tab('show');
                                //$('#INP_F_CHEQUE_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_CHEQUE_Num').val(data[0].IE_NUM_CHEQUE);
                                $('#INP_F_CHEQUE_DATE').val(data[0].IE_DATE);
                            } else if (data[0].IE_REGLEMENT == 'TRAITE') {
                                $('#NVMFLOC.nav-tabs a[href="#TRAITE"]').tab('show');
                                $('#INP_F_TRAITE_Num').val(data[0].IE_NUM_CHEQUE);
                                //$('#INP_F_TRAITE_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_TRAITE_DATE').val(data[0].IE_DATE);
                            } else if (data[0].IE_REGLEMENT == 'ESPECE') {
                                $('#NVMFLOC.nav-tabs a[href="#ESPECE"]').tab('show');
                                $('#INP_F_ESPECE_DATE').val(data[0].IE_DATE);
                            } else {
                                $('#NVMFLOC.nav-tabs a[href="#BC"]').tab('show');
                            }
                        }).fail(function(data) {
                            new PNotify({
                                title: 'Rechercher une facture',
                                text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                            });
                        }).always(function(data) {
                            $('#modal_Reglement').modal('show');

                        });
                }
            });
        });

        $('#REG_ETR_BTN').on('click', function() {
            var ud = {
                'IE_NUM_VIREMENT': $('#INP_FETR_VIREMENT_Num').val(),
                'IE_DATE_VIREMENT': $('#INP_FETR_VIREMENT_DATE').val(),
                'IE_REGLEMENT': ($('#INP_FETR_VIREMENT_Num').val()) ? 'VIREMENT' : 'BC'
            }
            $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['SET_ETR_REG', ud, data_table.IE_CLE]))
                .done(function(data) {
                    table.ajax.reload();
                    $('#modal_Reglement_etr').modal('hide');
                }).fail(function(error) {
                    console.log('fail : ', error);
                    new PNotify({
                        title: 'Règlement Facture etranger',
                        text: "Problème inattendue, contactez le développeur",
                        addclass: 'stack-bottom-right bg-warning-700',
                        icon: 'icon-cancel-circle2',
                        delay: 4000,
                        stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                    });
                })
        });

        $('#REG_BTN').on('click', function() {
            var ud = {};
            var sel = $("ul.nav-tabs li.active a").attr('href');
            switch (sel) {
                case '#BC':
                    ud = {
                        'IE_REGLEMENT': 'BC',
                        'IE_NUM_CHEQUE': '',
                        //'AAM_BANQUE': '',
                        'IE_DATE': '',
                        //'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;

                case '#ESPECE':
                    ud = {
                        'IE_REGLEMENT': 'ESPECE',
                        'IE_DATE': $('#INP_F_ESPECE_DATE').val(),
                        'IE_NUM_CHEQUE': '',
                        //'AAM_BANQUE': '',
                        //'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;

                case '#CHEQUE':
                    ud = {
                        'IE_REGLEMENT': 'CHEQUE',
                        'IE_NUM_CHEQUE': $('#INP_F_CHEQUE_Num').val(),
                        //'AAM_BANQUE': $('#INP_F_CHEQUE_Banq').val(),
                        'IE_DATE': $('#INP_F_CHEQUE_DATE').val(),
                        //'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_CHEQUE_Num').val() + 'Banque : ' + $('#INP_F_CHEQUE_Banq').val()
                    }
                    break;

                case '#TRAITE':
                    ud = {
                        'IE_REGLEMENT': 'TRAITE',
                        'IE_NUM_CHEQUE': $('#INP_F_TRAITE_Num').val(),
                        //'AAM_BANQUE': $('#INP_F_TRAITE_Banq').val(),
                        'IE_DATE': $('#INP_F_TRAITE_DATE').val(),
                        //'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_TRAITE_Num').val() + 'Banque : ' + $('#INP_F_TRAITE_Banq').val()
                    }
                    break;

                case '#VIREMENT':
                    ud = {
                        'IE_REGLEMENT': 'VIREMENT',
                        'IE_NUM_CHEQUE': $('#INP_F_VIREMENT_Num').val(),
                        //'AAM_BANQUE': $('#INP_F_VIREMENT_Banq').val(),
                        'IE_DATE': $('#INP_F_VIREMENT_DATE').val(),
                        //'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_VIREMENT_Num').val() + 'Banque : ' + $('#INP_F_VIREMENT_Banq').val()
                    }
                    break;
                default:
                    ud = {
                        'IE_REGLEMENT': 'BC',
                        'IE_NUM_CHEQUE': '',
                        //'AAM_BANQUE': '',
                        'IE_DATE': '',
                        //'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;
            }
            $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['SET_LOC_REG', ud, data_table.IE_CLE]))
                .done(function(data) {
                    table.ajax.reload();
                    $('#modal_Reglement').modal('hide');
                }).fail(function(error) {
                    console.log('fail : ', error);
                    new PNotify({
                        title: 'Règlement Facture etranger',
                        text: "Problème inattendue, contactez le développeur",
                        addclass: 'stack-bottom-right bg-warning-700',
                        icon: 'icon-cancel-circle2',
                        delay: 4000,
                        stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                    });
                })
        });
    });

});