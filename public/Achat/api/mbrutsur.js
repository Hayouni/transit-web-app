$(document).ready(function() {
    moment.locale('fr');
    var startDate = ''
    var endDate = '';

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        order: [],
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        bStateSave: true,
        bDestroy: true,
        destroy: true,
        paging: false
    });

    $('#SRC_DOS_BTN').on('click', function() {
        table = $('#table').DataTable({
            order: [],
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            aaSorting: [],
            aoColumnDefs: [{
                bSortable: false,
                aTargets: ["sorting_disabled"]
            }],
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    title: 'title',
                    message: $('.daterange-ranges span').text(),
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(win) {
                        // Style the body..
                        $(win.document.body)
                            .addClass('asset-print-body')
                            .css({ 'text-align': 'center' });

                        /* Style for the table */
                        $(win.document.body)
                            .find('table')
                            .addClass('compact minimalistBlack')
                            .css({
                                margin: '5px 5px auto'
                            });
                    },
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn bg-success btn-icon',
                    text: '<i class="icon-file-excel position-left"></i>',
                    title: 'title2',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn bg-danger btn-icon',
                    title: 'title3',
                    text: '<i class="icon-file-pdf position-left"></i>',
                    message: 'msg',
                    exportOptions: {
                        columns: ':visible'
                    },
                    customize: function(doc) {
                        doc.styles.title = {
                            fontSize: '20',
                            alignment: 'center'
                        };
                        doc.styles.tableHeader = {
                            bold: true,
                            fontSize: 11,
                            color: "white",
                            fillColor: "#2d4154",
                            border: "3px solid #000000",
                            width: "100%",
                            "text-align": "left",
                            "border-collapse": "collapse",

                        };
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                    className: 'btn bg-default btn-icon'
                }
            ],
            bStateSave: true,
            bDestroy: true,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Achat/api/mbrutsur.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_DATA",
                        "1": $('#SRC_DOS_INP').val(),
                        "2": $('#SRC_DOS_INP').val()
                    });
                },
            },
            columns: [
                {
                    "data": "DM_NUM_DOSSIER",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                    }
                },
                { "data": "DC_NUM_CONTAINER" },
                { "data": "AAM_TOTAL_TTC" },
                { "data": "IE_TTC" },
                { 
                    "data": null,
                    "render": function(data, type, row) {
                        let ssum = (Number(row.AAM_TOTAL_TTC) - Number(row.IE_TTC)).toFixed(3)
                        if (ssum > 0) {
                            return '<span class="text-success-600"><i class="icon-stats-growth2 position-left"></i> ' + ssum + ' DT</span>';
                        } else if(ssum == 0) {
                            return '<span class="text-muted">' + ssum + ' DT</span>';
                        } else {
                            return '<span class="text-danger"><i class="icon-stats-decline2 position-left"></i> ' + ssum + ' DT</span>';
                        }
                    } 
                }
            ]
        });
    });
});