<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Dépense</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/visualization/canvasjs/canvasjs.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/dep.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }

        .btn-primary.active, .btn-primary:active, .open>.dropdown-toggle.btn-primary {
        background-color: #6e7479 !important;
        border-color: #2196f3;
        }

        .nav-tabs.nav-justified.nav-tabs-highlight2>li>a, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:focus, .nav-tabs.nav-justified.nav-tabs-highlight2>li>a:hover {
            border-top-width: 2px;
        }

        .nav-tabs.nav-tabs-highlight2>li.active>a, .nav-tabs.nav-tabs-highlight2>li.active>a:focus, .nav-tabs.nav-tabs-highlight2>li.active>a:hover {
            border-top-color: #5d4037;
        }

        .dt-buttons>.btn {
            border-radius: 0;
            border-left-color: white;
        }
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Achat</span> - Dépense </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Recherche</h6>
                                </div>

                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-3">
                                            
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Ajouter </label>
                                            <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="add_tab_btn"><i class="icon-plus22"></i></button>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="display-block">Sélectionnez la plage de dates </label>
                                                <button type="button" class="btn bg-teal-400 daterange-ranges" id="daterange">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="get_tab_btn"><i class="icon-search4"></i></button>
                                        </div>
                                        <div class="col-lg-3">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table display datatable-button-print-rows table-striped table-bordered" id="table">
                                                <thead class="bg-teal-400">
                                                    <tr>
                                                        <th class="hide_me">key</th>
                                                        <th>D.Operation</th>
                                                        <th>Dépense</th>
                                                        <th>N°Facture</th>
                                                        <th>D.Facture</th>
                                                        <th>H.T</th>
                                                        <th>T.V.A</th>
                                                        <th>TTC</th>
                                                        <th>Règlement</th>
                                                        <th>N°.Règlement</th>
                                                        <th class="text-center"><i class="icon-cog52"></i></th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <!-- modal_ajout modal -->
                                    <div id="modal_AFact" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-brown-700">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-price-tags2"></i> &nbsp;AJOUTER FACTURE</h5>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-vertical">
                                                        <div class="row text-center">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Date Operation :</label>
                                                                    <input type="date" class="form-control border-black border-lg text-black text-center" id="Date_oper" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Type Dépense :</label>
                                                                    <select class="form-control border-black border-lg text-black" id="type_depense" data-live-search="true" data-width="100%"></select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>N° Facture :</label>
                                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_Fact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Date Facture :</label>
                                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_Fact" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Hors Taxe [HT] :</label>
                                                                    <input type="number" class="form-control border-black border-lg text-black" id="HT_Fact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>TVA :</label>
                                                                    <input type="number" class="form-control border-black border-lg text-black" id="TVA_Fact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>TTC :</label>
                                                                    <input type="number" class="form-control border-black border-lg text-black" id="TTC_Fact" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <legend class="text-semibold">Règlement de Facture</legend>
                                                        <div class="tabbable">
                                                            <ul class="nav nav-tabs nav-tabs-highlight2 nav-justified" id="NVLOC">
                                                                <li class="active"><a href="#Fact_BC" data-toggle="tab">BC</a></li>
                                                                <li><a href="#Fact_ESP" data-toggle="tab">Espèce</a></li>
                                                                <li><a href="#Fact_CHQ" data-toggle="tab">Chèque</a></li>
                                                                <li><a href="#Fact_VIR" data-toggle="tab">Virement</a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="Fact_BC">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            Pas de règlement
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_ESP">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <div class="form-group">
                                                                                <label>Date :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_DESP" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_CHQ">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>N° Chèque :</label>
                                                                                <input type="text" class="form-control border-black border-lg text-black" id="Fact_NCHQ" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Date :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_DCHQ" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_VIR">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>N° Virement :</label>
                                                                                <input type="text" class="form-control border-black border-lg text-black" id="Fact_NVIR" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Date Virement :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_DVIR" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="modal-footer text-center">
                                                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                    <button class="btn bg-brown-700" id="add_fact"><i class="icon-check"></i> Ajouter</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /modal_AFact modal -->

                                    <!-- modal_MFact modal -->
                                    <div id="modal_MFact" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header bg-teal-700">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-price-tags2"></i> &nbsp;MODIFIER FACTURE</h5>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-vertical">
                                                        <div class="row text-center">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label>Date Operation :</label>
                                                                    <input type="date" class="form-control border-black border-lg text-black text-center" id="Date_Moper" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Type Dépense :</label>
                                                                    <select class="form-control border-black border-lg text-black" id="type_depense_m" data-live-search="true" data-width="100%"></select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>N° Facture :</label>
                                                                    <input type="text" class="form-control border-black border-lg text-black" id="Num_MFact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Date Facture :</label>
                                                                    <input type="date" class="form-control border-black border-lg text-black" id="Date_MFact" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>Hors Taxe [HT] :</label>
                                                                    <input type="text" class="form-control border-black border-lg text-black" id="HT_MFact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>TVA :</label>
                                                                    <input type="text" class="form-control border-black border-lg text-black" id="TVA_MFact" placeholder="...">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label>TTC :</label>
                                                                    <input type="text" class="form-control border-black border-lg text-black" id="TTC_MFact" placeholder="...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <legend class="text-semibold">Règlement de Facture</legend>
                                                        <div class="tabbable">
                                                            <ul class="nav nav-tabs nav-tabs-highlight2 nav-justified" id="NVMFLOC">
                                                                <li class="active"><a href="#Fact_MBC" data-toggle="tab">BC</a></li>
                                                                <li><a href="#Fact_MESP" data-toggle="tab">Espèce</a></li>
                                                                <li><a href="#Fact_MCHQ" data-toggle="tab">Chèque</a></li>
                                                                <li><a href="#Fact_MVIR" data-toggle="tab">Virement</a></li>
                                                            </ul>

                                                            <div class="tab-content">
                                                                <div class="tab-pane active" id="Fact_MBC">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            Pas de règlement
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_MESP">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <!--div class="form-group">
                                                                                <label>Date :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_MDESP" placeholder="...">
                                                                            </div-->
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_MCHQ">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <div class="form-group">
                                                                                <label>N° Chèque :</label>
                                                                                <input type="text" class="form-control border-black border-lg text-black" id="Fact_MNCHQ" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <!--div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Date :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_MDCHQ" placeholder="...">
                                                                            </div>
                                                                        </div-->
                                                                    </div>
                                                                </div>

                                                                <div class="tab-pane" id="Fact_MVIR">
                                                                    <div class="row">
                                                                        <div class="col-md-12 text-center">
                                                                            <div class="form-group">
                                                                                <label>N° Virement :</label>
                                                                                <input type="text" class="form-control border-black border-lg text-black" id="Fact_MNVIR" placeholder="...">
                                                                            </div>
                                                                        </div>
                                                                        <!--div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Date Virement :</label>
                                                                                <input type="date" class="form-control border-black border-lg text-black" id="Fact_MDVIR" placeholder="...">
                                                                            </div>
                                                                        </div-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="modal-footer text-center">
                                                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                    <button class="btn bg-brown-700" id="mod_fact"><i class="icon-check"></i> Appliquer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /modal_FLOC modal -->

                                    <hr>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div id="chartContainer" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>