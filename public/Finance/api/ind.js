$(document).ready(function() {
    moment.locale('fr');
    var startDate, endDate = '';
    var data_table = [];

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        //dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        dom: '<"toolbar">fBrtip',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    var table = $('#table').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    var sql_etr = "SELECT\
                        AAM_NUM_DOSSIER,\
                        AAM_DES_CLIENT,\
                        AAM_NUM_FACTURE,\
                        DATE_FORMAT( AAM_DATE, '%d/%m/%Y' ) AS AAM_DATE,\
                        AAM_DEVISE,\
                        AAM_TOTAL_TTC AS AAM_TOTAL_D,\
                        IF ( AAM_NUM_REGLEMENT IS NULL OR AAM_NUM_REGLEMENT = '', 'BC', 'VIREMENT' ) AS AAM_BON_COMMAND,\
                        AAM_NUM_REGLEMENT,\
                        DATE_FORMAT( AAM_DATE_REGLEMENT, '%d/%m/%Y' ) AS AAM_DATE_REGLEMENT \
                    FROM\
                        invoice\
                    WHERE\
                        (AAM_DATE BETWEEN '{{dd}}' AND '{{df}}') AND AAM_NUM_FACTURE IS NOT NULL ";

    var sql_etr_end = "ORDER BY date(AAM_DATE) ASC";

    var sql_loc = "SELECT\
                        d.DM_NUM_DOSSIER AS AAM_NUM_DOSSIER,\
                        AAM_DES_CLIENT,\
                        AAM_NUM_FACTURE,\
                        DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,\
                        AAM_TOT_NON_TAXABLE,\
                        AAM_TO_TAXABLE,\
                        AAM_TVA_PORCENTAGE,\
                        AAM_TIMBRE,\
                        IF (AAM_TYPE_FACT = 'V',CONCAT('-',AAM_TOTAL_TTC),AAM_TOTAL_TTC) AS AAM_TOTAL_TTC,\
                        FT_LIBELLE,\
                        IF ( AAM_BON_COMMAND IS NULL OR AAM_BON_COMMAND = '', 'BC', AAM_BON_COMMAND ) AS AAM_BON_COMMAND,\
                        AAM_NUM_CHEQUE,\
                        AAM_BANQUE,\
                        DATE_FORMAT( AAM_DATE_REG, '%d/%m/%Y' ) AS AAM_DATE_REG \
                    FROM\
                        avis_arrive_mig\
                        INNER JOIN dossier_maritime d ON AAM_CODE_DOSSIER = d.DM_CLE \
	                    LEFT JOIN type_facture ON AAM_TYPE_FACT = FT_CODE \
                    WHERE\
                        (AAM_DATE_FACTURE BETWEEN '{{dd}}' AND '{{df}}') AND AAM_NUM_FACTURE IS NOT NULL ";

    var sql_loc_end = "ORDER BY date(AAM_DATE_FACTURE) ASC";

    /*$.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_CLIENT'])).fail(function(data) {
        console.log('fail', data);
    }).done(function(data) {
        $('#Clients').empty().append('<option value="ALL">Tous les agents</option><option disabled>──────────</option>');
        data.forEach(function(element) {
            $('#Clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    });*/
    $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_AGT_ETR'])).fail(function(data) {
        console.log('fail', data);
    }).done(function(data) {
        $('#Clients').empty().append('<option value="ALL">Client Etranger</option><option disabled>──────────</option>');
        data.forEach(function(element) {
            $('#Clients').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
        }, this);
    }).always(function() {
        $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_CLIENT'])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            $('#Clients').append('<option value="ALL">Client Locale</option><option disabled>──────────</option>');
            data.forEach(function(element) {
                $('#Clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
            }, this);
        }).always(function() {
            $(".livesearch").chosen({
                width: '100%',
                enable_split_word_search: true
            });
        });
    });

    $('#Type_Fact').on('change', function() {
        switch (this.value) {
            case '1':
                $.post('/GTRANS/public/Achat/api/ind.php', JSON.stringify(['GET_AGT_ETR'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    $('#Clients').empty().append('<option value="ALL">Client Etranger</option><option disabled>──────────</option>');
                    data.forEach(function(element) {
                        $('#Clients').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
                    }, this);
                }).always(function() {
                    $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_CLIENT'])).fail(function(data) {
                        console.log('fail', data);
                    }).done(function(data) {
                        $('#Clients').append('<option disabled>──────────</option><option value="ALL">Client Locale</option><option disabled>──────────</option>');
                        data.forEach(function(element) {
                            $('#Clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        }, this);
                    }).always(function() {
                        $(".livesearch").trigger("chosen:updated");
                    });
                });
                break;

            default:
                $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_CLIENT'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    $('#Clients').empty().append('<option value="ALL">Client Locale</option><option disabled>──────────</option>');
                    data.forEach(function(element) {
                        $('#Clients').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                    }, this);
                }).always(function() {
                    $(".livesearch").trigger("chosen:updated");
                });
                break;
        }
    });



    $('#get_tab_btn').on('click', function() {
        var sql = '';
        var lna = 'Liste des Factures ';
        switch ($('#Type_Fact').val()) {
            case '1':
                lna += 'Etranger';
                if ($('#Clients').val() == 'ALL') {
                    sql = sql_etr.replace('{{dd}}', startDate).replace('{{df}}', endDate) + sql_etr_end;
                } else {
                    sql = sql_etr.replace('{{dd}}', startDate).replace('{{df}}', endDate) + " AND AAM_CODE_CLIENT = " + $('#Clients').val() + " " + sql_etr_end;
                }
                break;
            default:
                lna += 'Locale';
                if ($('#Clients').val() == 'ALL') {
                    sql = sql_loc.replace('{{dd}}', startDate).replace('{{df}}', endDate) + sql_loc_end;
                } else {
                    sql = sql_loc.replace('{{dd}}', startDate).replace('{{df}}', endDate) + " AND AAM_CODE_CLIENT = " + $('#Clients').val() + " " + sql_loc_end;
                }
                break;
        }
        $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_TAB_COLUMNS', sql])).done((data) => {
            var tfffo = '';
            data.forEach(element => {
                tfffo += '<th></th>'; //' + element.title + '
            });
            $('#table').DataTable().clear().destroy();
            $('#table').empty();
            $("#table").append(
                $('<tfoot/>').append("<tr>" + tfffo + "</tr>")
                //$('<tfoot/>').append($("#table thead tr").clone())
            );
            $('#table tfoot').insertAfter($('#table thead'));
            table = $('#table').DataTable({
                aaSorting: [],
                bSort: false,
                bPaginate: false,
                orderCellsTop: true,
                buttons: {
                    buttons: [{
                            extend: 'print',
                            footer: true,
                            className: 'btn bg-blue btn-icon',
                            text: '<i class="icon-printer position-left"></i>',
                            title: lna,
                            message: $('.daterange-ranges span').text(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(win) {
                                // Style the body..
                                $(win.document.body)
                                    .addClass('asset-print-body')
                                    .css({ 'text-align': 'center' });

                                /* Style for the table */
                                $(win.document.body)
                                    .find('table')
                                    .addClass('compact minimalistBlack')
                                    .css({
                                        margin: '5px 5px auto'
                                    });
                            },
                        },
                        {
                            extend: 'pdfHtml5',
                            className: 'btn bg-danger btn-icon',
                            footer: true,
                            text: '<i class="icon-file-pdf position-left"></i>',
                            title: lna,
                            message: $('.daterange-ranges span').text(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(doc) {
                                doc.styles.title = {
                                    fontSize: '20',
                                    alignment: 'center'
                                };
                                doc.styles.tableHeader = {
                                    bold: true,
                                    fontSize: 11,
                                    color: "white",
                                    fillColor: "#2d4154",
                                    border: "3px solid #000000",
                                    width: "100%",
                                    "text-align": "left",
                                    "border-collapse": "collapse",

                                };
                            }
                        },
                        {
                            extend: 'excel',
                            className: 'btn bg-success btn-icon',
                            footer: true,
                            text: '<i class="icon-file-excel position-left"></i>',
                            title: lna,
                            message: $('.daterange-ranges span').text(),
                            exportOptions: {
                                columns: ':visible'
                            },
                            customize: function(xlsx) {
                                var sheet = xlsx.xl.worksheets['sheet1.xml'];

                                $('row c[r^="C"]', sheet).attr('s', '2');
                            }
                        },
                        {
                            extend: 'colvis',
                            text: '<i class="icon-eye-minus"></i> <span class="caret"></span>',
                            className: 'btn bg-default btn-icon'
                        }
                    ],
                },
                bStateSave: true,
                bDestroy: true,
                select: false,
                destroy: true,
                scrollY: true,
                scrollX: true,
                ajax: {
                    url: "/GTRANS/public/Finance/api/ind.php",
                    type: "POST",
                    data: function(d) {
                        return JSON.stringify({
                            "0": "GET_TAB_DATA",
                            "1": sql
                        });
                    }
                },
                columns: data,
                aoColumnDefs: [{
                    "type": "number",
                    "targets": 0,
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.AAM_NUM_DOSSIER + '">' + row.AAM_NUM_DOSSIER + '</a>';
                    }
                }, {
                    "targets": 2,
                    "render": function(data, type, row) {
                        return '<a id="SHOWFACT" class="btn btn-link"><i class="icon-credit-card position-left"></i> ' + row.AAM_NUM_FACTURE + '</a>';
                    }
                }, {
                    "targets": 3,
                    "type": "date",
                    "dateFormat": "dd/mm/yyyy",
                }],
                footerCallback: function(row, data, start, end, display) {
                    var api = this.api(),
                        data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function(i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                            i : 0;
                    };

                    if ($('#Type_Fact').val() == 1) {
                        // Total over this page
                        pageTotal = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        //console.log(api.column(4, { page: 'current' }).data())
                        var usdd = (api.column(4, { page: 'current' }).data()[0] !== undefined) ? api.column(4, { page: 'current' }).data()[0] : 'N';
                        // Update footer
                        $(api.column(3).footer()).html('Total');
                        $(api.column(4).footer()).html(usdd + ' =');
                        $(api.column(5).footer()).html(
                            Number(pageTotal).toFixed(3)
                        );
                    } else {
                        // Total over this page
                        pageTotal_NT = api
                            .column(4, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(4).footer()).html(
                            Number(pageTotal_NT).toFixed(3)
                        );
                        pageTotal_T = api
                            .column(5, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(5).footer()).html(
                            Number(pageTotal_T).toFixed(3)
                        );
                        pageTotal_TVA = api
                            .column(6, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(6).footer()).html(
                            Number(pageTotal_TVA).toFixed(3)
                        );
                        pageTotal_Timb = api
                            .column(7, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(7).footer()).html(
                            Number(pageTotal_Timb).toFixed(3)
                        );
                        pageTotal_TTC = api
                            .column(8, { page: 'current' })
                            .data()
                            .reduce(function(a, b) {
                                return intVal(a) + intVal(b);
                            }, 0);

                        // Update footer
                        $(api.column(8).footer()).html(
                            Number(pageTotal_TTC).toFixed(3) + ' DT'
                        );
                    }
                },
                initComplete: function() {
                    $("div.toolbar").html('<div class="row mb-20"><div class="col-sm-6"><div class="form-group"><label class="text-black">Filtrer Client dans le tableau</label><div id="flspinpcl"></div></div></div><div class="col-sm-6"><div class="form-group"><label class="text-black">Filtrer Type de Règlement dans le tableau</label><div id="flspinpreg"></div></div></div>');
                    if ($('#Type_Fact').val() == 1) {
                        this.api().columns().every(function() {
                            var column = this;
                            if (column[0] == 1) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpcl").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()

                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            } else if (column[0] == 6) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpreg").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            }
                        });
                    } else {
                        this.api().columns().every(function() {
                            var column = this;
                            if (column[0] == 1) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpcl").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });
                            } else if (column[0] == 10) {
                                var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                                    .appendTo($("#flspinpreg").empty())
                                    .on('change', function() {
                                        var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                        );

                                        column
                                            .search(val ? '^' + val + '$' : '', true, false)
                                            .draw();
                                    });

                                column.data().unique().sort().each(function(d, j) {
                                    select.append('<option value="' + d + '">' + d + '</option>');
                                });

                            }
                        });
                    }
                }

            });
        }).fail((errors) => {
            console.log('error : ', errors);
        }).always((data) => {
            $('#table tbody').on('click', 'a#SHOWFACT', function() {
                data_table = table.row($(this).parents('tr')).data();
                //console.log(data_table)
                if ($('#Type_Fact').val() == 1) {
                    $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_FACTETR_DETAIL', data_table.AAM_NUM_FACTURE]))
                        .done(function(data) {
                            $('#ndosetr').text(data[0].AAM_NUM_DOSSIER);
                            $('#nfactetr').text(data[0].AAM_NUM_FACTURE);
                            $('#dateetr').text(data[0].AAM_DATE_FACTURE);
                            $('#totetr').text(data[0].AAM_TOTAL_TTC + ' ' + data[0].AAM_DEVISE);
                            $('#INP_FETR_VIREMENT_Num').val(data[0].AAM_NUM_REGLEMENT);
                            $('#INP_FETR_VIREMENT_DATE').val(data[0].AAM_DATE_REGLEMENT);
                        }).fail(function(data) {
                            new PNotify({
                                title: 'Rechercher une facture',
                                text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                            });
                        }).always(function(data) {
                            $('#modal_Reglement_etr').modal('show');
                        });
                } else {
                    $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['GET_FACTLOC_DETAIL', data_table.AAM_NUM_FACTURE]))
                        .done(function(data) {
                            $('#ndosloc').text(data[0].AAM_NUM_DOSSIER);
                            $('#nfactloc').text(data[0].AAM_NUM_FACTURE);
                            $('#dateloc').text(data[0].AAM_DATE_FACTURE);
                            $('#totloc').text(data[0].AAM_TOTAL_TTC);
                            if (data[0].AAM_BON_COMMAND == 'VIREMENT') {
                                $('#NVMFLOC.nav-tabs a[href="#VIREMENT"]').tab('show');
                                $('#INP_F_VIREMENT_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_VIREMENT_Num').val(data[0].AAM_NUM_CHEQUE);
                                $('#INP_F_VIREMENT_DATE').val(data[0].AAM_DATE_REG);
                            } else if (data[0].AAM_BON_COMMAND == 'CHEQUE') {
                                $('#NVMFLOC.nav-tabs a[href="#CHEQUE"]').tab('show');
                                $('#INP_F_CHEQUE_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_CHEQUE_Num').val(data[0].AAM_NUM_CHEQUE);
                                $('#INP_F_CHEQUE_DATE').val(data[0].AAM_DATE_REG);
                            } else if (data[0].AAM_BON_COMMAND == 'TRAITE') {
                                $('#NVMFLOC.nav-tabs a[href="#TRAITE"]').tab('show');
                                $('#INP_F_TRAITE_Num').val(data[0].AAM_NUM_CHEQUE);
                                $('#INP_F_TRAITE_Banq').val(data[0].AAM_BANQUE);
                                $('#INP_F_TRAITE_DATE').val(data[0].AAM_DATE_REG);
                            } else if (data[0].AAM_BON_COMMAND == 'ESPECE') {
                                $('#NVMFLOC.nav-tabs a[href="#ESPECE"]').tab('show');
                                $('#INP_F_ESPECE_DATE').val(data[0].AAM_DATE_REG);
                            } else {
                                $('#NVMFLOC.nav-tabs a[href="#BC"]').tab('show');
                            }
                        }).fail(function(data) {
                            new PNotify({
                                title: 'Rechercher une facture',
                                text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                            });
                        }).always(function(data) {
                            $('#modal_Reglement').modal('show');

                        });
                }
            });
        });

        $('#REG_ETR_BTN').on('click', function() {
            var ud = {
                'AAM_NUM_REGLEMENT': $('#INP_FETR_VIREMENT_Num').val(),
                'AAM_DATE_REGLEMENT': $('#INP_FETR_VIREMENT_DATE').val()
            }
            $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['SET_ETR_REG', ud, data_table.AAM_NUM_FACTURE]))
                .done(function(data) {
                    table.ajax.reload();
                    $('#modal_Reglement_etr').modal('hide');
                }).fail(function(error) {
                    console.log('fail : ', error);
                    new PNotify({
                        title: 'Règlement Facture etranger',
                        text: "Problème inattendue, contactez le développeur",
                        addclass: 'stack-bottom-right bg-warning-700',
                        icon: 'icon-cancel-circle2',
                        delay: 4000,
                        stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                    });
                })
        });

        $('#REG_BTN').on('click', function() {
            var ud = {};
            var sel = $("ul.nav-tabs li.active a").attr('href');
            switch (sel) {
                case '#BC':
                    ud = {
                        'AAM_BON_COMMAND': 'BC',
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_DATE_REG': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;

                case '#ESPECE':
                    ud = {
                        'AAM_BON_COMMAND': 'ESPECE',
                        'AAM_DATE_REG': $('#INP_F_ESPECE_DATE').val(),
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;

                case '#CHEQUE':
                    ud = {
                        'AAM_BON_COMMAND': 'CHEQUE',
                        'AAM_NUM_CHEQUE': $('#INP_F_CHEQUE_Num').val(),
                        'AAM_BANQUE': $('#INP_F_CHEQUE_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_CHEQUE_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_CHEQUE_Num').val() + 'Banque : ' + $('#INP_F_CHEQUE_Banq').val()
                    }
                    break;

                case '#TRAITE':
                    ud = {
                        'AAM_BON_COMMAND': 'TRAITE',
                        'AAM_NUM_CHEQUE': $('#INP_F_TRAITE_Num').val(),
                        'AAM_BANQUE': $('#INP_F_TRAITE_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_TRAITE_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_TRAITE_Num').val() + 'Banque : ' + $('#INP_F_TRAITE_Banq').val()
                    }
                    break;

                case '#VIREMENT':
                    ud = {
                        'AAM_BON_COMMAND': 'VIREMENT',
                        'AAM_NUM_CHEQUE': $('#INP_F_VIREMENT_Num').val(),
                        'AAM_BANQUE': $('#INP_F_VIREMENT_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_VIREMENT_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_VIREMENT_Num').val() + 'Banque : ' + $('#INP_F_VIREMENT_Banq').val()
                    }
                    break;
                default:
                    ud = {
                        'AAM_BON_COMMAND': 'BC',
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_DATE_REG': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }
                    break;
            }
            $.post('/GTRANS/public/Finance/api/ind.php', JSON.stringify(['SET_LOC_REG', ud, data_table.AAM_NUM_FACTURE]))
                .done(function(data) {
                    table.ajax.reload();
                    $('#modal_Reglement').modal('hide');
                }).fail(function(error) {
                    console.log('fail : ', error);
                    new PNotify({
                        title: 'Règlement Facture etranger',
                        text: "Problème inattendue, contactez le développeur",
                        addclass: 'stack-bottom-right bg-warning-700',
                        icon: 'icon-cancel-circle2',
                        delay: 4000,
                        stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 }
                    });
                })
        });
    });

});