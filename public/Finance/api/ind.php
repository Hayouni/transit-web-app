<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {

    case 'GET_TAB_DATA':
        $db = new MySQL();
        $GET_TAB_DATA["data"] = $db->get_results($response[1]);
        echo json_encode($GET_TAB_DATA);
        break;

    case 'GET_TAB_COLUMNS':
        $database = new MySQL();
        $fields = $database->list_fields($response[1]);
        $Filter_out = [];
        $Columns = [];
        foreach ($fields as $key => $value) {
            $rs2 = Dictio($value->name);
            if (!empty($rs2)) {
                array_push($Columns, array('data' => $value->name,'title' => $rs2 ));
            }
        }
        echo json_encode($Columns);
        break;

    case 'GET_CLIENT':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT CL_CODE,CL_LIBELLE FROM trans.client");
        echo json_encode($fournisseurs);
        break;

    case 'GET_FACTETR_DETAIL':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT
                                                AAM_CODE,
                                                AAM_NUM_FACTURE,
                                                AAM_NUM_DOSSIER,
                                                DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                AAM_TOTAL_TTC,
                                                AAM_DEVISE,
                                                DATE_FORMAT( AAM_DATE_REGLEMENT, '%Y-%m-%d' ) AS AAM_DATE_REGLEMENT,
                                                AAM_NUM_REGLEMENT 
                                            FROM
                                                invoice 
                                            WHERE
                                                AAM_NUM_FACTURE = '{$response[1]}'");
        echo json_encode($fournisseurs);
        break;

    case 'SET_ETR_REG':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_NUM_FACTURE' => $response[2]);
        echo $db->update( 'invoice', $response[1], $update_where_AA );
        break;

    case 'GET_FACTLOC_DETAIL':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT
                                                AAM_CODE,
                                                AAM_NUM_FACTURE,
                                                AAM_NUM_DOSSIER,
                                                DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                AAM_TOTAL_TTC,
                                                AAM_BON_COMMAND,
                                                AAM_NUM_CHEQUE,
                                                AAM_BANQUE,
                                                DATE_FORMAT( AAM_DATE_REG, '%Y-%m-%d' ) AS AAM_DATE_REG
                                            FROM
                                                avis_arrive_mig 
                                            WHERE
                                                AAM_NUM_FACTURE = '{$response[1]}'");
        echo json_encode($fournisseurs);
        break;

    case 'SET_LOC_REG':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_NUM_FACTURE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA );
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}

function Dictio($var = null)
{
    switch ($var) {
        // Facture
        case 'AAM_DES_CLIENT':
            return 'Client';
        case 'AAM_NUM_FACTURE':
            return 'N° Facture';
        case 'AAM_DATE':
        case 'AAM_DATE_FACTURE':
            return 'Date Facture';
        case 'AAM_DEVISE':
            return 'Devise';
        case 'AAM_TOTAL_D':
            return 'Montant';
        case 'AAM_TOTAL_TTC_DINARS':
            return 'Montant DT';
        case 'AAM_BON_COMMAND':
            return 'Règlement';
        case 'AAM_NUM_DOSSIER':
            return 'N° Dossier';
        case 'FT_LIBELLE' :
            return 'Type.F';
        case 'AAM_NUM_CHEQUE':
        case 'AAM_NUM_REGLEMENT':
            return 'N°.Reg';
        case 'AAM_TOTAL_TTC':
            return 'T.T.C';
        case 'AAM_TOT_NON_TAXABLE':
            return 'Non Taxable';
        case 'AAM_TO_TAXABLE':
            return 'Taxable';
        case 'AAM_TVA_PORCENTAGE':
            return 'TVA';
        case 'AAM_TIMBRE':
            return 'Timbre';
        case 'AAM_BANQUE':
            return 'Banque';
        case 'AAM_DATE_REG':
        case 'AAM_DATE_REGLEMENT':
            return 'Date.Reg';
        default:
            return '-';
    }
}

function get_client($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT CL_CODE,CL_LIBELLE FROM client");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['CL_CODE']]=$value['CL_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT CL_LIBELLE FROM client WHERE CLE_CODE = {$var}");
    }
    
}

function get_fournisseur($var = null)
{
    $database = new MySQL();
    if (is_null($var)) {
        $res = $database->get_results("SELECT FR_CODE,FR_LIBELLE FROM fournisseur");
        $out = [];
        foreach ($res as $key => $value) {
            $out[$value['FR_CODE']]=$value['FR_LIBELLE'];
        }
        return $out;
    } else {
        return $database->get_results("SELECT FR_LIBELLE FROM fournisseur WHERE FR_CODE = {$var}");
    }
    
}

?>