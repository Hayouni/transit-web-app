<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur","Financier","Commercial","Comptable"])) {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | V.Règlement</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.html5.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/ind.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 14px;
            font-weight: 700;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }

        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Vente</span> - Règlement </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold"><i class="icon-books"></i> - Recherche</h6>
                                </div>

                                <div class="panel-body">
                                    <div class="row text-center">
                                        <div class="col-lg-3">
                                            <label class="display-block">Type Facture </label>
                                            <select class="border-black border-lg text-black form-control" id="Type_Fact">
                                                <option value="1" style="color:#1565C0">ETREANGER</option>
                                                <option value="2" style="color:#C62828">LOCALE</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="display-block">Sélectionnez la plage de dates </label>
                                                <button type="button" class="btn bg-teal-400 daterange-ranges" id="daterange">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="display-block">Client </label>
                                            <select class="border-black border-lg text-black form-control livesearch" id="Clients">
                                                <option value="ALL">Tous les Client</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="get_tab_btn"><i class="icon-search4"></i></button>
                                        </div>
                                    </div>
                                    <table class="table display datatable-button-print-rows table-striped table-bordered" id="table">
                                        <thead></thead>
                                        <tbody class="text-black"></tbody>
                                        <tfoot></tfoot>
                                    </table>

                                    <!-- reglement etr modal -->
                                    <div id="modal_Reglement_etr" class="modal fade">
                                        <div class="modal-dialog modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Règlement Facture Etranger</h5>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">N°Dos:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="ndosetr"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">N°Fact:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="nfactetr"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">Date:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="dateetr"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">Total:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="totetr"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend class="text-semibold">Virement</legend>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <input type="text" id="INP_FETR_VIREMENT_Num" class="form-control input-xs text-bold text-center" placeholder="Numéro" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="date" id="INP_FETR_VIREMENT_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                    <button class="btn btn-primary" id="REG_ETR_BTN"><i class="icon-check"></i> Effectuer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /reglement ter modal -->

                                    <!-- reglement modal -->
                                    <div id="modal_Reglement" class="modal fade">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Règlement Facture</h5>
                                                </div>

                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">N°Dos:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="ndosloc"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">N°Fact:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="nfactloc"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">Date:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="dateloc"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-lg-4 control-label">Total:</label>
                                                                <div class="col-lg-8">
                                                                    <div class="form-control-static text-black" id="totloc"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <legend class="text-semibold">Règlement</legend>
                                                    <div class="tabbable nav-tabs-vertical nav-tabs-left">
                                                        <ul class="nav nav-tabs nav-tabs-highlight" id="NVMFLOC">
                                                            <li class="active"><a href="#BC" data-toggle="tab"><i class="icon-file-text3 position-left"></i> BC</a></li>
                                                            <li><a href="#ESPECE" data-toggle="tab"><i class="icon-cash position-left"></i> ESPECE</a></li>
                                                            <li><a href="#CHEQUE" data-toggle="tab"><i class="icon-file-spreadsheet2 position-left"></i> CHEQUE</a></li>
                                                            <li><a href="#TRAITE" data-toggle="tab"><i class="icon-price-tag position-left"></i> TRAITE</a></li>
                                                            <li><a href="#VIREMENT" data-toggle="tab"><i class="icon-credit-card2 position-left"></i> VIREMENT BANCAIRE</a></li>
                                                        </ul>

                                                        <div class="tab-content">
                                                            <div class="tab-pane active has-padding" id="BC">
                                                                Pas Règlement.
                                                            </div>

                                                            <div class="tab-pane has-padding" id="ESPECE">
                                                                <div class="form-group">
                                                                    <input type="date" id="INP_F_ESPECE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane has-padding" id="CHEQUE">
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_CHEQUE_Num" class="form-control input-xs text-bold text-center" placeholder="N° Cheque" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_CHEQUE_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="date" id="INP_F_CHEQUE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane has-padding" id="TRAITE">
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_TRAITE_Num" class="form-control input-xs text-bold text-center" placeholder="Numéro" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_TRAITE_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="date" id="INP_F_TRAITE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                </div>
                                                            </div>

                                                            <div class="tab-pane has-padding" id="VIREMENT">
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_VIREMENT_Num" class="form-control input-xs text-bold text-center" placeholder="N° de Transfert" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="text" id="INP_F_VIREMENT_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="date" id="INP_F_VIREMENT_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                    <button class="btn btn-primary" id="REG_BTN"><i class="icon-check"></i> Effectuer</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /reglement modal -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>