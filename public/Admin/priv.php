<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur"])) {
    header("location: /GTRANS");
	exit();
}
?>
<?php
//session_start();
$default_pic = $_SERVER['DOCUMENT_ROOT']."/GTRANS/assets/images/default-img.png";
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/install_sql/db_conx.php');
$_SESSION['message'] = '';
//the form has been submitted with post
if (isset($_POST['CIN']) && isset($_POST['NOM'])) {
    
    //define other variables with submitted values from $_POST
    $CIN = $db_conx->real_escape_string($_POST['CIN']);
    $NOM = $db_conx->real_escape_string($_POST['NOM']);
    $PRENOM = $db_conx->real_escape_string($_POST['PRENOM']);
    $DDN = $db_conx->real_escape_string($_POST['DDN']);
    $LDN = $db_conx->real_escape_string($_POST['LDN']);
    $ADRESSE = $db_conx->real_escape_string($_POST['ADRESSE']);
    $TEL = $db_conx->real_escape_string($_POST['TEL']);
    $MAIL = $db_conx->real_escape_string($_POST['MAIL']);
    $FB = $db_conx->real_escape_string($_POST['FB']);
    $TW = $db_conx->real_escape_string($_POST['TW']);
    $LNKD = $db_conx->real_escape_string($_POST['LNKD']);

    $pat = $_SERVER['DOCUMENT_ROOT']."/GTRANS/public/users/user_data/".$CIN;
    if (!file_exists($pat)) {
        mkdir($pat, 0755);
    }
    if (!empty($_FILES['PIC']['name'])) {
        //make sure the file type is image
        if (preg_match("!image!",$_FILES['PIC']['type'])) {
            
            $PIC_NAME = $_FILES['PIC']['name'];
            
            //path were our PIC image will be stored
            $PIC_path = $db_conx->real_escape_string($pat."/".$_FILES['PIC']['name']);

            //copy image to images/ folder 
            if (copy($_FILES['PIC']['tmp_name'], $PIC_path)){
                
                //set session variables to display on welcome page
                $_SESSION['CIN'] = $CIN;
                $_SESSION['PIC'] = $PIC_path;

                //insert user data into database
                $sql = "INSERT INTO users (id ,username, name, subname, codezip, vill, address1, email, phone, soc3, soc2, soc1, avatar) VALUES 
                ('', '$CIN', '$NOM', '$PRENOM', '$DDN', '$LDN', '$ADRESSE', '$MAIL', '$TEL', '$FB', '$TW', '$LNKD', '$PIC_NAME')";
                
                //check if mysql query is successful ;
                if (mysqli_query($db_conx, $sql)){
                    $_SESSION['message'] = "Ajoutez-vous avec succès!". " Ajout de $CIN à la base de données!";
                }
                else {
                    $_SESSION['message'] = 'Personnel n\'a pas pu être ajouté à la base de données!';
                }
                mysqli_close($db_conx);     
                header('Location: ./priv');
                exit;    
            }
            else {
                $_SESSION['message'] = 'Le téléchargement de l\'image a échoué!';
            }
        }
        else {
            $_SESSION['message'] = 'Veuillez télécharger uniquement des images GIF, JPG ou PNG.';
        }
    } else {
        $PIC_NAME = "default-img.png";
        
        //path were our PIC image will be stored
        $PIC_path = $db_conx->real_escape_string($pat."/default-img.png");

        //copy image to images/ folder 
        if (copy($default_pic, $PIC_path)){
            
            //insert user data into database
            $sql = "INSERT INTO users (id ,username, name, subname, codezip, vill, address1, email, phone, soc3, soc2, soc1, avatar) VALUES 
            ('', '$CIN', '$NOM', '$PRENOM', '$DDN', '$LDN', '$ADRESSE', '$MAIL', '$TEL', '$FB', '$TW', '$LNKD', '$PIC_NAME')";
            
            //check if mysql query is successful ;
            if (mysqli_query($db_conx, $sql)){
                $_SESSION['message'] = "Ajoutez-vous avec succès!". " Ajout de $CIN à la base de données!";
            }
            else {
                $_SESSION['message'] = 'Personnel n\'a pas pu être ajouté à la base de données!';
            }
            mysqli_close($db_conx);  
            header('Location: ./priv');
            exit;       
        }
        else {
            $_SESSION['message'] = 'Le téléchargement de l\'image a échoué!';
        }
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|Utilisateurs</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->

    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/priv.js"></script>
    <!-- /theme JS files -->

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Gestions utilisateurs</span> - privilèges & mot de passe
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Liste des utilisateurs</h6>
                                </div>

                                <div class="panel-body">
                                    <table class="table datatable-button-print-rows">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Utilisateur</th>
                                                <th>Nom | Prénom</th>
                                                <th>Email</th>
                                                <th>Type</th>
                                                <th>Statut</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Ajouter une nouvelle personne</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <!--button type="button" id="btn_add" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_fourn"><i class="fa fa-plus-circle"></i> Ajouter</button-->
                                            <div class="label label-info"><?= $_SESSION['message'] ?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <form action="priv" method="POST" enctype="multipart/form-data" autocomplete="off">
                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <label>CIN <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="CIN" id="CIN" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Nom <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="NOM" id="NOM" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Prénom </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="PRENOM" id="PRENOM" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Code postal </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="DDN" id="DDN" value="">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <label>Ville </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="LDN" id="LDN" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Adresse </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="ADRESSE" id="ADRESSE" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Téléphone </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="TEL" id="TEL" value="">
                                            </div>
                                            <div class="col-md-3">
                                                <label>Email </label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="MAIL" id="MAIL" value="">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-3">
                                                <label>Photo</label>
                                                <input type="file" class="file-styled-primary" name="PIC" id="PIC">
                                            </div>
                                            <div class="col-md-3">
                                                <label><i class="icon-facebook2"></i></label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="FB" id="FB" value="Facebook">
                                            </div>
                                            <div class="col-md-3">
                                                <label><i class="icon-twitter2"></i></label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="TW" id="TW" value="Twitter">
                                            </div>
                                            <div class="col-md-3">
                                                <label><i class="icon-linkedin"></i></label>
                                                <input type="text" class="form-control border-black border-lg text-black" name="LNKD" id="LNKD" value="Linkedin">
                                            </div>
                                        </div>
                                        <div class="text-right">
                                            <button type="submit" class="btn btn-primary"><i class="icon-plus-circle2"></i> Valider</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- user_change modal -->
					<div id="modal_user_change" class="modal fade">
						<div class="modal-dialog modal-xs">
							<div class="modal-content">
								<div class="modal-body">
                                    <h5 class="content-group">
										<span class="label label-flat label-rounded label-icon border-grey text-grey mr-10">
											<i class="icon-user-tie"></i>
										</span>

										<a class="text-default" id="u_mod_name"></a>
									</h5>
									<div class="content-group">
                                        <div class="row row-seamless btn-block-group">
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-default btn-block btn-float btn-float-lg" id="RES_BTN">
                                                    <i class="icon-user-lock text-warning-600"></i>
                                                    <span>Réinitial.M.Pass</span>
                                                </button>

                                                <button type="button" class="btn btn-default btn-block btn-float btn-float-lg" id="ACT_BTN">
                                                    <i class="icon-user-check text-success-400"></i>
                                                    <span>Activer</span>
                                                </button>
                                            </div>

                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-default btn-block btn-float btn-float-lg" id="DACT_BTN">
                                                    <i class="icon-user-block text-blue"></i>
                                                    <span>Désactiver</span>
                                                </button>

                                                <button type="button" class="btn btn-default btn-block btn-float btn-float-lg" id="DEL_BTN">
                                                    <i class="icon-user-cancel text-danger-400"></i>
                                                    <span>Supprimer</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>	
                                    <select name="select" class="form-control input-xlg" id="u_mod_level">
                                        <option value="Administrateur">Administrateur</option>
                                        <option value="Financier">Financier</option>
                                        <option value="Commercial">Commercial</option>
                                        <option value="Comptable">Comptable</option>
                                        <option value="Exploitation">Exploitation</option>
                                    </select>	
								</div>

								<div class="modal-footer text-center">
                                <button type="button" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded" data-dismiss="modal"><i class="icon-x"></i></button>
								</div>
							</div>
						</div>
					</div>
					<!-- /user_change modal -->


                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>