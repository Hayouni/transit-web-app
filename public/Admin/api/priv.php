<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'get_users':
        $db = new MySQL();
        $get_users["data"] = $db->get_results("SELECT concat( name, ' ',subname ) as name ,username,email,userlevel,avatar,activated FROM trans.users");
        echo json_encode($get_users);
        break;

    case 'RES_PASS':
        $db = new MySQL();
        $new_pass = generateRandomString(4);
        $update_where = array( 'username' => $response[1] );
        $update_data = array( 'password' => $new_pass );
        $retVal = $db->update( 'users', $update_data, $update_where, 1 );
        if ($retVal == '1'){
            echo json_encode('{"msg":"Nouveau mot de passe : '.$new_pass.'"}');
        }else {
            echo json_encode('{"msg":"Erreur de changement de mot de passe !"}');
        }
        break;

    case 'ACT_ACC':
        $db = new MySQL();
        $update_where = array( 'username' => $response[1] );
        $update_data = array( 'activated' => 1 );
        $retVal = $db->update( 'users', $update_data, $update_where, 1 );
        if ($retVal == '1'){
            echo json_encode('{"msg":"Le compte nommé [ '.$response[1].' ] est Activé"}');
        }else {
            echo json_encode('{"msg":"Erreur d\'activation !"}');
        }
        break;

    case 'DACT_ACC':
        $db = new MySQL();
        $update_where = array( 'username' => $response[1] );
        $update_data = array( 'activated' => 0 );
        $retVal = $db->update( 'users', $update_data, $update_where, 1 );
        if ($retVal == '1'){
            echo json_encode('{"msg":"Le compte nommé [ '.$response[1].' ] est Désactivé"}');
        }else {
            echo json_encode('{"msg":"Erreur désactivation !"}');
        }
        break;

    case 'DEL_ACC':
        $db = new MySQL();
        $where_cont = array( 'username' => $response[1] );
        $retVal = $db->delete( 'users', $where_cont );
        if ($retVal == '1'){
            echo json_encode('{"msg":"Le compte nommé [ '.$response[1].' ] est supprimé"}');
        }else {
            echo json_encode('{"msg":"Erreur Suppression !"}');
        }
        break;

    case 'CH_PRIV_ACC':
        $db = new MySQL();
        $update_where = array( 'username' => $response[1] );
        $update_data = array( 'userlevel' => $response[2] );
        $retVal = $db->update( 'users', $update_data, $update_where, 1 );
        if ($retVal == '1'){
            echo json_encode('{"msg":"niveau d\'utilisateur [ '.$response[1].' ] changé vers : '.$response[2].'"}');
        }else {
            echo json_encode('{"msg":"une erreur est survenue !"}');
        }
        break;
    
    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
?>