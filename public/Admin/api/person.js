$(document).ready(function() {
    // Primary file input
    $(".file-styled-primary").uniform({
        fileButtonClass: 'action btn bg-blue',
        fileButtonHtml: '<i class="icon-cloud-upload"></i>',
        fileDefaultHtml: 'Aucune photo sélectionnée'
    });

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    var table = $('.datatable-button-print-rows').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        ajax: {
            url: "/GTRANS/public/Admin/api/person.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_pers"
                });
            }
        },
        columns: [{
            "data": "pic",
            "width": "5%",
            "render": function(data, type, row) {
                switch (row.pic) {
                    case null:
                        return '<img class="img-circle img-md" src="/GTRANS/assets/images/default-img.png" alt="Pic">';

                    default:
                        return '<img class="img-circle img-md" src="/GTRANS/public/users/user_data/' + row.cin + '/' + row.pic + '" alt="Pic">';
                }
            }
        }, {
            "data": "CIN",
            "render": function(data, type, row) {
                return '<a>' + row.cin + '</a>';
            }
        }, {
            "data": "nom"
        }, {
            "data": "tel"
        }, {
            "data": "mail"
        }, {
            "data": "user",
            "width": "5%",
            "render": function(data, type, row) {
                switch (row.user) {
                    case "1":
                        return '<span class="label label-flat label-rounded label-icon border-danger text-danger-600"><i class="icon-display"></i></span>';

                    default:
                        return '<span class="label label-flat label-rounded label-icon border-primary text-primary-600"><i class="icon-accessibility"></i></span>';
                }
            }
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="vuprof" class="btn btn-link text-success"><i class="icon-file-eye2"></i></a>' +
                '<a id="editprof" class="btn btn-link text-primary"><i class="icon-file-plus2"></i></a>' +
                '<a id="delprof" class="btn btn-link text-danger"><i class="icon-file-minus2"></i></a>'
        }]
    });

    $('.datatable-button-print-rows tbody').on('click', 'a#vuprof', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Admin/api/person.php', JSON.stringify(['RES_PASS', data.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
        });
    });
    $('.datatable-button-print-rows tbody').on('click', 'a#editprof', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Admin/api/person.php', JSON.stringify(['ACT_ACC', data.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
    $('.datatable-button-print-rows tbody').on('click', 'a#delprof', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Admin/api/person.php', JSON.stringify(['delprof', data.cin, data.user])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
    $('.datatable-button-print-rows tbody').on('click', 'a#DEL_BTN', function() {
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Admin/api/person.php', JSON.stringify(['DEL_ACC', data.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
});