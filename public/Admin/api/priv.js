$(document).ready(function() {
    // Primary file input
    $(".file-styled-primary").uniform({
        fileButtonClass: 'action btn bg-blue',
        fileButtonHtml: '<i class="icon-cloud-upload"></i>',
        fileDefaultHtml: 'Aucune photo sélectionnée'
    });
    $.extend($.fn.dataTable.defaults, {
        autoWidth: true,
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    var table = $('.datatable-button-print-rows').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        aaSorting: [],
        //select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        ajax: {
            url: "/GTRANS/public/Admin/api/priv.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "get_users"
                });
            }
        },
        columns: [{
                "data": "avatar",
                "render": function(data, type, row) {
                    switch (row.avatar) {
                        case null:
                            return '<img class="img-circle img-md" src="/GTRANS/assets/images/default-img.png" alt="Pic">';

                        default:
                            return '<img class="img-circle img-md" src="/GTRANS/public/users/user_data/' + row.username + '/' + row.avatar + '" alt="Pic">';
                    }
                }
            }, {
                "data": "username",
                "render": function(data, type, row) {
                    return '<a>' + row.username + '</a>';
                }
            }, {
                "data": "name"
            }, {
                "data": "email"
            }, {
                "data": "userlevel",
                "render": function(data, type, row) {
                    switch (row.userlevel) {
                        case 'Administrateur':
                            return '<span class="label label-danger">' + row.userlevel + '</span>';
                        case 'Financier':
                            return '<span class="label label-primary">' + row.userlevel + '</span>';
                        case 'Comptable':
                            return '<span class="label label-success">' + row.userlevel + '</span>';
                        case 'Exploitation':
                            return '<span class="label label-warning">' + row.userlevel + '</span>';
                        default:
                            return '<span class="label label-info">' + row.userlevel + '</span>';
                    }
                }
            }, {
                "data": "activated",
                "render": function(data, type, row) {
                    switch (row.activated) {
                        case '1':
                            return '<i class="icon-circle2 text-success"></i> Activé';

                        default:
                            return '<i class="icon-circle2"></i> Désactivé';
                    }
                }
            },
            {
                "data": null,
                "defaultContent": '<a id="conf_prof" class="btn btn-link text-indigo"><i class="icon-hammer-wrench"></i></a>'
            }
        ]
    });

    var USER_SELECTED = '';

    $('.datatable-button-print-rows tbody').on('click', 'a#conf_prof', function() {
        USER_SELECTED = table.row($(this).parents('tr')).data();
        $('#u_mod_name').html(USER_SELECTED.username);
        $('#u_mod_level').val(USER_SELECTED.userlevel);
        $('#modal_user_change').modal('show');
        /*
         */
    });
    $('#RES_BTN').on('click', function() {
        $.post('/GTRANS/public/Admin/api/priv.php', JSON.stringify(['RES_PASS', USER_SELECTED.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
        });
    });
    $('#ACT_BTN').on('click', function() {
        $.post('/GTRANS/public/Admin/api/priv.php', JSON.stringify(['ACT_ACC', USER_SELECTED.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('#modal_user_change').modal('hide');
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
    $('#DACT_BTN').on('click', function() {
        $.post('/GTRANS/public/Admin/api/priv.php', JSON.stringify(['DACT_ACC', USER_SELECTED.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('#modal_user_change').modal('hide');
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
    $('#DEL_BTN').on('click', function() {
        $.post('/GTRANS/public/Admin/api/priv.php', JSON.stringify(['DEL_ACC', USER_SELECTED.username])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('#modal_user_change').modal('hide');
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });

    $('#u_mod_level').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        $.post('/GTRANS/public/Admin/api/priv.php', JSON.stringify(['CH_PRIV_ACC', USER_SELECTED.username, valueSelected])).done(function(data) {
            data = JSON.parse(data);
            swal({
                title: data.msg,
                confirmButtonColor: "#2196F3"
            });
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        });
    });
});