$(document).ready(function() {
    function UID_BLK(id, cls, msg) {
        id === undefined || id === null ? id = "body" : console.log();
        cls === undefined || cls === null || cls === '' ? cls = "icon-spinner4 spinner" : console.log();
        msg === undefined || msg === null || msg === '' ? msg = "" : msg = '<br><br><span class="text-semibold display-block">' + msg + '</span>';
        $(id).block({
            message: '<i class="' + cls + '"></i>' + msg,
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    }
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });


    var table = $('#table_loc').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/four.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_fourn"
                });
            }
        },
        columns: [{
            "data": "FR_CODE",
            "width": "5%",
        }, {
            "data": "FR_LIBELLE"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('#table_loc tbody').on('click', 'a#remove', function() {
        UID_BLK('#table_loc tbody', 'fa fa-spinner fa-spin');
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['DEL_fourn', data.FR_CODE])).done(function(data) {
            $('#table_loc tbody').unblock();
            $('#table_loc').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('#table_loc tbody').unblock();
            $('#table_loc').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression du Fournisseur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table_loc tbody').on('click', 'a#modif', function() {
        UID_BLK('#table_loc tbody', 'fa fa-spinner fa-spin');
        var data_tab = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_fourn_info', data_tab.FR_CODE])).done(function(data) {
            $('#table_loc tbody').unblock();
            $('#MFR_CODE').val(data[0].FR_CODE)
            $('#MFR_LIBELLE').val(data[0].FR_LIBELLE)
            $('#MFR_ADRESSE').val(data[0].FR_ADRESSE)
            $('#MFR_EMAIL').val(data[0].FR_EMAIL)
            $('#MFR_AUTRE_INFO').val(data[0].FR_AUTRE_INFO)
            $('#MFR_FAX').val(data[0].FR_FAX)
            $('#MFR_RSPONSABLE').val(data[0].FR_RSPONSABLE)
            $('#MFR_TEL_ETABLISS').val(data[0].FR_TEL_ETABLISS)
            $('#MFR_TEL_RESPONSABLE').val(data[0].FR_TEL_RESPONSABLE)
            $('#modal_mod_fourn').modal('show');
        }).fail(function(xhr, status, error) {
            $('#table_loc tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du fournisseur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#gen_key').on('click', function() {
        UID_BLK('#gen_key', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_KEY'])).done(function(data) {
            $('#AFR_CODE').val(data);
            $('#gen_key').unblock();
        }).fail(function(xhr, status, error) {
            $('#gen_key').unblock();
            new PNotify({
                title: 'Erreur lors de la generation d\'un CODE',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_af').on('click', function() {
        if ($('#AFR_LIBELLE').val() && $('#AFR_ADRESSE').val()) {
            UID_BLK('#btn_af', 'fa fa-spinner fa-spin');
            if ($('#AFR_CODE').val()) {
                $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['ADD_fourn_info', {
                    'FR_CODE': $('#AFR_CODE').val(),
                    'FR_LIBELLE': $('#AFR_LIBELLE').val(),
                    'FR_ADRESSE': $('#AFR_ADRESSE').val(),
                    'FR_RSPONSABLE': $('#AFR_RSPONSABLE').val(),
                    'FR_TEL_RESPONSABLE': $('#AFR_TEL_RESPONSABLE').val(),
                    'FR_TEL_ETABLISS': $('#AFR_TEL_ETABLISS').val(),
                    'FR_FAX': $('#AFR_FAX').val(),
                    'FR_EMAIL': $('#AFR_EMAIL').val(),
                    'FR_AUTRE_INFO': $('#AFR_AUTRE_INFO').val(),
                }])).done(function(data) {
                    $('#btn_af').unblock();
                    $('#modal_add_fourn').modal('hide');
                    $('#table_loc').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_af').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du fournisseur',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            } else {
                $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_KEY'])).done(function(data) {
                    $('#AFR_CODE').val(data);
                }).fail(function(xhr, status, error) {
                    $('#gen_key').unblock();
                    new PNotify({
                        title: 'Erreur lors de la generation d\'un CODE',
                        text: error,
                        icon: 'icon-warning22'
                    });
                }).always(function(data) {
                    $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['ADD_fourn_info', {
                        'FR_CODE': $('#AFR_CODE').val(),
                        'FR_LIBELLE': $('#AFR_LIBELLE').val(),
                        'FR_ADRESSE': $('#AFR_ADRESSE').val(),
                        'FR_RSPONSABLE': $('#AFR_RSPONSABLE').val(),
                        'FR_TEL_RESPONSABLE': $('#AFR_TEL_RESPONSABLE').val(),
                        'FR_TEL_ETABLISS': $('#AFR_TEL_ETABLISS').val(),
                        'FR_FAX': $('#AFR_FAX').val(),
                        'FR_EMAIL': $('#AFR_EMAIL').val(),
                        'FR_AUTRE_INFO': $('#AFR_AUTRE_INFO').val(),
                    }])).done(function(data) {
                        $('#btn_af').unblock();
                        $('#modal_add_fourn').modal('hide');
                        $('#table_loc').DataTable().ajax.reload();
                    }).fail(function(xhr, status, error) {
                        $('#btn_ac').unblock();
                        new PNotify({
                            title: 'Erreur lors de l\'enregistrement du fournisseur',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
                });
            }
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom et Adresse deux paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mf').on('click', function() {
        if ($('#MFR_LIBELLE').val() && $('#MFR_ADRESSE').val() && $('#MFR_CODE').val()) {
            UID_BLK('#btn_mf', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['MOD_fourn_info', $('#MFR_CODE').val(), {
                'FR_LIBELLE': $('#MFR_LIBELLE').val(),
                'FR_ADRESSE': $('#MFR_ADRESSE').val(),
                'FR_RSPONSABLE': $('#MFR_RSPONSABLE').val(),
                'FR_TEL_RESPONSABLE': $('#MFR_TEL_RESPONSABLE').val(),
                'FR_TEL_ETABLISS': $('#MFR_TEL_ETABLISS').val(),
                'FR_FAX': $('#MFR_FAX').val(),
                'FR_EMAIL': $('#MFR_EMAIL').val(),
                'FR_AUTRE_INFO': $('#MFR_AUTRE_INFO').val(),
            }])).done(function(data) {
                $('#btn_mf').unblock();
                $('#modal_mod_fourn').modal('hide');
                $('#table_loc').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mf').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du fournisseur',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse et Code Client des paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });


    // Etranger


    var table_etr = $('#table_etr').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/four.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_fourn_etr"
                });
            }
        },
        columns: [{
            "data": "FR_CODE",
            "width": "5%",
        }, {
            "data": "FR_LIBELLE"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('#table_etr tbody').on('click', 'a#remove', function() {
        UID_BLK('#table_etr tbody', 'fa fa-spinner fa-spin');
        var data = table_etr.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['DEL_fourn_etr', data.FR_CODE])).done(function(data) {
            $('#table_etr tbody').unblock();
            $('#table_etr').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('#table_etr tbody').unblock();
            $('#table_etr').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression du Fournisseur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table_etr tbody').on('click', 'a#modif', function() {
        UID_BLK('#table_etr tbody', 'fa fa-spinner fa-spin');
        var data_tab = table_etr.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_fourn_info_etr', data_tab.FR_CODE])).done(function(data) {
            $('#table_etr tbody').unblock();
            $('#MFR_CODE_ETR').val(data[0].FR_CODE)
            $('#MFR_LIBELLE_ETR').val(data[0].FR_LIBELLE)
            $('#MFR_ADRESSE_ETR').val(data[0].FR_ADRESSE)
            $('#MFR_EMAIL_ETR').val(data[0].FR_EMAIL)
            $('#MFR_AUTRE_INFO_ETR').val(data[0].FR_AUTRE_INFO)
            $('#MFR_FAX_ETR').val(data[0].FR_FAX)
            $('#MFR_RSPONSABLE_ETR').val(data[0].FR_RSPONSABLE)
            $('#MFR_TEL_ETABLISS_ETR').val(data[0].FR_TEL_ETABLISS)
            $('#MFR_TEL_RESPONSABLE_ETR').val(data[0].FR_TEL_RESPONSABLE)
            $('#modal_mod_fourn_etr').modal('show');
        }).fail(function(xhr, status, error) {
            $('#table_etr tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du fournisseur',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#gen_key_etr').on('click', function() {
        UID_BLK('#gen_key_etr', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_KEY_ETR'])).done(function(data) {
            $('#AFR_CODE_ETR').val(data);
            $('#gen_key_etr').unblock();
        }).fail(function(xhr, status, error) {
            $('#gen_key_etr').unblock();
            new PNotify({
                title: 'Erreur lors de la generation d\'un CODE',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_af_etr').on('click', function() {
        if ($('#AFR_LIBELLE_ETR').val() && $('#AFR_ADRESSE_ETR').val()) {
            UID_BLK('#btn_af_etr', 'fa fa-spinner fa-spin');
            if ($('#AFR_CODE_ETR').val()) {
                $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['ADD_fourn_info_etr', {
                    'FR_CODE': $('#AFR_CODE_ETR').val(),
                    'FR_LIBELLE': $('#AFR_LIBELLE_ETR').val(),
                    'FR_ADRESSE': $('#AFR_ADRESSE_ETR').val(),
                    'FR_RSPONSABLE': $('#AFR_RSPONSABLE_ETR').val(),
                    'FR_TEL_RESPONSABLE': $('#AFR_TEL_RESPONSABLE_ETR').val(),
                    'FR_TEL_ETABLISS': $('#AFR_TEL_ETABLISS_ETR').val(),
                    'FR_FAX': $('#AFR_FAX_ETR').val(),
                    'FR_EMAIL': $('#AFR_EMAIL_ETR').val(),
                    'FR_AUTRE_INFO': $('#AFR_AUTRE_INFO_ETR').val(),
                }])).done(function(data) {
                    $('#btn_af_etr').unblock();
                    $('#modal_add_fourn_etr').modal('hide');
                    $('#table_etr').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_af_etr').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du fournisseur',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            } else {
                $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['GET_KEY_ETR'])).done(function(data) {
                    $('#AFR_CODE_ETR').val(data);
                }).fail(function(xhr, status, error) {
                    $('#gen_key_etr').unblock();
                    new PNotify({
                        title: 'Erreur lors de la generation d\'un CODE',
                        text: error,
                        icon: 'icon-warning22'
                    });
                }).always(function(data) {
                    $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['ADD_fourn_info_etr', {
                        'FR_CODE': $('#AFR_CODE_ETR').val(),
                        'FR_LIBELLE': $('#AFR_LIBELLE_ETR').val(),
                        'FR_ADRESSE': $('#AFR_ADRESSE_ETR').val(),
                        'FR_RSPONSABLE': $('#AFR_RSPONSABLE_ETR').val(),
                        'FR_TEL_RESPONSABLE': $('#AFR_TEL_RESPONSABLE_ETR').val(),
                        'FR_TEL_ETABLISS': $('#AFR_TEL_ETABLISS_ETR').val(),
                        'FR_FAX': $('#AFR_FAX_ETR').val(),
                        'FR_EMAIL': $('#AFR_EMAIL_ETR').val(),
                        'FR_AUTRE_INFO': $('#AFR_AUTRE_INFO_ETR').val(),
                    }])).done(function(data) {
                        $('#btn_af_etr').unblock();
                        $('#modal_add_fourn_etr').modal('hide');
                        $('#table_etr').DataTable().ajax.reload();
                    }).fail(function(xhr, status, error) {
                        $('#btn_ac_etr').unblock();
                        new PNotify({
                            title: 'Erreur lors de l\'enregistrement du fournisseur',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
                });
            }
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom et Adresse deux paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mf_etr').on('click', function() {
        if ($('#MFR_LIBELLE_ETR').val() && $('#MFR_ADRESSE_ETR').val() && $('#MFR_CODE_ETR').val()) {
            UID_BLK('#btn_mf_etr', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/four.php', JSON.stringify(['MOD_fourn_info_etr', $('#MFR_CODE_ETR').val(), {
                'FR_LIBELLE': $('#MFR_LIBELLE_ETR').val(),
                'FR_ADRESSE': $('#MFR_ADRESSE_ETR').val(),
                'FR_RSPONSABLE': $('#MFR_RSPONSABLE_ETR').val(),
                'FR_TEL_RESPONSABLE': $('#MFR_TEL_RESPONSABLE_ETR').val(),
                'FR_TEL_ETABLISS': $('#MFR_TEL_ETABLISS_ETR').val(),
                'FR_FAX': $('#MFR_FAX_ETR').val(),
                'FR_EMAIL': $('#MFR_EMAIL_ETR').val(),
                'FR_AUTRE_INFO': $('#MFR_AUTRE_INFO_ETR').val(),
            }])).done(function(data) {
                $('#btn_mf_etr').unblock();
                $('#modal_mod_fourn_etr').modal('hide');
                $('#table_etr').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mf_etr').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du fournisseur',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse et Code Client des paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });
});