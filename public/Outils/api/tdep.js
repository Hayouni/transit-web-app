$(document).ready(function() {
    function UID_BLK(id, cls, msg) {
        id === undefined || id === null ? id = "body" : console.log();
        cls === undefined || cls === null || cls === '' ? cls = "icon-spinner4 spinner" : console.log();
        msg === undefined || msg === null || msg === '' ? msg = "" : msg = '<br><br><span class="text-semibold display-block">' + msg + '</span>';
        $(id).block({
            message: '<i class="' + cls + '"></i>' + msg,
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    }
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    var table = $('.datatable-button-print-rows').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/tdep.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_tdep"
                });
            }
        },
        aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
        columns: [{
            "data": "TD_CODE",
            "width": "1%",
        }, {
            "data": "TD_LIBELLE"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('.datatable-button-print-rows tbody').on('click', 'a#remove', function() {
        UID_BLK('.datatable-button-print-rows tbody', 'fa fa-spinner fa-spin');
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/tdep.php', JSON.stringify(['DEL_tdep', data.TD_CODE])).done(function(data) {
            $('.datatable-button-print-rows tbody').unblock();
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('.datatable-button-print-rows tbody').unblock();
            $('.datatable-button-print-rows').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression d\'enregistrement',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('.datatable-button-print-rows tbody').on('click', 'a#modif', function() {
        UID_BLK('.datatable-button-print-rows tbody', 'fa fa-spinner fa-spin');
        var data_tab = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/tdep.php', JSON.stringify(['GET_tdep_info', data_tab.TD_CODE])).done(function(data) {
            $('.datatable-button-print-rows tbody').unblock();
            $('#MTD_CODE').val(data[0].TD_CODE)
            $('#MTD_LIBELLE').val(data[0].TD_LIBELLE)
            $('#modal_mod_tdep').modal('show');
        }).fail(function(xhr, status, error) {
            $('.datatable-button-print-rows tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du tdep',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_acf').on('click', function() {
        if ($('#ATD_LIBELLE').val()) {
            UID_BLK('#btn_acf', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/tdep.php', JSON.stringify(['GET_KEY'])).done(function(data1) {
                $.post('/GTRANS/public/Outils/api/tdep.php', JSON.stringify(['ADD_tdep_info', {
                    'TD_CODE': data1,
                    'TD_LIBELLE': $('#ATD_LIBELLE').val()
                }])).done(function(data) {
                    $('#btn_acf').unblock();
                    $('#modal_add_tdep').modal('hide');
                    $('.datatable-button-print-rows').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_ac').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du type facture',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            }).fail(function(xhr, status, error) {
                new PNotify({
                    title: 'Erreur lors de la generation d\'un ID',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'TD_CODE et TD_LIBELLER deux paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mcf').on('click', function() {
        if ($('#MTD_LIBELLE').val() && $('#MTD_CODE').val()) {
            UID_BLK('#btn_mcf', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/tdep.php', JSON.stringify(['MOD_tdep_info', $('#MTD_CODE').val(), {
                'TD_LIBELLE': $('#MTD_LIBELLE').val()
            }])).done(function(data) {
                $('#btn_mcf').unblock();
                $('#modal_mod_tdep').modal('hide');
                $('.datatable-button-print-rows').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mcf').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du tdep',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom est un paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });
});