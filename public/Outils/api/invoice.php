<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {
    case 'GET_all_c_facture':
        $db = new MySQL();
        $data["data"] = $db->get_results("SELECT * FROM trans.cont_invoices");
        echo json_encode($data);
        break;

    case 'GET_c_facture_info':
        $db = new MySQL();
        echo json_encode($db->get_results("SELECT * FROM trans.cont_invoices WHERE CODE = '$response[1]'"));
        break;

    case 'MOD_c_facture_info':
        $db = new MySQL();
        $where= array('CODE'=>$response[1]);
        echo $db->update( 'trans.cont_invoices',$response[2],$where );
        break;
    
    case 'ADD_c_facture_info':
        $db = new MySQL();
        echo $db->insert( 'trans.cont_invoices',$response[1]);
        break;

    case 'DEL_c_facture':
        $db = new MySQL();
        $where= array('CODE'=>$response[1]);
        echo $db->delete( 'trans.cont_invoices',$where );
        break;

    case 'GET_KEY':
        $db = new MySQL();
        echo $db->get_results("SELECT MAX(CODE) + 1 AS CLE FROM trans.cont_invoices")[0]['CLE'];
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>