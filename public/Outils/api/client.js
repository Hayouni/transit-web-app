$(document).ready(function() {
    function UID_BLK(id, cls, msg) {
        id === undefined || id === null ? id = "body" : console.log();
        cls === undefined || cls === null || cls === '' ? cls = "icon-spinner4 spinner" : console.log();
        msg === undefined || msg === null || msg === '' ? msg = "" : msg = '<br><br><span class="text-semibold display-block">' + msg + '</span>';
        $(id).block({
            message: '<i class="' + cls + '"></i>' + msg,
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    }
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });


    var table = $('#table_cl').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/client.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_client"
                });
            }
        },
        columns: [{
            "data": "CL_CODE",
            "width": "5%",
        }, {
            "data": "CL_LIBELLE"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('#table_cl tbody').on('click', 'a#remove', function() {
        UID_BLK('#table_cl tbody', 'fa fa-spinner fa-spin');
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['DEL_client', data.CL_CODE])).done(function(data) {
            $('#table_cl tbody').unblock();
            $('#table_cl').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('#table_cl tbody').unblock();
            $('#table_cl').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression du client',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table_cl tbody').on('click', 'a#modif', function() {
        UID_BLK('#table_cl tbody', 'fa fa-spinner fa-spin');
        var data_tab = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_client_info', data_tab.CL_CODE])).done(function(data) {
            $('#table_cl tbody').unblock();
            $('#MCL_CODE').val(data[0].CL_CODE)
            $('#MCL_LIBELLE').val(data[0].CL_LIBELLE)
            $('#MCL_ADRESSE').val(data[0].CL_ADRESSE)
            $('#MCL_EMAIL').val(data[0].CL_EMAIL)
            $('#MCL_AUTRE_INFO').val(data[0].CL_AUTRE_INFO)
            $('#MCL_FAX').val(data[0].CL_FAX)
            $('#MCL_RSPONSABLE').val(data[0].CL_RSPONSABLE)
            $('#MCL_TEL_ETABLISS').val(data[0].CL_TEL_ETABLISS)
            $('#MCL_TEL_RESPONSABLE').val(data[0].CL_TEL_RESPONSABLE)
            $('#modal_mod_client').modal('show');
        }).fail(function(xhr, status, error) {
            $('#table_cl tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du client',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#gen_key').on('click', function() {
        UID_BLK('#gen_key', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_KEY'])).done(function(data) {
            $('#ACL_CODE').val(data);
            $('#gen_key').unblock();
        }).fail(function(xhr, status, error) {
            $('#gen_key').unblock();
            new PNotify({
                title: 'Erreur lors de la generation d\'un CODE',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_ac').on('click', function() {
        if ($('#ACL_LIBELLE').val() && $('#ACL_ADRESSE').val()) {
            UID_BLK('#btn_ac', 'fa fa-spinner fa-spin');
            if ($('#ACL_CODE').val()) {
                $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['ADD_client_info', {
                    'CL_CODE': $('#ACL_CODE').val(),
                    'CL_LIBELLE': $('#ACL_LIBELLE').val(),
                    'CL_ADRESSE': $('#ACL_ADRESSE').val(),
                    'CL_RSPONSABLE': $('#ACL_RSPONSABLE').val(),
                    'CL_TEL_RESPONSABLE': $('#ACL_TEL_RESPONSABLE').val(),
                    'CL_TEL_ETABLISS': $('#ACL_TEL_ETABLISS').val(),
                    'CL_FAX': $('#ACL_FAX').val(),
                    'CL_EMAIL': $('#ACL_EMAIL').val(),
                    'CL_AUTRE_INFO': $('#ACL_AUTRE_INFO').val(),
                }])).done(function(data) {
                    $('#btn_ac').unblock();
                    $('#modal_add_client').modal('hide');
                    $('#table_cl').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_ac').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du client',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            } else {
                $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_KEY'])).done(function(data) {
                    $('#ACL_CODE').val(data);
                }).fail(function(xhr, status, error) {
                    $('#gen_key').unblock();
                    new PNotify({
                        title: 'Erreur lors de la generation d\'un CODE',
                        text: error,
                        icon: 'icon-warning22'
                    });
                }).always(function(data) {
                    $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['ADD_client_info', {
                        'CL_CODE': $('#ACL_CODE').val(),
                        'CL_LIBELLE': $('#ACL_LIBELLE').val(),
                        'CL_ADRESSE': $('#ACL_ADRESSE').val(),
                        'CL_RSPONSABLE': $('#ACL_RSPONSABLE').val(),
                        'CL_TEL_RESPONSABLE': $('#ACL_TEL_RESPONSABLE').val(),
                        'CL_TEL_ETABLISS': $('#ACL_TEL_ETABLISS').val(),
                        'CL_FAX': $('#ACL_FAX').val(),
                        'CL_EMAIL': $('#ACL_EMAIL').val(),
                        'CL_AUTRE_INFO': $('#ACL_AUTRE_INFO').val(),
                    }])).done(function(data) {
                        $('#btn_ac').unblock();
                        $('#modal_add_client').modal('hide');
                        $('#table_cl').DataTable().ajax.reload();
                    }).fail(function(xhr, status, error) {
                        $('#btn_ac').unblock();
                        new PNotify({
                            title: 'Erreur lors de l\'enregistrement du client',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
                });
            }
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse deux paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mc').on('click', function() {
        if ($('#MCL_LIBELLE').val() && $('#MCL_ADRESSE').val() && $('#MCL_CODE').val()) {
            UID_BLK('#btn_mc', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['MOD_client_info', $('#MCL_CODE').val(), {
                'CL_LIBELLE': $('#MCL_LIBELLE').val(),
                'CL_ADRESSE': $('#MCL_ADRESSE').val(),
                'CL_RSPONSABLE': $('#MCL_RSPONSABLE').val(),
                'CL_TEL_RESPONSABLE': $('#MCL_TEL_RESPONSABLE').val(),
                'CL_TEL_ETABLISS': $('#MCL_TEL_ETABLISS').val(),
                'CL_FAX': $('#MCL_FAX').val(),
                'CL_EMAIL': $('#MCL_EMAIL').val(),
                'CL_AUTRE_INFO': $('#MCL_AUTRE_INFO').val(),
            }])).done(function(data) {
                $('#btn_mc').unblock();
                $('#modal_mod_client').modal('hide');
                $('#table_cl').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mc').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du client',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse et Code Client des paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });


    //////////////// Client Etranger


    var table_etr = $('#table_etr').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/client.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_client_etr"
                });
            }
        },
        columns: [{
            "data": "CLE_CODE",
            "width": "5%",
        }, {
            "data": "CLE_LIBELLE"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('#table_etr tbody').on('click', 'a#remove', function() {
        UID_BLK('#table_etr tbody', 'fa fa-spinner fa-spin');
        var data = table_etr.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['DEL_client_etr', data.CLE_CODE])).done(function(data) {
            $('#table_etr tbody').unblock();
            $('#table_etr').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('#table_etr tbody').unblock();
            $('#table_etr').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression du client',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#table_etr tbody').on('click', 'a#modif', function() {
        UID_BLK('#table_etr tbody', 'fa fa-spinner fa-spin');
        var data_tab = table_etr.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_client_info_etr', data_tab.CLE_CODE])).done(function(data) {
            $('#table_etr tbody').unblock();
            $('#MCL_CODE_ETR').val(data[0].CLE_CODE)
            $('#MCL_LIBELLE_ETR').val(data[0].CLE_LIBELLE)
            $('#MCL_ADRESSE_ETR').val(data[0].CLE_ADRESSE)
            $('#MCL_EMAIL_ETR').val(data[0].CLE_EMAIL)
            $('#MCL_AUTRE_INFO_ETR').val(data[0].CLE_AUTRE_INFO)
            $('#MCL_FAX_ETR').val(data[0].CLE_FAX)
            $('#MCL_RSPONSABLE_ETR').val(data[0].CLE_RSPONSABLE)
            $('#MCL_TEL_ETABLISS_ETR').val(data[0].CLE_TEL_ETABLISS)
            $('#MCL_TEL_RESPONSABLE_ETR').val(data[0].CLE_TEL_RESPONSABLE)
            $('#modal_mod_client_etr').modal('show');
        }).fail(function(xhr, status, error) {
            $('#table_etr tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du client',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#gen_key_etr').on('click', function() {
        UID_BLK('#gen_key_etr', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_KEY_ETR'])).done(function(data) {
            $('#ACL_CODE_ETR').val(data);
            $('#gen_key_etr').unblock();
        }).fail(function(xhr, status, error) {
            $('#gen_key_etr').unblock();
            new PNotify({
                title: 'Erreur lors de la generation d\'un CODE',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_ac_etr').on('click', function() {
        if ($('#ACL_LIBELLE_ETR').val() && $('#ACL_ADRESSE_ETR').val()) {
            UID_BLK('#btn_ac_etr', 'fa fa-spinner fa-spin');
            if ($('#ACL_CODE_ETR').val()) {
                $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['ADD_client_info_etr', {
                    'CLE_CODE': $('#ACL_CODE_ETR').val(),
                    'CLE_LIBELLE': $('#ACL_LIBELLE_ETR').val(),
                    'CLE_ADRESSE': $('#ACL_ADRESSE_ETR').val(),
                    'CLE_RSPONSABLE': $('#ACL_RSPONSABLE_ETR').val(),
                    'CLE_TEL_RESPONSABLE': $('#ACL_TEL_RESPONSABLE_ETR').val(),
                    'CLE_TEL_ETABLISS': $('#ACL_TEL_ETABLISS_ETR').val(),
                    'CLE_FAX': $('#ACL_FAX_ETR').val(),
                    'CLE_EMAIL': $('#ACL_EMAIL_ETR').val(),
                    'CLE_AUTRE_INFO': $('#ACL_AUTRE_INFO_ETR').val(),
                }])).done(function(data) {
                    $('#btn_ac_etr').unblock();
                    $('#modal_add_client_etr').modal('hide');
                    $('#table_etr').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_ac_etr').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du client',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            } else {
                $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['GET_KEY_ETR'])).done(function(data) {
                    $('#ACL_CODE_ETR').val(data);
                }).fail(function(xhr, status, error) {
                    $('#gen_key_etr').unblock();
                    new PNotify({
                        title: 'Erreur lors de la generation d\'un CODE',
                        text: error,
                        icon: 'icon-warning22'
                    });
                }).always(function(data) {
                    $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['ADD_client_info_etr', {
                        'CLE_CODE': $('#ACL_CODE_ETR').val(),
                        'CLE_LIBELLE': $('#ACL_LIBELLE_ETR').val(),
                        'CLE_ADRESSE': $('#ACL_ADRESSE_ETR').val(),
                        'CLE_RSPONSABLE': $('#ACL_RSPONSABLE_ETR').val(),
                        'CLE_TEL_RESPONSABLE': $('#ACL_TEL_RESPONSABLE_ETR').val(),
                        'CLE_TEL_ETABLISS': $('#ACL_TEL_ETABLISS_ETR').val(),
                        'CLE_FAX': $('#ACL_FAX_ETR').val(),
                        'CLE_EMAIL': $('#ACL_EMAIL_ETR').val(),
                        'CLE_AUTRE_INFO': $('#ACL_AUTRE_INFO_ETR').val(),
                    }])).done(function(data) {
                        $('#btn_ac_etr').unblock();
                        $('#modal_add_client_etr').modal('hide');
                        $('#table_etr').DataTable().ajax.reload();
                    }).fail(function(xhr, status, error) {
                        $('#btn_ac_etr').unblock();
                        new PNotify({
                            title: 'Erreur lors de l\'enregistrement du client',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
                });
            }
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse deux paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mc_etr').on('click', function() {
        if ($('#MCL_LIBELLE_ETR').val() && $('#MCL_ADRESSE_ETR').val() && $('#MCL_CODE_ETR').val()) {
            UID_BLK('#btn_mc_etr', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/client.php', JSON.stringify(['MOD_client_info_etr', $('#MCL_CODE_ETR').val(), {
                'CLE_LIBELLE': $('#MCL_LIBELLE_ETR').val(),
                'CLE_ADRESSE': $('#MCL_ADRESSE_ETR').val(),
                'CLE_RSPONSABLE': $('#MCL_RSPONSABLE_ETR').val(),
                'CLE_TEL_RESPONSABLE': $('#MCL_TEL_RESPONSABLE_ETR').val(),
                'CLE_TEL_ETABLISS': $('#MCL_TEL_ETABLISS_ETR').val(),
                'CLE_FAX': $('#MCL_FAX_ETR').val(),
                'CLE_EMAIL': $('#MCL_EMAIL_ETR').val(),
                'CLE_AUTRE_INFO': $('#MCL_AUTRE_INFO_ETR').val(),
            }])).done(function(data) {
                $('#btn_mc_etr').unblock();
                $('#modal_mod_client_etr').modal('hide');
                $('#table_etr').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mc_etr').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du client',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom du client et Adresse et Code Client des paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });
});