<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');

$db = new MySQL();

$CL_COUNT = $db->get_results("SELECT COUNT(*) AS CL_COUNT FROM trans.client");
//SELECT COUNT(*) AS CL_COUNT FROM trans.client
$FR_COUNT = $db->get_results("SELECT COUNT(*) AS FR_COUNT FROM trans.fournisseur");
//SELECT COUNT(*) AS FR_COUNT FROM trans.fournisseur
$PO_COUNT = $db->get_results("SELECT COUNT(*) AS PO_COUNT FROM trans.port");
//SELECT COUNT(*) AS PO_COUNT FROM trans.port
$AE_COUNT = $db->get_results("SELECT COUNT(*) AS AE_COUNT FROM trans.aeroport");
//SELECT COUNT(*) AS AE_COUNT FROM trans.aeroport
$NA_COUNT = $db->get_results("SELECT COUNT(*) AS NA_COUNT FROM trans.navire");
//SELECT COUNT(*) AS NA_COUNT FROM trans.navire

function retco($var = null, $L = null){
    if (count($var)>0){
        return $var[0][$L];
    }else {
        return "0";
    }
}
echo json_encode(array('CL_COUNT' => retco($CL_COUNT,"CL_COUNT"),
                        'FR_COUNT' => retco($FR_COUNT,"FR_COUNT"),
                        'PO_COUNT' => retco($PO_COUNT,"PO_COUNT"),
                        'AE_COUNT' => retco($AE_COUNT,"AE_COUNT"),
                        'NA_COUNT' => retco($NA_COUNT,"NA_COUNT")
                        ));
