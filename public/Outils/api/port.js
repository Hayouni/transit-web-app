$(document).ready(function() {
    function UID_BLK(id, cls, msg) {
        id === undefined || id === null ? id = "body" : console.log();
        cls === undefined || cls === null || cls === '' ? cls = "icon-spinner4 spinner" : console.log();
        msg === undefined || msg === null || msg === '' ? msg = "" : msg = '<br><br><span class="text-semibold display-block">' + msg + '</span>';
        $(id).block({
            message: '<i class="' + cls + '"></i>' + msg,
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    }
    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    var table = $('.datatable-button-print-rows').DataTable({
        buttons: {
            buttons: [{
                    extend: 'print',
                    className: 'btn bg-blue btn-icon',
                    text: '<i class="icon-printer position-left"></i>',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/port.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_port"
                });
            }
        },
        //aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
        columns: [{
            "data": "PO_CODE",
            "width": "1%",
        }, {
            "data": "PO_LIBELLE"
        }, {
            "data": "PO_LAT"
        }, {
            "data": "PO_LNG"
        }, {
            "data": "PO_ISO2"
        }, {
            "data": "PO_ISO3"
        }, {
            "data": null,
            "width": "10%",
            "defaultContent": '<a id="modif" class="btn border-primary text-primary-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-pencil"></i></a>&nbsp;&nbsp<a id="remove" class="btn border-danger text-danger-600 btn-flat btn-icon btn-rounded btn-xs"><i class="icon-trash text-danger"></i></a>'
        }]
    });

    $('.datatable-button-print-rows tbody').on('click', 'a#remove', function() {
        UID_BLK('.datatable-button-print-rows tbody', 'fa fa-spinner fa-spin');
        var data = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['DEL_port', data.PO_CODE])).done(function(data) {
            $('.datatable-button-print-rows tbody').unblock();
            $('.datatable-button-print-rows').DataTable().ajax.reload();
        }).fail(function(xhr, status, error) {
            $('.datatable-button-print-rows tbody').unblock();
            $('.datatable-button-print-rows').DataTable().ajax.reload();
            new PNotify({
                title: 'Erreur lors de la supression du port',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('.datatable-button-print-rows tbody').on('click', 'a#modif', function() {
        UID_BLK('.datatable-button-print-rows tbody', 'fa fa-spinner fa-spin');
        var data_tab = table.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['GET_port_info', data_tab.PO_CODE])).done(function(data) {
            $('.datatable-button-print-rows tbody').unblock();
            $('#MPO_CODE').val(data[0].PO_CODE)
            $('#MPO_LIBELLE').val(data[0].PO_LIBELLE)
            $('#MPO_LAT').val(data[0].PO_LAT)
            $('#MPO_LNG').val(data[0].PO_LNG)
            $('#MPO_ISO2').val(data[0].PO_ISO2)
            $('#MPO_ISO3').val(data[0].PO_ISO3)
            $('#modal_mod_port').modal('show');
        }).fail(function(xhr, status, error) {
            $('.datatable-button-print-rows tbody').unblock();
            new PNotify({
                title: 'Erreur lors de chargement des données du port',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#gen_key').on('click', function() {
        UID_BLK('#gen_key', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['GET_KEY'])).done(function(data) {
            $('#APO_CODE').val(data);
            $('#gen_key').unblock();
        }).fail(function(xhr, status, error) {
            $('#gen_key').unblock();
            new PNotify({
                title: 'Erreur lors de la generation d\'un CODE',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_ap').on('click', function() {
        if ($('#APO_LIBELLE').val()) {
            UID_BLK('#btn_ap', 'fa fa-spinner fa-spin');
            if ($('#APO_CODE').val()) {
                $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['ADD_port_info', {
                    'PO_CODE': $('#APO_CODE').val(),
                    'PO_LIBELLE': $('#APO_LIBELLE').val(),
                    'PO_LAT': $('#APO_LAT').val(),
                    'PO_LNG': $('#APO_LNG').val(),
                    'PO_ISO2': $('#APO_ISO2').val(),
                    'PO_ISO3': $('#APO_ISO3').val()
                }])).done(function(data) {
                    $('#btn_ap').unblock();
                    $('#modal_add_port').modal('hide');
                    $('.datatable-button-print-rows').DataTable().ajax.reload();
                }).fail(function(xhr, status, error) {
                    $('#btn_ap').unblock();
                    new PNotify({
                        title: 'Erreur lors de l\'enregistrement du port',
                        text: error,
                        icon: 'icon-warning22'
                    });
                });
            } else {
                $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['GET_KEY'])).done(function(data) {
                    $('#APO_CODE').val(data);
                }).fail(function(xhr, status, error) {
                    $('#gen_key').unblock();
                    new PNotify({
                        title: 'Erreur lors de la generation d\'un CODE',
                        text: error,
                        icon: 'icon-warning22'
                    });
                }).always(function(data) {
                    $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['ADD_port_info', {
                        'PO_CODE': $('#APO_CODE').val(),
                        'PO_LIBELLE': $('#APO_LIBELLE').val(),
                        'PO_LAT': $('#APO_LAT').val(),
                        'PO_LNG': $('#APO_LNG').val(),
                        'PO_ISO2': $('#APO_ISO2').val(),
                        'PO_ISO3': $('#APO_ISO3').val()
                    }])).done(function(data) {
                        $('#btn_ap').unblock();
                        $('#modal_add_port').modal('hide');
                        $('.datatable-button-print-rows').DataTable().ajax.reload();
                    }).fail(function(xhr, status, error) {
                        $('#btn_ap').unblock();
                        new PNotify({
                            title: 'Erreur lors de l\'enregistrement du port',
                            text: error,
                            icon: 'icon-warning22'
                        });
                    });
                });
            }
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Libeller est un paramètre requi',
                addclass: 'bg-danger'
            });
        }
    });

    $('#btn_mp').on('click', function() {
        if ($('#MPO_LIBELLE').val() && $('#MPO_CODE').val()) {
            UID_BLK('#btn_mp', 'fa fa-spinner fa-spin');
            $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['MOD_port_info', $('#MPO_CODE').val(), {
                'PO_LIBELLE': $('#MPO_LIBELLE').val(),
                'PO_LAT': $('#MPO_LAT').val(),
                'PO_LNG': $('#MPO_LNG').val(),
                'PO_ISO2': $('#MPO_ISO2').val(),
                'PO_ISO3': $('#MPO_ISO3').val()
            }])).done(function(data) {
                $('#btn_mp').unblock();
                $('#modal_mod_port').modal('hide');
                $('.datatable-button-print-rows').DataTable().ajax.reload();
            }).fail(function(xhr, status, error) {
                $('#btn_mf').unblock();
                new PNotify({
                    title: 'Erreur lors de l\'enregistrement du port',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        } else {
            new PNotify({
                title: '<i class="fa fa-quote-right"> erreur</i>',
                text: 'Nom et Code de port des paramètres requis',
                addclass: 'bg-danger'
            });
        }
    });

    var TAB_PAYS = $('#TAB_PAYS').DataTable({
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        select: true,
        destroy: true,
        scrollY: false,
        scrollX: false,
        pageLength: 5,
        lengthMenu: [
            [5, 10, 25, 50, -1],
            [5, 10, 25, 50, "All"]
        ],
        ajax: {
            url: "/GTRANS/public/Outils/api/port.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_all_pays"
                });
            }
        },
        columns: [{
            "data": "ISO2",
            "width": "10%",
        }, {
            "data": "ISO3",
            "width": "10%",
        }, {
            "data": "N_FR"
        }, {
            "data": "N_EN"
        }]
    });
});

$(function() {

    var map;

    // Map settings
    function initialize() {

        // Optinos
        var mapOptions = {
            zoom: 5,
            center: new google.maps.LatLng(36.806987, 10.167085)
        };

        // Apply options
        map = new google.maps.Map($('.map-container')[0], mapOptions);

        google.maps.event.addListener(map, "rightclick", function(event) {
            //console.log(event)
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();
            // populate yor box/field with lat, lng
            //alert("Lat=" + lat + "; Lng=" + lng);
            $('#IPO_LAT').val(lat);
            $('#IPO_LNG').val(lng);
            $.post('/GTRANS/public/Outils/api/port.php', JSON.stringify(['GET_ADRES', lat, lng])).done(function(data) {
                $('#IPO_ADRES').val(data);
            }).fail(function(xhr, status, error) {
                new PNotify({
                    title: 'Erreur lors de chargement d\'adresse',
                    text: error,
                    icon: 'icon-warning22'
                });
            });
        });
    }
    // Load map
    google.maps.event.addDomListener(window, 'load', initialize);


});