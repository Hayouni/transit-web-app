$(document).ready(function() {
    function UID_BLK(id, cls, msg) {
        id === undefined || id === null ? id = "body" : console.log();
        cls === undefined || cls === null || cls === '' ? cls = "icon-spinner4 spinner" : console.log();
        msg === undefined || msg === null || msg === '' ? msg = "" : msg = '<br><br><span class="text-semibold display-block">' + msg + '</span>';
        $(id).block({
            message: '<i class="' + cls + '"></i>' + msg,
            overlayCSS: {
                backgroundColor: '#1b2024',
                opacity: 0.8,
                zIndex: 1200,
                cursor: 'wait'
            },
            css: {
                border: 0,
                color: '#fff',
                padding: 0,
                zIndex: 1201,
                backgroundColor: 'transparent'
            }
        });
    }
    $('#btn_svinfo').on('click', function() {
        UID_BLK('#btn_svinfo', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['SET_PERS_INFO', {
            'ADDRESS': $("#ADDRESS").val(),
            'Country': $('#Country').val(),
            'City': $('#City').val(),
            'TEL': $('#TEL').val(),
            'FAX': $('#FAX').val(),
            'WEB': $('#WEB').val(),
            'MAIL': $('#MAIL').val()
        }])).done(function(data) {
            $('#btn_svinfo').unblock();
        }).fail(function(xhr, status, error) {
            $('#btn_svinfo').unblock();
            new PNotify({
                title: 'Erreur lors de creation sauvegard',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_svfisc').on('click', function() {
        UID_BLK('#btn_svfisc', 'fa fa-spinner fa-spin');
        $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['SET_FISC', {
            'TVA': $("#TVA").val(),
            'CCB': $('#CCB').val(),
            'RC': $('#RC').val()
        }, {
            'VALEUR': $("#taxtva").val()
        }, {
            'VALEUR': $("#timb").val()
        }])).done(function(data) {
            $('#btn_svfisc').unblock();
        }).fail(function(xhr, status, error) {
            $('#btn_svfisc').unblock();
            new PNotify({
                title: 'Erreur lors de sauvegard',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    $('#btn_svcoldos').on('click', function() {
        $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['CHNG_COLOR',
            $('#MCI').val().slice(1), $('#MCE').val().slice(1),
            $('#MGI').val().slice(1), $('#MGE').val().slice(1),
            $('#AI').val().slice(1), $('#AE').val().slice(1)
        ])).done(function(data) {
            console.log(data)
        }).fail(function(xhr, status, error) {
            new PNotify({
                title: 'Erreur lors de sauvegard',
                text: error,
                icon: 'icon-warning22'
            });
        });
    });

    /** volume conteneur */
    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['volum_container'])).done(function(data) {
        $('#LIST_VOLUM').empty();
        data.forEach(function(element) {
            $('#LIST_VOLUM').append('<option value="' + element.CV_LBELLE + '"></option>');
        }, this);
    });

    $("#type_Con").on('input', function() {
        var val = this.value;
        if ($('#LIST_VOLUM').find('option').filter(function() {
                return this.value.toUpperCase() === val.toUpperCase();
            }).length) {
            //send ajax request
            var vvv = this.value;
            if (~this.value.indexOf("'")) {
                vvv += "'";
            }
            $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['GET_VOLVAL_CONT', vvv]))
                .done(function(data) {
                    $('#Con_Val').val(data[0].CV_VALUE);
                })
        }
    });

    $('#btn_svdvol').on('click', function() {
        if ($('#type_Con').val()) {
            if ($('#Con_Val').val()) {
                var found = false;
                $('#LIST_VOLUM option').each(function() {
                    if (~$(this).val().indexOf($('#type_Con').val())) {
                        found = true;
                        return;
                    }
                });
                if (found) {
                    $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['MOD_VOL_CONT', $('#type_Con').val(), { 'CV_VALUE': $('#Con_Val').val() }]))
                        .done(function(data) {
                            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['volum_container']))
                                .done(function(data) {
                                    $('#LIST_VOLUM').empty();
                                    data.forEach(function(element) {
                                        $('#LIST_VOLUM').append('<option value="' + element.CV_LBELLE + '"></option>');
                                    }, this);
                                    $('#type_Con').val('');
                                    $('#Con_Val').val('');
                                    new PNotify({
                                        title: 'Modifier avec succès',
                                        type: 'success',
                                        addclass: 'alert alert-success alert-styled-left'
                                    });
                                });
                        });
                } else {
                    $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['ADD_VOL_CONT', { 'CV_LBELLE': $('#type_Con').val(), 'CV_VALUE': $('#Con_Val').val() }]))
                        .done(function(data) {
                            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['volum_container'])).done(function(data) {
                                $('#LIST_VOLUM').empty();
                                data.forEach(function(element) {
                                    $('#LIST_VOLUM').append('<option value="' + element.CV_LBELLE + '"></option>');
                                }, this);
                                $('#type_Con').val('');
                                $('#Con_Val').val('');
                                new PNotify({
                                    title: 'Ajouter avec succès',
                                    type: 'success',
                                    addclass: 'alert alert-success alert-styled-left'
                                });
                            });
                        });
                }
            } else {
                new PNotify({
                    title: 'Valeur en Dinar requi !!',
                    type: 'warning',
                    addclass: 'alert alert-warning alert-styled-left'
                });
            }
        }
    });

    $("#type_Compt").on('input', function() {
        var val = this.value;
        if ($('#LIST_COMPT').find('option').filter(function() {
                return this.value.toUpperCase() === val.toUpperCase();
            }).length) {
            //send ajax request
            var vvv = this.value;
            $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['GET_VAL_COMPT', vvv]))
                .done(function(data) {
                    $('#Compt_Val').val(data[0].CLE);
                })
        }
    });

    $('#btn_svcompt').on('click', function() {
        if ($('#type_Compt').val()) {
            if ($('#Compt_Val').val()) {
                var found = false;
                $('#LIST_COMPT option').each(function() {
                    if (~$(this).val().indexOf($('#type_Compt').val())) {
                        found = true;
                        return;
                    }
                });
                if (found) {
                    $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['MOD_VAL_COMPT', $('#type_Compt').val(), { 'NUMERO': $('#Compt_Val').val() }]))
                        .done(function(data) {
                            $('#type_Compt').val('');
                            $('#Compt_Val').val('');
                            new PNotify({
                                title: 'Modifier avec succès',
                                type: 'success',
                                addclass: 'alert alert-success alert-styled-left'
                            });
                        });
                } else {
                    new PNotify({
                        title: 'Type compteur non defini ou modifier manuellement !!',
                        type: 'warning',
                        addclass: 'alert alert-warning alert-styled-left'
                    });
                }
            } else {
                new PNotify({
                    title: 'Valeur compteur requi !!',
                    type: 'warning',
                    addclass: 'alert alert-warning alert-styled-left'
                });
            }
        }
    });

    $('#btn_correction').on('click', function() {
        swal({
                title: "valider mot de passe",
                text: "Correction des valeur TVA dans BD",
                type: "input",
                showCancelButton: true,
                confirmButtonColor: "#2196F3",
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "..."
            },
            function(inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("require !!");
                    return false;
                }
                if (inputValue === 'voiture') {
                    $.post('/GTRANS/public/Outils/api/prm.php', JSON.stringify(['CORRECTION']))
                        .done(function(data) {
                            swal({
                                title: "Nice!",
                                text: "Operation réussie",
                                type: "success",
                                confirmButtonColor: "#2196F3"
                            });
                        })
                        .fail(function(error) {
                            swal({
                                title: "Error",
                                text: "Operation erroné ",
                                type: "warning",
                                confirmButtonColor: "#2196F3"
                            });
                        });
                } else {
                    swal({
                        title: "Error",
                        text: "MDP incorrect: " + inputValue,
                        type: "warning",
                        confirmButtonColor: "#2196F3"
                    });
                }
            });
    });
});