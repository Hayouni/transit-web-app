<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {
    case 'SET_PERS_INFO':
        $db = new MySQL();
        $where= array('id'=>'MGOD');
        echo $db->update( 'trans.conf_tab',$response[1],$where );
        break;

    case 'SET_FISC':
        $db = new MySQL();
        $where= array('id'=>'MGOD');
        echo $db->update( 'trans.conf_tab',$response[1],$where );
        $where= array('CODE'=>'3'); //TVA
        echo $db->update( 'trans.table_pourcentage',$response[2],$where );
        $where= array('CODE'=>'4'); //TIMBRE
        echo $db->update( 'trans.table_pourcentage',$response[3],$where );
        break;

    case 'CHNG_COLOR':
        include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/class.iniparser/class.iniparser.php');
        $cfg = new iniParser("../../../sys/config/init.ini");
        echo $cfg->setValue("folder_color","EXPORT", "$response[6]");
        echo $cfg->setValue("folder_color","IMPORT", "$response[5]");
        echo $cfg->setValue("folder_color","CEXPORT", "$response[2]");
        echo $cfg->setValue("folder_color","CIMPORT", "$response[1]");
        echo $cfg->setValue("folder_color","GEXPORT", "$response[4]");
        echo $cfg->setValue("folder_color","GIMPORT", "$response[3]");
        echo $cfg->save("../../../sys/config/init.ini");
        break;

    case 'MOD_VOL_CONT':
        $db = new MySQL();
        $where= array('CV_LBELLE'=>$response[1]);
        echo $db->update( 'trans.container_volume',$response[2],$where );
        break;
    
    case 'ADD_VOL_CONT':
        $db = new MySQL();
        echo $db->insert( 'trans.container_volume',$response[1]);
        break;

    case 'GET_VOLVAL_CONT':
        $db = new MySQL();
        echo json_encode($db->get_results("SELECT CV_VALUE FROM trans.container_volume WHERE CV_LBELLE = '$response[1]'"));
        break;

    case 'GET_VAL_COMPT':
        $db = new MySQL();
        switch ($response[1]) {
            case 'Facture':
                echo json_encode($db->get_results("SELECT MAX(NUMERO) AS CLE FROM compteur_facture"));
                break;
            case 'Avis':
                echo json_encode($db->get_results("SELECT MAX(NUMERO) AS CLE FROM compteur_avis_arrive"));
                break;
            case 'Dossier':
                echo json_encode($db->get_results("SELECT MAX(NUMERO) AS CLE FROM compteur_dossier"));
                break;
            
            default:
                echo 'error0';
                break;
        }
        break;

    case 'MOD_VAL_COMPT':
        $db = new MySQL();
        $tab = '';
        switch ($response[1]) {
            case 'Facture':
                $tab = "compteur_facture";
                break;
            case 'Avis':
                $tab = "compteur_avis_arrive";
                break;
            case 'Dossier':
                $tab = "compteur_dossier";
                break;
            
            default:
                echo 'error0';
                break;
        }
        $db->query("TRUNCATE ".$tab);
        echo $db->insert( $tab, $response[2]);
        break;

    case 'CORRECTION':
        $db = new MySQL();
        echo $db->query( "UPDATE avis_arrive_mig SET AAM_TVA_VAL = 18 WHERE YEAR(AAM_DATE) < '2018'" );
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>