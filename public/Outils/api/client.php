<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
switch ($response[0]) {
    case 'GET_all_client':
        $db = new MySQL();
        $data["data"] = $db->get_results("SELECT * FROM trans.client");
        echo json_encode($data);
        break;

    case 'GET_client_info':
        $db = new MySQL();
        echo json_encode($db->get_results("SELECT * FROM trans.client WHERE CL_CODE = '$response[1]'"));
        break;

    case 'MOD_client_info':
        $db = new MySQL();
        $where= array('CL_CODE'=>$response[1]);
        echo $db->update( 'trans.client',$response[2],$where );
        break;
    
    case 'ADD_client_info':
        $db = new MySQL();
        echo $db->insert( 'trans.client',$response[1]);
        break;

    case 'DEL_client':
        $db = new MySQL();
        $where= array('CL_CODE'=>$response[1]);
        echo $db->delete( 'trans.client',$where );
        break;

    case 'GET_KEY':
        $db = new MySQL();
        echo $db->get_results("SELECT MAX(CL_CODE) + 1 AS CLE FROM trans.client")[0]['CLE'];
        break;

    /// ETRANGER

    case 'GET_all_client_etr':
        $db = new MySQL();
        $data["data"] = $db->get_results("SELECT * FROM trans.client_etranger");
        echo json_encode($data);
        break;

    case 'GET_client_info_etr':
        $db = new MySQL();
        echo json_encode($db->get_results("SELECT * FROM trans.client_etranger WHERE CLE_CODE = '$response[1]'"));
        break;

    case 'MOD_client_info_etr':
        $db = new MySQL();
        $where= array('CLE_CODE'=>$response[1]);
        echo $db->update( 'trans.client_etranger',$response[2],$where );
        break;
    
    case 'ADD_client_info_etr':
        $db = new MySQL();
        echo $db->insert( 'trans.client_etranger',$response[1]);
        break;

    case 'DEL_client_etr':
        $db = new MySQL();
        $where= array('CLE_CODE'=>$response[1]);
        echo $db->delete( 'trans.client_etranger',$where );
        break;

    case 'GET_KEY_ETR':
        $db = new MySQL();
        echo $db->get_results("SELECT MAX(CLE_CODE) + 1 AS CLE FROM trans.client_etranger")[0]['CLE'];
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>