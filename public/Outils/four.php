<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur","Financier","Exploitation"])) {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|Fournisseurs</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->

    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/prism.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/four.js"></script>
    <!-- /theme JS files -->

    <style>
        .sidebar-default .navigation>li.active>a,
        .sidebar-default .navigation>li.active>a:focus,
        .sidebar-default .navigation>li.active>a:hover {
            background-color: rgba(38, 166, 154, 0.72);
            /*#37474f;*/
            color: #fff;
        }
    </style>
    <script>
        $.post('api/counter.php').done(function(data) {
            data = JSON.parse(data);
            $('#CL_COUNT').html(data.CL_COUNT);
            $('#FR_COUNT').html(data.FR_COUNT);
            $('#PO_COUNT').html(data.PO_COUNT);
            $('#AE_COUNT').html(data.AE_COUNT);
            $('#NA_COUNT').html(data.NA_COUNT);
        });
    </script>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Secondary sidebar -->
            <div class="sidebar sidebar-secondary sidebar-default">
                <div class="sidebar-content">
                    <!-- Sub Codifications -->
                    <div class="category-title">
                        <span>Codifications</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li><a href="./"><i class="icon-user"></i> Clients <span id="CL_COUNT" class="badge badge-primary">86</span></a></li>
                                <li class="active"><a href="./four"><i class="icon-user-tie"></i> Fournisseurs <span id="FR_COUNT" class="badge badge-primary">120</span></a></li>
                                <li><a href="./port"><i class="fa fa-life-saver"></i> Ports <span id="PO_COUNT" class="badge badge-primary">71</span></a></li>
                                <li><a href="./aeroport"><i class="fa fa-plane"></i> Aéroports <span id="AE_COUNT" class="badge badge-primary">63</span></a></li>
                                <li><a href="./navir"><i class="fa fa-ship"></i> Navires <span id="NA_COUNT" class="badge badge-primary">56</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Codifications -->
                </div>
                <div class="sidebar-content">
                    <!-- Sub Facture -->
                    <div class="category-title category-collapsed">
                        <span>Facture</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category" style="display: none;">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li><a href="./tfact"><i class="fa fa-chevron-circle-right"></i> Type Facture</a></li>
                                <li><a href="./tdep"><i class="fa fa-chevron-circle-right"></i> Type Dépense</a></li>
                                <li><a href="./timbr"><i class="fa fa-chevron-circle-right"></i> Timbrage</a></li>
                                <li><a href="./magazinage"><i class="fa fa-chevron-circle-right"></i> Magasinage</a></li>
                                <li><a href="./surist"><i class="fa fa-chevron-circle-right"></i> Surestarie</a></li>
                                <li><a href="./invoice"><i class="fa fa-chevron-circle-right"></i> Invoice</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Facture -->
                </div>
                <?php if ($_SESSION['LEVEL'] == "Administrateur") { ?>
                <div class="sidebar-content">
                    <!-- Sub Facture -->
                    <div class="category-title">
                        <span>Autre</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li><a href="./prm"><i class="icon-cog4"></i> Parametrage</a></li>
                                <li><a href="./bcp"><i class="icon-database"></i> Base de données + MAJ</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Facture -->
                </div>
                <?php } else{}?>
            </div>

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Outils</span> - Fournisseurs
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">LISTE DES FOURNISSERUS</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_add" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_fourn"><i class="fa fa-plus-circle"></i> Ajouter un Fournisseur</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <table class="table datatable-button-print-rows" id="table_loc">
                                        <thead>
                                            <tr>
                                                <th>CODE</th>
                                                <th>Nom</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Large modal -->
                            <div id="modal_add_fourn" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title"><i class=" icon-user-plus"></i> Nouveaux Fournisseur</h5>
                                        </div>

                                        <div class="modal-body">
                                            <form class="form-horizontal" id="frm_fr">
                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <label>CODE <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-btn"><button class="btn btn-danger" id="gen_key" type="button"><i class="fa fa-key"></i></button></span>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="AFR_CODE" id="AFR_CODE" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>NOM-LIBELLER <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_LIBELLE" id="AFR_LIBELLE" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_RSPONSABLE" id="AFR_RSPONSABLE" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>E-MAIL</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_EMAIL" id="AFR_EMAIL" value="">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_TEL_ETABLISS" id="AFR_TEL_ETABLISS" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_TEL_RESPONSABLE" id="AFR_TEL_RESPONSABLE" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>FAX</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_FAX" id="AFR_FAX" value="">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label>ADRESSE <span class="text-danger">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="AFR_ADRESSE" id="AFR_ADRESSE"></textarea>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>AUTRE INFORMATION</label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="AFR_AUTRE_INFO" id="AFR_AUTRE_INFO"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                            <button type="button" class="btn btn-success" id="btn_af">Ajouter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /large modal -->

                            <!-- Large modal -->
                            <div id="modal_mod_fourn" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header bg-brown">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title"><i class="icon-user-check"></i> Modifier Fournisseur</h5>
                                        </div>

                                        <div class="modal-body">
                                            <form class="form-horizontal" id="frm_m_fr">
                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <label>CODE <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_CODE" id="MFR_CODE" disabled>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>NOM-LIBELLER <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_LIBELLE" id="MFR_LIBELLE" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_RSPONSABLE" id="MFR_RSPONSABLE" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>E-MAIL</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_EMAIL" id="MFR_EMAIL" value="">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_TEL_ETABLISS" id="MFR_TEL_ETABLISS" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_TEL_RESPONSABLE" id="MFR_TEL_RESPONSABLE" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>FAX</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_FAX" id="MFR_FAX" value="">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label>ADRESSE <span class="text-danger">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="MFR_ADRESSE" id="MFR_ADRESSE"></textarea>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>AUTRE INFORMATION</label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="MFR_AUTRE_INFO" id="MFR_AUTRE_INFO"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                            <button type="button" class="btn bg-brown" id="btn_mf">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /large modal -->
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">LISTE DES FOURNISSERUS ETRANGERS</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_add_etr" class="btn btn-primary" data-toggle="modal" data-target="#modal_add_fourn_etr"><i class="fa fa-plus-circle"></i> Ajouter un Fournisseur</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <table class="table datatable-button-print-rows" id="table_etr">
                                        <thead>
                                            <tr>
                                                <th>CODE</th>
                                                <th>Nom</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Large modal -->
                            <div id="modal_add_fourn_etr" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header bg-success">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title"><i class=" icon-user-plus"></i> Nouveaux Fournisseur Etranger</h5>
                                        </div>

                                        <div class="modal-body">
                                            <form class="form-horizontal" id="frm_fr_etr">
                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <label>CODE <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <span class="input-group-btn"><button class="btn btn-danger" id="gen_key_etr" type="button"><i class="fa fa-key"></i></button></span>
                                                            <input type="text" class="form-control border-black border-lg text-black" name="AFR_CODE_ETR" id="AFR_CODE_ETR" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>NOM-LIBELLER <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_LIBELLE_ETR" id="AFR_LIBELLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_RSPONSABLE_ETR" id="AFR_RSPONSABLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>E-MAIL</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_EMAIL_ETR" id="AFR_EMAIL_ETR" value="">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_TEL_ETABLISS_ETR" id="AFR_TEL_ETABLISS_ETR" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_TEL_RESPONSABLE_ETR" id="AFR_TEL_RESPONSABLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>FAX</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="AFR_FAX_ETR" id="AFR_FAX_ETR" value="">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label>ADRESSE <span class="text-danger">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="AFR_ADRESSE_ETR" id="AFR_ADRESSE_ETR"></textarea>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>AUTRE INFORMATION</label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="AFR_AUTRE_INFO_ETR" id="AFR_AUTRE_INFO_ETR"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                            <button type="button" class="btn btn-success" id="btn_af_etr">Ajouter</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /large modal -->

                            <!-- Large modal -->
                            <div id="modal_mod_fourn_etr" class="modal fade">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header bg-brown">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title"><i class="icon-user-check"></i> Modifier Fournisseur Etranger</h5>
                                        </div>

                                        <div class="modal-body">
                                            <form class="form-horizontal" id="frm_m_fr_etr">
                                                <div class="row form-group">
                                                    <div class="col-md-3">
                                                        <label>CODE <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_CODE_ETR" id="MFR_CODE_ETR" disabled>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>NOM-LIBELLER <span class="text-danger">*</span></label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_LIBELLE_ETR" id="MFR_LIBELLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_RSPONSABLE_ETR" id="MFR_RSPONSABLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label>E-MAIL</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_EMAIL_ETR" id="MFR_EMAIL_ETR" value="">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row form-group">
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_TEL_ETABLISS_ETR" id="MFR_TEL_ETABLISS_ETR" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>TELEPHONE RESPONSABLE</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_TEL_RESPONSABLE_ETR" id="MFR_TEL_RESPONSABLE_ETR" value="">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>FAX</label>
                                                        <input type="text" class="form-control border-black border-lg text-black" name="MFR_FAX_ETR" id="MFR_FAX_ETR" value="">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-6">
                                                        <label>ADRESSE <span class="text-danger">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="MFR_ADRESSE_ETR" id="MFR_ADRESSE_ETR"></textarea>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>AUTRE INFORMATION</label>
                                                        <textarea rows="5" cols="5" class="form-control border-black border-lg text-black" placeholder="" name="MFR_AUTRE_INFO_ETR" id="MFR_AUTRE_INFO_ETR"></textarea>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal">Annuler</button>
                                            <button type="button" class="btn bg-brown" id="btn_mf_etr">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /large modal -->
                        </div>

                    </div>
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>