<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur"])) {
    header("location: /GTRANS");
	exit();
}
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
$db = new MySQL();
$pers_info = $db->get_results("SELECT * FROM trans.conf_tab WHERE id ='MGOD'");
$tva = $db->get_results("SELECT VALEUR FROM trans.table_pourcentage WHERE CODE = '3'");
$tim = $db->get_results("SELECT VALEUR FROM trans.table_pourcentage WHERE CODE = '4'");
?>
<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/class.iniparser/class.iniparser.php');
$cfg = new iniParser("../../sys/config/init.ini");
$EXPORT = "#".$cfg->get("folder_color","EXPORT");
$IMPORT = "#".$cfg->get("folder_color","IMPORT");
$CEXPORT = "#".$cfg->get("folder_color","CEXPORT");
$CIMPORT = "#".$cfg->get("folder_color","CIMPORT");
$GEXPORT = "#".$cfg->get("folder_color","GEXPORT");
$GIMPORT = "#".$cfg->get("folder_color","GIMPORT");
$UNKNOWN = "#".$cfg->get("folder_color","UNKNOWN");
$MAPCOL = "#".$cfg->get("MAP","MAPCOL");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|Parametrage</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->

    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/prism.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/prm.js"></script>
    <!-- /theme JS files -->

    <style>
        .sidebar-default .navigation>li.active>a,
        .sidebar-default .navigation>li.active>a:focus,
        .sidebar-default .navigation>li.active>a:hover {
            background-color: rgba(38, 166, 154, 0.72);
            /*#37474f;*/
            color: #fff;
        }
    </style>
    <script>
        $.post('api/counter.php').done(function(data) {
            data = JSON.parse(data);
            $('#CL_COUNT').html(data.CL_COUNT);
            $('#FR_COUNT').html(data.FR_COUNT);
            $('#PO_COUNT').html(data.PO_COUNT);
            $('#AE_COUNT').html(data.AE_COUNT);
            $('#NA_COUNT').html(data.NA_COUNT);
        });
    </script>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Secondary sidebar -->
            <div class="sidebar sidebar-secondary sidebar-default">
                <div class="sidebar-content">
                    <!-- Sub Codifications -->
                    <div class="category-title">
                        <span>Codifications</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li><a href="./"><i class="icon-user"></i> Clients <span id="CL_COUNT" class="badge badge-primary">86</span></a></li>
                                <li><a href="./four"><i class="icon-user-tie"></i> Fournisseurs <span id="FR_COUNT" class="badge badge-primary">120</span></a></li>
                                <li><a href="./port"><i class="fa fa-life-saver"></i> Ports <span id="PO_COUNT" class="badge badge-primary">71</span></a></li>
                                <li><a href="./aeroport"><i class="fa fa-plane"></i> Aéroports <span id="AE_COUNT" class="badge badge-primary">63</span></a></li>
                                <li><a href="./navir"><i class="fa fa-ship"></i> Navires <span id="NA_COUNT" class="badge badge-primary">56</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Codifications -->
                </div>
                <div class="sidebar-content">
                    <!-- Sub Facture -->
                    <div class="category-title category-collapsed">
                        <span>Facture</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category" style="display: none;">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li><a href="./tfact"><i class="fa fa-chevron-circle-right"></i> Type Facture</a></li>
                                <li><a href="./tdep"><i class="fa fa-chevron-circle-right"></i> Type Dépense</a></li>
                                <li><a href="./timbr"><i class="fa fa-chevron-circle-right"></i> Timbrage</a></li>
                                <li><a href="./magazinage"><i class="fa fa-chevron-circle-right"></i> Magasinage</a></li>
                                <li><a href="./surist"><i class="fa fa-chevron-circle-right"></i> Surestarie</a></li>
                                <li><a href="./invoice"><i class="fa fa-chevron-circle-right"></i> Invoice</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Facture -->
                </div>
                <div class="sidebar-content">
                    <!-- Sub Facture -->
                    <div class="category-title">
                        <span>Autre</span>
                        <ul class="icons-list">
                            <li>
                                <a href="#" data-action="collapse"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar-category">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-alt navigation-accordion">
                                <li class="active"><a href="./prm"><i class="icon-cog4"></i> Parametrage</a></li>
                                <li><a href="./bcp"><i class="icon-database"></i> Base de données + MAJ</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /sub Facture -->
                </div>
            </div>

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Outils</span> - Parametrage
                            </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">INFOS PERSONNELLES</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_correction" class="btn btn-danger"> CORRECTION</button>
                                            <button type="button" id="btn_svinfo" class="btn btn-primary"><i class="fa fa-check-circle"></i> Sauvegarder</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-8">
                                            <label>Adresse</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="ADDRESS" id="ADDRESS" value="<?php echo $pers_info[0]['ADDRESS']; ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Pays</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="Country" id="Country" value="<?php echo $pers_info[0]['Country']; ?>">
                                        </div>
                                        <div class="col-md-2">
                                            <label>Région</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="City" id="City" value="<?php echo $pers_info[0]['City']; ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-3">
                                            <label>Téléphone</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="TEL" id="TEL" value="<?php echo $pers_info[0]['TEL']; ?>">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Fax</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="FAX" id="FAX" value="<?php echo $pers_info[0]['FAX']; ?>">
                                        </div>
                                        <div class="col-md-3">
                                            <label>Site Web</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="WEB" id="WEB" value="<?php echo $pers_info[0]['WEB']; ?>">
                                        </div>
                                        <div class="col-md-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="MAIL" id="MAIL" value="<?php echo $pers_info[0]['MAIL']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">PARAMÈTRES FISCAUX</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_svfisc" class="btn btn-primary"><i class="fa fa-check-circle"></i> Sauvegarder</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label>TVA</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="TVA" id="TVA" value="<?php echo $pers_info[0]['TVA']; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>CCB</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="CCB" id="CCB" value="<?php echo $pers_info[0]['CCB']; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>RC</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="RC" id="RC" value="<?php echo $pers_info[0]['RC']; ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>TAXE TVA (%)</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="taxtva" id="taxtva" value="<?php echo $tva[0]['VALEUR']; ?>">
                                        </div>
                                        <div class="col-md-6">
                                            <label>TAXE TIMBRE</label>
                                            <input type="text" class="form-control border-black border-lg text-black" name="timb" id="timb" value="<?php echo $tim[0]['VALEUR']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">VOLUME COTENEUR</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_svdvol" class="btn btn-primary"><i class="fa fa-check-circle"></i> Sauvegarder</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>Volume</label>
                                            <input type="text" list="LIST_VOLUM" class="form-control border-black border-lg text-black" name="type_Con" id="type_Con" value="">
                                            <datalist id="LIST_VOLUM">
                                                
                                            </datalist>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Valuer Dinars</label>
                                            <input type="number" class="form-control border-black border-lg text-black" name="Con_Val" id="Con_Val" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">Réinitialisation des Compteurs</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_svcompt" class="btn btn-primary"><i class="fa fa-check-circle"></i> Sauvegarder</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label>Choix de compteur</label>
                                            <input type="text" list="LIST_COMPT" class="form-control border-black border-lg text-black" name="type_Compt" id="type_Compt" value="">
                                            <datalist id="LIST_COMPT">
                                                <option value="Facture"></option>
                                                <option value="Avis"></option>
                                                <option value="Dossier"></option>
                                            </datalist>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Valuer</label>
                                            <input type="number" class="form-control border-black border-lg text-black" name="Compt_Val" id="Compt_Val" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default border-grey">
                                <div class="panel-heading">
                                    <h6 class="panel-title text-muted text-bold">PERSONALISER COULEURS DES DOSSIER</h6>
                                    <div class="heading-elements">
                                        <div class="heading-btn">
                                            <button type="button" id="btn_svcoldos" class="btn btn-primary"><i class="fa fa-check-circle"></i> Sauvegarder</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-body">
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label>MARITIME COMPLET IMPORT</label>
                                            <input type="color" class="form-control" name="MCI" id="MCI" value="<?php echo $CIMPORT; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>MARITIME COMPLET EXPORT</label>
                                            <input type="color" class="form-control" name="MCE" id="MCE" value="<?php echo $CEXPORT; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>AERIEN IMPORT</label>
                                            <input type="color" class="form-control" name="AI" id="AI" value="<?php echo $IMPORT; ?>">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-4">
                                            <label>MARITIME GROUPAGE IMPORT</label>
                                            <input type="color" class="form-control" name="MGI" id="MGI" value="<?php echo $GIMPORT; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>MARITIME GROUPAGE EXPORT</label>
                                            <input type="color" class="form-control" name="MGE" id="MGE" value="<?php echo $GEXPORT; ?>">
                                        </div>
                                        <div class="col-md-4">
                                            <label>AERIEN EXPORT</label>
                                            <input type="color" class="form-control" name="AE" id="AE" value="<?php echo $EXPORT; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>