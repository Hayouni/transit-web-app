<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>


<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|MENU</title>

    <!-- Global stylesheets -->
    <link href="../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../assets/js/plugins/maps/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/ui/livestamp.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/notifications/pnotify.min.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/maps/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
    <script type="text/javascript" src="../assets/js/plugins/maps/jvectormap/map_files/world.js"></script>

    <script type="text/javascript" src="../assets/js/plugins/visualization/canvasjs/canvasjs.js"></script>

    <script type="text/javascript" src="../assets/js/core/app.js"></script>
    <script type="text/javascript" src="./Dossier/api/tdb.js"></script>
    <!-- /theme JS files -->

    <!-- Chart code -->

    <style>
        .jvectormap-legend {
            background: rgba(132, 132, 132, 0.28);
            color: #1e3043;
            border-radius: 0%;
        }
        
        .jvectormap-legend-title {
            font-weight: bold;
            font-size: 14px;
            text-align: center;
            border-bottom-width: 1px;
            border-bottom-style: dashed;
            padding-bottom: 5px;
        }
        
        .jvectormap-legend-inner {
            padding-top: 5px;
            text-align: -webkit-center;
        }
        
        .jvectormap-legend-cnt-h {
            bottom: -10px;
            right: 0;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick {
            width: auto !important;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend .jvectormap-legend-tick {
            float: left;
            border-right-style: dashed;
            border-right-width: 1px;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick-sample {
            height: 18px !important;
            width: 18px !important;
            border-radius: 100% !important;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick-text {
            text-align: center;
            padding: 0px 6px;
        }
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
    </style>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Tableau de bord</h4>
                        </div>
                        <div class="heading-elements">
                            <div class="btn-group heading-btn">
                                <button class="btn bg-slate" id="nex_refresh_btn">...</button>
                                <button class="btn bg-slate dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right bg-slate" id="set_ref_options" style="min-width: 138px !important;">
                                    <li><a opt="30"><i class="icon-history text-danger-300 position-left"></i> 30 sec</a></li>
                                    <li><a opt="60"><i class="icon-history text-danger position-left"></i> 60 sec</a></li>
                                    <li><a opt="90"><i class="icon-history text-danger-800 position-left"></i> 90 sec</a></li>
                                    <li class="divider"></li>
                                    <li><a opt="start"><i class="icon-alarm-check text-success position-left"></i> DÉMARRER</a></li>
                                    <li><a opt="stop"><i class="icon-alarm-cancel position-left"></i> ARRÊTER</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->
                    <div class="row">
                        <!-- Traffic sources -->
                        <div class="panel panel-flat" style="background-color: #f5f5f5;">
                            <div class="panel-heading" style="background-color: #f5f5f5;">
                                <h6 class="panel-title"><i class="fa fa-map"></i> Dossier Actives</h6>
                            </div>

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="map-container map-choropleth"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- DOS MAP modal -->
                            <div id="MAP_MODAL" class="modal fade">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header bg-blue-300" style="padding: 0px 55px;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h6 id="qSZ" class="text-bold">les données de carte</h6>
                                        </div>

                                        <div class="modal-body">
                                            <div id="hkjk" style="overflow-y:scroll; max-height:300px; "></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /large modal -->
                        </div>

                        <!--div class="row">
                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body panel-body-accent">
                                    <div class="media no-margin">
                                        <div class="media-left media-middle">
                                            <i id="SP_CO_EXP" class="fa fa-plane fa-4x"></i>
                                        </div>

                                        <div class="media-body text-right">
                                            <h3 id="SP_CO_EXP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">A.EXPORT</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body">
                                    <div class="media no-margin">
                                        <div class="media-left media-middle">
                                            <i id="SP_CO_IMP" class="fa fa-plane fa-4x"></i>
                                        </div>

                                        <div class="media-body text-right">
                                            <h3 id="SP_CO_IMP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">A.IMPORT</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body">
                                    <div class="media no-margin">
                                        <div class="media-body">
                                            <h3 id="SP_CO_CEXP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">M.C.EXPORT</span>
                                        </div>

                                        <div class="media-right media-middle">
                                            <i id="SP_CO_CEXP" class="fa fa-ship fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body">
                                    <div class="media no-margin">
                                        <div class="media-body">
                                            <h3 id="SP_CO_CIMP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">M.C.IMPORT</span>
                                        </div>

                                        <div class="media-right media-middle">
                                            <i id="SP_CO_CIMP" class="fa fa-ship fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body">
                                    <div class="media no-margin">
                                        <div class="media-body">
                                            <h3 id="SP_CO_GEXP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">M.G.EXPORT</span>
                                        </div>

                                        <div class="media-right media-middle">
                                            <i id="SP_CO_GEXP" class="fa fa-ship fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6 col-md-3 col-lg-2">
                                <div class="panel panel-body">
                                    <div class="media no-margin">
                                        <div class="media-body">
                                            <h3 id="SP_CO_GIMP_H" class="no-margin text-semibold">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted">M.G.IMPORT</span>
                                        </div>

                                        <div class="media-right media-middle">
                                            <i id="SP_CO_GIMP" class="fa fa-ship fa-4x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div-->

                        <div class="row">
                            <div class="col-lg-8">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <span id="SP_CO_TOT" class="label border-left-success label-striped"></span>
                                        <div class="heading-elements">
                                            <button type="button" class="btn btn-danger btn-xs heading-btn" id="SP_BTN_CO_TOT_UNK"></button>
                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <table class="table datatable-button-print-rows" style="width:100%" id="TAB_DACTIV">
                                            <thead>
                                                <tr>
                                                    <th class="hide_me">N°</th>
                                                    <th>TYPE</th>
                                                    <th>N° Dos</th>
                                                    <th>Navire</th>
                                                    <th>Info</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="panel panel-body panel-body-accent">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_EXP" class="text-bold">Aérien</h3>
                                                </div>

                                                <div class="media-body text-right">
                                                    <h3 id="SP_CO_EXP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">EXPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="panel panel-body">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_IMP" class="text-bold">Aérien</h3>
                                                </div>

                                                <div class="media-body text-right">
                                                    <h3 id="SP_CO_IMP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">IMPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="panel panel-body">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_CEXP" class="text-bold">Maritime</h3>
                                                </div>
                                                <div class="media-body">
                                                    <h3 id="SP_CO_CEXP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">C.EXPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="panel panel-body">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_CIMP" class="text-bold">Maritime</h3>
                                                </div>
                                                <div class="media-body">
                                                    <h3 id="SP_CO_CIMP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">C.IMPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="panel panel-body">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_GEXP" class="text-bold">Maritime</h3>
                                                </div>
                                                <div class="media-body">
                                                    <h3 id="SP_CO_GEXP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">G.EXPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="panel panel-body">
                                            <div class="media no-margin">
                                                <div class="media-left media-middle">
                                                    <h3 id="SP_CO_GIMP" class="text-bold">Maritime</h3>
                                                </div>
                                                <div class="media-body">
                                                    <h3 id="SP_CO_GIMP_H" class="no-margin text-semibold">0</h3>
                                                    <span class="text-uppercase text-size-mini text-muted">G.IMPORT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- List of latest updates -->
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h6 class="panel-title"><i class="icon-folder-plus2 position-left"></i> Evénements</h6>

                                        <div class="heading-elements">
                                            <span class="label label-default heading-text" id="last_add_older_ego"></span>
                                            <ul class="icons-list">
                                                <li><a id="load_last_dos_btn"><i class="icon-history text-slate position-right"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <ul class="media-list" id="last_add_older"> </ul>
                                    </div>
                                </div>
                                <!-- /list of latest updates -->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-flat">
                                    <div class="container-fluid">
                                        <div id="Dep_chart" style="height: 370px;"></div>
                                    </div>
                                </div>
                            </div>                                     
                        </div>

                    </div>
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
                <!-- /content area -->

            </div>
        </div>
    </div>
    <!-- /Page container -->
    <script>
        $(document).ready(function() {

        });
    </script>


</body>

</html>

</html>