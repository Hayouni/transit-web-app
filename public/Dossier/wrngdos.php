<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<!DOCTYPE html>


<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|MENU</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/maps/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/livestamp.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="./api/wrngdos.js"></script>
    <!-- /theme JS files -->

    <!-- Chart code -->

    <style>
        .jvectormap-legend {
            background: rgba(132, 132, 132, 0.28);
            color: #1e3043;
            border-radius: 0%;
        }
        
        .jvectormap-legend-title {
            font-weight: bold;
            font-size: 14px;
            text-align: center;
            border-bottom-width: 1px;
            border-bottom-style: dashed;
            padding-bottom: 5px;
        }
        
        .jvectormap-legend-inner {
            padding-top: 5px;
            text-align: -webkit-center;
        }
        
        .jvectormap-legend-cnt-h {
            bottom: -10px;
            right: 0;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick {
            width: auto !important;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend .jvectormap-legend-tick {
            float: left;
            border-right-style: dashed;
            border-right-width: 1px;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick-sample {
            height: 18px !important;
            width: 18px !important;
            border-radius: 100% !important;
        }
        
        .jvectormap-legend-cnt-h .jvectormap-legend-tick-text {
            text-align: center;
            padding: 0px 6px;
        }
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
    </style>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->

    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Tableau de bord - <b>Dossiers avec des données erronées</b></h4>
                        </div>
                    </div>
                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">

                    <!-- Main charts -->
                    <div class="row">
                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <span id="SP_CO_TOT_UNK" class="label border-left-danger label-striped" data-popup="tooltip" title="" data-placement="bottom" data-original-title="Dossier actif avec les données manquantes"></span>
                                        <div class="heading-elements">
                                            <span class="icon-question6 text-warning-400 heading-text"> Ne correspond pas</span>
                                            <span class="badge badge-flat border-danger text-danger-600 heading-text">code d'élément</span>
                                            <span class="icon-cross2 text-danger-400 heading-text"> N'existe pas</span>
                                            <span class="icon-checkmark3 text-success-400 heading-text"> Trouvé</span>

                                        </div>
                                    </div>

                                    <div class="container-fluid">
                                        <table class="table datatable-button-print-rows" style="width:100%" id="TAB_DACTIV_UNK">
                                            <thead>
                                                <tr>
                                                    <th class="hide_me">N°</th>
                                                    <th>TYPE</th>
                                                    <th>N° Dos</th>
                                                    <th>D.Décharge</th>
                                                    <th>Navire</th>
                                                    <th>POD</th>
                                                    <th>POL</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
                <!-- /content area -->

            </div>
        </div>
    </div>


</body>

</html>

</html>