$(document).ready(function() {
    moment.locale('fr');
    var startDate, endDate = '';

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }

    // Initialize with options
    $('.daterange-ranges').daterangepicker({
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            //dateLimit: { days: 365 },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Les 7 derniers jours': [moment().subtract(6, 'days'), moment()],
                'Les 30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            locale: {
                applyLabel: 'OK',
                cancelLabel: 'Annuler',
                fromLabel: 'Entre',
                toLabel: 'et',
                customRangeLabel: 'Période personnalisée',
                format: 'DD/MM/YYYY'
            }
        },
        function(start, end) {
            $('.daterange-ranges span').html(start.format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + end.format('D MMMM, YYYY'));
            startDate = start.format('YYYY-MM-DD hh:mm:ss');
            endDate = end.format('YYYY-MM-DD hh:mm:ss');
        }
    );

    // Display date format
    $('.daterange-ranges span').html(moment().subtract(29, 'days').format('D MMMM, YYYY') + ' &nbsp; <i class="icon-arrow-right14"></i> &nbsp; ' + moment().format('D MMMM, YYYY'));

    startDate = moment().subtract(29, 'days').format('YYYY-MM-DD hh:mm:ss');
    endDate = moment().format('YYYY-MM-DD hh:mm:ss');

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        //dom: '<"toolbar">fBrtip',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    // MARITIME

    var table_mar = $('#table_mar').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#get_tab_btn_mar').on('click', function() {
        table_mar = $('#table_mar').DataTable({
            aaSorting: [],
            bStateSave: true,
            bDestroy: true,
            select: false,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Dossier/api/dossier.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "SEARCH_DOS_MAR",
                        "1": startDate,
                        "2": endDate
                    });
                },
            },
            columns: [{
                    "data": "DM_NUM_DOSSIER",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                    }
                },
                { "data": "DM_NUM_BL" },
                { "data": "DM_LIB_COMP_GROUP" },
                { "data": "DM_IMP_EXP" },
                { "data": "FR_LIBELLE" },
                { "data": "CL_LIBELLE" },
                { "data": "NA_LIBELLE" },
                { "data": "DM_POIDS" },
                { "data": "DM_NOMBRE" },
                { "data": "DM_MARQUE" },
                { "data": "DM_TERME" },
                { "data": "DM_ESCALE" }
            ],
            initComplete: function(settings, json) {
                this.api().columns().every(function() {
                    var column = this;
                    var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                });

                /*function format(d) {
                    // `d` is the original data object for the row
                    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">' +
                        '<tr>' +
                        '<td>Poids:</td>' +
                        '<td>' + d.DM_POIDS + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Nombre:</td>' +
                        '<td>' + d.DM_NOMBRE + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Marque:</td>' +
                        '<td>' + d.DM_MARQUE + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Terme:</td>' +
                        '<td>' + d.DM_TERME + '</td>' +
                        '</tr>' +
                        '</table>';
                }


                $('#table_mar tbody').on('click', 'td.details-control', function() {
                    var tr = $(this).closest('tr');
                    var row = table_mar.row(tr);

                    if (row.child.isShown()) {
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        // Open this row
                        row.child(format(row.data())).show();
                        tr.addClass('shown');
                    }
                });*/
            }
        }).columns.adjust().draw();
    });

    // CONTENEUR

    var table_cont = $('#table_cont').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#btn_get_tab_cont').on('click', function() {
        table_cont = $('#table_cont').DataTable({
            aaSorting: [],
            bStateSave: true,
            bDestroy: true,
            select: false,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Dossier/api/dossier.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "GET_TAB_CONT",
                        "1": $('#num_cont').val()
                    });
                },
            },
            columns: [{
                    "data": "DM_NUM_DOSSIER",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                    }
                },
                { "data": "DM_MARCHANDISE" },
                { "data": "DC_NUM_CONTAINER" },
                { "data": "DC_CONTAINER" },
                { "data": "DC_POID" },
                { "data": "DC_RUBRIQUE" }
            ],
            initComplete: function(settings, json) {
                this.api().columns().every(function() {
                    var column = this;
                    var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                });
            }
        }).columns.adjust().draw();
    });

    // AERIEN

    var table_aer = $('#table_aer').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });


    $('#get_tab_btn_aer').on('click', function() {
        table_aer = $('#table_aer').DataTable({
            aaSorting: [],
            buttons: [{
                //text: '<i class="icon-printer"></i>',
                extend: 'print',
                //footer: true,
                className: 'btn btn-default border-primary text-primary',
                text: '<i class="icon-printer position-left"></i>',
                title: 'Liste des Dossier',
                message: $('.daterange-ranges span').text(),
                exportOptions: {
                    columns: ':visible'
                },
                customize: function(win) {
                    // Style the body..
                    $(win.document.body)
                        .addClass('asset-print-body')
                        .css({ 'text-align': 'center' });

                    /* Style for the table */
                    $(win.document.body)
                        .find('table')
                        .addClass('compact minimalistBlack')
                        .css({
                            margin: '5px 5px auto'
                        });
                },
            }, {
                extend: 'collection',
                text: '<i class="icon-three-bars"></i>',
                buttons: [{
                    extend: 'columnsToggle',
                    columns: ':not([data-visible="false"])'
                }],
                className: 'btn btn-default border-primary text-primary'
            }],
            bStateSave: true,
            bDestroy: true,
            select: false,
            destroy: true,
            ajax: {
                url: "/GTRANS/public/Dossier/api/dossier.php",
                type: "POST",
                data: function(d) {
                    return JSON.stringify({
                        "0": "SEARCH_DOS_AER",
                        "1": startDate,
                        "2": endDate
                    });
                },
            },
            columns: [{
                    "data": "DM_NUM_DOSSIER",
                    "render": function(data, type, row) {
                        return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                    }
                },
                { "data": "DM_NUM_LTA" },
                { "data": "DM_IMP_EXP" },
                { "data": "FR_LIBELLE" },
                { "data": "CL_LIBELLE" },
                { "data": "DM_MARCHANDISE" },
                { "data": "DM_POIDS" },
                { "data": "DM_NOMBRE" },
                { "data": "DM_MARQUE" },
                { "data": "DM_TERME" },
                { "data": "DM_MAGASIN" }
            ],
            initComplete: function(settings, json) {
                this.api().columns().every(function() {
                    var column = this;
                    var select = $('<select class="border-black border-lg text-black form-control"><option value="">Tout</option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function() {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                });
            }
        }).columns.adjust().draw();
    });

    // Local

    var table_local = $('#table_local').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#btn_get_tab_local').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_LOCAL_FACT', $('#num_local').val()]))
            .done(function(data) {
                if (Object.keys(data).length) {
                    $('#date_local').val(data[0].AAM_DATE_FACTURE),
                        $('#type_local').val(data[0].FT_LIBELLE),
                        $('#num_dos_local').val(data[0].AAM_NUM_DOSSIER),
                        $('#N_TAX').text(data[0].AAM_TOT_NON_TAXABLE),
                        $('#T_TAX').text(data[0].AAM_TO_TAXABLE),
                        $('#T_TVA').text(data[0].AAM_TVA_PORCENTAGE),
                        $('#VAL_TVA').text('(' + data[0].AAM_TVA_VAL + ' %)'),
                        $('#VAL_TIMBRE').text(data[0].AAM_TIMBRE),
                        $('#T_TT').text(data[0].AAM_TOTAL_TTC),
                        $('#TOT_LIB').text('ARRÉTÉE LA PRÉSENTE A LA SOMME DE ' + data[0].AA_TTC_LETTRE),
                        table_local = $('#table_local').DataTable({
                            aaSorting: [],
                            bStateSave: true,
                            bDestroy: true,
                            select: false,
                            destroy: true,
                            paging: false,
                            dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
                            ajax: {
                                url: "/GTRANS/public/Dossier/api/dossier.php",
                                type: "POST",
                                data: function(d) {
                                    return JSON.stringify({
                                        "0": "GET_TAB_LOCAL_FACT",
                                        "1": data[0].AAM_CODE
                                    });
                                },
                            },
                            columns: [{
                                    "data": "AAM_LIB_CONTENU",
                                    "render": function(data, type, row) {
                                        if (row.AAM_LIB_CONTENU.indexOf("       ") !== -1) {
                                            var part1 = row.AAM_LIB_CONTENU.split(/  +/)[0];
                                            var part2 = row.AAM_LIB_CONTENU.split(/  +/)[1];
                                            return '<strong>' + part1 + '</strong><span class="label label-primary pull-right">' + part2 + '</span>';
                                        } else if (row.AAM_LIB_CONT_SUIT) {
                                            return '<strong>' + row.AAM_LIB_CONTENU + '</strong><span class="label label-primary pull-right">' + row.AAM_LIB_CONT_SUIT + '</span>';
                                        } else {
                                            return '<strong>' + row.AAM_LIB_CONTENU + '</strong>';
                                        }
                                    }
                                },
                                {
                                    "data": "AAM_VAL_NON_TAXABLE",
                                    "render": function(data, type, row) {
                                        if (data == '0.000') {
                                            return '';
                                        } else {
                                            return data;
                                        }
                                    }
                                },
                                {
                                    "data": "AAM_VAL_TAXABLE",
                                    "render": function(data, type, row) {
                                        if (data == '0.000') {
                                            return '';
                                        } else {
                                            return data;
                                        }
                                    }
                                }
                            ]
                        }).columns.adjust().draw();
                } else {

                }
            });
    });

    $('#show_dos_local').on('click', function() {
        if ($('#num_dos_local').val()) {
            var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('#num_dos_local').val(), '_blank');
            win.focus();
        }
    });


    // Etranger

    var table_etr = $('#table_etr').DataTable({
        aaSorting: [],
        aoColumnDefs: [{
            bSortable: false,
            aTargets: ["sorting_disabled"]
        }],
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#btn_get_tab_etr').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_ETR_FACT', $('#num_etr').val()]))
            .done(function(data) {
                if (Object.keys(data).length) {
                    $('#date_etr').val(data[0].AAM_DATE_FACTURE),
                        $('#num_dos_etr').val(data[0].AAM_NUM_DOSSIER),
                        $('#T_TT_etr').text(data[0].TT + ' ' + data[0].AAM_DEVISE),
                        table_etr = $('#table_etr').DataTable({
                            aaSorting: [],
                            bStateSave: true,
                            bDestroy: true,
                            select: false,
                            destroy: true,
                            paging: false,
                            dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
                            ajax: {
                                url: "/GTRANS/public/Dossier/api/dossier.php",
                                type: "POST",
                                data: function(d) {
                                    return JSON.stringify({
                                        "0": "GET_TAB_ETR_FACT",
                                        "1": data[0].AAM_CODE
                                    });
                                },
                            },
                            columns: [{
                                    "data": "AAM_LIB_CONTENU",
                                    "render": function(data, type, row) {
                                        if (row.AAM_LIB_CONTENU.indexOf("       ") !== -1) {
                                            var part1 = row.AAM_LIB_CONTENU.split(/  +/)[0];
                                            var part2 = row.AAM_LIB_CONTENU.split(/  +/)[1];
                                            return '<strong>' + part1 + '</strong><span class="label label-primary pull-right">' + part2 + '</span>';
                                        } else if (row.AAM_LIB_CONT_SUIT) {
                                            return '<strong>' + row.AAM_LIB_CONTENU + '</strong><span class="label label-primary pull-right">' + row.AAM_LIB_CONT_SUIT + '</span>';
                                        } else {
                                            return '<strong>' + row.AAM_LIB_CONTENU + '</strong>';
                                        }
                                    }
                                },
                                {
                                    "data": "AMOUNT",
                                    "render": function(data, type, row) {
                                        if (data == '0.000') {
                                            return '';
                                        } else {
                                            return data;
                                        }
                                    }
                                }
                            ]
                        }).columns.adjust().draw();
                } else {

                }
            });
    });

    $('#show_dos_etr').on('click', function() {
        if ($('#num_dos_etr').val()) {
            var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('#num_dos_etr').val(), '_blank');
            win.focus();
        }
    });
});