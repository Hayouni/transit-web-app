$(document).ready(function() {

    /**Attribute  */
    $('#BTN_ATT_U_ADD_AER').on('click', function() {
        $.post('/GTRANS/sys/session.php').fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_AER').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_AER').val(info.user_name);
        });
    });

    $('#clients_AER').on('change', function(e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.length !== 0 ? $('#clients_ADR_AER').val(data[0].CL_ADRESSE) : $('#clients_ADR_AER').val('');
        });
    });

    $('#BTN_ADD_AER').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_AER', {
            'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
            'DM_FOURNISSEUR': $('#fournisseurs_AER').val(),
            'DM_CLIENT': $('#clients_AER').val(),
            'DM_PLACE_DELIVERY': $('#clients_PC_AER').val(),
            'DM_NUM_LTA': $('#LTA_AER').val(),
            'DM_POL': $('#port_DEP_AER').val(),
            'DM_POD': $('#port_ARR_AER').val(),
            'DM_MARCHANDISE': $('#MARCH_AER').val(),
            'DM_DATE_EMBARQ': $('#Date_Dep_AER').val(),
            'DM_DATE_DECHARG': $('#Date_arr_AER').val(),
            'DM_POIDS': $('#Poids_AER').val(),
            'DM_NOMBRE': $('#Nombre_AER').val(),
            'DM_LONGUEUR': $('#long_AER').val(),
            'DM_LARGEUR': $('#larg_AER').val(),
            'DM_HAUTEUR': $('#haut_AER').val(),
            'DM_MAGASIN': $('#magasin_AER').val(),
            'DM_TERME': $('#term_AER').val(),
            'DM_MARQUE': $('#marq_AER').val(),
            'DM_RUBRIQUE': $('#Rubr_AER').val(),
            'DM_VAL_DEVISE': $('#VAL_DEV_AER').val(),
            'DM_ATTRIBUER': $('#DM_ATTRIBUER_AER').attr("user"),
            'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_AER').val()
        }])).fail(function(error) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'L\'ajout au dossier a échoué', 'danger');
            console.dir('fail', error);
        }).done(function(data) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Ajouter au dossier avec succès', 'success');
        });
    });

    $('#BTN_MOD_AER').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MOD_DOS_AER', {
            'DM_FOURNISSEUR': $('#fournisseurs_AER').val(),
            'DM_CLIENT': $('#clients_AER').val(),
            'DM_PLACE_DELIVERY': $('#clients_PC_AER').val(),
            'DM_NUM_LTA': $('#LTA_AER').val(),
            'DM_POL': $('#port_DEP_AER').val(),
            'DM_POD': $('#port_ARR_AER').val(),
            'DM_MARCHANDISE': $('#MARCH_AER').val(),
            'DM_DATE_EMBARQ': $('#Date_Dep_AER').val(),
            'DM_DATE_DECHARG': $('#Date_arr_AER').val(),
            'DM_POIDS': $('#Poids_AER').val(),
            'DM_NOMBRE': $('#Nombre_AER').val(),
            'DM_LONGUEUR': $('#long_AER').val(),
            'DM_LARGEUR': $('#larg_AER').val(),
            'DM_HAUTEUR': $('#haut_AER').val(),
            'DM_MAGASIN': $('#magasin_AER').val(),
            'DM_TERME': $('#term_AER').val(),
            'DM_MARQUE': $('#marq_AER').val(),
            'DM_RUBRIQUE': $('#Rubr_AER').val(),
            'DM_VAL_DEVISE': $('#VAL_DEV_AER').val(),
            'DM_ATTRIBUER': $('#DM_ATTRIBUER_AER').attr("user"),
            'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_AER').val()
        }, $('#DOS_NUM_INP').val()])).fail(function(error) {
            console.log('fail', error);
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'La modification du dossier n\'a pas abouti', 'danger');
        }).done(function(data) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Dossier modifié avec succès', 'success');
        });
    });

    $('#BTN_CLOT_AER').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CLOT_DOS', $('#DOS_NUM_INP').val()]))
            .done(function(data) {
                if (data[0].DM_CLOTURE == "0") {
                    $.CLOT_ME()
                } else {
                    $.UNCLOT_ME()
                }
                //console.log(data)
            });
    });

    /**FACTURATION */

    $('#PAA_AER').on('click', function() {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function(data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                        .fail(function(error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement', 'danger');
                        })
                        .done(function(data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Print/viewer.php?id=PAAAER&dmcle=' + dmcle, '_blank');
                                win.focus();
                                /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAAAER.mrt&dmcle=' + dmcle, '_blank');
                                win.focus();*/
                            } else {
                                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_PAA_AER', dmcle])).done(function(data) {
                                    //console.log(data);
                                    var win = window.open('/GTRANS/public/Print/viewer.php?id=PAAAER&dmcle=' + dmcle, '_blank');
                                    win.focus();
                                    /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAAAER.mrt&dmcle=' + dmcle, '_blank');
                                    win.focus();*/
                                });
                            }
                        });
                })
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#AA_AER').on('click', function() {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function(data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                        .fail(function(error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function(data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Dossier/fact/AAA?DM_CLE=' + dmcle, '_blank');
                                win.focus();
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                            }
                        });
                })
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });


    $('#INV_AER').on('click', function() {
        //var table_mig = $('#TAB_MIG').DataTable();
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function(data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_INV', dmcle]))
                        .fail(function(error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function(data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Dossier/fact/INV?DM_CLE=' + dmcle, '_blank');
                                win.focus();
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                            }
                        });
                })
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#BAD_AER').on('click', function() {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function(data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_HAS_FACT', dmcle]))
                        .fail(function(data) {
                            console.log('fail');
                            console.dir(data);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function(data) {
                            if (Object.keys(data).length) {
                                swal({
                                        title: "Bon a delivrer",
                                        text: "Entrez le du magasin",
                                        type: "input",
                                        showCancelButton: true,
                                        confirmButtonColor: "#2196F3",
                                        closeOnConfirm: false,
                                        animation: "slide-from-right",
                                        inputPlaceholder: "..."
                                    },
                                    function(inputValue) {
                                        if (inputValue === false) return false;
                                        if (inputValue === "") {
                                            swal.showInputError("Nom de magasin requi !");
                                            return false
                                        }
                                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_AAM_CODE', dmcle]))
                                            .done(function(data) {
                                                if (data[0].AAM_CODE) {
                                                    var win = window.open('/GTRANS/public/Print/viewer.php?id=BAD&A=' + dmcle + '&B=' + data[0].AAM_CODE + '&N=' + inputValue, '_blank');
                                                    win.focus();
                                                    /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=ABAD.mrt&B=' + data[0].AAM_CODE + ' &N=' + inputValue, '_blank');
                                                    win.focus();*/
                                                    swal({
                                                        title: 'Demande effectuée',
                                                        text: 'attendre la redirection vers le rapport...',
                                                        type: "success",
                                                        timer: 2000,
                                                        confirmButtonColor: "#2196F3"
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Error !",
                                                        text: "aucun avis d'arrivé trouvé !",
                                                        confirmButtonColor: "#EF5350",
                                                        type: "error"
                                                    });
                                                }
                                            })
                                            .fail(function(error) {
                                                swal({
                                                    title: "Error !",
                                                    text: "demande échoué !",
                                                    confirmButtonColor: "#EF5350",
                                                    type: "error"
                                                });
                                                console.log('fail', error)
                                            });
                                    });
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune Facture sur ce dossier !', 'warning');
                            }
                        });
                })
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });


});