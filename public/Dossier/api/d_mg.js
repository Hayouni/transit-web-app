$(document).ready(function() {

    /**Attribute  */
    $('#BTN_ATT_U_ADD_MIG').on('click', function() {
        $.post('/GTRANS/sys/session.php').fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_MIG').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_MIG').val(info.user_name);
        });
    });

    var TAB_MIG = $('#TAB_MIG').DataTable({
        destroy: true,
        empty: true,
        aaData: null,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#clients_MIG').on('change', function(e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.length !== 0 ? $('#clients_ADR_MIG').val(data[0].CL_ADRESSE) : $('#clients_ADR_MIG').val('');
        });
    });

    $.create_TAB_MIG = function() {
        TAB_MIG = $('#TAB_MIG').DataTable({
            select: {
                style: 'single'
            },
            destroy: true,
            paging: false,
            empty: true,
            scrollY: "200px",
            ajax: {
                url: "/GTRANS/public/Dossier/api/dossier.php",
                type: "POST",
                data: {
                    "0": "GET_DOS_MIG_MARCH",
                    "1": $('#DOS_NUM_INP').val()
                }
            },
            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
            columns: [
                { "data": "DM_CLE" },
                { "data": "DM_NUM_BL" },
                { "data": "DM_MARCHANDISE" },
                { "data": "DM_POIDS" },
                { "data": "DM_NOMBRE" },
                { "data": "CL_LIBELLE" },
                {
                    "data": null,
                    "render": function(data, type, row) {
                        switch (data.DM_CLOTURE) {
                            case 0:
                            case "0":
                                return '<a id="delbtn" class="btn btn-link text-danger"><i class="icon-trash"></i></a>';

                            default:
                                return '<a class="btn btn-link text-warning"><i class="icon-file-locked2"></i></a>';
                        }
                    },
                    "defaultContent": '<a class="btn btn-link text-info"><i class="icon-spinner"></i></a>'
                }
            ]
        }).on('select', function(e, dt, type, indexes) {
            var rowData = $('#TAB_MIG').DataTable().rows(indexes).data().toArray();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_MARCH_DATA', rowData[0].DM_CLE])).fail(function(data) {
                console.log('fail', data);
            }).done(function(data) {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    $('#clients_ADR_MIG').val(data[0].CL_ADRESSE);
                });
                //console.log(data); 
                $('#fournisseurs_MIG').val(data[0].DM_FOURNISSEUR).trigger("chosen:updated");
                $('#clients_MIG').val(data[0].DM_CLIENT).trigger("chosen:updated");
                $('#clients_PC_MIG').val(data[0].DM_PLACE_DELIVERY).trigger("chosen:updated");
                $('#BL_MIG').val(data[0].DM_NUM_BL);
                $('#navire_MIG').val(data[0].DM_NAVIRE).trigger("chosen:updated");
                $('#port_DEP_MIG').val(data[0].DM_POL).trigger("chosen:updated");
                $('#port_ARR_MIG').val(data[0].DM_POD).trigger("chosen:updated");
                $('#MARCH_MIG').val(data[0].DM_MARCHANDISE);
                data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_MIG').val(data[0].DM_DATE_EMBARQ) : $('#Date_Dep_MIG').val('');
                data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_MIG').val(data[0].DM_DATE_DECHARG) : $('#Date_arr_MIG').val('');

                $('#Poids_MIG').val(data[0].DM_POIDS);
                $('#Nombre_MIG').val(data[0].DM_NOMBRE);
                $('#long_MIG').val(data[0].DM_LONGUEUR);
                $('#larg_MIG').val(data[0].DM_LARGEUR);
                $('#haut_MIG').val(data[0].DM_HAUTEUR);
                data[0].DM_TERME ? $('#term_MIG').val(data[0].DM_TERME.toUpperCase()) : $('#term_MIG').val('');
                data[0].DM_MARQUE ? $('#marq_MIG').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_MIG').val('');
                $('#escal_MIG').val(data[0].DM_ESCALE);
                $('#Rubr_MIG').val(data[0].DM_RUBRIQUE);
                $('#VAL_DEV_MIG').val(data[0].DM_VAL_DEVISE);
            });
        }).columns.adjust().draw();
    }

    $('#BTN_ADD_to_MIG').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_To_MIG', {
            'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
            'DM_FOURNISSEUR': $('#fournisseurs_MIG').val(),
            'DM_CLIENT': $('#clients_MIG').val(),
            'DM_PLACE_DELIVERY': $('#clients_PC_MIG').val(),
            'DM_NUM_BL': $('#BL_MIG').val(),
            'DM_NAVIRE': $('#navire_MIG').val(),
            'DM_POL': $('#port_DEP_MIG').val(),
            'DM_POD': $('#port_ARR_MIG').val(),
            'DM_MARCHANDISE': $('#MARCH_MIG').val(),
            'DM_DATE_EMBARQ': $('#Date_Dep_MIG').val(),
            'DM_DATE_DECHARG': $('#Date_arr_MIG').val(),
            'DM_POIDS': $('#Poids_MIG').val(),
            'DM_NOMBRE': $('#Nombre_MIG').val(),
            'DM_LONGUEUR': $('#long_MIG').val(),
            'DM_LARGEUR': $('#larg_MIG').val(),
            'DM_HAUTEUR': $('#haut_MIG').val(),
            'DM_TERME': $('#term_MIG').val(),
            'DM_MARQUE': $('#marq_MIG').val(),
            'DM_ESCALE': $('#escal_MIG').val(),
            'DM_RUBRIQUE': $('#Rubr_MIG').val(),
            'DM_VAL_DEVISE': $('#VAL_DEV_MIG').val(),
            'DM_IMP_EXP': $("#SEL_IMEX_DOS").val(),
            'DM_CODE_COMP_GROUP': $("#SEL_GC_DOS").val(),
            'DM_LIB_COMP_GROUP': $.CG_LIB(),
            'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIG').attr("user"),
            'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIG').val()
        }])).fail(function(error) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'L\'ajout au dossier a échoué', 'danger');
            console.dir('fail', error);
        }).done(function(data) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Ajouter au dossier avec succès', 'success');
            //TAB_MIG.ajax.reload();
            $.create_TAB_MIG();
        });
    });

    $('#BTN_MOD_MIG').on('click', function() {
        if (TAB_MIG.rows('.selected').any()) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_MIG', {
                    'DM_FOURNISSEUR': $('#fournisseurs_MIG').val(),
                    'DM_CLIENT': $('#clients_MIG').val(),
                    'DM_PLACE_DELIVERY': $('#clients_PC_MIG').val(),
                    'DM_NUM_BL': $('#BL_MIG').val(),
                    'DM_NAVIRE': $('#navire_MIG').val(),
                    'DM_POL': $('#port_DEP_MIG').val(),
                    'DM_POD': $('#port_ARR_MIG').val(),
                    'DM_MARCHANDISE': $('#MARCH_MIG').val(),
                    'DM_DATE_EMBARQ': $('#Date_Dep_MIG').val(),
                    'DM_DATE_DECHARG': $('#Date_arr_MIG').val(),
                    'DM_POIDS': $('#Poids_MIG').val(),
                    'DM_NOMBRE': $('#Nombre_MIG').val(),
                    'DM_LONGUEUR': $('#long_MIG').val(),
                    'DM_LARGEUR': $('#larg_MIG').val(),
                    'DM_HAUTEUR': $('#haut_MIG').val(),
                    'DM_TERME': $('#term_MIG').val(),
                    'DM_MARQUE': $('#marq_MIG').val(),
                    'DM_ESCALE': $('#escal_MIG').val(),
                    'DM_RUBRIQUE': $('#Rubr_MIG').val(),
                    'DM_VAL_DEVISE': $('#VAL_DEV_MIG').val(),
                    'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIG').attr("user"),
                    'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIG').val()
                }, TAB_MIG.rows('.selected').data()[0].DM_CLE]))
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'La modification du dossier n\'a pas abouti', 'danger');
                })
                .done(function(data) {
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Dossier modifié avec succès', 'success');
                    $('#TAB_MIG').DataTable().ajax.reload();
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });

    $('#TAB_MIG tbody').on('click', 'a#delbtn', function() {
        var data = TAB_MIG.row($(this).parents('tr')).data();
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_from_MIG', data.DM_CLE]))
            .fail(function(error) {
                console.log('fail', error);
                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de suppression', 'danger');
            })
            .done(function(data) {
                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'La suppression a été réussie', 'success');
                $('#TAB_MIG').DataTable().ajax.reload();
            });
    });

    $('#BTN_CLOT_MIG').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CLOT_DOS', $('#DOS_NUM_INP').val()]))
            .done(function(data) {
                if (data[0].DM_CLOTURE == "0") {
                    $.CLOT_ME()
                } else {
                    $.UNCLOT_ME()
                }
                //console.log(data)
            });
    });

    /**FACTURATION */

    $('#PAA_MIG').on('click', function() {
        //var table_mig = $('#TAB_MIG').DataTable();
        if (TAB_MIG.rows('.selected').any()) {
            var dmcle = TAB_MIG.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Print/viewer.php?id=PAA&dmcle=' + dmcle, '_blank');
                        win.focus();
                        /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAA.mrt&dmcle=' + dmcle, '_blank');
                        win.focus();*/
                    } else {
                        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_PAA', dmcle])).done(function(data) {
                            //console.log(data);
                            var win = window.open('/GTRANS/public/Print/viewer.php?id=PAA&dmcle=' + dmcle, '_blank');
                            win.focus();
                            /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAA.mrt&dmcle=' + dmcle, '_blank');
                            win.focus();*/
                        });
                    }
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });

    $('#AA_MIG').on('click', function() {
        //var table_mig = $('#TAB_MIG').DataTable();
        if (TAB_MIG.rows('.selected').any()) {
            var dmcle = TAB_MIG.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });

    $('#MAG_MIG').on('click', function() {
        //var table_mig = $('#TAB_MIG').DataTable();
        if (TAB_MIG.rows('.selected').any()) {
            var dmcle = TAB_MIG.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_MAG', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Dossier/fact/MAG?DM_CLE=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }

    });

    $('#INV_MIG').on('click', function() {
        //var table_mig = $('#TAB_MIG').DataTable();
        if (TAB_MIG.rows('.selected').any()) {
            var dmcle = TAB_MIG.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_INV', dmcle]))
                .fail(function(error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Dossier/fact/INV?DM_CLE=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });


    $('#BAD_MIG').on('click', function() {
        if (TAB_MIG.rows('.selected').any()) {
            var dmcle = TAB_MIG.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        swal({
                                title: "Bon a delivrer",
                                text: "Entrez le du magasin",
                                type: "input",
                                showCancelButton: true,
                                confirmButtonColor: "#2196F3",
                                closeOnConfirm: false,
                                animation: "slide-from-right",
                                inputPlaceholder: "..."
                            },
                            function(inputValue) {
                                if (inputValue === false) return false;
                                if (inputValue === "") {
                                    swal.showInputError("Nom de magasin requi !");
                                    return false
                                }
                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_AAM_CODE', dmcle]))
                                    .done(function(data) {
                                        if (data[0].AAM_CODE) {
                                            var win = window.open('/GTRANS/public/Print/viewer.php?id=BAD&A=' + dmcle + '&B=' + data[0].AAM_CODE + '&N=' + inputValue, '_blank');
                                            win.focus();
                                            /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=BAD.mrt&A=' + dmcle + ' &B=' + data[0].AAM_CODE + ' &N=' + inputValue, '_blank');
                                            win.focus();*/
                                            swal({
                                                title: 'Demande effectuée',
                                                text: 'attendre la redirection vers le rapport...',
                                                type: "success",
                                                timer: 2000,
                                                confirmButtonColor: "#2196F3"
                                            });
                                        } else {
                                            swal({
                                                title: "Error !",
                                                text: "aucun avis d'arrivé trouvé !",
                                                confirmButtonColor: "#EF5350",
                                                type: "error"
                                            });
                                        }
                                    })
                                    .fail(function(error) {
                                        swal({
                                            title: "Error !",
                                            text: "demande échoué !",
                                            confirmButtonColor: "#EF5350",
                                            type: "error"
                                        });
                                        console.log('fail', error)
                                    });
                            });
                    } else {
                        $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });
});