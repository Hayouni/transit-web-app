$(document).ready(function () {

    /**INIT COMPONENTS */
    moment.locale('fr');

    $(".selectbox").selectBoxIt({
        autoWidth: false
    });

    jQuery.fn.extend({
        disable: function (state) {
            return this.each(function () {
                this.disabled = state;
            });
        }
    });

    $.fn.selectpicker.defaults = {
        iconBase: '',
        tickIcon: 'icon-checkmark3'
    }

    $('.bootstrap-select').hide();

    $.PNotify_Alert = function (title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 5000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 280
            },
        });
    };

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        //empty: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        //dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        }
    });
    /**END INT COMPONENT */

    /**Initialize All Selectbox Fr Cl Aer Nav Port */
    $('#sidebr_rt').block({
        message: '<span class="text-semibold"><i class="icon-spinner4 spinner position-left"></i>&nbsp; chargement de données</span>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: '10px 15px',
            color: '#fff',
            width: 'auto',
            '-webkit-border-radius': 2,
            '-moz-border-radius': 2,
            backgroundColor: '#333'
        },
        onBlock: function () {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['fournisseurs'])).fail(function (data) {
                console.log('fail', data);
            }).done(function (data) {
                $('#fournisseurs_MIG').append('<option value="">-</option>');
                $('#fournisseurs_MIC').append('<option value="">-</option>');
                $('#fournisseurs_AER').append('<option value="">-</option>');
                data.forEach(function (element) {
                    $('#fournisseurs_MIG').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                    $('#fournisseurs_MIC').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                    $('#fournisseurs_AER').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                }, this);
            }).always(function () {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function (data) {
                    console.log('fail', data);
                }).done(function (data) {
                    $('#clients_PC_MIG').append('<option value="">-</option>');
                    $('#clients_PC_MIC').append('<option value="">-</option>');
                    $('#clients_PC_AER').append('<option value="">-</option>');
                    $('#clients_MIG').append('<option value="">-</option>');
                    $('#clients_MIC').append('<option value="">-</option>');
                    $('#clients_AER').append('<option value="">-</option>');
                    data.forEach(function (element) {
                        $('#clients_MIG').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_PC_MIG').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_MIC').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_PC_MIC').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_AER').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_PC_AER').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                    }, this);
                }).always(function () {
                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['navire'])).fail(function (data) {
                        console.log('fail', data);
                    }).done(function (data) {
                        $('#navire_MIG').append('<option value="">-</option>');
                        $('#navire_MIC').append('<option value="">-</option>');
                        data.forEach(function (element) {
                            $('#navire_MIG').append('<option value="' + element.NA_CODE + '">' + element.NA_LIBELLE + '</option>');
                            $('#navire_MIC').append('<option value="' + element.NA_CODE + '">' + element.NA_LIBELLE + '</option>');
                        }, this);
                    }).always(function () {
                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['port'])).fail(function (data) {
                            console.log('fail', data);
                        }).done(function (data) {
                            $('#port_DEP_MIG').append('<option value="">-</option>');
                            $('#port_ARR_MIG').append('<option value="">-</option>');
                            $('#port_DEP_MIC').append('<option value="">-</option>');
                            $('#port_ARR_MIC').append('<option value="">-</option>');
                            data.forEach(function (element) {
                                $('#port_DEP_MIG').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                                $('#port_ARR_MIG').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                                $('#port_DEP_MIC').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                                $('#port_ARR_MIC').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                            }, this);
                        }).always(function () {
                            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['aeroport'])).fail(function (data) {
                                console.log('fail', data);
                            }).done(function (data) {
                                $('#port_DEP_AER').append('<option value="">-</option>');
                                $('#port_ARR_AER').append('<option value="">-</option>');
                                data.forEach(function (element) {
                                    $('#port_DEP_AER').append('<option value="' + element.AE_CODE + '">' + element.AE_LIBELLE + '</option>');
                                    $('#port_ARR_AER').append('<option value="' + element.AE_CODE + '">' + element.AE_LIBELLE + '</option>');
                                }, this);
                            }).always(function () {
                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['volum_container'])).fail(function (data) {
                                    console.log('fail', data);
                                }).done(function (data) {
                                    data.forEach(function (element) {
                                        $('#VOLUM_MIC').append('<option value="' + element.CV_LBELLE + '">' + element.CV_LBELLE + '</option>');
                                    }, this);
                                }).always(function () {
                                    $('#sidebr_rt').unblock();
                                    $(".livesearch").chosen({
                                        width: '100%',
                                        enable_split_word_search: false
                                    });
                                    if (NumD) {
                                        $('#GET_DOS_BTN').click();
                                    }
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_ACTIVE_DOS'])).fail(function (data) {
                                        console.log('fail', data);
                                    }).done(function (data) {
                                        $('.opend-folder-slbox').html('');
                                        $('.opend-folder-slbox').append('<option value="">Dossiers ouverts</option>');
                                        $('.opend-folder-slbox').append('<option value="" data-divider="true">divider</option>');
                                        data.forEach(function (element) {
                                            $('.opend-folder-slbox').append('<option value="' + element.DM_NUM_DOSSIER + '">' + element.DM_NUM_DOSSIER + '</option>');
                                        }, this);
                                    }).always(function () {
                                        $('.bootstrap-select').selectpicker();
                                        $('.bootstrap-select').show();
                                        $('#sel-here').click(function () {
                                            if ($('.bootstrap-select option:selected').val()) {
                                                $('#DOS_NUM_INP').val($('.bootstrap-select option:selected').val())
                                                $('#GET_DOS_BTN').click();
                                            }
                                        });
                                        $('#sel-out').click(function () {
                                            if ($('.bootstrap-select option:selected').val()) {
                                                var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('.bootstrap-select option:selected').val(), '_blank');
                                                win.focus();
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }
    });

    var TAB_FACT = $('#TAB_FACT').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: true,
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });
    /**-- End Initialize */

    function Create_TAB_FACT_AER() {
        TAB_FACT = $('#TAB_FACT').DataTable({
            select: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            responsive: true,
            empty: true,
            destroy: true,
            paging: true,
            ajax: {
                url: "/GTRANS/public/Dossier/api/dossier.php",
                type: "POST",
                data: function (d) {
                    return JSON.stringify({
                        "0": "GET_FACT_DOS",
                        "1": $('#DOS_NUM_INP').val()
                    });
                },
            },
            aoColumnDefs: [{
                "sClass": "hide_me",
                "aTargets": [0]
            }],
            columns: [{
                    "data": "AAM_CODE"
                },
                {
                    "data": "AAM_NUM_FACTURE",
                    "render": function (data, type, row) {
                        return '<a id="SHOWFACT">' + row.AAM_NUM_FACTURE + '</a>';
                    }
                },
                {
                    "data": "AAM_DATE_FACTURE"
                },
                {
                    "data": "FT_LIBELLE",
                    "render": function (data, type, row) {
                        switch (row.FT_LIBELLE) {
                            case 'Timbrage':
                                return '<span class="label bg-danger-600">' + row.FT_LIBELLE + '</span>';
                            case 'Magasinage':
                                return '<span class="label bg-info-600">' + row.FT_LIBELLE + '</span>';
                            case 'Surestarie':
                                return '<span class="label bg-brown-600">' + row.FT_LIBELLE + '</span>';
                            case 'Aérien':
                                return '<span class="label bg-indigo-600">' + row.FT_LIBELLE + '</span>';
                            case 'Avoirs':
                                return '<span class="label bg-warning-600">' + row.FT_LIBELLE + '</span>';
                            default:
                                return '<span class="label bg-slate-600">' + row.FT_LIBELLE + '</span>';
                        }
                    }
                },
                {
                    "data": "AAM_BON_COMMAND"
                },
                {
                    "data": "AAM_TOTAL_TTC"
                },
                {
                    "data": "CL_LIBELLE"
                },
                {
                    "data": "CL_RSPONSABLE"
                },
                {
                    "data": "CL_TEL_ETABLISS"
                },
                {
                    "data": "CL_FAX"
                },
                {
                    "data": "CL_EMAIL"
                }
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;
                    if (column[0] == 3 || column[0] == 4 || column[0] == 6) {
                        var select = $('<select class="border-black border-lg text-black form-control"><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
                $('#TAB_FACT tbody').on('click', 'a#SHOWFACT', function () {
                    var data = TAB_FACT.row($(this).parents('tr')).data();
                    $.post('/GTRANS/sys/config/way.php', JSON.stringify(['FIND_FACT', data.AAM_NUM_FACTURE])).done(function (data) {
                        if (data.URL !== undefined) {
                            var win = window.open(data.URL, '_blank');
                            win.focus();
                        } else {
                            new PNotify({
                                title: 'Rechercher une facture',
                                text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: {
                                    "dir1": "up",
                                    "dir2": "left",
                                    "firstpos1": 25,
                                    "firstpos2": 25
                                }
                            });
                        }
                    });
                });
            }
        }).columns.adjust().draw();
    }

    $.CG_LIB = function () {
        return ($("#SEL_TYPE_DOS").val() == 'M') ? ($("#SEL_GC_DOS").val() == 'C') ? 'COMPLET' : 'GROUPAGE' : null
    }

    $('#DOS_NUM_INP').on('keypress', function (e) {
        if (e.which === 13) {
            get_dos();
        }
    });

    $('#GET_DOS_BTN').on('click', function (e) {
        get_dos();
    });

    $('.toutes_les_factures').on('click', function () {
        Create_TAB_FACT_AER(),
            $('#modal_facture').modal('show');
    });


    $('#BTN_SET_COICE_DOS').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CHECK_DOS_EXIST', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    //console.log(data)
                    if (Number(data[0].NBR_DOS) > 0) {
                        $.PNotify_Alert('Erreur', "Le numéro de dossier existe déjà", 'danger');
                    } else {
                        swal({
                                title: "valider le numéro de dossier",
                                text: "N° DOSSIER : " + $('#DOS_NUM_INP').val(),
                                type: "info",
                                showCancelButton: true,
                                closeOnConfirm: false,
                                confirmButtonColor: "#2196F3",
                                showLoaderOnConfirm: true
                            },
                            function () {
                                var id = $('#DOS_NUM_INP').val();
                                var dos_param = {
                                    'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
                                    'DM_IMP_EXP': $("#SEL_IMEX_DOS").val(),
                                    'DM_CODE_COMP_GROUP': ($("#SEL_TYPE_DOS").val() == 'M') ? $("#SEL_GC_DOS").val() : null,
                                    'DM_LIB_COMP_GROUP': $.CG_LIB()
                                }
                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SAVE_DOS', id.substr(id.length - 4)]))
                                    .fail(function (data) {
                                        console.log('fail', data);
                                        swal({
                                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                            text: 'Validation a échoué',
                                            type: "error",
                                            timer: 2500
                                        });
                                    }).done(function (data) {
                                        if (($("#SEL_GC_DOS").val() !== 'G') || ($("#SEL_TYPE_DOS").val() == 'A')) {
                                            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_To_MIG', dos_param])).fail(function (data) {
                                                console.log('fail', data);
                                                swal({
                                                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                                    text: 'Validation a échoué',
                                                    type: "error",
                                                    timer: 2500
                                                });
                                            }).done(function (data) {
                                                $('#BTN_ADD_MIC').css('display', 'block');
                                                $('#BTN_ADD_AER').css('display', 'block');
                                                swal({
                                                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                                    text: "le numéro de dossier est validé",
                                                    timer: 2500,
                                                    type: "success"
                                                });
                                            });
                                        } else {
                                            swal({
                                                title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                                text: "le numéro de dossier est validé",
                                                timer: 2500,
                                                type: "success"
                                            });
                                        }
                                        SET_SELCTED_DOS();
                                    });
                            });
                    }
                })
                .fail(function (data) {
                    console.log('fail', data);
                    swal({
                        title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                        text: 'Erreur : 0XD0001',
                        type: "error",
                        timer: 2500
                    });
                });
        }
        /*$("#SEL_TYPE_DOS").selectBoxIt('selectOption', 'A');
        $("#SEL_TYPE_DOS").selectBoxIt('disable');
        $("#SEL_TYPE_DOS").selectBoxIt('enable');*/
    });

    $('#CLS_DOS_BTN').on('click', function () {
        $('#AER_SHOW').css('display', 'none');
        $('#MC_SHOW').css('display', 'none');
        $('#MG_SHOW').css('display', 'none');
        cloture(false);
        Clean_SELECTED_DOS_CHOICE();
        DM_CLEANER();

        //console.log($('#fournisseurs_MIC').val())
        //console.log("selected text " + $("#fournisseurs_MIC option:selected").text());
    });

    $("#SEL_TYPE_DOS").selectBoxIt().change(function () {
        //console.log(this.value);
        if (this.value == 'A') {
            $("#SEL_GC_DOS").parents('div.form-group').css("display", "none");
            $("#SEL_GC_DOS").addClass('Aer_undef');
        } else {
            $("#SEL_GC_DOS").parents('div.form-group').css("display", "block");
            $("#SEL_GC_DOS").removeClass('Aer_undef');
        }
    });

    function Clean_SELECTED_DOS_CHOICE() {
        $("#SEL_TYPE_DOS").selectBoxIt('enable');
        $("#SEL_IMEX_DOS").selectBoxIt('enable');
        $("#SEL_GC_DOS").selectBoxIt('enable');
        $('#BTN_SET_COICE_DOS').disable(false);
    }

    function SET_SELCTED_DOS(_TYPE, _IMEX, _GC) {
        if (_TYPE && _IMEX) {
            if (_TYPE == 'M' && (_GC == null || _GC == undefined)) {
                $.PNotify_Alert('Erreur', "mauvaise sélection automatique du dossier !", 'danger')
            } else {
                $("#SEL_TYPE_DOS").selectBoxIt('selectOption', _TYPE);
                $("#SEL_IMEX_DOS").selectBoxIt('selectOption', _IMEX);
                $("#SEL_GC_DOS").selectBoxIt('selectOption', _GC);
            }
        } else {
            _TYPE = $("#SEL_TYPE_DOS").val();
            _IMEX = $("#SEL_IMEX_DOS").val()
            _GC = $("#SEL_GC_DOS").val();
        }
        var DOS_FULL_NAME = '';
        switch (_TYPE) {
            case 'A':
                DOS_FULL_NAME = '<i class="icon-airplane2"></i> - AERIEN';
                switch (_IMEX) {
                    case 'IMPORT':
                        DOS_FULL_NAME += ' IMPORT';
                        break;

                    default:
                        DOS_FULL_NAME += ' EXPORT';
                        break;
                }
                $('#AER_SHOW').css('display', 'block');
                $('#MC_SHOW').css('display', 'none');
                $('#MG_SHOW').css('display', 'none');
                break;

            default:
                DOS_FULL_NAME = '<i class="icon-ship"></i> - MARITIME';
                switch (_IMEX) {
                    case 'IMPORT':
                        DOS_FULL_NAME += ' IMPORT';
                        break;

                    default:
                        DOS_FULL_NAME += ' EXPORT';
                        break;
                }
                switch (_GC) {
                    case 'C':
                        DOS_FULL_NAME += ' COMPLET';
                        $('#MC_SHOW').css('display', 'block');
                        $('#AER_SHOW').css('display', 'none');
                        $('#MG_SHOW').css('display', 'none');
                        break;

                    default:
                        DOS_FULL_NAME += ' GROUPAGE';
                        $('#MG_SHOW').css('display', 'block');
                        $('#AER_SHOW').css('display', 'none');
                        $('#MC_SHOW').css('display', 'none');
                        break;
                }
                break;
        }
        $("#SEL_TYPE_DOS").selectBoxIt('disable');
        $("#SEL_IMEX_DOS").selectBoxIt('disable');
        $("#SEL_GC_DOS").selectBoxIt('disable');
        $('#BTN_SET_COICE_DOS').disable(true);
        $('.NAME_DOS_CLFDS').html(DOS_FULL_NAME);
    }

    function cloture(val) {
        if (val) {
            $('#BTN_CLOT_MIG i').removeClass('icon-unlocked').addClass('icon-lock4');
            $('#BTN_MOD_MIG').css('display', 'none');
            $('#BTN_ADD_to_MIG').css('display', 'none');

            $('#BTN_CLOT_MIC i').removeClass('icon-unlocked').addClass('icon-lock4');
            $('#BTN_MOD_MIC').css('display', 'none');
            //$('#BTN_ADD_MIC').css('display', 'none');
            //MCMarch
            $('#DEL_MICM').css('display', 'none');
            $('#MOD_MICM').css('display', 'none');
            $('#ADD_MICM').css('display', 'none');
            //MCMarch
            $('#DEL_MICC').css('display', 'none');
            $('#MOD_MICC').css('display', 'none');
            $('#ADD_MICC').css('display', 'none');

            $('#BTN_CLOT_AER i').removeClass('icon-unlocked').addClass('icon-lock4');
            $('#BTN_MOD_AER').css('display', 'none');
            //$('#BTN_ADD_AER').css('display', 'none');

        } else {
            $('#BTN_CLOT_MIG i').removeClass('icon-lock4').addClass('icon-unlocked');
            $('#BTN_MOD_MIG').css('display', 'block');
            $('#BTN_ADD_to_MIG').css('display', 'block');

            $('#BTN_CLOT_MIC i').removeClass('icon-lock4').addClass('icon-unlocked');
            $('#BTN_MOD_MIC').css('display', 'block');
            //$('#BTN_ADD_MIC').css('display', 'block');
            //MCMarch
            $('#DEL_MICM').css('display', 'block');
            $('#MOD_MICM').css('display', 'block');
            $('#ADD_MICM').css('display', 'block');
            //MCMarch
            $('#DEL_MICC').css('display', 'block');
            $('#MOD_MICC').css('display', 'block');
            $('#ADD_MICC').css('display', 'block');

            $('#BTN_CLOT_AER i').removeClass('icon-lock4').addClass('icon-unlocked');
            $('#BTN_MOD_AER').css('display', 'block');
            //$('#BTN_ADD_AER').css('display', 'block');
        }
    }

    function get_dos() {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CLOT_DOS', $('#DOS_NUM_INP').val()])).done(function (data) {
                if (Object.keys(data).length) {
                    if (data[0].DM_CLOTURE === "1") {
                        /*new PNotify({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: '<span class="text-center display-block text-bold"><h6>Dossier Cloturé ! <i class="icon-file-locked2 ml-5"></i></h6></span>',
                            addclass: 'stack-top-left bg-danger-400',
                            icon: 'icon-info22',
                            hide: false,
                            stack: { "dir1": "down", "dir2": "right", "push": "top" }
                        });*/
                        cloture(true);
                    } else {
                        cloture(false);
                    }
                } else {
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), "Dossier introuvable ou inexistant, vérifiez le numéro", 'warning');
                }
            }).always(function (data) {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM_INP').val()]), function (data) {
                    if (Object.keys(data).length) {
                        DM_CLEANER();
                        switch (data[0].DM_CODE_COMP_GROUP) {
                            case null: //Case Aerian
                            case '':
                                SET_SELCTED_DOS('A', data[0].DM_IMP_EXP)
                                $('#BTN_ADD_AER').css('display', 'none');

                                $('#DM_ATTRIBUER_AER').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_AER').val(data.UFNAME);
                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_AER').prop('disabled', true);
                                } else {
                                    $('#BTN_ATT_U_ADD_AER').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_AER').prop('readonly', true);

                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_AER_DATA', $('#DOS_NUM_INP').val()])).fail(function (data) {
                                    console.log('fail', data);
                                }).done(function (data) {
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function (data) {
                                        console.log('fail', data);
                                    }).done(function (data) {
                                        $('#clients_ADR_AER').val(data[0].CL_ADRESSE);
                                    });
                                    //console.log(data);
                                    $('#fournisseurs_AER').val(data[0].DM_FOURNISSEUR).trigger("chosen:updated");
                                    $('#clients_AER').val(data[0].DM_CLIENT).trigger("chosen:updated");
                                    $('#clients_PC_AER').val(data[0].DM_PLACE_DELIVERY).trigger("chosen:updated");
                                    $('#LTA_AER').val(data[0].DM_NUM_LTA);
                                    $('#port_DEP_AER').val(data[0].DM_POL).trigger("chosen:updated");
                                    $('#port_ARR_AER').val(data[0].DM_POD).trigger("chosen:updated");
                                    $('#MARCH_AER').val(data[0].DM_MARCHANDISE);
                                    data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_AER').val(data[0].DM_DATE_EMBARQ) : $('#Date_Dep_AER').val('');
                                    data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_AER').val(data[0].DM_DATE_DECHARG) : $('#Date_arr_AER').val('');

                                    $('#Poids_AER').val(data[0].DM_POIDS);
                                    $('#Nombre_AER').val(data[0].DM_NOMBRE);
                                    $('#long_AER').val(data[0].DM_LONGUEUR);
                                    $('#larg_AER').val(data[0].DM_LARGEUR);
                                    $('#haut_AER').val(data[0].DM_HAUTEUR);
                                    data[0].DM_MAGASIN ? $('#magasin_AER').val(data[0].DM_MAGASIN.toUpperCase()) : $('#magasin_AER').val('');
                                    data[0].DM_TERME ? $('#term_AER').val(data[0].DM_TERME.toUpperCase()) : $('#term_AER').val('');
                                    data[0].DM_MARQUE ? $('#marq_AER').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_AER').val('');
                                    $('#Rubr_AER').val(data[0].DM_RUBRIQUE);
                                    $('#VAL_DEV_AER').val(data[0].DM_VAL_DEVISE);
                                });
                                break;

                            case 'G':
                                SET_SELCTED_DOS('M', data[0].DM_IMP_EXP, 'G');

                                $('#DM_ATTRIBUER_MIG').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_MIG').val(data.UFNAME);
                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_MIG').prop('disabled', true);
                                } else {
                                    $('#BTN_ATT_U_ADD_MIG').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_MIG').prop('readonly', true);

                                $.create_TAB_MIG();

                                break;

                            case 'C':
                                SET_SELCTED_DOS('M', data[0].DM_IMP_EXP, 'C');
                                $('#BTN_ADD_MIC').css('display', 'none');

                                $('#DM_ATTRIBUER_MIC').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_MIC').val(data.UFNAME);
                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_MIC').prop('disabled', true);
                                } else {
                                    $('#BTN_ATT_U_ADD_MIC').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_MIC').prop('readonly', true);

                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA', $('#DOS_NUM_INP').val()])).fail(function (data) {
                                    console.log('fail', data);
                                }).done(function (data) {
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function (data) {
                                        console.log('fail', data);
                                    }).done(function (data) {
                                        $('#clients_ADR_MIC').val(data[0].CL_ADRESSE);
                                    });
                                    $('#fournisseurs_MIC').val(data[0].DM_FOURNISSEUR).trigger("chosen:updated");
                                    $('#clients_MIC').val(data[0].DM_CLIENT).trigger("chosen:updated");
                                    $('#clients_PC_MIC').val(data[0].DM_PLACE_DELIVERY).trigger("chosen:updated");
                                    $('#BL_MIC').val(data[0].DM_NUM_BL);
                                    $('#navire_MIC').val(data[0].DM_NAVIRE).trigger("chosen:updated");
                                    $('#port_DEP_MIC').val(data[0].DM_POL).trigger("chosen:updated");
                                    $('#port_ARR_MIC').val(data[0].DM_POD).trigger("chosen:updated");
                                    data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_MIC').val(data[0].DM_DATE_EMBARQ) : $('#Date_Dep_MIC').val('');
                                    data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_MIC').val(data[0].DM_DATE_DECHARG) : $('#Date_arr_MIC').val('');
                                    $('#NBR_MIC').val(data[0].DM_NOMBRE);
                                    data[0].DM_TERME ? $('#term_MIC').val(data[0].DM_TERME.toUpperCase()) : $('#term_MIC').val('');
                                    data[0].DM_MARQUE ? $('#marq_MIC').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_MIC').val('');
                                    $('#escal_MIC').val(data[0].DM_ESCALE);
                                    $('#VAL_DEV_MIC').val(data[0].DM_VAL_DEVISE);

                                    $.Create_MC1MC2_TAB();

                                });
                                break;
                            default:
                                break;
                        }
                    } else {
                        $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), "'Dossier introuvable ou inexistant, vérifiez le numéro',", 'warning');
                    }
                });
            });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    }

    function DM_CLEANER() {
        //MIG 
        if ($("#TAB_MIG").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIG').DataTable().clear().draw();
        if ($("#TAB_FACT").DataTable().page.info().recordsTotal !== 0) $('#TAB_FACT').DataTable().clear().draw();
        $('#BL_MIG').val('');
        $('#MARCH_MIG').val('');
        $('#Date_Dep_MIG').val('');
        $('#Date_arr_MIG').val('');
        $('#Poids_MIG').val('');
        $('#Nombre_MIG').val('');
        $('#long_MIG').val('');
        $('#larg_MIG').val('');
        $('#haut_MIG').val('');
        $('#escal_MIG').val('');
        $('#Rubr_MIG').val('');
        $('#VAL_DEV_MIG').val('');
        $('#clients_ADR_MIG').val('');
        $('#DM_ATTRIBUER_MIG').attr("user", "");
        $('#DM_ATTRIBUER_MIG').val('');
        //MIC
        $('#BL_MIC').val('');
        $('#Date_Dep_MIC').val('');
        $('#Date_arr_MIC').val('');
        $('#NBR_MIC').val('');
        $('#term_MIC').val('');
        $('#marq_MIC').val('');
        $('#escal_MIC').val('');
        $('#VAL_DEV_MIC').val('');
        $('#clients_ADR_MIC').val('');
        $('#DM_ATTRIBUER_MIC').attr("user", "");
        $('#DM_ATTRIBUER_MIC').val('');
        //----> MIC MARCH
        if ($("#TAB_MIC1").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIC1').DataTable().clear().draw();
        $('#MARCH_MIC').val('');
        $('#Rubr_MIC').val('');
        $('#Nombre_MIC').val('');
        $('#Poids_MIC').val('');
        //----> MIC CONT
        if ($("#TAB_MIC2").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIC2').dataTable().fnClearTable();
        $('#Numero_MICC').val('');
        $('#Rubrq_MICC').val('');
        $('#PoidsV_MIC').val('');
        $('#MARC_NAME_C').html('');

        //AER
        $('#fournisseurs_AER').val('');
        $('#clients_AER').val('');
        $('#LTA_AER').val('');
        $('#port_DEP_AER').val('');
        $('#port_ARR_AER').val('');
        $('#MARCH_AER').val('');
        $('#Date_Dep_AER').val('');
        $('#Date_arr_AER').val('');
        $('#Poids_AER').val('');
        $('#Nombre_AER').val('');
        $('#long_AER').val('');
        $('#larg_AER').val('');
        $('#haut_AER').val('');
        $('#magasin_AER').val('');
        $('#term_AER').val('');
        $('#marq_AER').val('');
        $('#Rubr_AER').val('');
        $('#VAL_DEV_AER').val('');
        $('#DM_ATTRIBUER_AER').attr("user", "");
        $('#DM_ATTRIBUER_AER').val('');
    }

    $('#ADD_DOS_BTN').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GEN_NEW_DOS_NUM'])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            $('#AER_SHOW').css('display', 'none');
            $('#MC_SHOW').css('display', 'none');
            $('#MG_SHOW').css('display', 'none');
            cloture(false);
            Clean_SELECTED_DOS_CHOICE();
            DM_CLEANER();
            $('#DOS_NUM_INP').val(data);
        });
    });

    $('#DEL_DOS_BTN').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CHECK_DOS_EXIST', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    if (Number(data[0].NBR_DOS) > 0) {
                        swal({
                                title: "mot de passe administrateur",
                                text: "Entrez le mot de passe de l'administrateur pour confirmation",
                                type: "input",
                                showCancelButton: true,
                                confirmButtonColor: "#2196F3",
                                closeOnConfirm: false,
                                animation: "slide-from-top",
                                inputPlaceholder: "..."
                            },
                            function (inputValue) {
                                if (inputValue === false) return false;
                                if (inputValue === "") {
                                    swal.showInputError("Mot de passe requis !");
                                    return false
                                }
                                var id = $('#DOS_NUM_INP').val();
                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CONF_DEL_PASS', inputValue, id]))
                                    .fail(function (error) {
                                        swal({
                                            title: "Mot de passe incorrect !",
                                            text: "Erreur : contacter l\'administrateur !",
                                            confirmButtonColor: "#EF5350",
                                            type: "error"
                                        });
                                    })
                                    .done(function (data) {
                                        swal({
                                            title: "Mot de passe confirmé",
                                            text: 'Dossier supprimé définitivement. ' + id + '.',
                                            type: "success",
                                            confirmButtonColor: "#2196F3",
                                            timer: 2000
                                        });
                                        $('#AER_SHOW').css('display', 'none');
                                        $('#MC_SHOW').css('display', 'none');
                                        $('#MG_SHOW').css('display', 'none');
                                        cloture(false);
                                        Clean_SELECTED_DOS_CHOICE();
                                        DM_CLEANER();
                                    });

                            });
                    } else {
                        $.PNotify_Alert('Erreur', "Aucun enregistrement pour ce dossier seulement le numéro réservé", 'danger');
                    }
                });
        }

    });

    $.CLOT_ME = function () {
        swal({
                title: "Cloture de dossier : " + $('#DOS_NUM_INP').val(),
                text: 'Êtes-vous sûr ?',
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonColor: "#2196F3",
                showLoaderOnConfirm: true
            },
            function () {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CLOT_ME', $('#DOS_NUM_INP').val()]))
                    .fail(function (error) {
                        swal({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: 'Problème lors de la fermeture du dossier',
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        });
                        console.dir('fail', error);
                    }).done(function (data) {
                        cloture(true);
                        swal({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: 'Dossier cloturé avec succès',
                            type: "success",
                            confirmButtonColor: "#2196F3",
                            timer: 2000
                        });
                    });
            });
    }

    $.UNCLOT_ME = function () {
        swal({
                title: "mot de passe administrateur",
                text: "Entrez le mot de passe de l'administrateur pour confirmation",
                type: "input",
                showCancelButton: true,
                confirmButtonColor: "#2196F3",
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "..."
            },
            function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("Mot de passe requis !");
                    return false
                }
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CONF_ADM_PASS', inputValue]))
                    .fail(function (error) {
                        swal({
                            title: "Mot de passe incorrect !",
                            text: "Erreur : contacter l\'administrateur !",
                            confirmButtonColor: "#EF5350",
                            type: "error"
                        });
                    })
                    .done(function (data) {
                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['UNCLOT_ME', $('#DOS_NUM_INP').val()]))
                            .fail(function (error) {
                                swal({
                                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                    text: 'Problème lors de la l\'ouverture  du dossier',
                                    confirmButtonColor: "#EF5350",
                                    type: "error"
                                });
                                console.dir('fail', error);
                            }).done(function (data) {
                                cloture(false);
                                swal({
                                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                                    text: 'le dossier a ouvert avec succès',
                                    type: "success",
                                    timer: 2000,
                                    confirmButtonColor: "#2196F3"
                                });
                            });
                    });

            });
    }

});