$(document).ready(function() {
    moment.locale('fr');

    $.fn.toText = function(str) {
        var cache = this.children();
        this.text(str).append(cache);
    };

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        aaSorting: [],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_FC'])).done(function(data) {
        var COL_EXPORT = data.EXPORT,
            COL_IMPORT = data.IMPORT,
            COL_CEXPORT = data.CEXPORT,
            COL_CIMPORT = data.CIMPORT,
            COL_GEXPORT = data.GEXPORT,
            COL_GIMPORT = data.GIMPORT,
            COL_UNKNOWN = data.UNKNOWN,
            COL_MAP = data.MAPCOL;

        $('#SP_CO_EXP').css('color', '#' + COL_EXPORT);
        $('#SP_CO_IMP').css('color', '#' + COL_IMPORT);
        $('#SP_CO_CEXP').css('color', '#' + COL_CEXPORT);
        $('#SP_CO_CIMP').css('color', '#' + COL_CIMPORT);
        $('#SP_CO_GEXP').css('color', '#' + COL_GEXPORT);
        $('#SP_CO_GIMP').css('color', '#' + COL_GIMPORT);
        $('#SP_CO_UNK').css('color', '#' + COL_UNKNOWN);

        /** refresh data */
        var intrv = 0;
        $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_REFTIME'])).done(function(data) {
            if (data.STATE) {
                clearInterval(intrv);
                auto_refresh_data(data.REFSEC);
            } else {
                clearInterval(intrv);
                $('#nex_refresh_btn').html('<i class="icon-alarm-cancel position-left"></i> ARRÊTER');
            }
        });

        function auto_refresh_data(secondsBetweenActions) {
            var secondsRemaining = secondsBetweenActions;
            intrv = setInterval(function() {
                $('#nex_refresh_btn').html(secondsRemaining);
                secondsRemaining--;
                if (secondsRemaining <= 0) {
                    //console.log('doAction()');
                    $('#TAB_DACTIV').DataTable().ajax.reload();
                    $('#TAB_DACTIV_UNK').DataTable().ajax.reload();
                    load_last_doss_events();
                    create_chart();
                    secondsRemaining = secondsBetweenActions;
                }
            }, 1000);
        }
        $('#nex_refresh_btn').on('click', function() {
            $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_REFTIME'])).done(function(data) {
                if (data.STATE) {
                    $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', 'STOP']));
                } else {
                    $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', 'START']))
                }
            }).always(function() {
                $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', 'START'])).done(function() {
                    $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_REFTIME'])).done(function(data) {
                        if (data.STATE) {
                            clearInterval(intrv);
                            auto_refresh_data(data.REFSEC);
                        } else {
                            clearInterval(intrv);
                        }
                    });
                });
            });
        });
        $("#set_ref_options li a").click(function() {
            if ($(this).attr('opt') == 'start') {
                $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', 'START']));
            } else if ($(this).attr('opt') == 'stop') {
                $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', 'STOP']));
            } else {
                $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['SET_REFTIME', $(this).attr('opt')]));
            }
            $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_REFTIME'])).done(function(data) {
                if (data.STATE) {
                    clearInterval(intrv);
                    auto_refresh_data(data.REFSEC);
                } else {
                    clearInterval(intrv);
                    $('#nex_refresh_btn').html('<i class="icon-alarm-cancel position-left"></i> ARRÊTER');
                }
            });
        });
        /**End */

        function GC_extend(params) {
            params === null || params === undefined ? params = '' : console.log();
            switch (params.toUpperCase()) {
                case 'C':
                    return 'MARITIME COMPLET';
                case 'G':
                    return 'MARITIME GROUPAGE';
                default:
                    return 'AERIEN';
            }
        }

        function Gen_Color_Marker(ImEx, GCv) {
            GCv === null || GCv === undefined ? GCv = '' : console.log();
            ImEx === null || ImEx === undefined ? ImEx = '' : console.log();
            switch (GCv.toUpperCase() + ImEx.toUpperCase()) {
                case 'EXPORT':
                    return '#' + COL_EXPORT;
                case 'IMPORT':
                    return '#' + COL_IMPORT;
                case 'CEXPORT':
                    return '#' + COL_CEXPORT;
                case 'CIMPORT':
                    return '#' + COL_CIMPORT;
                case 'GEXPORT':
                    return '#' + COL_GEXPORT;
                case 'GIMPORT':
                    return '#' + COL_GIMPORT;
                default:
                    return '#' + COL_UNKNOWN;
            }
        }

        function gen(min, max) {
            return max - Math.random() * (max - min);
        }

        function get_html(indx, object, spd) {
            for (var key in object) {
                for (var key2 in object[key]) {
                    if (object[key][key2].index == indx) {
                        if (spd !== undefined) {
                            return object[key][key2][spd];
                        } else {
                            return object[key][key2].html;
                        }
                    }
                }
            }
        }

        mapObj = new jvm.Map({
            container: $('.map-choropleth'),
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            scaleColors: ['#C8EEFF', '#0071A4'],
            normalizeFunction: 'polynomial',
            regionStyle: {
                initial: {
                    fill: '#' + COL_MAP
                }
            },
            series: {
                markers: [{
                    attribute: 'fill',
                    scale: {
                        'A.EXPORT': '#' + COL_EXPORT,
                        'A.IMPORT': '#' + COL_IMPORT,
                        'M.C.EXPORT': '#' + COL_CEXPORT,
                        'M.G.EXPORT': '#' + COL_GEXPORT,
                        'M.C.IMPORT': '#' + COL_CIMPORT,
                        'M.G.IMPORT': '#' + COL_GIMPORT,
                        'INCONNUE': '#' + COL_UNKNOWN
                    },
                    legend: {
                        horizontal: true,
                        title: 'Légendes'
                    }
                }]
            },
            onMarkerTipShow: function(event, label, index) {
                label.html(get_html(index, data2));
                label.css("overflow-y", "scroll");
                label.css("max-height", "150px");
            },
            onMarkerClick: function(events, index) {
                $('#hkjk').empty();
                $('#hkjk').html(get_html(index, data2));
                $('#qSZ').html('DOSSIER ' + get_html(index, data2, 'DTYP').toUpperCase());
                var col = get_html(index, data2, 'COL');
                $('#MAP_MODAL').find('.modal-header').css('background-color', col).css('border-color', col).css('color', '#fff');
                $('#hkjk').find('span').css('background-color', col).css('border-color', col).css('color', '#fff');
                $('#MAP_MODAL').modal('show');
            },
        });
        var data2 = [];
        var co = 0;
        var sp_co_tot = 0;

        $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['MAP_INFO'])).done(function(data) {
            data.forEach(function(element) {
                if (data2.hasOwnProperty(element.PO_CODE)) {
                    var fd = false,
                        v = '';
                    data2[element.PO_CODE].forEach(function(el) {
                        element.DM_CODE_COMP_GROUP === null || element.DM_CODE_COMP_GROUP === undefined ? element.DM_CODE_COMP_GROUP = '' : console.log();
                        element.DM_IMP_EXP === null || element.DM_IMP_EXP === undefined ? element.DM_IMP_EXP = '' : console.log();
                        el.CG === null || el.CG === undefined ? el.CG = '' : console.log();
                        el.ImEx === null || el.ImEx === undefined ? el.ImEx = '' : console.log();
                        if (el.CG.toUpperCase() === element.DM_CODE_COMP_GROUP.toUpperCase() && el.ImEx.toUpperCase() === element.DM_IMP_EXP.toUpperCase()) {
                            fd = true;
                            v = el.index;
                            return;
                        }
                    }, this);
                    if (fd) {
                        data2[element.PO_CODE][v].html += '<span class="label bg-grey label-block"><i class="icon-arrow-down12"></i></span><br/><b>POD : </b>' + element.POD_LIBELLE + ' | <b> <i class="icon-move-right"></i> </b>' + element.POD_PAYS + '<br/>' +
                            '<b>POL : </b>' + element.POL_LIBELLE + ' | <b> <i class="icon-move-right"></i> </b>' + element.POL_PAYS + '<br/>' +
                            '<b>Navire : </b>' + element.NA_LIBELLE + '</br>' +
                            '<b>N° Dos : </b><a target="_blank" href="/GTRANS/public/Dossier/?NumD=' + element.DM_NUM_DOSSIER + '">' + element.DM_NUM_DOSSIER + '</a></br>' +
                            '<b>Date Décharg : </b>' + element.DM_DATE_DECHARG + '</br>';
                    } else {
                        data2[element.PO_CODE][co] = {
                            index: co,
                            COL: Gen_Color_Marker(element.DM_IMP_EXP, element.DM_CODE_COMP_GROUP),
                            DTYP: GC_extend(element.DM_CODE_COMP_GROUP) + ' ' + element.DM_IMP_EXP,
                            ImEx: element.DM_IMP_EXP,
                            CG: element.DM_CODE_COMP_GROUP,
                            html: '<b>POD : </b>' + element.POD_LIBELLE + ' | <b> <i class="icon-move-right"></i> </b>' + element.POD_PAYS + '<br/>' +
                                '<b>POL : </b>' + element.POL_LIBELLE + ' | <b> <i class="icon-move-right"></i> </b>' + element.POL_PAYS + '<br/>' +
                                '<b>Navire : </b>' + element.NA_LIBELLE + '</br>' +
                                '<b>N° Dos : </b><a target="_blank" href="/GTRANS/public/Dossier/?NumD=' + element.DM_NUM_DOSSIER + '">' + element.DM_NUM_DOSSIER + '</a></br>' +
                                '<b>Date Décharg : </b>' + element.DM_DATE_DECHARG + '</br>',
                            name: 'PORT : ' + element.POD_LIBELLE,
                            latLng: [Number((Number(element.PO_LAT) + gen(0.1, 0.9)).toFixed(6)), Number((Number(element.PO_LNG) + gen(0.1, 0.9)).toFixed(6))],
                            style: {
                                r: 7,
                                'fill': Gen_Color_Marker(element.DM_IMP_EXP, element.DM_CODE_COMP_GROUP),
                                'fill-opacity': 0.9,
                                'stroke': 'ffffff',
                                'stroke-width': 1.5,
                                'stroke-opacity': 0.9
                            }
                        };
                        co++;
                    }
                } else {
                    data2[element.PO_CODE] = [];
                    data2[element.PO_CODE][co] = {
                        index: co,
                        COL: Gen_Color_Marker(element.DM_IMP_EXP, element.DM_CODE_COMP_GROUP),
                        DTYP: GC_extend(element.DM_CODE_COMP_GROUP) + ' ' + element.DM_IMP_EXP,
                        ImEx: element.DM_IMP_EXP,
                        CG: element.DM_CODE_COMP_GROUP,
                        html: '<b>POD : </b>' + element.POD_LIBELLE + ' <b> <i class="icon-move-right"></i> </b>' + element.POD_PAYS + '<br/>' +
                            '<b>POL : </b>' + element.POL_LIBELLE + ' | <b> <i class="icon-move-right"></i> </b>' + element.POL_PAYS + '<br/>' +
                            '<b>Navire : </b>' + element.NA_LIBELLE + '</br>' +
                            '<b>N° Dos : </b><a target="_blank" href="/GTRANS/public/Dossier/?NumD=' + element.DM_NUM_DOSSIER + '">' + element.DM_NUM_DOSSIER + '</a></br>' +
                            '<b>Date Décharg : </b>' + element.DM_DATE_DECHARG + '</br>',
                        name: 'PORT : ' + element.POD_LIBELLE,
                        latLng: [Number(element.PO_LAT), Number(element.PO_LNG)],
                        style: {
                            r: 7,
                            'fill': Gen_Color_Marker(element.DM_IMP_EXP, element.DM_CODE_COMP_GROUP),
                            'fill-opacity': 0.9,
                            'stroke': 'ffffff',
                            'stroke-width': 1.5,
                            'stroke-opacity': 0.9
                        }
                    };
                    co++;
                }
            }, this);
        }).always(function(data) {
            data2.forEach(function(element2) {
                element2.forEach(function(element3) {
                    mapObj.addMarker(element3.index, {
                        latLng: element3.latLng,
                        style: element3.style
                    });
                }, this);
            }, this);
            $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['MAP_INFO_CO'])).done(function(data) {
                data.forEach(function(element) {
                    sp_co_tot += Number(element.CO)
                    element.DM_CODE_COMP_GROUP === null || element.DM_CODE_COMP_GROUP === undefined ? element.DM_CODE_COMP_GROUP = '' : console.log();
                    element.DM_IMP_EXP === null || element.DM_IMP_EXP === undefined ? element.DM_IMP_EXP = '' : console.log();
                    switch (element.DM_CODE_COMP_GROUP + element.DM_IMP_EXP) {
                        case 'EXPORT':
                            $('#SP_CO_EXP_H').text(element.CO);
                            break;
                        case 'IMPORT':
                            $('#SP_CO_IMP_H').text(element.CO);
                            break;
                        case 'CEXPORT':
                            $('#SP_CO_CEXP_H').text(element.CO);
                            break;
                        case 'GEXPORT':
                            $('#SP_CO_GEXP_H').text(element.CO);
                            break;
                        case 'CIMPORT':
                            $('#SP_CO_CIMP_H').text(element.CO);
                            break;
                        case 'GIMPORT':
                            $('#SP_CO_GIMP_H').text(element.CO);
                            break;
                        default:
                            $('#SP_CO_UNK').text(element.CO);
                            break;
                    }
                }, this);
                $('#SP_CO_TOT').text('Dossiers pas encore cloturés : ' + sp_co_tot);
            }).always(function(data) {
                var TAB_DACTIV = $('#TAB_DACTIV').DataTable({
                    buttons: {
                        buttons: [{
                                extend: 'print',
                                className: 'btn bg-blue btn-icon',
                                text: '<i class="icon-printer position-left"></i>',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                extend: 'colvis',
                                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                                className: 'btn bg-blue btn-icon'
                            }
                        ],
                    },
                    select: false,
                    destroy: true,
                    scrollY: false,
                    scrollX: false,
                    ajax: {
                        url: "/GTRANS/public/Dossier/api/tdb.php",
                        type: "POST",
                        data: function(d) {
                            return JSON.stringify({
                                "0": "MAP_INFO_TAB"
                            });
                        }
                    },
                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                    columns: [
                        { "data": "DM_CLE" },
                        {
                            "data": "DM_CODE_COMP_GROUP",
                            "width": "15%",
                            "render": function(data, type, row) {
                                var col = Gen_Color_Marker(row.DM_IMP_EXP, row.DM_CODE_COMP_GROUP);
                                var ty = GC_extend(row.DM_CODE_COMP_GROUP) + ' ' + row.DM_IMP_EXP;
                                var icomo = (row.DM_CODE_COMP_GROUP == '' || row.DM_CODE_COMP_GROUP == null) ? 'fa fa-plane' : 'fa fa-ship';
                                var pb = '';
                                var start = new Date(row.C_DM_DATE_EMBARQ), // Jan 1, 2015
                                    end = new Date(row.C_DM_DATE_DECHARG), // June 24, 2015
                                    today = new Date(); // April 23, 2015
                                var v = 100;
                                var p = 100 + '%';
                                if ((end - start) > 0) {
                                    v = Math.round(((today - start) / (end - start)) * 100);
                                    p = Math.round(((today - start) / (end - start)) * 100) + '%';
                                }

                                // console.log(v)
                                if (v === Infinity || isNaN(v) || v < 0) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%;background-color: #000000;"></div></div>';
                                } else if (v > 0 && v <= 50) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-success progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 50 && v <= 75) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 75 && v <= 100) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-warning progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 100) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%"></div></div>';
                                } else {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                }
                                //return '<span class="label label-flat bg-grey label-block" style="border-color: ' + col + ';background-color: ' + col + ';">' + ty + '</span>';
                                var html = '<ul class="list list-unstyled no-margin">\
                                                <li class="no-margin text-bold">\
                                                    <i class="' + icomo + ' text-size-base position-left"  style="color: ' + col + ';"></i> ' + ty + '\
                                                </li>\
                                                <li class="no-margin">\
                                                    ' + pb + '\
                                                </li>\
                                            </ul>'
                                return html;
                            }
                        },
                        {
                            "data": "DM_NUM_DOSSIER",
                            "render": function(data, type, row) {
                                return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                            }
                        },
                        { "data": "NA_LIBELLE" },
                        {
                            "data": "POD_LIBELLE",
                            "render": function(data, type, row) {
                                return '<ul class="list list-unstyled no-margin">\
                                                <li class="no-margin text-bold">\
                                                    <i class="icon-upload4 text-size-base text-warning position-left"></i>\
                                                    POL :\
                                                    <a>' + row.POL_LIBELLE + '</a>\
                                                </li>\
                                                <li class="no-margin">\
                                                    <i class="icon-download4 text-size-base text-success position-left"></i>\
                                                    POD :\
                                                    <a>' + row.POD_LIBELLE + '</a>\
                                                </li>\
                                                <li class="no-margin">\
                                                    <i class="icon-calendar2 text-size-base text-primary position-left"></i>\
                                                    Décharge :\
                                                    <a>' + row.DM_DATE_DECHARG + '</a>\
                                                </li>\
                                            </ul>';
                            }
                        }
                    ],
                    initComplete: function() {
                        var tt_unk = Number(sp_co_tot) - Number(TAB_DACTIV.data().count());
                        //console.log(sp_co_tot + ' - ' + TAB_DACTIV.data().count() + ' = ' + tt_unk)
                        //$('#SP_CO_TOT_UNK').text('TOTAL DOSSIER ACTIFS [?] : ' + tt_unk);
                        $('#SP_BTN_CO_TOT_UNK').text('Dossiers avec des données erronées : ' + tt_unk);
                        $('#SP_BTN_CO_TOT_UNK').on('click', function() {
                            var win = window.open('/GTRANS/public/Dossier/wrngdos', '_blank');
                            win.focus();
                        });
                    },
                });
                var NAV_LIST = new Array();
                var PORT_LIST = new Array();
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['navire'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        NAV_LIST.push(element.NA_CODE);
                    }, this);
                }).always(function() {
                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['port'])).fail(function(data) {
                        console.log('fail', data);
                    }).done(function(data) {
                        data.forEach(function(element) {
                            PORT_LIST.push(element.PO_CODE);
                        }, this);
                    });
                });
            });
        }).fail(function(data) {
            console.log('MAPS_DATA_FAILS : ', data);
        });

        $('#load_last_dos_btn').on('click', function() {
            load_last_doss_events();
        });

        load_last_doss_events();

        create_chart();

        function load_last_doss_events() {
            $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_LAS_ADDED'])).done(function(data) {
                $('#last_add_older').empty();
                data.forEach(element => {
                    var col = Gen_Color_Marker(element.DM_IMP_EXP, element.DM_CODE_COMP_GROUP);
                    var icomo = (element.DM_CODE_COMP_GROUP == '' || element.DM_CODE_COMP_GROUP == null) ? 'icon-airplane3' : 'icon-ship';
                    var attrna = (element.DM_ATTRIBUER_LIB == '' || element.DM_ATTRIBUER_LIB == null) ? 'Indéfini' : element.DM_ATTRIBUER_LIB;
                    $('#last_add_older').append('<li class="media">\
                    <div class="media-left">\
                        <a target="_blank" href="/GTRANS/public/Dossier/?NumD=' + element.DM_NUM_DOSSIER + '" class="btn btn-flat btn-icon btn-rounded btn-sm" style="color: ' + col + ';border-color: ' + col + ';">\
                            <i class="' + icomo + '"></i>\
                        </a>\
                    </div>\
                    <div class="media-body">\
                        <ul class="list list-unstyled no-margin">\
                            <li class="no-margin">\
                                <i class="icon-folder text-size-base text-teal position-left"></i>\
                                <a class="text-teal" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + element.DM_NUM_DOSSIER + '">' + element.DM_NUM_DOSSIER + '</a>\
                            </li>\
                            <li class="no-margin">\
                                <i class="icon-upload4 text-size-base text-warning position-left"></i>\
                                POL : \
                                <a>' + element.POL_LIBELLE + '</a>\
                                <i class="icon-arrow-right8 text-size-base position-left"></i>\
                                POD : \
                                <a>' + element.POD_LIBELLE + '</a>\
                            </li>\
                            <li class="no-margin">\
                                <i class="icon-user text-size-base text-indigo position-left"></i>\
                                <a href="#" class="text-indigo">' + attrna + '</a>\
                                <div class="media-annotation"><span data-livestamp="' + element.DM_DATE_CREATION + '"></span></div>\
                            </li>\
                        </ul>\
                    </div>\
                </li>');
                });
            }).always(function() {
                $('#last_add_older_ego').livestamp(new Date());
            });
        }
    });

    function create_chart() {
        var currentYear = (new Date).getFullYear();
        $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['CHART_DEP_DATA', currentYear]))
            .done(function(result1) {
                $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['CHART_DEP_DATA', currentYear - 1]))
                    .done(function(result2) {
                        $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['CHART_DEP_DATA', currentYear - 2]))
                            .done(function(result3) {
                                var dp1 = [];
                                var dp2 = [];
                                var dp3 = [];
                                for (var i = 0; i < result1.length; i++) {
                                    var data = result1[i].x;
                                    var xd = String(data);

                                    dp1.push({ x: new Date(xd), y: result1[i].y })
                                }
                                for (var i = 0; i < result2.length; i++) {
                                    var data = result2[i].x;
                                    var xd = String(data);

                                    dp2.push({ x: new Date(xd), y: result2[i].y })
                                }
                                for (var i = 0; i < result3.length; i++) {
                                    var data = result3[i].x;
                                    var xd = String(data);

                                    dp3.push({ x: new Date(xd), y: result3[i].y })
                                }
                                var chart = new CanvasJS.Chart("Dep_chart", {
                                    exportEnabled: true,
                                    animationEnabled: true,
                                    title: {
                                        text: "Dépense Graphe " + currentYear
                                    },
                                    axisX: {
                                        valueFormatString: "MMMM"
                                    },
                                    axisY: {
                                        title: "Dépense",
                                        includeZero: false,
                                        /*scaleBreaks: {
                                            autoCalculate: true
                                        },*/
                                        suffix: " DT"
                                    },
                                    toolTip: {
                                        shared: true
                                    },
                                    legend: {
                                        cursor: "pointer",
                                        itemclick: toggleDataSeries
                                    },
                                    data: [{
                                            type: "line",
                                            name: (currentYear - 2).toString(),
                                            xValueFormatString: "MMMM",
                                            color: '#43A047',
                                            showInLegend: true,
                                            dataPoints: dp3
                                        },
                                        {
                                            type: "line",
                                            name: (currentYear - 1).toString(),
                                            xValueFormatString: "MMMM",
                                            color: "#369EAD",
                                            showInLegend: true,
                                            dataPoints: dp2
                                        }, {
                                            type: "line",
                                            name: currentYear.toString(),
                                            xValueFormatString: "MMMM",
                                            color: "#F08080",
                                            showInLegend: true,
                                            dataPoints: dp1
                                        }
                                    ]
                                });
                                chart.render();

                                function toggleDataSeries(e) {
                                    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                        e.dataSeries.visible = false;
                                    } else {
                                        e.dataSeries.visible = true;
                                    }
                                    e.chart.render();
                                }
                            });
                    });
            });
    }
});