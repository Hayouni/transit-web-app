$(document).ready(function () {

    /**Attribute  */
    $('#BTN_ATT_U_ADD_MIC').on('click', function () {
        $.post('/GTRANS/sys/session.php').fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_MIC').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_MIC').val(info.user_name);
        });
    });

    var TAB_MIC1 = $('#TAB_MIC1').DataTable({
        destroy: true,
        empty: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });
    var TAB_MIC2 = $('#TAB_MIC2').DataTable({
        destroy: true,
        empty: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    $('#clients_MIC').on('change', function (e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            data.length !== 0 ? $('#clients_ADR_MIC').val(data[0].CL_ADRESSE) : $('#clients_ADR_MIC').val('');
        });
    });

    $.Create_MC1MC2_TAB = function () {
        TAB_MIC1 = $('#TAB_MIC1').DataTable({
                destroy: true,
                paging: false,
                empty: true,
                ajax: {
                    url: "/GTRANS/public/Dossier/api/dossier.php",
                    type: "POST",
                    data: function (d) {
                        return JSON.stringify({
                            "0": "GET_DOS_COMP_DATA_MARCH",
                            "1": $('#DOS_NUM_INP').val()
                        });
                    },
                },
                aoColumnDefs: [{
                    "sClass": "hide_me",
                    "aTargets": [0]
                }],
                columns: [{
                        "data": "DM_CODE"
                    },
                    {
                        "data": "DM_MARCHANDISE"
                    },
                    {
                        "data": "DM_POID"
                    },
                    {
                        "data": "DM_RUBRIQUE"
                    },
                    {
                        "data": "DM_NBR"
                    }
                ]
            }).on('select', function (e, dt, type, indexes) {
                var rowData = $('#TAB_MIC1').DataTable().rows(indexes).data().toArray();
                $('#MARC_NAME_C').html('MARCHANDISE : ' + rowData[0].DM_MARCHANDISE);
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_MARCH_SELECT', rowData[0].DM_CODE])).fail(function (data) {
                    console.log('fail', data);
                }).done(function (data) {
                    TAB_MIC2 = $('#TAB_MIC2').DataTable({
                        select: {
                            style: 'single'
                        },
                        empty: true,
                        destroy: true,
                        paging: false,
                        aoColumnDefs: [{
                            "sClass": "hide_me",
                            "aTargets": [0]
                        }],
                        columns: [{
                                "data": "DC_CODE"
                            },
                            {
                                "data": "DC_CONTAINER"
                            },
                            {
                                "data": "DC_NUM_CONTAINER"
                            },
                            {
                                "data": "DC_RUBRIQUE"
                            },
                            {
                                "data": "DC_POID"
                            }
                        ],
                        ajax: {
                            url: "/GTRANS/public/Dossier/api/dossier.php",
                            type: "POST",
                            data: function (d) {
                                return JSON.stringify({
                                    "0": "GET_DOS_COMP_DATA_CONT",
                                    "1": rowData[0].DM_CODE
                                });
                            },
                        }
                    }).on('select', function (e, dt, type, indexes) {
                        var rowData2 = $('#TAB_MIC2').DataTable().rows(indexes).data().toArray();
                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_CONT_SELECT', rowData2[0].DC_CODE])).fail(function (data) {
                            console.log('fail', data);
                        }).done(function (data) {
                            $('#VOLUM_MIC').val(data[0].DC_CONTAINER);
                            $('#Numero_MICC').val(data[0].DC_NUM_CONTAINER);
                            $('#Rubrq_MICC').val(data[0].DC_RUBRIQUE);
                            $('#PoidsV_MIC').val(data[0].DC_POID);
                        });
                    }).columns.adjust().draw();
                    $('#MARCH_MIC').val(data[0].DM_MARCHANDISE);
                    $('#Rubr_MIC').val(data[0].DM_RUBRIQUE);
                    $('#Nombre_MIC').val(data[0].DM_NBR);
                    $('#Poids_MIC').val(data[0].DM_POID);
                });
            })
            .on('deselect', function (e, dt, type, indexes) {
                var rowData = $('#TAB_MIC1').DataTable().rows(indexes).data().toArray();
                $('#MARC_NAME_C').html('');
                $('#TAB_MIC2').DataTable().clear().draw();
            }).columns.adjust().draw();

    }

    $('#MOD_MICM').on('click', function () {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP_MARCH', {
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_NBR': $('#Nombre_MIC').val(),
            'DM_POID': $('#Poids_MIC').val(),
        }, dataArr, {
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_POIDS': $('#Poids_MIC').val(),
        }, $('#DOS_NUM_INP').val()])).fail(function (data) {
            console.log('fail');
            console.dir(data);
        }).done(function (data) {
            $('#TAB_MIC1').DataTable().ajax.reload();
            $('#MARCH_MIC').val('');
            $('#Rubr_MIC').val('');
            $('#Nombre_MIC').val('');
            $('#Poids_MIC').val('');
        });
    });

    $('#ADD_MICM').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP_MARCH', {
            'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_NBR': $('#Nombre_MIC').val(),
            'DM_POID': $('#Poids_MIC').val(),
        }, {
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_POIDS': $('#Poids_MIC').val(),
        }, $('#DOS_NUM_INP').val()])).fail(function (data) {
            console.log('fail');
            console.dir(data);
        }).done(function (data) {
            //$('#TAB_MIC1').DataTable().ajax.reload();
            $.Create_MC1MC2_TAB()
            $('#MARCH_MIC').val('');
            $('#Rubr_MIC').val('');
            $('#Nombre_MIC').val('');
            $('#Poids_MIC').val('');
        });
    });

    $('#DEL_MICM').on('click', function () {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_CONT', dataArr]))
            .fail(function (error) {
                console.log('fail', error);
            })
            .done(function (data) {
                //console.log('length :' + Object.keys(data.data).length);
                if (Object.keys(data.data).length) {
                    swal({
                            title: "Marchandise non vide !",
                            text: 'Êtes-vous sûr de supprimer cette marchandise et ses conteneurs ?',
                            type: "info",
                            showCancelButton: true,
                            closeOnConfirm: false,
                            confirmButtonColor: "#2196F3",
                            showLoaderOnConfirm: true
                        },
                        function () {
                            DEL_MARCH_COMPLET(dataArr);
                            $.PNotify_Alert('Marchandise', 'La suppression a été réussie', 'success');
                        });
                } else {
                    DEL_MARCH_COMPLET(dataArr, true);
                    $.PNotify_Alert('Marchandise', 'La suppression a été réussie', 'success');
                }
            });
    });

    function DEL_MARCH_COMPLET(key, empty) {
        empty = (typeof empty !== 'undefined') ? empty : false;
        //console.log(empty + ' -- ' + key);
        //return true;
        switch (empty) {
            case false:
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_CONT_OF_MARCH', key]))
                    .fail(function (error) {
                        console.log('fail', error);
                    })
                    .done(function (data) {
                        $('#TAB_MIC1').DataTable().ajax.reload();
                        $('#MARCH_MIC').val('');
                        $('#Rubr_MIC').val('');
                        $('#Nombre_MIC').val('');
                        $('#Poids_MIC').val('');
                        $('#TAB_MIC2').DataTable().ajax.reload();
                        $('#VOLUM_MIC').val('');
                        $('#Numero_MICC').val('');
                        $('#Rubrq_MICC').val('');
                        $('#PoidsV_MIC').val('');
                    });
                break;

            default:
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_EMPTY_MARCH', key]))
                    .fail(function (error) {
                        console.log('fail', error);
                    })
                    .done(function (data) {
                        $('#TAB_MIC1').DataTable().ajax.reload();
                        $('#MARCH_MIC').val('');
                        $('#Rubr_MIC').val('');
                        $('#Nombre_MIC').val('');
                        $('#Poids_MIC').val('');
                        $('#TAB_MIC2').DataTable().ajax.reload();
                        $('#VOLUM_MIC').val('');
                        $('#Numero_MICC').val('');
                        $('#Rubrq_MICC').val('');
                        $('#PoidsV_MIC').val('');
                    });
                break;
        }
        setTimeout(function () {
            swal({
                title: "terminée",
                confirmButtonColor: "#2196F3"
            });
        }, 2000);

    }

    $('#MOD_MICC').on('click', function () {
        var dataArr;
        $.each($("#TAB_MIC2 tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP_CONT', {
            'DC_CONTAINER': $('#VOLUM_MIC').val(),
            'DC_NUM_CONTAINER': $('#Numero_MICC').val(),
            'DC_RUBRIQUE': $('#Rubrq_MICC').val(),
            'DC_POID': $('#PoidsV_MIC').val(),
        }, dataArr])).fail(function (error) {
            console.log('fail', error);
        }).done(function (data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    $('#ADD_MICC').on('click', function () {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP_CONT', {
            'DC_CODE_MARCHANDISE': dataArr,
            'DC_CONTAINER': $('#VOLUM_MIC').val(),
            'DC_NUM_CONTAINER': $('#Numero_MICC').val(),
            'DC_RUBRIQUE': $('#Rubrq_MICC').val(),
            'DC_POID': $('#PoidsV_MIC').val(),
        }])).fail(function (error) {
            console.log('fail', error);
        }).done(function (data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    $('#DEL_MICC').on('click', function () {
        var dataArr;
        $.each($("#TAB_MIC2 tr.selected"), function () {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_CONT', dataArr])).fail(function (error) {
            console.log('fail', error);
        }).done(function (data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    $('#BTN_ADD_MIC').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP', {
            'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
            'DM_FOURNISSEUR': $('#fournisseurs_MIC').val(),
            'DM_CLIENT': $('#clients_MIC').val(),
            'DM_PLACE_DELIVERY': $('#clients_PC_MIC').val(),
            'DM_NUM_BL': $('#BL_MIC').val(),
            'DM_NAVIRE': $('#navire_MIC').val(),
            'DM_POL': $('#port_DEP_MIC').val(),
            'DM_POD': $('#port_ARR_MIC').val(),
            'DM_DATE_EMBARQ': $('#Date_Dep_MIC').val(),
            'DM_DATE_DECHARG': $('#Date_arr_MIC').val(),
            'DM_NOMBRE': $('#NBR_MIC').val(),
            'DM_NBR_UNITE_MARCH': $('#Nombre_MIC').val(),
            'DM_TERME': $('#term_MIC').val(),
            'DM_MARQUE': $('#marq_MIC').val(),
            'DM_ESCALE': $('#escal_MIC').val(),
            'DM_VAL_DEVISE': $('#VAL_DEV_MIC').val(),
            'DM_CODE_COMP_GROUP': $('select#SEL_CG_DOS').val(),
            'DM_IMP_EXP': $('select#SEL_T_DOS').val(),
            'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIC').attr("user"),
            'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIC').val()
        }])).fail(function (error) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'L\'ajout au dossier a échoué', 'danger');
            console.dir('fail', error);
        }).done(function (data) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Ajouter au dossier avec succès', 'success');
        });
    });

    $('#BTN_MOD_MIC').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP', {
            'DM_FOURNISSEUR': $('#fournisseurs_MIC').val(),
            'DM_CLIENT': $('#clients_MIC').val(),
            'DM_PLACE_DELIVERY': $('#clients_PC_MIC').val(),
            'DM_NUM_BL': $('#BL_MIC').val(),
            'DM_NAVIRE': $('#navire_MIC').val(),
            'DM_POL': $('#port_DEP_MIC').val(),
            'DM_POD': $('#port_ARR_MIC').val(),
            'DM_DATE_EMBARQ': $('#Date_Dep_MIC').val(),
            'DM_DATE_DECHARG': $('#Date_arr_MIC').val(),
            'DM_NOMBRE': $('#NBR_MIC').val(),
            'DM_TERME': $('#term_MIC').val(),
            'DM_MARQUE': $('#marq_MIC').val(),
            'DM_ESCALE': $('#escal_MIC').val(),
            'DM_VAL_DEVISE': $('#VAL_DEV_MIC').val(),
            'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIC').attr("user"),
            'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIC').val()
        }, $('#DOS_NUM_INP').val()])).fail(function (error) {
            console.log('fail', error);
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'La modification du dossier n\'a pas abouti', 'danger');
        }).done(function (data) {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Dossier modifié avec succès', 'success');
        });
    });

    $('#BTN_CLOT_MIC').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CLOT_DOS', $('#DOS_NUM_INP').val()]))
            .done(function (data) {
                if (data[0].DM_CLOTURE == "0") {
                    $.CLOT_ME()
                } else {
                    $.UNCLOT_ME()
                }
                //console.log(data)
            });
    });

    /**FACTURATION */

    $('#PAA_MIC').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                        .fail(function (error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement', 'danger');
                        })
                        .done(function (data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Print/viewer.php?id=PAAC&dmcle=' + dmcle + '&nd=' + $('#DOS_NUM_INP').val(), '_blank');
                                win.focus();
                                /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAAC.mrt&dmcle=' + dmcle + ' &nd=' + $('#DOS_NUM_INP').val(), '_blank');
                                win.focus();*/
                            } else {
                                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_PAA', dmcle])).done(function (data) {
                                    //console.log(data);
                                    var win = window.open('/GTRANS/public/Print/viewer.php?id=PAAC&dmcle=' + dmcle + '&nd=' + $('#DOS_NUM_INP').val(), '_blank');
                                    win.focus();
                                    /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAAC.mrt&dmcle=' + dmcle + ' &nd=' + $('#DOS_NUM_INP').val(), '_blank');
                                    win.focus();*/
                                });
                            }
                        });
                })
                .fail(function (error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#AA_MIC').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                        .fail(function (error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function (data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Dossier/fact/AAC?DM_CLE=' + dmcle, '_blank');
                                win.focus();
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                            }
                        });
                })
                .fail(function (error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#INV_MIC').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_INV', dmcle]))
                        .fail(function (error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function (data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Dossier/fact/INV?DM_CLE=' + dmcle, '_blank');
                                win.focus();
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                            }
                        });
                })
                .fail(function (error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#BAD_MIC').on('click', function () {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_HAS_FACT_S', dmcle]))
                        .fail(function (data) {
                            console.log('fail');
                            console.dir(data);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                        })
                        .done(function (data) {
                            if (Object.keys(data).length) {
                                swal({
                                        title: "Bon a delivrer",
                                        text: "Entrez le du magasin",
                                        type: "input",
                                        showCancelButton: true,
                                        confirmButtonColor: "#2196F3",
                                        closeOnConfirm: false,
                                        animation: "slide-from-right",
                                        inputPlaceholder: "..."
                                    },
                                    function (inputValue) {
                                        if (inputValue === false) return false;
                                        if (inputValue === "") {
                                            swal.showInputError("Nom de magasin requi !");
                                            return false
                                        }
                                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_AAM_CODE', dmcle]))
                                            .done(function (data) {
                                                if (data[0].AAM_CODE) {
                                                    var win = window.open('/GTRANS/public/Print/viewer.php?id=CBAD&A=' + dmcle + '&B=' + data[0].AAM_CODE + '&D=' + $('#DOS_NUM_INP').val() + '&N=' + inputValue, '_blank');
                                                    win.focus();
                                                    /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=CBAD.mrt&A=' + dmcle + ' &B=' + data[0].AAM_CODE + ' &D=' + $('#DOS_NUM_INP').val() + ' &N=' + inputValue, '_blank');
                                                    win.focus();*/
                                                    swal({
                                                        title: 'Demande effectuée',
                                                        text: 'attendre la redirection vers le rapport...',
                                                        type: "success",
                                                        timer: 2000,
                                                        confirmButtonColor: "#2196F3"
                                                    });
                                                } else {
                                                    swal({
                                                        title: "Error !",
                                                        text: "aucun avis d'arrivé trouvé !",
                                                        confirmButtonColor: "#EF5350",
                                                        type: "error"
                                                    });
                                                }
                                            })
                                            .fail(function (error) {
                                                swal({
                                                    title: "Error !",
                                                    text: "demande échoué !",
                                                    confirmButtonColor: "#EF5350",
                                                    type: "error"
                                                });
                                                console.log('fail', error)
                                            });
                                    });
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune Facture sur ce dossier !', 'warning');
                            }
                        });
                })
                .fail(function (error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'danger');
                });
        } else {
            $.PNotify_Alert('Erreur', "Numéro de dossier non valide !", 'warning');
        }
    });

    $('#FSURS_MICM').on('click', function () {
        if (TAB_MIC2.rows('.selected').any()) {
            var dcnum = TAB_MIC2.rows('.selected').data()[0].DC_NUM_CONTAINER;
            var dcvol = TAB_MIC2.rows('.selected').data()[0].DC_CONTAINER;
            var dcc = TAB_MIC2.rows('.selected').data()[0].DC_CODE;
            var dcpoi = TAB_MIC2.rows('.selected').data()[0].DC_POID;
            var dmnum = TAB_MIC1.rows('.selected').data()[0].DM_MARCHANDISE;
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DM_CLE', $('#DOS_NUM_INP').val()]))
                .done(function (data) {
                    var dmcle = data[0].DM_CLE;
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_HAS_FACT_S', dmcle, 'S', dcnum]))
                        .fail(function (error) {
                            console.log('fail', error);
                            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'la date de restitution pas encore ajouté, il faut l\'ajouter a partir [Conteneur Client]', 'warning');
                        })
                        .done(function (data) {
                            if (Object.keys(data).length) {
                                var win = window.open('/GTRANS/public/Dossier/fact/SU?DM_CLE=' + dmcle + '&CN=' + dcnum + '&CC=' + dcc, '_blank');
                                win.focus();
                            } else {
                                $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                            }
                        });
                })
                .fail(function (error) {
                    console.log('fail', error);
                    $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'An error auccured !', 'error');
                });
        } else {
            $.PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
        }
    });

    $('#CONS_MIC').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CONT_LIST', $('#DOS_NUM_INP').val()]))
            .done(function (data) {
                var valCont = '-';
                var total_conD = 0;
                var ttdb = '';
                var cvollistdb = '';
                $('#tbosyconslist').empty();
                data.forEach(element => {
                        valCont = N2L(Number(element.CV_VALUE)) + ' (' + element.CV_VALUE + ' DT)';
                        total_conD += Number(element.CV_VALUE);
                        if (!~cvollistdb.indexOf(element.DC_CONTAINER)) {
                            cvollistdb += ' ' + Number(element.CV_VALUE) + ' DT/' + element.DC_CONTAINER
                        }
                        $('#tbosyconslist').append('<tr><td>' +
                            element.DC_NUM_CONTAINER + '</td><td>' +
                            element.DC_CONTAINER + '</td><td>' +
                            valCont + '</td></tr>');
                    }),
                    ttdb = N2L(Number(total_conD).toFixed(3)) + ' (' + Number(total_conD).toFixed(3) + ' DT)',
                    $('#tbosyconslist').append('<tr><td> Total </td><td colspan=2>' + ttdb + '</td></tr>'),
                    $('#modal_Consign').modal('show'),
                    $('#NEXT_CONSI').on('click', function () {
                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                                'DM_CONSI_TOTLIB': ttdb,
                                'DM_CON_CONSI_LIB': cvollistdb
                            }, $('#DOS_NUM_INP').val()]))
                            .fail(function (data) {
                                $.PNotify_Alert('Consignation', 'Une erreur s\'est produite lors du traitement', 'warning');
                            }).done(function (data) {
                                $('#modal_Consign').modal('hide');
                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_REGL_CONSI', $('#DOS_NUM_INP').val()]))
                                    .done(function (data) {
                                        switch (data[0].DM_CONSI_REGLEMENT) {
                                            case 'BC':
                                                $('.nav-tabs a[href="#BC"]').tab('show');
                                                break;
                                            case 'ESPECE':
                                                $('.nav-tabs a[href="#ESPECE"]').tab('show');
                                                $('#INP_F_ESPECE_DATE').val(data[0].DM_CONSI_DATE);
                                                break;
                                            case 'CHEQUE':
                                                $('.nav-tabs a[href="#CHEQUE"]').tab('show');
                                                $('#INP_F_CHEQUE_Num').val(data[0].DM_CONSI_NUM);
                                                $('#INP_F_CHEQUE_Banq').val(data[0].DM_CONSI_BANQUE);
                                                $('#INP_F_CHEQUE_DATE').val(data[0].DM_CONSI_DATE);
                                                break;
                                            case 'TRAITE':
                                                $('.nav-tabs a[href="#TRAITE"]').tab('show');
                                                $('#INP_F_TRAITE_Num').val(data[0].DM_CONSI_NUM);
                                                $('#INP_F_TRAITE_Banq').val(data[0].DM_CONSI_BANQUE);
                                                $('#INP_F_TRAITE_DATE').val(data[0].DM_CONSI_DATE);
                                                break;
                                            case 'VIREMENT':
                                                $('.nav-tabs a[href="#VIREMENT"]').tab('show');
                                                $('#INP_F_VIREMENT_Num').val(data[0].DM_CONSI_NUM);
                                                $('#INP_F_VIREMENT_Banq').val(data[0].DM_CONSI_BANQUE);
                                                $('#INP_F_VIREMENT_DATE').val(data[0].DM_CONSI_DATE);
                                                break;

                                            default:
                                                break;
                                        }
                                        $('#modal_Reglement').modal('show');
                                    });
                            });
                    });
            });
    });

    $('#PRINT_CONSI').on('click', function () {
        var win = window.open('/GTRANS/public/Print/viewer.php?id=CONSIGNE&A=' + $('#DOS_NUM_INP').val(), '_blank');
        win.focus();

        /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=CONSIGNE.mrt&A=' + $('#DOS_NUM_INP').val(), '_blank');
        win.focus();*/
    });

    $('#REG_BTN_CONSI').on('click', function () {
        var sel = $("ul.nav-tabs li.active a").attr('href');
        switch (sel) {
            case '#BC':
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'BC',
                        'DM_CONSI_NUM': '',
                        'DM_CONSI_BANQUE': '',
                        'DM_CONSI_DATE': '',
                        'DM_CONSI_LIB': ''
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                        $.PNotify_Alert('Règlement', 'Règlement effectué avec succès', 'success');
                    });
                break;
            case '#ESPECE':
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'ESPECE',
                        'DM_CONSI_DATE': $('#INP_F_ESPECE_DATE').val(),
                        'DM_CONSI_NUM': '',
                        'DM_CONSI_BANQUE': '',
                        'DM_CONSI_LIB': ''
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                        $.PNotify_Alert('Règlement', 'Règlement effectué avec succès', 'success');
                    });
                break;

            case '#CHEQUE':
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'CHEQUE',
                        'DM_CONSI_NUM': $('#INP_F_CHEQUE_Num').val(),
                        'DM_CONSI_BANQUE': $('#INP_F_CHEQUE_Banq').val(),
                        'DM_CONSI_DATE': $('#INP_F_CHEQUE_DATE').val(),
                        'DM_CONSI_LIB': 'N° ' + $('#INP_F_CHEQUE_Num').val() + 'Banque : ' + $('#INP_F_CHEQUE_Banq').val()
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                        $.PNotify_Alert('Règlement', 'Règlement effectué avec succès', 'success');
                    });
                break;

            case '#TRAITE':
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'TRAITE',
                        'DM_CONSI_NUM': $('#INP_F_TRAITE_Num').val(),
                        'DM_CONSI_BANQUE': $('#INP_F_TRAITE_Banq').val(),
                        'DM_CONSI_DATE': $('#INP_F_TRAITE_DATE').val(),
                        'DM_CONSI_LIB': 'N° ' + $('#INP_F_TRAITE_Num').val() + 'Banque : ' + $('#INP_F_TRAITE_Banq').val()
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                        $.PNotify_Alert('Règlement', 'Règlement effectué avec succès', 'success');
                    });
                break;

            case '#VIREMENT':
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'VIREMENT',
                        'DM_CONSI_NUM': $('#INP_F_VIREMENT_Num').val(),
                        'DM_CONSI_BANQUE': $('#INP_F_VIREMENT_Banq').val(),
                        'DM_CONSI_DATE': $('#INP_F_VIREMENT_DATE').val(),
                        'DM_CONSI_LIB': 'N° ' + $('#INP_F_VIREMENT_Num').val() + 'Banque : ' + $('#INP_F_VIREMENT_Banq').val()
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                        $.PNotify_Alert('Règlement', 'Règlement effectué avec succès', 'success');
                    });
                break;
            default:
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SET_REGL_CONSI', {
                        'DM_CONSI_REGLEMENT': 'BC',
                        'DM_CONSI_NUM': '',
                        'DM_CONSI_BANQUE': '',
                        'DM_CONSI_DATE': '',
                        'DM_CONSI_LIB': ''
                    }, $('#DOS_NUM_INP').val()]))
                    .fail(function (data) {
                        $.PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function (data) {
                        //location.reload();
                    });
                break;
        }
    });

});