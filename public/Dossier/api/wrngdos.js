$(document).ready(function() {
    moment.locale('fr');
    $.fn.toText = function(str) {
        var cache = this.children();
        this.text(str).append(cache);
    };

    $.extend($.fn.dataTable.defaults, {
        autoWidth: false,
        aaSorting: [],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: 'Tapez pour filtrer ...',
            lengthMenu: '<span>Afficher:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            processing: "Traitement en cours...",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

    $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['GET_FC'])).done(function(data) {
        var COL_EXPORT = data.EXPORT,
            COL_IMPORT = data.IMPORT,
            COL_CEXPORT = data.CEXPORT,
            COL_CIMPORT = data.CIMPORT,
            COL_GEXPORT = data.GEXPORT,
            COL_GIMPORT = data.GIMPORT,
            COL_UNKNOWN = data.UNKNOWN,
            COL_MAP = data.MAPCOL;

        function GC_extend(params) {
            params === null || params === undefined ? params = '' : console.log();
            switch (params.toUpperCase()) {
                case 'C':
                    return 'MARITIME COMPLET';
                case 'G':
                    return 'MARITIME GROUPAGE';
                default:
                    return 'AERIEN';
            }
        }

        function Gen_Color_Marker(ImEx, GCv) {
            GCv === null || GCv === undefined ? GCv = '' : console.log();
            ImEx === null || ImEx === undefined ? ImEx = '' : console.log();
            switch (GCv.toUpperCase() + ImEx.toUpperCase()) {
                case 'EXPORT':
                    return '#' + COL_EXPORT;
                case 'IMPORT':
                    return '#' + COL_IMPORT;
                case 'CEXPORT':
                    return '#' + COL_CEXPORT;
                case 'CIMPORT':
                    return '#' + COL_CIMPORT;
                case 'GEXPORT':
                    return '#' + COL_GEXPORT;
                case 'GIMPORT':
                    return '#' + COL_GIMPORT;
                default:
                    return '#' + COL_UNKNOWN;
            }
        }


        var NAV_LIST = new Array();
        var PORT_LIST = new Array();
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['navire'])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.forEach(function(element) {
                NAV_LIST.push(element.NA_CODE);
            }, this);
        }).always(function() {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['port'])).fail(function(data) {
                console.log('fail', data);
            }).done(function(data) {
                data.forEach(function(element) {
                    PORT_LIST.push(element.PO_CODE);
                }, this);
            }).always(function() {
                var TAB_DACTIV_UNK = $('#TAB_DACTIV_UNK').DataTable({
                    buttons: {
                        buttons: [{
                                extend: 'print',
                                className: 'btn bg-blue btn-icon',
                                text: '<i class="icon-printer position-left"></i>',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            {
                                extend: 'colvis',
                                text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                                className: 'btn bg-blue btn-icon'
                            }
                        ],
                    },
                    select: false,
                    destroy: true,
                    scrollY: false,
                    scrollX: false,
                    ajax: {
                        url: "/GTRANS/public/Dossier/api/tdb.php",
                        type: "POST",
                        data: function(d) {
                            return JSON.stringify({
                                "0": "MAP_INFO_UNK"
                            });
                        }
                    },
                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                    columns: [
                        { "data": "DM_CLE" },
                        {
                            "data": "DM_CODE_COMP_GROUP",
                            "width": "21%",
                            "render": function(data, type, row) {
                                /*var col = Gen_Color_Marker(row.DM_IMP_EXP, row.DM_CODE_COMP_GROUP);
                                var ty = GC_extend(row.DM_CODE_COMP_GROUP) + ' ' + row.DM_IMP_EXP;
                                return '<span class="label label-flat bg-grey label-block" style="border-color: ' + col + ';background-color: ' + col + ';">' + ty + '</span>';*/
                                var col = Gen_Color_Marker(row.DM_IMP_EXP, row.DM_CODE_COMP_GROUP);
                                var ty = GC_extend(row.DM_CODE_COMP_GROUP) + ' ' + row.DM_IMP_EXP;
                                var icomo = (row.DM_CODE_COMP_GROUP == '' || row.DM_CODE_COMP_GROUP == null) ? 'fa fa-plane' : 'fa fa-ship';
                                var pb = '';
                                var start = new Date(row.C_DM_DATE_EMBARQ), // Jan 1, 2015
                                    end = new Date(row.C_DM_DATE_DECHARG), // June 24, 2015
                                    today = new Date(); // April 23, 2015
                                var v = 100;
                                var p = 100 + '%';
                                if ((end - start) > 0) {
                                    v = Math.round(((today - start) / (end - start)) * 100);
                                    p = Math.round(((today - start) / (end - start)) * 100) + '%';
                                }

                                // console.log(v)
                                if (v === Infinity || isNaN(v) || v < 0) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%;background-color: #000000;"></div></div>';
                                } else if (v > 0 && v <= 50) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-success progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 50 && v <= 75) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 75 && v <= 100) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-warning progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                } else if (v > 100) {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-danger" style="width: 100%"></div></div>';
                                } else {
                                    pb = '<div class="progress progress-xxs"><div class="progress-bar progress-bar-striped active" style="width: ' + p + '"></div></div>';
                                }
                                //return '<span class="label label-flat bg-grey label-block" style="border-color: ' + col + ';background-color: ' + col + ';">' + ty + '</span>';
                                var html = '<ul class="list list-unstyled no-margin">\
                                            <li class="no-margin text-bold">\
                                                <i class="' + icomo + ' text-size-base position-left"  style="color: ' + col + ';"></i> ' + ty + '\
                                            </li>\
                                            <li class="no-margin">\
                                                ' + pb + '\
                                            </li>\
                                        </ul>'
                                return html;
                            }
                        },
                        {
                            "data": "DM_NUM_DOSSIER",
                            "render": function(data, type, row) {
                                return '<a class="text-bold" target="_blank" href="/GTRANS/public/Dossier/?NumD=' + row.DM_NUM_DOSSIER + '">' + row.DM_NUM_DOSSIER + '</a>';
                            }
                        },
                        { "data": "DM_DATE_DECHARG" },
                        {
                            "data": "DM_NAVIRE",
                            "render": function(data, type, row) {
                                switch (row.DM_NAVIRE) {
                                    case null:
                                    case '0':
                                        if (row.DM_CODE_COMP_GROUP) {
                                            return '<i class="icon-cross2 text-danger-400 text-bold"></i>';
                                        } else {
                                            return '';
                                        }
                                        break;
                                    default:
                                        if ($.inArray(row.DM_NAVIRE, NAV_LIST) !== -1) {
                                            return '<i class="icon-checkmark3 text-success-400 text-bold"></i>';
                                        } else {
                                            return '<i class="icon-question6 text-warning-400"></i><span class="badge badge-flat border-danger text-danger-600 position-right">' + row.DM_NAVIRE + '</span>';
                                        }
                                }
                            }
                        },
                        {
                            "data": "DM_POD",
                            "render": function(data, type, row) {
                                switch (row.DM_POD) {
                                    case null:
                                    case '0':
                                        return '<i class="icon-cross2 text-danger-400 text-bold"></i>';

                                    default:
                                        if ($.inArray(row.DM_POD, PORT_LIST) !== -1) {
                                            return '<i class="icon-checkmark3 text-success-400 text-bold"></i>';
                                        } else {
                                            return '<i class="icon-question6 text-warning-400"></i><span class="badge badge-flat border-danger text-danger-600 position-right">' + row.DM_POD + '</span>';
                                        }
                                }
                            }
                        },
                        {
                            "data": "DM_POL",
                            "render": function(data, type, row) {
                                switch (row.DM_POL) {
                                    case null:
                                    case '0':
                                        return '<i class="icon-cross2 text-danger-400 text-bold"></i>';

                                    default:
                                        if ($.inArray(row.DM_POL, PORT_LIST) !== -1) {
                                            return '<i class="icon-checkmark3 text-success-400 text-bold"></i>';
                                        } else {
                                            return '<i class="icon-question6 text-warning-400"></i><span class="badge badge-flat border-danger text-danger-600 position-right">' + row.DM_POL + '</span>';
                                        }
                                }
                            }
                        }
                    ]
                });
            });
        });

    });
});