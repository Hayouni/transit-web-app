<?php

require_once $_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/Encryption.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$db = new MySQL();
$get_user_data = $db->get_results("SELECT * FROM users WHERE username = '".$_SESSION['username']."'");
if (!$get_user_data[0]['smtp_host'] || !$get_user_data[0]['smtp_port'] || !$get_user_data[0]['smtp_pass'] || !$get_user_data[0]['email']) {
    echo json_encode(['account' => false]);
    return false;
    die();
}

if (isset($_POST['sent_a']) && isset($_POST['sent_subject']) && isset($_POST['sent_body'])) {
    require_once $_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/phpmailer/PHPMailerAutoload.php';
    //ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    $mail = new PHPMailer;
    //$mail = new PHPMailer\PHPMailer\PHPMailer; //From email address and name 
    //Tell PHPMailer to use SMTP
    $mail->isSMTP();
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );
    //$mail->HttpProxyHostname = '172.20.10.249';
    //$mail->HttpProxyPort = 800;
    //Enable SMTP debugging
    // 0 = off (for production use)
    // 1 = client messages
    // 2 = client and server messages
    $mail->SMTPDebug = 0;
    $mail->CharSet = 'UTF-8';
    //Set the hostname of the mail server
    $mail->Host = $get_user_data[0]['smtp_host'];
    // use
    // $mail->Host = gethostbyname('smtp.gmail.com');
    // if your network does not support SMTP over IPv6
    //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
    $mail->Port = $get_user_data[0]['smtp_port'];
    //Set the encryption system to use - ssl (deprecated) or tls
    $mail->SMTPSecure = $get_user_data[0]['smtp_crypto'];
    //Whether to use SMTP authentication
    $mail->SMTPAuth = true;
    //Username to use for SMTP authentication - use full email address for gmail
    $mail->Username = $get_user_data[0]['email'];
    //Password to use for SMTP authentication
    $cryptor = new Encryption();
    $mail->Password = $cryptor->safe_b64decode($get_user_data[0]['smtp_pass']);
    //Set who the message is to be sent from
    $mail->setFrom($get_user_data[0]['email'], $get_user_data[0]['name']." ".$get_user_data[0]['subname']);
    //Set an alternative reply-to address
    //$mail->addReplyTo('replyto@example.com', 'First Last');
    //Set who the message is to be sent to
    $mail->addAddress($_POST['sent_a']);
    //Set the subject line
    $mail->Subject = $_POST['sent_subject'];
    //Read an HTML message body from an external file, convert referenced images to embedded,
    //convert HTML into a basic plain-text alternative body
    $mail->msgHTML($_POST['sent_body']);
    //Replace the plain text body with one created manually
    //$mail->AltBody = 'This is a plain-text message body';
    //Attach an image file
    //$mail->addAttachment('images/phpmailer_mini.png');

//Section 2: IMAP
    //IMAP commands requires the PHP IMAP Extension, found at: https://php.net/manual/en/imap.setup.php
    //Function to call which uses the PHP imap_*() functions to save messages: https://php.net/manual/en/book.imap.php
    //You can use imap_getmailboxes($imapStream, '/imap/ssl') to get a list of available folders or labels, this can
    //be useful if you are trying to get this working on a non-Gmail IMAP server.
    function save_mail($mail)
    {
        if (!$get_user_data[0]['imap_host'] || !$get_user_data[0]['imap_port']) {
            return false;
            exit;
        }
        //You can change 'Sent Mail' to any other folder or tag
        $path = "{".$get_user_data[0]['imap_host'].":".$get_user_data[0]['imap_port']."}";
        //Tell your server to open an IMAP connection using the same username and password as you used for SMTP
        $imapStream = imap_open($path, $mail->Username, $mail->Password);
        $list = imap_list($mbox, $path, "*");
        if (is_array($list)) {
            foreach ($list as $val) {
                if (strpos(imap_utf7_decode($val), 'Messages') !== false || strpos(imap_utf7_decode($val), 'Sent') !== false) {
                    $path = imap_utf7_decode($val) ;
                }
            }
        } else {
            echo "imap_list a échoué : " . imap_last_error() . "\n";
            return false;
            exit;
        }
        $result = imap_append($imapStream, $path, $mail->getSentMIMEMessage());
        imap_close($imapStream);
        return $result;
    }



    //send the message, check for errors
    if (!$mail->send()) {
        echo json_encode(["Sent" => false, "Msg" => $mail->ErrorInfo]);
    } else {
        //Section 2: IMAP
        //Uncomment these to save your message in the 'Sent Mail' folder.
        if (save_mail($mail)) {
            echo json_encode(["Sent" => true,"saved" => true]);
        }else {
            echo json_encode(["Sent" => true,"saved" => false]);
        }
    }
}