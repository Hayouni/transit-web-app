/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

$(document).ready(function() {

    function get_center_pos(width, top) {
        // top is empty when creating a new notification and is set when recentering
        if (!top) {
            top = 90;
            // this part is needed to avoid notification stacking on top of each other
            $('.ui-pnotify').each(function() {
                top += $(this).outerHeight() + 20;
            });
        }
        return {
            "top": top,
            "left": ($(window).width() / 2) - (width / 2) + 10
        };
    }

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'alert alert-styled-left bg-' + type,
            type: type,
            delay: 1500,
            before_open: function(PNotify) {
                PNotify.get().css(get_center_pos(PNotify.get().width()));
            },
        });
    }

    $(window).resize(function() {
        $(".ui-pnotify").each(function() {
            $(this).css(get_center_pos($(this).width(), $(this).position().top));
        });
    });
    var percent = 0;
    var notice = new PNotify({
        text: "Patientez s'il-vous-plait",
        addclass: 'bg-primary',
        type: 'info',
        icon: 'icon-spinner4 spinner',
        before_open: function(PNotify) {
            PNotify.get().css(get_center_pos(PNotify.get().width()));
        },
        hide: false,
        buttons: {
            closer: false,
            sticker: false
        },
        opacity: 0.9,
        //width: "170px"
    });

    $('#NUM_DOS_DIV').css('opacity', 0);

    setTimeout(function() {
        notice.update({
            title: false
        });
        var interval = setInterval(function() {
            percent += 2;
            var options = {
                text: percent + "% complété."
            };
            if (percent == 4) {
                options.title = "Chargement -| <i class='icon-users4'></i> Fournisseurs";
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['fournisseurs'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        $('#fournisseurs_MIG').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                        $('#fournisseurs_MIC').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                        $('#fournisseurs_AER').append('<option value="' + element.FR_CODE + '">' + element.FR_LIBELLE + '</option>');
                    }, this);
                });
            }
            if (percent == 20) {
                options.title = "Chargement -| <i class='icon-person'></i> Clients";
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        $('#clients_MIG').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_MIC').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                        $('#clients_AER').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
                    }, this);
                });
            }
            if (percent == 40) {
                options.title = "Chargement -| <i class='icon-ship'></i> Navires";
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['navire'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        $('#navire_MIG').append('<option value="' + element.NA_CODE + '">' + element.NA_LIBELLE + '</option>');
                        $('#navire_MIC').append('<option value="' + element.NA_CODE + '">' + element.NA_LIBELLE + '</option>');
                    }, this);
                });
            }
            if (percent == 60) {
                options.title = "Chargement -| <i class='icon-earth'></i> Ports";
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['port'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        $('#port_DEP_MIG').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                        $('#port_ARR_MIG').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                        $('#port_DEP_MIC').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                        $('#port_ARR_MIC').append('<option value="' + element.PO_CODE + '">' + element.PO_LIBELLE + '</option>');
                    }, this);
                });
            }
            if (percent == 70) {
                options.title = "Chargement -| <i class='icon-airplane2'></i> Aéroport";
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['aeroport'])).fail(function(data) {
                    console.log('fail', data);
                }).done(function(data) {
                    data.forEach(function(element) {
                        $('#port_DEP_AER').append('<option value="' + element.AE_CODE + '">' + element.AE_LIBELLE + '</option>');
                        $('#port_ARR_AER').append('<option value="' + element.AE_CODE + '">' + element.AE_LIBELLE + '</option>');
                    }, this);
                });
            }
            if (percent == 80) {
                options.title = "On y est presque ..";
                $.fn.selectpicker.defaults = {
                    iconBase: '',
                    tickIcon: 'icon-checkmark3'
                };
                $('.bootstrap-select').selectpicker();

                //init combox : Nature Dossier && Type Dossier
                $('.multiselect').multiselect({
                    enableHTML: true,
                    onChange: function() {
                        $.uniform.update();
                        var selected = this.$select.val();
                        switch (selected) {
                            case 'Maritime':
                                $('option[value="G"]', $('#SEL_CG_DOS')).attr('selected', 'selected');
                                $('option[value="G"]', $('#SEL_CG_DOS')).prop('selected', true);
                                $('#SEL_CG_DOS').multiselect('enable');
                                break;
                            case 'Aeriene':
                                $('option[value=""]', $('#SEL_CG_DOS')).attr('selected', 'selected');
                                $('option[value=""]', $('#SEL_CG_DOS')).prop('selected', true);
                                $('#SEL_CG_DOS').multiselect('disable');
                                break;

                            default:
                                break;
                        }
                    }
                });

                // Success
                $('.multiselect-success').multiselect({
                    buttonClass: 'btn btn-success'
                });
                // init radio inside combox 
                $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });

                // ------------------------------
                // Button with spinner
                Ladda.bind('.btn-ladda-spinner', {
                    dataSpinnerSize: 16,
                    timeout: 2000
                });

                // Button with progress
                /*Ladda.bind('.btn-ladda-progress', {
                    callback: function(instance) {
                        var progress = 0;
                        var interval = setInterval(function() {
                            progress = Math.min(progress + Math.random() * 0.1, 1);
                            instance.setProgress(progress);
            
                            if (progress === 1) {
                                instance.stop();
                                clearInterval(interval);
                            }
                        }, 200);
                    }
                });*/

            }
            if (percent >= 100) {
                window.clearInterval(interval);
                options.title = "Tout est fait, bon travail";
                options.addclass = "bg-success";
                options.type = "success";
                options.hide = true;
                options.delay = 1000;
                options.before_open = function(PNotify) {
                    PNotify.get().css(get_center_pos(PNotify.get().width()));
                };
                options.buttons = {
                    closer: true,
                    sticker: true
                };
                options.icon = 'icon-checkmark3';
                options.opacity = 1;
                options.width = PNotify.prototype.options.width;
                $('#NUM_DOS_DIV').delay(1000).animate({ "opacity": "1" }, 700);
                if (NumD) {
                    $('#GET_DOS_BTN').click();
                }
            }
            notice.update(options);
        }, 120);
    }, 2000);

    //Selection de dossier
    $("#BTN_SEL_DOS").on('click', function() {
        cloture(false);
        Select_DOS();
    });

    function Select_DOS() {
        var choix = [];
        choix.push($('select#SEL_N_DOS').val(), $('select#SEL_T_DOS').val(), $('select#SEL_CG_DOS').val());
        console.log(choix.join(' ').replace(/\s/g, ''));
        switch (choix.join(' ').replace(/\s/g, '')) {
            case "MaritimeIMPORTG":
                $('#DOS_MAR_NAME').html('Dossier Maritime Import Groupage');
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "none");
                $('#DIV_MAR').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            case "MaritimeIMPORTC":
                $('#DOS_MARC_NAME').html('Dossier Maritime Import Complet');
                $('#DIV_MAR').css("display", "none");
                $('#DIV_AER').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            case "MaritimeEXPORTG":
                $('#DOS_MAR_NAME').html('Dossier Maritime Export Groupage');
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "none");
                $('#DIV_MAR').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            case "MaritimeEXPORTC":
                $('#DOS_MARC_NAME').html('Dossier Maritime Export Complet');
                $('#DIV_MAR').css("display", "none");
                $('#DIV_AER').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            case "AerieneIMPORT":
                $('#DOS_AER_NAME').html('Dossier Aériene Import');
                $('#DIV_MAR').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            case "AerieneEXPORT":
                $('#DOS_AER_NAME').html('Dossier Aériene Export');
                $('#DIV_MAR').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "block");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
            default:
                //hide maritime part if no choice
                console.log('MART DEFAULT CASE');
                $('#DOS_MAR_NAME').html('Dossier ...');
                $('#DIV_MAR').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "none");
                $('#INP_DOS_SPIN').css('visibility', 'hidden');
                break;
        }
    }

    function toDate(dateStr) {
        if (dateStr) {
            const [day, month, year] = dateStr.split("/");
            return [year, month, day].join('/');
        } else {
            return '';
        }
    }

    var TAB_MIG, TAB_MIG_FACT, TAB_MIC_FACT = $('#TAB_MIG').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });
    var TAB_MIC1 = $('#TAB_MIC1').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });
    var TAB_MIC2 = $('#TAB_MIC2').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        paging: false,
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
        }
    });

    // Setting datatable defaults
    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        //dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        drawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $('#clients_MIG').on('change', function(e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.length !== 0 ? $('#clients_ADR_MIG').val(data[0].CL_ADRESSE) : $('#clients_ADR_MIG').val('');
        });
    });

    $('#clients_MIC').on('change', function(e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.length !== 0 ? $('#clients_ADR_MIC').val(data[0].CL_ADRESSE) : $('#clients_ADR_MIC').val('');
        });
    });

    $('#clients_AER').on('change', function(e) {
        var selected = e.target.value;
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', selected])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            data.length !== 0 ? $('#clients_ADR_AER').val(data[0].CL_ADRESSE) : $('#clients_ADR_AER').val('');
        });
    });


    $('#DOS_NUM_INP').on('keypress', function(e) {
        if (e.which === 13) {
            $('#GET_DOS_BTN').click();
        }
    });

    $('#BTN_ATT_U_ADD_MIG').on('click', function() {
        $.post('/GTRANS/sys/session.php').fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_MIG').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_MIG').val(info.user_name);
        });
    });

    $('#BTN_ATT_U_ADD_MIC').on('click', function() {
        $.post('/GTRANS/sys/session.php').fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_MIC').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_MIC').val(info.user_name);
        });
    });

    $('#BTN_ATT_U_ADD_AER').on('click', function() {
        $.post('/GTRANS/sys/session.php').fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            var info = JSON.parse(data);
            $('#DM_ATTRIBUER_AER').attr("user", info.user_connected);
            $('#DM_ATTRIBUER_AER').val(info.user_name);
        });
    });

    function cloture(lock) {
        switch (lock) {
            case true:
                //DMG
                $("#BTN_SAVE_MIG, #BTN_DEL_MIG, #BTN_MOD_MIG, #BTN_ADD_to_MIG").addClass('disabled');
                $('#BTN_CLOT_MIG').html('<span class="ladda-label"><i class="icon-lock4"></i> clôturer</span>');
                //DMC
                $("#BTN_SAVE_MIC, #BTN_MOD_MIC, #BTN_ADD_MIC, #DEL_MICM, #MOD_MICM, #ADD_MICM, #DEL_MICC, #MOD_MICC, #ADD_MICC").addClass('disabled');
                $('#BTN_CLOT_MIC').html('<span class="ladda-label"><i class="icon-lock4"></i> clôturer</span>');
                //AER
                $("#BTN_SAVE_AER, #BTN_MOD_AER, #BTN_ADD_AER").addClass('disabled');
                $('#BTN_CLOT_AER').html('<span class="ladda-label"><i class="icon-lock4"></i> clôturer</span>');
                break;

            default:
                //DMG
                $("#BTN_SAVE_MIG, #BTN_DEL_MIG, #BTN_MOD_MIG, #BTN_ADD_to_MIG").removeClass('disabled');
                $('#BTN_CLOT_MIG').html('<span class="ladda-label"><i class="icon-unlocked"></i> clôturer</span>');
                //DMC
                $("#BTN_SAVE_MIC, #BTN_MOD_MIC, #BTN_ADD_MIC, #DEL_MICM, #MOD_MICM, #ADD_MICM, #DEL_MICC, #MOD_MICC, #ADD_MICC").removeClass('disabled');
                $('#BTN_CLOT_MIC').html('<span class="ladda-label"><i class="icon-unlocked"></i> clôturer</span>');
                //AER
                $("#BTN_SAVE_AER, #BTN_MOD_AER, #BTN_ADD_AER").removeClass('disabled');
                $('#BTN_CLOT_AER').html('<span class="ladda-label"><i class="icon-unlocked"></i> clôturer</span>');
                break;
        }
    }

    $('#GET_DOS_BTN').on('click', function() {
        if ($('#DOS_NUM_INP').val().replace(/ /g, '').length == 10) {
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_CLOT_DOS', $('#DOS_NUM_INP').val()])).done(function(data) {
                if (data[0].DM_CLOTURE === "1") {
                    new PNotify({
                        title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                        text: '<i class="fa fa-lock fa-2x"></i> Dossier Cloturé !',
                        addclass: 'alert alert-styled-left bg-info',
                        type: 'info',
                        delay: 2500,
                        before_open: function(PNotify) {
                            PNotify.get().css(get_center_pos(PNotify.get().width()));
                        },
                    });
                    cloture(true);
                } else {
                    cloture(false);
                }
            }).always(function(data) {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM_INP').val()]), function(data) {
                    if (Object.keys(data).length) {
                        switch (data[0].DM_CODE_COMP_GROUP) {
                            case null: //Case Aerian
                            case '':
                                $('option[value="Aeriene"]', $('#SEL_N_DOS')).attr('selected', 'selected');
                                $('option[value="Aeriene"]', $('#SEL_N_DOS')).prop('selected', true);

                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).attr('selected', 'selected');
                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).prop('selected', true);

                                $('option[value=""]', $('#SEL_CG_DOS')).attr('selected', 'selected');
                                $('option[value=""]', $('#SEL_CG_DOS')).prop('selected', true);

                                $('#DM_ATTRIBUER_AER').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_AER').val(data.UFNAME);

                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_AER').prop('disabled', true);
                                    /*$.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_ATTR_DATA']))
                                        .done(function(data) {
                                            $('.attr_in_np_img').attr('src', '/GTRANS/public/users/user_data/' + data.UFNAME + '/' + data.UFNAME);
                                            $('.attr_in_np').text(data.UFNAME);
                                        });*/
                                } else {
                                    $('#BTN_ATT_U_ADD_AER').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_AER').prop('readonly', true);

                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_AER_DATA', $('#DOS_NUM_INP').val()])).fail(function(data) {
                                    console.log('fail', data);
                                }).done(function(data) {
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function(data) {
                                        console.log('fail', data);
                                    }).done(function(data) {
                                        $('#clients_ADR_AER').val(data[0].CL_ADRESSE);
                                    });
                                    //console.log(data);
                                    $('#fournisseurs_AER').val(data[0].DM_FOURNISSEUR);
                                    $('#clients_AER').val(data[0].DM_CLIENT);
                                    $('#LTA_AER').val(data[0].DM_NUM_LTA);
                                    $('#port_DEP_AER').val(data[0].DM_POL);
                                    $('#port_ARR_AER').val(data[0].DM_POD);
                                    $('#MARCH_AER').val(data[0].DM_MARCHANDISE);
                                    data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_AER').val(new Date(data[0].DM_DATE_EMBARQ).formatDate('dd/MM/yyyy')) : $('#Date_Dep_AER').val('');
                                    data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_AER').val(new Date(data[0].DM_DATE_DECHARG).formatDate('dd/MM/yyyy')) : $('#Date_arr_AER').val('');

                                    $('#Poids_AER').val(data[0].DM_POIDS);
                                    $('#Nombre_AER').val(data[0].DM_NOMBRE);
                                    $('#long_AER').val(data[0].DM_LONGUEUR);
                                    $('#larg_AER').val(data[0].DM_LARGEUR);
                                    $('#haut_AER').val(data[0].DM_HAUTEUR);
                                    data[0].DM_MAGASIN ? $('#magasin_AER').val(data[0].DM_MAGASIN.toUpperCase()) : $('#magasin_AER').val('');
                                    data[0].DM_TERME ? $('#term_AER').val(data[0].DM_TERME.toUpperCase()) : $('#term_AER').val('');
                                    data[0].DM_MARQUE ? $('#marq_AER').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_AER').val('');
                                    $('#Rubr_AER').val(data[0].DM_RUBRIQUE);
                                    $('#VAL_DEV_AER').val(data[0].DM_VAL_DEVISE);
                                });
                                TAB_AER_FACT = $('#TAB_AER_FACT').DataTable({
                                    select: {
                                        style: 'single'
                                    },
                                    destroy: true,
                                    scrollY: "400px",
                                    scrollX: false,
                                    scrollCollapse: true,
                                    paging: false,
                                    ajax: {
                                        url: "/GTRANS/public/Dossier/api/dossier.php",
                                        type: "POST",
                                        data: function(d) {
                                            return JSON.stringify({
                                                "0": "GET_FACT_DOS",
                                                "1": $('#DOS_NUM_INP').val()
                                            });
                                        },
                                    },
                                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                    columns: [
                                        { "data": "AAM_CODE" },
                                        { "data": "AAM_NUM_FACTURE" },
                                        { "data": "AAM_DATE_FACTURE" },
                                        { "data": "FT_LIBELLE" },
                                        { "data": "AAM_BON_COMMAND" },
                                        { "data": "AAM_TOTAL_TTC" },
                                        { "data": "CL_LIBELLE" },
                                        { "data": "CL_RSPONSABLE" },
                                        { "data": "CL_TEL_ETABLISS" },
                                        { "data": "CL_FAX" },
                                        { "data": "CL_EMAIL" }
                                    ],
                                    initComplete: function() {
                                        this.api().columns().every(function() {
                                            var column = this;
                                            var select = $('<select class="form-control"><option value=""></option></select>')
                                                .appendTo($(column.footer()).empty())
                                                .on('change', function() {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                        $(this).val()
                                                    );

                                                    column
                                                        .search(val ? '^' + val + '$' : '', true, false)
                                                        .draw();
                                                });

                                            column.data().unique().sort().each(function(d, j) {
                                                select.append('<option value="' + d + '">' + d + '</option>')
                                            });
                                        });
                                    }
                                }).columns.adjust().draw();
                                break;

                            case 'G':
                                DM_CLEANER();
                                $('option[value="Maritime"]', $('#SEL_N_DOS')).attr('selected', 'selected');
                                $('option[value="Maritime"]', $('#SEL_N_DOS')).prop('selected', true);

                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).attr('selected', 'selected');
                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).prop('selected', true);

                                $('option[value="G"]', $('#SEL_CG_DOS')).attr('selected', 'selected');
                                $('option[value="G"]', $('#SEL_CG_DOS')).prop('selected', true);
                                $('#DM_ATTRIBUER_MIG').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_MIG').val(data.UFNAME);
                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_MIG').prop('disabled', true);
                                } else {
                                    $('#BTN_ATT_U_ADD_MIG').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_MIG').prop('readonly', true);

                                TAB_MIG = $('#TAB_MIG').DataTable({
                                    select: {
                                        style: 'single'
                                    },
                                    destroy: true,
                                    scrollY: "200px",
                                    scrollX: false,
                                    scrollCollapse: true,
                                    paging: false,
                                    ajax: {
                                        url: "/GTRANS/public/Dossier/api/dossier.php",
                                        type: "POST",
                                        data: function(d) {
                                            return JSON.stringify({
                                                "0": "GET_DOS_MIG_MARCH",
                                                "1": $('#DOS_NUM_INP').val()
                                            });
                                        },
                                    },
                                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                    columns: [
                                        { "data": "DM_CLE" },
                                        { "data": "DM_NUM_BL" },
                                        { "data": "DM_MARCHANDISE" },
                                        { "data": "DM_POIDS" },
                                        { "data": "DM_NOMBRE" },
                                        { "data": "CL_LIBELLE" }
                                    ]
                                }).on('select', function(e, dt, type, indexes) {
                                    var rowData = $('#TAB_MIG').DataTable().rows(indexes).data().toArray();
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_MARCH_DATA', rowData[0].DM_CLE])).fail(function(data) {
                                        console.log('fail', data);
                                    }).done(function(data) {
                                        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function(data) {
                                            console.log('fail', data);
                                        }).done(function(data) {
                                            $('#clients_ADR_MIG').val(data[0].CL_ADRESSE);
                                        });
                                        //console.log(data);
                                        $('#fournisseurs_MIG').val(data[0].DM_FOURNISSEUR);
                                        $('#clients_MIG').val(data[0].DM_CLIENT);
                                        $('#BL_MIG').val(data[0].DM_NUM_BL);
                                        $('#navire_MIG').val(data[0].DM_NAVIRE);
                                        $('#port_DEP_MIG').val(data[0].DM_POL);
                                        $('#port_ARR_MIG').val(data[0].DM_POD);
                                        $('#MARCH_MIG').val(data[0].DM_MARCHANDISE);
                                        data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_MIG').val(new Date(data[0].DM_DATE_EMBARQ).formatDate('dd/MM/yyyy')) : $('#Date_Dep_MIG').val('');
                                        data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_MIG').val(new Date(data[0].DM_DATE_DECHARG).formatDate('dd/MM/yyyy')) : $('#Date_arr_MIG').val('');

                                        $('#Poids_MIG').val(data[0].DM_POIDS);
                                        $('#Nombre_MIG').val(data[0].DM_NOMBRE);
                                        $('#long_MIG').val(data[0].DM_LONGUEUR);
                                        $('#larg_MIG').val(data[0].DM_LARGEUR);
                                        $('#haut_MIG').val(data[0].DM_HAUTEUR);
                                        data[0].DM_TERME ? $('#term_MIG').val(data[0].DM_TERME.toUpperCase()) : $('#term_MIG').val('');
                                        data[0].DM_MARQUE ? $('#marq_MIG').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_MIG').val('');
                                        $('#escal_MIG').val(data[0].DM_ESCALE);
                                        $('#Rubr_MIG').val(data[0].DM_RUBRIQUE);
                                        $('#VAL_DEV_MIG').val(data[0].DM_VAL_DEVISE);
                                    });
                                }).columns.adjust().draw();

                                TAB_MIG_FACT = $('#TAB_MIG_FACT').DataTable({
                                    select: {
                                        style: 'single'
                                    },
                                    destroy: true,
                                    scrollY: "400px",
                                    scrollX: false,
                                    scrollCollapse: true,
                                    paging: false,
                                    ajax: {
                                        url: "/GTRANS/public/Dossier/api/dossier.php",
                                        type: "POST",
                                        data: function(d) {
                                            return JSON.stringify({
                                                "0": "GET_FACT_DOS",
                                                "1": $('#DOS_NUM_INP').val()
                                            });
                                        },
                                    },
                                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                    columns: [
                                        { "data": "AAM_CODE" },
                                        { "data": "AAM_NUM_FACTURE" },
                                        { "data": "AAM_DATE_FACTURE" },
                                        { "data": "FT_LIBELLE" },
                                        { "data": "AAM_BON_COMMAND" },
                                        { "data": "AAM_TOTAL_TTC" },
                                        { "data": "CL_LIBELLE" },
                                        { "data": "CL_RSPONSABLE" },
                                        { "data": "CL_TEL_ETABLISS" },
                                        { "data": "CL_FAX" },
                                        { "data": "CL_EMAIL" }
                                    ],
                                    initComplete: function() {
                                        this.api().columns().every(function() {
                                            var column = this;
                                            var select = $('<select class="form-control"><option value=""></option></select>')
                                                .appendTo($(column.footer()).empty())
                                                .on('change', function() {
                                                    var val = $.fn.dataTable.util.escapeRegex(
                                                        $(this).val()
                                                    );

                                                    column
                                                        .search(val ? '^' + val + '$' : '', true, false)
                                                        .draw();
                                                });

                                            column.data().unique().sort().each(function(d, j) {
                                                select.append('<option value="' + d + '">' + d + '</option>')
                                            });
                                        });
                                    }
                                }).columns.adjust().draw();
                                break;

                            case 'C':
                                DM_CLEANER();
                                $('#INP_DOS_SPIN').css('visibility', 'visible');
                                $('option[value="Maritime"]', $('#SEL_N_DOS')).attr('selected', 'selected');
                                $('option[value="Maritime"]', $('#SEL_N_DOS')).prop('selected', true);

                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).attr('selected', 'selected');
                                $('option[value="' + data[0].DM_IMP_EXP + '"]', $('#SEL_T_DOS')).prop('selected', true);

                                $('option[value="C"]', $('#SEL_CG_DOS')).attr('selected', 'selected');
                                $('option[value="C"]', $('#SEL_CG_DOS')).prop('selected', true);

                                $('#DM_ATTRIBUER_MIC').attr("user", data[0].DM_ATTRIBUER);
                                $('#DM_ATTRIBUER_MIC').val(data.UFNAME);
                                if (data.UFNAME) {
                                    $('#BTN_ATT_U_ADD_MIC').prop('disabled', true);
                                } else {
                                    $('#BTN_ATT_U_ADD_MIC').prop('disabled', false);
                                }
                                $('#DM_ATTRIBUER_MIC').prop('readonly', true);

                                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA', $('#DOS_NUM_INP').val()])).fail(function(data) {
                                    console.log('fail', data);
                                }).done(function(data) {
                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CL_ADDRESS', data[0].DM_CLIENT])).fail(function(data) {
                                        console.log('fail', data);
                                    }).done(function(data) {
                                        $('#clients_ADR_MIC').val(data[0].CL_ADRESSE);
                                    });
                                    $('#fournisseurs_MIC').val(data[0].DM_FOURNISSEUR);
                                    $('#clients_MIC').val(data[0].DM_CLIENT);
                                    $('#BL_MIC').val(data[0].DM_NUM_BL);
                                    $('#navire_MIC').val(data[0].DM_NAVIRE);
                                    $('#port_DEP_MIC').val(data[0].DM_POL);
                                    $('#port_ARR_MIC').val(data[0].DM_POD);
                                    data[0].DM_DATE_EMBARQ && data[0].DM_DATE_EMBARQ !== '0000-00-00' ? $('#Date_Dep_MIC').val(new Date(data[0].DM_DATE_EMBARQ).formatDate('dd/MM/yyyy')) : $('#Date_Dep_MIC').val('');
                                    data[0].DM_DATE_DECHARG && data[0].DM_DATE_DECHARG !== '0000-00-00' ? $('#Date_arr_MIC').val(new Date(data[0].DM_DATE_DECHARG).formatDate('dd/MM/yyyy')) : $('#Date_arr_MIC').val('');
                                    $('#NBR_MIC').val(data[0].DM_NOMBRE);
                                    data[0].DM_TERME ? $('#term_MIC').val(data[0].DM_TERME.toUpperCase()) : $('#term_MIC').val('');
                                    data[0].DM_MARQUE ? $('#marq_MIC').val(data[0].DM_MARQUE.toLowerCase()) : $('#marq_MIC').val('');
                                    $('#escal_MIC').val(data[0].DM_ESCALE);
                                    $('#VAL_DEV_MIC').val(data[0].DM_VAL_DEVISE);
                                    $('#TAB_MIC1').DataTable({
                                            destroy: true,
                                            paging: false,
                                            ajax: {
                                                url: "/GTRANS/public/Dossier/api/dossier.php",
                                                type: "POST",
                                                data: function(d) {
                                                    return JSON.stringify({
                                                        "0": "GET_DOS_COMP_DATA_MARCH",
                                                        "1": $('#DOS_NUM_INP').val()
                                                    });
                                                },
                                            },
                                            aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                            columns: [
                                                { "data": "DM_CODE" },
                                                { "data": "DM_MARCHANDISE" },
                                                { "data": "DM_POID" },
                                                { "data": "DM_RUBRIQUE" },
                                                { "data": "DM_NBR" }
                                            ]
                                        }).on('select', function(e, dt, type, indexes) {
                                            var rowData = $('#TAB_MIC1').DataTable().rows(indexes).data().toArray();
                                            $('#MARC_NAME_C').html('MARCHANDISE : ' + rowData[0].DM_MARCHANDISE);
                                            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_MARCH_SELECT', rowData[0].DM_CODE])).fail(function(data) {
                                                console.log('fail', data);
                                            }).done(function(data) {
                                                $('#TAB_MIC2').DataTable({
                                                    select: {
                                                        style: 'single'
                                                    },
                                                    destroy: true,
                                                    scrollY: "200px",
                                                    scrollX: false,
                                                    scrollCollapse: true,
                                                    paging: false,
                                                    aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                                    columns: [
                                                        { "data": "DC_CODE" },
                                                        { "data": "DC_CONTAINER" },
                                                        { "data": "DC_NUM_CONTAINER" },
                                                        { "data": "DC_RUBRIQUE" },
                                                        { "data": "DC_POID" }
                                                    ],
                                                    ajax: {
                                                        url: "/GTRANS/public/Dossier/api/dossier.php",
                                                        type: "POST",
                                                        data: function(d) {
                                                            return JSON.stringify({
                                                                "0": "GET_DOS_COMP_DATA_CONT",
                                                                "1": rowData[0].DM_CODE
                                                            });
                                                        },
                                                    }
                                                }).on('select', function(e, dt, type, indexes) {
                                                    var rowData2 = $('#TAB_MIC2').DataTable().rows(indexes).data().toArray();
                                                    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_CONT_SELECT', rowData2[0].DC_CODE])).fail(function(data) {
                                                        console.log('fail', data);
                                                    }).done(function(data) {
                                                        $('#VOLUM_MIC').val(data[0].DC_CONTAINER);
                                                        $('#Numero_MICC').val(data[0].DC_NUM_CONTAINER);
                                                        $('#Rubrq_MICC').val(data[0].DC_RUBRIQUE);
                                                        $('#PoidsV_MIC').val(data[0].DC_POID);
                                                    });
                                                }).columns.adjust().draw();
                                                $('#MARCH_MIC').val(data[0].DM_MARCHANDISE);
                                                $('#Rubr_MIC').val(data[0].DM_RUBRIQUE);
                                                $('#Nombre_MIC').val(data[0].DM_NBR);
                                                $('#Poids_MIC').val(data[0].DM_POID);
                                            });
                                        })
                                        .on('deselect', function(e, dt, type, indexes) {
                                            var rowData = $('#TAB_MIC1').DataTable().rows(indexes).data().toArray();
                                            $('#MARC_NAME_C').html('');
                                            $('#TAB_MIC2').DataTable().clear().draw();
                                        }).columns.adjust().draw();

                                    TAB_MIC_FACT = $('#TAB_MIC_FACT').DataTable({
                                        select: {
                                            style: 'single'
                                        },
                                        destroy: true,
                                        scrollY: "400px",
                                        scrollX: false,
                                        scrollCollapse: true,
                                        paging: false,
                                        ajax: {
                                            url: "/GTRANS/public/Dossier/api/dossier.php",
                                            type: "POST",
                                            data: function(d) {
                                                return JSON.stringify({
                                                    "0": "GET_FACT_DOS",
                                                    "1": $('#DOS_NUM_INP').val()
                                                });
                                            },
                                        },
                                        aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
                                        columns: [
                                            { "data": "AAM_CODE" },
                                            {
                                                "data": "AAM_NUM_FACTURE",
                                                "render": function(data, type, row) {
                                                    return '<a id="SHOWFACT">' + row.AAM_NUM_FACTURE + '</a>';
                                                }
                                            },
                                            { "data": "AAM_DATE_FACTURE" },
                                            {
                                                "data": "FT_LIBELLE",
                                                "render": function(data, type, row) {
                                                    switch (row.FT_LIBELLE) {
                                                        case 'Timbrage':
                                                            return '<span class="label bg-danger-600">' + row.FT_LIBELLE + '</span>';
                                                        case 'Magasinage':
                                                            return '<span class="label bg-info-600">' + row.FT_LIBELLE + '</span>';
                                                        case 'Surestarie':
                                                            return '<span class="label bg-brown-600">' + row.FT_LIBELLE + '</span>';
                                                        case 'Aérien':
                                                            return '<span class="label bg-indigo-600">' + row.FT_LIBELLE + '</span>';
                                                        case 'Avoirs':
                                                            return '<span class="label bg-warning-600">' + row.FT_LIBELLE + '</span>';
                                                        default:
                                                            return '<span class="label bg-slate-600">' + row.FT_LIBELLE + '</span>';
                                                    }
                                                }
                                            },
                                            { "data": "AAM_BON_COMMAND" },
                                            { "data": "AAM_TOTAL_TTC" },
                                            { "data": "CL_LIBELLE" },
                                            { "data": "CL_RSPONSABLE" },
                                            { "data": "CL_TEL_ETABLISS" },
                                            { "data": "CL_FAX" },
                                            { "data": "CL_EMAIL" }
                                        ],
                                        initComplete: function() {
                                            this.api().columns().every(function() {
                                                var column = this;
                                                var select = $('<select class="form-control"><option value=""></option></select>')
                                                    .appendTo($(column.footer()).empty())
                                                    .on('change', function() {
                                                        var val = $.fn.dataTable.util.escapeRegex(
                                                            $(this).val()
                                                        );

                                                        column
                                                            .search(val ? '^' + val + '$' : '', true, false)
                                                            .draw();
                                                    });

                                                column.data().unique().sort().each(function(d, j) {
                                                    select.append('<option value="' + d + '">' + d + '</option>')
                                                });
                                            });
                                        }
                                    }).columns.adjust().draw();
                                });
                                break;
                            default:
                                break;
                        }
                        $('#BTN_SEL_DOS').prop("disabled", true);
                        $('#SEL_N_DOS').multiselect('disable');
                        $('#SEL_T_DOS').multiselect('disable');
                        $('#SEL_CG_DOS').multiselect('disable');
                        $('#SEL_N_DOS').multiselect('refresh');
                        $('#SEL_T_DOS').multiselect('refresh');
                        $('#SEL_CG_DOS').multiselect('refresh');
                        $.uniform.update();
                        $('#DOS_SELECT_DIV').css("display", "block");
                        Select_DOS();
                    } else {
                        $('#DOS_SELECT_DIV').css("display", "none");
                        $('#DIV_MAR').css("display", "none");
                        $('#BTN_SEL_DOS').prop("disabled", false);
                        $('#SEL_N_DOS').multiselect('enable');
                        $('#SEL_T_DOS').multiselect('enable');
                        $('#SEL_CG_DOS').multiselect('enable');
                        new PNotify({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: 'Dossier introuvable ou inexistant, vérifiez le numéro',
                            addclass: 'alert alert-styled-left bg-warning',
                            type: 'warning',
                            delay: 1500,
                            before_open: function(PNotify) {
                                PNotify.get().css(get_center_pos(PNotify.get().width()));
                            },
                        });
                    }
                });
            });
        } else {
            new PNotify({
                title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                text: 'Numéro de dossier non valide !',
                addclass: 'alert alert-styled-left bg-info',
                type: 'info',
                delay: 1500,
                before_open: function(PNotify) {
                    PNotify.get().css(get_center_pos(PNotify.get().width()));
                },
            });
        }
    });

    /*var formSubmitting = false;
    var setFormSubmitting = function() { formSubmitting = true; };
    var NotsetFormSubmitting = function() { formSubmitting = false; };
    var isDirty = function() { return false; };
    window.onload = function() {
        window.addEventListener("beforeunload", function(e) {
            if (formSubmitting) {
                return undefined;
            }
            var confirmationMessage = 'It looks like you have been editing something. ' +
                'If you leave before saving, your changes will be lost.';

            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
        });
    };*/

    $('#ADD_NEW_DMIG').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GEN_NEW_DOS_NUM'])).fail(function(data) {
            console.log('fail', data);
        }).done(function(data) {
            $('#BTN_SEL_DOS').prop("disabled", false);
            $('#SEL_N_DOS').multiselect('enable');
            $('#SEL_T_DOS').multiselect('enable');
            $('#SEL_CG_DOS').multiselect('enable');
            $('#DOS_SELECT_DIV').css("display", "block");
            $('#DIV_MAR').css("display", "none");
            $('#DIV_MAR_COMP').css("display", "none");
            DM_CLEANER();
            $('#DOS_NUM_INP').val(data);
            $('#TAB_MIG').DataTable().columns.adjust().draw();
            $('#TAB_MIG_FACT').DataTable().columns.adjust().draw();
            $('#TAB_MIC1').DataTable().columns.adjust().draw();
            $('#TAB_MIC2').DataTable().columns.adjust().draw();
            $('#TAB_MIC_FACT').DataTable().columns.adjust().draw();
            $('#TAB_AER_FACT').DataTable().columns.adjust().draw();
            $.post('/GTRANS/sys/session.php').fail(function(data) {
                console.log('fail', data);
            }).done(function(data) {
                var info = JSON.parse(data);
                $('#DM_ATTRIBUER_MIG').attr("user", info.user_connected);
                $('#DM_ATTRIBUER_MIG').val(info.user_name);
            });
        });
    });

    Ladda.bind('#BTN_SAVE_MIG', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            var id = $('#DOS_NUM_INP').val();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['SAVE_DOS', id.substr(id.length - 4)])).fail(function(data) {
                console.log('fail', data);
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'L\'ajout du dossier a échoué',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                DM_CLEANER();
                var num_d = $('#DOS_NUM_INP').val();
                $('#DOS_NUM_INP').val('');
                $('#DOS_SELECT_DIV').css("display", "none");
                $('#DIV_MAR').css("display", "none");
                new PNotify({
                    title: 'N° DOS : ' + id,
                    text: 'Dossier sauvegardé avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    Ladda.bind('#BTN_MOD_MIG', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            if (TAB_MIG.rows('.selected').any()) {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_MIG', {
                    'DM_FOURNISSEUR': $('#fournisseurs_MIG').val(),
                    'DM_CLIENT': $('#clients_MIG').val(),
                    'DM_NUM_BL': $('#BL_MIG').val(),
                    'DM_NAVIRE': $('#navire_MIG').val(),
                    'DM_POL': $('#port_DEP_MIG').val(),
                    'DM_POD': $('#port_ARR_MIG').val(),
                    'DM_MARCHANDISE': $('#MARCH_MIG').val(),
                    'DM_DATE_EMBARQ': toDate($('#Date_Dep_MIG').val()),
                    'DM_DATE_DECHARG': toDate($('#Date_arr_MIG').val()),
                    'DM_POIDS': $('#Poids_MIG').val(),
                    'DM_NOMBRE': $('#Nombre_MIG').val(),
                    'DM_LONGUEUR': $('#long_MIG').val(),
                    'DM_LARGEUR': $('#larg_MIG').val(),
                    'DM_HAUTEUR': $('#haut_MIG').val(),
                    'DM_TERME': $('#term_MIG').val(),
                    'DM_MARQUE': $('#marq_MIG').val(),
                    'DM_ESCALE': $('#escal_MIG').val(),
                    'DM_RUBRIQUE': $('#Rubr_MIG').val(),
                    'DM_VAL_DEVISE': $('#VAL_DEV_MIG').val(),
                }, TAB_MIG.rows('.selected').data()[0].DM_CLE])).fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    new PNotify({
                        title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                        text: 'La modification du dossier n\'a pas abouti',
                        addclass: 'alert alert-styled-left bg-danger',
                        type: 'error',
                        delay: 1500,
                        before_open: function(PNotify) {
                            PNotify.get().css(get_center_pos(PNotify.get().width()));
                        },
                    });
                    instance.stop();
                    clearInterval(interval);
                }).done(function(data) {
                    new PNotify({
                        title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                        text: 'Dossier modifié avec succès',
                        addclass: 'alert alert-styled-left bg-success',
                        type: 'success',
                        delay: 1500,
                        before_open: function(PNotify) {
                            PNotify.get().css(get_center_pos(PNotify.get().width()));
                        },
                    });
                    $('#TAB_MIG').DataTable().ajax.reload();
                    instance.stop();
                    clearInterval(interval);
                });
            } else {
                PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
                instance.stop();
                clearInterval(interval);
            }
        }
    });

    Ladda.bind('#BTN_DEL_MIG', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            if (TAB_MIG.rows('.selected').any()) {
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_from_MIG', TAB_MIG.rows('.selected').data()[0].DM_CLE]))
                    .fail(function(data) {
                        console.log('fail');
                        console.dir(data);
                        new PNotify({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: 'Une erreur s\'est produite lors du traitement de suppression',
                            addclass: 'alert alert-styled-left bg-danger',
                            type: 'error',
                            delay: 1500,
                            before_open: function(PNotify) {
                                PNotify.get().css(get_center_pos(PNotify.get().width()));
                            },
                        });
                        instance.stop();
                        clearInterval(interval);
                    })
                    .done(function(data) {
                        new PNotify({
                            title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                            text: 'La suppression a été réussie',
                            addclass: 'alert alert-styled-left bg-success',
                            type: 'success',
                            delay: 1500,
                            before_open: function(PNotify) {
                                PNotify.get().css(get_center_pos(PNotify.get().width()));
                            },
                        });
                        $('#TAB_MIG').DataTable().ajax.reload();
                        instance.stop();
                        clearInterval(interval);
                    });
            } else {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Aucune donnée sélectionnée dans le tableau',
                    addclass: 'alert alert-styled-left bg-warning',
                    type: 'warning',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                TAB_MIG.ajax.reload();
                instance.stop();
                clearInterval(interval);

            }
        }
    });

    Ladda.bind('#BTN_ADD_to_MIG', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_To_MIG', {
                'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
                'DM_FOURNISSEUR': $('#fournisseurs_MIG').val(),
                'DM_CLIENT': $('#clients_MIG').val(),
                'DM_NUM_BL': $('#BL_MIG').val(),
                'DM_NAVIRE': $('#navire_MIG').val(),
                'DM_POL': $('#port_DEP_MIG').val(),
                'DM_POD': $('#port_ARR_MIG').val(),
                'DM_MARCHANDISE': $('#MARCH_MIG').val(),
                'DM_DATE_EMBARQ': toDate($('#Date_Dep_MIG').val()),
                'DM_DATE_DECHARG': toDate($('#Date_arr_MIG').val()),
                'DM_POIDS': $('#Poids_MIG').val(),
                'DM_NOMBRE': $('#Nombre_MIG').val(),
                'DM_LONGUEUR': $('#long_MIG').val(),
                'DM_LARGEUR': $('#larg_MIG').val(),
                'DM_HAUTEUR': $('#haut_MIG').val(),
                'DM_TERME': $('#term_MIG').val(),
                'DM_MARQUE': $('#marq_MIG').val(),
                'DM_ESCALE': $('#escal_MIG').val(),
                'DM_RUBRIQUE': $('#Rubr_MIG').val(),
                'DM_VAL_DEVISE': $('#VAL_DEV_MIG').val(),
                'DM_CODE_COMP_GROUP': $('select#SEL_CG_DOS').val(),
                'DM_IMP_EXP': $('select#SEL_T_DOS').val(),
                'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIG').attr("user"),
                'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIG').val()
            }])).fail(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'L\'ajout au dossier a échoué',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                console.dir(data);
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                TAB_MIG.columns.adjust().draw();
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Ajouter au dossier avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                TAB_MIG.ajax.reload();
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    $('#DEL_DMIG').on('click', function() {

        var notice = new PNotify({
            title: 'Supprimer Dos : ' + $('#DOS_NUM_INP').val(),
            icon: 'icon-warning22',
            addclass: 'custom text-center',
            width: '20%',
            before_open: function(PNotify) {
                PNotify.get().css(get_center_pos(PNotify.get().width()));
            },
            text: 'Entrez le mot de passe pour supprimer, cette opération n\'est pas reversible !',
            hide: false,
            confirm: {
                prompt: true,
                prompt_class: 'text-center',
                buttons: [{
                        text: 'Confirmer',
                        addClass: 'btn btn-sm bg-teal-800'
                    },
                    {
                        text: 'Annuler',
                        addClass: 'btn btn-sm bg-default'
                    }
                ]
            },
            buttons: {
                closer: false,
                sticker: false
            },
            history: {
                history: false
            }
        });

        // On confirm
        notice.get().on('pnotify.confirm', function(e, notice, val) {
            var id = $('#DOS_NUM_INP').val();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['CONF_DEL_PASS', val, id])).fail(function(data) {
                notice.cancelRemove().update({
                    title: 'Mot de passe incorrect !',
                    text: 'Erreur : contacter l\'administrateur !',
                    icon: 'icon-blocked',
                    type: 'error',
                    delay: 2000,
                    hide: true,
                    confirm: {
                        prompt: false
                    },
                    buttons: {
                        closer: true,
                        sticker: true
                    }
                });
            }).done(function(data) {
                DM_CLEANER();
                $('#DOS_NUM_INP').val('');
                $('#DOS_SELECT_DIV').css("display", "none");
                $('#DIV_MAR').css("display", "none");
                notice.cancelRemove().update({
                    title: 'Mot de passe confirmé',
                    text: 'Dossier supprimé définitivement. ' + id + '.',
                    icon: 'icon-checkmark3',
                    type: 'success',
                    delay: 2000,
                    hide: true,
                    confirm: {
                        prompt: false
                    },
                    buttons: {
                        closer: true,
                        sticker: true
                    }
                });
            });

        });

        // On cancel
        notice.get().on('pnotify.cancel', function(e, notice) {
            notice.cancelRemove().update({
                title: 'Operation annuler !',
                text: '',
                icon: 'icon-blocked',
                type: 'info',
                delay: 2000,
                hide: true,
                confirm: {
                    prompt: false
                },
                buttons: {
                    closer: true,
                    sticker: true
                }
            });
        });

        $('.ui-pnotify-action-bar').children('input').attr('type', 'password');
    });

    //CLEAN

    $('#CLS_DMIG').on('click', function() {
        $('#DOS_SELECT_DIV').css("display", "none");
        $('#DIV_MAR').css("display", "none");
        $('#DIV_MAR_COMP').css("display", "none");
        $('#DIV_AER').css("display", "none");
        DM_CLEANER();
    });

    function DM_CLEANER() {
        //MIG 
        if ($("#TAB_MIG").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIG').DataTable().clear().draw();
        if ($("#TAB_MIG_FACT").DataTable().page.info().recordsTotal !== 0) $('#TAB_FACT').DataTable().clear().draw();
        $('#BL_MIG').val('');
        $('#MARCH_MIG').val('');
        $('#Date_Dep_MIG').val('');
        $('#Date_arr_MIG').val('');
        $('#Poids_MIG').val('');
        $('#Nombre_MIG').val('');
        $('#long_MIG').val('');
        $('#larg_MIG').val('');
        $('#haut_MIG').val('');
        $('#escal_MIG').val('');
        $('#Rubr_MIG').val('');
        $('#VAL_DEV_MIG').val('');
        $('#clients_ADR_MIG').val('');
        $('#DM_ATTRIBUER_MIG').attr("user", "");
        $('#DM_ATTRIBUER_MIG').val('');
        //MIC
        $('#BL_MIC').val('');
        $('#Date_Dep_MIC').val('');
        $('#Date_arr_MIC').val('');
        $('#NBR_MIC').val('');
        $('#term_MIC').val('');
        $('#marq_MIC').val('');
        $('#escal_MIC').val('');
        $('#VAL_DEV_MIC').val('');
        $('#clients_ADR_MIC').val('');
        $('#DM_ATTRIBUER_MIC').attr("user", "");
        $('#DM_ATTRIBUER_MIC').val('');
        //----> MIC MARCH
        if ($("#TAB_MIC1").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIC1').DataTable().clear().draw();
        $('#MARCH_MIC').val('');
        $('#Rubr_MIC').val('');
        $('#Nombre_MIC').val('');
        $('#Poids_MIC').val('');
        //----> MIC CONT
        if ($("#TAB_MIC2").DataTable().page.info().recordsTotal !== 0) $('#TAB_MIC2').dataTable().fnClearTable();
        $('#Numero_MICC').val('');
        $('#Rubrq_MICC').val('');
        $('#PoidsV_MIC').val('');
        $('#MARC_NAME_C').html('');

        //AER
        $('#fournisseurs_AER').val('');
        $('#clients_AER').val('');
        $('#LTA_AER').val('');
        $('#port_DEP_AER').val('');
        $('#port_ARR_AER').val('');
        $('#MARCH_AER').val('');
        $('#Date_Dep_AER').val('');
        $('#Date_arr_AER').val('');
        $('#Poids_AER').val('');
        $('#Nombre_AER').val('');
        $('#long_AER').val('');
        $('#larg_AER').val('');
        $('#haut_AER').val('');
        $('#magasin_AER').val('');
        $('#term_AER').val('');
        $('#marq_AER').val('');
        $('#Rubr_AER').val('');
        $('#VAL_DEV_AER').val('');
        $('#DM_ATTRIBUER_AER').attr("user", "");
        $('#DM_ATTRIBUER_AER').val('');
    }

    //COMPLET

    $('#MOD_MICM').on('click', function() {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function() {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP_MARCH', {
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_NBR': $('#Nombre_MIC').val(),
            'DM_POID': $('#Poids_MIC').val(),
        }, dataArr])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            $('#TAB_MIC1').DataTable().ajax.reload();
            $('#MARCH_MIC').val('');
            $('#Rubr_MIC').val('');
            $('#Nombre_MIC').val('');
            $('#Poids_MIC').val('');
        });
    });

    $('#ADD_MICM').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP_MARCH', {
            'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
            'DM_MARCHANDISE': $('#MARCH_MIC').val(),
            'DM_RUBRIQUE': $('#Rubr_MIC').val(),
            'DM_NBR': $('#Nombre_MIC').val(),
            'DM_POID': $('#Poids_MIC').val(),
        }])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            $('#TAB_MIC1').DataTable().ajax.reload();
            $('#MARCH_MIC').val('');
            $('#Rubr_MIC').val('');
            $('#Nombre_MIC').val('');
            $('#Poids_MIC').val('');
        });
    });

    $('#DEL_MICM').on('click', function() {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function() {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['GET_DOS_COMP_DATA_CONT', dataArr])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            //console.log('length :' + Object.keys(data).length);
            if (Object.keys(data).length) {
                var notice = new PNotify({
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                    width: '20%',
                    icon: 'icon-warning22',
                    addclass: 'custom text-center',
                    title: 'Marchandise non vide !',
                    text: '<p>Êtes-vous sûr de supprimer cette marchandise et ses conteneurs ?</p>',
                    hide: false,
                    type: 'warning',
                    confirm: {
                        confirm: true,
                        buttons: [{
                                text: 'Oui',
                                addClass: 'btn btn-sm btn-primary'
                            },
                            {
                                text: 'Non',
                                addClass: 'btn btn-sm btn-default'
                            }
                        ]
                    },
                    buttons: {
                        closer: false,
                        sticker: false
                    },
                    history: {
                        history: false
                    }
                });

                // On confirm
                notice.get().on('pnotify.confirm', function() {
                    DEL_MARCH_COMPLET(dataArr);
                });

                // On cancel
                notice.get().on('pnotify.cancel', function() {
                    //alert('Oh ok. Chicken, I see.');
                });
            } else {
                DEL_MARCH_COMPLET(dataArr, true);
            }
        });
    });

    function DEL_MARCH_COMPLET(key, empty) {
        empty = (typeof empty !== 'undefined') ? empty : false;
        console.log(empty + ' -- ' + key);
        //return true;
        switch (empty) {
            case false:
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_CONT_OF_MARCH', key])).fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                }).done(function(data) {
                    $('#TAB_MIC1').DataTable().ajax.reload();
                    $('#MARCH_MIC').val('');
                    $('#Rubr_MIC').val('');
                    $('#Nombre_MIC').val('');
                    $('#Poids_MIC').val('');
                    $('#TAB_MIC2').DataTable().ajax.reload();
                    $('#VOLUM_MIC').val('');
                    $('#Numero_MICC').val('');
                    $('#Rubrq_MICC').val('');
                    $('#PoidsV_MIC').val('');
                });
                break;

            default:
                $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_EMPTY_MARCH', key])).fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                }).done(function(data) {
                    $('#TAB_MIC1').DataTable().ajax.reload();
                    $('#MARCH_MIC').val('');
                    $('#Rubr_MIC').val('');
                    $('#Nombre_MIC').val('');
                    $('#Poids_MIC').val('');
                    $('#TAB_MIC2').DataTable().ajax.reload();
                    $('#VOLUM_MIC').val('');
                    $('#Numero_MICC').val('');
                    $('#Rubrq_MICC').val('');
                    $('#PoidsV_MIC').val('');
                });
                break;
        }

    }

    $('#MOD_MICC').on('click', function() {
        var dataArr;
        $.each($("#TAB_MIC2 tr.selected"), function() {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP_CONT', {
            'DC_CONTAINER': $('#VOLUM_MIC').val(),
            'DC_NUM_CONTAINER': $('#Numero_MICC').val(),
            'DC_RUBRIQUE': $('#Rubrq_MICC').val(),
            'DC_POID': $('#PoidsV_MIC').val(),
        }, dataArr])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    $('#ADD_MICC').on('click', function() {
        var dataArr;
        $.each($("#TAB_MIC1 tr.selected"), function() {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP_CONT', {
            'DC_CODE_MARCHANDISE': dataArr,
            'DC_CONTAINER': $('#VOLUM_MIC').val(),
            'DC_NUM_CONTAINER': $('#Numero_MICC').val(),
            'DC_RUBRIQUE': $('#Rubrq_MICC').val(),
            'DC_POID': $('#PoidsV_MIC').val(),
        }])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    $('#DEL_MICC').on('click', function() {
        var dataArr;
        $.each($("#TAB_MIC2 tr.selected"), function() {
            dataArr = $(this).find('td').eq(0).text();
        });
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['DEL_CONT', dataArr])).fail(function(data) {
            console.log('fail');
            console.dir(data);
        }).done(function(data) {
            $('#TAB_MIC2').DataTable().ajax.reload();
            $('#VOLUM_MIC').val('');
            $('#Numero_MICC').val('');
            $('#Rubrq_MICC').val('');
            $('#PoidsV_MIC').val('');
        });
    });

    Ladda.bind('#BTN_MOD_MIC', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MODIF_DOS_COMP', {
                'DM_FOURNISSEUR': $('#fournisseurs_MIC').val(),
                'DM_CLIENT': $('#clients_MIC').val(),
                'DM_NUM_BL': $('#BL_MIC').val(),
                'DM_NAVIRE': $('#navire_MIC').val(),
                'DM_POL': $('#port_DEP_MIC').val(),
                'DM_POD': $('#port_ARR_MIC').val(),
                'DM_DATE_EMBARQ': toDate($('#Date_Dep_MIC').val()),
                'DM_DATE_DECHARG': toDate($('#Date_arr_MIC').val()),
                'DM_NOMBRE': $('#NBR_MIC').val(),
                'DM_TERME': $('#term_MIC').val(),
                'DM_MARQUE': $('#marq_MIC').val(),
                'DM_ESCALE': $('#escal_MIC').val(),
                'DM_VAL_DEVISE': $('#VAL_DEV_MIC').val(),
                'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIC').attr("user"),
                'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIC').val()
            }, $('#DOS_NUM_INP').val()])).fail(function(data) {
                console.log('fail');
                console.dir(data);
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'La modification du dossier n\'a pas abouti',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                console.log(data);
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Dossier modifié avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    Ladda.bind('#BTN_ADD_MIC', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            var id = $('#DOS_NUM_INP').val();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_COMP', {
                'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
                'DM_FOURNISSEUR': $('#fournisseurs_MIC').val(),
                'DM_CLIENT': $('#clients_MIC').val(),
                'DM_NUM_BL': $('#BL_MIC').val(),
                'DM_NAVIRE': $('#navire_MIC').val(),
                'DM_POL': $('#port_DEP_MIC').val(),
                'DM_POD': $('#port_ARR_MIC').val(),
                'DM_DATE_EMBARQ': toDate($('#Date_Dep_MIC').val()),
                'DM_DATE_DECHARG': toDate($('#Date_arr_MIC').val()),
                'DM_NOMBRE': $('#NBR_MIC').val(),
                'DM_TERME': $('#term_MIC').val(),
                'DM_MARQUE': $('#marq_MIC').val(),
                'DM_ESCALE': $('#escal_MIC').val(),
                'DM_VAL_DEVISE': $('#VAL_DEV_MIC').val(),
                'DM_CODE_COMP_GROUP': $('select#SEL_CG_DOS').val(),
                'DM_IMP_EXP': $('select#SEL_T_DOS').val(),
                'DM_ATTRIBUER': $('#DM_ATTRIBUER_MIC').attr("user"),
                'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_MIC').val()
            }, id.substr(id.length - 4)])).fail(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'L\'ajout au dossier a échoué',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                console.dir(data);
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Dossier ajouter avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    Ladda.bind('#BTN_SAVE_MIC', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
                $('#DOS_NUM_INP').val('');
                $('#DOS_SELECT_DIV').css("display", "none");
                $('#DIV_MAR').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "none");
                DM_CLEANER();
                instance.stop();
                clearInterval(interval);
            }, 200);
        }
    });

    //AERIEN

    Ladda.bind('#BTN_ADD_AER', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            var id = $('#DOS_NUM_INP').val();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['ADD_DOS_AER', {
                'DM_NUM_DOSSIER': $('#DOS_NUM_INP').val(),
                'DM_FOURNISSEUR': $('#fournisseurs_AER').val(),
                'DM_CLIENT': $('#clients_AER').val(),
                'DM_NUM_LTA': $('#LTA_AER').val(),
                'DM_POL': $('#port_DEP_AER').val(),
                'DM_POD': $('#port_ARR_AER').val(),
                'DM_MARCHANDISE': $('#MARCH_AER').val(),
                'DM_DATE_EMBARQ': toDate($('#Date_Dep_AER').val()),
                'DM_DATE_DECHARG': toDate($('#Date_arr_AER').val()),
                'DM_POIDS': $('#Poids_AER').val(),
                'DM_NOMBRE': $('#Nombre_AER').val(),
                'DM_LONGUEUR': $('#long_AER').val(),
                'DM_LARGEUR': $('#larg_AER').val(),
                'DM_HAUTEUR': $('#haut_AER').val(),
                'DM_MAGASIN': $('#magasin_AER').val(),
                'DM_TERME': $('#term_AER').val(),
                'DM_MARQUE': $('#marq_AER').val(),
                'DM_RUBRIQUE': $('#Rubr_AER').val(),
                'DM_VAL_DEVISE': $('#VAL_DEV_AER').val(),
                'DM_ATTRIBUER': $('#DM_ATTRIBUER_AER').attr("user"),
                'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_AER').val()
            }, id.substr(id.length - 4)])).fail(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'L\'ajout du dossier a échoué',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                console.dir(data);
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Dossier ajouter avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    delay: 1500,
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    Ladda.bind('#BTN_MOD_AER', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
            }, 200);
            var id = $('#DOS_NUM_INP').val();
            $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['MOD_DOS_AER', {
                'DM_FOURNISSEUR': $('#fournisseurs_AER').val(),
                'DM_CLIENT': $('#clients_AER').val(),
                'DM_NUM_LTA': $('#LTA_AER').val(),
                'DM_POL': $('#port_DEP_AER').val(),
                'DM_POD': $('#port_ARR_AER').val(),
                'DM_MARCHANDISE': $('#MARCH_AER').val(),
                'DM_DATE_EMBARQ': toDate($('#Date_Dep_AER').val()),
                'DM_DATE_DECHARG': toDate($('#Date_arr_AER').val()),
                'DM_POIDS': $('#Poids_AER').val(),
                'DM_NOMBRE': $('#Nombre_AER').val(),
                'DM_LONGUEUR': $('#long_AER').val(),
                'DM_LARGEUR': $('#larg_AER').val(),
                'DM_HAUTEUR': $('#haut_AER').val(),
                'DM_MAGASIN': $('#magasin_AER').val(),
                'DM_TERME': $('#term_AER').val(),
                'DM_MARQUE': $('#marq_AER').val(),
                'DM_RUBRIQUE': $('#Rubr_AER').val(),
                'DM_VAL_DEVISE': $('#VAL_DEV_AER').val(),
                'DM_ATTRIBUER': $('#DM_ATTRIBUER_AER').attr("user"),
                'DM_ATTRIBUER_LIB': $('#DM_ATTRIBUER_AER').val()
            }, $('#DOS_NUM_INP').val()])).fail(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'La modification du dossier n\'a pas abouti',
                    addclass: 'alert alert-styled-left bg-danger',
                    type: 'error',
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                console.dir(data);
                instance.stop();
                clearInterval(interval);
            }).done(function(data) {
                new PNotify({
                    title: 'N° DOS : ' + $('#DOS_NUM_INP').val(),
                    text: 'Dossier modifié avec succès',
                    addclass: 'alert alert-styled-left bg-success',
                    type: 'success',
                    before_open: function(PNotify) {
                        PNotify.get().css(get_center_pos(PNotify.get().width()));
                    },
                });
                instance.stop();
                clearInterval(interval);
            });
        }
    });

    Ladda.bind('#BTN_SAVE_AER', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    progress = 0;
                }
                $('#DOS_NUM_INP').val('');
                $('#DOS_SELECT_DIV').css("display", "none");
                $('#DIV_MAR').css("display", "none");
                $('#DIV_MAR_COMP').css("display", "none");
                $('#DIV_AER').css("display", "none");
                DM_CLEANER();
                instance.stop();
                clearInterval(interval);
            }, 200);
        }
    });


    //From Dossier...
    $('#PAA_MIG').on('click', function() {
        var table_mig = $('#TAB_MIG').DataTable();
        if (table_mig.rows('.selected').any()) {
            var dmcle = table_mig.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAA.mrt&dmcle=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_PAA', dmcle])).done(function(data) {
                            //console.log(data);
                            var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=PAA.mrt&dmcle=' + dmcle, '_blank');
                            win.focus();
                        });
                    }
                });
        }
    });

    $('#AA_MIG').on('click', function() {
        var table_mig = $('#TAB_MIG').DataTable();
        if (table_mig.rows('.selected').any()) {
            var dmcle = table_mig.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        }


    });

    $('#MAG_MIG').on('click', function() {
        var table_mig = $('#TAB_MIG').DataTable();
        if (table_mig.rows('.selected').any()) {
            var dmcle = table_mig.rows('.selected').data()[0].DM_CLE;
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['CHECK_PAA', dmcle]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de votre demande', 'danger');
                })
                .done(function(data) {
                    if (Object.keys(data).length) {
                        var win = window.open('/GTRANS/public/Dossier/fact/MAG?DM_CLE=' + dmcle, '_blank');
                        win.focus();
                    } else {
                        PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Le Préavis d\'arrivée pas encore crée !', 'warning');
                    }
                });
        }

    });

    //fin-----------
});