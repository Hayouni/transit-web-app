<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/include/class.iniparser/class.iniparser.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'GET_FC':
        $cfg = new iniParser("../../../sys/config/init.ini");
        $EXPORT = $cfg->get("folder_color","EXPORT");
        $IMPORT = $cfg->get("folder_color","IMPORT");
        $CEXPORT = $cfg->get("folder_color","CEXPORT");
        $CIMPORT = $cfg->get("folder_color","CIMPORT");
        $GEXPORT = $cfg->get("folder_color","GEXPORT");
        $GIMPORT = $cfg->get("folder_color","GIMPORT");
        $UNKNOWN = $cfg->get("folder_color","UNKNOWN");
        $MAPCOL = $cfg->get("MAP","MAPCOL");
        echo json_encode(array('EXPORT' => $EXPORT,'IMPORT' => $IMPORT,'CEXPORT' => $CEXPORT,'CIMPORT' => $CIMPORT,'GEXPORT' => $GEXPORT,'GIMPORT' => $GIMPORT,'UNKNOWN' => $UNKNOWN,'MAPCOL' => $MAPCOL ));
        break;

    case 'SET_FC':
        $cfg = new iniParser("../../../sys/config/init.ini");
        $cfg->setValue("folder_color","EXPORT", "$response[1]");
        $cfg->setValue("folder_color","IMPORT", "$response[1]");
        $cfg->setValue("folder_color","CEXPORT", "$response[1]");
        $cfg->setValue("folder_color","CIMPORT", "$response[1]");
        $cfg->setValue("folder_color","GEXPORT", "$response[1]");
        $cfg->setValue("folder_color","GIMPORT", "$response[1]");
        $cfg->setValue("folder_color","UNKNOWN", "$response[1]");
        break;

    case 'GET_REFTIME':
        $cfg = new iniParser("../../../sys/config/init.ini");
        $STATE = $cfg->get("TBD_REFTIME","STATE");
        $REFSEC = $cfg->get("TBD_REFTIME","REFSEC");
        echo json_encode(array('STATE' => $STATE,'REFSEC' => $REFSEC));
        break;

    case 'SET_REFTIME':
        $cfg = new iniParser("../../../sys/config/init.ini");
        switch ($response[1]) {
            case 'START':
                $cfg->setValue("TBD_REFTIME","STATE", "TRUE");
                break;
            case 'STOP':
                $cfg->setValue("TBD_REFTIME","STATE", "FALSE");
                break;
            default:
                $cfg->setValue("TBD_REFTIME","REFSEC", "$response[1]");
                break;
        }
        echo $cfg->save("../../../sys/config/init.ini");
        break;

    case 'MAP_INFO':
        $db = new MySQL();
        $MAP_INFO = $db->get_results("SELECT
                                            DM_CLE,
                                            DM_NUM_DOSSIER,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP,
                                            DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                            DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                            DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                            p.PO_CODE,
                                            p.PO_LIBELLE AS POD_LIBELLE,
                                            s.N_FR AS POD_PAYS,
                                            p.PO_LAT,
                                            p.PO_LNG,
                                            p.PO_ISO3,
                                            o.PO_LIBELLE AS POL_LIBELLE,
                                            d.N_FR AS POL_PAYS,
                                            NA_LIBELLE 
                                        FROM
                                            trans.dossier_maritime
                                            INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                            INNER JOIN trans.pays s ON p.PO_ISO2 = s.ISO2
                                            INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE
                                            INNER JOIN trans.pays d ON o.PO_ISO2 = d.ISO2
                                            INNER JOIN trans.navire ON DM_NAVIRE = NA_CODE 
                                        WHERE
                                            DM_CLOTURE = 0 
                                            AND DM_CODE_COMP_GROUP IN ( 'C', 'G' ) 
                                        GROUP BY
                                            DM_NUM_DOSSIER UNION ALL
                                            (
                                        SELECT
                                            DM_CLE,
                                            DM_NUM_DOSSIER,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP,
                                            DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                            DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                            DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                            p.AE_CODE AS PO_CODE,
                                            p.AE_LIBELLE AS POD_LIBELLE,
                                            s.N_FR AS POD_PAYS,
                                            p.AE_LAT AS PO_LAT,
                                            p.AE_LNG AS PO_LNG,
                                            p.AE_ISO3 AS PO_ISO3,
                                            o.AE_LIBELLE AS POL_LIBELLE,
                                            d.N_FR AS POL_PAYS,
                                            '' AS NA_LIBELLE 
                                        FROM
                                            trans.dossier_maritime
                                            INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                            INNER JOIN trans.pays s ON p.AE_ISO2 = s.ISO2
                                            INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE
                                            INNER JOIN trans.pays d ON o.AE_ISO2 = d.ISO2 
                                        WHERE
                                            DM_CLOTURE = 0 
                                            AND ( DM_CODE_COMP_GROUP IS NULL OR DM_CODE_COMP_GROUP = '' ) 
                                        GROUP BY
                                            DM_NUM_DOSSIER 
                                            ) 
                                        ORDER BY
                                            STR_TO_DATE( DM_DATE_DECHARG, '%d/%m/%Y' ) DESC");
        echo json_encode($MAP_INFO);
        break;

    case 'MAP_INFO_TAB':
        $db = new MySQL();
        $MAP_INFO["data"] = $db->get_results(" SELECT
                                                    DM_CLE,
                                                    DM_NUM_DOSSIER,
                                                    DM_CODE_COMP_GROUP,
                                                    DM_IMP_EXP,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                                    DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                                    DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                                    p.PO_CODE,
                                                    p.PO_LIBELLE AS POD_LIBELLE,
                                                    s.N_FR AS POD_PAYS,
                                                    p.PO_LAT,
                                                    p.PO_LNG,
                                                    p.PO_ISO3,
                                                    o.PO_LIBELLE AS POL_LIBELLE,
                                                    d.N_FR AS POL_PAYS,
                                                    NA_LIBELLE 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                                    INNER JOIN trans.pays s ON p.PO_ISO2 = s.ISO2
                                                    INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE
                                                    INNER JOIN trans.pays d ON o.PO_ISO2 = d.ISO2
                                                    INNER JOIN trans.navire ON DM_NAVIRE = NA_CODE 
                                                WHERE
                                                    DM_CLOTURE = 0 
                                                    AND DM_CODE_COMP_GROUP IN ( 'C', 'G' ) 
                                                GROUP BY
                                                    DM_NUM_DOSSIER UNION ALL
                                                    (
                                                SELECT
                                                    DM_CLE,
                                                    DM_NUM_DOSSIER,
                                                    DM_CODE_COMP_GROUP,
                                                    DM_IMP_EXP,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                                    DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                                    DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                                    p.AE_CODE AS PO_CODE,
                                                    p.AE_LIBELLE AS POD_LIBELLE,
                                                    s.N_FR AS POD_PAYS,
                                                    p.AE_LAT AS PO_LAT,
                                                    p.AE_LNG AS PO_LNG,
                                                    p.AE_ISO3 AS PO_ISO3,
                                                    o.AE_LIBELLE AS POL_LIBELLE,
                                                    d.N_FR AS POL_PAYS,
                                                    '' AS NA_LIBELLE 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                                    INNER JOIN trans.pays s ON p.AE_ISO2 = s.ISO2
                                                    INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE
                                                    INNER JOIN trans.pays d ON o.AE_ISO2 = d.ISO2 
                                                WHERE
                                                    DM_CLOTURE = 0 
                                                    AND ( DM_CODE_COMP_GROUP IS NULL OR DM_CODE_COMP_GROUP = '' ) 
                                                GROUP BY
                                                    DM_NUM_DOSSIER 
                                                    ) 
                                                ORDER BY
                                                    STR_TO_DATE( DM_DATE_DECHARG, '%d/%m/%Y' ) DESC");
        echo json_encode($MAP_INFO);
        break;

    case 'MAP_INFO_CO':
        $db = new MySQL();
        $MAP_INFO = $db->get_results("SELECT
                                            COUNT( DISTINCT(DM_NUM_DOSSIER) ) AS CO,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP 
                                        FROM
                                            trans.dossier_maritime 
                                        WHERE
                                            DM_CLOTURE = 0 
                                        GROUP BY
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP");
        echo json_encode($MAP_INFO);
        break;

    case 'MAP_INFO_UNK':
        $db = new MySQL();
        $MAP_INFO["data"] = $db->get_results("SELECT
                                                    DM_CLE,
                                                    DM_NUM_DOSSIER,
                                                    DM_CODE_COMP_GROUP,
                                                    DM_IMP_EXP,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                                    DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                                    DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                                    DM_POD,
                                                    DM_POL,
                                                    DM_NAVIRE 
                                                FROM
                                                    trans.dossier_maritime 
                                                WHERE
                                                    DM_CLOTURE = 0 
                                                    AND DM_NUM_DOSSIER NOT IN (
                                                SELECT
                                                    DM_NUM_DOSSIER 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                                    INNER JOIN trans.pays s ON p.PO_ISO2 = s.ISO2
                                                    INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE
                                                    INNER JOIN trans.pays d ON o.PO_ISO2 = d.ISO2
                                                    INNER JOIN trans.navire ON DM_NAVIRE = NA_CODE 
                                                WHERE
                                                    DM_CLOTURE = 0 UNION ALL
                                                SELECT
                                                    DM_NUM_DOSSIER 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                                    INNER JOIN trans.pays s ON p.AE_ISO2 = s.ISO2
                                                    INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE
                                                    INNER JOIN trans.pays d ON o.AE_ISO2 = d.ISO2 
                                                WHERE
                                                    DM_CLOTURE = 0 
                                                    AND ( DM_CODE_COMP_GROUP IS NULL OR DM_CODE_COMP_GROUP = '' ) 
                                                    ) GROUP BY
                                                        DM_NUM_DOSSIER");
        echo json_encode($MAP_INFO);
        break;

    case 'GET_LAS_ADDED':
        $db = new MySQL();
        $GET_LAS_ADDED = $db->get_results("( SELECT
                                                DM_CLE,
                                                DM_NUM_DOSSIER,
                                                p.PO_LIBELLE AS POD_LIBELLE,
                                                o.PO_LIBELLE AS POL_LIBELLE,
                                                DM_CODE_COMP_GROUP,
                                                DM_IMP_EXP,
                                                DM_ATTRIBUER_LIB,
                                                DM_DATE_CREATION 
                                                FROM
                                                    dossier_maritime
                                                    INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                                    INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE 
                                                WHERE
                                                    DM_CODE_COMP_GROUP IN ( 'C', 'G' ) 
                                                GROUP BY
                                                    DM_NUM_DOSSIER 
                                                ORDER BY
                                                    DM_DATE_CREATION DESC 
                                                    LIMIT 7 
                                                    ) UNION ALL
                                                    (
                                                SELECT
                                                    DM_CLE,
                                                    DM_NUM_DOSSIER,
                                                    p.AE_LIBELLE AS POD_LIBELLE,
                                                    o.AE_LIBELLE AS POL_LIBELLE,
                                                    DM_CODE_COMP_GROUP,
                                                    DM_IMP_EXP,
                                                    DM_ATTRIBUER_LIB,
                                                    DM_DATE_CREATION 
                                                FROM
                                                    dossier_maritime
                                                    INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                                    INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE 
                                                WHERE
                                                    DM_CODE_COMP_GROUP IS NULL 
                                                    OR DM_CODE_COMP_GROUP = '' 
                                                ORDER BY
                                                    DM_DATE_CREATION DESC 
                                                    LIMIT 7 
                                                    ) 
                                                ORDER BY
                                                    DM_DATE_CREATION DESC 
                                                    LIMIT 7");
        echo json_encode($GET_LAS_ADDED);
        break; 

    case 'COUNT_ACTIVE_DOS':
        $db = new MySQL();
        $COUNT_ACTIVE_DOS = $db->get_results("SELECT COUNT(DISTINCT(DM_NUM_DOSSIER)) as CO FROM trans.dossier_maritime WHERE DM_CLOTURE = 0")[0]['CO'];
        echo json_encode($COUNT_ACTIVE_DOS);
        break;

    case 'CHART_DEP_DATA':
        $db = new MySQL();
        $CHART_DEP_DATA = $db->get_results("SELECT
                                                DATE_FORMAT( AD_DATE_OPER, '%m' ) AS x,
                                                SUM( AD_TTC ) AS y
                                            FROM
                                                trans.autre_depense 
                                            WHERE
                                                YEAR ( AD_DATE_OPER ) = '$response[1]' 
                                            GROUP BY
                                                DATE_FORMAT( AD_DATE_OPER, '%m' )");
        echo json_encode($CHART_DEP_DATA, JSON_NUMERIC_CHECK);
        break;
        
    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}

/*list des factures non reglé
SELECT
	DM_CLE,
	DM_NUM_DOSSIER 
FROM
	dossier_maritime,
	avis_arrive_mig 
WHERE
	DM_NUM_DOSSIER = AAM_NUM_DOSSIER 
	AND AAM_BON_COMMAND IS NULL OR AAM_BON_COMMAND = ''
*/

/*
*/


/**Old mapd data
 * 
 * case 'MAP_INFO_TAB':
        $db = new MySQL();
        $MAP_INFO_MART = $db->get_results("SELECT
                                            DM_CLE,
                                            DM_NUM_DOSSIER,
                                            DM_CODE_COMP_GROUP,
                                            DM_IMP_EXP,
                                            DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                            DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                            DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                            p.PO_CODE,
                                            p.PO_LIBELLE AS POD_LIBELLE,
                                            s.N_FR AS POD_PAYS,
                                            p.PO_LAT,
                                            p.PO_LNG,
                                            p.PO_ISO3,
                                            o.PO_LIBELLE AS POL_LIBELLE,
                                            d.N_FR AS POL_PAYS,
                                            NA_LIBELLE 
                                        FROM
                                            trans.dossier_maritime
                                            INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                            INNER JOIN trans.pays s ON p.PO_ISO2 = s.ISO2
                                            INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE
                                            INNER JOIN trans.pays d ON o.PO_ISO2 = d.ISO2
                                            INNER JOIN trans.navire ON DM_NAVIRE = NA_CODE
                                        WHERE
                                            DM_CLOTURE = 0
                                        ORDER BY
                                            DATE(DM_DATE_DECHARG) DESC");
        $MAP_INFO_AER = $db->get_results("SELECT
                                                DM_CLE,
                                                DM_NUM_DOSSIER,
                                                DM_CODE_COMP_GROUP,
                                                DM_IMP_EXP,
                                                DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                                DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                                DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                                p.AE_CODE AS PO_CODE,
                                                p.AE_LIBELLE AS POD_LIBELLE,
                                                s.N_FR AS POD_PAYS,
                                                p.AE_LAT AS PO_LAT,
                                                p.AE_LNG AS PO_LNG,
                                                p.AE_ISO3 AS PO_ISO3,
                                                o.AE_LIBELLE AS POL_LIBELLE,
                                                d.N_FR AS POL_PAYS,
                                                '' AS NA_LIBELLE 
                                            FROM
                                                trans.dossier_maritime
                                                INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                                INNER JOIN trans.pays s ON p.AE_ISO2 = s.ISO2
                                                INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE
                                                INNER JOIN trans.pays d ON o.AE_ISO2 = d.ISO2
                                            WHERE
                                                DM_CLOTURE = 0
                                                AND ( DM_CODE_COMP_GROUP IS NULL OR DM_CODE_COMP_GROUP = '' )
                                            ORDER BY
                                                DATE( DM_DATE_DECHARG ) DESC");

        $MAP_INFO["data"] = array_merge($MAP_INFO_MART,$MAP_INFO_AER);
        echo json_encode($MAP_INFO);
        break;
 */

 /**UNK TAB
  * 


  case 'MAP_INFO_UNK':
        $db = new MySQL();
        $MAP_INFO["data"] = $db->get_results("SELECT
                                                    DM_CLE,
                                                    DM_NUM_DOSSIER,
                                                    DM_CODE_COMP_GROUP,
                                                    DM_IMP_EXP,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG,
                                                    DM_DATE_DECHARG AS C_DM_DATE_DECHARG,
                                                    DM_DATE_EMBARQ AS C_DM_DATE_EMBARQ,
                                                    DM_POD,
                                                    DM_POL,
                                                    DM_NAVIRE 
                                                FROM
                                                    trans.dossier_maritime 
                                                WHERE
                                                    DM_CLOTURE = 0 /*DM_NUM_DOSSIER NOT IN ( SELECT AAM_NUM_DOSSIER FROM avis_arrive_mig ) *
                                                    AND DM_NUM_DOSSIER NOT IN (
                                                        SELECT
                                                            DM_NUM_DOSSIER 
                                                        FROM
                                                            trans.dossier_maritime
                                                            INNER JOIN trans.PORT p ON DM_POD = p.PO_CODE
                                                            INNER JOIN trans.pays s ON p.PO_ISO2 = s.ISO2
                                                            INNER JOIN trans.PORT o ON DM_POL = o.PO_CODE
                                                            INNER JOIN trans.pays d ON o.PO_ISO2 = d.ISO2
                                                            INNER JOIN trans.navire ON DM_NAVIRE = NA_CODE 
                                                        WHERE
                                                            DM_CLOTURE = 0 /*DM_NUM_DOSSIER NOT IN ( SELECT AAM_NUM_DOSSIER FROM avis_arrive_mig ) *
                                                            AND DM_CODE_COMP_GROUP IN ( 'C', 'G' )
                                                    ) 
                                                    AND DM_NUM_DOSSIER NOT IN (
                                                        SELECT
                                                            DM_NUM_DOSSIER 
                                                        FROM
                                                            trans.dossier_maritime
                                                            INNER JOIN trans.aeroport p ON DM_POD = p.AE_CODE
                                                            INNER JOIN trans.pays s ON p.AE_ISO2 = s.ISO2
                                                            INNER JOIN trans.aeroport o ON DM_POL = o.AE_CODE
                                                            INNER JOIN trans.pays d ON o.AE_ISO2 = d.ISO2 
                                                        WHERE
                                                            DM_CLOTURE = 0 /*DM_NUM_DOSSIER NOT IN ( SELECT AAM_NUM_DOSSIER FROM avis_arrive_mig ) *
                                                            AND ( DM_CODE_COMP_GROUP IS NULL OR DM_CODE_COMP_GROUP = '' )
                                                    )");
        echo json_encode($MAP_INFO);
        break;
  */
?>