/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

$(document).ready(function() {
    $('#currency-widget').currency();

    function toDate(dateStr) {
        if (dateStr) {
            const [day, month, year] = dateStr.split("/");
            return [year, month, day].join('/');
        } else {
            return '';
        }
    }

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: { "dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25 },
        });
    }
    // Floating labels
    // ------------------------------

    // Variables
    var onClass = "on";
    var showClass = "is-visible";


    // Setup
    $("input:not(.token-input):not(.bootstrap-tagsinput > input), textarea, select").on("checkval change", function() {

        // Define label
        var label = $(this).parents('.form-group-material').children(".control-label");

        // Toggle label
        if (this.value !== "") {
            label.addClass(showClass);
        } else {
            label.removeClass(showClass).addClass('animate');
        }

    }).on("keyup", function() {
        $(this).trigger("checkval");
    }).trigger("checkval").trigger('change');


    // Remove animation on page load
    $(window).on('load', function() {
        $(".form-group-material").children('.control-label.is-visible').removeClass('animate');
    });
    // ------------------------------

    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function(data) {
        console.log('fail' + data);
    }).done(function(data) {
        data.forEach(function(element) {
            $('#LIST_CLIENT_AA').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    }).always(function() {
        try {
            $('#LIST_CLIENT_AA').val(CL_CODE)
        } catch (error) {

        }
    });

    $('#MOD_CLIENT_AA').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['MOD_CLIENT_AA', $('#LIST_CLIENT_AA').val(), $('#LIST_CLIENT_AA option:selected').text(), DM_CLE,AAM_CODE])).done(function(data) {
            try {
                data = JSON.parse(data);
                //console.log(data);
                if (data.RES_AA === '1') { //data.RES_DM === '1' &&
                    location.reload();
                } else {
                    PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
                }
            } catch (error) {
                PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
            }
        });
    });

    //GET PARAMETER
    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_CONT_FACT_SUR'])).done(function(data) {
        data.forEach(function(element) {
            $('#DESC_AA').append('<option value="' + element.LIBELLE + '">' + element.CODE + '</option>');
        }, this);
    });


    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        //dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        /*drawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }*/
    });

    // Basic datatable
    var table = $('#AA_TAB').DataTable({
        select: {
            style: 'single'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        scrollCollapse: true,
        paging: false,
        ajax: {
            url: "/GTRANS/public/Dossier/api/fact.php",
            type: "POST",
            data: function(d) {
                return JSON.stringify({
                    "0": "GET_AA_TAB_DATA",
                    "1": AAM_CODE
                });
            }
        },
        aoColumnDefs: [{ "sClass": "hide_me", "aTargets": [0] }],
        columns: [
            { "data": "AAM_CODE_SUIT" },
            {
                "data": "AAM_LIB_CONTENU",
                "render": function(data, type, row) {
                    if (row.AAM_LIB_CONTENU.indexOf("       ") !== -1) {
                        var part1 = row.AAM_LIB_CONTENU.split(/  +/)[0];
                        var part2 = row.AAM_LIB_CONTENU.split(/  +/)[1];
                        return '<strong>' + part1 + '</strong><span class="label label-primary pull-right">' + part2 + '</span>';
                    } else if (row.AAM_LIB_CONT_SUIT) {
                        return '<strong>' + row.AAM_LIB_CONTENU + '</strong><span class="label label-primary pull-right">' + row.AAM_LIB_CONT_SUIT + '</span>';
                    } else {
                        return '<strong>' + row.AAM_LIB_CONTENU + '</strong>';
                    }
                }
            },
            {
                "data": "AAM_VAL_NON_TAXABLE",
                "render": function(data, type, row) {
                    if (data == '0.000') {
                        return '';
                    } else {
                        return data;
                    }
                }
            },
            {
                "data": "AAM_VAL_TAXABLE",
                "render": function(data, type, row) {
                    if (data == '0.000') {
                        return '';
                    } else {
                        return data;
                    }
                }
            }
        ],
        drawCallback: function() {

        },
        fnDrawCallback: function(oSettings) {
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_TT_AA', AAM_CODE])).done(function(data) {
                $('#T_TAX').html(Number(data[0].TAX).toFixed(3) + ' TND');
                $('#N_TAX').html(Number(data[0].NTAX).toFixed(3) + ' TND');
                $('#T_TVA').html(Number(data[0].TVA).toFixed(3) + ' TND');
                $('#T_TT').html(Number(data[0].TT).toFixed(3) + ' TND');
                $('#TT_INFO').html(Number(data[0].TT).toFixed(3) + ' TND');
                C2L = N2L(Number(data[0].TT).toFixed(3));
                $('#T_TT_L').html('Arrétée la présente a la somme de ' + C2L);
            });
        }
    });

    $('#AA_PRINT_BTN').on('click', function() {
        var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=CAA.mrt&A=' + DM_CLE + ' &B=' + AAM_CODE + ' &C=' + DM_NUM_DOSSIER, '_blank');
        win.focus();
    });

    $('#FACT_PRINT_BTN').on('click', function() {
        var win = window.open('/GTRANS/public/Print/viewer.php?id=FS&A=' + DM_CLE + '&B=' + AAM_CODE, '_blank');
        win.focus();

        /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=FS.mrt&A=' + DM_CLE + ' &B=' + AAM_CODE + ' &C=' + DM_NUM_DOSSIER, '_blank');
        win.focus(); */
    });

    function RETOUR_DE_FONDS_CALC() {
        switch ($('#IN_N_TAX_AA').val()) {
            case '':
                return ((Number($('#IN_TAX_AA').val()) * RETOUR_DE_FONDS) / 100);

            default:
                return ((Number($('#IN_N_TAX_AA').val()) * RETOUR_DE_FONDS) / 100);
        }
    }

    function IsNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $('#ADD_AA').on('click', function() {
        if (IsNumeric($('#IN_N_TAX_AA').val()) || IsNumeric($('#IN_TAX_AA').val())) {
            if ($('#AA_TAB tr > td:contains(' + $('#DESC_AA_N').val() + ')').length == 0) {
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_To_AA', {
                    'AAM_CODE_MIG': AAM_CODE,
                    'AAM_CODE_CONTENU': '', //$('#DESC_AA').val(),
                    'AAM_LIB_CONTENU': $('#DESC_AA_N').val(),
                    'AAM_LIB_CONT_SUIT': $('#DESC_AA_SUIT').val(),
                    'AAM_VAL_NON_TAXABLE': $('#IN_N_TAX_AA').val(),
                    'AAM_VAL_TAXABLE': $('#IN_TAX_AA').val(),
                }])).done(function(data) {
                    update_aa_sum();
                    $('#IN_N_TAX_AA').val('');
                    $('#IN_TAX_AA').val('');
                    $('#DESC_AA_SUIT').val('');
                });
            } else {
                PNotify_Alert('Redondance', $('#DESC_AA_N').val() + ' : déjà calculé !', 'warning');
            }
        }
    });

    $('#DEL_AA').on('click', function() {
        if (table.rows('.selected').any()) {
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['DEL_From_AA', table.rows('.selected').data()[0].AAM_CODE_SUIT]))
                .fail(function(data) {
                    console.log('fail');
                    console.dir(data);
                    PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Une erreur s\'est produite lors du traitement de suppression', 'warning');
                }).done(function(data) {
                    update_aa_sum();
                });
        } else {
            PNotify_Alert('N° DOS : ' + $('#DOS_NUM_INP').val(), 'Aucune donnée sélectionnée dans le tableau', 'danger');
            table.ajax.reload();
        }
    });

    $('#SET_DREST').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_DATE_RESTITUTION', AAM_CODE, $('#INP_DREST').val(), CCCCC]))
            .done(function(data) {
                location.reload();
            });
    });

    $('#SET_FRANSH').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_FRANSHISE', AAM_CODE, $('#INP_FRANSH').val()]))
            .done(function(data) {
                location.reload();
            });
    });

    $('#NEW_NFACT').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_CMPT_FACT']))
            .done(function(data) {
                data = JSON.parse(data);
                $('#INP_NFACT').val(data.CPT);
            });
    });

    $('#NEW_NFACT_SET').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_NUM_FACT', AAM_CODE, $('#INP_NFACT').val()]))
            .done(function(data) {
                location.reload();
            });
    });
    /*$('#SET_CAA').on('click', function() {
        if ($('#INP_CAA').val()) {
            swal({
                    title: "Changement de compteur avis d'arrivér",
                    text: "Après confirmation, le compteur sera : " + $('#INP_CAA').val(),
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonColor: "#1565C0",
                    confirmButtonText: "Confirmer",
                    cancelButtonText: "Annuler",
                    showLoaderOnConfirm: true
                },
                function() {
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_CPT_AA', (Number($('#INP_CAA').val()) - 1)]))
                        .done(function(data) {
                            swal({
                                title: "Le compteur est changé avec succès",
                                type: "success",
                                confirmButtonColor: "#2E7D32",
                                confirmButtonText: "Fermer",
                            });
                            $('#SP_CAA_INFO').html(Number(data.NCPT) + 1);
                        }).fail(function(data) {
                            swal({
                                title: "Erreur de changement de compteur",
                                type: "error",
                                confirmButtonColor: "#C62828",
                                confirmButtonText: "Fermer",
                            });
                        });
                });
        }
    });*/

    function update_aa_sum() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_SUM', AAM_CODE, '']))
            .fail(function(data) {
                PNotify_Alert('Avis D\'ariivée', 'Problème mis a jours somme !', 'danger');
            }).done(function(data) {
                var N2L_val = '';
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_TT_AA', AAM_CODE])).done(function(data) {
                    N2L_val = N2L(Number(data[0].TT).toFixed(3));
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_TT_AA_LET', {
                        'AA_TTC_LETTRE': N2L_val
                    }, AAM_CODE])).done(function(data) {
                        table.ajax.reload();
                    });
                });
            });
    }

    $('#SEARCH_NAA').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SERACH_NUM_AA', $('#INP_NAA').val()]))
            .done(function(data) {
                if (Object.keys(data).length) {
                    switch (data[0].AAM_G_C) {
                        case 'G':
                            var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + data[0].AAM_CODE_DOSSIER, '_blank');
                            win.focus();
                            break;

                        case 'C':
                            console.log('C :' + data[0].AAM_CODE_DOSSIER);
                            break;

                        default:
                            console.log('A :' + data[0].AAM_CODE_DOSSIER);
                            break;
                    }
                } else {
                    PNotify_Alert('Avis D\'ariivée', 'Avis d\'arrivée non trouvée !', 'warning');
                }
            }).fail(function() { //delay: 2000,
                PNotify_Alert('Avis D\'ariivée', 'Avis d\'arrivée non trouvée !', 'warning');
            });
    });

    $('#SEARCH_NFACT').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SERACH_NUM_FACT', $('#INP_NFACT').val()]))
            .done(function(data) {
                if (Object.keys(data).length) {
                    switch (data[0].AAM_G_C) {
                        case 'G':
                            var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + data[0].AAM_CODE_DOSSIER, '_blank');
                            win.focus();
                            break;

                        case 'C':
                            console.log('C :' + data[0].AAM_CODE_DOSSIER);
                            break;

                        default:
                            console.log('A :' + data[0].AAM_CODE_DOSSIER);
                            break;
                    }
                } else {
                    PNotify_Alert('Avis D\'ariivée', 'Avis d\'arrivée non trouvée !', 'warning');
                }
            }).fail(function() { //delay: 2000,
                PNotify_Alert('Avis D\'arrivée', 'Avis d\'arrivée non trouvée !', 'warning');
            });
    });

    $('#GET_FACT_DATE').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_DATE_TIME']))
            .done(function(data) {
                $('#INP_DATEFACT').val(data.date);
            }).fail(function(data) {
                console.log(data);
            });
    });

    $('#SET_FATE_FACT').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_DATE_FACT', { 'AAM_DATE_FACTURE': $('#INP_DATEFACT').val() }, AAM_CODE]))
            .done(function(data) {
                location.reload();
            }).fail(function(data) {
                PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
            });
    });

    $('#REG_BTN').on('click', function() {
        var sel = $("ul.nav-tabs li.active a").attr('href');
        switch (sel) {
            case '#BC':
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'BC',
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_DATE_REG': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;
            case '#ESPECE':
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'ESPECE',
                        'AAM_DATE_REG': $('#INP_F_ESPECE_DATE').val(),
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;

            case '#CHEQUE':
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'CHEQUE',
                        'AAM_NUM_CHEQUE': $('#INP_F_CHEQUE_Num').val(),
                        'AAM_BANQUE': $('#INP_F_CHEQUE_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_CHEQUE_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_CHEQUE_Num').val() + 'Banque : ' + $('#INP_F_CHEQUE_Banq').val()
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;

            case '#TRAITE':
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'TRAITE',
                        'AAM_NUM_CHEQUE': $('#INP_F_TRAITE_Num').val(),
                        'AAM_BANQUE': $('#INP_F_TRAITE_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_TRAITE_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_TRAITE_Num').val() + 'Banque : ' + $('#INP_F_TRAITE_Banq').val()
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;

            case '#VIREMENT':
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'VIREMENT',
                        'AAM_NUM_CHEQUE': $('#INP_F_VIREMENT_Num').val(),
                        'AAM_BANQUE': $('#INP_F_VIREMENT_Banq').val(),
                        'AAM_DATE_REG': $('#INP_F_VIREMENT_DATE').val(),
                        'AAM_LIB_CHEQUE_TRAITE': 'N° ' + $('#INP_F_VIREMENT_Num').val() + 'Banque : ' + $('#INP_F_VIREMENT_Banq').val()
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;
            default:
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_REGLEMENT', {
                        'AAM_BON_COMMAND': 'BC',
                        'AAM_NUM_CHEQUE': '',
                        'AAM_BANQUE': '',
                        'AAM_DATE_REG': '',
                        'AAM_LIB_CHEQUE_TRAITE': ''
                    }, AAM_CODE]))
                    .fail(function(data) {
                        PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                    }).done(function(data) {
                        location.reload();
                    });
                break;
        }
    });



    $('#CONV_L_DEV').on('click', function() {
        if (IsNumeric($('#INP_VAL_DEV').val()) && IsNumeric($('#INP_TAUX_DEV').val())) {
            $('#INP_RES_DEV').val((Number($('#INP_VAL_DEV').val()) * Number($('#INP_TAUX_DEV').val())).toFixed(3));
        } else {
            PNotify_Alert('Convert. devises local', 'les valeurs non numerique !', 'warning');
        }
    });
});