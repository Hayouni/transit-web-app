<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'CHECK_PAA':
        $db = new MySQL();
        $CHECK_PAA = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER = '$response[1]' AND (AAM_TYPE_FACT = 'T' OR AAM_TYPE_FACT = 'A')");
        echo json_encode($CHECK_PAA);
        break;

    case 'CHECK_MAG':
        $db = new MySQL();
        $CHECK_HAS_FACT = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER = '$response[1]' AND AAM_TYPE_FACT = 'M'");
            if (count($CHECK_HAS_FACT)) {
                echo json_encode($CHECK_HAS_FACT);
            } else {
                $CPT = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM avis_arrive_mig")[0]['CPT'];
                //$GET_CONTAINER_INFO = $db->get_results("SELECT * FROM dossier_container WHERE DC_CODE_MARCHANDISE IN (SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[5]' ) AND DC_NUM_CONTAINER = '$response[3]'");
                $GET_PAA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
                $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = ".$GET_PAA_INFO[0]["DM_CLIENT"]);
                $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
                $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
                $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.PO_CODE = ".$GET_PAA_INFO[0]["DM_POL"]);
                $DM_POD_LIB = $ALL_PO[0]["POD"];
                $DM_POL_LIB = $ALL_PO[0]["POL"];
                $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = ".$GET_PAA_INFO[0]["DM_FOURNISSEUR"])[0]["FR_LIBELLE"];
                $DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = ".$GET_PAA_INFO[0]["DM_NAVIRE"])[0]["NA_LIBELLE"];
                $INSET_DATA = array(
                    'AAM_CODE' => $CPT,
                    'AAM_CODE_DOSSIER' => $GET_PAA_INFO[0]["DM_CLE"],
                    'AAM_DATE' => date('Y-m-d'),
                    'AAM_CODE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
                    'AAM_DES_CLIENT' => $DM_CLIENT_LIB,
                    'AAM_ADR_CLIENT' => $DM_CLIENT_ADDR,
                    'AAM_NAVIRE' => $DM_NAVIRE_LIB,
                    'AAM_PORT_EMBARQ' => $DM_POL_LIB,
                    'AAM_PORT_DECHAG' => $DM_POD_LIB,
                    'AAM_DATE_DEBARQ' => $GET_PAA_INFO[0]["DM_DATE_DECHARG"],
                    'AAM_FOURNISSEUR' => $DM_FOURNISSEUR_LIB,
                    'AAM_MARCHANDISE' => $GET_PAA_INFO[0]["DM_MARCHANDISE"],
                    'AAM_NBR_COLIS' => $GET_PAA_INFO[0]["DM_NOMBRE"],
                    'AAM_MARQUE' => $GET_PAA_INFO[0]["DM_MARQUE"],
                    'AAM_ESCALE' => $GET_PAA_INFO[0]["DM_ESCALE"],
                    'AAM_RUBRIQUE' => $GET_PAA_INFO[0]["DM_RUBRIQUE"],
                    'AAM_IMP_EXP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
                    'AAM_TYPE_FACT' => 'M',
                    'AAM_BON_COMMAND' => 'BC',
                    'AAM_G_C' => $GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"],
                    'AAM_CLE_FOURNISSSEUR' => $GET_PAA_INFO[0]["DM_FOURNISSEUR"],
                    'AAM_CLE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
                    'AAM_NUM_DOSSIER' => $GET_PAA_INFO[0]["DM_NUM_DOSSIER"],
                    'AAM_MARIT_AERIEN' => (empty($GET_PAA_INFO[0]["DM_NUM_LTA"])) ? 'MAR' : 'AER' 
                );
                $res = $db->insert( 'avis_arrive_mig', $INSET_DATA );
                echo json_encode(array('RES' => $res, 'F_CLE' => $CPT));
                //echo json_encode('{"RES":"'.$res.'"},{"PAA_CLE","'.$CPT.'"}');
            }
        break;

    case 'CHECK_HAS_FACT_S':
        $db = new MySQL();
        if (isset($response[2])) {
            if ($response[2] == 'S_CONT') {
                $CHECK_HAS_FACT = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_NUM_DOSSIER = '$response[1]' AND AAM_NUM_CONTENEUR = '$response[3]'");        
                if (count($CHECK_HAS_FACT)) {
                    $update_where_AA= array( 'AAM_CODE' => $CHECK_HAS_FACT[0]['AAM_CODE']);
                    $rrss = $db->update( 'avis_arrive_mig', array('AAM_DATE_RESTITUTION' => $response[6]), $update_where_AA, 1 );
                    if ($rrss) {
                        echo json_encode($CHECK_HAS_FACT);
                        $GET_CONTAINER_INFO = $db->get_results("SELECT * FROM dossier_container WHERE DC_CODE_MARCHANDISE IN (SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[5]' ) AND DC_NUM_CONTAINER = '$response[3]'");
                        $ups = $db->query("UPDATE dossier_container SET DC_DATE_R = '$response[6]' WHERE DC_CODE = ".$GET_CONTAINER_INFO[0]['DC_CODE']);
                        return true;
                    }
                }         
            } else {
                $CHECK_HAS_FACT = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER = '$response[1]' AND AAM_NUM_CONTENEUR = '$response[3]' AND AAM_DATE_RESTITUTION IS NOT NULL");
            }
            if (count($CHECK_HAS_FACT)) {
                echo json_encode($CHECK_HAS_FACT);
            } else {
                if ($response[2] !== 'S_CONT') return false;
                $CPT = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM avis_arrive_mig")[0]['CPT'];
                $GET_CONTAINER_INFO = $db->get_results("SELECT * FROM dossier_container WHERE DC_CODE_MARCHANDISE IN (SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[5]' ) AND DC_NUM_CONTAINER = '$response[3]'");
                $GET_PAA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
                $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = ".$GET_PAA_INFO[0]["DM_CLIENT"]);
                $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
                $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
                $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.PO_CODE = ".$GET_PAA_INFO[0]["DM_POL"]);
                $DM_POD_LIB = $ALL_PO[0]["POD"];
                $DM_POL_LIB = $ALL_PO[0]["POL"];
                $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = ".$GET_PAA_INFO[0]["DM_FOURNISSEUR"])[0]["FR_LIBELLE"];
                $DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = ".$GET_PAA_INFO[0]["DM_NAVIRE"])[0]["NA_LIBELLE"];
                $INSET_DATA = array(
                    'AAM_CODE' => $CPT,
                    'AAM_CODE_DOSSIER' => $GET_PAA_INFO[0]["DM_CLE"],
                    'AAM_DATE' => date('Y-m-d'),
                    'AAM_CODE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
                    'AAM_DES_CLIENT' => $DM_CLIENT_LIB,
                    'AAM_ADR_CLIENT' => $DM_CLIENT_ADDR,
                    'AAM_NAVIRE' => $DM_NAVIRE_LIB,
                    'AAM_PORT_EMBARQ' => $DM_POL_LIB,
                    'AAM_PORT_DECHAG' => $DM_POD_LIB,
                    'AAM_DATE_DEBARQ' => $GET_PAA_INFO[0]["DM_DATE_DECHARG"],
                    'AAM_FOURNISSEUR' => $DM_FOURNISSEUR_LIB,
                    'AAM_MARCHANDISE' => $response[4],//$GET_PAA_INFO[0]["DM_MARCHANDISE"],
                    'AAM_NBR_COLIS' => $GET_PAA_INFO[0]["DM_NOMBRE"],
                    'AAM_MARQUE' => $GET_PAA_INFO[0]["DM_MARQUE"],
                    'AAM_POIDS' => $GET_CONTAINER_INFO[0]["DC_POID"],
                    'AAM_ESCALE' => $GET_PAA_INFO[0]["DM_ESCALE"],
                    'AAM_RUBRIQUE' => $GET_PAA_INFO[0]["DM_RUBRIQUE"],
                    'AAM_IMP_EXP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
                    'AAM_TYPE_FACT' => 'S',
                    'AAM_BON_COMMAND' => 'BC',
                    'AAM_G_C' => $GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"],
                    'AAM_CLE_FOURNISSSEUR' => $GET_PAA_INFO[0]["DM_FOURNISSEUR"],
                    'AAM_CLE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
                    'AAM_NUM_DOSSIER' => $GET_PAA_INFO[0]["DM_NUM_DOSSIER"],
                    'AAM_VOLUME_CONTENEUR'=> $GET_CONTAINER_INFO[0]["DC_CONTAINER"],
                    'AAM_NUM_CONTENEUR' => $response[3],
                    'AAM_DATE_RESTITUTION' => $response[6],
                    'AAM_MARIT_AERIEN' => 'MAR'
                );
                $res = $db->insert( 'avis_arrive_mig', $INSET_DATA );
                $ups = $db->query("UPDATE dossier_container SET DC_DATE_R = '$response[6]' WHERE DC_CODE = ".$GET_CONTAINER_INFO[0]['DC_CODE']);
                echo json_encode(array('RES' => $res, 'F_CLE' => $CPT));
                //echo json_encode('{"RES":"'.$res.'"},{"PAA_CLE","'.$CPT.'"}');
            }
        } else {
            $CHECK_HAS_FACT = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER = '$response[1]'");
            echo json_encode($CHECK_HAS_FACT);
        }
        
        /*$CHECK_HAS_FACT = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER = '$response[1]'");
        echo json_encode($CHECK_HAS_FACT);*/
        break;

    case 'ADD_PAA':
        $db = new MySQL();
        $CPT = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM avis_arrive_mig")[0]['CPT'];
        $GET_PAA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
        $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = ".$GET_PAA_INFO[0]["DM_CLIENT"]);
        $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
        $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
        $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.PO_CODE = ".$GET_PAA_INFO[0]["DM_POL"]);
        $DM_POD_LIB = $ALL_PO[0]["POD"];
        $DM_POL_LIB = $ALL_PO[0]["POL"];
        $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = ".$GET_PAA_INFO[0]["DM_FOURNISSEUR"])[0]["FR_LIBELLE"];
        $DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = ".$GET_PAA_INFO[0]["DM_NAVIRE"])[0]["NA_LIBELLE"];
        $INSET_DATA = array(
            'AAM_CODE' => $CPT,
            'AAM_CODE_DOSSIER' => $GET_PAA_INFO[0]["DM_CLE"],
            'AAM_DATE' => date('Y-m-d'),
            'AAM_CODE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
            'AAM_DES_CLIENT' => $DM_CLIENT_LIB,
            'AAM_ADR_CLIENT' => $DM_CLIENT_ADDR,
            'AAM_NAVIRE' => $DM_NAVIRE_LIB,
            'AAM_PORT_EMBARQ' => $DM_POL_LIB,
            'AAM_PORT_DECHAG' => $DM_POD_LIB,
            'AAM_DATE_DEBARQ' => $GET_PAA_INFO[0]["DM_DATE_DECHARG"],
            'AAM_FOURNISSEUR' => $DM_FOURNISSEUR_LIB,
            'AAM_MARCHANDISE' => $GET_PAA_INFO[0]["DM_MARCHANDISE"],
            'AAM_NBR_COLIS' => $GET_PAA_INFO[0]["DM_NOMBRE"],
            'AAM_MARQUE' => $GET_PAA_INFO[0]["DM_MARQUE"],
            'AAM_POIDS' => $GET_PAA_INFO[0]["DM_POIDS"],
            'AAM_ESCALE' => $GET_PAA_INFO[0]["DM_ESCALE"],
            'AAM_RUBRIQUE' => $GET_PAA_INFO[0]["DM_RUBRIQUE"],
            'AAM_IMP_EXP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
            'AAM_TYPE_FACT' => 'T',
            'AAM_BON_COMMAND' => 'BC',
            'AAM_G_C' => $GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"],
            'AAM_CLE_FOURNISSSEUR' => $GET_PAA_INFO[0]["DM_FOURNISSEUR"],
            'AAM_CLE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
            'AAM_NUM_DOSSIER' => $GET_PAA_INFO[0]["DM_NUM_DOSSIER"],
            'AAM_MARIT_AERIEN' => 'MAR'
        );
        $res = $db->insert( 'avis_arrive_mig', $INSET_DATA );
        $res2 = $db->query("UPDATE dossier_container SET DC_DATE_A = '".date('Y-m-d')."' WHERE DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = ".$GET_PAA_INFO[0]["DM_NUM_DOSSIER"].")");
        echo json_encode('{"RES":"'.$res.'"},{"PAA_CLE","'.$CPT.'"},{"Container_update","'.$res2.'"}');
        break;

    case 'ADD_PAA_AER':
        $db = new MySQL();
        $CPT = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM avis_arrive_mig")[0]['CPT'];
        $GET_PAA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
        $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = ".$GET_PAA_INFO[0]["DM_CLIENT"]);
        $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
        $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
        $ALL_PO = $db->get_results("SELECT a.AE_LIBELLE AS POD,c.AE_LIBELLE AS POL FROM trans.aeroport a ,trans.aeroport c WHERE a.AE_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.AE_CODE = ".$GET_PAA_INFO[0]["DM_POL"]);
        $DM_POD_LIB = $ALL_PO[0]["POD"];
        $DM_POL_LIB = $ALL_PO[0]["POL"];
        $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = ".$GET_PAA_INFO[0]["DM_FOURNISSEUR"])[0]["FR_LIBELLE"];
        $DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = ".$GET_PAA_INFO[0]["DM_NAVIRE"])[0]["NA_LIBELLE"];
        $INSET_DATA = array(
            'AAM_CODE' => $CPT,
            'AAM_CODE_DOSSIER' => $GET_PAA_INFO[0]["DM_CLE"],
            'AAM_DATE' => date('Y-m-d'),
            'AAM_CODE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
            'AAM_DES_CLIENT' => $DM_CLIENT_LIB,
            'AAM_ADR_CLIENT' => $DM_CLIENT_ADDR,
            'AAM_AEROPOR_EMBARQ' => $DM_POL_LIB,
            'AAM_AEROPOR_DECHAG' => $DM_POD_LIB,
            'AAM_DATE_DEBARQ' => $GET_PAA_INFO[0]["DM_DATE_DECHARG"],
            'AAM_FOURNISSEUR' => $DM_FOURNISSEUR_LIB,
            'AAM_MARCHANDISE' => $GET_PAA_INFO[0]["DM_MARCHANDISE"],
            'AAM_NBR_COLIS' => $GET_PAA_INFO[0]["DM_NOMBRE"],
            'AAM_MARQUE' => $GET_PAA_INFO[0]["DM_MARQUE"],
            'AAM_POIDS' => $GET_PAA_INFO[0]["DM_POIDS"],
            'AAM_ESCALE' => $GET_PAA_INFO[0]["DM_ESCALE"],
            'AAM_RUBRIQUE' => $GET_PAA_INFO[0]["DM_RUBRIQUE"],
            'AAM_IMP_EXP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
            'AAM_TYPE_FACT' => 'A',
            'AAM_BON_COMMAND' => 'BC',
            'AAM_G_C' => $GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"],
            'AAM_CLE_FOURNISSSEUR' => $GET_PAA_INFO[0]["DM_FOURNISSEUR"],
            'AAM_CLE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
            'AAM_NUM_DOSSIER' => $GET_PAA_INFO[0]["DM_NUM_DOSSIER"],
            'AAM_MARIT_AERIEN' => 'AER'
        );
        $res = $db->insert( 'avis_arrive_mig', $INSET_DATA );
        //$res2 = $db->query("UPDATE dossier_container SET DC_DATE_A = '".date('Y-m-d')."' WHERE DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = ".$GET_PAA_INFO[0]["DM_NUM_DOSSIER"].")");
        echo json_encode('{"RES":"'.$res.'"},{"PAA_CLE","'.$CPT.'"}');//,{"Container_update","'.$res2.'"}
        break;

    case 'GET_TVA':
        $db = new MySQL();
        $GET_TVA = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '3'");
        echo json_encode($GET_TVA);
        break;

    case 'GET_TIMBRE':
        $db = new MySQL();
        $GET_TIMBRE = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '4'");
        echo json_encode($GET_TIMBRE);
        break;

    case 'GET_CONT_FACT':
        $db = new MySQL();
        $GET_CONT_FACT = $db->get_results("SELECT CODE, LIBELLE FROM cont_fact_normal");
        echo json_encode($GET_CONT_FACT);
        break;

    case 'GET_CONT_FACT_MAG':
        $db = new MySQL();
        $GET_CONT_FACT = $db->get_results("SELECT CODE, LIBELLE FROM cont_fact_magasinage");
        echo json_encode($GET_CONT_FACT);
        break;

    case 'GET_CONT_FACT_SUR':
        $db = new MySQL();
        $GET_CONT_FACT_SUR = $db->get_results("SELECT CODE, LIBELLE FROM cont_fact_surestarie");
        echo json_encode($GET_CONT_FACT_SUR);
        break;

    case 'GET_AA_TAB_DATA':
        $db = new MySQL();
        $GET_AA_TAB_DATA["data"] = $db->get_results("SELECT * FROM avis_arrive_mig_suite WHERE AAM_CODE_MIG = '$response[1]' ORDER BY AAM_CODE_CONTENU ASC");
        echo json_encode($GET_AA_TAB_DATA);
        break;

    case 'GET_TT_AA':
        $db = new MySQL();
        $tt = $db->get_results("SELECT
                                    AAM_TOT_NON_TAXABLE AS NTAX,
                                    AAM_TO_TAXABLE AS TAX,
                                    AAM_TVA_PORCENTAGE AS TVA,
                                    AAM_TVA_VAL AS TVA_VAL,
                                    AAM_TIMBRE AS TIMBER_VAL,
                                    AAM_TOTAL_TTC AS TT 
                                FROM
                                     avis_arrive_mig
                                WHERE
                                    AAM_CODE = '$response[1]'");
        echo json_encode($tt);
        break;

    case 'GET_TT_AA_OLD': //NOT USED
        $db = new MySQL();
        $tt = $db->get_results("SELECT COALESCE
                                    ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) AS NTAX,
                                    COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) AS TAX,
                                    ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) AS TVA, 
                                    ( COALESCE ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) + COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) + ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) + ( z.VALEUR ) ) AS TT,
                                    t.VALEUR AS TVA_VAL,
                                    z.VALEUR AS TIMBER_VAL 
                                FROM
                                    avis_arrive_mig_suite,
                                    table_pourcentage t,
                                    table_pourcentage z 
                                WHERE
                                    AAM_CODE_MIG = '$response[1]'
                                    AND t.CODE = 3 
                                    AND z.CODE = 4");
        echo json_encode($tt);
        break;

    case 'ADD_To_AA':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(AAM_CODE_SUIT) + 1 as CPT FROM avis_arrive_mig_suite")[0]['CPT'];
        $response[1]['AAM_CODE_SUIT'] = $key;
        echo $db->insert( 'avis_arrive_mig_suite', $response[1]);
        break;

    case 'DEL_From_AA':
        $db = new MySQL();
        $Del_where = array('AAM_CODE_SUIT' => $response[1]);
        echo $db->delete( 'avis_arrive_mig_suite', $Del_where, 1 );
        break;

    case 'UP_AA_SUM':
        $db = new MySQL();
        $tt = $db->get_results("SELECT COALESCE
                                    ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) AS NTAX,
                                    COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) AS TAX,
                                    ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) AS TVA, 
                                    ( COALESCE ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) + COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) + ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) + ( z.VALEUR ) ) AS TT,
                                    t.VALEUR AS TVA_VAL,
                                    z.VALEUR AS TIMBER_VAL 
                                FROM
                                    avis_arrive_mig_suite,
                                    table_pourcentage t,
                                    table_pourcentage z 
                                WHERE
                                    AAM_CODE_MIG = '$response[1]'
                                    AND t.CODE = 3 
                                    AND z.CODE = 4");
        $update_AA = array( 'AAM_TOT_NON_TAXABLE' => $tt[0]["NTAX"], 
                            'AAM_TO_TAXABLE' => $tt[0]["TAX"], 
                            'AAM_TVA_PORCENTAGE' => $tt[0]["TVA"],
                            'AAM_TVA_VAL' => $tt[0]["TVA_VAL"],
                            'AAM_TIMBRE' => $tt[0]["TIMBER_VAL"],
                            'AAM_TOTAL_TTC' => $tt[0]["TT"],
                            'AA_TTC_LETTRE' => $response[2]
                        );
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        break;

    case 'SET_TT_AA_LET': //NOT COMPLETED
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        break;

    case 'GET_CMPT_AA':
        $db = new MySQL();
        $GET_CMPT_AA = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_avis_arrive")[0]['CPT'];
        $res = '{"CPT" : "'.date("Y").'/'.sprintf( "%04d", $GET_CMPT_AA).'"}';
        echo json_encode($res);
        break;

    case 'SET_NUM_AA':
        $db = new MySQL();
        $update_AA = array( 'AAM_NUMERO' => $response[2], 'AAM_DATE' => date('Y-m-d'));
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        $res = $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        if($res == 1){
            $NUM = explode("/", $response[2])[1];
            $user_data = array('NUMERO' => $NUM);
            echo $db->insert( 'compteur_avis_arrive', $user_data );
        }else {
            echo "error";
        }
        break;

    case 'SET_DATE_RESTITUTION':
        $db = new MySQL();
        $update_AA = array( 'AAM_DATE_RESTITUTION' => $response[2]);
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        echo $db->query("UPDATE dossier_container SET DC_DATE_R = '$response[2]' WHERE DC_CODE = $response[3]");        
        break;

    case 'SET_FRANSHISE':
        $db = new MySQL();
        $update_AA = array( 'AAM_FRANCHISE' => $response[2]);
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        break;

    case 'SERACH_NUM_AA':
        $db = new MySQL();
        $SERACH_NUM_AA = $db->get_results("SELECT AAM_CODE_DOSSIER,AAM_G_C FROM avis_arrive_mig WHERE AAM_NUMERO = '$response[1]'");
        echo json_encode($SERACH_NUM_AA);
        break;

    case 'GET_CMPT_FACT':
        $db = new MySQL();
        $GET_CMPT_FACT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_facture")[0]['CPT'];
        $res = '{"CPT" : "'.date("Y").'/'.sprintf( "%04d", $GET_CMPT_FACT).'"}';
        echo json_encode($res);
        break;

    case 'SET_NUM_FACT':
        $db = new MySQL();
        $update_AA = array( 'AAM_NUM_FACTURE' => $response[2]);
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        $res = $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        if($res == 1){
            $NUM = explode("/", $response[2])[1];
            $user_data = array('NUMERO' => $NUM);
            echo $db->insert( 'compteur_facture', $user_data );
        }else {
            echo "error";
        }
        break;

    case 'SERACH_NUM_FACT':
        $db = new MySQL();
        $SERACH_NUM_FACT = $db->get_results("SELECT AAM_CODE_DOSSIER,AAM_G_C FROM avis_arrive_mig WHERE AAM_NUM_FACTURE = '$response[1]'");
        echo json_encode($SERACH_NUM_FACT);
        break;

    case 'MOD_CLIENT_AA':
        $db = new MySQL();
        $CL_CODE = $response[1];
        $CL_LIBEL = $response[2];
        $DM_CLE = $response[3];
        $AAM_CODE = $response[4];
        $CL_ADDRESS = $db->get_results("SELECT CL_ADRESSE from trans.client WHERE CL_CODE = '$CL_CODE'")[0]['CL_ADRESSE'];
        //MODIF DOSSIER
        //$update_DM = array( 'DM_CLIENT' => $CL_CODE);
        //$update_where_DM = array( 'DM_CLE' => $DM_CLE);
        //$res1 = $db->update( 'dossier_maritime', $update_DM, $update_where_DM, 1 );
        //MODIF DOSSIER
        $update_AA = array( 'AAM_CODE_CLIENT' => $CL_CODE, 'AAM_DES_CLIENT' => $CL_LIBEL, 'AAM_ADR_CLIENT' => $CL_ADDRESS);
        $update_where_AA= array( 'AAM_CODE' => $AAM_CODE);
        $res2 = $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA );

        echo json_encode('{"RES_AA" : "'.$res2.'"}');
        break;

    case 'UP_AA_REGLEMENT':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        break;

    case 'SET_DATE_FACT':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        if (isset($response[3])){
            echo $db->query("UPDATE dossier_container SET DC_DATE_T = '$response[3]' WHERE DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = $response[4])");
        }
        break;

    case 'GET_DATE_TIME':
        $date = date('d/m/Y');
        $time = date('H:i');
        $res = array('date' => $date, 'time' =>$time);
        echo json_encode($res);
        break;

    case 'GET_TYPEHEAD_CLIENT_MAIL':
        $db = new MySQL();
        $res = $db->get_results("SELECT CL_EMAIL FROM client WHERE CL_EMAIL <> '' AND CL_EMAIL IS NOT NULL");
        $out = [];
        foreach ($res as $key => $value) {
           array_push($out, $value['CL_EMAIL']);
        }
        echo json_encode($out);
        break;

    // INVOICE
    case 'GET_CONT_FACT_INV':
        $db = new MySQL();
        $GET_CONT_FACT_INV = $db->get_results("SELECT CODE, LIBELLE FROM cont_invoices");
        echo json_encode($GET_CONT_FACT_INV);
        break;

    case 'GET_AA_TAB_DATA_INV':
        $db = new MySQL();
        $GET_AA_TAB_DATA_INV["data"] = $db->get_results("SELECT * FROM invoice_suite WHERE AAM_CODE_MIG = '$response[1]' ORDER BY AAM_CODE_CONTENU ASC");
        echo json_encode($GET_AA_TAB_DATA_INV);
        break;

    case 'ADD_To_INV':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(AAM_CODE_SUIT) + 1 as CPT FROM invoice_suite")[0]['CPT'];
        $response[1]['AAM_CODE_SUIT'] = $key;
        echo $db->insert( 'invoice_suite', $response[1]);
        break;
    
    case 'DEL_From_INV':
        $db = new MySQL();
        $Del_where = array('AAM_CODE_SUIT' => $response[1]);
        echo $db->delete( 'invoice_suite', $Del_where, 1 );
        break;

    case 'GET_TT_INV':
        $db = new MySQL();
        $tt = $db->get_results("SELECT
                                    AAM_DEVISE,
                                    AAM_TOTAL_TTC AS TT
                                FROM
                                    invoice 
                                WHERE
                                    AAM_CODE = '$response[1]'");
        echo json_encode($tt);
        break;

    case 'SET_NUM_FACT_INV':
        $db = new MySQL();
        $update_AA = array( 'AAM_NUM_FACTURE' => $response[2]);
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        $res = $db->update( 'invoice', $update_AA, $update_where_AA, 1 );
        if($res == 1){
            $NUM = explode("/", $response[2])[1];
            $user_data = array('NUMERO' => $NUM);
            echo $db->insert( 'compteur_facture', $user_data );
        }else {
            echo "error";
        }
        break;
    
    case 'SET_DATE_FACT_INV':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'invoice', $response[1], $update_where_AA );
        //echo $db->query("UPDATE dossier_container SET DC_DATE_T = '$response[3]' WHERE DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = $response[4])");
        break;

    case 'UP_AA_SUM_INV':
        $db = new MySQL();
        $tt = $db->get_results("SELECT 
                                    COALESCE( SUM( AMOUNT ), 0 ) AS TT
                                FROM
                                    invoice_suite
                                WHERE
                                    AAM_CODE_MIG = '$response[1]' ");
        $update_AA = array( 
                            'AAM_TOTAL_TTC' => $tt[0]["TT"]
                        );
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'invoice', $update_AA, $update_where_AA, 1 );
        break;

    case 'CHECK_INV':
        $db = new MySQL();
        $CHECK_PAA = $db->get_results("SELECT AAM_CODE FROM invoice WHERE AAM_CODE_DOSSIER = '$response[1]'");
        if (count($CHECK_PAA) < 1) {
            $db = new MySQL();
            $CPT = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM invoice")[0]['CPT'];
            $GET_PAA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
            $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = ".$GET_PAA_INFO[0]["DM_CLIENT"]);
            $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
            $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
            $ALL_PO = "";
            $AAM_MARIT_AERIEN = "";
            if (empty($GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"])) {
                $ALL_PO = $db->get_results("SELECT a.AE_LIBELLE AS POD,c.AE_LIBELLE AS POL FROM trans.aeroport a ,trans.aeroport c WHERE a.AE_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.AE_CODE = ".$GET_PAA_INFO[0]["DM_POL"]."");
                $AAM_MARIT_AERIEN = "AER";
            } else {
                $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.PO_CODE = ".$GET_PAA_INFO[0]["DM_POL"]."");
                $AAM_MARIT_AERIEN = "MAR";
            }
            //$ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = ".$GET_PAA_INFO[0]["DM_POD"]." AND c.PO_CODE = ".$GET_PAA_INFO[0]["DM_POL"]);
            $DM_POD_LIB = $ALL_PO[0]["POD"];
            $DM_POL_LIB = $ALL_PO[0]["POL"];
            $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = ".$GET_PAA_INFO[0]["DM_FOURNISSEUR"])[0]["FR_LIBELLE"];
      
            $INSET_DATA = array(
                'AAM_CODE' => $CPT,
                'AAM_CODE_DOSSIER' => $GET_PAA_INFO[0]["DM_CLE"],
                'AAM_DATE' => date('Y-m-d'),
                'AAM_CODE_CLIENT' => $GET_PAA_INFO[0]["DM_CLIENT"],
                'AAM_DES_CLIENT' => $DM_CLIENT_LIB,
                'AAM_ADR_CLIENT' => $DM_CLIENT_ADDR,
                'AAM_PORT_EMBARQ' => $DM_POL_LIB,
                'AAM_PORT_DECHAG' => $DM_POD_LIB,
                'AAM_DATE_DEBARQ' => $GET_PAA_INFO[0]["DM_DATE_DECHARG"],
                'AAM_FOURNISSEUR' => $DM_FOURNISSEUR_LIB,
                'AAM_MARCHANDISE' => $GET_PAA_INFO[0]["DM_MARCHANDISE"],
                'AAM_NBR_COLIS' => $GET_PAA_INFO[0]["DM_NOMBRE"],
                'AAM_MARQUE' => $GET_PAA_INFO[0]["DM_MARQUE"],
                'AAM_POIDS' => $GET_PAA_INFO[0]["DM_POIDS"],
                'AAM_NUM_LTA' => $GET_PAA_INFO[0]["DM_NUM_LTA"],
                'AAM_IMP_EXP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
                'AAM_G_C' => $GET_PAA_INFO[0]["DM_CODE_COMP_GROUP"],
                'AAM_MARIT_AERIEN' => $AAM_MARIT_AERIEN,
                'AAM_LIB_EXP_IMP' => $GET_PAA_INFO[0]["DM_IMP_EXP"],
                'AAM_NUM_DOSSIER' => $GET_PAA_INFO[0]["DM_NUM_DOSSIER"]
            );
            $res = $db->insert( 'invoice', $INSET_DATA );
            $CHECK_PAA = $db->get_results("SELECT AAM_CODE FROM invoice WHERE AAM_CODE_DOSSIER = '$response[1]'");
            echo json_encode($CHECK_PAA);
        }else{
            echo json_encode($CHECK_PAA);
        }
        break;

    case 'UP_INV_REGLEMENT':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'invoice', $response[1], $update_where_AA );
        break;

    case 'ADD_INV_TVD':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(TVD_CLE) + 1 as CPT FROM tab_valeur_dinar")[0]['CPT'];
        $response[1]['TVD_CLE'] = $key;
        echo $db->insert( 'tab_valeur_dinar', $response[1]);
        break;

    case 'GET_TVD_INV':
        $db = new MySQL();
        $tt = $db->get_results("SELECT * FROM tab_valeur_dinar WHERE TVD_CLE_INVOICE = '$response[1]' ORDER BY TVD_CLE DESC");
        echo json_encode($tt);
        break;

    case 'MOD_CLIENT_INV':
        $db = new MySQL();
        $CL_CODE = $response[1];
        $CL_LIBEL = $response[2];
        $DM_CLE = $response[3];
        if (strpos($CL_CODE, 'CL_') !== false) {
            $CL_CODE = substr($CL_CODE, 3);
            $CL_ADDRESS = $db->get_results("SELECT CL_ADRESSE from trans.client WHERE CL_CODE = '$CL_CODE'")[0]['CL_ADRESSE'];
        } else {
            $CL_ADDRESS = $db->get_results("SELECT CLE_ADRESSE from trans.client_etranger WHERE CLE_CODE = '$CL_CODE'")[0]['CLE_ADRESSE'];
        }
        
        //MODIF INV
        $update_AA = array( 'AAM_CODE_CLIENT' => $CL_CODE, 'AAM_DES_CLIENT' => $CL_LIBEL, 'AAM_ADR_CLIENT' => $CL_ADDRESS);
        $update_where_AA= array( 'AAM_CODE_DOSSIER' => $DM_CLE);
        $res2 = $db->update( 'invoice', $update_AA, $update_where_AA );

        echo json_encode('{"RES_AA" : "'.$res2.'"}');
        break;

        //Delete avis + facture
    case 'DEL_AA_FACT':
        $db = new MySQL();
        $where = array( 'AAM_CODE' => $response[1] );
        echo $db->delete( 'avis_arrive_mig', $where );
        $where_suite = array( 'AAM_CODE_MIG' => $response[1] );
        echo $db->delete( 'avis_arrive_mig_suite', $where_suite );
        break;

        
    case 'DEL_INV_FACT':
        $db = new MySQL();
        $where = array( 'AAM_CODE' => $response[1] );
        echo $db->delete( 'invoice', $where );
        $where_suite = array( 'AAM_CODE_MIG' => $response[1] );
        echo $db->delete( 'invoice_suite', $where_suite );
        break;


    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>