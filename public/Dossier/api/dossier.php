<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json");
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false) {
    $jqxdata = explode('&', $str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=', $value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'fournisseurs':
        $db = new MySQL();
        $fournisseurs = $db->get_results("SELECT FR_CODE,FR_LIBELLE FROM trans.fournisseur ORDER BY FR_LIBELLE");
        echo json_encode($fournisseurs);
        break;

    case 'clients':
        $db = new MySQL();
        $clients = $db->get_results("SELECT CL_CODE,CL_LIBELLE FROM trans.client ORDER BY CL_LIBELLE");
        echo json_encode($clients);
        break;

    case 'client_etranger':
        $db = new MySQL();
        $client_etranger = $db->get_results("SELECT CLE_CODE,CLE_LIBELLE FROM trans.client_etranger ORDER BY CLE_LIBELLE");
        echo json_encode($client_etranger);
        break;

    case 'navire':
        $db = new MySQL();
        $navire = $db->get_results("SELECT NA_CODE,NA_LIBELLE FROM trans.navire ORDER BY NA_LIBELLE");
        echo json_encode($navire);
        break;

    case 'port':
        $db = new MySQL();
        $port = $db->get_results("SELECT PO_CODE,PO_LIBELLE FROM trans.port ORDER BY PO_LIBELLE");
        echo json_encode($port);
        break;

    case 'aeroport':
        $db = new MySQL();
        $aeroport = $db->get_results("SELECT AE_CODE,AE_LIBELLE FROM trans.aeroport ORDER BY AE_LIBELLE");
        echo json_encode($aeroport);
        break;

    case 'volum_container':
        $db = new MySQL();
        $volum_container = $db->get_results("SELECT * FROM trans.container_volume");
        echo json_encode($volum_container);
        break;

    case 'CL_ADDRESS':
        $db = new MySQL();
        $CL_ADDRESS = $db->get_results("SELECT CL_ADRESSE from trans.client WHERE CL_CODE = '$response[1]'");
        echo json_encode($CL_ADDRESS);
        break;

    case 'GET_CLOT_DOS':
        $db = new MySQL();
        $GET_CLOT_DOS = $db->get_results("SELECT DM_CLOTURE FROM trans.dossier_maritime WHERE DM_NUM_DOSSIER = '$response[1]' LIMIT 1");
        echo json_encode($GET_CLOT_DOS);
        break;

    case 'SEARCH_DOS':
        $db = new MySQL();
        $DM_NUM_DOSSIER = $db->get_results("SELECT DM_IMP_EXP,DM_CODE_COMP_GROUP,DM_ATTRIBUER FROM dossier_maritime WHERE DM_NUM_DOSSIER LIKE '$response[1]'");
        if (!empty($DM_NUM_DOSSIER)) {
            $usn = $DM_NUM_DOSSIER[0]['DM_ATTRIBUER'];
            if ($usn) {
                $DM_NUM_DOSSIER['UFNAME'] = $db->get_results("SELECT group_concat( concat( name, ' ',subname ) SEPARATOR ' ') as UFNAME FROM users WHERE username = '$usn'")[0]['UFNAME'];
            } else {
                $DM_NUM_DOSSIER['UFNAME'] = '';
            }
        }
        echo json_encode($DM_NUM_DOSSIER);
        break;

    case 'CHECK_DOS_EXIST':
        $db = new MySQL();
        $CHECK_DOS_EXIST = $db->get_results("SELECT COUNT(*) AS NBR_DOS FROM dossier_maritime WHERE DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($CHECK_DOS_EXIST);
        break;

    case 'GET_DOS_MIG_MARCH':
        $db = new MySQL();
        $GET_DOS_MIG_MARCH["data"] = $db->get_results("SELECT DM_CLE,DM_NUM_BL,DM_MARCHANDISE,DM_POIDS,DM_NOMBRE,CL_LIBELLE,DM_CLOTURE from dossier_maritime, client WHERE DM_NUM_DOSSIER = '$response[1]' and DM_CLIENT = CL_CODE");//'$response[1]'");
        echo json_encode($GET_DOS_MIG_MARCH);
        break;

    case 'GET_MARCH_DATA':
        $db = new MySQL();
        $GET_MARCH_DATA = $db->get_results("SELECT DM_FOURNISSEUR,DM_CLIENT,DM_MARCHANDISE,DM_DATE_EMBARQ,DM_DATE_DECHARG,DM_POIDS,DM_NUM_BL,DM_NAVIRE,DM_POL,DM_POD,DM_PLACE_DELIVERY,DM_NOMBRE,DM_LONGUEUR,DM_LARGEUR,DM_HAUTEUR,DM_TERME,DM_MARQUE,DM_ESCALE,DM_RUBRIQUE,DM_VAL_DEVISE,DM_CLOTURE FROM trans.dossier_maritime WHERE DM_CLE = '$response[1]'");
        echo json_encode($GET_MARCH_DATA);
        break;

    case 'MODIF_DOS_MIG':
        $db = new MySQL();
        $update_where = array('DM_CLE' => $response[2]);
        echo $db->update('dossier_maritime', $response[1], $update_where, 1);
        break;

    case 'GEN_NEW_DOS_NUM':
        $db = new MySQL();
        $CPT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_dossier")[0]['CPT'];
        switch (strlen((string)$CPT)) {
            case '1':
                $CPT = '000' . $CPT;
                break;
            case '2':
                $CPT = '00' . $CPT;
                break;
            case '3':
                $CPT = '0' . $CPT;
                break;

            default:
                break;
        }
        echo date('Y') . date('m') . $CPT;
        break;

    case 'ADD_To_MIG':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(DM_CLE) + 1 as k FROM dossier_maritime")[0]['k'];
        $response[1]['DM_CLE'] = $key;
        echo $db->insert('dossier_maritime', $response[1]);
        break;

    case 'DEL_from_MIG':
        $db = new MySQL();
        $where = array('DM_CLE' => $response[1]);
        echo $db->delete('dossier_maritime', $where, 1);
        break;

    case 'SAVE_DOS':
        $db = new MySQL();
        //$key = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_dossier")[0]['CPT'];
        echo $db->insert('compteur_dossier', array('NUMERO' => $response[1]));
        break;

    case 'CONF_DEL_PASS':
        $db = new MySQL();
        $key = $db->get_results("SELECT PASS FROM CONF_TAB where id = 'MGOD'")[0]['PASS'];
        //echo $key . '  '.$response[1];
        if ($key == $response[1]) {
            $where = array('DM_NUM_DOSSIER' => $response[2]);
            echo $db->delete('dossier_maritime', $where);
        } else {
            echo 'Wrong';
        }
        break;


    //COMPLET

    case 'GET_DOS_COMP_DATA':
        $db = new MySQL();
        $GET_DOS_COMP_DATA = $db->get_results("SELECT DM_CLE,DM_FOURNISSEUR,DM_CLIENT,DM_DATE_EMBARQ,DM_DATE_DECHARG,DM_POIDS,DM_NUM_BL,DM_NAVIRE,DM_POL,DM_POD,DM_PLACE_DELIVERY,DM_NOMBRE,DM_ESCALE,DM_TERME,DM_MARQUE,DM_VAL_DEVISE,DM_CLOTURE from dossier_maritime WHERE DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA);
        break;

    case 'GET_DOS_COMP_DATA_MARCH':
        $db = new MySQL();
        $GET_DOS_COMP_DATA_MARCH["data"] = $db->get_results("SELECT DM_CODE,DM_MARCHANDISE,DM_RUBRIQUE,DM_NBR,DM_POID FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA_MARCH);
        break;

    case 'GET_DOS_COMP_DATA_MARCH_SELECT':
        $db = new MySQL();
        $GET_DOS_COMP_DATA_MARCH = $db->get_results("SELECT DM_MARCHANDISE,DM_RUBRIQUE,DM_NBR,DM_POID FROM dossier_marchandise WHERE DM_CODE = '$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA_MARCH);
        break;

    case 'GET_DOS_COMP_DATA_CONT':
        $db = new MySQL();
        $GET_DOS_COMP_DATA_CONT["data"] = $db->get_results("SELECT DC_CODE,DC_CONTAINER,DC_NUM_CONTAINER,DC_RUBRIQUE,DC_POID FROM dossier_container WHERE DC_CODE_MARCHANDISE = '$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA_CONT);
        break;

    case 'GET_DOS_COMP_DATA_CONT_SELECT':
        $db = new MySQL();
        $GET_DOS_COMP_DATA_MARCH = $db->get_results("SELECT DC_CONTAINER,DC_NUM_CONTAINER,DC_RUBRIQUE,DC_POID FROM dossier_container WHERE DC_CODE = '$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA_MARCH);
        break;

    case 'MODIF_DOS_COMP':
        $db = new MySQL();
        $update_where = array('DM_NUM_DOSSIER' => $response[2]);
        echo $db->update('dossier_maritime', $response[1], $update_where, 1);
        break;

    //COMP MARCH

    case 'MODIF_DOS_COMP_MARCH':
        $db = new MySQL();
        $update_where = array('DM_CODE' => $response[2]);
        echo $db->update('dossier_marchandise', $response[1], $update_where, 1);
        $update_where = array('DM_NUM_DOSSIER' => $response[4]);
        echo $db->update('dossier_maritime', $response[3], $update_where);
        break;

    case 'ADD_DOS_COMP_MARCH':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(DM_CODE) + 1 as k FROM dossier_marchandise")[0]['k'];
        $response[1]['DM_CODE'] = $key;
        echo $db->insert('dossier_marchandise', $response[1]);
        $update_where = array('DM_NUM_DOSSIER' => $response[3]);
        echo $db->update('dossier_maritime', $response[2], $update_where);
        break;

    case 'DEL_CONT_OF_MARCH':
        $db = new MySQL();
        $where_cont = array('DC_CODE_MARCHANDISE' => $response[1]);
        echo $db->delete('dossier_container', $where_cont);
        $where_cont2 = array('DM_CODE' => $response[1]);
        echo $db->delete('dossier_marchandise', $where_cont2);
        break;
    case 'DEL_EMPTY_MARCH':
        $db = new MySQL();
        $where_cont = array('DM_CODE' => $response[1]);
        echo $db->delete('dossier_marchandise', $where_cont);
        break;

    //COMP CONTE
    case 'MODIF_DOS_COMP_CONT':
        $db = new MySQL();
        $update_where = array('DC_CODE' => $response[2]);
        echo $db->update('dossier_container', $response[1], $update_where, 1);
        break;

    case 'ADD_DOS_COMP_CONT':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(DC_CODE) + 1 as k FROM dossier_container")[0]['k'];
        $response[1]['DC_CODE'] = $key;
        echo $db->insert('dossier_container', $response[1]);
        break;

    case 'DEL_CONT':
        $db = new MySQL();
        $where_cont = array('DC_CODE' => $response[1]);
        echo $db->delete('dossier_container', $where_cont);
        break;

    case 'ADD_DOS_COMP':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(DM_CLE) + 1 as k FROM dossier_maritime")[0]['k'];
        $response[1]['DM_CLE'] = $key;
        echo $db->insert('dossier_maritime', $response[1]);
        /*if ($res == '1'){
            echo $db->insert( 'compteur_dossier', array('NUMERO' => $response[2]));
        }else {
            echo 'error add new DMC';
        }*/
        break;

    //AERIEN
    case 'GET_DOS_AER_DATA':
        $db = new MySQL();
        $GET_DOS_COMP_DATA = $db->get_results("SELECT DM_CLE,DM_FOURNISSEUR,DM_CLIENT,DM_DATE_EMBARQ,DM_DATE_DECHARG,DM_POIDS,DM_MAGASIN,DM_POL,DM_POD,DM_PLACE_DELIVERY,DM_NOMBRE,DM_NUM_LTA,DM_TERME,DM_MARQUE,DM_VAL_DEVISE,DM_LONGUEUR,DM_LARGEUR,DM_HAUTEUR,DM_MARCHANDISE,DM_CLOTURE from dossier_maritime WHERE DM_NUM_DOSSIER ='$response[1]'");
        echo json_encode($GET_DOS_COMP_DATA);
        break;

    case 'ADD_DOS_AER':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(DM_CLE) + 1 as k FROM dossier_maritime")[0]['k'];
        $response[1]['DM_CLE'] = $key;
        $res = $db->insert('dossier_maritime', $response[1]);
        if ($res == '1') {
            echo $db->insert('compteur_dossier', array('NUMERO' => $response[2]));
        } else {
            echo 'error add new DMC';
        }
        break;

    case 'MOD_DOS_AER':
        $db = new MySQL();
        $update_where = array('DM_NUM_DOSSIER' => $response[2]);
        echo $db->update('dossier_maritime', $response[1], $update_where);
        break;

    case 'GET_FACT_DOS':
        $db = new MySQL();
        $GET_FACT_DOS["data"] = $db->get_results("SELECT
                                                        AAM_CODE,
                                                        CL_LIBELLE,
                                                        CL_RSPONSABLE,
                                                        CL_TEL_ETABLISS,
                                                        CL_FAX,
                                                        CL_EMAIL,
                                                        AAM_NUM_FACTURE,
                                                        DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                        FT_LIBELLE,
                                                        AAM_BON_COMMAND,
                                                        AAM_TOTAL_TTC 
                                                    FROM
                                                        avis_arrive_mig
                                                        INNER JOIN client ON CL_CODE = AAM_CODE_CLIENT
                                                        INNER JOIN type_facture ON FT_CODE = AAM_TYPE_FACT
                                                    WHERE
                                                        AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_FACT_DOS);
        break;

    case 'CLOT_ME':
        $db = new MySQL();
        $update_where = array('DM_NUM_DOSSIER' => $response[1]);
        echo $db->update('dossier_maritime', array('DM_CLOTURE' => 1, 'DM_CLOT_DATE' => 'CURRENT_TIMESTAMP'), $update_where);
        break;

    case 'UNCLOT_ME':
        $db = new MySQL();
        $update_where = array('DM_NUM_DOSSIER' => $response[1]);
        echo $db->update('dossier_maritime', array('DM_CLOTURE' => 0, 'DM_CLOT_DATE' => 'CURRENT_TIMESTAMP'), $update_where);
        break;

    case 'CONF_ADM_PASS':
        $db = new MySQL();
        $key = $db->get_results("SELECT PASS FROM CONF_TAB where id = 'MGOD'")[0]['PASS'];
        if ($key == $response[1]) {
            echo true;
        } else {
            echo false;
        }
        break;

    case 'GET_AAM_CODE':
        $db = new MySQL();
        $GET_AAM_CODE = $db->get_results("SELECT AAM_CODE FROM avis_arrive_mig WHERE AAM_CODE_DOSSIER ='$response[1]'");
        echo json_encode($GET_AAM_CODE);
        break;

    case 'GET_DM_CLE':
        $db = new MySQL();
        $GET_DM_CLE = $db->get_results("SELECT DM_CLE FROM dossier_maritime WHERE DM_NUM_DOSSIER ='$response[1]'");
        echo json_encode($GET_DM_CLE);
        break;



    //CONSIGNATION DMC
    case 'GET_CONT_LIST':
        $db = new MySQL();
        $GET_REGL_CONSI = $db->get_results("SELECT
                                                DC_CONTAINER,
                                                DC_NUM_CONTAINER,
                                                CV_VALUE
                                            FROM
                                                dossier_container
                                                INNER JOIN container_volume ON DC_CONTAINER = CV_LBELLE 
                                            WHERE
                                                DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '$response[1]' )");
        echo json_encode($GET_REGL_CONSI);
        break;

    case 'GET_REGL_CONSI':
        $db = new MySQL();
        $GET_REGL_CONSI = $db->get_results("SELECT DM_CONSI_REGLEMENT,DM_CONSI_BANQUE,DM_CONSI_NUM,DATE_FORMAT( DM_CONSI_DATE, '%Y-%m-%d' ) AS DM_CONSI_DATE FROM dossier_maritime WHERE DM_NUM_DOSSIER ='$response[1]'");
        echo json_encode($GET_REGL_CONSI);
        break;

    case 'SET_REGL_CONSI':
        $db = new MySQL();
        $update_where = array('DM_NUM_DOSSIER' => $response[2]);
        echo $db->update('dossier_maritime', $response[1], $update_where, 1);
        $dc_code = $db->get_results("SELECT DC_CODE FROM dossier_container 
        WHERE DC_CODE_MARCHANDISE IN ( SELECT DM_CODE FROM dossier_marchandise WHERE DM_NUM_DOSSIER = '{$response[2]}' )");
        //var_dump($dc_code);
        foreach ($dc_code as $key => $value) {
            $update_where = array('DC_CODE' => $value["DC_CODE"]);
            echo $db->update('dossier_container', $response[1], $update_where);
        }
        break;

    // recherche dossier
    case 'SEARCH_DOS_MAR':
        $db = new MySQL();
        $SEARCH_DOS_MAR["data"] = $db->get_results("SELECT
                                                    DM_NUM_DOSSIER,
                                                    DM_IMP_EXP,
                                                    DM_FOURNISSEUR,
                                                    FR_LIBELLE,
                                                    DM_CLIENT,
                                                    CL_LIBELLE,
                                                    DM_NAVIRE,
                                                    NA_LIBELLE,
                                                    DM_LIB_COMP_GROUP,
                                                    DM_ESCALE,
                                                    DM_NUM_BL,
                                                    DM_POIDS,
                                                    DM_NOMBRE,
                                                    DM_MARQUE,
                                                    DM_TERME
                                                FROM
                                                    dossier_maritime
                                                    INNER JOIN fournisseur ON DM_FOURNISSEUR = FR_CODE
                                                    INNER JOIN client ON DM_CLIENT = CL_CODE
                                                    INNER JOIN navire ON DM_NAVIRE = NA_CODE 
                                                WHERE
                                                    ( DM_DATE_CREATION BETWEEN '$response[1]' AND '$response[2]' )");
        echo json_encode($SEARCH_DOS_MAR);
        break;

    case 'SEARCH_DOS_AER':
        $db = new MySQL();
        $SEARCH_DOS_AER["data"] = $db->get_results("SELECT
                                                    DM_NUM_DOSSIER,
                                                    DM_IMP_EXP,
                                                    DM_FOURNISSEUR,
                                                    FR_LIBELLE,
                                                    DM_CLIENT,
                                                    CL_LIBELLE,
                                                    DM_MARCHANDISE,
                                                    DM_NUM_LTA,
                                                    DM_POIDS,
                                                    DM_NOMBRE,
                                                    DM_MARQUE,
                                                    DM_TERME,
                                                    DM_MAGASIN
                                                FROM
                                                    dossier_maritime
                                                    INNER JOIN fournisseur ON DM_FOURNISSEUR = FR_CODE
                                                    INNER JOIN client ON DM_CLIENT = CL_CODE 
                                                WHERE
                                                    ( DM_DATE_CREATION BETWEEN '$response[1]' AND '$response[2]' ) 
                                                    AND (DM_CODE_COMP_GROUP = '' OR DM_CODE_COMP_GROUP IS NULL ) ");
        echo json_encode($SEARCH_DOS_AER);
        break;

    case 'GET_LOCAL_FACT':
        $db = new MySQL();
        $GET_LOCAL_FACT = $db->get_results("SELECT
                                                AAM_CODE,
                                                DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                AAM_TYPE_FACT,
                                                FT_LIBELLE,
                                                AAM_NUM_DOSSIER,
                                                AAM_TOT_NON_TAXABLE,
                                                AAM_TO_TAXABLE,
                                                AAM_TVA_PORCENTAGE,
                                                AAM_TVA_VAL,
                                                AAM_TIMBRE,
                                                AAM_TOTAL_TTC,
                                                AA_TTC_LETTRE 
                                            FROM
                                                avis_arrive_mig INNER JOIN type_facture ON AAM_TYPE_FACT = FT_CODE
                                            WHERE
                                                AAM_NUM_FACTURE = '$response[1]'");
        echo json_encode($GET_LOCAL_FACT);
        break;

    case 'GET_TAB_LOCAL_FACT':
        $db = new MySQL();
        $GET_TAB_LOCAL_FACT["data"] = $db->get_results("SELECT * FROM avis_arrive_mig_suite WHERE AAM_CODE_MIG ='$response[1]'");
        echo json_encode($GET_TAB_LOCAL_FACT);
        break;

    case 'GET_ETR_FACT':
        $db = new MySQL();
        $GET_ETR_FACT = $db->get_results("SELECT
                                            AAM_CODE,
                                            DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                            AAM_NUM_DOSSIER,
                                            AAM_DEVISE,
                                            AAM_TOTAL_TTC AS TT,
                                            IFNULL( AAM_EURO, AAM_DOLLAR ) AS TTOLD
                                        FROM
                                            invoice 
                                        WHERE
                                            AAM_NUM_FACTURE = '$response[1]'");
        echo json_encode($GET_ETR_FACT);
        break;

    case 'GET_TAB_ETR_FACT':
        $db = new MySQL();
        $GET_TAB_ETR_FACT["data"] = $db->get_results("SELECT * FROM invoice_suite WHERE AAM_CODE_MIG ='$response[1]'");
        echo json_encode($GET_TAB_ETR_FACT);
        break;

    case 'GET_TAB_CONT':
        $db = new MySQL();
        $GET_TAB_CONT["data"] = $db->get_results("SELECT
                                                    DM_NUM_DOSSIER,
                                                    DM_MARCHANDISE,
                                                    DC_NUM_CONTAINER,
                                                    DC_CONTAINER,
                                                    DC_POID,
                                                    DC_RUBRIQUE 
                                                FROM
                                                    dossier_container
                                                    INNER JOIN dossier_marchandise ON DC_CODE_MARCHANDISE = DM_CODE 
                                                WHERE
                                                    DC_NUM_CONTAINER LIKE '$response[1]'");
        echo json_encode($GET_TAB_CONT);
        break;


    case 'GET_ACTIVE_DOS':
        $db = new MySQL();
        $GET_ACTIVE_DOS = $db->get_results("SELECT DISTINCT(DM_NUM_DOSSIER) FROM trans.dossier_maritime WHERE DM_CLOTURE = 0");
        echo json_encode($GET_ACTIVE_DOS);
        break;


    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>