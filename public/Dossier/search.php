<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
/*if (!in_array($_SESSION['LEVEL'], ["Administrateur","Financier","Commercial","Comptable"])) {
    header("location: /GTRANS");
	exit();
}*/
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Recherche Dossier</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="api/search.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 14px;
            font-weight: 700;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dossier</span> - Recherche </h4>
                        </div>
                        <div class="heading-elements">
                            <div class="col-sm-6 col-md-6 pull-left">
                                <div class="navbar-form">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">
                    <div class="panel-body">
                        <ul class="nav nav-tabs nav-justified text-black">
                            <li class="active">
                                <a href="#tab-MAR" data-toggle="tab"> <i class="icon-ship position-left"></i>
                                    MARITIME
                                </a>
                            </li>

                            <li>
                                <a href="#tab-MARCONT" data-toggle="tab"> <i class="icon-ship position-left"></i>
                                    CONTENEUR
                                </a>
                            </li>

                            <li>
                                <a href="#tab-AER" data-toggle="tab"><i class="icon-airplane2 position-left"></i>
                                    AERIEN
                                </a>
                            </li>

                            <li>
                                <a href="#tab-LOCAL" data-toggle="tab"><i class="icon-file-text2 position-left"></i>
                                    FACTURE LOCALE
                                </a>
                            </li>
                            <li>
                                <a href="#tab-ETRAN" data-toggle="tab"><i class="icon-file-text2 position-left"></i>
                                    FACTURE ETRANGER
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active in has-padding" id="tab-MAR">
                                <div class="row text-center">
                                    <div class="col-lg-4">
                                        
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="display-block">Sélectionnez la plage de dates </label>
                                            <button type="button" class="btn bg-teal-400 daterange-ranges" id="daterange_mar">
                                                <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="display-block">Rechercher </label>
                                        <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="get_tab_btn_mar"><i class="icon-search4"></i></button>
                                    </div>
                                    <div class="col-lg-3">
                            
                                    </div>
                                </div>
                                <table class="table display datatable-button-print-rows table-striped table-bordered" id="table_mar">
                                    <thead class="bg-slate">
                                        <th>N°.Dos</th>
                                        <th>N° BL</th>
                                        <th>G/C/A</th>
                                        <th>IMPORT/EXPORT</th>
                                        <th>Fournisseur</th>
                                        <th>Client</th>
                                        <th>Navire</th>
                                        <th>Poids</th>
                                        <th>Nombre</th>
                                        <th>Marque</th>
                                        <th>Terme</th>
                                        <th>Escale</th>
                                    </thead>
                                    <tbody class="text-black"></tbody>
                                    <tfoot>
                                        <th>N°.Dos</th>
                                        <th>N° BL</th>
                                        <th>G/C/A</th>
                                        <th>IMPORT/EXPORT</th>
                                        <th>Fournisseur</th>
                                        <th>Client</th>
                                        <th>Navire</th>
                                        <th>Poids</th>
                                        <th>Nombre</th>
                                        <th>Marque</th>
                                        <th>Terme</th>
                                        <th>Escale</th>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane fade has-padding" id="tab-MARCONT">
                                <div class="row text-center">
                                    <div class="col-lg-4">
                                        
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="display-block">N° CONTENEUR </label>
                                        <div class="input-group">
                                            <input type="text" id="num_cont" class="form-control border-teal border-lg text-black" placeholder="">
                                            <span class="input-group-btn">
                                                <button class="btn bg-teal" type="button" id="btn_get_tab_cont">Recherche</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table display datatable-button-print-rows table-striped table-bordered" id="table_cont">
                                            <thead class="bg-slate">
                                                <tr>
                                                    <th>N° Dossier</th>
                                                    <th>Marchandise</th>
                                                    <th>Conteneur</th>
                                                    <th>Voume</th>
                                                    <th>Poids</th>
                                                    <th>Rubrique</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-black">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade has-padding" id="tab-AER">
                                <div class="row text-center">
                                        <div class="col-lg-4">
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="display-block">Sélectionnez la plage de dates </label>
                                                <button type="button" class="btn bg-teal-400 daterange-ranges" id="daterange_aer">
                                                    <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <label class="display-block">Rechercher </label>
                                            <button type="button" class="btn bg-teal-400 btn-icon btn-rounded" id="get_tab_btn_aer"><i class="icon-search4"></i></button>
                                        </div>
                                        <div class="col-lg-3">
                                
                                        </div>
                                    </div>
                                    <div class="row">
                                        <table class="table display datatable-button-print-rows table-striped table-bordered" id="table_aer">
                                            <thead class="bg-slate">
                                                <th>N°.Dos</th>
                                                <th>N° LTA</th>
                                                <th>IMPORT/EXPORT</th>
                                                <th>Fournisseur</th>
                                                <th>Client</th>
                                                <th>Marchandise</th>
                                                <th>Poids</th>
                                                <th>Nombre</th>
                                                <th>Marque</th>
                                                <th>Terme</th>
                                                <th>Magasin</th>
                                            </thead>
                                            <tbody class="text-black"></tbody>
                                            <tfoot>
                                                <th>N°.Dos</th>
                                                <th>N° LTA</th>
                                                <th>IMPORT/EXPORT</th>
                                                <th>Fournisseur</th>
                                                <th>Client</th>
                                                <th>Marchandise</th>
                                                <th>Poids</th>
                                                <th>Nombre</th>
                                                <th>Marque</th>
                                                <th>Terme</th>
                                                <th>Magasin</th>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            <div class="tab-pane fade has-padding" id="tab-LOCAL">
                                <div class="row text-center">
                                    <div class="col-lg-3">
                                        <label class="display-block">N° Facture </label>
                                        <div class="input-group">
                                            <input type="text" id="num_local" class="form-control border-teal border-lg text-black" placeholder="">
                                            <span class="input-group-btn">
                                                <button class="btn bg-teal" type="button" id="btn_get_tab_local">Recherche</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text" class="form-control text-black" id="date_local" placeholder="...">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <input type="text" class="form-control text-black" id="type_local" placeholder="...">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                    <label class="display-block">N° Dossier </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control text-black" id="num_dos_local" placeholder="...">
                                            <span class="input-group-btn">
                                                <button class="btn bg-default" id="show_dos_local" type="button"><i class="icon-eye8"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table display datatable-button-print-rows table-striped table-bordered" id="table_local">
                                            <thead class="bg-slate">
                                                <tr>
                                                    <th>Description</th>
                                                    <th class="col-sm-1">NON TAXABLE</th>
                                                    <th class="col-sm-1">TAXABLE</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-black">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="content-group">
                                            <h6>Total</h6>
                                            <div class="table-responsive no-border">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <th>TOTAL NON TAXABLE:</th>
                                                            <td class="text-right" id="N_TAX"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TOTAL TAXABLE :</th>
                                                            <td class="text-right" id="T_TAX"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TVA: <span class="text-regular" id="VAL_TVA"></span></th>
                                                            <td class="text-right" id="T_TVA"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TIMBRE :</th>
                                                            <td class="text-right" id="VAL_TIMBRE"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Total TTC:</th>
                                                            <td class="text-right text-primary">
                                                                <h5 class="text-semibold" id="T_TT"></h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <span class="label label-primary" id="TOT_LIB"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade has-padding" id="tab-ETRAN">
                                <div class="row text-center">
                                    <div class="col-lg-3">
                                        <label class="display-block">N° Facture </label>
                                        <div class="input-group">
                                            <input type="text" id="num_etr" class="form-control border-teal border-lg text-black" placeholder="">
                                            <span class="input-group-btn">
                                                <button class="btn bg-teal" type="button" id="btn_get_tab_etr">Recherche</button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="text" class="form-control text-black" id="date_etr" placeholder="...">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="display-block">N° Dossier </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control text-black" id="num_dos_etr" placeholder="...">
                                            <span class="input-group-btn">
                                                <button class="btn bg-default" id="show_dos_etr" type="button"><i class="icon-eye8"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table display datatable-button-print-rows table-striped table-bordered" id="table_etr">
                                            <thead class="bg-slate">
                                                <tr>
                                                    <th>Description</th>
                                                    <th class="col-sm-1">Montant</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-black">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="content-group">
                                            <div class="table-responsive no-border">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Total:</th>
                                                            <td class="text-right text-primary">
                                                                <h5 class="text-semibold" id="T_TT_etr"></h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->
        </div>
        <!-- /Page container -->
</body>

</html>
