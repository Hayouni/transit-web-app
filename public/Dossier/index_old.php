<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?>
<?php
$get_doss = null;
if(isset($_GET['NumD']) && $_GET['NumD'] !== '' && is_numeric($_GET['NumD'])){
    $get_doss = $_GET['NumD'];
}
?>
<!DOCTYPE html>


<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS|Dossier</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/buttons/spin.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/buttons/ladda.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/noty.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/switch.min.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../../assets/js/formatDate.js"></script>
    <script type="text/javascript" src="api/NumberToLetter.js"></script>
    <script type="text/javascript" src="api/dossier.js"></script>
    <!-- /theme JS files -->
    <!--Style text box-->
    <style>
        .font_text_Bold_24 {
            font-weight: bold;
            font-size: 24px;
            /*font: normal normal bold 24px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .font_text_Bold {
            font-size: 16px;
            font-weight: bold;
            /* font: normal normal bold 16px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .border_2px {
            border: 2px solid rgba(0, 0, 0, 0.91);
        }
        
        .background_blue {
            font-size: 13px;
            /*color: rgba(255, 255, 255, 1);
            background: #2196f3;*/
            color: rgb(0, 0, 0);
            background: #a9b8c3;
            font-weight: bold;
        }
        
        .bootstrap-select>.btn.btn-default,
        .bootstrap-select>.btn.btn-default.disabled {
            color: #000;
            background-color: #a9b8c3;
            border-color: #a9b8c3;
            font-weight: bold;
        }
        
        .multiselect-selected-text {
            font-weight: bold;
            font-size: large;
        }
        /*thead {
            background-color: #0084ec;
            color: white;
        }
        
        tbody {
            background-color: #9cd7ff;
        }
        
        tr {
            font-size: 16px;
            font-weight: bold;
        }*/
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
        
        /*ul.dropdown-menu {
            background-color: #a9b8c3;
        }
        
        div.dropdown-menu.open {
            background-color: #a9b8c3;
        }*/
        
        .ui-pnotify.custom .ui-pnotify-container {
            background-color: #FF5722 !important;
            background-image: none !important;
            border: none !important;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }
        
        .ui-pnotify.custom .ui-pnotify-text {
            font-family: Arial, Helvetica, sans-serif !important;
            text-shadow: 2px 2px 3px black !important;
            font-size: 11pt !important;
            color: #FFF !important;
            padding-left: 50px !important;
            line-height: 1 !important;
            text-rendering: geometricPrecision !important;
            font-weight: bold !important;
        }
        
        .ui-pnotify.custom .ui-pnotify-title {
            font-family: Arial, Helvetica, sans-serif !important;
            text-shadow: 2px 2px 3px black !important;
            font-size: 16pt !important;
            color: #FFF !important;
            padding-left: 50px !important;
            line-height: 1 !important;
            text-rendering: geometricPrecision !important;
            font-weight: bold !important;
        }
        
        .ui-pnotify.custom .ui-pnotify-icon {
            float: left;
            margin: 3px;
            width: 33px;
            height: 33px;
            font-size: 33px;
            color: #FFF;
        }
        
         ::-webkit-input-placeholder {
            /* WebKit, Blink, Edge */
            color: #022345 !important;
        }
        
         :-moz-placeholder {
            /* Mozilla Firefox 4 to 18 */
            color: #022345 !important;
            opacity: 1;
        }
        
         ::-moz-placeholder {
            /* Mozilla Firefox 19+ */
            color: #022345 !important;
            opacity: 1;
        }
        
         :-ms-input-placeholder {
            /* Internet Explorer 10-11 */
            color: #022345 !important;
        }
        
         ::-ms-input-placeholder {
            /* Microsoft Edge */
            color: #022345 !important;
        }
        
        .selected td {
            background-color: #545252 !important;
            color: #ff4242 !important;
            font-size: 15px;
            font-weight: 800;
            /* Add !important to make sure override datables base styles */
        }
        
        select.bootstrap-select1 {
            width: 100%;
            padding: 6px 11px;
            border: 1px solid #a9b8c3;
            height: 34px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: #a9b8c3;
        }
        /* CAUTION: IE hackery ahead */
        
        select::-ms-expand {
            display: none;
            /* remove default arrow on ie10 and ie11 */
        }
        /* target Internet Explorer 9 to undo the custom arrow */
        
        @media screen and (min-width: 0\0) {
            select {
                background: none \9;
                padding: 5px \9;
            }
        }
        
        a.btnn {
            font-weight: 800 !important;
            font-size: 14px !important;
        }
    </style>
    <script>
        var NumD = "<?= $get_doss; ?>";
    </script>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="page-title">
                                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Dossier
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">

                            </div>
                            <div class="col-md-6 form-horizontal">
                                <div class="form-group page-title" id="NUM_DOS_DIV">
                                    <label class="col-sm-3 col-md-2 control-label font_text_Bold text-right">N° Dos
                                    : </label>
                                    <div class="col-sm-9 col-md-10">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="INP_DOS_SPIN" style="visibility:hidden;"><i
                                                class="icon-spinner2 spinner"></i></span>
                                            <input type="text" class="form-control font_text_Bold_24 text-center" style="border-bottom: 2px solid;" placeholder="" id="DOS_NUM_INP" data-popup="tooltip" title="Numéro Dossier" data-placement="left" value="<?= $get_doss; ?>">
                                            <span class="input-group-btn">
                                                <button class="btn bg-grey-800" type="button" id="GET_DOS_BTN"
                                                        data-popup="tooltip" title="Charger" data-placement="top"> <i
                                                        class="icon-file-download2"></i></button>
                                                <button class="btn bg-teal" type="button" id="ADD_NEW_DMIG"
                                                        data-popup="tooltip" title="Ajouter" data-placement="bottom"><i
                                                        class="icon-file-plus2"></i></button>
                                                <button class="btn bg-danger" type="button" id="DEL_DMIG"
                                                        data-popup="tooltip" title="Supprimer" data-placement="top"><i
                                                        class="icon-file-minus2"></i></button>
                                                <button class="btn bg-blue-800" type="button" id="CLS_DMIG"
                                                        data-popup="tooltip" title="Nettoyer" data-placement="bottom"><i
                                                        class="icon-file-empty2"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">
                    <!-- Main Choice -->
                    <div class="row" style="display: none" id="DOS_SELECT_DIV">
                        <div class="col-lg-12">
                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Choix de Dossier</h6>
                                </div>
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="">Nature de dossier</label>
                                                <div class="multi-select-full" style="border-bottom: 2px solid;">
                                                    <select class="multiselect" name="SEL_N_DOS" id="SEL_N_DOS">
                                                    <option value="Maritime">Maritime</option>
                                                    <option value="Aeriene">Aériene</option>
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Type de dossier</label>
                                                <div class="multi-select-full" style="border-bottom: 2px solid; font-size: 24px; font-weight: bold;">
                                                    <select class="multiselect" name="SEL_T_DOS" id="SEL_T_DOS">
                                                    <option value="IMPORT">Import</option>
                                                    <option value="EXPORT">Export</option>
                                                </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>dossier</label>
                                                <div class="input-group">
                                                    <div class="multi-select-full" style="border-bottom: 2px solid;">
                                                        <select class="multiselect" name="SEL_CG_DOS" id="SEL_CG_DOS">
                                                        <option value="G">Groupage</option>
                                                        <option value="C">Complet</option>
                                                        <option value="">-</option>
                                                    </select>
                                                    </div>
                                                    <div class="input-group-btn">
                                                        <button type="button" name="BTN_SEL_DOS" id="BTN_SEL_DOS" class="btn btn-lg bg-slate-800 multiselect-toggle-selection-button">
                                                        <i class="icon-arrow-right6"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Matritime-->
                    <div class="row" id="DIV_MAR" style="display: none">
                        <div class="col-lg-12">
                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-center text-muted" id="DOS_MAR_NAME">Dossier Maritime</h4>
                                    <div class="heading-elements">
                                        <ul class="icons-list">
                                            <li>
                                                <a class="clear_data"><i class="icon-file-empty"></i></a>
                                            </li>
                                            <!--li>
                                            <a data-action="reload"></a>
                                        </li>
                                        <!-li>
                                            <a data-action="close"></a>
                                        </li-->
                                        </ul>
                                    </div>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="./" id="NEW_DMIG" name="NEW_DMIG" class="form-horizontal">
                                                <div class="row">
                                                    <div class="col-md-6 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">attribuer à
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <input name="DM_ATTRIBUER_MIG" id="DM_ATTRIBUER_MIG" type="text" user="" class="form-control input-sm font_text_Bold text-danger text-center" readonly>
                                                                    <span class="input-group-btn">
                                                                        <button type="button" id="BTN_ATT_U_ADD_MIG"
                                                                                class="btn btn-link"
                                                                                data-popup="tooltip"
                                                                                title="Attribuez-vous à ce dossier ?"
                                                                                data-placement="right"><i
                                                                                class="icon-user-plus position-left"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-lg-8">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <!--div class="content-divider text-muted form-group"><span><i class="icon-printer2 pull-left"></i></span></div>
                                                    <!-div class="btn-group btn-group-justified">
                                                        <a  class="btn bg-slate-700 btnn btn-block"> PREAVIS D'ARRIVEE</a>
                                                        <a  class="btn bg-slate-700 btnn btn-block"> AVIS D'ARRIVEE + FACTURE</a>
                                                        <a href="#" class="btn bg-slate-700 btnn"> FACTURE MAGASINAGE</a>
                                                        <a href="#" class="btn bg-slate-700 btnn"> BON A DELIVRER</a>
                                                        <a href="#" class="btn bg-slate-700 btnn"> INVOICE</a>
                                                        <a href="#" class="btn bg-slate-700 btnn"> FACTURE DIVER</a>
                                                        <a href="#" class="btn bg-slate-700 btnn"> FACTURE AVOIR</a>
                                                    </div-->
                                                        <button type="button" id="PAA_MIG" class="btn btn-default"><i
                                                                class="icon-printer4 position-left"></i> PREAVIS D'ARRIVEE
                                                        </button>
                                                            <button type="button" id="AA_MIG" class="btn btn-default"><i
                                                                class="icon-file-text3 position-left"></i> AVIS D'ARRIVEE + FACTURE
                                                        </button>
                                                            <button type="button" id="MAG_MIG" class="btn btn-default"><i
                                                                class="icon-printer4 position-left"></i> MAGASINAGE
                                                        </button>
                                                            <button type="button" class="btn btn-default"><i
                                                                class="icon-printer4 position-left"></i> With icon
                                                        </button>

                                                        <hr>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <table class="table table-lg htNoWrap" style="width:100%" id="TAB_MIG">
                                                            <thead class="bg-info-700">
                                                                <tr>
                                                                    <th class="hide_me">N°</th>
                                                                    <th>Num BL</th>
                                                                    <th>Nature Marchandise</th>
                                                                    <th>Poids[KG]</th>
                                                                    <th>Nombre</th>
                                                                    <th>Client</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="bg-slate-700">

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </form>
                                            <hr>
                                            <form action="./" id="NEW_DMIG_ADD" name="NEW_DMIG_ADD" class="form-horizontal">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Fournisseur -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Fournisseur
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="fournisseurs_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Fournisseur -->
                                                        <!-- Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Client
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="clients_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Adresse
                                                            : </label>
                                                            <div class="col-sm-9"><input name="clients_ADR_MIG" id="clients_ADR_MIG" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <!-- P/C ->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label font_text_Bold">P/C : </label>
                                                        <div class="col-sm-9">
                                                            <select class="bootstrap-select1 background_blue" id="clients_PC_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!- /P/C -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Valeur en
                                                            devise : </label>
                                                            <div class="col-sm-9"><input name="VAL_DEV_MIG" id="VAL_DEV_MIG" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">B/L N°
                                                            : </label>
                                                            <div class="col-sm-9"><input name="BL_MIG" id="BL_MIG" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Navire
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="navire_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu Depart
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_DEP_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu
                                                            Arrivée : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_ARR_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Marchandise
                                                            : </label>
                                                            <div class="col-sm-9"><input name="MARCH_MIG" id="MARCH_MIG" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date Depart
                                                            :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_Dep_MIG">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date
                                                            Arrivée :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_arr_MIG">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Poids
                                                            : </label>
                                                            <div class="col-sm-9"><input name="Poids_MIG" id="Poids_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                <div class="form-control-feedback">
                                                                    KG
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Nombre
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="Nombre_MIG" id="Nombre_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-list-numbered"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Longueur
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="long_MIG" id="long_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-up"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Largeur
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="larg_MIG" id="larg_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-right"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Hauteur
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="haut_MIG" id="haut_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-up-right"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <!-- Terme-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Terme
                                                                    :</label>
                                                                    <div class="col-sm-6">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="term_MIG">
                                                                        <option value="EXWORK">EXWORK</option>
                                                                        <option value="FOB">FOB</option>
                                                                        <option value="FAS">FAS</option>
                                                                        <option value="DDP">DDP</option>
                                                                        <option value="DDU">DDU</option>
                                                                        <option value="CF">CF</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Terme -->
                                                            </div>
                                                            <div class="col-md-6">
                                                                <!-- Marque-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Marque
                                                                    :</label>
                                                                    <div class="col-sm-8">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="marq_MIG">
                                                                        <option value="palette">Palette</option>
                                                                        <option value="colis">Colis</option>
                                                                        <option value="caisse">Caisse</option>
                                                                        <option value="unite">Unité</option>
                                                                        <option value="crts">Crts</option>
                                                                        <option value="bulk">Bulk</option>
                                                                        <option value="box">Box</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Marque -->
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Escale
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="escal_MIG" id="escal_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Rubrique
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="Rubr_MIG" id="Rubr_MIG" type="text" class="form-control border-black border-lg text-black">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="btn bg-teal-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_SAVE_MIG"><span
                                                        class="ladda-label"><i class="icon-exit3"></i> Retour</span>
                                                </button>
                                                    <button type="button" class="btn bg-warning-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_CLOT_MIG"><span
                                                        class="ladda-label"><i
                                                        class="icon-unlocked"></i> clôturer</span></button>
                                                    <button type="button" class="btn bg-danger-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_DEL_MIG"><span
                                                        class="ladda-label"><i class="icon-bin"></i> Supprimer</span>
                                                </button>
                                                    <button type="button" class="btn bg-purple-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_MOD_MIG"><span
                                                        class="ladda-label"><i
                                                        class="icon-pen-plus"></i> Modifier</span></button>
                                                    <button type="button" class="btn bg-blue-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_ADD_to_MIG"><span
                                                        class="ladda-label"><i
                                                        class="icon-plus-circle2"></i> Ajouter</span></button>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Liste des factures</h5>
                                                    <table class="table table-lg htNoWrap" style="width:100%" id="TAB_MIG_FACT">
                                                        <thead class="bg-info-700">
                                                            <tr>
                                                                <th class="hide_me">N°</th>
                                                                <th>N° Facture</th>
                                                                <th>Date</th>
                                                                <th>Type</th>
                                                                <th>B.Cmd</th>
                                                                <th>Total TTC</th>
                                                                <th>Client</th>
                                                                <th>Responsable</th>
                                                                <th>Téléphone</th>
                                                                <th>Fax</th>
                                                                <th>Email</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="bg-slate-700">

                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td class="hide_me"></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /traffic sources -->
                        </div>

                    </div>

                    <!--Matritime-->
                    <div class="row" id="DIV_MAR_COMP" style="display: none">
                        <div class="col-lg-12">
                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-center text-muted" id="DOS_MARC_NAME">Dossier Maritime</h4>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="./" id="NEW_DMIC" name="NEW_DMIC" class="form-horizontal">
                                                <div class="row">
                                                    <div class="col-md-5 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">attribuer à
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <input name="DM_ATTRIBUER_MIG" id="DM_ATTRIBUER_MIC" type="text" user="" class="form-control input-sm font_text_Bold text-danger text-center" readonly>
                                                                    <span class="input-group-btn">
                                                                        <button type="button" id="BTN_ATT_U_ADD_MIC"
                                                                                class="btn btn-default btn-sm"
                                                                                data-popup="tooltip"
                                                                                title="Attribuez-vous à ce dossier ?"
                                                                                data-placement="right"><i
                                                                                class="icon-user-plus position-left"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-lg-8">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="content-divider text-muted form-group"><span><i
                                                            class="icon-printer2 pull-left"></i></span></div>
                                                        <div class="btn-group btn-group-justified">
                                                            <a href="#" class="btn bg-slate-700 btnn"> PREAVIS D'ARRIVEE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> AVIS D'ARRIVEE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE
                                                            MAGASINAGE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> BON A DELIVRER</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> INVOICE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE DIVER</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE AVOIR</a>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>
                                                <!--div class="row">
                                                <div class="col-md-12">
                                                    <table class="table datatable-select-basic nowrap" id="TAB_MIC">
                                                        <thead>
                                                            <tr>
                                                                <th class="hide_me">N°</th>
                                                                <th>Num BL</th>
                                                                <th>Nature Marchandise</th>
                                                                <th>Poids[KG]</th>
                                                                <th>Nombre</th>
                                                                <th>Client</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div-->
                                            </form>
                                            <form action="./" id="NEW_DMIC_ADD" name="NEW_DMIG_ADD" class="form-horizontal">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Fournisseur -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Fournisseur
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="fournisseurs_MIC" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Fournisseur -->
                                                        <!-- Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Client
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="clients_MIC" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Adresse
                                                            : </label>
                                                            <div class="col-sm-9"><input name="clients_ADR_MIG" id="clients_ADR_MIC" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <!-- P/C -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">P/C : </label>
                                                            <div class="col-sm-9">
                                                                <select class="bootstrap-select1 background_blue" id="clients_PC_MIC" data-live-search="true" data-width="100%">

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- /P/C -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Valeur en
                                                            devise : </label>
                                                            <div class="col-sm-9"><input name="VAL_DEV_MIG" id="VAL_DEV_MIC" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">B/L N°
                                                            : </label>
                                                            <div class="col-sm-9"><input name="BL_MIG" id="BL_MIC" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Navire
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="navire_MIC" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu Depart
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_DEP_MIC" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu
                                                            Arrivée : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_ARR_MIC" data-live-search="true" data-width="100%">

                                                            </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date Depart
                                                            :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_Dep_MIC">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date
                                                            Arrivée :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_arr_MIC">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <!-- Terme-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Terme
                                                                    :</label>
                                                                    <div class="col-sm-6">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="term_MIC">
                                                                        <option value="EXWORK">EXWORK</option>
                                                                        <option value="FOB">FOB</option>
                                                                        <option value="FAS">FAS</option>
                                                                        <option value="DDP">DDP</option>
                                                                        <option value="DDU">DDU</option>
                                                                        <option value="CF">CF</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Terme -->
                                                            </div>
                                                            <div class="col-md-6">
                                                                <!-- Marque-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Marque
                                                                    :</label>
                                                                    <div class="col-sm-8">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="marq_MIC">
                                                                        <option value="palette">Palette</option>
                                                                        <option value="colis">Colis</option>
                                                                        <option value="caisse">Caisse</option>
                                                                        <option value="unite">Unité</option>
                                                                        <option value="crts">Crts</option>
                                                                        <option value="bulk">Bulk</option>
                                                                        <option value="box">Box</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Marque -->
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Escale
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="escal_MIC" id="escal_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Nombre
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="NBR_MIC" id="NBR_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <button type="button" class="btn bg-teal-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_SAVE_MIC"><span
                                                            class="ladda-label"><i class="icon-exit3"></i> Retour</span>
                                                    </button>
                                                        <button type="button" class="btn bg-warning-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_CLOT_MIC"><span
                                                            class="ladda-label"><i
                                                            class="icon-unlocked"></i> clôturer</span></button>
                                                        <button type="button" class="btn bg-purple-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_MOD_MIC"><span
                                                            class="ladda-label"><i
                                                            class="icon-pen-plus"></i> Modifier</span></button>
                                                        <button type="button" class="btn bg-blue-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_ADD_MIC"><span
                                                            class="ladda-label"><i class="icon-plus-circle2"></i> Ajouter</span>
                                                    </button>
                                                    </div>
                                                </div>
                                                <hr>
                                            </form>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="panel panel-info panel-bordered">
                                                        <div class="panel-heading">
                                                            <h6 class="panel-title font_text_Bold text-center">
                                                                MARCHANDISE</h6>
                                                        </div>
                                                        <div class="panel-body">
                                                            <form class="form-horizontal">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Marchandise</label>
                                                                            <div class="col-sm-8"><input name="MARCH_MIC" id="MARCH_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Rubrique</label>
                                                                            <div class="col-sm-8"><input name="Rubr_MIC" id="Rubr_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group has-feedback">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Nombre</label>
                                                                            <div class="col-sm-8"><input name="Nombre_MIC" id="Nombre_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                                <div class="form-control-feedback">
                                                                                    <i class="icon-list-numbered"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 text-center">
                                                                        <div class="form-group has-feedback">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Poids</label>
                                                                            <div class="col-sm-8"><input name="Poids_MIC" id="Poids_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                                <div class="form-control-feedback">
                                                                                    KG
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 text-center">
                                                                        <button type="button" class="btn btn-danger btn-labeled" id="DEL_MICM"><b><i class="icon-cross3"></i></b>
                                                                        Supprimer
                                                                    </button>
                                                                        <button type="button" class="btn btn-success btn-labeled" id="MOD_MICM"><b><i class="icon-pencil"></i></b>
                                                                        Modifier
                                                                    </button>
                                                                        <button type="button" class="btn bg-brown-400 btn-labeled" id="ADD_MICM"><b><i
                                                                            class="icon-checkmark2"></i></b> Ajouter
                                                                    </button>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                            </form>
                                                            <div class="row">
                                                                <table class="table datatable-select-basic nowrap" style="width:100%" id="TAB_MIC1">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hide_me">N°</th>
                                                                            <th>Marchandise</th>
                                                                            <th>Poids[KG]</th>
                                                                            <th>RUB</th>
                                                                            <th>Nombre</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-md-6">
                                                    <div class="panel panel-info panel-bordered">
                                                        <div class="panel-heading">
                                                            <h6 class="panel-title font_text_Bold text-center"><span id="MARC_NAME_C" class="bg-slate-800 pull-left"></span> CONTENEUR
                                                            </h6>
                                                        </div>
                                                        <div class="panel-body">
                                                            <form class="form-horizontal">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Volume</label>
                                                                            <div class="col-sm-8">
                                                                                <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="VOLUM_MIC">
                                                                                <option value="20 '">20 '</option>
                                                                                <option value="40 '">40 '</option>
                                                                                <option value="40 HC">40 HC</option>
                                                                            </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Numéro</label>
                                                                            <div class="col-sm-8"><input name="Numero_MICC" id="Numero_MICC" type="text" class="form-control border-black border-lg text-black">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group has-feedback">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Rubrique</label>
                                                                            <div class="col-sm-8"><input name="Rubrq_MICC" id="Rubrq_MICC" type="text" class="form-control border-black border-lg text-black">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 text-center">
                                                                        <div class="form-group has-feedback">
                                                                            <label class="col-sm-4 control-label font_text_Bold">Poids
                                                                            vide</label>
                                                                            <div class="col-sm-8"><input name="PoidsV_MIC" id="PoidsV_MIC" type="text" class="form-control border-black border-lg text-black">
                                                                                <div class="form-control-feedback">
                                                                                    KG
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12 text-center">
                                                                        <button type="button" class="btn btn-danger btn-labeled" id="DEL_MICC"><b><i class="icon-cross3"></i></b>
                                                                        Supprimer
                                                                    </button>
                                                                        <button type="button" class="btn btn-success btn-labeled" id="MOD_MICC"><b><i class="icon-pencil"></i></b>
                                                                        Modifier
                                                                    </button>
                                                                        <button type="button" class="btn bg-brown-400 btn-labeled" id="ADD_MICC"><b><i
                                                                            class="icon-checkmark2"></i></b> Ajouter
                                                                    </button>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                            </form>
                                                            <div class="row">
                                                                <table class="table datatable-select-basic nowrap" style="width:100%" id="TAB_MIC2">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="hide_me">N°</th>
                                                                            <th>Volume</th>
                                                                            <th>Numéro</th>
                                                                            <th>Rubriq.</th>
                                                                            <th>Poids vide</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Liste des factures</h5>
                                                    <table class="table table-lg htNoWrap" style="width:100%" id="TAB_MIC_FACT">
                                                        <thead class="bg-info-700">
                                                            <tr>
                                                                <th class="hide_me">N°</th>
                                                                <th>N° Facture</th>
                                                                <th>Date</th>
                                                                <th>Type</th>
                                                                <th>B.Cmd</th>
                                                                <th>Total TTC</th>
                                                                <th>Client</th>
                                                                <th>Responsable</th>
                                                                <th>Téléphone</th>
                                                                <th>Fax</th>
                                                                <th>Email</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="bg-slate-700">

                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td class="hide_me"></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /traffic sources -->
                        </div>

                    </div>

                    <!--Aériene-->
                    <div class="row" id="DIV_AER" style="display: none">
                        <div class="col-lg-12">
                            <!-- Traffic sources -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-center text-muted" id="DOS_AER_NAME">Dossier Aériene</h4>
                                </div>

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="./" id="NEW_DAER" name="NEW_DAER" class="form-horizontal">
                                                <div class="row">
                                                    <div class="col-md-5 col-lg-4">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">attribuer à
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <div class="input-group">
                                                                    <input name="DM_ATTRIBUER_AER" id="DM_ATTRIBUER_AER" type="text" user="" class="form-control input-sm font_text_Bold text-danger text-center" readonly>
                                                                    <span class="input-group-btn">
                                                                            <button type="button" id="BTN_ATT_U_ADD_AER"
                                                                                    class="btn btn-default btn-sm"
                                                                                    data-popup="tooltip"
                                                                                    title="Attribuez-vous à ce dossier ?"
                                                                                    data-placement="right"><i
                                                                                    class="icon-user-plus position-left"></i></button>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-7 col-lg-8">

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="content-divider text-muted form-group"><span><i
                                                            class="icon-printer2 pull-left"></i></span></div>
                                                        <div class="btn-group btn-group-justified">
                                                            <a href="#" class="btn bg-slate-700 btnn"> PREAVIS D'ARRIVEE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> AVIS D'ARRIVEE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> INVOICE</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE DIVER</a>
                                                            <a href="#" class="btn bg-slate-700 btnn"> FACTURE AVOIR</a>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>
                                            </form>
                                            <form action="./" id="NEW_DAER_ADD" name="NEW_DAER_ADD" class="form-horizontal">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <!-- Fournisseur -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Fournisseur
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="fournisseurs_AER" data-live-search="true" data-width="100%"></select>
                                                            </div>
                                                        </div>
                                                        <!-- /Fournisseur -->
                                                        <!-- Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Client
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="clients_AER" data-live-search="true" data-width="100%"> </select>
                                                            </div>
                                                        </div>
                                                        <!-- /Client -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Adresse
                                                            : </label>
                                                            <div class="col-sm-9"><input name="clients_ADR_MIG" id="clients_ADR_AER" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                        <!-- P/C ->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label font_text_Bold">P/C : </label>
                                                        <div class="col-sm-9">
                                                            <select class="bootstrap-select1 background_blue" id="clients_PC_MIG" data-live-search="true" data-width="100%">

                                                            </select>
                                                        </div>
                                                    </div>
                                                    <!- /P/C -->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Valeur en
                                                            devise : </label>
                                                            <div class="col-sm-9"><input name="VAL_DEV_MIG" id="VAL_DEV_AER" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">LTA N°
                                                            : </label>
                                                            <div class="col-sm-9"><input name="BL_MIG" id="LTA_AER" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>

                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu Depart
                                                            : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_DEP_AER" data-live-search="true" data-width="100%"></select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->
                                                        <!-- Navire-->
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Lieu
                                                            Arrivée : </label>
                                                            <div class="col-sm-9">
                                                                <select class="form-control border-black border-lg text-black" id="port_ARR_AER" data-live-search="true" data-width="100%"></select>
                                                            </div>
                                                        </div>
                                                        <!-- /Navire -->

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Poids
                                                                    : </label>
                                                                    <div class="col-sm-6"><input name="Poids_MIG" id="Poids_AER" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            KG
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Magasin
                                                                    :</label>
                                                                    <div class="col-sm-8">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="magasin_AER">
                                                                        <option value="MAS">MAS</option>
                                                                        <option value="OPAT">OPAT</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date Depart
                                                            :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_Dep_AER">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group has-feedback">
                                                            <label class="col-sm-3 control-label font_text_Bold">Date
                                                            Arrivée :</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" class="form-control border-black border-lg text-black daterange-single" type="date" id="Date_arr_AER">
                                                                <div class="form-control-feedback">
                                                                    <i class="icon-calendar22"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label font_text_Bold">Marchandise
                                                            : </label>
                                                            <div class="col-sm-9"><input name="VAL_DEV_MIG" id="MARCH_AER" type="text" class="form-control border-black border-lg text-black">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <!-- Terme-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Terme
                                                                    :</label>
                                                                    <div class="col-sm-6">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="term_AER">
                                                                        <option value="EXWORK">EXWORK</option>
                                                                        <option value="FOB">FOB</option>
                                                                        <option value="FAS">FAS</option>
                                                                        <option value="DDP">DDP</option>
                                                                        <option value="DDU">DDU</option>
                                                                        <option value="CF">CF</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Terme -->
                                                            </div>
                                                            <div class="col-md-6">
                                                                <!-- Marque-->
                                                                <div class="form-group">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Marque
                                                                    :</label>
                                                                    <div class="col-sm-8">
                                                                        <select class="form-control border-black border-lg text-black" data-live-search="true" data-width="100%" id="marq_AER">
                                                                        <option value="palette">Palette</option>
                                                                        <option value="colis">Colis</option>
                                                                        <option value="caisse">Caisse</option>
                                                                        <option value="unite">Unité</option>
                                                                        <option value="crts">Crts</option>
                                                                        <option value="bulk">Bulk</option>
                                                                        <option value="box">Box</option>
                                                                        <option value="">-</option>
                                                                    </select>
                                                                    </div>
                                                                </div>
                                                                <!-- /Marque -->
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Largeur
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="larg_MIG" id="larg_AER" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-right"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Hauteur
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="haut_MIG" id="haut_AER" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-up-right"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-6 control-label font_text_Bold">Longueur
                                                                    :</label>
                                                                    <div class="col-sm-6"><input name="long_MIG" id="long_AER" type="text" class="form-control border-black border-lg text-black">
                                                                        <div class="form-control-feedback">
                                                                            <i class="icon-square-up"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group has-feedback">
                                                                    <label class="col-sm-4 control-label font_text_Bold">Nombre
                                                                    :</label>
                                                                    <div class="col-sm-8"><input name="NBR_MIC" id="Nombre_AER" type="text" class="form-control border-black border-lg text-black">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 text-right">
                                                        <button type="button" class="btn bg-teal-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_SAVE_AER"><span
                                                            class="ladda-label"><i class="icon-exit3"></i> Retour</span>
                                                    </button>
                                                        <button type="button" class="btn bg-warning-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_CLOT_AER"><span
                                                            class="ladda-label"><i
                                                            class="icon-unlocked"></i> clôturer</span></button>
                                                        <button type="button" class="btn bg-purple-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_MOD_AER"><span
                                                            class="ladda-label"><i
                                                            class="icon-pen-plus"></i> Modifier</span></button>
                                                        <button type="button" class="btn bg-blue-800 btn-ladda btn-ladda-progress" data-style="slide-left" id="BTN_ADD_AER"><span
                                                            class="ladda-label"><i class="icon-plus-circle2"></i> Ajouter</span>
                                                    </button>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Liste des factures</h5>
                                                        <table class="table table-lg htNoWrap" style="width:100%" id="TAB_AER_FACT">
                                                            <thead class="bg-info-700">
                                                                <tr>
                                                                    <th class="hide_me">N°</th>
                                                                    <th>N° Facture</th>
                                                                    <th>Date</th>
                                                                    <th>Type</th>
                                                                    <th>B.Cmd</th>
                                                                    <th>Total TTC</th>
                                                                    <th>Client</th>
                                                                    <th>Responsable</th>
                                                                    <th>Téléphone</th>
                                                                    <th>Fax</th>
                                                                    <th>Email</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="bg-slate-700">

                                                            </tbody>
                                                            <tfoot>
                                                                <tr>
                                                                    <td class="hide_me"></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /traffic sources -->
                        </div>

                    </div>
                    <!-- /Main charts -->
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                    <!-- /footer -->
                </div>
                <!-- /content area -->

            </div>
        </div>
    </div>
    <!-- /Page container -->
</body>

</html>