<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once($_SERVER['DOCUMENT_ROOT'].'/GTRANS/public/users/check_login_status.php');
if($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
	exit();
}
?><?php
if(isset($_GET['DM_CLE']) && $_GET['DM_CLE'] !== '' && is_numeric($_GET['DM_CLE'])){
    include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
    $DM_CLE = $_GET['DM_CLE'];
    $db = new MySQL();
    $AA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = $DM_CLE");
    if (count($AA_INFO)){
        $DM_NUM_DOSSIER = $AA_INFO[0]["DM_NUM_DOSSIER"];
        $DM_CODE_COMP_GROUP = $AA_INFO[0]["DM_CODE_COMP_GROUP"];
        $DM_NUM_BL = $AA_INFO[0]["DM_NUM_BL"];
        $DM_DATE_DECHARG = date("d/m/Y", strtotime($AA_INFO[0]["DM_DATE_DECHARG"]));
        $DM_FOURNISSEUR = $AA_INFO[0]["DM_FOURNISSEUR"];//code
        $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = $DM_FOURNISSEUR")[0]["FR_LIBELLE"];
        $DM_CLIENT = $AA_INFO[0]["DM_CLIENT"]; //code
        $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE,CL_EMAIL FROM trans.client WHERE CL_CODE = $DM_CLIENT");
        $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
        $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
        $CL_EMAIL = $DM_CLIENT_INFO[0]["CL_EMAIL"];
        $DM_PLACE_DELIVERY = $AA_INFO[0]["DM_PLACE_DELIVERY"]; //code
        if ($DM_PLACE_DELIVERY) {
            $DM_PLACE_DELIVERY_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE FROM trans.client WHERE CL_CODE = $DM_PLACE_DELIVERY");
            $DM_PLACE_DELIVERY_LIB = $DM_PLACE_DELIVERY_INFO[0]["CL_LIBELLE"];
        } else {
            $DM_PLACE_DELIVERY_LIB = '';
        }
        $DM_POD = $AA_INFO[0]["DM_POD"]; //code
        $DM_POL = $AA_INFO[0]["DM_POL"];//code
        $ALL_PO = "";
        if (empty($AA_INFO[0]["DM_CODE_COMP_GROUP"])) {
            $ALL_PO = $db->get_results("SELECT a.AE_LIBELLE AS POD,c.AE_LIBELLE AS POL FROM trans.aeroport a ,trans.aeroport c WHERE a.AE_CODE = $DM_POD AND c.AE_CODE = $DM_POL");
        } else {
            $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = $DM_POD AND c.PO_CODE = $DM_POL");
        }
        $DM_POD_LIB = $ALL_PO[0]["POD"];
        $DM_POL_LIB = $ALL_PO[0]["POL"];
        $DM_MARCHANDISE = $AA_INFO[0]["DM_MARCHANDISE"];
        $DM_POIDS = $AA_INFO[0]["DM_POIDS"];
        $DM_NOMBRE = $AA_INFO[0]["DM_NOMBRE"];
        $DM_MARQUE = $AA_INFO[0]["DM_MARQUE"];
        //$DM_NAVIRE = $AA_INFO[0]["DM_NAVIRE"];
        //$DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = $DM_NAVIRE")[0]["NA_LIBELLE"];
        //APPINFO
        $db_info = $db->get_results("SELECT * from conf_tab where id = 'MGOD'");
        $ADDRESS = explode(",", $db_info[0]["ADDRESS"]);
        $ADDRESS_1 = $ADDRESS[0].', '.$ADDRESS[1];
        $ADDRESS_2 = $ADDRESS[2];
        $TEL = $db_info[0]["TEL"];
    
        //Fact INFO
        $fact_inf = $db->get_results("SELECT * FROM invoice WHERE AAM_CODE_DOSSIER = $DM_CLE");
        $NUM_AA = $fact_inf[0]["AAM_NUMERO"];
        $DATE_AA = $fact_inf[0]["AAM_DATE"];
        $NUM_FACT = $fact_inf[0]["AAM_NUM_FACTURE"];
        $DATE_FACT = "";
        $DATE_FACT1 = "";
        if ($fact_inf[0]["AAM_DATE_FACTURE"]) {
            $DATE_FACT = date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_FACTURE"]));
        }else {
            $DATE_FACT = date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE"]));
        }
        if ($fact_inf[0]["AAM_DATE_FACTURE"]) {
            $DATE_FACT1 = $fact_inf[0]["AAM_DATE_FACTURE"];//date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_FACTURE"]));
        }else {
            $DATE_FACT1 = $fact_inf[0]["AAM_DATE"];//date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE"]));
        }
        $AAM_CODE = $fact_inf[0]["AAM_CODE"];
        $AAM_NUM_REGLEMENT = $fact_inf[0]["AAM_NUM_REGLEMENT"];
        $REGLE_STAT = '';
        $REG_INFO_PULL = '';
        switch ($AAM_NUM_REGLEMENT) {
            case '':
                $REGLE_STAT = '<li><span class="text-uppercase text-bold text-danger-700">Pas de règlement</span></li>';
                $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-danger-400 btn-labeled heading-btn"><b><i class="icon-spinner spinner text-default-800"></i></b>Règlement Facture</button>';
                break;

            default:
                
                $REGLE_STAT = '<li><span class="text-uppercase text-bold text-success-700">Règlement par VIREMENT :</span>
                                    <ul>
                                        <li><span class="text-uppercase text-bold text-primary-700">Numèro : '.$fact_inf[0]["AAM_NUM_REGLEMENT"].'</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Date   : '.date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_REGLEMENT"])).'</span></li>
                                    </ul>
                                </li>';
                $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-success-400 btn-labeled heading-btn"><b><i class="icon-checkmark2 text-default-800"></i></b> Règlement Facture</button>';
                break;
        }
    
        //GET TVA
        //$GET_TVA = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '3'")[0]["VALEUR"];
        //$RETOUR_DE_FONDS = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '2'")[0]["VALEUR"];
        //$GET_TIMBRE = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '4'")[0]["VALEUR"];
        $GET_AA_CPT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_avis_arrive")[0]["CPT"];
        $GET_FACT_CPT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_facture")[0]["CPT"];
    }else {
        header("location: ../../users/message.php?msg=Accès incorrect, fermez cette page et réessayez.Peut-être avez-vous besoin d'un Preavis d'arrivée d'abord? ou contactez le développeur");
        exit;
    }
}else{
    header("location: ../../users/message.php?msg=Accès incorrect, fermez cette page et réessayez ou contactez le développeur");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | IVOICE</title>

    <!-- Global stylesheets -->
    <link href="../../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/css/currency/currency.own.css" rel="stylesheet" type="text/css">
    <link href="../../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="../../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/extensions/sum.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/tables/datatables/extensions/currency.js"></script>
    <script type="text/javascript" src="../../../assets/js/currency/js/jquery.currency.js"></script>
    <script type="text/javascript" src="../../../assets/js/currency/js/jquery.currency.localization.fr_FR.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/tags/tagsinput.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../../assets/js/plugins/editors/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="../../../assets/js/plugins/editors/summernote/lang/summernote-fr-FR.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../api/NumberToLetter.js"></script>
    <script type="text/javascript" src="../api/inv.js"></script>
    <!-- /theme JS files -->

    <style>
        .font_text_Bold_24 {
            font-weight: bold;
            font-size: 24px;
            /*font: normal normal bold 24px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .font_text_Bold {
            font-size: 16px;
            font-weight: bold;
            /* font: normal normal bold 16px/normal Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;*/
        }
        
        .border_2px {
            border: 2px solid rgba(0, 0, 0, 0.91);
        }
        
        .background_blue {
            font-size: 13px;
            /*color: rgba(255, 255, 255, 1);
            background: #2196f3;*/
            color: rgb(0, 0, 0);
            background: #a9b8c3;
            font-weight: bold;
        }
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
        
        .selected td {
            background-color: #545252 !important;
            color: #ff4242 !important;
            /* Add !important to make sure override datables base styles */
        }
        
        select.bootstrap-select1 {
            width: 100%;
            padding: 6px 11px;
            border: 1px solid #a9b8c3;
            height: 34px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: #a9b8c3;
        }
        /* CAUTION: IE hackery ahead */
        
        select::-ms-expand {
            display: none;
            /* remove default arrow on ie10 and ie11 */
        }
        /* target Internet Explorer 9 to undo the custom arrow */
        
        @media screen and (min-width:0\0) {
            select {
                background: none\9;
                padding: 5px\9;
            }
        }

        .form-control {
            border: 1px solid #999;
        }
        
        .form-control:focus {
            border-color: #4733d4;
        }

        .btn-default {
            border-color: #999;
        }

        .btn-default:hover {
            border-color: #4733d4;
        }

        .input-group-addon {
            border: 1px solid #999;
        }
        select{
            text-align-last: center; text-align: center;
            -ms-text-align-last: center;
            -moz-text-align-last: center; text-align-last: center;
        }
        .bootstrap-tagsinput {
            border: 1px solid #fff !important;
        }

        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
            /*border-width: 2px !important;
            border-color: #000000 !important;
            border: 1px solid #ddd !important;
            border-radius: 3px !important;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            /* border-bottom-right-radius: 0; */
            /* border-bottom-left-radius: 0; */
            /* background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #eee), color-stop(80%, #fff)); */
            /* background-image: linear-gradient(#eee 20%, #fff 80%); */
            /* -webkit-box-shadow: 0 1px 0 #fff inset; */
            /* box-shadow: 0 1px 0 #fff inset; */
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }
    </style>
    <script>
        var DM_CLE = "<?php echo $DM_CLE; ?>";
        var AAM_CODE = "<?php echo $AAM_CODE; ?>";
        var DM_NUM_DOSSIER = "<?= $DM_NUM_DOSSIER; ?>";
        var DM_CODE_COMP_GROUP = "<?= $DM_CODE_COMP_GROUP; ?>";
    </script>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/navbar.html")?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/sidebar.php")?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="page-title">
                                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Dossier - Avis D'arrivée
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <!-- Invoice template -->
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">Invoice</h6>
                                <div class="heading-elements">
                                    <div class="heading-btn-group">
                                        <button type="button" class="btn bg-danger-400 btn-labeled" id="Del_AA_BTN"><b><i class="icon-warning"></i></b> Supprimer</button>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body no-padding-bottom">
                                <div class="row">
                                    <div class="col-sm-4 content-group">
                                        <img src="/GTRANS/assets/images/medways_log.png" class="content-group mt-10" alt="" style="width: 120px;">
                                        <ul class="list-condensed list-unstyled">
                                            <li><?php echo $ADDRESS_1; ?></li>
                                            <li><?php echo $ADDRESS_2; ?></li>
                                            <li><?php echo $TEL; ?></li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-4 content-group">
                                        <div class="row">
                                            <!--div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="SET_CAA" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-add"></i></button>
                                                    </div>
                                                    <input type="text" id="INP_CAA" class="form-control input-xs text-bold text-center" placeholder="Définir le compteur à">
                                                    <span class="input-group-addon" id="SP_CAA_INFO" data-popup="tooltip" title="Le N° suivant" data-placement="left"><?php echo $GET_AA_CPT; ?></span>
                                                </div>
                                            </div-->
                                            <!--div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="NEW_NAA" class="btn btn-default btn-xs" data-popup="tooltip" title="Charger" data-placement="right"><i class="icon-file-plus"></i></button>
                                                    </div>
                                                    <input type="text" id="INP_NAA" class="form-control input-xs text-bold text-center" placeholder="N° Avis d'arrivée" value="<?php echo $NUM_AA; ?>" data-popup="tooltip" title="veuillez modifier uniquement pour la recherche" data-placement="top">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="NEW_NAA_SET" class="btn btn-default btn-xs" data-popup="tooltip" title="Affecter" data-placement="left"><i class="icon-circle-right2"></i></button>
                                                        <button type="button" id="SEARCH_NAA" class="btn btn-default btn-xs" data-popup="tooltip" title="Chercher" data-placement="right"><i class="icon-search4"></i></button>
                                                    </div>
                                                </div>
                                            </div-->
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="MOD_CLIENT_AA" class="btn btn-default btn-xs btn-block" data-popup="tooltip" title="Changer" data-placement="right"><i class="icon-shuffle"></i></button>
                                                    </div>
                                                    <select class="form-control input-xs livesearch" id="LIST_CLIENT_AA" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 content-group">
                                        <div class="invoice-details">
                                            <h5 class="text-uppercase text-semibold"><?php echo 'INVOICE N° '.$NUM_FACT; ?></h5>
                                            <ul class="list-condensed list-unstyled">
                                                <li>Date: <span class="text-semibold"><?php echo $DATE_FACT == '' ?  date("d/m/Y") : date("d/m/Y", strtotime($DATE_FACT)); ?></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-lg-4 content-group">
                                        <span class="text-muted">Facture à:</span>
                                        <ul class="list-condensed list-unstyled">
                                            <li>
                                                <h5 id="DM_CLIENT_LIB"><?php echo $DM_CLIENT_LIB; ?></h5>
                                            </li>
                                            <li><span class="text-semibold"><?php echo $DM_CLIENT_ADDR; ?></span></li>
                                            <li>
                                                
                                            </li>
                                        </ul>
                                        <span class="text-muted">P/C :</span>
                                        <ul class="list-condensed list-unstyled">
                                            <li>
                                                <h5 id="DM_PLACE_DELIVERY_LIB"><?php echo $DM_PLACE_DELIVERY_LIB; ?></h5>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-md-3 col-lg-3 content-group">
                                        <span class="text-muted">Informations sur le dossier :</span>
                                        <ul class="list-condensed list-unstyled invoice-payment-details">
                                            <li>
                                                <h5>Dossier N° : <span class="text-right text-semibold"><?php echo $DM_NUM_DOSSIER; ?></span></h5>
                                            </li>
                                            <li>N° BL: <span><?php echo $DM_NUM_BL; ?></span></li>
                                            <li>Embarquement: <span><?php echo $DM_POL_LIB; ?></span></li>
                                            <li>Déchargement: <span><?php echo $DM_POD_LIB; ?></span></li>
                                            <li>Date d'arrivée: <span class="text-semibold"><?php echo $DM_DATE_DECHARG; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-1 col-lg-1">

                                    </div>

                                    <div class="col-md-4 col-lg-4 content-group">
                                        <span class="text-muted">Détails de paiement:</span>
                                        <ul class="list-condensed list-unstyled invoice-payment-details">
                                            <li>
                                                <h5>TOTAL : <span class="text-right text-semibold" id="TT_INFO"></span></h5>
                                            </li>
                                            <li>Fournisseur: <span class="text-semibold"><?php echo $DM_FOURNISSEUR_LIB; ?></span></li>
                                            <li>Marchandise: <span><?php echo $DM_MARCHANDISE; ?></span></li>
                                            <li>Nombre: <span><?php echo $DM_NOMBRE.' '.$DM_MARQUE; ?></span></li>
                                            <li>Poids: <span class="text-semibold"><?php echo $DM_POIDS.' KGs'; ?></span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <form class="form-horizontal">                                             
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Description</label>
                                                        <!--select class="form-control" id="DESC_AA" data-live-search="true" data-width="100%"></select-->
                                                        <input type="text" id="DESC_AA_N" list="DESC_AA" class="form-control">
                                                        <datalist id="DESC_AA"></datalist>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1"></div>
                                                <div class="col-sm-1">
                                                    <!--div class="form-group form-group-material">
                                                        <label class="control-label">Devise</label>
                                                        <input name="TDEVISE" id="TDEVISE" placeholder="Type Devise" type="text" class="form-control">
                                                    </div-->
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Montant</label>
                                                        <input name="Amount" id="Amount" placeholder="Montant" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="btn btn-danger btn-labeled" id="DEL_AA"><b><i class="icon-cross3"></i></b> Supprimer</button>
                                                    <button type="button" class="btn bg-brown-400 btn-labeled" id="ADD_AA"><b><i class="icon-checkmark2"></i></b> Ajouter</button>
                                                </div>
                                            </div>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-lg" id="AA_TAB">
                                    <thead class="bg-info-700">
                                        <tr>
                                            <th class="hide_me">#</th>
                                            <th>Description</th>
                                            <th class="col-sm-1">Montant</th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-slate-700">

                                    </tbody>
                                </table>
                            </div>

                            <div class="panel-body">
                                <div class="row invoice-payment">
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <!--div class="col-md-6">
                                                <div class="panel panel-white">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title text-center text-bold">Information sur la Facture</h6>
                                                    </div>
                                                    <div class="panel-body" >
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="NEW_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Charger" data-placement="right"><i class="icon-file-plus"></i></button>
                                                                </div>
                                                                <input type="text" id="INP_NFACT" class="form-control input-xs text-bold text-center" placeholder="N° Facture" value="<?php echo $NUM_FACT; ?>">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="NEW_NFACT_SET" class="btn btn-default btn-xs" data-popup="tooltip" title="Affecter" data-placement="left"><i class="icon-circle-right2"></i></button>
                                                                    <button type="button" id="SEARCH_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Chercher" data-placement="right"><i class="icon-search4"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="GET_FACT_DATE" class="btn btn-default btn-xs" data-popup="tooltip" title="Aujourd'hui" data-placement="right"><i class="icon-calendar3"></i></button>
                                                                </div>
                                                                <input type="date" id="INP_DATEFACT" name="INP_DATEFACT" class="form-control input-xs text-bold text-center" placeholder="Définir la Date à" value="<?= $DATE_FACT1; ?>">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="SET_FATE_FACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-file-check"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-footer text-center">
                                                            <div class="heading-elements">
                                                            <?php echo $REG_INFO_PULL; ?>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div-->

                                            <div class="col-md-6">
                                                <?php if ($_SESSION['LEVEL'] == "Administrateur") { ?>
                                                    <div class="panel panel-white">
                                                        <div class="panel-heading">
                                                            <h6 class="panel-title text-center text-bold">Information sur la Facture</h6>
                                                        </div>
                                                        <div class="panel-body" >
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="NEW_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Charger" data-placement="right"><i class="icon-file-plus"></i></button>
                                                                    </div>
                                                                    <input type="text" id="INP_NFACT" class="form-control input-xs text-bold text-center" placeholder="N° Facture" value="<?php echo $NUM_FACT; ?>">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="NEW_NFACT_SET" class="btn btn-default btn-xs" data-popup="tooltip" title="Affecter" data-placement="left"><i class="icon-circle-right2"></i></button>
                                                                        <button type="button" id="SEARCH_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Chercher" data-placement="right"><i class="icon-search4"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="GET_FACT_DATE" class="btn btn-default btn-xs" data-popup="tooltip" title="Aujourd'hui" data-placement="right"><i class="icon-calendar3"></i></button>
                                                                    </div>
                                                                    <input id="INP_DATEFACT" type="date" name="date" class="form-control input-xs text-bold text-center" placeholder="Définir la Date à" value="<?php echo $DATE_FACT; ?>">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="SET_FATE_FACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-file-check"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel-footer text-center">
                                                                <div class="heading-elements">
                                                                <?php echo $REG_INFO_PULL; ?>
                                                                </div>
                                                            </div>
                                                    </div>
                                                <?php
                                                    } else {
                                                ?>
                                                    <div class="panel panel-white">
                                                        <div class="panel-heading">
                                                            <h6 class="panel-title text-center text-bold">Information sur la Facture</h6>
                                                        </div>
                                                        <div class="panel-body" >
                                                            <div class="form-group">
                                                                <input type="text" id="INP_NFACT" class="form-control input-xs text-bold text-center" placeholder="N° Facture" value="<?php echo $NUM_FACT; ?>">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" id="INP_DATEFACT" type="date" name="date" class="form-control input-xs text-bold text-center" placeholder="Définir la Date à" value="<?php echo $DATE_FACT; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="panel-footer text-center">
                                                                <div class="heading-elements">
                                                                </div>
                                                            </div>
                                                    </div>
                                                <?php
                                                    } 
                                                ?>
                                            </div>

                                             <!-- reglement modal -->
                                            <div id="modal_Reglement" class="modal fade">
                                                <div class="modal-dialog modal-xs">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Règlement Facture</h5>
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input type="text" id="INP_F_VIREMENT_Num" class="form-control input-xs text-bold text-center" placeholder="Numéro" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="date" id="INP_F_VIREMENT_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                            <button class="btn btn-primary" id="REG_BTN"><i class="icon-check"></i> Effectuer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /reglement modal -->

                                            
                                            <div class="col-md-6">
                                                <!--div class="tabbable">
                                                    <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                                        <li class="active"><a href="#bottom-justified-tab1" data-toggle="tab">Convert. devises en ligne</a></li>
                                                        <li><a href="#bottom-justified-tab2" data-toggle="tab">Convert. devises locale</a></li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="bottom-justified-tab1">
                                                            <div id="currency-widget"></div>
                                                        </div>

                                                        <div class="tab-pane" id="bottom-justified-tab2">
                                                            <div class="form-group">
                                                                <input type="text" id="INP_VAL_DEV" class="form-control input-xs text-bold text-center" placeholder="Valeur" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" id="INP_TAUX_DEV" class="form-control input-xs text-bold text-center" placeholder="Taux" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="CONV_L_DEV" class="btn btn-default btn-xs" data-popup="tooltip" title="Convert" data-placement="right"><i class="icon-chevron-right"></i></button>
                                                                    </div>
                                                                    <input type="text" id="INP_RES_DEV" class="form-control input-xs text-bold text-center" placeholder="Resultat" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div-->
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <div class="content-group">
                                            <h6>Total</h6>
                                            <div class="table-responsive no-border">
                                                <table class="table">
                                                    <tbody>
                                                        <!--tr>
                                                            <th>Devise :</th>
                                                            <td class="text-right" id="T_DEVISE"></td>
                                                        </tr-->
                                                        <tr>
                                                            <th>Total TTC:</th>
                                                            <td class="text-right text-primary">
                                                                <h5 class="text-semibold" id="T_TT"></h5>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Devise :</th>
                                                            <td class="text-right">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-xs" id="inp_dev" placeholder="">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn bg-teal btn-xs" id="btn_dev" type="button">valider</button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Valeur en Dinars :</th>
                                                            <td class="text-right">
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control input-xs" id="inp_vd" placeholder="">
                                                                    <span class="input-group-btn">
                                                                        <button class="btn bg-teal btn-xs" id="btn_vd" type="button">valider</button>
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel panel-flat border-top-info border-bottom-info">
                                            <div class="panel-body">
                                                <ul>
                                                    <li><span id="T_TT_L" class="text-uppercase text-bold"></span></li>
                                                    <li><span class="text-uppercase text-bold">statut de règlement :</span>
                                                        <ul id="FACT_REG_info">
                                                            <?php echo $REGLE_STAT;?>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" id="FACT_PRINT_BTN" class="btn btn-success btn-labeled"><b><i class="icon-printer"></i></b> Impression</button>
                                </div>
                            </div>
                        </div>
                        <!-- /invoice template -->
                        <!-- Footer -->
                        <?php include_once($_SERVER['DOCUMENT_ROOT']."/GTRANS/sys/include/html/footer.php")?>
                        <!-- /footer -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>