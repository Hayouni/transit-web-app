<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
?>
<?php
$get_doss = null;
if (isset($_GET['NumD']) && $_GET['NumD'] !== '' && is_numeric($_GET['NumD'])) {
    $get_doss = $_GET['NumD'];
}
?>
<!DOCTYPE html>


<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | Dossier</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/js/plugins/forms/selects/chosen/chosen.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment_locales.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/datepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery_ui/widgets.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/selectboxit.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/chosen/chosen.jquery.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/fnReloadAjax.js"></script>

    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="./api/NumberToLetter.js"></script>
    <script type="text/javascript" src="./api/d_mg.js"></script>
    <script type="text/javascript" src="./api/d_mc.js"></script>
    <script type="text/javascript" src="./api/d_a.js"></script>
    <script type="text/javascript" src="./api/d_main.js"></script>
    <!-- /theme JS files -->

    <!--Style text box-->

    <style>
        .content-wrapper+.sidebar-default {
            border-left: 1px solid #37474f;
        }
        
        .sidebar-default {
            border-bottom: 0;
            border-right: 1px solid #263238;
        }
        
        .sidebar-default {
            background-color: #263238;
            color: #fff;
        }
        
        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }

        .selected td {
            background-color: #545252 !important;
            color: #ff4242 !important;
            /* Add !important to make sure override datables base styles */
        }
        .chosen-container-single .chosen-single {
            border: 2px solid #000 !important;
            /*border-width: 2px !important;
            border-color: #000000 !important;
            border: 1px solid #ddd !important;
            border-radius: 3px !important;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075) !important;
            -webkit-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s !important;
            /* border-bottom-right-radius: 0; */
            /* border-bottom-left-radius: 0; */
            /* background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #eee), color-stop(80%, #fff)); */
            /* background-image: linear-gradient(#eee 20%, #fff 80%); */
            /* -webkit-box-shadow: 0 1px 0 #fff inset; */
            /* box-shadow: 0 1px 0 #fff inset; */
        }

        .chosen-container .chosen-results li.highlighted {
            background-color: #757575;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(20%, #3875d7), color-stop(90%, #2a62bc));
            background-image: linear-gradient(#757575 20%, #757575 90%);
            color: #fff;
        }

        .chosen-container .chosen-results {
            color: #000;
        }

        .chosen-single{
            display: block !important;
            font-weight: 900 !important;
            width: 100% !important;
            height: 36px !important;
            padding: 7px 12px !important;
            font-size: 13px !important;
            line-height: 1.5384616 !important;
            color: #333 !important;
            background-color: #fff !important;
            background-image: none !important;
        }

    </style>
    <script>
        var NumD = "<?= $get_doss; ?>";
    </script>
</head>

<body class="navbar-top scrollbar sidebar-opposite-visible sidebar-main-hidden" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->
    
    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar" id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->

            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default page-header-sm">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Dossier </h4>
                        </div>
                        <div class="heading-elements">
                            <form class="heading-form">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select class="bootstrap-select opend-folder-slbox" data-width="100%"> </select>

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-primary" id="sel-here"><i class="icon-arrow-down52"></i></button>
                                            <button type="button" class="btn btn-danger" id="sel-out"><i class="icon-arrow-up-right32"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /page header -->

                <div class="content">

                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/d_mg.html") ?>

                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/d_mc.html") ?>
                    
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/d_a.html") ?>

                    <!-- Full width modal -->
					<div id="modal_facture" class="modal fade">
						<div class="modal-dialog modal-full">
							<div class="modal-content">
								<div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5>Liste des factures</h5>
                                            <table class="table display datatable-button-print-rows table-striped table-bordered" style="width:100%" id="TAB_FACT">
                                                <thead class="bg-blue-400">
                                                    <tr>
                                                        <th class="hide_me">N°</th>
                                                        <th>N° Facture</th>
                                                        <th>Date</th>
                                                        <th>Type</th>
                                                        <th>B.Cmd</th>
                                                        <th>Total TTC</th>
                                                        <th>Client</th>
                                                        <th>Responsable</th>
                                                        <th>Téléphone</th>
                                                        <th>Fax</th>
                                                        <th>Email</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-black">

                                                </tbody>
                                                <tfoot class="bg-blue-400">
                                                    <tr>
                                                        <td class="hide_me"></td>
                                                        <th>N° Facture</th>
                                                        <th>Date</th>
                                                        <th>Type</th>
                                                        <th>B.Cmd</th>
                                                        <th>Total TTC</th>
                                                        <th>Client</th>
                                                        <th>Responsable</th>
                                                        <th>Téléphone</th>
                                                        <th>Fax</th>
                                                        <th>Email</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

								<div class="modal-footer text-center">
									<button type="button" class="btn btn-danger btn-xs" data-dismiss="modal">Fermer</button>
								</div>
							</div>
						</div>
					</div>
					<!-- /full width modal -->
                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                    <!-- /footer -->
                </div>
            </div>
            <!-- /content area -->

            <!-- Opposite sidebar -->
            <div class="sidebar sidebar-opposite sidebar-default" id="sidebr_rt">
                <div class="sidebar-content">

                    <!-- Sidebar search -->
                    <div class="sidebar-category">
                        <div class="category-content">
                            <div class="has-feedback has-feedback-left">
                                <input type="number" id="DOS_NUM_INP" class="form-control text-black" placeholder="N° Dossier .." value="<?= $get_doss; ?>">
                                <div class="form-control-feedback">
                                    <i class="icon-folder text-size-base text-muted"></i>
                                </div>
                            </div>
                            <div class="category-content">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <button class="btn bg-warning-700 btn-block btn-float btn-float-lg" type="button" id="GET_DOS_BTN"><i class="icon-file-download2"></i> <span>Consulté</span></button>
                                        <button class="btn bg-danger-700 btn-block btn-float btn-float-lg" type="button" id="DEL_DOS_BTN"><i class="icon-file-minus2"></i> <span>Supprimer</span></button>
                                    </div>

                                    <div class="col-xs-6">
                                        <button class="btn bg-success-700 btn-block btn-float btn-float-lg" type="button" id="ADD_DOS_BTN"><i class="icon-file-plus2"></i> <span>Ajouter</span></button>
                                        <button class="btn bg-blue-700 btn-block btn-float btn-float-lg" type="button" id="CLS_DOS_BTN"><i class="icon-file-empty2"></i> <span>Nettoyer</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /sidebar search -->

                    <!-- Other selects -->
                    <div class="sidebar-category">
                        <div class="category-title">
                            <span>TYPE DE DOSSIER</span>
                        </div>

                        <div class="category-content">
                            <div class="form-group">
                                <select class="selectbox" id="SEL_TYPE_DOS">
                                    <option value="M" data-icon="icon-ship">Maritime</option>
                                    <option value="A" data-icon="icon-airplane2">Aerien</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectbox" id="SEL_IMEX_DOS">
                                    <option value="EXPORT" data-icon="icon-esc">Export</option>
                                    <option value="IMPORT" data-icon="icon-enter6">Import</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectbox" id="SEL_GC_DOS">
                                    <option value="G" data-icon="icon-checkbox-partial2">Groupage</option>
                                    <option value="C" data-icon="icon-diamond3">Complet</option>
                                </select>
                            </div>
                        </div>

                        <div class="category-content text-center">
                            <div class="mb-10">
                                <button type="button" class="btn btn-danger btn-labeled" id="BTN_SET_COICE_DOS">
                                    <b><i class="icon-arrow-left16"></i></b> Appliquer le choix
                                </button>
                            </div>

                        </div>
                    </div>
                    <!-- /other selects -->
                </div>
            </div>
            <!-- /opposite sidebar -->
        </div>
        <!-- /Page container -->
</body>

</html>