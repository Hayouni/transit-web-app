<?php


include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur", "Financier"])) {
    header("location: /GTRANS");
    exit();
}
?><?php
    if (isset($_GET['DM_CLE']) && $_GET['DM_CLE'] !== '' && is_numeric($_GET['DM_CLE']) && isset($_GET['AAM_CODE']) && $_GET['AAM_CODE'] !== '' && is_numeric($_GET['AAM_CODE'])) {
        include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/sys/drivers/mysql.php');
        $DM_CLE = $_GET['DM_CLE'];
        $AAM_CODE = $_GET['AAM_CODE'];
        $db = new MySQL();
        $AA_INFO = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = $DM_CLE");
        if (count($AA_INFO)) {
            $DM_NUM_DOSSIER = $AA_INFO[0]["DM_NUM_DOSSIER"];
            $DM_NUM_BL = $AA_INFO[0]["DM_NUM_BL"];
            $DM_DATE_DECHARG = date("d/m/Y", strtotime($AA_INFO[0]["DM_DATE_DECHARG"]));
            $DM_FOURNISSEUR = $AA_INFO[0]["DM_FOURNISSEUR"];//code
            $DM_FOURNISSEUR_LIB = $db->get_results("SELECT FR_LIBELLE FROM trans.fournisseur WHERE FR_CODE = $DM_FOURNISSEUR")[0]["FR_LIBELLE"];
            $DM_CLIENT = $AA_INFO[0]["DM_CLIENT"]; //code
            $DM_CLIENT_INFO = $db->get_results("SELECT CL_LIBELLE,CL_ADRESSE,CL_EMAIL FROM trans.client WHERE CL_CODE = $DM_CLIENT");
            $DM_CLIENT_LIB = $DM_CLIENT_INFO[0]["CL_LIBELLE"];
            $DM_CLIENT_ADDR = $DM_CLIENT_INFO[0]["CL_ADRESSE"];
            $CL_EMAIL = $DM_CLIENT_INFO[0]["CL_EMAIL"];
            $DM_POD = $AA_INFO[0]["DM_POD"]; //code
            $DM_POL = $AA_INFO[0]["DM_POL"];//code
            $ALL_PO = $db->get_results("SELECT a.PO_LIBELLE AS POD,c.PO_LIBELLE AS POL FROM trans.port a ,trans.port c WHERE a.PO_CODE = $DM_POD AND c.PO_CODE = $DM_POL");
            $DM_POD_LIB = $ALL_PO[0]["POD"];
            $DM_POL_LIB = $ALL_PO[0]["POL"];
            $DM_MARCHANDISE = $AA_INFO[0]["DM_MARCHANDISE"];
            $DM_POIDS = $AA_INFO[0]["DM_POIDS"];
            $DM_NOMBRE = $AA_INFO[0]["DM_NOMBRE"];
            $DM_MARQUE = $AA_INFO[0]["DM_MARQUE"];
            $DM_NAVIRE = $AA_INFO[0]["DM_NAVIRE"];
            if ($DM_NAVIRE) {
                $DM_NAVIRE_LIB = $db->get_results("SELECT NA_LIBELLE FROM trans.navire WHERE NA_CODE = $DM_NAVIRE")[0]["NA_LIBELLE"];
            } else {
                $DM_NAVIRE_LIB = "";
            }
        
        //APPINFO
            $db_info = $db->get_results("SELECT * from conf_tab where id = 'MGOD'");
            $ADDRESS = explode(",", $db_info[0]["ADDRESS"]);
            $ADDRESS_1 = $ADDRESS[0] . ', ' . $ADDRESS[1];
            $ADDRESS_2 = $ADDRESS[2];
            $TEL = $db_info[0]["TEL"];
    
        //Fact INFO
            $fact_inf = $db->get_results("SELECT * FROM avis_arrive_mig WHERE AAM_CODE = $AAM_CODE");
            $AAM_TYPE_FACT = $fact_inf[0]["AAM_TYPE_FACT"];
            $NUM_FACT = $fact_inf[0]["AAM_NUM_FACTURE"];
            $lib_FT = $db->get_results("SELECT FT_LIBELLE FROM type_facture WHERE FT_CODE = '$AAM_TYPE_FACT'");
            $str_type_fact = (count($lib_FT)) ? 'Facture ' . $lib_FT[0]['FT_LIBELLE'] . ' N° ' . $NUM_FACT : 'Facture N° ' . $NUM_FACT;
            $DATE_FACT = date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_FACTURE"]));
            $AAM_BON_COMMAND = $fact_inf[0]["AAM_BON_COMMAND"];
            $REGLE_STAT = '';
            $REG_INFO_PULL = '';
            switch ($AAM_BON_COMMAND) {
                case 'BC':
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-danger-700">Pas de règlement</span></li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-danger-400 btn-labeled heading-btn"><b><i class="icon-spinner spinner text-default-800"></i></b>Règlement Facture</button>';
                    break;
                case 'ESPECE':
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-success-700">Règlement en espèce</span>
                                    <ul>
                                        <li><span class="text-uppercase text-bold text-primary-700">Date   : ' . date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_REG"])) . '</span></li>
                                    </ul>
                                </li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-success-400 btn-labeled heading-btn"><b><i class="icon-checkmark2 text-default-800"></i></b> Règlement Facture</button>';
                    break;
                case 'CHEQUE':
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-success-700">Règlement par Chèque :</span>
                                    <ul>
                                        <li><span class="text-uppercase text-bold text-primary-700">Banque : ' . $fact_inf[0]["AAM_BANQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Numèro : ' . $fact_inf[0]["AAM_NUM_CHEQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Date   : ' . date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_REG"])) . '</span></li>
                                    </ul>
                                </li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-success-400 btn-labeled heading-btn"><b><i class="icon-checkmark2 text-default-800"></i></b> Règlement Facture</button>';
                    break;
                case 'TRAITE':
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-success-700">Règlement par Traite :</span>
                                    <ul>
                                        <li><span class="text-uppercase text-bold text-primary-700">Banque : ' . $fact_inf[0]["AAM_BANQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Numèro : ' . $fact_inf[0]["AAM_NUM_CHEQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Date   : ' . date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_REG"])) . '</span></li>
                                    </ul>
                                </li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-success-400 btn-labeled heading-btn"><b><i class="icon-checkmark2 text-default-800"></i></b> Règlement Facture</button>';
                    break;
                case 'VIREMENT':
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-success-700">Règlement par VIREMENT :</span>
                                    <ul>
                                        <li><span class="text-uppercase text-bold text-primary-700">Banque : ' . $fact_inf[0]["AAM_BANQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Numèro : ' . $fact_inf[0]["AAM_NUM_CHEQUE"] . '</span></li>
                                        <li><span class="text-uppercase text-bold text-primary-700">Date   : ' . date("d/m/Y", strtotime($fact_inf[0]["AAM_DATE_REG"])) . '</span></li>
                                    </ul>
                                </li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-success-400 btn-labeled heading-btn"><b><i class="icon-checkmark2 text-default-800"></i></b> Règlement Facture</button>';
                    break;
                default:
                    $REGLE_STAT = '<li><span class="text-uppercase text-bold text-danger-700">Pas de reglement</span></li>';
                    $REG_INFO_PULL = '<button type="button" data-toggle="modal" data-target="#modal_Reglement" class="btn bg-danger-400 btn-labeled heading-btn"><b><i class="icon-spinner spinner text-default-800"></i></b>Règlement Facture</button>';
                    break;
            }
    
        //GET TVA
            $GET_TVA = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '3'")[0]["VALEUR"];
            $RETOUR_DE_FONDS = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '2'")[0]["VALEUR"];
            $GET_TIMBRE = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '4'")[0]["VALEUR"];
            $GET_AA_CPT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_avis_arrive")[0]["CPT"];
            $GET_FACT_CPT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_facture")[0]["CPT"];
        } else {
            header("location: /GTRANS/public/users/message.php?msg=Accès incorrect, fermez cette page et réessayez.Peut-être avez-vous besoin d'un Preavis d'arrivée d'abord? ou contactez le développeur");
            exit;
        }
    } else {
        header("location: /GTRANS/public/users/message.php?msg=Accès incorrect, fermez cette page et réessayez ou contactez le développeur");
        exit;
    }

    ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | FACTURATION</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/currency/currency.own.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
	<script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/sum.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/currency.js"></script>
    <script type="text/javascript" src="../../assets/js/currency/js/jquery.currency.js"></script>
    <script type="text/javascript" src="../../assets/js/currency/js/jquery.currency.localization.fr_FR.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/tags/tagsinput.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/summernote.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/editors/summernote/lang/summernote-fr-FR.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../Dossier/api/NumberToLetter.js"></script>
    <script type="text/javascript" src="./api/fact.js"></script>
    <!-- /theme JS files -->

    <style>
        
        th.hide_me,
        td.hide_me {
            display: none;
        }
        
        .selected td {
            background-color: #545252 !important;
            color: #ff4242 !important;
            /* Add !important to make sure override datables base styles */
        }
        
        select.bootstrap-select1 {
            width: 100%;
            padding: 6px 11px;
            border: 1px solid #a9b8c3;
            height: 34px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            background: #a9b8c3;
        }
        /* CAUTION: IE hackery ahead */
        
        select::-ms-expand {
            display: none;
            /* remove default arrow on ie10 and ie11 */
        }
        /* target Internet Explorer 9 to undo the custom arrow */
        
        @media screen and (min-width:0\0) {
            select {
                background: none\9;
                padding: 5px\9;
            }
        }

        .form-control {
            border: 1px solid #999;
        }
        
        .form-control:focus {
            border-color: #4733d4;
        }

        .btn-default {
            border-color: #999;
        }

        .btn-default:hover {
            border-color: #4733d4;
        }

        .input-group-addon {
            border: 1px solid #999;
        }
        select{
            text-align-last: center; text-align: center;
            -ms-text-align-last: center;
            -moz-text-align-last: center; text-align-last: center;
        }
        .bootstrap-tagsinput {
            border: 1px solid #fff !important;
        }
    </style>
    <script>
        var DM_CLE = "<?php echo $DM_CLE; ?>";
        var AAM_CODE = "<?php echo $AAM_CODE; ?>";
        var TVA = "<?php echo $GET_TVA;
                    $GET_TVA = str_replace(".000", "", $GET_TVA); ?>";
        var TIMBRE = "<?php echo $GET_TIMBRE;
                        $GET_TIMBRE = str_replace(".000", "", $GET_TIMBRE); ?>";
        var CL_CODE = "<?php echo $DM_CLIENT; ?>";
        var RETOUR_DE_FONDS = "<?php echo $RETOUR_DE_FONDS; ?>";
        var TYP_FACT = "<?php echo $AAM_TYPE_FACT; ?>";
        var DM_CLIENT_LIB = "<?php echo $DM_CLIENT_LIB; ?>";
        var DM_CLIENT_ADDR = "<?php echo $DM_CLIENT_ADDR; ?>";
        var DM_FOURNISSEUR_LIB = "<?php echo $DM_FOURNISSEUR_LIB; ?>";
        var DM_NAVIRE_LIB = "<?php echo $DM_NAVIRE_LIB; ?>";
        var DM_POD_LIB = "<?php echo $DM_POD_LIB; ?>";
        var DM_POL_LIB = "<?php echo $DM_POL_LIB; ?>";
        var C2L = '';
        function add_to_DESCSUIT(btn) {
            var dss = $('#DESC_AA_SUIT').val();
            $('#DESC_AA_SUIT').val(dss + ' '+ $(btn).attr('data'));
        }
    </script>
</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="page-title">
                                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Facturation
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <!-- Invoice template -->
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">Facture</h6>
                                <div class="heading-elements">
                                    <div class="heading-btn-group">
                                        <button type="button" class="btn bg-danger-400 btn-labeled" id="Del_AA_BTN"><b><i class="icon-warning"></i></b> Supprimer</button>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body no-padding-bottom">
                                <div class="row">
                                    <div class="col-sm-4 content-group">
                                        <img src="../../assets/images/medways_log.png" class="content-group mt-10" alt="" style="width: 120px;">
                                        <ul class="list-condensed list-unstyled">
                                            <li><?php echo $ADDRESS_1; ?></li>
                                            <li><?php echo $ADDRESS_2; ?></li>
                                            <li><?php echo $TEL; ?></li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-4 content-group">
                                        <div class="row">
                                            <!--div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="SET_CAA" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-add"></i></button>
                                                    </div>
                                                    <input type="text" id="INP_CAA" class="form-control input-xs text-bold text-center" placeholder="Définir le compteur à">
                                                    <span class="input-group-addon" id="SP_CAA_INFO" data-popup="tooltip" title="Le N° suivant" data-placement="left"><?php echo $GET_AA_CPT; ?></span>
                                                </div>
                                            </div-->
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="SET_TYP_FACT" class="btn btn-default btn-xs btn-block" data-popup="tooltip" title="Appliquer" data-placement="right"><i class="icon-file-check2"></i></button>
                                                    </div>
                                                    <select class="form-control input-xs" id="LIST_TYPE_FACT" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <button type="button" id="MOD_CLIENT_AA" class="btn btn-default btn-xs btn-block" data-popup="tooltip" title="Changer" data-placement="right"><i class="icon-shuffle"></i></button>
                                                    </div>
                                                    <select class="form-control input-xs" id="LIST_CLIENT_AA" data-live-search="true" data-width="100%"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4 content-group">
                                        <div class="invoice-details">
                                            <h5 class="text-uppercase text-semibold"><?php echo $str_type_fact; ?></h5>
                                            <ul class="list-condensed list-unstyled">
                                                <li>Date: <span class="text-semibold"><?php echo $DATE_FACT ?></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-lg-4 content-group">
                                        <span class="text-muted">Facture à:</span>
                                        <ul class="list-condensed list-unstyled">
                                            <li>
                                                <h5 id="DM_CLIENT_LIB"><?php echo $DM_CLIENT_LIB; ?></h5>
                                            </li>
                                            <li><span class="text-semibold"><?php echo $DM_CLIENT_ADDR; ?></span></li>
                                            <li>
                                                
                                            </li>
                                        </ul>
                                        <span class="text-muted">Pour le compte :</span>
                                        <ul class="list-condensed list-unstyled">
                                            <li>
                                                <h5 id="DM_CLIENT_LIB"><?php echo $DM_CLIENT_LIB; ?></h5>
                                            </li>
                                            <li><span class="text-semibold"><?php echo $DM_CLIENT_ADDR; ?></span></li>
                                            <li>
                                                
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-md-3 col-lg-3 content-group">
                                        <span class="text-muted">Informations sur le dossier :</span>
                                        <ul class="list-condensed list-unstyled invoice-payment-details">
                                            <li>
                                                <h5>Dossier N° : <span class="text-right text-semibold"><?php echo $DM_NUM_DOSSIER; ?></span></h5>
                                            </li>
                                            <li>N° BL: <span><?php echo $DM_NUM_BL; ?></span></li>
                                            <li>Port d'embarquement: <span><?php echo $DM_POL_LIB; ?></span></li>
                                            <li>Port de déchargement: <span><?php echo $DM_POD_LIB; ?></span></li>
                                            <li>Date d'arrivée: <span class="text-semibold"><?php echo $DM_DATE_DECHARG; ?></span></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-1 col-lg-1">

                                    </div>

                                    <div class="col-md-4 col-lg-4 content-group">
                                        <span class="text-muted">Détails de paiement:</span>
                                        <ul class="list-condensed list-unstyled invoice-payment-details">
                                            <li>
                                                <h5>TOTAL TTC: <span class="text-right text-semibold" id="TT_INFO"></span></h5>
                                            </li>
                                            <li>Fournisseur: <span class="text-semibold"><?php echo $DM_FOURNISSEUR_LIB; ?></span></li>
                                            <!--li>Navire: <span><?php echo $DM_NAVIRE_LIB; ?></span></li-->
                                            <li>Marchandise: <span><?php echo $DM_MARCHANDISE; ?></span></li>
                                            <li>Nombre: <span><?php echo $DM_NOMBRE . ' ' . $DM_MARQUE; ?></span></li>
                                            <li>Poids: <span class="text-semibold"><?php echo $DM_POIDS . ' KGs'; ?></span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <form class="form-horizontal">                                             
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Description</label>
                                                        <input name="DESC_AA" id="DESC_AA" placeholder="Description" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Suite</label>
                                                        <div class="input-group">
                                                            <div class="input-group-btn">
                                                                <input name="DESC_AA_SUIT" id="DESC_AA_SUIT" placeholder="Suite" type="text" class="form-control">
                                                            </div>
                                                            <div class="input-group-btn">
                                                                <button type="button" class="btn btn-default dropdown-toggle btn-icon" data-toggle="dropdown" aria-expanded="false">
                                                                    <span class="caret"></span>
                                                                </button>
                                                                <ul class="dropdown-menu bg-slate-600" style="min-width: 60px;">
                                                                    <li><a onclick="add_to_DESCSUIT(this);" data="USD">USD</a></li>
                                                                    <li><a onclick="add_to_DESCSUIT(this);" data="$">$</a></li>
                                                                    <li><a onclick="add_to_DESCSUIT(this);" data="EURO">EURO</a></li>
                                                                    <li><a onclick="add_to_DESCSUIT(this);" data="€">€</a></li>
                                                                    <li><a onclick="add_to_DESCSUIT(this);" data="£">£</a></li>
                                                                </ul>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div><div class="col-sm-1"></div>
                                                <div class="col-sm-2">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Non Taxable</label>
                                                        <input name="IN_N_TAX_AA" id="IN_N_TAX_AA" placeholder="Non Taxable" type="text" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group form-group-material">
                                                        <label class="control-label">Taxable</label>
                                                        <input name="IN_TAX_AA" id="IN_TAX_AA" placeholder="Taxable" type="text" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="btn btn-danger btn-labeled" id="DEL_AA"><b><i class="icon-cross3"></i></b> Supprimer</button>
                                                    <button type="button" class="btn bg-brown-400 btn-labeled" id="ADD_AA"><b><i class="icon-checkmark2"></i></b> Ajouter</button>
                                                </div>
                                            </div>
                                        </form>
                                        <br>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="table-responsive">
                                <table class="table table-lg" id="AA_TAB">
                                    <thead class="bg-info-700">
                                        <tr>
                                            <th class="hide_me">#</th>
                                            <th>Description</th>
                                            <th class="col-sm-1">NON TAXABLE</th>
                                            <th class="col-sm-1">TAXABLE</th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-slate-700">

                                    </tbody>
                                </table>
                            </div>

                            <div class="panel-body">
                                <div class="row invoice-payment">
                                    <div class="col-sm-7">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="panel panel-white">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title text-center text-bold">Information sur la Facture</h6>
                                                    </div>
                                                    
                                                    <div class="panel-body" >
                                                        <!--div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="SET_CFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-add"></i></button>
                                                                </div>
                                                                <input type="text" id="INP_CFACT" class="form-control input-xs text-bold text-center" placeholder="Définir le compteur à">
                                                                <span class="input-group-addon" id="SP_CFACT_INFO" data-popup="tooltip" title="Le N° suivant" data-placement="left"><?php echo $GET_FACT_CPT; ?></span>
                                                            </div>
                                                        </div-->
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="NEW_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Charger" data-placement="right"><i class="icon-file-plus"></i></button>
                                                                </div>
                                                                <input type="text" id="INP_NFACT" class="form-control input-xs text-bold text-center" placeholder="N° Facture" value="<?php echo $NUM_FACT; ?>">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="NEW_NFACT_SET" class="btn btn-default btn-xs" data-popup="tooltip" title="Affecter" data-placement="left"><i class="icon-circle-right2"></i></button>
                                                                    <button type="button" id="SEARCH_NFACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Chercher" data-placement="right"><i class="icon-search4"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="GET_FACT_DATE" class="btn btn-default btn-xs" data-popup="tooltip" title="Aujourd'hui" data-placement="right"><i class="icon-calendar3"></i></button>
                                                                </div>
                                                                <input type="text" id="INP_DATEFACT" type="date" name="date" class="form-control input-xs text-bold text-center" placeholder="Définir la Date à" value="<?php echo $DATE_FACT; ?>">
                                                                <div class="input-group-btn">
                                                                    <button type="button" id="SET_FATE_FACT" class="btn btn-default btn-xs" data-popup="tooltip" title="Définir" data-placement="right"><i class="icon-file-check"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--div class="form-group text-center">
                                                            <hr>
                                            
                                                        </div-->
                                                        
                                                    </div>
                                                    <div class="panel-footer text-center">
                                                            <div class="heading-elements">
                                                            <?php echo $REG_INFO_PULL; ?>
                                                            </div>
                                                        </div>
                                                </div>
                                            </div>

                                             <!-- reglement modal -->
                                            <div id="modal_Reglement" class="modal fade">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h5 class="modal-title"><i class="icon-menu7"></i> &nbsp;Règlement Facture</h5>
                                                        </div>

                                                        <div class="modal-body">
                                                            <div class="tabbable nav-tabs-vertical nav-tabs-left">
                                                                <ul class="nav nav-tabs nav-tabs-highlight">
                                                                    <li class="active"><a href="#BC" data-toggle="tab"><i class="icon-file-text3 position-left"></i> BC</a></li>
                                                                    <li><a href="#ESPECE" data-toggle="tab"><i class="icon-cash position-left"></i> ESPECE</a></li>
                                                                    <li><a href="#CHEQUE" data-toggle="tab"><i class="icon-file-spreadsheet2 position-left"></i> CHEQUE</a></li>
                                                                    <li><a href="#TRAITE" data-toggle="tab"><i class="icon-price-tag position-left"></i> TRAITE</a></li>
                                                                    <li><a href="#VIREMENT" data-toggle="tab"><i class="icon-credit-card2 position-left"></i> VIREMENT BANCAIRE</a></li>
                                                                </ul>

                                                                <div class="tab-content">
                                                                    <div class="tab-pane active has-padding" id="BC">
                                                                        Pas Règlement.
                                                                    </div>

                                                                    <div class="tab-pane has-padding" id="ESPECE">
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_ESPECE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane has-padding" id="CHEQUE">
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_CHEQUE_Num" class="form-control input-xs text-bold text-center" placeholder="N° Cheque" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_CHEQUE_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_CHEQUE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane has-padding" id="TRAITE">
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_TRAITE_Num" class="form-control input-xs text-bold text-center" placeholder="Numéro" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_TRAITE_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_TRAITE_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                        </div>
                                                                    </div>

                                                                    <div class="tab-pane has-padding" id="VIREMENT">
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_VIREMENT_Num" class="form-control input-xs text-bold text-center" placeholder="N° de Transfert" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_VIREMENT_Banq" class="form-control input-xs text-bold text-center" placeholder="Banque" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input type="text" id="INP_F_VIREMENT_DATE" class="form-control input-xs text-bold text-center" placeholder="Date" value="">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal-footer">
                                                            <button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> Fermer</button>
                                                            <button class="btn btn-primary" id="REG_BTN"><i class="icon-check"></i> Effectuer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /reglement modal -->

                                            
                                            <div class="col-md-6">
                                                <div class="tabbable">
                                                    <ul class="nav nav-tabs nav-tabs-bottom nav-justified">
                                                        <li class="active"><a href="#bottom-justified-tab1" data-toggle="tab">Convert. devises en ligne</a></li>
                                                        <li><a href="#bottom-justified-tab2" data-toggle="tab">Convert. devises locale</a></li>
                                                    </ul>

                                                    <div class="tab-content">
                                                        <div class="tab-pane active" id="bottom-justified-tab1">
                                                            <div id="currency-widget"></div>
                                                        </div>

                                                        <div class="tab-pane" id="bottom-justified-tab2">
                                                            <div class="form-group">
                                                                <input type="text" id="INP_VAL_DEV" class="form-control input-xs text-bold text-center" placeholder="Valeur" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" id="INP_TAUX_DEV" class="form-control input-xs text-bold text-center" placeholder="Taux" value="">
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="CONV_L_DEV" class="btn btn-default btn-xs" data-popup="tooltip" title="Convert" data-placement="right"><i class="icon-chevron-right"></i></button>
                                                                    </div>
                                                                    <input type="text" id="INP_RES_DEV" class="form-control input-xs text-bold text-center" placeholder="Resultat" value="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-5">
                                        <div class="content-group">
                                            <h6>Total</h6>
                                            <div class="table-responsive no-border">
                                                <table class="table">
                                                    <tbody>
                                                        <tr>
                                                            <th>TOTAL NON TAXABLE:</th>
                                                            <td class="text-right" id="N_TAX"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TOTAL TAXABLE :</th>
                                                            <td class="text-right" id="T_TAX"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TVA: <span class="text-regular"><?php echo "(" . $GET_TVA . "%)"; ?></span></th>
                                                            <td class="text-right" id="T_TVA"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>TIMBRE :</th>
                                                            <td class="text-right"><?php echo $GET_TIMBRE . " TND"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Total TTC:</th>
                                                            <td class="text-right text-primary">
                                                                <h5 class="text-semibold" id="T_TT"></h5>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="panel panel-flat border-top-info border-bottom-info">
                                            <div class="panel-body">
                                                <ul>
                                                    <li><span id="T_TT_L" class="text-uppercase text-bold"></span></li>
                                                    <li><span class="text-uppercase text-bold">statut de règlement :</span>
                                                        <ul id="FACT_REG_info">
                                                            <?php echo $REGLE_STAT; ?>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button type="button" id="FACT_MAIL_BTN" class="btn bg-indigo btn-labeled disabled"><b><i class="icon-mention"></i></b> Envoyer la facture par E-Mail</button>
                                    <button type="button" id="FACT_PRINT_BTN" class="btn btn-success btn-labeled"><b><i class="icon-printer"></i></b> Facture</button>
                                </div>
                            </div>
                        </div>
                        <!-- /invoice template -->
                        <!-- Write mail modal -->
                        <div id="modal_writem" class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <!-- Mail toolbar -->
                                        <div class="panel-toolbar panel-toolbar-inbox">
                                            <div class="navbar navbar-default">
                                                <ul class="nav navbar-nav visible-xs-block no-border">
                                                    <li>
                                                        <a class="text-center collapsed" data-toggle="collapse" data-target="#inbox-toolbar-toggle-single">
                                                            <i class="icon-circle-down2"></i>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <div class="navbar-collapse collapse" id="inbox-toolbar-toggle-single">
                                                    <div class="btn-group navbar-btn">
                                                        <button type="button" class="btn bg-blue" id="SEND_MAIL_BTN"><i class="fa fa-send position-left"></i> Envoyer</button>
                                                    </div>

                                                    <div class="btn-group navbar-btn">
                                                        <button type="button" class="btn btn-default"><i class="icon-plus-circle2"></i> <span class="hidden-xs position-right">Enregistrer</span></button>
                                                        <button type="button" class="btn btn-default"><i class="icon-cancel-circle2"></i> <span class="hidden-xs position-right">Annuler</span></button>
                                                    </div>

                                                    <div class="pull-right-lg">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /mail toolbar -->

                                        <!-- Mail details -->
                                        <div class="table-responsive mail-details-write">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td style="width: 150px">À:</td>
                                                        <td class="no-padding"><input type="text" id="MAIL_DEST" name="MAIL_DEST" class="form-control tagsinput-typeahead" data-role="tagsinput" placeholder="Ajouter des destinataires" value="<?= $CL_EMAIL; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 150px">Sujet:</td>
                                                        <td class="no-padding"><input type="text" id="MAIL_SUBJECT" name="MAIL_SUBJECT" class="form-control" placeholder="Ajouter un sujet" value="FACTURE N° <?php echo $NUM_FACT; ?>"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <input id="upload" type="file" style="display:none" />
                                                            <ul class="list-inline no-margin">
                                                                <li><a href="" id="upload_link"><i class="icon-attachment position-left"></i> Joindre des fichiers</a></li>
                                                            </ul>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /mail details -->

                                        <!-- Mail container -->
                                        <div class="mail-container-write">
                                            <div class="summernote" id="MAIL_BODY">
                                            </div>
                                        </div>
                                        <!-- /mail container -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Write mail modal -->

                        <!-- Footer -->
                        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                        <!-- /footer -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>