<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {

    case 'GET_FACT_DOS':
        $db = new MySQL();
        $GET_FACT_DOS["data"] = $db->get_results("SELECT
                                                        AAM_CODE,
                                                        CL_LIBELLE,
                                                        CL_RSPONSABLE,
                                                        CL_TEL_ETABLISS,
                                                        CL_FAX,
                                                        CL_EMAIL,
                                                        AAM_NUM_FACTURE,
                                                        DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                        FT_LIBELLE,
                                                        AAM_BON_COMMAND,
                                                        AAM_TOTAL_TTC 
                                                    FROM
                                                        avis_arrive_mig
                                                        INNER JOIN client ON CL_CODE = AAM_CODE_CLIENT
                                                        INNER JOIN type_facture ON FT_CODE = AAM_TYPE_FACT
                                                    WHERE
                                                        AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_FACT_DOS);
        break;

    case 'GET_INV_DOS':
        $db = new MySQL();
        $GET_INV_DOS["data"] = $db->get_results("SELECT
                                                    AAM_CODE,
                                                    AAM_NUM_FACTURE,
                                                    DATE_FORMAT( AAM_DATE_FACTURE, '%d/%m/%Y' ) AS AAM_DATE_FACTURE,
                                                IF
                                                    ( ( AAM_NUM_REGLEMENT IS NULL OR AAM_NUM_REGLEMENT = '' ), 'BC', 'Virement' ) AS BCMD,
                                                    AAM_TOTAL_TTC,
                                                    AAM_DEVISE,
                                                    AAM_DES_CLIENT 
                                                FROM
                                                    invoice 
                                                WHERE
                                                    AAM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_INV_DOS);
        break;

    case 'GET_DOS_DATA':
        $db = new MySQL();
        $GET_DOS_DATA = $db->get_results("SELECT * FROM dossier_maritime WHERE DM_CLE = '$response[1]'");
        echo json_encode($GET_DOS_DATA);
        break;

    case 'GET_DOS_TAB_MAR':
        $db = new MySQL();
        $GET_DOS_TAB['data'] = $db->get_results("SELECT
                                                    DM_CLE,
                                                    DM_NUM_BL AS NUM_BL_LTA,
                                                    CL_LIBELLE,
                                                    p.PO_LIBELLE AS POD,
                                                    o.PO_LIBELLE AS POL,
                                                    DATE_FORMAT( DM_DATE_EMBARQ, '%d/%m/%Y' ) AS DM_DATE_EMBARQ,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN client on DM_CLIENT = CL_CODE
                                                    INNER JOIN PORT p ON DM_POD = p.PO_CODE
                                                    INNER JOIN PORT o ON DM_POL = o.PO_CODE 
                                                WHERE
                                                    DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_DOS_TAB);
        break;

    case 'GET_DOS_TAB_AER':
        $db = new MySQL();
        $GET_DOS_TAB['data'] = $db->get_results("SELECT
                                                    DM_CLE,
                                                    DM_NUM_LTA AS NUM_BL_LTA,
                                                    CL_LIBELLE,
                                                    p.AE_LIBELLE AS POD,
                                                    o.AE_LIBELLE AS POL,
                                                    DATE_FORMAT( DM_DATE_EMBARQ, '%d/%m/%Y' ) AS DM_DATE_EMBARQ,
                                                    DATE_FORMAT( DM_DATE_DECHARG, '%d/%m/%Y' ) AS DM_DATE_DECHARG 
                                                FROM
                                                    trans.dossier_maritime
                                                    INNER JOIN client ON DM_CLIENT = CL_CODE
                                                    INNER JOIN aeroport p ON DM_POD = p.AE_CODE
                                                    INNER JOIN aeroport o ON DM_POL = o.AE_CODE 
                                                WHERE
                                                    DM_NUM_DOSSIER = '$response[1]'");
        echo json_encode($GET_DOS_TAB);
        break;

    case 'SET_NEW_FACT':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(AAM_CODE) + 1 as CPT FROM avis_arrive_mig")[0]['CPT'];
        $response[1]['AAM_CODE'] = $key;
        $res = $db->insert( 'avis_arrive_mig', $response[1]);
        if($res == 1){
            $NUM = explode("/", $response[2])[1];
            $user_data = array('NUMERO' => $NUM);
            $db->insert( 'compteur_facture', $user_data );
        }else {
            echo "error";
        }
        echo json_encode(array('AAM_CODE' => $key ));
        break;

    case 'GET_TIMBRE':
        $db = new MySQL();
        $GET_TIMBRE = $db->get_results("SELECT VALEUR FROM table_pourcentage WHERE CODE = '4'");
        echo json_encode($GET_TIMBRE);
        break;

    case 'GET_AA_TAB_DATA':
        $db = new MySQL();
        $GET_AA_TAB_DATA["data"] = $db->get_results("SELECT * FROM avis_arrive_mig_suite WHERE AAM_CODE_MIG = '$response[1]' ORDER BY AAM_CODE_CONTENU ASC");
        echo json_encode($GET_AA_TAB_DATA);
        break;

    case 'GET_TT_AA':
        $db = new MySQL();
        $tt = $db->get_results("SELECT
                                    AAM_TOT_NON_TAXABLE AS NTAX,
                                    AAM_TO_TAXABLE AS TAX,
                                    AAM_TVA_PORCENTAGE AS TVA,
                                    AAM_TVA_VAL AS TVA_VAL,
                                    AAM_TIMBRE AS TIMBER_VAL,
                                    AAM_TOTAL_TTC AS TT 
                                FROM
                                     avis_arrive_mig
                                WHERE
                                    AAM_CODE = '$response[1]'");
        echo json_encode($tt);
        break;

    case 'GET_TT_AA_OLD': //NOT USED
        $db = new MySQL();
        $tt = $db->get_results("SELECT COALESCE
                                    ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) AS NTAX,
                                    COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) AS TAX,
                                    ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) AS TVA, 
                                    ( COALESCE ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) + COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) + ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) + ( z.VALEUR ) ) AS TT,
                                    t.VALEUR AS TVA_VAL,
                                    z.VALEUR AS TIMBER_VAL 
                                FROM
                                    avis_arrive_mig_suite,
                                    table_pourcentage t,
                                    table_pourcentage z 
                                WHERE
                                    AAM_CODE_MIG = '$response[1]'
                                    AND t.CODE = 3 
                                    AND z.CODE = 4");
        echo json_encode($tt);
        break;

    case 'ADD_To_AA':
        $db = new MySQL();
        $key = $db->get_results("SELECT MAX(AAM_CODE_SUIT) + 1 as CPT FROM avis_arrive_mig_suite")[0]['CPT'];
        $response[1]['AAM_CODE_SUIT'] = $key;
        echo $db->insert( 'avis_arrive_mig_suite', $response[1]);
        break;

    case 'DEL_From_AA':
        $db = new MySQL();
        $Del_where = array('AAM_CODE_SUIT' => $response[1]);
        echo $db->delete( 'avis_arrive_mig_suite', $Del_where, 1 );
        break;

    case 'UP_AA_SUM':
        $db = new MySQL();
        $tt = $db->get_results("SELECT COALESCE
                                    ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) AS NTAX,
                                    COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) AS TAX,
                                    ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) AS TVA, 
                                    ( COALESCE ( SUM( AAM_VAL_NON_TAXABLE ), 0 ) + COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) + ( ( COALESCE ( SUM( AAM_VAL_TAXABLE ), 0 ) * t.VALEUR ) / 100 ) + ( z.VALEUR ) ) AS TT,
                                    t.VALEUR AS TVA_VAL,
                                    z.VALEUR AS TIMBER_VAL 
                                FROM
                                    avis_arrive_mig_suite,
                                    table_pourcentage t,
                                    table_pourcentage z 
                                WHERE
                                    AAM_CODE_MIG = '$response[1]'
                                    AND t.CODE = 3 
                                    AND z.CODE = 4");
        $update_AA = array( 'AAM_TOT_NON_TAXABLE' => $tt[0]["NTAX"], 
                            'AAM_TO_TAXABLE' => $tt[0]["TAX"], 
                            'AAM_TVA_PORCENTAGE' => $tt[0]["TVA"],
                            'AAM_TVA_VAL' => $tt[0]["TVA_VAL"],
                            'AAM_TIMBRE' => $tt[0]["TIMBER_VAL"],
                            'AAM_TOTAL_TTC' => $tt[0]["TT"],
                            'AA_TTC_LETTRE' => $response[2]
                        );
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        break;

    case 'SET_TT_AA_LET': //NOT COMPLETED
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        break;

    case 'SERACH_NUM_AA':
        $db = new MySQL();
        $SERACH_NUM_AA = $db->get_results("SELECT AAM_CODE_DOSSIER,AAM_G_C FROM avis_arrive_mig WHERE AAM_NUMERO = '$response[1]'");
        echo json_encode($SERACH_NUM_AA);
        break;

    case 'GET_CMPT_FACT':
        $db = new MySQL();
        $GET_CMPT_FACT = $db->get_results("SELECT MAX(NUMERO) + 1 as CPT FROM compteur_facture")[0]['CPT'];
        $res = '{"CPT" : "'.date("Y").'/'.sprintf( "%04d", $GET_CMPT_FACT).'"}';
        echo json_encode($res);
        break;

    case 'SET_NUM_FACT':
        $db = new MySQL();
        $update_AA = array( 'AAM_NUM_FACTURE' => $response[2]);
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        $res = $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        if($res == 1){
            $NUM = explode("/", $response[2])[1];
            $user_data = array('NUMERO' => $NUM);
            echo $db->insert( 'compteur_facture', $user_data );
        }else {
            echo "error";
        }
        break;

    case 'SERACH_NUM_FACT':
        $db = new MySQL();
        $SERACH_NUM_FACT = $db->get_results("SELECT AAM_CODE_DOSSIER,AAM_G_C FROM avis_arrive_mig WHERE AAM_NUM_FACTURE = '$response[1]'");
        echo json_encode($SERACH_NUM_FACT);
        break;

    case 'MOD_CLIENT_AA':
        $db = new MySQL();
        $CL_CODE = $response[1];
        $CL_LIBEL = $response[2];
        $DM_CLE = $response[3];
        $CL_ADDRESS = $db->get_results("SELECT CL_ADRESSE from trans.client WHERE CL_CODE = '$CL_CODE'")[0]['CL_ADRESSE'];
        //MODIF DOSSIER
        $update_DM = array( 'DM_CLIENT' => $CL_CODE);
        $update_where_DM = array( 'DM_CLE' => $DM_CLE);
        $res1 = $db->update( 'dossier_maritime', $update_DM, $update_where_DM, 1 );
        //MODIF DOSSIER
        $update_AA = array( 'AAM_CODE_CLIENT' => $CL_CODE, 'AAM_DES_CLIENT' => $CL_LIBEL, 'AAM_ADR_CLIENT' => $CL_ADDRESS);
        $update_where_AA= array( 'AAM_CODE_DOSSIER' => $DM_CLE);
        $res2 = $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );

        echo json_encode('{"RES_DM" : "'.$res1.'", "RES_AA" : "'.$res2.'"}');
        break;

    case 'UP_AA_REGLEMENT':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        break;

    case 'SET_DATE_FACT':
        $db = new MySQL();
        $update_where_AA= array( 'AAM_CODE' => $response[2]);
        echo $db->update( 'avis_arrive_mig', $response[1], $update_where_AA, 1 );
        break;

    case 'GET_DATE_TIME':
        $date = date('Y-m-d');
        $time = date('H:i');
        $res = array('date' => $date, 'time' =>$time);
        echo json_encode($res);
        break;

    case 'GET_TYPEHEAD_CLIENT_MAIL':
        $db = new MySQL();
        $res = $db->get_results("SELECT CL_EMAIL FROM client WHERE CL_EMAIL <> '' AND CL_EMAIL IS NOT NULL");
        $out = [];
        foreach ($res as $key => $value) {
           array_push($out, $value['CL_EMAIL']);
        }
        echo json_encode($out);
        break;

    case 'CONF_DEL_PASS':
        $db = new MySQL();
        $key = $db->get_results("SELECT PASS FROM CONF_TAB where id = 'MGOD'")[0]['PASS'];
        if ( $key == $response[1]) {
            $where = array( 'AAM_CODE' => $response[2]);
            echo $db->delete( 'avis_arrive_mig', $where );
            $where = array( 'AAM_CODE_MIG' => $response[2]);
            echo $db->delete( 'avis_arrive_mig_suite', $where );
        }else{
            echo 'Wrong';
        }
        break;

    case 'CONF_DEL_PASS_INV':
        $db = new MySQL();
        $key = $db->get_results("SELECT PASS FROM CONF_TAB where id = 'MGOD'")[0]['PASS'];
        if ( $key == $response[1]) {
            $where = array( 'AAM_CODE' => $response[2]);
            echo $db->delete( 'invoice', $where );
            $where = array( 'AAM_CODE_MIG' => $response[2]);
            echo $db->delete( 'invoice_suite', $where );
        }else{
            echo 'Wrong';
        }
        break;

    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>