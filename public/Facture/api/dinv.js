/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

$(document).ready(function () {
    //$('#currency-widget').currency();
    console.log(AAM_CODE)

    $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function (data) {
        console.log('fail', data);
    }).done(function (data) {
        $('#clients_PC_MIG').append('<option value="">-</option>');
        data.forEach(function (element) {
            $('#LIST_CLIENT_AA').append('<option value="CL_' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    }).always(function () {
        $.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['client_etranger'])).fail(function (data) {
            console.log('fail', data);
        }).done(function (data) {
            data.forEach(function (element) {
                $('#LIST_CLIENT_AA').append('<option value="' + element.CLE_CODE + '">' + element.CLE_LIBELLE + '</option>');
            }, this);
        }).always(function () {
            $(".livesearch").chosen({
                width: '100%',
                enable_split_word_search: false
            });
        });
    });

    function toDate(dateStr) {
        if (dateStr) {
            const [day, month, year] = dateStr.split("/");
            return [year, month, day].join('/');
        } else {
            return '';
        }
    }

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }
    // Floating labels
    // ------------------------------

    // Variables
    var onClass = "on";
    var showClass = "is-visible";


    // Setup
    $("input:not(.token-input):not(.bootstrap-tagsinput > input), textarea, select").on("checkval change", function () {

        // Define label
        var label = $(this).parents('.form-group-material').children(".control-label");

        // Toggle label
        if (this.value !== "") {
            label.addClass(showClass);
        } else {
            label.removeClass(showClass).addClass('animate');
        }

    }).on("keyup", function () {
        $(this).trigger("checkval");
    }).trigger("checkval").trigger('change');


    // Remove animation on page load
    $(window).on('load', function () {
        $(".form-group-material").children('.control-label.is-visible').removeClass('animate');
    });
    // ------------------------------

    /*$.post('/GTRANS/public/Dossier/api/dossier.php', JSON.stringify(['clients'])).fail(function(data) {
        console.log('fail' + data);
    }).done(function(data) {
        data.forEach(function(element) {
            $('#LIST_CLIENT_AA').append('<option value="' + element.CL_CODE + '">' + element.CL_LIBELLE + '</option>');
        }, this);
    }).always(function() {
        try {
            $('#LIST_CLIENT_AA').val(CL_CODE)
        } catch (error) {

        }
    });

    $('#MOD_CLIENT_AA').on('click', function() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['MOD_CLIENT_AA', $('#LIST_CLIENT_AA').val(), $('#LIST_CLIENT_AA option:selected').text(), DM_CLE])).done(function(data) {
            try {
                data = JSON.parse(data);
                //console.log(data);
                if (data.RES_DM === '1' && data.RES_AA === '1') {
                    location.reload();
                } else {
                    PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
                }
            } catch (error) {
                PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
            }
        });
    });*/

    //GET PARAMETER
    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_CONT_FACT_INV'])).done(function (data) {
        data.forEach(function (element) {
            $('#DESC_AA').append('<option value="' + element.LIBELLE + '">' + element.CODE + '</option>');
        }, this);
    });


    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        //dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        dom: '<"datatable-header"><"datatable-scroll"t><"datatable-footer">',
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: "<i class='icon-cabinet'></i> Aucune donnée disponible",
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "<i class='icon-spinner2 spinner'></i> Chargement en cours",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        /*drawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }*/
    });

    // Basic datatable
    var table = $('#AA_TAB').DataTable({
        select: {
            style: 'single'
        },
        destroy: true,
        scrollY: false,
        scrollX: false,
        scrollCollapse: true,
        paging: false,
        ajax: {
            url: "/GTRANS/public/Dossier/api/fact.php",
            type: "POST",
            data: function (d) {
                return JSON.stringify({
                    "0": "GET_AA_TAB_DATA_INV",
                    "1": AAM_CODE
                });
            }
        },
        aoColumnDefs: [{
            "sClass": "hide_me",
            "aTargets": [0]
        }],
        columns: [{
                "data": "AAM_CODE_SUIT"
            },
            {
                "data": "AAM_LIB_CONTENU"
            },
            {
                "data": "AMOUNT",
                "render": function (data, type, row) {
                    if (data == '0.000') {
                        return '';
                    } else {
                        return data;
                    }
                }
            }
        ],
        drawCallback: function () {

        },
        fnDrawCallback: function (oSettings) {
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_TT_INV', AAM_CODE])).done(function (data) {
                $('#inp_dev').val(data[0].AAM_DEVISE);
                $('#T_TT').html(Number(data[0].TT).toFixed(3));
                $('#TT_INFO').html(Number(data[0].TT).toFixed(3) + ' ' + data[0].AAM_DEVISE);
            });
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_TVD_INV', AAM_CODE])).done(function (data) {
                $('#inp_vd').val(data[0].TVD_VALEUR);
            });
        }
    });

    $('#FACT_PRINT_BTN').on('click', function () {
        if (DM_CODE_COMP_GROUP == 'C') {
            var win = window.open('/GTRANS/public/Print/viewer.php?id=CINV&A=' + DM_CLE + '&B=' + AAM_CODE + '&C=' + DM_NUM_DOSSIER, '_blank');
            win.focus();
            /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=CINV.mrt&A=' + DM_CLE + ' &B=' + AAM_CODE + ' &C=' + DM_NUM_DOSSIER, '_blank');
            win.focus();*/
        } else {
            var win = window.open('/GTRANS/public/Print/viewer.php?id=INV&A=' + DM_CLE + '&B=' + AAM_CODE, '_blank');
            win.focus();
            /*var win = window.open('/GTRANS/public/FLEX/stimulsoft/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=INV.mrt&A=' + DM_CLE + ' &B=' + AAM_CODE, '_blank');
            win.focus();*/
        }
    });


    function IsNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    $('#ADD_AA').on('click', function () {
        if (IsNumeric($('#Amount').val())) {
            if ($('#AA_TAB tr > td:contains(' + $('#DESC_AA_N').val() + ')').length == 0) {
                $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_To_INV', {
                    'AAM_CODE_MIG': AAM_CODE,
                    'AAM_CODE_CONTENU': '', //$('#DESC_AA').val(),
                    'AAM_LIB_CONTENU': $('#DESC_AA_N').val(),
                    'AMOUNT': $('#Amount').val()
                }])).done(function (data) {
                    update_aa_sum();
                    $('#Amount').val('');
                });
            } else {
                PNotify_Alert('Redondance', $('#DESC_AA_N').val() + ' : déjà calculé !', 'warning');
            }
        }
    });

    $('#DEL_AA').on('click', function () {
        if (table.rows('.selected').any()) {
            $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['DEL_From_INV', table.rows('.selected').data()[0].AAM_CODE_SUIT]))
                .fail(function (data) {
                    console.log('fail');
                    console.dir(data);
                    PNotify_Alert('Erreur ', 'Une erreur s\'est produite lors du traitement de suppression', 'warning');
                }).done(function (data) {
                    update_aa_sum();
                });
        } else {
            PNotify_Alert('Erreur ', 'Aucune donnée sélectionnée dans le tableau', 'danger');
            table.ajax.reload();
        }
    });


    $('#NEW_NFACT').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_CMPT_FACT']))
            .done(function (data) {
                data = JSON.parse(data);
                $('#INP_NFACT').val(data.CPT);
            });
    });

    $('#NEW_NFACT_SET').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_NUM_FACT_INV', AAM_CODE, $('#INP_NFACT').val()]))
            .done(function (data) {
                location.reload();
            });
    });
    /*$('#SET_CAA').on('click', function() {
        if ($('#INP_CAA').val()) {
            swal({
                    title: "Changement de compteur avis d'arrivér",
                    text: "Après confirmation, le compteur sera : " + $('#INP_CAA').val(),
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    confirmButtonColor: "#1565C0",
                    confirmButtonText: "Confirmer",
                    cancelButtonText: "Annuler",
                    showLoaderOnConfirm: true
                },
                function() {
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_CPT_AA', (Number($('#INP_CAA').val()) - 1)]))
                        .done(function(data) {
                            swal({
                                title: "Le compteur est changé avec succès",
                                type: "success",
                                confirmButtonColor: "#2E7D32",
                                confirmButtonText: "Fermer",
                            });
                            $('#SP_CAA_INFO').html(Number(data.NCPT) + 1);
                        }).fail(function(data) {
                            swal({
                                title: "Erreur de changement de compteur",
                                type: "error",
                                confirmButtonColor: "#C62828",
                                confirmButtonText: "Fermer",
                            });
                        });
                });
        }
    });*/

    function update_aa_sum() {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_AA_SUM_INV', AAM_CODE, $('#TDEVISE').val()]))
            .fail(function (data) {
                PNotify_Alert('Avis D\'ariivée', 'Problème mis a jours somme !', 'danger');
            }).done(function (data) {
                table.ajax.reload();
            });
    }

    $('#SEARCH_NFACT').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SERACH_NUM_FACT', $('#INP_NFACT').val()]))
            .done(function (data) {
                if (Object.keys(data).length) {
                    switch (data[0].AAM_G_C) {
                        case 'G':
                            var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + data[0].AAM_CODE_DOSSIER, '_blank');
                            win.focus();
                            break;

                        case 'C':
                            console.log('C :' + data[0].AAM_CODE_DOSSIER);
                            break;

                        default:
                            console.log('A :' + data[0].AAM_CODE_DOSSIER);
                            break;
                    }
                } else {
                    PNotify_Alert('Avis D\'ariivée', 'Avis d\'arrivée non trouvée !', 'warning');
                }
            }).fail(function () { //delay: 2000,
                PNotify_Alert('Avis D\'arrivée', 'Avis d\'arrivée non trouvée !', 'warning');
            });
    });

    $('#GET_FACT_DATE').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['GET_DATE_TIME']))
            .done(function (data) {
                $('#INP_DATEFACT').val(data.date);
            }).fail(function (data) {
                console.log(data);
            });
    });

    $('#SET_FATE_FACT').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['SET_DATE_FACT_INV', {
                'AAM_DATE_FACTURE': $('#INP_DATEFACT').val()
            }, AAM_CODE]))
            .done(function (data) {
                location.reload();
            }).fail(function (data) {
                PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
            });
    });

    $('#REG_BTN').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_INV_REGLEMENT', {
                'AAM_NUM_REGLEMENT': $('#INP_F_VIREMENT_Num').val(),
                'AAM_DATE_REGLEMENT': $('#INP_F_VIREMENT_DATE').val()
            }, AAM_CODE]))
            .fail(function (data) {
                PNotify_Alert('Règlement Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
            }).done(function (data) {
                //console.log(data)
                location.reload();
            });
    });

    $('#btn_dev').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['UP_INV_REGLEMENT', {
                'AAM_DEVISE': $('#inp_dev').val()
            }, AAM_CODE]))
            .fail(function (data) {
                PNotify_Alert('MAJ Devise', 'Une erreur s\'est produite lors du traitement', 'warning');
            }).done(function (data) {
                //console.log(data)
                location.reload();
            });
    });

    $('#btn_vd').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['ADD_INV_TVD', {
                'TVD_CLE_INVOICE': AAM_CODE,
                'TVD_NUM_DOSSIER': DM_NUM_DOSSIER,
                'TVD_VALEUR': $('#inp_vd').val()
            }]))
            .fail(function (data) {
                PNotify_Alert('MAJ Valeur en Dinars', 'Une erreur s\'est produite lors du traitement', 'warning');
            }).done(function (data) {
                //console.log(data)
                location.reload();
            });
    });

    $('#MOD_CLIENT_AA').on('click', function () {
        $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['MOD_CLIENT_INV', $('#LIST_CLIENT_AA').val(), $('#LIST_CLIENT_AA option:selected').text(), DM_CLE])).done(function (data) {
            try {
                data = JSON.parse(data);
                //console.log(data);
                if (data.RES_AA === '1') {
                    location.reload();
                } else {
                    PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
                }
            } catch (error) {
                PNotify_Alert('Une erreur s\'est produite', 'Une erreur s\'est produite lors du traitement', 'danger');
            }
        });
    });

    //Delete Avis+Fact

    $('#Del_AA_BTN').on('click', function () {
        swal({
                title: "Êtes-vous sûr?",
                text: "Vous ne serez pas en mesure de récupérer cette facture!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: "Oui, supprimez-le!",
                cancelButtonText: "Non, annulez!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post('/GTRANS/public/Dossier/api/fact.php', JSON.stringify(['DEL_INV_FACT', AAM_CODE]))
                        .fail(function (data) {
                            PNotify_Alert('Suppression Facture', 'Une erreur s\'est produite lors du traitement', 'warning');
                        }).done(function (data) {
                            swal({
                                title: "Supprimé!",
                                text: "Votre facture a été supprimée. cet onglet se fermera automatiquement (2s)",
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                timer: 2000
                            }, function () {
                                window.close();
                            });
                        });
                } else {
                    swal({
                        title: "Annulé!",
                        text: "Votre facture est en sécurité :)",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            });
    });

});