<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {

    case 'GET_TYPE_FACT':
        $db = new MySQL();
        $GET_TYPE_FACT = $db->get_results("SELECT
                                                FT_CODE,FT_LIBELLE
                                            FROM
                                                type_facture
                                                WHERE
                                                FT_CODE NOT IN ('T','M','S','A')");
        echo json_encode($GET_TYPE_FACT);
        break;

    case 'SET_TYPE_FACT':
        $db = new MySQL();
        $update_AA = array( 
            'AAM_TYPE_FACT' => $response[2], 
            'AAM_DES_CLIENT' => $response[3], 
            'AAM_ADR_CLIENT' => $response[4],
            'AAM_FOURNISSEUR' => $response[5],
            'AAM_PORT_EMBARQ' => $response[6],
            'AAM_PORT_DECHAG' => $response[7],
            'AAM_NAVIRE' => $response[8]
        );
        $update_where_AA= array( 'AAM_CODE' => $response[1]);
        echo $db->update( 'avis_arrive_mig', $update_AA, $update_where_AA, 1 );
        break;


    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>