/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

$(document).ready(function () {

    function toDate(dateStr) {
        if (dateStr) {
            const [day, month, year] = dateStr.split("/");
            return [year, month, day].join('/');
        } else {
            return '';
        }
    }

    function PNotify_Alert(title, text, type) {
        new PNotify({
            title: title,
            text: text,
            addclass: 'stack-bottom-right bg-' + type,
            icon: 'icon-cancel-circle2',
            delay: 4000,
            stack: {
                "dir1": "up",
                "dir2": "left",
                "firstpos1": 25,
                "firstpos2": 25
            },
        });
    }
    // Floating labels
    // ------------------------------

    // Variables
    var onClass = "on";
    var showClass = "is-visible";


    // Setup
    $("input:not(.token-input):not(.bootstrap-tagsinput > input), textarea, select").on("checkval change", function () {

        // Define label
        var label = $(this).parents('.form-group-material').children(".control-label");

        // Toggle label
        if (this.value !== "") {
            label.addClass(showClass);
        } else {
            label.removeClass(showClass).addClass('animate');
        }

    }).on("keyup", function () {
        $(this).trigger("checkval");
    }).trigger("checkval").trigger('change');


    // Remove animation on page load
    $(window).on('load', function () {
        $(".form-group-material").children('.control-label.is-visible').removeClass('animate');
    });
    // ------------------------------


    $('#open_dos').on('click', function () {
        //'/GTRANS/public/Dossier/?NumD='+$('#DOS_NUM').val()
        if ($('#DOS_NUM').val()) {
            var win = window.open('/GTRANS/public/Dossier/?NumD=' + $('#DOS_NUM').val(), '_blank');
            win.focus();
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#NEW_NFACT').on('click', function () {
        $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['GET_CMPT_FACT']))
            .done(function (data) {
                data = JSON.parse(data);
                $('#INP_NFACT').val(data.CPT);
            });
    });

    $('#GET_FACT_DATE').on('click', function () {
        $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['GET_DATE_TIME']))
            .done(function (data) {
                $('#INP_DATEFACT').val(data.date);
            })
            .fail(function (data) {
                console.log(data);
            });
    });

    $.extend($.fn.dataTable.defaults, {
        select: true,
        destroy: true,
        responsive: true,
        aaSorting: [],
        paging: false,
        autoWidth: true,
        columnDefs: [{
            orderable: false,
            width: '100%'
        }],
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        buttons: [{
            text: '<i class="icon-sync"></i>',
            className: 'btn btn-default border-primary text-primary',
            action: function (e, dt, node, config) {
                if ($('#DOS_NUM').val()) {
                    try {
                        dt.ajax.reload();
                    } catch (error) {
                        console.log('error : ', error)
                    }
                } else {
                    PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
                }
            }
        }, {
            text: '<i class="icon-printer"></i>',
            extend: 'print',
            footer: true,
            className: 'btn btn-default border-primary text-primary',
            text: '<i class="icon-printer position-left"></i>',
            title: 'Liste des Factures',
            message: 'Numéro Dossier : ' + $('#DOS_NUM').val(),
            exportOptions: {
                columns: ':visible'
            },
            customize: function (win) {
                // Style the body..
                $(win.document.body)
                    .addClass('asset-print-body')
                    .css({
                        'text-align': 'center'
                    });

                /* Style for the table */
                $(win.document.body)
                    .find('table')
                    .addClass('compact minimalistBlack')
                    .css({
                        margin: '5px 5px auto'
                    });
            },
        }, {
            extend: 'collection',
            text: '<i class="icon-three-bars"></i>',
            buttons: [{
                extend: 'columnsToggle',
                columns: ':not([data-visible="false"])'
            }],
            className: 'btn btn-default border-primary text-primary'
        }],
        fade: true,
        language: {
            search: '<span>Filtre:</span> _INPUT_',
            searchPlaceholder: '...',
            lengthMenu: '<span>Affichage:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            sEmptyTable: '<i class="icon-file-empty"></i> Aucune donnée disponible',
            sInfo: "Affichage de _START_ à _END_ de _TOTAL_ entrées",
            sInfoEmpty: "Affichage de 0 à 0 de 0 entrées",
            sInfoFiltered: "(filtrer de _MAX_ totale entrées)",
            sInfoPostFix: "",
            sDecimal: "",
            sThousands: ",",
            sLengthMenu: "Show _MENU_ entries",
            sLoadingRecords: "Chargement...",
            sProcessing: "Progression...",
            sSearch: "Chercher:",
            sSearchPlaceholder: "",
            sUrl: "",
            sZeroRecords: "No matching records found"
        },
        /*drawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }*/
    });

    var TAB_MIG_FACT = $('#TAB_MIG_FACT').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: '<i class="icon-file-empty"></i> Aucune donnée disponible',
        }
    });

    var TAB_MIG_INV = $('#TAB_MIG_INV').DataTable({
        destroy: true,
        responsive: true,
        autoWidth: true,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            sEmptyTable: '<i class="icon-file-empty"></i> Aucune donnée disponible',
        }
    });

    var TAB_DOS = $('#TAB_DOS').DataTable({
        dom: '<"datatable-header"><"datatable-scroll-wrap"t><"datatable-footer">',
        select: {
            style: 'single'
        },
        destroy: true,
    });

    var TYP_DOS = 'E';
    $('#get_dos').on('click', function () {
        if ($('#DOS_NUM').val()) {
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM').val()]), function (data) {
                if (Object.keys(data).length) {
                    switch (data[0].DM_CODE_COMP_GROUP) {
                        case null: //Case Aerian
                        case '':
                            $('#DOS_TYPE').empty().text('Aèrien ' + data[0].DM_IMP_EXP);
                            TYP_DOS = 'A';
                            break;

                        case 'G':
                            $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' GROUPAGE');
                            TYP_DOS = 'G';
                            break;

                        case 'C':
                            $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' COMPLET');
                            TYP_DOS = 'C';
                            break;
                        default:
                            PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                            TYP_DOS = 'E';
                            break;
                    }
                } else {
                    PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                }
            }).always(function (data) {

                TAB_MIG_FACT = $('#TAB_MIG_FACT').DataTable({
                    aaSorting: [],
                    buttons: [{
                        text: '<i class="icon-sync"></i>',
                        className: 'btn btn-default border-primary text-primary',
                        action: function (e, dt, node, config) {
                            if ($('#DOS_NUM').val()) {
                                try {
                                    dt.ajax.reload();
                                } catch (error) {
                                    console.log('error : ', error)
                                }
                            } else {
                                PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
                            }
                        }
                    }, {
                        text: '<i class="icon-printer"></i>',
                        extend: 'print',
                        footer: true,
                        className: 'btn btn-default border-primary text-primary',
                        text: '<i class="icon-printer position-left"></i>',
                        title: 'Liste des Factures',
                        message: 'Num Dossier : ' + $('#DOS_NUM').val(),
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function (win) {
                            // Style the body..
                            $(win.document.body)
                                .addClass('asset-print-body')
                                .css({
                                    'text-align': 'center'
                                });

                            /* Style for the table */
                            $(win.document.body)
                                .find('table')
                                .addClass('compact minimalistBlack')
                                .css({
                                    margin: '5px 5px auto'
                                });
                        },
                    }, {
                        extend: 'collection',
                        text: '<i class="icon-three-bars"></i>',
                        buttons: [{
                            extend: 'columnsToggle',
                            columns: ':not([data-visible="false"])'
                        }],
                        className: 'btn btn-default border-primary text-primary'
                    }],
                    bStateSave: true,
                    bDestroy: true,
                    select: false,
                    destroy: true,
                    ajax: {
                        url: "/GTRANS/public/Facture/api/ind.php",
                        type: "POST",
                        data: function (d) {
                            return JSON.stringify({
                                "0": "GET_FACT_DOS",
                                "1": $('#DOS_NUM').val()
                            });
                        },
                    },
                    aoColumnDefs: [{
                        "sClass": "hide_me",
                        "aTargets": [0]
                    }],
                    columns: [{
                            "data": "AAM_CODE"
                        },
                        {
                            "data": "AAM_NUM_FACTURE",
                            "render": function (data, type, row) {
                                return '<a id="SHOWFACT">' + row.AAM_NUM_FACTURE + '</a>';
                            }
                        },
                        {
                            "data": "AAM_DATE_FACTURE"
                        },
                        {
                            "data": "FT_LIBELLE",
                            "render": function (data, type, row) {
                                switch (row.FT_LIBELLE) {
                                    case 'Timbrage':
                                        return '<span class="label bg-danger-600">' + row.FT_LIBELLE + '</span>';
                                    case 'Magasinage':
                                        return '<span class="label bg-info-600">' + row.FT_LIBELLE + '</span>';
                                    case 'Surestarie':
                                        return '<span class="label bg-brown-600">' + row.FT_LIBELLE + '</span>';
                                    case 'Aérien':
                                        return '<span class="label bg-indigo-600">' + row.FT_LIBELLE + '</span>';
                                    case 'Avoirs':
                                        return '<span class="label bg-warning-600">' + row.FT_LIBELLE + '</span>';
                                    default:
                                        return '<span class="label bg-slate-600">' + row.FT_LIBELLE + '</span>';
                                }
                            }
                        },
                        {
                            "data": "AAM_BON_COMMAND"
                        },
                        {
                            "data": "AAM_TOTAL_TTC"
                        },
                        {
                            "data": "CL_LIBELLE"
                        }, {
                            "data": null,
                            "defaultContent": '<a id="delbtn" class="btn btn-link text-danger"><i class="icon-trash"></i></a>'
                        }
                    ],
                    initComplete: function (settings, json) {
                        var iii = TAB_MIG_FACT.columns(5)
                            .data().eq(0);
                        var jjj = TAB_MIG_FACT.columns(2)
                            .data().eq(0);
                    }
                }).columns.adjust().draw();

                TAB_MIG_INV = $('#TAB_MIG_INV').DataTable({
                    aaSorting: [],
                    buttons: [{
                        text: '<i class="icon-sync"></i>',
                        className: 'btn btn-default border-primary text-primary',
                        action: function (e, dt, node, config) {
                            if ($('#DOS_NUM').val()) {
                                try {
                                    dt.ajax.reload();
                                } catch (error) {
                                    console.log('error : ', error)
                                }
                            } else {
                                PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
                            }
                        }
                    }, {
                        text: '<i class="icon-printer"></i>',
                        extend: 'print',
                        footer: true,
                        className: 'btn btn-default border-primary text-primary',
                        text: '<i class="icon-printer position-left"></i>',
                        title: 'Liste des "Invoices"',
                        message: 'Num Dossier : ' + $('#DOS_NUM').val(),
                        exportOptions: {
                            columns: ':visible'
                        },
                        customize: function (win) {
                            // Style the body..
                            $(win.document.body)
                                .addClass('asset-print-body')
                                .css({
                                    'text-align': 'center'
                                });

                            /* Style for the table */
                            $(win.document.body)
                                .find('table')
                                .addClass('compact minimalistBlack')
                                .css({
                                    margin: '5px 5px auto'
                                });
                        },
                    }, {
                        extend: 'collection',
                        text: '<i class="icon-three-bars"></i>',
                        buttons: [{
                            extend: 'columnsToggle',
                            columns: ':not([data-visible="false"])'
                        }],
                        className: 'btn btn-default border-primary text-primary'
                    }],
                    bStateSave: true,
                    bDestroy: true,
                    select: false,
                    destroy: true,
                    ajax: {
                        url: "/GTRANS/public/Facture/api/ind.php",
                        type: "POST",
                        data: function (d) {
                            return JSON.stringify({
                                "0": "GET_INV_DOS",
                                "1": $('#DOS_NUM').val()
                            });
                        },
                    },
                    aoColumnDefs: [{
                        "sClass": "hide_me",
                        "aTargets": [0]
                    }],
                    columns: [{
                            "data": "AAM_CODE"
                        },
                        {
                            "data": "AAM_NUM_FACTURE",
                            "render": function (data, type, row) {
                                return '<a id="SHOWFACT">' + row.AAM_NUM_FACTURE + '</a>';
                            }
                        },
                        {
                            "data": "AAM_DATE_FACTURE"
                        },
                        {
                            "data": "BCMD"
                        },
                        {
                            "data": "AAM_TOTAL_TTC"
                        },
                        {
                            "data": "AAM_DEVISE"
                        },
                        {
                            "data": "AAM_DES_CLIENT"
                        }, {
                            "data": null,
                            "defaultContent": '<a id="delbtn" class="btn btn-link text-danger"><i class="icon-trash"></i></a>'
                        }
                    ],
                    initComplete: function (settings, json) {
                        /*var iii = TAB_MIG_FACT.columns(5)
                            .data().eq(0);
                        var jjj = TAB_MIG_FACT.columns(2)
                            .data().eq(0);*/
                    }
                }).columns.adjust().draw();
            });
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#DOS_NUM').on('keypress', function (e) {
        if (e.which === 13) {
            $('#get_dos').click();
        }
    });

    $('#add_fact_to_dos').on('click', function () {
        //'/GTRANS/public/Dossier/?NumD='+$('#DOS_NUM').val()
        if ($('#DOS_NUM').val()) {
            $.post('/GTRANS/public/Achat/api/fact.php', JSON.stringify(['SEARCH_DOS', $('#DOS_NUM').val()]), function (data) {
                if (Object.keys(data).length) {
                    switch (data[0].DM_CODE_COMP_GROUP) {
                        case null: //Case Aerian
                        case '':
                            $('#DOS_TYPE').empty().text('Aèrien ' + data[0].DM_IMP_EXP);
                            TYP_DOS = 'A';
                            break;

                        case 'G':
                            $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' GROUPAGE');
                            TYP_DOS = 'G';
                            break;

                        case 'C':
                            $('#DOS_TYPE').empty().text('MARITIME ' + data[0].DM_IMP_EXP + ' COMPLET');
                            TYP_DOS = 'C';
                            break;
                        default:
                            PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                            TYP_DOS = 'E';
                            break;
                    }
                } else {
                    PNotify_Alert('Avertissement', 'Numéro de dossier inconnu ou incorrect', 'warning');
                }
            }).always(function (data) {
                $('#INP_NDOS').val($('#DOS_NUM').val());
                $('#GET_FACT_DATE').click();
                $('#NEW_NFACT').click();
                var type_se = '';
                switch (TYP_DOS) {
                    case 'A':
                        type_se = 'GET_DOS_TAB_AER';
                        break;
                    case 'G':
                    case 'C':
                        type_se = 'GET_DOS_TAB_MAR';
                        break;

                    default:
                        break;
                }
                TAB_DOS = $('#TAB_DOS').DataTable({
                    dom: '<"datatable-scroll-wrap"t><"datatable-footer">',
                    select: {
                        style: 'single',
                        info: false
                    },
                    destroy: true,
                    ajax: {
                        url: "/GTRANS/public/Facture/api/ind.php",
                        type: "POST",
                        data: function (d) {
                            return JSON.stringify({
                                "0": type_se,
                                "1": $('#DOS_NUM').val()
                            });
                        },
                    },
                    aoColumnDefs: [{
                        "sClass": "hide_me",
                        "aTargets": [0]
                    }],
                    columns: [{
                            "data": "DM_CLE"
                        },
                        {
                            "data": "NUM_BL_LTA"
                        },
                        {
                            "data": "CL_LIBELLE"
                        },
                        {
                            "data": "POD"
                        },
                        {
                            "data": "POL"
                        },
                        {
                            "data": "DM_DATE_EMBARQ"
                        },
                        {
                            "data": "DM_DATE_DECHARG"
                        }

                    ],
                    initComplete: function () {
                        $('#modal_theme_bg_custom').modal('show');
                    }
                }).columns.adjust().draw();
                /*var win = window.open('/GTRANS/public/Facture/Fact?NumD=' + $('#DOS_NUM').val(), '_blank'); //C:\xampp\htdocs\GTRANS\public\Facture\Fact.php
                win.focus();*/
            });
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#SET_GO_TO_FACT').on('click', function () {
        if ($('#DOS_NUM').val()) {
            if (TAB_DOS.rows('.selected').any()) {
                var dmcle = TAB_DOS.rows('.selected').data()[0].DM_CLE;
                $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['GET_DOS_DATA', dmcle]))
                    .done(function (data) {
                        //console.log(data)
                        var d = {
                            'AAM_CODE_DOSSIER': dmcle,
                            'AAM_CODE_CLIENT': data[0].DM_CLIENT,
                            'AAM_DES_CLIENT': data[0].DM_CLIENT, //lib
                            'AAM_ADR_CLIENT': data[0].DM_CLIENT, //adr
                            'AAM_NAVIRE': data[0].DM_CLIENT, //name
                            'AAM_PORT_EMBARQ': data[0].DM_CLIENT, //name
                            'AAM_PORT_DECHAG': data[0].DM_CLIENT, //name
                            'AAM_DATE_DEBARQ': data[0].DM_DATE_EMBARQ,
                            'AAM_FOURNISSEUR': data[0].DM_CLIENT, //name
                            'AAM_MARCHANDISE': data[0].DM_MARCHANDISE,
                            'AAM_NBR_COLIS': data[0].DM_NOMBRE,
                            'AAM_MARQUE': data[0].DM_MARQUE,
                            'AAM_POIDS': data[0].DM_POD,
                            'AAM_ESCALE': data[0].DM_ESCALE,
                            'AAM_RUBRIQUE': data[0].DM_RUBRIQUE,
                            'AAM_NUM_FACTURE': $('#INP_NFACT').val(),
                            'AAM_DATE_FACTURE': $('#INP_DATEFACT').val(),
                            'AAM_NUM_LTA': data[0].DM_NUM_LTA,
                            'AAM_AEROPOR_EMBARQ': data[0].DM_DEPART_DELIVERY,
                            'AAM_AEROPOR_DECHAG': data[0].DM_DEPART_RECEIP,
                            'AAM_IMP_EXP': data[0].DM_IMP_EXP,
                            'AAM_G_C': data[0].DM_CODE_COMP_GROUP,
                            'AAM_MARIT_AERIEN': (data[0].DM_CODE_COMP_GROUP == 'G' || data[0].DM_CODE_COMP_GROUP == 'C') ? 'MAR' : 'AER',
                            'AAM_NUM_DOSSIER': $('#DOS_NUM').val()
                        };
                        //console.log(d)
                        $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['SET_NEW_FACT', d, $('#INP_NFACT').val()]))
                            .done(function (data) {
                                $('#NEW_NFACT').click();
                                var win = window.open('/GTRANS/public/Facture/fact?DM_CLE=' + dmcle + '&AAM_CODE=' + data.AAM_CODE, '_blank');
                                win.focus();
                            })
                            .fail(function (error) {
                                PNotify_Alert('Errure', 'une erreur s\'est produite lors de la création de la facture', 'danger');
                            });
                    });
            } else {
                PNotify_Alert('Avertissement', 'Sélectionnez la ligne d\'un client dans la table', 'info');
            }
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#SET_GO_TO_FACTINV').on('click', function () {
        if ($('#DOS_NUM').val()) {
            if (TAB_DOS.rows('.selected').any()) {
                var dmcle = TAB_DOS.rows('.selected').data()[0].DM_CLE;
                $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['GET_DOS_DATA', dmcle]))
                    .done(function (data) {
                        //console.log(data)
                        var d = {
                            'AAM_CODE_DOSSIER': dmcle,
                            'AAM_CODE_CLIENT': data[0].DM_CLIENT,
                            'AAM_DES_CLIENT': data[0].DM_CLIENT, //lib
                            'AAM_ADR_CLIENT': data[0].DM_CLIENT, //adr
                            'AAM_NAVIRE': data[0].DM_CLIENT, //name
                            'AAM_PORT_EMBARQ': data[0].DM_CLIENT, //name
                            'AAM_PORT_DECHAG': data[0].DM_CLIENT, //name
                            'AAM_DATE_DEBARQ': data[0].DM_DATE_EMBARQ,
                            'AAM_FOURNISSEUR': data[0].DM_CLIENT, //name
                            'AAM_MARCHANDISE': data[0].DM_MARCHANDISE,
                            'AAM_NBR_COLIS': data[0].DM_NOMBRE,
                            'AAM_MARQUE': data[0].DM_MARQUE,
                            'AAM_POIDS': data[0].DM_POD,
                            'AAM_ESCALE': data[0].DM_ESCALE,
                            'AAM_RUBRIQUE': data[0].DM_RUBRIQUE,
                            'AAM_NUM_FACTURE': $('#INP_NFACT').val(),
                            'AAM_DATE_FACTURE': $('#INP_DATEFACT').val(),
                            'AAM_TYPE_FACT': 'DI',
                            'AAM_NUM_LTA': data[0].DM_NUM_LTA,
                            'AAM_AEROPOR_EMBARQ': data[0].DM_DEPART_DELIVERY,
                            'AAM_AEROPOR_DECHAG': data[0].DM_DEPART_RECEIP,
                            'AAM_IMP_EXP': data[0].DM_IMP_EXP,
                            'AAM_G_C': data[0].DM_CODE_COMP_GROUP,
                            'AAM_MARIT_AERIEN': (data[0].DM_CODE_COMP_GROUP == 'G' || data[0].DM_CODE_COMP_GROUP == 'C') ? 'MAR' : 'AER',
                            'AAM_NUM_DOSSIER': $('#DOS_NUM').val()
                        };
                        console.log(d)
                        $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['SET_NEW_FACT', d, $('#INP_NFACT').val()]))
                            .done(function (data) {
                                $('#NEW_NFACT').click();
                                var win = window.open('/GTRANS/public/Facture/DINV?DM_CLE=' + dmcle + '&AAM_CODE=' + data.AAM_CODE, '_blank');
                                win.focus();
                            })
                            .fail(function (error) {
                                PNotify_Alert('Errure', 'une erreur s\'est produite lors de la création de la facture', 'danger');
                            });
                    });
            } else {
                PNotify_Alert('Avertissement', 'Sélectionnez la ligne d\'un client dans la table', 'info');
            }
        } else {
            PNotify_Alert('Avertissement', 'Définir le numéro de dossier en premier', 'warning');
        }
    });

    $('#SERCH_FACT_BTN').on('click', function () {
        $.post('/GTRANS/sys/config/way.php', JSON.stringify(['FIND_FACT', $('#SERCH_FACT_INP').val()])).done(function (data) {
            if (data.URL !== undefined) {
                var win = window.open(data.URL, '_blank');
                win.focus();
            } else {
                new PNotify({
                    title: 'Rechercher une facture',
                    text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                    addclass: 'stack-bottom-right bg-warning-700',
                    icon: 'icon-cancel-circle2',
                    delay: 4000,
                    stack: {
                        "dir1": "up",
                        "dir2": "left",
                        "firstpos1": 25,
                        "firstpos2": 25
                    }
                });
            }
        });
    });

    $('#TAB_MIG_FACT tbody').on('click', 'a#SHOWFACT', function () {
        var data = TAB_MIG_FACT.row($(this).parents('tr')).data();
        $.post('/GTRANS/sys/config/way.php', JSON.stringify(['FIND_FACT', data.AAM_NUM_FACTURE])).done(function (data) {
            if (data.URL !== undefined) {
                var win = window.open(data.URL, '_blank');
                win.focus();
            } else {
                new PNotify({
                    title: 'Rechercher une facture',
                    text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                    addclass: 'stack-bottom-right bg-warning-700',
                    icon: 'icon-cancel-circle2',
                    delay: 4000,
                    stack: {
                        "dir1": "up",
                        "dir2": "left",
                        "firstpos1": 25,
                        "firstpos2": 25
                    }
                });
            }
        });
    });

    $('#TAB_MIG_FACT tbody').on('click', 'a#delbtn', function () {
        var data = TAB_MIG_FACT.row($(this).parents('tr')).data();
        swal({
                title: "mot de passe administrateur",
                text: "Entrez le mot de passe de l'administrateur pour confirmation",
                type: "input",
                showCancelButton: true,
                confirmButtonColor: "#2196F3",
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "..."
            },
            function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("Mot de passe requis !");
                    return false
                }
                var num_f = data.AAM_NUM_FACTURE
                $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['CONF_DEL_PASS', inputValue, data.AAM_CODE])).fail(function (data) {
                    swal({
                        title: "Mot de passe incorrect !",
                        text: "Erreur : contacter l\'administrateur !",
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    });
                }).done(function (data) {
                    swal({
                        title: "suppression réussie",
                        text: "La facture numéro " + num_f + " a été supprimée",
                        type: "success",
                        confirmButtonColor: "#2196F3"
                    });
                    TAB_MIG_FACT.ajax.reload();
                });

            });
    });

    $('#TAB_MIG_INV tbody').on('click', 'a#SHOWFACT', function () {
        var data = TAB_MIG_INV.row($(this).parents('tr')).data();
        $.post('/GTRANS/sys/config/way.php', JSON.stringify(['FIND_FACT', data.AAM_NUM_FACTURE])).done(function (data) {
            if (data.URL !== undefined) {
                var win = window.open(data.URL, '_blank');
                win.focus();
            } else {
                new PNotify({
                    title: 'Rechercher une facture',
                    text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                    addclass: 'stack-bottom-right bg-warning-700',
                    icon: 'icon-cancel-circle2',
                    delay: 4000,
                    stack: {
                        "dir1": "up",
                        "dir2": "left",
                        "firstpos1": 25,
                        "firstpos2": 25
                    }
                });
            }
        });
    });

    $('#TAB_MIG_INV tbody').on('click', 'a#delbtn', function () {
        var data = TAB_MIG_INV.row($(this).parents('tr')).data();
        swal({
                title: "mot de passe administrateur",
                text: "Entrez le mot de passe de l'administrateur pour confirmation",
                type: "input",
                showCancelButton: true,
                confirmButtonColor: "#2196F3",
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "..."
            },
            function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("Mot de passe requis !");
                    return false
                }
                var num_f = data.AAM_NUM_FACTURE
                $.post('/GTRANS/public/Facture/api/ind.php', JSON.stringify(['CONF_DEL_PASS_INV', inputValue, data.AAM_CODE])).fail(function (data) {
                    swal({
                        title: "Mot de passe incorrect !",
                        text: "Erreur : contacter l\'administrateur !",
                        confirmButtonColor: "#EF5350",
                        type: "error"
                    });
                }).done(function (data) {
                    swal({
                        title: "suppression réussie",
                        text: "La facture numéro " + num_f + " a été supprimée",
                        type: "success",
                        confirmButtonColor: "#2196F3"
                    });
                    TAB_MIG_INV.ajax.reload();
                });

            });
    });
});