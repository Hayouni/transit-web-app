<?php


include_once($_SERVER['DOCUMENT_ROOT'] . '/GTRANS/public/users/check_login_status.php');
if ($user_ok != true || $log_username == "") {
    header("location: /GTRANS");
    exit();
}
if (!in_array($_SESSION['LEVEL'], ["Administrateur", "Financier"])) {
    header("location: /GTRANS");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>GTRANS | FACTURATION</title>

    <!-- Global stylesheets -->
    <link href="../../assets/css/icons/Roboto/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="../../assets/css/currency/currency.own.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="../../assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/wizards/steps.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/forms/validation/validate.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/pickers/daterangepicker.js"></script>
    <script type="text/javascript" src="../../assets/js/pages/components_popups.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/notifications/pnotify.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/extensions/session_timeout.min.js"></script>

    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/select.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/responsive.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script type="text/javascript" src="../../assets/js/plugins/tables/datatables/extensions/boutton.flash.min.js"></script>

    
    <!-- Theme JS files -->
    <script type="text/javascript" src="../../assets/js/core/app.js"></script>
    <script type="text/javascript" src="../Dossier/api/NumberToLetter.js"></script>
    <script type="text/javascript" src="api/ind.js"></script>
    <!-- /theme JS files -->

    <style>
         .selected td {
            background-color: #545252 !important;
            color: #ff4242 !important;
            font-weight: 700;
        }

        table.minimalistBlack {
            border: 3px solid #000000;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        
        table.minimalistBlack td,
        table.minimalistBlack th {
            border: 1px solid #000000;
            padding: 5px 4px;
        }
        
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        
        table.minimalistBlack thead {
            background: #CFCFCF;
            background: -moz-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: -webkit-linear-gradient(top, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            background: linear-gradient(to bottom, #dbdbdb 0%, #d3d3d3 66%, #CFCFCF 100%);
            border-bottom: 3px solid #000000;
        }
        
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        
        table.minimalistBlack tfoot {
            font-size: 14px;
            font-weight: bold;
            color: #000000;
            border-top: 3px solid #000000;
        }
        
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }
    </style>

</head>

<body class="navbar-top scrollbar" id="style-body">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/navbar.html") ?>
    </div>
    <!-- /main navbar -->


    <!-- Page container -->
    <div class="page-container">

        <!-- Page content -->
        <div class="page-content">

            <!-- Main sidebar -->
            <div class="sidebar sidebar-main sidebar-fixed">
                <div class="sidebar-content scrollbar"  id="style-scrollbar">
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/sidebar.php") ?>
                </div>
            </div>
            <!-- /main sidebar -->
            <!-- Main content -->
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header page-header-default">
                    <div class="page-header-content">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="page-title">
                                    <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Accueil</span> - Facturation
                                    </h4>
                                </div>
                            </div>
                            <div class="col-md-3">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">

                    <!-- Invoice template -->
                    <div class="panel panel panel-default border-grey">
                        <div class="panel-heading">
                            <h6 class="panel-title">Facturation</h6>
                            <div class="heading-elements">
                            
                            </div>
                        </div>

                        <div class="panel-body no-padding-bottom">
                            <div class="row"> 
                                <div class="col-md-4"></div>
                                <div class="col-md-4 text-center">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default border-primary text-primary" id="open_dos"><i class="icon-folder-search"></i></button>
                                            </span>
                                            <input type="text" class="form-control text-center border-primary text-primary" id="DOS_NUM" placeholder="N°...">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default border-primary text-primary" id="get_dos"><i class="icon-file-download2"></i></button>
                                                <button class="btn btn-default border-primary text-primary" id="add_fact_to_dos"><i class="icon-file-plus"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="label label-block border-primary text-primary" id="DOS_TYPE">Entrez le numéro de dossier</span>
                                    <br>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Liste des factures</h5>
                                    <table class="table display datatable-button-print-rows table-striped table-bordered htNoWrap" style="width:100%" id="TAB_MIG_FACT">
                                        <thead class="border-primary text-primary">
                                            <tr>
                                                <th class="hide_me">N°</th>
                                                <th>N° Facture</th>
                                                <th>Date</th>
                                                <th>Type</th>
                                                <th>B.Cmd</th>
                                                <th>Total TTC</th>
                                                <th>Client</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-black">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h5>Liste des "Invoices"</h5>
                                    <table class="table display datatable-button-print-rows table-striped table-bordered htNoWrap" style="width:100%" id="TAB_MIG_INV">
                                        <thead class="border-primary text-primary">
                                            <tr>
                                                <th class="hide_me">N°</th>
                                                <th>N° Facture</th>
                                                <th>Date</th>
                                                <th>Règlement</th>
                                                <th>Total</th>
                                                <th>Devise</th>
                                                <th>Client</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-black">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /invoice template -->

                    <!-- Custom background color -->
                    <div id="modal_theme_bg_custom" class="modal fade">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content bg-primary-700">

                                <div class="modal-body">
                                    <fieldset>
                                        <legend class="text-semibold text-white">
                                            <i class="icon-file-text2 position-left"></i>
                                                Ajouter une nouvelle facture
                                            <a class="control-arrow text-white" data-toggle="collapse" data-target="#part1">
                                                <i class="icon-circle-down2"></i>
                                            </a>
                                        </legend>

                                        <div class="collapse in" id="part1">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>N° Dossier:</label>
                                                        <input type="text" class="form-control text-center bg-primary-400 border-default" id="INP_NDOS" placeholder="N° ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>N° Facture:</label>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control text-center bg-primary-400 border-default" id="INP_NFACT" placeholder="N° ...">
                                                            <span class="input-group-btn">
                                                                <button class="btn text-center bg-primary-400 border-default" type="button" id="NEW_NFACT"><i class="icon-sync"></i></button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="form-group">
                                                        <label>Date Facture:</label>
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <button class="btn text-center bg-primary-400 border-default" type="button" id="GET_FACT_DATE"><i class="icon-calendar"></i></button>
                                                            </span>
                                                            <input type="date" class="form-control text-center bg-primary-400 border-default" id="INP_DATEFACT" placeholder="Date ...">
                                                            <span class="input-group-btn">
                                                                <button class="btn text-center bg-primary-400 border-default" type="button" id="SET_GO_TO_FACT"><i class="icon-arrow-right13 position-left"></i> Divers/Avoirs</button>
                                                                <button class="btn text-center bg-primary-400 border-default" type="button" id="SET_GO_TO_FACTINV"><i class="icon-arrow-right13 position-left"></i>Invoice</button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table table-bordered" style="width:100%" id="TAB_DOS">
                                                        <thead class="text-center bg-primary-400 border-default">
                                                            <tr>
                                                                <th class="hide_me">N°</th>
                                                                <th>N° BL/LTA</th>
                                                                <th>Client</th>
                                                                <th>POD</th>
                                                                <th>POL</th>
                                                                <th>Date Embarq.</th>
                                                                <th>Date Décharg.</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend class="text-semibold text-white">
                                            <i class="icon-folder-search position-left"></i>
                                                Chercher une autre facture
                                            <a class="control-arrow text-white" data-toggle="collapse" data-target="#part2">
                                                <i class="icon-circle-down2"></i>
                                            </a>
                                        </legend>

                                        <div class="collapse in" id="part2">
                                            <div class="form-group">
                                                <label>N° Facture:</label>
                                                <div class="input-group">
                                                    <input type="text" id="SERCH_FACT_INP" class="form-control text-center bg-primary-400 border-default" placeholder="N° ...">
                                                    <span class="input-group-btn">
                                                        <button id="SERCH_FACT_BTN" class="btn text-center bg-primary-400 border-default" type="button"><i class="icon-arrow-right13"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /custom background color -->


                    <!-- Footer -->
                    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/GTRANS/sys/include/html/footer.php") ?>
                    <!-- /footer -->
                </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>