$(window).on("load", function () {
    $("body").removeClass("no-transitions")
    $('head').append('<link rel="stylesheet" type="text/css" href="/GTRANS/assets/css/icons/fontawesome/font-awesome-animation.min.css">');
}), $(function () {
    function e() {
        var e = $(window).height() - $(".page-container").offset().top - $(".navbar-fixed-bottom").outerHeight();
        $(".page-container").attr("style", "min-height:" + e + "px")
    }
    $("body").addClass("no-transitions"), e(), $(".panel-footer").has("> .heading-elements:not(.not-collapsible)").prepend('<a class="heading-elements-toggle"><i class="icon-more"></i></a>'), $(".page-title, .panel-title").parent().has("> .heading-elements:not(.not-collapsible)").children(".page-title, .panel-title").append('<a class="heading-elements-toggle"><i class="icon-more"></i></a>'), $(".page-title .heading-elements-toggle, .panel-title .heading-elements-toggle").on("click", function () {
        $(this).parent().parent().toggleClass("has-visible-elements").children(".heading-elements").toggleClass("visible-elements")
    }), $(".panel-footer .heading-elements-toggle").on("click", function () {
        $(this).parent().toggleClass("has-visible-elements").children(".heading-elements").toggleClass("visible-elements")
    }), $(".breadcrumb-line").has(".breadcrumb-elements").prepend('<a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>'), $(".breadcrumb-elements-toggle").on("click", function () {
        $(this).parent().children(".breadcrumb-elements").toggleClass("visible-elements")
    }), $(document).on("click", ".dropdown-content", function (e) {
        e.stopPropagation()
    }), $(".navbar-nav .disabled a").on("click", function (e) {
        e.preventDefault(), e.stopPropagation()
    }), $('.dropdown-content a[data-toggle="tab"]').on("click", function (e) {
        $(this).tab("show")
    }), $(".panel [data-action=reload]").click(function (e) {
        e.preventDefault();
        var a = $(this).parent().parent().parent().parent().parent();
        $(a).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: "#fff",
                opacity: .8,
                cursor: "wait",
                "box-shadow": "0 0 0 1px #ddd"
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: "none"
            }
        }), window.setTimeout(function () {
            $(a).unblock()
        }, 2e3)
    }), $(".category-title [data-action=reload]").click(function (e) {
        e.preventDefault();
        var a = $(this).parent().parent().parent().parent();
        $(a).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: "#000",
                opacity: .5,
                cursor: "wait",
                "box-shadow": "0 0 0 1px #000"
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: "none",
                color: "#fff"
            }
        }), window.setTimeout(function () {
            $(a).unblock()
        }, 2e3)
    }), $(".sidebar-default .category-title [data-action=reload]").click(function (e) {
        e.preventDefault();
        var a = $(this).parent().parent().parent().parent();
        $(a).block({
            message: '<i class="icon-spinner2 spinner"></i>',
            overlayCSS: {
                backgroundColor: "#fff",
                opacity: .8,
                cursor: "wait",
                "box-shadow": "0 0 0 1px #ddd"
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: "none"
            }
        }), window.setTimeout(function () {
            $(a).unblock()
        }, 2e3)
    }), $(".category-collapsed").children(".category-content").hide(), $(".category-collapsed").find("[data-action=collapse]").addClass("rotate-180"), $(".category-title [data-action=collapse]").click(function (a) {
        a.preventDefault();
        var i = $(this).parent().parent().parent().nextAll();
        $(this).parents(".category-title").toggleClass("category-collapsed"), $(this).toggleClass("rotate-180"), e(), i.slideToggle(150)
    }), $(".panel-collapsed").children(".panel-heading").nextAll().hide(), $(".panel-collapsed").find("[data-action=collapse]").addClass("rotate-180"), $(".panel [data-action=collapse]").click(function (a) {
        a.preventDefault();
        var i = $(this).parent().parent().parent().parent().nextAll();
        $(this).parents(".panel").toggleClass("panel-collapsed"), $(this).toggleClass("rotate-180"), e(), i.slideToggle(150)
    }), $(".panel [data-action=close]").click(function (a) {
        a.preventDefault();
        var i = $(this).parent().parent().parent().parent().parent();
        e(), i.slideUp(150, function () {
            $(this).remove()
        })
    }), $(".category-title [data-action=close]").click(function (a) {
        a.preventDefault();
        var i = $(this).parent().parent().parent().parent();
        e(), i.slideUp(150, function () {
            $(this).remove()
        })
    }), $(".navigation").find("li.active").parents("li").addClass("active"), $(".navigation").find("li").not(".active, .category-title").has("ul").children("ul").addClass("hidden-ul"), $(".navigation").find("li").has("ul").children("a").addClass("has-ul"), $(".dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu").has("li.active").addClass("active").parents(".navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)").addClass("active"), $(".navigation-main > .navigation-header > i").tooltip({
        placement: "right",
        container: "body"
    }), $(".navigation-main").find("li").has("ul").children("a").on("click", function (e) {
        e.preventDefault(), $(this).parent("li").not(".disabled").not($(".sidebar-xs").not(".sidebar-xs-indicator").find(".navigation-main").children("li")).toggleClass("active").children("ul").slideToggle(250), $(".navigation-main").hasClass("navigation-accordion") && $(this).parent("li").not(".disabled").not($(".sidebar-xs").not(".sidebar-xs-indicator").find(".navigation-main").children("li")).siblings(":has(.has-ul)").removeClass("active").children("ul").slideUp(250)
    }), $(".navigation-alt").find("li").has("ul").children("a").on("click", function (e) {
        e.preventDefault(), $(this).parent("li").not(".disabled").toggleClass("active").children("ul").slideToggle(200), $(".navigation-alt").hasClass("navigation-accordion") && $(this).parent("li").not(".disabled").siblings(":has(.has-ul)").removeClass("active").children("ul").slideUp(200)
    }), $(".sidebar-main-toggle").on("click", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-xs")
    }), $(document).on("click", ".navigation .disabled a", function (e) {
        e.preventDefault()
    }), $(document).on("click", ".sidebar-control", function (a) {
        e()
    }), $(document).on("click", ".sidebar-main-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-main-hidden")
    }), $(document).on("click", ".sidebar-secondary-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-secondary-hidden")
    }), $(document).on("click", ".sidebar-detached-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-detached-hidden")
    }), $(document).on("click", ".sidebar-all-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-all-hidden")
    }), $(document).on("click", ".sidebar-opposite-toggle", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-opposite-visible"), $("body").hasClass("sidebar-opposite-visible") ? ($("body").addClass("sidebar-xs"), $(".navigation-main").children("li").children("ul").css("display", "")) : $("body").removeClass("sidebar-xs")
    }), $(document).on("click", ".sidebar-opposite-main-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-opposite-visible"), $("body").hasClass("sidebar-opposite-visible") ? $("body").addClass("sidebar-main-hidden") : $("body").removeClass("sidebar-main-hidden")
    }), $(document).on("click", ".sidebar-opposite-secondary-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-opposite-visible"), $("body").hasClass("sidebar-opposite-visible") ? $("body").addClass("sidebar-secondary-hidden") : $("body").removeClass("sidebar-secondary-hidden")
    }), $(document).on("click", ".sidebar-opposite-hide", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-all-hidden"), $("body").hasClass("sidebar-all-hidden") ? ($("body").addClass("sidebar-opposite-visible"), $(".navigation-main").children("li").children("ul").css("display", "")) : $("body").removeClass("sidebar-opposite-visible")
    }), $(document).on("click", ".sidebar-opposite-fix", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-opposite-visible")
    }), $(".sidebar-mobile-main-toggle").on("click", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-mobile-main").removeClass("sidebar-mobile-secondary sidebar-mobile-opposite sidebar-mobile-detached")
    }), $(".sidebar-mobile-secondary-toggle").on("click", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-mobile-secondary").removeClass("sidebar-mobile-main sidebar-mobile-opposite sidebar-mobile-detached")
    }), $(".sidebar-mobile-opposite-toggle").on("click", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-mobile-opposite").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached")
    }), $(".sidebar-mobile-detached-toggle").on("click", function (e) {
        e.preventDefault(), $("body").toggleClass("sidebar-mobile-detached").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-opposite")
    }), $(window).on("resize", function () {
        setTimeout(function () {
            e(), $(window).width() <= 768 ? ($("body").addClass("sidebar-xs-indicator"), $(".sidebar-opposite").insertBefore(".content-wrapper"), $(".sidebar-detached").insertBefore(".content-wrapper"), $(".dropdown-submenu").on("mouseenter", function () {
                $(this).children(".dropdown-menu").addClass("show")
            }).on("mouseleave", function () {
                $(this).children(".dropdown-menu").removeClass("show")
            })) : ($("body").removeClass("sidebar-xs-indicator"), $(".sidebar-opposite").insertAfter(".content-wrapper"), $("body").removeClass("sidebar-mobile-main sidebar-mobile-secondary sidebar-mobile-detached sidebar-mobile-opposite"), $("body").hasClass("has-detached-left") ? $(".sidebar-detached").insertBefore(".container-detached") : $("body").hasClass("has-detached-right") && $(".sidebar-detached").insertAfter(".container-detached"), $(".page-header-content, .panel-heading, .panel-footer").removeClass("has-visible-elements"), $(".heading-elements").removeClass("visible-elements"), $(".dropdown-submenu").children(".dropdown-menu").removeClass("show"))
        }, 100)
    }).resize(), $('[data-popup="popover"]').popover(), $('[data-popup="tooltip"]').tooltip()
});
$(function () {

    function loadScript(url, callback) {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.onreadystatechange = callback;
        script.onload = callback;
        head.appendChild(script);
    }

    var d = new Date();
    var n = d.getFullYear();
    var cssRule =
        "color: #FF0000;" +
        "font-size: 30px;" +
        "font-weight: bold;" +
        "text-shadow: 0px 0px 1px rgba(0, 0, 0, 0.5);";
    console.log("%cCopyright GAYTH BACCARI | GTeCh+ © " + n, cssRule);
    var cssRule2 =
        "color: #000000;" +
        "opacity: 0.5;" +
        "font-size: 15px;" +
        "text-decoration: underline;" +
        "font-weight: bold;";
    console.log("%cIl s’agit d’une fonctionnalité de navigateur conçue pour les développeurs.", cssRule2);

    //Pnotif stack
    var stack_top_left = {
        "dir1": "down",
        "dir2": "right",
        "push": "top"
    };
    var stack_bottom_left = {
        "dir1": "right",
        "dir2": "up",
        "push": "top"
    };
    var stack_bottom_right = {
        "dir1": "up",
        "dir2": "left",
        "firstpos1": 25,
        "firstpos2": 25
    };
    var stack_custom_left = {
        "dir1": "right",
        "dir2": "down"
    };
    var stack_custom_right = {
        "dir1": "left",
        "dir2": "up",
        "push": "top"
    };
    var stack_custom_top = {
        "dir1": "down",
        "dir2": "right",
        "push": "top",
        "spacing1": 1
    };
    var stack_custom_bottom = {
        "dir1": "up",
        "dir2": "right",
        "spacing1": 1
    };

    function get_nv_mailcount() {
        $.post('/GTRANS/public/users/api/mail.php', JSON.stringify(['get_counter_for_mail'])).done((data) => {
            $('.nv_mail_data_co').text(data.unseen);
        }).fail((error) => {
            console.log('error : ', error);
            $('.nv_mail_data_co').removeClass('bg-info-400').addClass('bg-danger-400').html('<i class="fa fa-times"></i>');
        });
    }

    //e-mail synchro
    var auto_sync;
    if (location.pathname !== '/GTRANS/public/users/login_unlock' && location.pathname !== '/GTRANS/') {
        //session timeout
        /*$.sessionTimeout({
            heading: 'h5',
            title: 'Expiration de la session',
            message: 'Votre session est sur le point d\'expirer. Voulez-vous rester connecté?',
            ignoreUserActivity: false,
            warnAfter: 900000,
            logoutBtnText: 'Se déconnecter',
            keepBtnText: 'Rester connecté',
            redirAfter: 903000,
            keepAliveUrl: '/',
            redirUrl: '/GTRANS/public/users/login_unlock',
            logoutUrl: '/GTRANS/public/users/logout'
        });*/

        loadScript("/GTRANS/assets/js/core/libraries/jquery_ui/widgets.min.js", function () {
            $.datepicker.setDefaults($.datepicker.regional["fr"]);
            $(".datepicker_nvbr").datepicker({
                closeText: 'Fermer',
                prevText: 'Précédent',
                nextText: 'Suivant',
                currentText: 'Aujourd\'hui',
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
                dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
                weekHeader: 'Sem.',
                dateFormat: 'yy-mm-dd'
            });
        });

        //claender eventes
        $.post('/GTRANS/public/users/api/profile.php', JSON.stringify(['get_user_cal_events']), function (data) {
            data.forEach(function (element) {
                var htt = '<li class="media">\
                                        <div class="media-left"><img src="/GTRANS/assets/images/calen.png" class="img-circle img-sm" alt=""></div>\
                                        <div class="media-body">\
                                            <a target="_blank" href="/GTRANS/public/users/user" class="media-heading text-semibold">' + element['title'] + '</a>\
                                            <span class="display-block text-muted text-size-small">' + element['start'] + '</span>\
                                        </div>\
                                        <div class="media-right media-middle"><span class="status-mark" style="color:' + element['color'] + '"></span></div>\
                                    </li>';
                $('#nv_cal_events').append(htt);
            }, this);
            $('#nv_cal_events_co').html(Object.keys(data).length);
            if (Object.keys(data).length > 0) {
                $('#alert_event_icon').removeClass('animated-hover').addClass('animated');
            } else {
                $('#alert_event_icon').removeClass('animated').addClass('animated-hover');
            }
        });

        $('#make_seen_btn').on('click', function () {
            $.post('/GTRANS/public/users/api/profile.php', JSON.stringify(['make_events_seen'])).done(function (data) {
                $('#nv_cal_events').empty();
                $('#nv_cal_events_co').html('0');
                $('#alert_event_icon').removeClass('animated').addClass('animated-hover');
            });
        });

        // Count dossier active side bare
        $.post('/GTRANS/public/Dossier/api/tdb.php', JSON.stringify(['COUNT_ACTIVE_DOS'])).done(function (data) {
            $('#sb_count_act_dos').html(data);
        });

        $.post('/GTRANS/public/Commercial/api/prosp.php', JSON.stringify(['DRAFT', 'NUMBER_TODAY'])).done(function (data) {
            $('#sb_count_act_draft').html(data.DP_NOTIF);
        });

        // session config mail
        $.post('/GTRANS/sys/session.php').done(function (data) {
            data = JSON.parse(data);
            $('#nv_uname').html(data.user_name);
            $('#nv_upic').attr('src', data.user_pic);
            if (data.level == "Administrateur") {
                $('#nv_levl_account_type').removeClass("bg-success").addClass("bg-danger");
            }
            $('#nv_levl_account_type').text(data.level)
            /*if (data.email !== 'off' && data.smtp_pass == 'on') {
                var cache = $('#nv_mail_str').children();
                $('#nv_mail_str').text(data.email).append(cache);
                if (data.imap_host == 'off' || data.imap_port == 'off') {
                    $('.nv_mail_data_co').removeClass('bg-info-400').addClass('bg-danger-400').html('<i class="fa fa-times"></i>');
                    $('#nv_synch_mail').parent('li').addClass('disabled');
                    $('#nv_stat_recieve').text('Non').removeClass('text-success-600').addClass('text-danger-600');
                } else {
                    $('#nv_stat_recieve').text('Oui').removeClass('text-danger-600').addClass('text-success-600');
                }
                if (data.smtp_host == 'off' || data.smtp_port == 'off') {
                    $('#nv_stat_send').text('Non').removeClass('text-success-600').addClass('text-danger-600');
                } else {
                    $('#nv_stat_send').text('Oui').removeClass('text-danger-600').addClass('text-success-600');
                }
                if (data.mail_autoload == 'off') {
                    $('#nv_stat_sync').text('Non').removeClass('text-success-600').addClass('text-danger-600');
                } else {
                    $('#nv_stat_sync').text('Oui').removeClass('text-danger-600').addClass('text-success-600');
                }
            } else {
                $('.nv_mail_data_co').removeClass('bg-info-400').addClass('bg-danger-400').html('<i class="fa fa-times"></i>');
                $('#nv_mail_str').text('Email non configuré');
                $('#nv_synch_mail').parent('li').addClass('disabled');
                $('#nv_stat_send').text('Non').addClass('text-danger-600');
                $('#nv_stat_recieve').text('Non').addClass('text-danger-600');
                $('#nv_stat_sync').text('Non').addClass('text-danger-600');
            }*/
        }).always(function (data) {
            /*get_nv_mailcount();
            data = JSON.parse(data);
            if (data.mail_autoload == 'off') {
                clearTimeout(auto_sync);
            } else {
                auto_sync = setTimeout(function() {
                    $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_data'])).done(function(data) {
                        if (data.error) {
                            console.log(data.error)
                            new PNotify({
                                title: 'E-Mail Sync',
                                text: "une erreur est survenue, essayez de nouveau ou vérifiez votre configuration",
                                addclass: 'stack-bottom-left bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: stack_bottom_left,
                                opacity: 0.9,
                                width: "220px"
                            });
                        } else if (data.count == '0') {} else {
                            new PNotify({
                                text: data.count + " Nouveaux emails reçus",
                                addclass: 'stack-bottom-left bg-success-700',
                                icon: 'icon-checkmark4',
                                delay: 4000,
                                hide: false,
                                stack: stack_bottom_left,
                                opacity: 0.9,
                                width: "220px"
                            });
                            get_nv_mailcount();
                        }
                    }).fail(function(error) {
                        console.log(error)
                        new PNotify({
                            title: 'E-Mail Sync',
                            text: "une erreur est survenue, essayez de nouveau ou vérifiez votre configuration",
                            addclass: 'stack-bottom-left bg-danger-700',
                            icon: 'icon-cancel-circle2',
                            delay: 4000,
                            stack: stack_bottom_left,
                            opacity: 0.9,
                            width: "220px"
                        });
                    });
                }, 9000);
            }*/
        });

        //seacrh bar
        loadScript("/GTRANS/assets/js/plugins/forms/inputs/typeahead/bootstrap3-typeahead.js", function () {

            $('#srch-term').typeahead({
                source: function (query, result) {
                    $('#ico_srch_inp').removeClass('icon-search4').addClass('icon-spinner2 spinner');
                    $.ajax({
                        url: "/GTRANS/sys/config/search.php",
                        method: "POST",
                        data: {
                            query: query
                        },
                        dataType: "json",
                        success: function (data) {
                            $('#ico_srch_inp').removeClass('icon-spinner2 spinner').addClass('icon-search4');
                            result($.each(data, function () {
                                return this;
                            }));
                            /*result($.map(data, function(item) {
                                return item;
                            }));*/
                        }
                    })
                },
                onselect: function (item) {
                    switch (item[1]) {
                        case 'Dossier':
                            if (~item[2].indexOf("N° BL")) {
                                var win = window.open('/GTRANS/public/Dossier/?NumD=' + item[3], '_blank');
                                win.focus();
                            } else if (~item[2].indexOf("N° LTA")) {
                                var win = window.open('/GTRANS/public/Dossier/?NumD=' + item[3], '_blank');
                                win.focus();
                            } else {
                                var win = window.open('/GTRANS/public/Dossier/?NumD=' + item[0], '_blank');
                                win.focus();
                            }

                            break;
                        case 'A.Arrivée':
                            var win = window.open('/GTRANS/public/Dossier/fact/AA?DM_CLE=' + item[3], '_blank');
                            win.focus();
                            break;

                        case 'Invoice':
                            var win = window.open('/GTRANS/public/Dossier/fact/inv?DM_CLE=' + item[3], '_blank');
                            win.focus();
                            break;

                        case 'Facture':
                            $.post('/GTRANS/sys/config/way.php', JSON.stringify(['FIND_FACT', item[0]])).done(function (data) {
                                if (data.URL !== undefined) {
                                    var win = window.open(data.URL, '_blank');
                                    win.focus();
                                } else {
                                    new PNotify({
                                        title: 'Rechercher une facture',
                                        text: "Facture non trouvée, choisissez une autre ou contactez le développeur",
                                        addclass: 'stack-bottom-right bg-warning-700',
                                        icon: 'icon-cancel-circle2',
                                        delay: 4000,
                                        stack: stack_bottom_right
                                    });
                                }
                            });
                            break;

                        default:
                            new PNotify({
                                title: 'Rechercher',
                                text: "mauvais choix !!",
                                addclass: 'stack-bottom-right bg-warning-700',
                                icon: 'icon-cancel-circle2',
                                delay: 4000,
                                stack: stack_bottom_right
                            });
                            break;
                    }
                },
                displayText: function (item) {
                    return typeof item[0] !== 'undefined' && typeof item[0].name != 'undefined' ? item[0].name : item[0];
                },
                matcher: function (item) {
                    var it = this.displayText(item);
                    return ~it.toLowerCase().indexOf(this.query.toLowerCase());
                },
                render: function (items) {
                    var that = this;
                    var self = this;
                    var activeFound = false;
                    var data = [];
                    var _category = that.options.separator;
                    $.each(items, function (key, value) {
                        // inject separator
                        if (key > 0 && value[_category] !== items[key - 1][_category]) {
                            data.push({
                                __type: 'divider'
                            });
                        }

                        // inject category header
                        if (value[_category] && (key === 0 || value[_category] !== items[key - 1][_category])) {
                            data.push({
                                __type: 'category',
                                name: value[_category]
                            });
                        }
                        data.push(value);
                    });

                    items = $(data).map(function (i, item) {
                        if ((item.__type || false) == 'category') {
                            return $(that.options.headerHtml).text(item.name)[0];
                        }

                        if ((item.__type || false) == 'divider') {
                            return $(that.options.headerDivider)[0];
                        }

                        var text = self.displayText(item);
                        i = $(that.options.item).data('value', item);
                        var extra_style = '';
                        switch (item[1]) {
                            case 'Dossier':
                                if (~item[2].indexOf("N° BL")) {
                                    extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span><span class="text-muted text-size-small pull-right">' + item[3] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-folder"></i> ' + item[1] + '</div>';
                                } else if (~item[2].indexOf("N° LTA")) {
                                    extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span><span class="text-muted text-size-small pull-right">' + item[3] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-folder"></i> ' + item[1] + '</div>';
                                } else {
                                    extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-folder"></i> ' + item[1] + '</div>';
                                }
                                break;
                            case 'A.Arrivée':
                                extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-sticky-note"></i> ' + item[1] + '</div>';
                                break;
                            case 'Invoice':
                                extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-sticky-note"></i> ' + item[1] + '</div>';
                                break;
                            case 'Facture':
                                extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-file-text"></i> ' + item[1] + '</div>';
                                break;

                            default:
                                extra_style = '<span class="label bg-danger-400 pull-right">' + item[2] + '</span>' + that.highlighter(text, item) + '<div class="text-size-small mb-5 pl-10"><i class="fa fa-exclamation-circle"></i> ' + item[1] + '</div>';
                                break;
                        }
                        i.find('a').html(extra_style);
                        if (this.followLinkOnSelect) {
                            i.find('a').attr('href', self.itemLink(item));
                        }
                        if (text == self.$element.val()) {
                            i.addClass('active');
                            self.$element.data('active', item);
                            activeFound = true;
                        }
                        return i[0];
                    });

                    if (this.autoSelect && !activeFound) {
                        items.filter(':not(.dropdown-header)').first().addClass('active');
                        this.$element.data('active', items.first().data('value'));
                    }
                    this.$menu.html(items);
                    return this;
                },
            });
        });
    }


    /*$('#nv_synch_mail').on('click', function() {
        var notice = new PNotify({
            text: "Sync. E-Mail",
            addclass: 'stack-bottom-left bg-violet-700',
            type: 'info',
            icon: 'icon-spinner3 spinner',
            hide: false,
            stack: stack_bottom_left,
            buttons: {
                closer: false,
                sticker: false
            },
            opacity: 0.9,
            width: "220px"
        });
        $.post('/GTRANS/public/users/mailing/getmails.php', JSON.stringify(['mail_data'])).done(function(data) {
            if (data.error) {
                console.log(data.error)
                notice.update({
                    text: "une erreur est survenue, essayez de nouveau ou vérifiez votre configuration",
                    addclass: 'stack-bottom-left bg-warning-700',
                    icon: 'icon-cancel-circle2',
                    hide: true,
                    delay: 4000
                });
            } else if (data.count == '0') {
                notice.update({
                    text: "pas de nouveau courriel",
                    addclass: 'stack-bottom-left bg-info-700',
                    icon: 'icon-checkmark4',
                    hide: true,
                    delay: 4000
                });
            } else {
                notice.update({
                    text: data.count + " Nouveaux emails reçus",
                    addclass: 'stack-bottom-left bg-success-700',
                    icon: 'icon-checkmark4',
                    hide: true,
                    delay: 4000
                });
                get_nv_mailcount();
            }
        }).fail(function(error) {
            console.log(error)
            notice.update({
                text: "une erreur est survenue, essayez de nouveau ou vérifiez votre configuration",
                addclass: 'stack-bottom-left bg-danger-700',
                icon: 'icon-cancel-circle2',
                hide: true,
                delay: 4000
            });
        });
    });*/
});