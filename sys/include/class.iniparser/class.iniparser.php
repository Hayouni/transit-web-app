<?php
/******************************************************
 * �2006 copyrights by RE-Desgin (www.re-design.de)   *
 * Author: Enrico Reinsdorf (enrico@.re-design.de)    *
 * Modified: 2006-01-16                               *
 ******************************************************/

class iniParser {
	
	var $_iniFilename = '';
	var $_iniParsedArray = array();
	
	/** 
	*  creates a multidimensional array from the INI file
	**/
	function iniParser( $filename )
	{
		$this->_iniFilename = $filename;
		if($this->_iniParsedArray = parse_ini_file( $filename, true ) ) {
			return true;
		} else {
			return false;
		} 
	}
	
	/**
	* returns the complete section
	**/
	function getSection( $key )
	{
		return $this->_iniParsedArray[$key];
	}
	
	/**
	*  returns a value from a section
	**/
	function getValue( $section, $key )
	{
		if(!isset($this->_iniParsedArray[$section])) return false;
		return $this->_iniParsedArray[$section][$key];
	}
	
	/**
	*  returns the value of a section or the entire section
	**/
	function get( $section, $key=NULL )
	{
		if(is_null($key)) return $this->getSection($section);
		return $this->getValue($section, $key);
	}
	
	/**
	* Arrow value according to specified key
	**/
	function setSection( $section, $array )
	{
		if(!is_array($array)) return false;
		return $this->_iniParsedArray[$section] = $array;
	}
	
	/**
	* sets a new value in a section
	**/
	function setValue( $section, $key, $value )
	{
		if( $this->_iniParsedArray[$section][$key] = $value ) return true;
	}
	
	/**
	* sets a new value in a section or an entire new section
	**/
	function set( $section, $key, $value=NULL )
	{
		if(is_array($key) && is_null($value)) return $this->setSection($section, $key);
		return $this->setValue($section, $key, $value);
	}
	
	/**
	* saves the entire array to the INI file
	**/
	function save( $filename = null )
	{
		if( $filename == null ) $filename = $this->_iniFilename;
		if( is_writeable( $filename ) ) {
			$SFfdescriptor = fopen( $filename, "w" );
			foreach($this->_iniParsedArray as $section => $array){
				fwrite( $SFfdescriptor, "[" . $section . "]\n" );
				foreach( $array as $key => $value ) {
					fwrite( $SFfdescriptor, "$key = $value\n" );
				}
				fwrite( $SFfdescriptor, "\n" );
			}
			fclose( $SFfdescriptor );
			return true;
		} else {
			return false;
		}
	}
}
?>