<div class="modal fade" id="ModalAddEvent" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEvent">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="/GTRANS/public/users/api/addEvent.php">
                <div class="modal-header bg-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelEvent"><i class="icon-calendar2"></i> &nbsp;Ajouter un évènement</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" name="titleEvent" class="form-control border-black border-lg text-black" id="titleEvent" placeholder="Titre">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea rows="7" cols="6" name="descriptionEvent" id="descriptionEvent" class="form-control border-black border-lg text-black" placeholder="Description..."></textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="colorEvent" class="col-sm-3 control-label">Couleur</label>
                                <div class="col-sm-9">
                                    <input class="form-control border-black border-lg text-black" type="color" name="colorEvent" id="colorEvent">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="startEvent" class="col-sm-3 control-label">Début</label>
                                <div class="col-sm-9">
                                    <input type="date" name="startEvent" class="form-control border-black border-lg text-black" id="startEvent">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="endEvent" class="col-sm-3 control-label">Fin</label>
                                <div class="col-sm-9">
                                    <input type="date" name="endEvent" class="form-control border-black border-lg text-black" id="endEvent">
                                </div>
                            </div>                                                                
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    <button type="submit" class="btn btn-primary">Sauvegarder les modifications</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="footer text-muted">
    &copy; 2018. <a href="#">Gestions Transit Web App </a> by <a href="https://github.com/orgs/GTeCHSOFT" target="_blank">GTeCH+</a> <span style="right: 5px;position: inherit;">V 3.0.1 Beta Demo <i class="icon-comments pull-right pl-5"></i></span>
</div>