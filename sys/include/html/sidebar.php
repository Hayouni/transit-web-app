<?php
function get_active($pl = '', $out = null)
{
    if (is_null($out)) {
        return ($pl === $_SERVER['REQUEST_URI']) ? 'class="active"' : '';
    } else {
        return (strpos($_SERVER['REQUEST_URI'], "$out") !== false) ? 'class="active"' : '';
    }
}

?>
<!-- User menu -->
<div class="sidebar-user">
    <div class="category-content">
        <div class="media">
            <a class="media-left"><img src="/GTRANS/assets/images/medways_log_sb.png" class="img-responsive" alt=""></a>
            <!--div class="media-body">
                <span class="media-heading text-semibold">MEDWAYS <span class="text-size-mini text-muted">international</span></span>
                <div class="text-size-mini text-muted">
                    <i class="icon-pin text-size-small"></i> &nbsp;Rades, Tunis
                </div>
            </div-->
        </div>
    </div>
</div>
<!-- /user menu -->


<!-- Main navigation -->
<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <li class="navigation-header"><span><i class="icon-pin text-size-small"></i> &nbsp;Rades, Tunis</span> <i class="icon-menu" title="Main pages"></i></li>
            <!-- Main -->
            <!--class="active"-->
            <li <?php echo get_active('/GTRANS/public/'); ?>>
                <a href="/GTRANS/public"><i class="icon-home4"></i> <span>Tableau de bord</span></a>
            </li>

            <li <?php echo get_active('/GTRANS/public/Dossier/', 'NumD');
                echo get_active('/GTRANS/public/Dossier/');
                echo get_active('/GTRANS/public/Dossier/conte');
                echo get_active('/GTRANS/public/Facture/', 'Facture/');
                echo get_active('/GTRANS/public/Finance/');
                echo get_active('/GTRANS/public/Dossier/search'); ?>>
                <a href="#"><i class="icon-cart-remove"></i> <span>Vente</span></a>
                <ul>
                    <li <?php echo get_active('/GTRANS/public/Dossier/');
                        echo get_active('/GTRANS/public/Dossier/', 'NumD'); ?>><a href="/GTRANS/public/Dossier/"><i class="icon-folder"></i> Dossiers <span class="label bg-blue-400" id="sb_count_act_dos">0</span></a></li>
                    <li <?php echo get_active('/GTRANS/public/Dossier/search'); ?>><a href="/GTRANS/public/Dossier/search"><i class="icon-folder-search"></i>Recherche</a></li>
                    <li <?php echo get_active('/GTRANS/public/Dossier/conte'); ?>><a href="/GTRANS/public/Dossier/conte"><i class="fa fa-tasks"></i>Conteneurs Clients</a></li>
                    <?php if (in_array($_SESSION['LEVEL'], ["Administrateur", "Financier"])) { ?>
                    <li <?php echo get_active('/GTRANS/public/Facture/', 'Facture/'); ?>>
                        <a href="/GTRANS/public/Facture/"><i class="icon-stack-empty"></i> <span>Facturation</span></a>
                    </li>
                    <?php 
                } else {
                } ?>
                    <?php if (in_array($_SESSION['LEVEL'], ["Administrateur", "Financier", "Commercial", "Comptable"])) { ?>
                    <li <?php echo get_active('/GTRANS/public/Finance/'); ?>>
                        <a href="/GTRANS/public/Finance"><i class="icon-file-spreadsheet2"></i> <span>Règlement</span></a>
                    </li>
                    <?php 
                } else {
                } ?>
                </ul>
            </li>

            <li <?php echo get_active('/GTRANS/public/Achat/');
                echo get_active('/GTRANS/public/Achat/fact');
                echo get_active('/GTRANS/public/Achat/fact', 'Srch');
                echo get_active('/GTRANS/public/Achat/dep');
                echo get_active('/GTRANS/public/Achat/conte'); ?>>
                <a href="#"><i class="icon-cart-add2"></i> <span>Achat</span></a>
                <ul>
                    <li <?php echo get_active('/GTRANS/public/Achat/conte'); ?>><a href="/GTRANS/public/Achat/conte"><i class="fa fa-tasks"></i>Conteneurs Fournisseurs</a></li>
                    <li <?php echo get_active('/GTRANS/public/Achat/fact');
                        echo get_active('/GTRANS/public/Achat/fact', 'Srch'); ?>><a href="/GTRANS/public/Achat/fact"><i class="fa fa-money"></i>Facture</a></li>
                    <li <?php echo get_active('/GTRANS/public/Achat/dep'); ?>><a href="/GTRANS/public/Achat/dep"><i class="fa fa-cart-plus"></i>Dépense</a></li>
                    <li <?php echo get_active('/GTRANS/public/Achat/'); ?>><a href="/GTRANS/public/Achat/"><i class="fa fa-book"></i>Règlement</a></li>
                </ul>
            </li>

            <li <?php echo get_active('/GTRANS/public/Achat/mbrut');
                echo get_active('/GTRANS/public/Achat/mbrutsur');
                echo get_active('/GTRANS/public/Achat/lmbrut'); ?>>
                <a href="#"><i class="fa fa-balance-scale"></i> <span>Marge Brute</span></a>
                <ul>
                    <li <?php echo get_active('/GTRANS/public/Achat/mbrut'); ?>><a href="/GTRANS/public/Achat/mbrut"><i class="icon-folder2"></i>Dossier</a></li>
                    <li <?php echo get_active('/GTRANS/public/Achat/lmbrut'); ?>><a href="/GTRANS/public/Achat/lmbrut"><i class="icon-calendar2"></i>Plage de Date</a></li>
                    <li <?php echo get_active('/GTRANS/public/Achat/mbrutsur'); ?>><a href="/GTRANS/public/Achat/mbrutsur"><i class="icon-libreoffice"></i>Surestarie</a></li>
                </ul>
            </li>
            <!--
            <li <?php echo get_active('/GTRANS/public/Achat/mbrut');
                echo get_active('/GTRANS/public/Achat/lmbrut'); ?>><a href="/GTRANS/public/Achat/mbrut"><i class="fa fa-balance-scale"></i>Marge Brute</a></li>
            -->
            <?php if (in_array($_SESSION['LEVEL'], ["Administrateur", "Commercial", "Exploitation"])) { ?>            
            <li <?php echo get_active('/GTRANS/public/Commercial/rsearch');
                echo get_active('/GTRANS/public/Commercial/rapport');
                echo get_active('/GTRANS/public/Commercial/draft');
                echo get_active('/GTRANS/public/Commercial/history'); ?>>
                <a href="#"><i class="icon-file-text"></i> <span>Commercial</span></a>
                <ul>
                    <li <?php echo get_active('/GTRANS/public/Commercial/rsearch'); ?>><a href="/GTRANS/public/Commercial/rsearch"><i class="fa fa-search"></i>Recherche</a></li>
                    <li <?php echo get_active('/GTRANS/public/Commercial/rapport');
                        echo get_active('/GTRANS/public/Commercial/history') ?>><a href="/GTRANS/public/Commercial/rapport"><i class="fa fa-file-text-o"></i>Rapport</a></li>
                    <li <?php echo get_active('/GTRANS/public/Commercial/draft'); ?>><a href="/GTRANS/public/Commercial/draft"><i class="fa fa-pencil-square-o"></i>Brouillon <span class="label bg-success-400" id="sb_count_act_draft">0</span></a></li>
                </ul>
            </li>
            <?php 
        } else {
        } ?>

            <li <?php echo get_active('/GTRANS/public/Stat/list'); ?>><a href="/GTRANS/public/Stat/list"><i class="icon-stats-bars2"></i> <span>Statistique</span></a></li>

            <?php if (in_array($_SESSION['LEVEL'], ["Administrateur", "Financier", "Exploitation"])) { ?>
            <li <?php echo get_active('/GTRANS/public/Outils/', 'Outils/'); ?>><a href="/GTRANS/public/Outils"><i class="icon-cogs"></i> <span>Outils</span></a></li>
            <?php 
        } else {
        } ?>

            <?php if ($_SESSION['LEVEL'] == "Administrateur") { ?>
            <li class="navigation-header"><span><i class="icon-user-tie"></i> Administration</span> <i class="icon-menu" title="Main pages"></i></li>
            <li <?php echo get_active('/GTRANS/public/Admin/priv'); ?>><a href="/GTRANS/public/Admin/priv"><i class="icon-man-woman"></i> <span>Gestions utilisateurs</span></a></li>
            <?php 
        } else {
        } ?>
            <!--li <?php echo get_active('/GTRANS/public/Admin/person');
                    echo get_active('/GTRANS/public/Admin/priv'); ?>>
                <a href="#"><i class="icon-man-woman"></i> <span>Gestions utilisateurs</span></a>
                <ul>
                    <li <?php echo get_active('/GTRANS/public/Admin/person'); ?>><a href="/GTRANS/public/Admin/person">Personnels</a></li>
                    <li <?php echo get_active('/GTRANS/public/Admin/priv'); ?>><a href="/GTRANS/public/Admin/priv">privilèges & mot de passe</a></li>
                </ul>
            </li-->
        </ul>
    </div>
</div>
<!-- /main navigation -->