<?php
error_reporting(E_ERROR | E_PARSE);
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
$Seesion = [
            'user_connected' => $_SESSION["username"],
            'user_name' => $_SESSION["full_name"],
            'mail_autoload' => $_SESSION["mail_autoload"],
            'email' => $_SESSION["email"],
            'level' => $_SESSION["LEVEL"],
            'smtp_pass' => $_SESSION["smtp_pass"],
            'imap_host' => $_SESSION["imap_host"],
            'imap_port' => $_SESSION["imap_port"],
            'smtp_host' => $_SESSION["smtp_host"],
            'smtp_port' => $_SESSION["smtp_port"],
            'user_pic' => $_SESSION["user_pic"]
            ];
echo json_encode($Seesion, JSON_FORCE_OBJECT);
?>