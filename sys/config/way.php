<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 
$str_json = file_get_contents('php://input'); //($_POST doesn't work here)
$response = json_decode($str_json, true); // decoding received JSON to array
if (is_null($response) && strpos($str_json, '&') !== false){
    $jqxdata = explode('&',$str_json);
    foreach ($jqxdata as &$value) {
        $resp_init = explode('=',$value);
        $response[$resp_init[0]] = $resp_init[1];
    }
}
switch ($response[0]) {
    case 'FIND_FACT':
        $db = new MySQL();
        $FIND_FACT = $db->get_results("SELECT AAM_CODE,AAM_NUM_FACTURE,AAM_CODE_DOSSIER,AAM_TYPE_FACT,AAM_G_C,AAM_NUM_DOSSIER,AAM_NUMERO,AAM_NUM_CONTENEUR FROM avis_arrive_mig WHERE AAM_NUM_FACTURE = '$response[1]'");
        if (count($FIND_FACT) > 0) {
            switch ($FIND_FACT[0]['AAM_TYPE_FACT']) {
                case 'T':
                case 'A':
                    if ($FIND_FACT[0]['AAM_G_C'] == 'G') {
                        echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/AA?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER']));
                    } else if ($FIND_FACT[0]['AAM_G_C'] == 'C'){
                        echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/AAC?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER']));
                    } else {
                        echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/AAA?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER']));
                    }
                    break;
                
                case 'M':
                    echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/MAG?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER']));
                    break;
                
                case 'S':
                    //echo json_encode(['NOT']);
                    $CC = $db->get_results("SELECT
                                                * 
                                            FROM
                                                dossier_container
                                                INNER JOIN dossier_marchandise ON DM_CODE = DC_CODE_MARCHANDISE
                                            WHERE
                                            DM_NUM_DOSSIER = {$FIND_FACT[0]['AAM_NUM_DOSSIER']} AND DC_NUM_CONTAINER = '{$FIND_FACT[0]['AAM_NUM_CONTENEUR']}'");
                    echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/SU?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER'].'&CN='.$FIND_FACT[0]['AAM_NUM_CONTENEUR'].'&CC='.$CC[0]['DC_CODE']));
                    break;
                
                default:
                    echo json_encode(array('URL'=> '/GTRANS/public/Facture/fact?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER'].'&AAM_CODE='.$FIND_FACT[0]['AAM_CODE']));
                    break;
            }
        } else {
            echo json_encode(['NOT']);
        }
        break;

    case 'FIND_FACT_PRINT':
        $db = new MySQL();
        $FIND_FACT = $db->get_results("SELECT AAM_CODE,AAM_NUM_FACTURE,AAM_CODE_DOSSIER,AAM_TYPE_FACT,AAM_G_C,AAM_NUM_DOSSIER,AAM_NUMERO FROM avis_arrive_mig WHERE AAM_NUM_FACTURE = '$response[1]'");
        if (count($FIND_FACT) > 0) {
            switch ($FIND_FACT[0]['AAM_TYPE_FACT']) {
                case 'T':
                    echo json_encode(array('URL'=> '/GTRANS/public/Dossier/fact/AA?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER']));
                    break;
                
                default:
                    echo json_encode(array('URL'=> '/GTRANS/public/Facture/fact?DM_CLE='.$FIND_FACT[0]['AAM_CODE_DOSSIER'].'&AAM_CODE='.$FIND_FACT[0]['AAM_CODE']));
                    break;
            }
        } else {
            echo json_encode(['NOT']);
        }
        break;


    default:
        echo json_encode('{"0":"Error"}');
        echo json_encode($response);
        break;
}
?>