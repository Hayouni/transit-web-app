<?php
/*
 * Created on Sat Sep 23 2017
 *
 * Copyright (c) 2017 GTeCh+
 * Author :  GAYTH BACCARI
 * E-Mail :  gaith_baccari@hotmail.fr
 * Github :  github.com/orgs/GTeCHSOFT
 */

include_once ($_SERVER['DOCUMENT_ROOT'].'/GTRANS/sys/drivers/mysql.php');
header("Content-type: application/json"); 

$request = $_POST["query"];
$db = new MySQL();
$dossier_maritime_val = [];
//Num Dossier
$dossier_maritime = $db->get_results("SELECT DM_NUM_DOSSIER,DM_CODE_COMP_GROUP FROM dossier_maritime WHERE DM_NUM_DOSSIER LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $typ = ($dossier_maritime[$i]['DM_CODE_COMP_GROUP'] == '' || $dossier_maritime[$i]['DM_CODE_COMP_GROUP'] == NULL) ? '<i class="fa fa-plane"></i>':'<i class="fa fa-ship"></i>';
    $dossier_maritime_val[] = [$dossier_maritime[$i]['DM_NUM_DOSSIER'],'Dossier',$typ];
}
//Num BL
$dossier_maritime = $db->get_results("SELECT DM_NUM_BL,DM_NUM_DOSSIER FROM dossier_maritime WHERE DM_NUM_BL LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $dossier_maritime_val[] = [$dossier_maritime[$i]['DM_NUM_BL'],'Dossier','N° BL',$dossier_maritime[$i]['DM_NUM_DOSSIER']];
}
//Num LTA
$dossier_maritime = $db->get_results("SELECT DM_NUM_LTA,DM_NUM_DOSSIER FROM dossier_maritime WHERE DM_NUM_LTA LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $dossier_maritime_val[] = [$dossier_maritime[$i]['DM_NUM_LTA'],'Dossier','N° LTA',$dossier_maritime[$i]['DM_NUM_DOSSIER']];
}
//Num AA
$dossier_maritime = $db->get_results("SELECT AAM_NUMERO,AAM_CODE_DOSSIER FROM avis_arrive_mig WHERE AAM_NUMERO LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $dossier_maritime_val[] = [$dossier_maritime[$i]['AAM_NUMERO'],'A.Arrivée','N°A',$dossier_maritime[$i]['AAM_CODE_DOSSIER']];
}
//Num Fact
$dossier_maritime = $db->get_results("SELECT AAM_NUM_FACTURE,AAM_TYPE_FACT FROM avis_arrive_mig WHERE AAM_NUM_FACTURE LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $dossier_maritime_val[] = [$dossier_maritime[$i]['AAM_NUM_FACTURE'],'Facture',$dossier_maritime[$i]['AAM_TYPE_FACT']];
}

//Num INVOICE
$dossier_maritime = $db->get_results("SELECT AAM_NUM_FACTURE,AAM_CODE_DOSSIER FROM invoice WHERE AAM_NUM_FACTURE LIKE '%$request%'");
for ($i=0; $i < count($dossier_maritime); $i++) { 
    $dossier_maritime_val[] = [$dossier_maritime[$i]['AAM_NUM_FACTURE'],'Invoice','N°',$dossier_maritime[$i]['AAM_CODE_DOSSIER']];
}
echo json_encode($dossier_maritime_val);
?>